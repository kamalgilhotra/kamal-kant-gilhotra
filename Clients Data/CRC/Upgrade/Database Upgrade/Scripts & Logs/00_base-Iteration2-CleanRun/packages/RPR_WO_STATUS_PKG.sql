CREATE OR REPLACE package rpr_wo_status_pkg is
	G_PKG_VERSION varchar(35) := '2018.2.1.0';
	 /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: RPR_WO_STATUS_PKG
   || Description: Functions and Procedures to maipulate Tax Repairs basis buckets
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 2015.2   07/19/2016 D Brimeyer
   ||          02/14/2018 Chase H.       two fixes for related_wos
   ||          05/16/2018 N Gburek		 TEST_EXPLANATION column size forced match
   ||          06/19/2018 Chase H.		 Work Order Type RUC and Replacement Check logic, ignore HW for Quantity testing
   ||============================================================================
   */

	 procedure p_test_work_orders(a_batch_id         in number,
																a_company_id       in number,
																a_repair_schema_id in number,
																a_start_month      in number,
																a_end_month        in number,
																a_usage_flag       in varchar2);

   procedure p_test_work_orders_single (a_batch_id  in number);
   procedure p_batch_process_status (a_batch_id  in number);

end RPR_WO_STATUS_PKG;
/

CREATE OR REPLACE package body rpr_wo_status_pkg is
	 /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: rpr_wo_status_pkg
   || Description: Functions and Procedures to calculate tax repairs
   ||              status for work orders
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 2015.2   07/19/2016 D Brimeyer
   ||          02/14/2018 Chase H.       two fixes for related_wos
   ||          05/16/2018 N Gburek		 TEST_EXPLANATION column size forced match
   ||============================================================================
   */

 procedure P_TEST_WORK_ORDERS_SINGLE(a_batch_id         in number) is
      code int;
   begin

     delete from WORK_ORDER_TAX_STATUS_PROC_ARC where batch_id = a_batch_id;

     insert into WORK_ORDER_TAX_STATUS_PROCESS
         (batch_id, work_order_id, funding_wo_indicator, funding_wo_id, company_id)
         select a_batch_id, w.work_order_id, w.funding_wo_indicator, w.funding_wo_id, w.company_id
           from related_wos rw, work_order_control w
          where w.work_order_id = rw.work_order_id
            and rw.relation_id in (select r.relation_id
                                    from related_wos r, WORK_ORDER_TAX_STATUS_PROCESS T
                                   where r.work_order_id = t.work_order_id
                                     and t.funding_wo_indicator = 0)
            and NOT EXISTS
          (select 1 from WORK_ORDER_TAX_STATUS_PROCESS y where y.work_order_id = w.work_order_id);

      UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
         SET (t.relation_id) =
             (select Max(relation_id) from related_wos r where r.work_order_id = t.work_order_id)
       where t.batch_id = a_batch_id;

      --Test all work orders under a funding project
      insert into WORK_ORDER_TAX_STATUS_PROCESS
         (batch_id, work_order_id, funding_wo_indicator, funding_wo_id, company_id)
         select a_batch_id, w.work_order_id, w.funding_wo_indicator, w.funding_wo_id, w.company_id
           from related_wos rw, work_order_control w
          where w.work_order_id = rw.work_order_id
            and rw.relation_id in (select r.relation_id
                                    from related_wos r, WORK_ORDER_TAX_STATUS_PROCESS T
                                   where r.work_order_id = t.work_order_id
                                     and t.funding_wo_indicator = 0)
            and NOT EXISTS
          (select 1 from WORK_ORDER_TAX_STATUS_PROCESS y where y.work_order_id = w.work_order_id);

      insert into WORK_ORDER_TAX_STATUS_PROCESS
         (batch_id, work_order_id, funding_wo_indicator, funding_wo_id, company_id)
         select a_batch_id, woc.work_order_id, woc.funding_wo_indicator, woc.funding_wo_id, woc.company_id
           from work_order_control woc, WORK_ORDER_TAX_STATUS_PROCESS t
          where woc.funding_wo_id = t.work_order_id
            and not exists
          (select 1 from WORK_ORDER_TAX_STATUS_PROCESS x where x.work_order_id = woc.work_order_id);

      UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
         SET t.start_year_month = 0,
             t.end_year_month   = to_number(to_char(sysdate, 'yyyymm')),
             t.logfile          = '',
             t.test_explanation = ''
       where t.batch_id = a_batch_id;

      commit;
      p_batch_process_status(a_batch_id);

   end;

 procedure P_TEST_WORK_ORDERS(a_batch_id         in number,
																a_company_id       in number,
																a_repair_schema_id in number,
																a_start_month      in number,
																a_end_month        in number,
																a_usage_flag       in varchar2) is
			code int;
	 begin

			delete from WORK_ORDER_TAX_STATUS_PROCESS where batch_id = a_batch_id;
			delete from WORK_ORDER_TAX_STATUS_PROC_ARC where batch_id = a_batch_id;

			insert into WORK_ORDER_TAX_STATUS_PROCESS
				 (batch_id, work_order_id, funding_wo_indicator, funding_wo_id, company_id)
				 (SELECT a_batch_id,
								 work_order_control.work_order_id,
								 work_order_control.funding_wo_indicator,
								 work_order_control.funding_wo_id,
								 work_order_control.company_id
						FROM work_order_control, work_order_tax_repairs, wo_tax_expense_test, company, repair_schema
					 WHERE work_order_control.funding_wo_indicator = 0
								--AND work_order_control.company_id = a_company_id
						 AND work_order_control.company_id = a_company_id
						 AND work_order_control.work_order_id = work_order_tax_repairs.work_order_id
						 AND work_order_tax_repairs.tax_expense_test_id = wo_tax_expense_test.tax_expense_test_id
						 AND wo_tax_expense_test.repair_schema_id = repair_schema.repair_schema_id
						 AND Lower(repair_schema.usage_flag) = lower(a_usage_flag)
						 AND (repair_schema.repair_schema_id = a_repair_schema_id OR wo_tax_expense_test.always_capital = 1)
					MINUS
					SELECT a_batch_id,
								 work_order_control.work_order_id,
								 work_order_control.funding_wo_indicator,
								 work_order_control.funding_wo_id,
								 work_order_control.company_id
						FROM work_order_control,
								 cpr_ledger,
								 cpr_activity,
								 work_order_tax_repairs,
								 wo_tax_expense_test,
								 company,
								 repair_schema
					 WHERE cpr_ledger.asset_id = cpr_activity.asset_id
						 AND cpr_ledger.company_id = company.company_id
						 AND cpr_ledger.company_id = work_order_control.Company_Id
						 AND cpr_ledger.retirement_unit_id > 5
						 AND cpr_activity.ferc_activity_code = 1
						 AND cpr_activity.month_number <= a_end_month
								--AND work_order_control.company_id = a_company_id
						 AND work_order_control.company_id = a_company_id
						 AND work_order_control.work_order_number = cpr_activity.work_order_number
						 AND work_order_control.funding_wo_indicator = 0
						 AND work_order_control.work_order_id = work_order_tax_repairs.work_order_id
						 AND work_order_tax_repairs.tax_expense_test_id = wo_tax_expense_test.tax_expense_test_id
						 AND wo_tax_expense_test.repair_schema_id = repair_schema.repair_schema_id
						 AND Lower(repair_schema.usage_flag) = lower(a_usage_flag)
						 AND (repair_schema.repair_schema_id = a_repair_schema_id OR wo_tax_expense_test.always_capital = 1)) UNION
				 SELECT a_batch_id,
								work_order_control.work_order_id,
								work_order_control.funding_wo_indicator,
								work_order_control.funding_wo_id,
								work_order_control.company_id
					 FROM work_order_control,
								cpr_ledger,
								cpr_activity,
								work_order_tax_repairs,
								wo_tax_expense_test,
								company,
								repair_schema
					WHERE cpr_ledger.asset_id = cpr_activity.asset_id
						AND cpr_ledger.company_id = company.company_id
						AND cpr_ledger.company_id = work_order_control.company_id
						AND cpr_ledger.retirement_unit_id > 5
						AND cpr_activity.ferc_activity_code = 1
						AND cpr_activity.month_number BETWEEN a_start_month AND a_end_month
							 --AND work_order_control.company_id = a_company_id
						AND work_order_control.company_id = a_company_id
						AND work_order_control.work_order_number = cpr_activity.work_order_number
						AND work_order_control.funding_wo_indicator = 0
						AND work_order_control.work_order_id = work_order_tax_repairs.work_order_id
						AND work_order_tax_repairs.tax_expense_test_id = wo_tax_expense_test.tax_expense_test_id
						AND wo_tax_expense_test.repair_schema_id = repair_schema.repair_schema_id
						AND Lower (repair_schema.usage_flag) = lower
					(a_usage_flag)
						AND (repair_schema.repair_schema_id = a_repair_schema_id OR wo_tax_expense_test.always_capital = 1);

			insert into WORK_ORDER_TAX_STATUS_PROCESS
				 (batch_id, work_order_id, funding_wo_indicator, funding_wo_id, company_id)
				 select a_batch_id, w.work_order_id, w.funding_wo_indicator, w.funding_wo_id, w.company_id
					 from related_wos rw, work_order_control w
					where w.work_order_id = rw.work_order_id
						and rw.relation_id IN (select r.relation_id /* NCH 02.14.18 WAS = (select r.relation_id */
																		from related_wos r, WORK_ORDER_TAX_STATUS_PROCESS T
																	 where r.work_order_id = t.work_order_id
																		 and t.funding_wo_indicator = 0)
						and NOT EXISTS
					(select 1 from WORK_ORDER_TAX_STATUS_PROCESS y where y.work_order_id = w.work_order_id);

			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.relation_id) =
						 (select Max(relation_id) from related_wos r where r.work_order_id = t.work_order_id)
			 where t.batch_id = a_batch_id;

			--Test all work orders under a funding project
			insert into WORK_ORDER_TAX_STATUS_PROCESS
				 (batch_id, work_order_id, funding_wo_indicator, funding_wo_id, company_id)
				 select a_batch_id, w.work_order_id, w.funding_wo_indicator, w.funding_wo_id, w.company_id
					 from related_wos rw, work_order_control w
					where w.work_order_id = rw.work_order_id
						and rw.relation_id IN (select r.relation_id /* NCH 02.14.18 WAS = (select r.relation_id */
																		from related_wos r, WORK_ORDER_TAX_STATUS_PROCESS T
																	 where r.work_order_id = t.work_order_id
																		 and t.funding_wo_indicator = 0)
						and NOT EXISTS
					(select 1 from WORK_ORDER_TAX_STATUS_PROCESS y where y.work_order_id = w.work_order_id);

			insert into WORK_ORDER_TAX_STATUS_PROCESS
				 (batch_id, work_order_id, funding_wo_indicator, funding_wo_id, company_id)
				 select a_batch_id, woc.work_order_id, woc.funding_wo_indicator, woc.funding_wo_id, woc.company_id
					 from work_order_control woc, WORK_ORDER_TAX_STATUS_PROCESS t
					where woc.funding_wo_id = t.work_order_id
						and not exists
					(select 1 from WORK_ORDER_TAX_STATUS_PROCESS x where x.work_order_id = woc.work_order_id);

			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET t.start_year_month = a_start_month,
						 t.end_year_month   = a_end_month,
						 t.logfile          = '',
						 t.test_explanation = ''
			 where t.batch_id = a_batch_id;

			commit;
      p_batch_process_status(a_batch_id);
 end p_test_work_orders;

 procedure p_batch_process_status(a_batch_id         in number) is
      code int;
   begin

			code := analyze_table('WORK_ORDER_TAX_STATUS_PROCESS', 10);
			-- get repair schema and usage flag for each work order
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.repair_schema_id, t.usage_flag, t.tax_status_default_capital, t.repair_exclude, t.tax_expense_test_id) =
						 (select wtet.repair_schema_id,
										 rs.usage_flag,
										 wtet.tax_status_default_capital,
										 wtet.always_capital,
										 wtet.tax_expense_test_id
								from wo_tax_expense_test wtet, work_order_tax_repairs wotr, repair_schema rs
							 where wtet.tax_expense_test_id = wotr.tax_expense_test_id
								 and wtet.repair_schema_id = rs.repair_schema_id
								 and t.batch_id = a_batch_id
								 and wotr.work_order_id = t.work_order_id)
			 where t.batch_id = a_batch_id;

			-- get current open month for ccmpany being processed
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.current_open_month_number) =
						 (select company_open.current_open_month_number
								from (select company_id, max(current_open_month_number) current_open_month_number
												from (select company.company_id company_id,
																		 to_number(to_char(min(cpr_control.accounting_month), 'yyyymm')) current_open_month_number
																from company_setup company, cpr_control
															 where company.company_id = cpr_control.company_id
																 and cpr_control.cpr_closed is null
																 and cpr_control.accounting_month >
																		 (select max(inside.accounting_month)
																				from cpr_control inside
																			 where cpr_control.company_id = inside.company_id
																				 and inside.cpr_closed is not null)
															 group by company.company_id, company.description, company.gl_company_no
															union all
															select company.company_id, 0 current_open_month_number
																from company_setup company)
											 group by company_id) company_open
							 where t.company_id = company_open.company_id)
			 where t.batch_id = a_batch_id;

			code := analyze_table('WORK_ORDER_TAX_STATUS_PROCESS', 10);

			insert into work_order_tax_status
				 (work_order_id,
					effective_accounting_month,
					repair_unit_code_id,
					replacement_flag,
					repair_unit_source,
					replacement_source,
					REPAIR_LOC_ROLLUP_ID)
				 select woc.work_order_id,
								to_date(t.current_open_month_number, 'yyyymm'),
								wot.repair_unit_code_id,
								wot.replacement_flag,
								decode(wot.repair_unit_code_id, null, null, 'WO Type'),
								decode(wot.replacement_flag, null, null, 'WO Type'),
								0
					 from work_order_control woc, work_order_type wot, WORK_ORDER_TAX_STATUS_PROCESS T
					where woc.work_order_type_id = wot.work_order_type_id
						and woc.work_order_id = t.work_order_id
						and woc.funding_wo_id = t.funding_wo_id
						and t.batch_id = a_batch_id
						and not exists (select work_order_id
									 from work_order_tax_status w
									where w.work_order_id = t.work_order_id
									group by work_order_id);

			/*  UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
      SET (t.company_id,
           t.company_description,
           t.work_order_number,
           t.work_order_type_id) =
          (select c.COMPANY_ID,
                  c.DESCRIPTION,
                  w.work_order_number,
                  w.work_order_type_id
             from company c, work_order_control w
            where w.company_id = c.COMPANY_ID
              and w.work_order_id = t.work_order_id);*/

			-- get latest effect month record, if one exists
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.effective_accounting_month, reviewed_flag, external_reviewed_flag) =
						 (select nvl(max(effective_accounting_month), to_date('1900/01/01', 'yyyy/mm/dd')),
										 min(decode(reviewed_date, null, 0, 1)),
										 min(decode(external_reviewed_date, null, 0, 1))
								from work_order_tax_status
							 where work_order_id = t.work_order_id)
			 where t.batch_id = a_batch_id;

			-- get detais for work order
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.company_description,
							t.work_order_number,
							t.work_order_type_id,
							t.funding_wo_indicator,
							t.funding_wo_id,
							t.asset_location_id,
							t.major_location_id,
							t.repair_location_id,
							t.repair_location_method_id,
							t.tax_status_id,
							t.month_number,
							t.wo_current_revision) =
						 (select c.DESCRIPTION,
										 woc.work_order_number,
										 woc.work_order_type_id,
										 nvl(woc.funding_wo_indicator, 0),
										 woc.funding_wo_id,
										 al.asset_location_id,
										 ml.major_location_id,
										 decode(wotr.repair_location_method_id, 1, wotr.repair_location_id, al.repair_location_id) repair_location_id,
										 wotr.repair_location_method_id,
										 nvl(wots.tax_status_id, 0),
										 nvl((extract(year from wots.effective_accounting_month) * 100) +
												 extract(month from wots.effective_accounting_month),
												 190001),
										 woc.current_revision
								from work_order_control     woc,
										 company                c,
										 asset_location         al,
										 major_location         ml,
										 work_order_tax_repairs wotr,
										 work_order_tax_status  wots
							 where woc.work_order_id = t.work_order_id
								 and wotr.work_order_id = t.work_order_id
								 and t.batch_id = a_batch_id
								 and woc.company_id = c.COMPANY_ID
								 and wots.work_order_id = t.work_order_id
								 and woc.asset_location_id = al.asset_location_id(+)
								 and woc.major_location_id = ml.major_location_id(+)
								 and wots.effective_accounting_month =
										 (select max(effective_accounting_month)
												from work_order_tax_status
											 where work_order_id = t.work_order_id))
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id;

			--commit;

			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation) =
						 (select 'Could not find a default capital tax status for work order: ' || t.work_order_number ||
										 '. Please review',
										 test_explanation || ',  Could not find a default tax status.'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where (t.tax_status_default_capital is null or t.tax_status_default_capital <= 0)
				 and t.batch_id = a_batch_id;


     -- Tested at UOP Level - Capital
     UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
         SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
             (select 'Work order is set to "Tested at UOP Level - Capital"',
                     test_explanation ||
                     ',  Tested Tax Status set to "Tested at UOP Level - Capital".',
                     w.tax_status_default_capital,
                     '-999'
                from WORK_ORDER_TAX_STATUS_PROCESS w
               where w.work_order_id = t.work_order_id
                 and w.tax_status_id = -999
                 and t.batch_id = w.batch_id)
       where t.tax_status_id = -999
         and t.new_tax_status_id is null
         and t.batch_id = a_batch_id;


   -- Tested at UOP Level - Expense
     UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
         SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
             (select 'Work order is set to "Tested at UOP Level - Expense"',
                     test_explanation ||
                     ',  Tested Tax Status set to "Tested at UOP Level - Expense".',
                     w.tax_status_default_capital,
                     '-998'
                from WORK_ORDER_TAX_STATUS_PROCESS w
               where w.work_order_id = t.work_order_id
                 and w.tax_status_id = -998
                 and t.batch_id = w.batch_id)
       where t.tax_status_id = -999
         and t.new_tax_status_id is null
         and t.batch_id = a_batch_id;

			-- always capitalize
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Work order is set to ALWAYS CAPITAL',
										 test_explanation ||
										 ',  Tested Tax Status derived from "Default Status:  CAPITAL".  Test treatment set to Always use Default Status.',
										 w.tax_status_default_capital,
										 '1'
								from WORK_ORDER_TAX_STATUS_PROCESS w, wo_tax_status s
							 where w.work_order_id = t.work_order_id
								 and w.repair_exclude = 2
								 and w.tax_status_default_capital = s.tax_status_id
								 and s.expense_yn = 0
								 and t.batch_id = w.batch_id)
			 where t.repair_exclude = 2
				 and t.new_tax_status_id is null
				 and t.batch_id = a_batch_id;

			-- always expense
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Work order is set to ALWAYS EXPENSE',
										 test_explanation ||
										 ',  Tested Tax Status derived from "Default Status:  EXPENSE".  Test treatment set to Always use Default Status.',
										 w.tax_status_default_capital,
										 '2'
								from WORK_ORDER_TAX_STATUS_PROCESS w, wo_tax_status s
							 where w.work_order_id = t.work_order_id
								 and w.repair_exclude = 2
								 and w.tax_status_default_capital = s.tax_status_id
								 and s.expense_yn = 1
								 and t.batch_id = w.batch_id)
			 where t.repair_exclude = 2
				 and t.new_tax_status_id is null
				 and t.batch_id = a_batch_id;

			-- Figure out if actuals or estimates will be used
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.use_actuals) =
						 (select count(1)
								from cpr_ledger, cpr_activity
							 where cpr_ledger.asset_id = cpr_activity.asset_id
								 and cpr_ledger.company_id = t.company_id
								 and cpr_ledger.retirement_unit_id > 5
								 and cpr_activity.work_order_number = t.work_order_number
								 and cpr_activity.ferc_activity_code = 1
								 and rownum = 1)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id;

			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.te_fund_proj) =
						 (select nvl(te_fund_proj, 0)
								from work_order_control a, work_order_type b
							 where a.work_order_id = t.work_order_id
								 and a.work_order_type_id = b.work_order_type_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id;

			--Check the funding_project first, if not found, check the project
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (temp_tax_test_id,
							tax_status_from_fp,
							estimate_dollars_vs_units,
							est_revision,
							tax_status_no_replacement,
							tax_status_fp_fully_qualifying,
							tax_status_fp_non_qualifying,
							tax_status_ovh_to_udg,
							tax_status_minor_uop,
							add_retire_tolerance_pct,
							tax_status_over_tolerance,
							tax_status_over_max_amount) =
						 (select nvl(a.tax_expense_test_id, 0),
										 wtet.tax_status_from_fp,
										 wtet.estimate_dollars_vs_units,
										 wtet.est_revision,
										 wtet.tax_status_no_replacement,
										 wtet.tax_status_fp_fully_qualifying,
										 wtet.tax_status_fp_non_qualifying,
										 wtet.tax_status_ovh_to_udg,
										 wtet.tax_status_minor_uop,
										 wtet.add_retire_tolerance_pct,
										 wtet.tax_status_over_tolerance,
										 wtet.tax_status_over_max_amount
								from work_order_tax_repairs a, wo_tax_expense_test wtet, repair_schema rs
							 where a.work_order_id =
										 (select funding_wo_id from work_order_control c where c.work_order_id = t.work_order_id)
								 and a.tax_expense_test_id = wtet.tax_expense_test_id
								 and wtet.repair_schema_id = rs.repair_schema_id
								 and lower(rs.usage_flag) in ('cwip', 'cwip_cpr'))
			 where t.new_tax_status_id is null
				 and t.te_fund_proj <> 0
				 and t.batch_id = a_batch_id;

			--check indivual work ordrers
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (temp_tax_test_id,
							tax_status_from_fp,
							estimate_dollars_vs_units,
							est_revision,
							tax_status_no_replacement,
							tax_status_fp_fully_qualifying,
							tax_status_fp_non_qualifying,
							tax_status_ovh_to_udg,
							tax_status_minor_uop,
							add_retire_tolerance_pct,
							tax_status_over_tolerance,
							tax_status_over_max_amount) =
						 (select nvl(a.tax_expense_test_id, 0),
										 wtet.tax_status_from_fp,
										 wtet.estimate_dollars_vs_units,
										 wtet.est_revision,
										 wtet.tax_status_no_replacement,
										 wtet.tax_status_fp_fully_qualifying,
										 wtet.tax_status_fp_non_qualifying,
										 wtet.tax_status_ovh_to_udg,
										 wtet.tax_status_minor_uop,
										 wtet.add_retire_tolerance_pct,
										 wtet.tax_status_over_tolerance,
										 wtet.tax_status_over_max_amount
								from work_order_tax_repairs a, wo_tax_expense_test wtet, repair_schema rs
							 where a.work_order_id = t.work_order_id
								 and a.tax_expense_test_id = wtet.tax_expense_test_id
								 and wtet.repair_schema_id = rs.repair_schema_id
								 and lower(rs.usage_flag) in ('cwip', 'cwip_cpr'))
			 where t.new_tax_status_id is null
				 and (t.temp_tax_test_id = 0 or temp_tax_test_id is null)
				 and t.batch_id = a_batch_id;

			-- always expense
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Could not find a tax expense test for work_order: ' || t.work_order_number || '. Please review',
										 test_explanation ||
										 ',  Could not find tax expense test.  Tested Tax Status derived from "Default Status".  Test treatment set to "Always Evaluate".',
										 w.tax_status_default_capital,
										 '3'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and (t.temp_tax_test_id = 0 or temp_tax_test_id is null)
				 and t.batch_id = a_batch_id;

			-- Checking funding project for aggregation status
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (te_aggregation_id) =
						 (select te_aggregation_id
								from work_order_tax_repairs
							 where work_order_tax_repairs.work_order_id = t.funding_wo_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id;

			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET te_aggregation_id = 1
			 where t.new_tax_status_id is null
				 and (t.te_aggregation_id is null OR t.te_aggregation_id = 0)
				 and t.batch_id = a_batch_id;

			-- ested Tax Status set as FP Non Qualifying
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile,
							t.test_explanation,
							t.new_tax_status_id,
							t.total_cost,
							replacement_cost,
							repair_location_id,
							revision,
							t.tax_status_debug) =
						 (select 'Tested Tax Status set as FP Non Qualifying for work_order: ' || t.work_order_number,
										 test_explanation ||
										 ',  Tested Tax Status derived from "FP Non Qualifying" status.  Test treatment set to "Always Evaluate".',
										 w.tax_status_fp_non_qualifying,
										 t.total_cost,
										 replacement_cost,
										 repair_location_id,
										 revision,
										 4.1
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.te_aggregation_id = 2
				 and t.batch_id = a_batch_id;

			--Tested Tax Status set as FP Fully Qualifying.
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile,
							t.test_explanation,
							t.new_tax_status_id,
							t.total_cost,
							replacement_cost,
							repair_location_id,
							revision,
							t.tax_status_debug) =
						 (select 'Tested Tax Status set as FP Fully Qualifying for work_order: ' || t.work_order_number,
										 test_explanation ||
										 ',  Tested Tax Status derived from "FP Qualifying" status.  Test treatment set to "Always Evaluate".',
										 w.tax_status_fp_fully_qualifying,
										 t.total_cost,
										 replacement_cost,
										 repair_location_id,
										 revision,
										 4.2
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.te_aggregation_id = 3
				 and t.batch_id = a_batch_id;

			-- if te_aggregation_id = 1 then // Test each work order individually
			-- if te_aggregation_id = 4 then // Aggregate. proceed as normal. Aggregation occurred in uf_tax_test via related work order code.

			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.new_tax_status_id, t.tax_status_debug) =
						 (select w.tax_status_id, '5'
								from work_order_tax_status w
							 where w.work_order_id = t.work_order_id
								 and w.effective_accounting_month =
										 (select max(effective_accounting_month)
												from work_order_tax_status
											 where work_order_id = t.funding_wo_id))
			 where t.new_tax_status_id is null
				 and t.funding_wo_indicator = 0
				 and t.tax_status_from_fp = 1
				 and t.batch_id = a_batch_id;

			-- If the tax_status_from_fp is "Yes" and this is a work order,
			-- then get the value from the funding project and stop if  it exists

			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Tested Tax Status determined from the Funding Project for work_order: ' || t.work_order_number,
										 test_explanation ||
										 ',  Tested Tax Status derived from the Funding Project.  Test treatment set to "Always Evaluate".',
										 w.tax_status_default_capital,
										 '6'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where (t.new_tax_status_id is not null and t.new_tax_status_id <> 0)
				 and (t.funding_wo_indicator = 0 and t.tax_status_from_fp = 1)
				 and t.batch_id = a_batch_id;

			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET t.revision = 1
			 where t.new_tax_status_id is null
				 and t.use_actuals = 0
				 and (revision is null or revision = 0)
				 and t.batch_id = a_batch_id;

			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.revision) =
						 (select max(revision) from work_order_approval where work_order_id = t.work_order_id)
			 where t.new_tax_status_id is null
				 and t.use_actuals = 0
				 and lower(t.Est_Revision) = 'latest'
				 and t.batch_id = a_batch_id;

			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET t.revision = t.wo_current_revision
			 where t.new_tax_status_id is null
				 and t.use_actuals = 0
				 and lower(t.Est_Revision) = 'current'
				 and t.batch_id = a_batch_id;

			delete from wo_est_processing_temp;

			-- current esstimate comes from WORK_ORDER_CONTROL
			-- latest estimate comes from WORK_ORDER_APPROVAL
			--popupate for non-funding project work orders that are related
			insert into wo_est_processing_temp
				 (work_order_id, revision)
				 select DISTINCT rw.work_order_id, 0
					 from related_wos rw
					where rw.relation_id IN (select r.relation_id /* NCH 02.14.18 WAS = (select r.relation_id */
																		from related_wos r, WORK_ORDER_TAX_STATUS_PROCESS T
																	 where r.work_order_id = t.work_order_id
																		 and t.relation_id >= 0
																		 and t.funding_wo_indicator = 0)
						and work_order_id not in (select work_order_id from wo_est_processing_temp);

			--popupate for non-funding project work orders that are NOT related
			insert into wo_est_processing_temp
				 (work_order_id, revision)
				 select DISTINCT t.work_order_id, 0
					 from WORK_ORDER_TAX_STATUS_PROCESS T
					where t.relation_id is null
						and t.funding_wo_indicator = 0
						and t.batch_id = a_batch_id
						and t.work_order_id not in (select work_order_id from wo_est_processing_temp);

			UPDATE wo_est_processing_temp x
				 SET x.revision = 1000
			 WHERE x.revision = 0
				 and x.work_order_id in (select w.work_order_id
																	 from wo_estimate w, WORK_ORDER_TAX_STATUS_PROCESS t
																	where t.work_order_id = w.work_order_id
																		and w.revision = 1000
																		and t.batch_id = a_batch_id
																		and lower(trim(t.estimate_dollars_vs_units)) = 'unit-asbuilt');

			--no as-built exists, then look at the latest estimate
			UPDATE wo_est_processing_temp x
				 SET x.revision =
						 (select nvl(max(b.revision), 0)
								from work_order_approval b, WORK_ORDER_TAX_STATUS_PROCESS t
							 where b.work_order_id = x.work_order_id
								 and x.work_order_id = t.work_order_id
										--and lower(trim(t.estimate_dollars_vs_units)) = 'unit-asbuilt'
								 and lower(trim(t.est_revision)) = 'latest'
								 and t.batch_id = a_batch_id
								 and exists (select 1 from work_order_approval woa where woa.work_order_id = x.work_order_id))
			 where x.revision = 0;

			--no as-built exists, then look at the current estimate
			UPDATE wo_est_processing_temp x
				 SET x.revision =
						 (select nvl(max(wo_current_revision), 0)
								from WORK_ORDER_TAX_STATUS_PROCESS t
							 where t.work_order_id = x.work_order_id
								 and t.batch_id = a_batch_id
										--lower(trim(t.estimate_dollars_vs_units)) = 'unit-asbuilt'
								 and lower(trim(t.est_revision)) = 'current')
			 where x.revision = 0;

			--UPDATE wo_est_processing_temp x set x.revision = 1 where x.revision = 0;
			/*  -- not using as-built, look at the latest estimate
      UPDATE wo_est_processing_temp x
         SET x.revision =
             (select nvl(max(b.revision), 1)
                from work_order_approval b, WORK_ORDER_TAX_STATUS_PROCESS t
               where b.work_order_id = x.work_order_id
                 and b.work_order_id = t.work_order_id
                 and lower(trim(t.estimate_dollars_vs_units)) <> 'unit-asbuilt'
                 and lower(trim(t.est_revision)) = 'latest'
                 and exists (select 1 from work_order_approval woa where woa.work_order_id = x.work_order_id));

      -- not using as-built, look at the current estimate
      UPDATE wo_est_processing_temp x
         SET x.revision =
             (select nvl(max(wo_current_revision), 1)
                from WORK_ORDER_TAX_STATUS_PROCESS t
               where lower(trim(t.estimate_dollars_vs_units)) <> 'unit-asbuilt'
                 and lower(trim(t.est_revision)) = 'current'
                 and t.work_order_id = x.work_order_id)
       where x.revision = 0;*/

			/*where exists (select 1
       from WORK_ORDER_TAX_STATUS_PROCESS t
      where t.work_order_id = x.work_order_id
        and lower(trim(t.estimate_dollars_vs_units)) <> 'unit-asbuilt'
        and lower(trim(t.est_revision)) = 'current');*/

			--delete from wo_est_processing_temp_p;
			--insert into wo_est_processing_temp_p
			--  select * from wo_est_processing_temp;
			--commit;

      UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
         SET (replacement_flag_1, replacement_flag_2, replacement_flag_3, replacement_flag_4, replacement_flag_5) =
             (select max(replacement_flag_1) replacement_flag_1,
                     max(replacement_flag_2) replacement_flag_2,
                     max(replacement_flag_3) replacement_flag_3,
                     max(replacement_flag_4) replacement_flag_4,
                     max(replacement_flag_5) replacement_flag_5
                from (select tax_expense_test_id,
                             decode(sequence_number, 1, rp_code, null) replacement_flag_1,
                             decode(sequence_number, 2, rp_code, null) replacement_flag_2,
                             decode(sequence_number, 3, rp_code, null) replacement_flag_3,
                             decode(sequence_number, 4, rp_code, null) replacement_flag_4,
                             decode(sequence_number, 5, rp_code, null) replacement_flag_5
                        from (select rtp.tax_expense_test_id     tax_expense_test_id,
                                     rtp.repair_priority_id      repair_priority_id,
                                     rtp.repair_priority_type_id repair_priority_type_id,
                                     rtp.sequence_number         sequence_number,
                                     rp.description              rp_description,
                                     rp.repair_priority_code     rp_code,
                                     rpt.description
                                from repair_test_priority          rtp,
                                     repair_priority               rp,
                                     repair_priority_type          rpt,
                                     WORK_ORDER_TAX_STATUS_PROCESS t
                               where rtp.repair_priority_id = rp.repair_priority_id
                                 and rpt.repair_priority_type_id = rtp.repair_priority_type_id
                                 and t.batch_id = a_batch_id
                                 and rtp.repair_priority_type_id = 1 --replacement flag
                                 and rtp.tax_expense_test_id = T.TAX_EXPENSE_TEST_ID)) z
               where t.tax_expense_test_id = z.tax_expense_test_id
               group by z.tax_expense_test_id)
       where t.new_tax_status_id is null
         and t.batch_id = a_batch_id;


			-- loop through each of the five replacment priorities, in order
			FOR counter IN 1 .. 5 LOOP

				 -- use ACTUALS to get replacement flag
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (replacement_flag, replacement_source, replacement_source_debug) =
								(select decode(count(*), 0, 0, 1), 'Repl Check', 'A'
									 from cpr_ledger, cpr_activity, wo_est_processing_temp, work_order_control
									where cpr_ledger.asset_id = cpr_activity.asset_id
										and cpr_ledger.company_id = work_order_control.company_id
										and cpr_activity.work_order_number = work_order_control.work_order_number
										and cpr_activity.ferc_activity_code = 2
										and work_order_control.work_order_id = wo_est_processing_temp.work_order_id
										and t.batch_id = a_batch_id
										and work_order_control.work_order_id = t.work_order_id)
					where t.new_tax_status_id is null
						and t.replacement_flag is null
						and t.batch_id = a_batch_id
						and ((counter = 1 and t.replacement_flag_1 = 'RC') or (counter = 2 and t.replacement_flag_2 = 'RC') or
								(counter = 3 and t.replacement_flag_3 = 'RC') or (counter = 4 and t.replacement_flag_4 = 'RC') or
								(counter = 5 and t.replacement_flag_5 = 'RC'))
						and t.use_actuals = 1;

				 -- Dollar estimates - Pull from wo_est_monthly because this is where the month-by-month dollars are held.
				 -- Most clients only populate this table for funding projects.
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (replacement_flag, replacement_source, replacement_source_debug) =
								(select decode(count(*), 0, 0, 1), 'Repl Check', 'B'
									 from wo_est_monthly a, wo_est_processing_temp b
									where a.work_order_id = b.work_order_id
										and a.work_order_id = t.work_order_id
										and t.batch_id = a_batch_id
										and a.revision = b.revision
										and expenditure_type_id = 2)
					where t.new_tax_status_id is null
						and t.replacement_flag is null
						and t.batch_id = a_batch_id
						and ((counter = 1 and t.replacement_flag_1 = 'RC') or (counter = 2 and t.replacement_flag_2 = 'RC') or
								(counter = 3 and t.replacement_flag_3 = 'RC') or (counter = 4 and t.replacement_flag_4 = 'RC') or
								(counter = 5 and t.replacement_flag_5 = 'RC'))
						and lower(t.estimate_dollars_vs_units) = 'dollar'
						and t.use_actuals = 0;

				 -- Unit estimates - Pull from wo_estimate because this is where the actual units are held.
				 -- All clients use this, but a few may not put actual dollars on this table.
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (replacement_flag, replacement_source, replacement_source_debug) =
								(select decode(count(*), 0, 0, 1), 'Repl Check', 'C'
									 from wo_estimate a, wo_est_processing_temp b
									where a.work_order_id = b.work_order_id
										and t.batch_id = a_batch_id
										and a.revision = b.revision
										and expenditure_type_id = 2
										and b.work_order_id = t.work_order_id)
					where t.new_tax_status_id is null
						and t.replacement_flag is null
						and t.batch_id = a_batch_id
						and ((counter = 1 and t.replacement_flag_1 = 'RC') or (counter = 2 and t.replacement_flag_2 = 'RC') or
								(counter = 3 and t.replacement_flag_3 = 'RC') or (counter = 4 and t.replacement_flag_4 = 'RC') or
								(counter = 5 and t.replacement_flag_5 = 'RC'))
						and (lower(t.estimate_dollars_vs_units) = 'unit' or lower(t.estimate_dollars_vs_units) = 'unit-asbuilt')
						and t.use_actuals = 0;

				 -- Work Order Type - Replacement Flag
				 --NCH 06.19.18 - needs to be work_order_type_id, not work_type_id
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (replacement_flag, replacement_source, replacement_source_debug) =
								(select replacement_flag, 'WO Type', 'D'
									 from work_order_type wot
									where wot.work_order_type_id = t.work_order_type_id /*wot.work_type_id = t.work_order_type_id */
										and t.batch_id = a_batch_id)
					where t.new_tax_status_id is null
						and t.replacement_flag is null
						and t.batch_id = a_batch_id
						and ((counter = 1 and t.replacement_flag_1 = 'WOT') or (counter = 2 and t.replacement_flag_2 = 'WOT') or
								(counter = 3 and t.replacement_flag_3 = 'WOT') or (counter = 4 and t.replacement_flag_4 = 'WOT') or
								(counter = 5 and t.replacement_flag_5 = 'WOT'));


				 -- Work Flow Questions - Replacement Flag
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (replacement_flag, replacement_source, replacement_source_debug) =
								(select replacement_flag, 'Questions', 'E'
									 from work_order_tax_status W
									where w.work_order_id = t.work_order_id
										and t.batch_id = a_batch_id
										and w.effective_accounting_month = t.effective_accounting_month
										and w.replacement_source = 'Questions')
					where t.new_tax_status_id is null
						and t.replacement_flag is null
						and t.batch_id = a_batch_id
						and ((counter = 1 and t.replacement_flag_1 = 'WQ') or (counter = 2 and t.replacement_flag_2 = 'WQ') or
								(counter = 3 and t.replacement_flag_3 = 'WQ') or (counter = 4 and t.replacement_flag_4 = 'WQ') or
								(counter = 5 and t.replacement_flag_5 = 'WQ'));

				 -- User Input  - Replacement Flag
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (replacement_flag, replacement_source, replacement_source_debug) =
								(select replacement_flag, 'Manual', 'F'
									 from work_order_tax_status W
									where w.work_order_id = t.work_order_id
										and t.batch_id = a_batch_id
										and w.effective_accounting_month = t.effective_accounting_month
										and w.replacement_source = 'Manual')
					where t.new_tax_status_id is null
						and t.replacement_flag is null
						and t.batch_id = a_batch_id
						and ((counter = 1 and t.replacement_flag_1 = 'USR') or (counter = 2 and t.replacement_flag_2 = 'USR') or
								(counter = 3 and t.replacement_flag_3 = 'USR') or (counter = 4 and t.replacement_flag_4 = 'USR') or
								(counter = 5 and t.replacement_flag_5 = 'USR'));


				 -- Custom - Call the custom function if they want to be able to use it to determine the replacement flag
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (replacement_flag, replacement_source, replacement_source_debug) =
								(select replacement_flag, 'Extension', 'G'
									 from work_order_tax_status W
									where w.work_order_id = t.work_order_id
										and t.batch_id = a_batch_id
										and w.effective_accounting_month = t.effective_accounting_month
										and w.replacement_source = 'Extension')
					where t.new_tax_status_id is null
						and t.replacement_flag is null
						and t.batch_id = a_batch_id
						and ((counter = 1 and t.replacement_flag_1 = 'EXT') or (counter = 2 and t.replacement_flag_2 = 'EXT') or
								(counter = 3 and t.replacement_flag_3 = 'EXT') or (counter = 4 and t.replacement_flag_4 = 'EXT') or
								(counter = 5 and t.replacement_flag_5 = 'EXT'));
			END LOOP;


			-- Reset replacement flag and its source, if nothing was found.
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set replacement_flag = null, replacement_source = null, replacement_source_debug = 'H'
			 where t.new_tax_status_id is null
				 and t.replacement_flag is null
				 and t.batch_id = a_batch_id;


			-- Tested Tax Status set as No Replacement - replacement flag was set to NO
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Tested Tax Status set as No Replacement - replacement flag was set to NO for work order: ' ||
										 t.work_order_number || '.',
										 test_explanation ||
										 ',  Tested Tax Status derived from "No Replacement" status.  Replacement flag was set to NO.  Test treatment set to "Always Evaluate".',
										 t.tax_status_no_replacement,
										 '7'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.replacement_flag = 0;

			-- Tested Tax Status set as No Replacement - replacement flag was set to NO
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Tested Tax Status set as No Replacement - replacement flag was not found for work order: ' ||
										 t.work_order_number || '.  Please review.',
										 test_explanation ||
										 ',  Tested Tax Status derived from "No Replacement" status.  Not replacement flag was found.  Test treatment set to "Always Evaluate".',
										 t.tax_status_no_replacement,
										 '8'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.replacement_flag is null;

			-- update table to get OH/UG
			update WORK_ORDER_TAX_STATUS_PROCESS t
				 set t.oh_ug =
						 (select upper(trim(ppbase_system_options_company.option_value))
								from ppbase_system_options_company
							 where upper(trim(ppbase_system_options_company.system_option_id)) =
										 upper('Repairs - Ovh to Udgrd Rplcmt Check')
								 and company_id = t.company_id)
			 Where t.new_tax_status_id is null
				 and t.oh_ug is null
				 and t.batch_id = a_batch_id;

			update WORK_ORDER_TAX_STATUS_PROCESS t
				 set t.oh_ug =
						 (select UPPER(nvl(option_value, pp_default_value))
								from ppbase_system_options t
							 where upper(t.system_option_id) = upper('Repairs - Ovh to Udgrd Rplcmt Check'))
			 Where t.new_tax_status_id is null
				 and t.oh_ug is null
				 and t.batch_id = a_batch_id;

			--sum_estimate_rows
			update WORK_ORDER_TAX_STATUS_PROCESS t
				 set t.sum_estimate_rows =
						 (select UPPER(nvl(option_value, pp_default_value))
								from ppbase_system_options t
							 where upper(t.system_option_id) = upper('Repairs - CWIP Method - Sum Estimate Rows to Choose UOP'))
			 Where t.new_tax_status_id is null
				 and t.sum_estimate_rows is null
				 and t.batch_id = a_batch_id;

			update WORK_ORDER_TAX_STATUS_PROCESS t
				 set t.sum_estimate_rows = 'NO'
			 Where t.new_tax_status_id is null
				 and t.sum_estimate_rows is null
				 and t.batch_id = a_batch_id;

			--estimate_total_by_uop
			update WORK_ORDER_TAX_STATUS_PROCESS t
				 set t.estimate_total_by_uop =
						 (select UPPER(nvl(option_value, pp_default_value))
								from ppbase_system_options t
							 where upper(t.system_option_id) = upper('Repairs - CWIP Method - Estimate Total Qty by UOP'))
			 Where t.new_tax_status_id is null
				 and t.sum_estimate_rows is null
				 and t.batch_id = a_batch_id;

			update WORK_ORDER_TAX_STATUS_PROCESS t
				 set t.estimate_total_by_uop = 'NO'
			 Where t.new_tax_status_id is null
				 and t.estimate_total_by_uop is null
				 and t.batch_id = a_batch_id;

			--Repairs - Allow Non-Indexed Replace
			update WORK_ORDER_TAX_STATUS_PROCESS t
				 set t.hw_allow_non_index_repairs =
						 (select UPPER(nvl(option_value, pp_default_value))
								from ppbase_system_options t
							 where upper(t.system_option_id) = upper('Repairs - Allow Non-Indexed Replace'))
			 Where t.new_tax_status_id is null
				 and t.hw_allow_non_index_repairs is null
				 and t.batch_id = a_batch_id;

			update WORK_ORDER_TAX_STATUS_PROCESS t
				 set t.hw_allow_non_index_repairs = 'NO'
			 Where t.new_tax_status_id is null
				 and t.hw_allow_non_index_repairs is null
				 and t.batch_id = a_batch_id;

			--using actuals
			update WORK_ORDER_TAX_STATUS_PROCESS t
				 set t.oh_ug =
						 (select count(1)
								FROM cpr_ledger, cpr_activity, wo_est_processing_temp b, utility_account ua, work_order_control
							 WHERE cpr_ledger.asset_id = cpr_activity.asset_id
								 and cpr_ledger.company_id = work_order_control.company_id
								 and cpr_activity.ferc_activity_code = 1
								 and cpr_activity.work_order_number = work_order_control.work_order_number
								 AND cpr_ledger.utility_account_id = ua.utility_account_id
								 AND cpr_ledger.bus_segment_id = ua.bus_segment_id
								 and work_order_control.work_order_id = b.work_order_id
								 AND nvl(ua.ovh_to_udg, 0) = 1
								 and t.work_order_id = work_order_control.work_order_id
								 and t.batch_id = a_batch_id
								 AND EXISTS (SELECT 1
												from cpr_ledger cl, cpr_activity ca, utility_account ua2
											 where cl.asset_id = ca.asset_id
												 and cl.company_id = work_order_control.company_id
												 and cpr_activity.ferc_activity_code = 2
												 and ca.work_order_number = work_order_control.work_order_number
												 and cl.company_id = work_order_control.work_order_number
												 and cl.utility_account_id = ua2.utility_account_id
												 and cl.bus_segment_id = ua2.bus_segment_id
												 and nvl(ua2.ovh_to_udg, 0) = 2))
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.oh_ug = 'YES'
				 and t.use_actuals = 1;

			-- using estimates
			update WORK_ORDER_TAX_STATUS_PROCESS t
				 set t.oh_ug =
						 (select count(1)
								FROM wo_estimate a, wo_est_processing_temp b, utility_account ua
							 WHERE a.work_order_id = b.work_order_id
								 AND a.revision = b.revision
								 AND a.expenditure_type_id = 1
								 AND a.utility_account_id = ua.utility_account_id
								 AND a.bus_segment_id = ua.bus_segment_id
								 AND nvl(ua.ovh_to_udg, 0) = 1
								 and a.work_order_id = t.work_order_id
								 and t.batch_id = a_batch_id
								 AND EXISTS (SELECT 1
												from wo_estimate aa, utility_account ua2
											 where a.work_order_id = aa.work_order_id
												 and a.revision = aa.revision
												 and aa.expenditure_type_id = 2
												 and aa.utility_account_id = ua2.utility_account_id
												 and aa.bus_segment_id = ua2.bus_segment_id
												 and nvl(ua2.ovh_to_udg, 0) = 2))
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.oh_ug = 'YES'
				 and t.use_actuals = 0;

			-- Tested Tax Status set as Overhead to Underground
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Tested Tax Status set as Overhead to Underground for work order: ' || t.work_order_number || '.',
										 test_explanation ||
										 ',  Tested Tax Status derived from "Overhead to Underground" status.  Test treatment set to "Always Evaluate".',
										 t.tax_status_ovh_to_udg,
										 '9'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.oh_ug_count > 0;

			--------------------------------
			-- get list of repair unit code flags for each work order
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (repair_unit_code_1,
							repair_unit_code_2,
							repair_unit_code_3,
							repair_unit_code_4,
							repair_unit_code_5,
							repair_unit_code_6) =
						 (select max(repair_unit_code_1) repair_unit_code_1,
										 max(repair_unit_code_2) repair_unit_code_2,
										 max(repair_unit_code_3) repair_unit_code_3,
										 max(repair_unit_code_4) repair_unit_code_4,
										 max(repair_unit_code_5) repair_unit_code_5,
										 max(repair_unit_code_6) repair_unit_code_6
								from (select tax_expense_test_id,
														 decode(sequence_number, 1, rp_code, null) repair_unit_code_1,
														 decode(sequence_number, 2, rp_code, null) repair_unit_code_2,
														 decode(sequence_number, 3, rp_code, null) repair_unit_code_3,
														 decode(sequence_number, 4, rp_code, null) repair_unit_code_4,
														 decode(sequence_number, 5, rp_code, null) repair_unit_code_5,
														 decode(sequence_number, 6, rp_code, null) repair_unit_code_6
												from (select rtp.tax_expense_test_id     tax_expense_test_id,
																		 rtp.repair_priority_id      repair_priority_id,
																		 rtp.repair_priority_type_id repair_priority_type_id,
																		 rtp.sequence_number         sequence_number,
																		 rp.description              rp_description,
																		 rp.repair_priority_code     rp_code,
																		 rpt.description
																from repair_test_priority          rtp,
																		 repair_priority               rp,
																		 repair_priority_type          rpt,
																		 WORK_ORDER_TAX_STATUS_PROCESS t
															 where rtp.repair_priority_id = rp.repair_priority_id
																 and rpt.repair_priority_type_id = rtp.repair_priority_type_id
																 and rtp.repair_priority_type_id = 2 --repair unit code
																 and t.batch_id = a_batch_id
                                 and rtp.tax_expense_test_id = T.TAX_EXPENSE_TEST_ID)) z
                  where t.tax_expense_test_id = z.tax_expense_test_id
							 group by z.tax_expense_test_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id;

			-- loop through each of the six repair unti code priorities, in order
			FOR counter IN 1 .. 6 LOOP

				 -- Funding Project check - Repair unit code
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (t.repair_unit_code_id, t.repair_unit_source, repair_unit_source_debug) =
								(select repair_unit_code_id, 'Fund Proj', 'AA'
									 from work_order_tax_status
									where work_order_id = t.funding_wo_id
										and t.batch_id = a_batch_id
										and effective_accounting_month = t.effective_accounting_month)
					where t.new_tax_status_id is null
						and t.batch_id = a_batch_id
						and t.repair_unit_code_id is null
						and ((counter = 1 and t.repair_unit_code_1 = 'FP') or (counter = 2 and t.repair_unit_code_2 = 'FP') or
								(counter = 3 and t.repair_unit_code_3 = 'FP') or (counter = 4 and t.repair_unit_code_4 = 'FP') or
								(counter = 5 and t.repair_unit_code_5 = 'FP') OR (counter = 6 and t.repair_unit_code_6 = 'FP'))
						and t.funding_wo_indicator = 0;

				 -- Work Order Type - Repair Unit Code
				 --NCH 06.19.18 - needs to be work_order_type_id, not work_type_id
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (repair_unit_code_id, repair_unit_source, repair_unit_source_debug) =
								(select replacement_flag, 'WO Type', 'BB'
									 from work_order_type wot
									where wot.work_order_type_id = t.work_order_type_id /*wot.work_type_id = t.work_order_type_id */
										and t.batch_id = a_batch_id)
					where t.new_tax_status_id is null
						and t.batch_id = a_batch_id
						and t.repair_unit_code_id is null
						and ((counter = 1 and t.repair_unit_code_1 = 'WOT') or (counter = 2 and t.repair_unit_code_2 = 'WOT') or
								(counter = 3 and t.repair_unit_code_3 = 'WOT') or (counter = 4 and t.repair_unit_code_4 = 'WOT') or
								(counter = 5 and t.repair_unit_code_5 = 'WOT') or (counter = 6 and t.repair_unit_code_6 = 'WOT'));

				 -- Property Group - Repair unit code
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set ruc_func_class_count =
								(select count(*) from repair_unit_code_func_class)
					where t.new_tax_status_id is null
						and t.batch_id = a_batch_id
						and t.repair_unit_code_id is null
						and ((counter = 1 and t.repair_unit_code_1 = 'PG') or (counter = 2 and t.repair_unit_code_2 = 'PG') or
								(counter = 3 and t.repair_unit_code_3 = 'PG') or (counter = 4 and t.repair_unit_code_4 = 'PG') or
								(counter = 5 and t.repair_unit_code_5 = 'PG') or (counter = 6 and t.repair_unit_code_6 = 'PG'));

				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (repair_unit_code_id, repair_unit_source, repair_unit_source_debug) =
								(select repair_unit_code_id, 'Prop Grp', 'CC'
									 from property_group
									where property_group_id = /*This next section is all about getting the Property Group ID with the biggest dollar amount from the estimate*/
												(select property_group_id
													 from (select property_group_id
																	 from (select work_order_control.work_order_id,
																								cpr_ledger.property_group_id,
																								sum(cpr_activity.activity_cost) activity_cost
																					 from cpr_ledger,
																								cpr_activity,
																								property_group,
																								wo_est_processing_temp,
																								work_order_control
																					where cpr_ledger.asset_id = cpr_activity.asset_id
																						and cpr_ledger.company_id = work_order_control.company_id
																						and cpr_activity.ferc_activity_code = 1
																						and cpr_ledger.retirement_unit_id > 5
																						and cpr_activity.work_order_number = work_order_control.work_order_number
																						and property_group.property_group_id = cpr_ledger.property_group_id
																						and property_group.repair_unit_code_id is not null
																						and work_order_control.work_order_id = wo_est_processing_temp.work_order_id
																					group by work_order_control.work_order_id, cpr_ledger.property_group_id)
																	order by abs(activity_cost) desc, property_group_id desc)
													where rownum = 1
														and t.work_order_id = work_order_id
														and t.batch_id = a_batch_id))
					where t.new_tax_status_id is null
						and t.batch_id = a_batch_id
						and t.repair_unit_code_id is null
						and t.ruc_func_class_count = 0
						and t.use_actuals = 1
						and ((counter = 1 and t.repair_unit_code_1 = 'PG') or (counter = 2 and t.repair_unit_code_2 = 'PG') or
								(counter = 3 and t.repair_unit_code_3 = 'PG') or (counter = 4 and t.repair_unit_code_4 = 'PG') or
								(counter = 5 and t.repair_unit_code_5 = 'PG') or (counter = 6 and t.repair_unit_code_6 = 'PG'));

				 -- This gets the Repair Unit from Property Group, only when the Property Group is associated with a functional class that is on the work order, and that has mapping
				 --  on the repair_unit_code_func_class table.  This means that you won't necessarily pick up the property group with the biggest amount - instead you'll pick
				 --  up that property group with the biggest amount that corresponds the functional class on the work order.
				 --  Per Sunjin - we decided to use the functional class on the work order, instead of the ones on CPR */
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (repair_unit_code_id, repair_unit_source, repair_unit_source_debug) =
								(select repair_unit_code_id, 'Prop Grp', 'DD'
									 from property_group
									where property_group_id = /*This next section is all about getting the Property Group ID with the biggest dollar amount from the estimate*/
												(select property_group_id
													 from (select work_order_id, property_group_id
																	 from (select work_order_control.work_order_id,
																								cpr_ledger.property_group_id,
																								sum(cpr_activity.activity_cost) activity_cost
																					 from cpr_ledger,
																								cpr_activity,
																								property_group,
																								wo_est_processing_temp,
																								work_order_control
																					where cpr_ledger.asset_id = cpr_activity.asset_id
																						and cpr_ledger.company_id = work_order_control.company_id
																						and cpr_activity.ferc_activity_code = 1
																						and cpr_ledger.retirement_unit_id > 5
																						and cpr_activity.work_order_number = work_order_control.work_order_number
																						and property_group.property_group_id = cpr_ledger.property_group_id
																						and property_group.repair_unit_code_id is not null
																						and work_order_control.work_order_id = wo_est_processing_temp.work_order_id
																					group by work_order_control.work_order_id, cpr_ledger.property_group_id)
																	order by abs(activity_cost) desc, property_group_id desc)
													where rownum = 1
														and work_order_id = t.work_order_id
														and t.batch_id = a_batch_id))
					where t.new_tax_status_id is null
						and t.batch_id = a_batch_id
						and t.repair_unit_code_id is null
						and t.ruc_func_class_count > 0
						and t.use_actuals = 1
						and ((counter = 1 and t.repair_unit_code_1 = 'PG') or (counter = 2 and t.repair_unit_code_2 = 'PG') or
								(counter = 3 and t.repair_unit_code_3 = 'PG') or (counter = 4 and t.repair_unit_code_4 = 'PG') or
								(counter = 5 and t.repair_unit_code_5 = 'PG') or (counter = 6 and t.repair_unit_code_6 = 'PG'));

				 -- using estimates
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (repair_unit_code_id, repair_unit_source, repair_unit_source_debug) =
								(select repair_unit_code_id, 'Prop Grp', 'EE'
									 from property_group
									where property_group_id = /*This next section is all about getting the Property Group ID with the biggest dollar amount from the estimate*/
												(select property_group_id
													 from (select property_group_id
																	 from (select a.work_order_id, a.property_group_id, sum(amount) amount
																					 from wo_estimate a, property_group b, wo_est_processing_temp c
																					where a.work_order_id = c.work_order_id
																						and a.revision = c.revision
																						and a.expenditure_type_id = 1
																						and a.property_group_id = b.property_group_id
																						and b.repair_unit_code_id is not null
																					group by a.work_order_id, a.property_group_id)
																	order by abs(amount) desc, property_group_id desc)
													where rownum = 1
														and t.work_order_id = work_order_id
														and t.batch_id = a_batch_id))
					where t.new_tax_status_id is null
						and t.batch_id = a_batch_id
						and t.repair_unit_code_id is null
						and t.ruc_func_class_count = 0
						and t.use_actuals = 0
						and ((counter = 1 and t.repair_unit_code_1 = 'PG') or (counter = 2 and t.repair_unit_code_2 = 'PG') or
								(counter = 3 and t.repair_unit_code_3 = 'PG') or (counter = 4 and t.repair_unit_code_4 = 'PG') or
								(counter = 5 and t.repair_unit_code_5 = 'PG') or (counter = 6 and t.repair_unit_code_6 = 'PG'));

				 -- using estimates
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (repair_unit_code_id, repair_unit_source, repair_unit_source_debug) =
								(select repair_unit_code_id, 'Prop Grp', 'FF'
									 from property_group
									where property_group_id = /*This next section is all about getting the Property Group ID with the biggest dollar amount from the estimate*/
												(select property_group_id
													 from (select property_group_id
																	 from (select a.work_order_id, a.property_group_id, sum(amount) amount
																					 from wo_estimate                 a,
																								property_group              b,
																								repair_unit_code_func_class c,
																								wo_est_processing_temp      d
																					where a.work_order_id = d.work_order_id
																						and a.revision = d.revision
																						and a.property_group_id = b.property_group_id
																						and b.repair_unit_code_id = c.repair_unit_code_id
																						and c.func_class_id in
																								(select func_class_id
																									 from work_order_account a, wo_est_processing_temp b
																									where a.work_order_id = b.work_order_id)
																					group by a.work_order_id, a.property_group_id)
																	order by abs(amount) desc, property_group_id desc)
													where rownum = 1
														and t.work_order_id = work_order_id
														and t.batch_id = a_batch_id))
					where t.new_tax_status_id is null
						and t.batch_id = a_batch_id
						and t.repair_unit_code_id is null
						and t.ruc_func_class_count = 1
						and t.use_actuals = 0
						and ((counter = 1 and t.repair_unit_code_1 = 'PG') or (counter = 2 and t.repair_unit_code_2 = 'PG') or
								(counter = 3 and t.repair_unit_code_3 = 'PG') or (counter = 4 and t.repair_unit_code_4 = 'PG') or
								(counter = 5 and t.repair_unit_code_5 = 'PG') or (counter = 6 and t.repair_unit_code_6 = 'PG'));

				 -- User Input  - Replacement Flag
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (repair_unit_code_id, repair_unit_source, repair_unit_source_debug) =
								(select repair_unit_code_id, 'Manual', 'GG'
									 from work_order_tax_status W
									where w.work_order_id = t.work_order_id
										and w.effective_accounting_month = t.effective_accounting_month
										and t.batch_id = a_batch_id
										and w.repair_unit_source = 'Manual')
					where t.new_tax_status_id is null
						and t.batch_id = a_batch_id
						and t.repair_unit_code_id is null
						and ((counter = 1 and t.repair_unit_code_1 = 'USR') or (counter = 2 and t.repair_unit_code_2 = 'USR') or
								(counter = 3 and t.repair_unit_code_3 = 'USR') or (counter = 4 and t.repair_unit_code_4 = 'USR') or
								(counter = 5 and t.repair_unit_code_5 = 'USR') or (counter = 6 and t.repair_unit_code_6 = 'USR'));

				 -- Utility Account Property Unit - Repair unit code
				 -- using actuals
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (repair_unit_code_id, repair_unit_source, repair_unit_source_debug) =
								(select repair_unit_code_id, 'Util Acct Prop Unit', 'HH'
									 from (select work_order_id, repair_unit_code_id
													 from (select x.work_order_id,
																				x.repair_unit_code_id,
																				x.activity_cost,
																				ROW_NUMBER() OVER(PARTITION BY x.work_order_id ORDER BY x.activity_cost desc, x.repair_unit_code_id desc) AS row_num
																	 from (select work_order_control.work_order_id,
																								util_acct_prop_unit.repair_unit_code_id,
																								sum(cpr_activity.activity_cost) activity_cost
																					 from util_acct_prop_unit,
																								cpr_ledger,
																								cpr_activity,
																								retirement_unit,
																								wo_est_processing_temp,
																								work_order_control,
																								WORK_ORDER_TAX_STATUS_PROCESS x
																					where cpr_ledger.asset_id = cpr_activity.asset_id
																						and cpr_ledger.company_id = work_order_control.company_id
																						and cpr_activity.ferc_activity_code = 1
																						and cpr_activity.work_order_number = work_order_control.work_order_number
																						and cpr_ledger.retirement_unit_id = retirement_unit.retirement_unit_id
																						and util_acct_prop_unit.utility_account_id = cpr_ledger.utility_account_id
																						and util_acct_prop_unit.bus_segment_id = cpr_ledger.bus_segment_id
																						and util_acct_prop_unit.property_unit_id = retirement_unit.property_unit_id
																						and work_order_control.work_order_id = wo_est_processing_temp.work_order_id
																						and work_order_control.work_order_id = x.work_order_id
																						and cpr_ledger.retirement_unit_id > 5
																						and x.batch_id = a_batch_id
																					group by work_order_control.work_order_id,
																									 util_acct_prop_unit.repair_unit_code_id
																					order by abs(activity_cost) desc, util_acct_prop_unit.repair_unit_code_id desc) x,
																				WORK_ORDER_TAX_STATUS_PROCESS T
																	where x.work_order_id = t.work_order_id
																		and t.batch_id = a_batch_id)
													where row_num = 1)
									where work_order_id = t.work_order_id
										and t.batch_id = a_batch_id)
					where t.new_tax_status_id is null
						and t.batch_id = a_batch_id
						and t.use_actuals = 1
						and t.repair_unit_code_id is null
						and ((counter = 1 and t.repair_unit_code_1 = 'UAPU') or (counter = 2 and t.repair_unit_code_2 = 'UAPU') or
								(counter = 3 and t.repair_unit_code_3 = 'UAPU') or (counter = 4 and t.repair_unit_code_4 = 'UAPU') or
								(counter = 5 and t.repair_unit_code_5 = 'UAPU') or (counter = 6 and t.repair_unit_code_6 = 'UAPU'));

				 -- Utility Account Property Unit - Repair unit code
				 -- get largest DOLLAR TAX UNIT from estimates
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (repair_unit_code_id, repair_unit_source, repair_unit_source_debug) =
								(select repair_unit_code_id, 'Util Acct Prop Unit', 'II'
									 from (select work_order_id, repair_unit_code_id
													 from (select x.work_order_id,
																				x.repair_unit_code_id,
																				x.amount,
																				ROW_NUMBER() OVER(PARTITION BY x.work_order_id ORDER BY x.amount desc, x.repair_unit_code_id desc) AS row_num
																	 from (select b.work_order_id, a.repair_unit_code_id, sum(b.amount) amount
																					 from util_acct_prop_unit           a,
																								wo_estimate                   b,
																								retirement_unit               c,
																								wo_est_processing_temp        d,
																								WORK_ORDER_TAX_STATUS_PROCESS T
																					where b.work_order_id = d.work_order_id
																						and b.revision = d.revision
																						and b.expenditure_type_id = 1
																						and b.retirement_unit_id = c.retirement_unit_id
																						and a.utility_account_id = b.utility_account_id
																						and a.bus_segment_id = b.bus_segment_id
																						and a.property_unit_id = c.property_unit_id
																						and b.work_order_id = t.work_order_id
																						and t.batch_id = a_batch_id
																					group by b.work_order_id, a.repair_unit_code_id
																					order by abs(amount) desc, a.repair_unit_code_id desc) x,
																				WORK_ORDER_TAX_STATUS_PROCESS T
																	where x.work_order_id = t.work_order_id
																		and t.batch_id = a_batch_id)
													where row_num = 1)
									where work_order_id = t.work_order_id
										and t.batch_id = a_batch_id)
					where t.new_tax_status_id is null
						and t.batch_id = a_batch_id
						and t.use_actuals = 0
						and t.repair_unit_code_id is null
						and t.sum_estimate_rows = 'YES'
						and ((counter = 1 and t.repair_unit_code_1 = 'UAPU') or (counter = 2 and t.repair_unit_code_2 = 'UAPU') or
								(counter = 3 and t.repair_unit_code_3 = 'UAPU') or (counter = 4 and t.repair_unit_code_4 = 'UAPU') or
								(counter = 5 and t.repair_unit_code_5 = 'UAPU') or (counter = 6 and t.repair_unit_code_6 = 'UAPU'));

				 -- Utility Account Property Unit - Repair unit code
				 -- get largest DOLLAR ITEM from estimates
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (repair_unit_code_id, repair_unit_source, repair_unit_source_debug) =
								(select repair_unit_code_id, 'Util Acct Prop Unit', 'JJ'
									 from (select work_order_id, repair_unit_code_id
													 from (select x.work_order_id,
																				x.repair_unit_code_id,
																				x.amount,
																				ROW_NUMBER() OVER(PARTITION BY x.work_order_id ORDER BY abs(x.amount) desc) AS row_num
																	 from (select b.work_order_id, b.estimate_id, a.repair_unit_code_id, SUM(b.amount) amount
																					 from util_acct_prop_unit           a,
																								wo_estimate                   b,
																								retirement_unit               c,
																								wo_est_processing_temp        d,
																								WORK_ORDER_TAX_STATUS_PROCESS T
																					where b.work_order_id = d.work_order_id
																						and b.revision = d.revision
																						and b.expenditure_type_id = 1
																						and b.retirement_unit_id = c.retirement_unit_id
																						and a.utility_account_id = b.utility_account_id
																						and a.bus_segment_id = b.bus_segment_id
																						and a.property_unit_id = c.property_unit_id
																						and b.work_order_id = t.work_order_id
																						and t.batch_id = a_batch_id
																					group by b.work_order_id, b.estimate_id, a.repair_unit_code_id
																					order by abs(amount) desc) x,
																				WORK_ORDER_TAX_STATUS_PROCESS T
																	where x.work_order_id = t.work_order_id
																		and t.batch_id = a_batch_id)
													where row_num = 1)
									where work_order_id = t.work_order_id
										and t.batch_id = a_batch_id)
					where t.new_tax_status_id is null
						and t.batch_id = a_batch_id
						and t.use_actuals = 0
						and t.repair_unit_code_id is null
						and t.sum_estimate_rows = 'NO'
						and ((counter = 1 and t.repair_unit_code_1 = 'UAPU') or (counter = 2 and t.repair_unit_code_2 = 'UAPU') or
								(counter = 3 and t.repair_unit_code_3 = 'UAPU') or (counter = 4 and t.repair_unit_code_4 = 'UAPU') or
								(counter = 5 and t.repair_unit_code_5 = 'UAPU') or (counter = 6 and t.repair_unit_code_6 = 'UAPU'));

				 -- Custom - Call the custom function if they want to be able to use it to determine the replacement flag
				 update WORK_ORDER_TAX_STATUS_PROCESS T
						set (repair_unit_code_id, repair_unit_source, repair_unit_source_debug) =
								(select replacement_flag, 'Extension', 'KK'
									 from work_order_tax_status W
									where w.work_order_id = t.work_order_id
										and t.batch_id = a_batch_id
										and w.effective_accounting_month = t.effective_accounting_month
										and w.replacement_source = 'Extension')
					where t.new_tax_status_id is null
						and t.repair_unit_code_id is null
						and t.batch_id = a_batch_id
						and ((counter = 1 and t.replacement_flag_1 = 'EXT') or (counter = 2 and t.replacement_flag_2 = 'EXT') or
								(counter = 3 and t.replacement_flag_3 = 'EXT') or (counter = 4 and t.replacement_flag_4 = 'EXT') or
								(counter = 5 and t.replacement_flag_5 = 'EXT'));
			END LOOP;

			-- Reset replacement flag and its source, if nothing was found.
			-- make work order capital as a RUC is required for further testing
			-- set status to tax_status_no_replacement
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (repair_unit_code_id,
							repair_unit_source,
							repair_unit_source_debug,
							t.logfile,
							t.test_explanation,
							t.new_tax_status_id,
							t.tax_status_debug) =
						 (select NULL,
										 NULL,
										 'LL',
										 'Could not find a tax unit of property' || ' for work_order: ' || t.work_order_number || '.',
										 test_explanation ||
										 ',  Couid not find a tax unit of property.  Tested Tax Status derived from "Default Status" status.  Test treatment set to "Always Evaluate".',
										 t.tax_status_default_capital,
										 --t.tax_status_no_replacement,
										 '10'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and (t.repair_unit_code_id is null or t.repair_unit_code_id = 0);

			--------------------------------
			-- of_set_cost_qty()
			--------------------------------

			--- Need to limit total_qty by unit of measure
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set (unit_of_measure_id, quantity_vs_cost) =
						 (select unit_of_measure_id, quantity_vs_cost
								from repair_unit_code ruc
							 where ruc.repair_unit_code_id = t.repair_unit_code_id
								 and t.batch_id = a_batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.repair_unit_code_id is not null;

			--------------------------------------------------------------------------------
			-- Set to captital where quantity_vs_cost = 0, ineligible
			--------------------------------------------------------------------------------
			UPDATE WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Tested Tax Status set as Default Status because Repair Unit Code is "Expense Ineligible" for work order: ' ||
										 t.work_order_number || '.',
										 test_explanation ||
										 ',  Tested Tax Status set as Default Status because Repair Unit Code is "Expense Ineligible".  Test treatment set to "Always Evaluate".',
										 t.tax_status_default_capital,
										 '0'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.quantity_vs_cost = 0;

			/**** USING ACTUALS ****/
			-- tesing by QUANTITY and "Repairs - CWIP Method - Estimate Total Qty by UOP" = YES
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set (t.total_quantity, t.total_quantity_retire, total_quantity_debug) =
						 (select sum(decode(cpr_activity.ferc_activity_code,
																1,
																activity_quantity * nvl(retirement_unit.repair_qty_include, 1),
																0)),
										 sum(decode(cpr_activity.ferc_activity_code,
																2,
																activity_quantity * nvl(retirement_unit.repair_qty_include, 1),
																0)),
										 'Q1'
								from cpr_ledger,
										 cpr_activity,
										 retirement_unit,
										 wo_est_processing_temp,
										 work_order_control,
										 util_acct_prop_unit
							 where cpr_ledger.asset_id = cpr_activity.asset_id
								 and cpr_ledger.company_id = work_order_control.company_id
								 and cpr_ledger.retirement_unit_id = retirement_unit.retirement_unit_id
								 and util_acct_prop_unit.utility_account_id = cpr_ledger.utility_account_id
								 and util_acct_prop_unit.bus_segment_id = cpr_ledger.bus_segment_id
								 and util_acct_prop_unit.property_unit_id = retirement_unit.property_unit_id
								 and cpr_activity.ferc_activity_code in (1, 2)
								 and cpr_activity.work_order_number = work_order_control.work_order_number
								 and work_order_control.work_order_id = wo_est_processing_temp.work_order_id
								 and work_order_control.work_order_id = t.work_order_id
								 and util_acct_prop_unit.repair_unit_code_id = t.repair_unit_code_id
								 and t.batch_id = a_batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.repair_unit_code_id is not null
				 and t.quantity_vs_cost = 1 --quantity
				 and t.use_actuals = 1 -- using actuals
				 and t.estimate_total_by_uop = 'YES';

			/**** USING ACTUALS ****/
			-- tesing by QUANTITY and "Repairs - CWIP Method - Estimate Total Qty by UOP" = NO
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set (t.total_quantity, t.total_quantity_retire, total_quantity_debug) =
						 (select sum(decode(cpr_activity.ferc_activity_code,
																1,
																activity_quantity * nvl(retirement_unit.repair_qty_include, 1),
																0)),
										 sum(decode(cpr_activity.ferc_activity_code,
																2,
																activity_quantity * nvl(retirement_unit.repair_qty_include, 1),
																0)),
										 'Q2'
								from cpr_ledger,
										 cpr_activity,
										 retirement_unit,
										 wo_est_processing_temp,
										 work_order_control,
										 util_acct_prop_unit,
										 property_unit
							 where cpr_ledger.asset_id = cpr_activity.asset_id
								 and cpr_ledger.company_id = work_order_control.company_id
								 and cpr_ledger.retirement_unit_id = retirement_unit.retirement_unit_id
								 and util_acct_prop_unit.utility_account_id = cpr_ledger.utility_account_id
								 and util_acct_prop_unit.bus_segment_id = cpr_ledger.bus_segment_id
								 and util_acct_prop_unit.property_unit_id = retirement_unit.property_unit_id
								 and cpr_activity.ferc_activity_code in (1, 2)
								 and cpr_activity.work_order_number = work_order_control.work_order_number
								 and work_order_control.work_order_id = wo_est_processing_temp.work_order_id
								 and work_order_control.work_order_id = t.work_order_id
								 and util_acct_prop_unit.repair_unit_code_id = t.repair_unit_code_id
								 and t.batch_id = a_batch_id
								 and retirement_unit.property_unit_id = property_unit.property_unit_id
								 and nvl2(t.unit_of_measure_id, property_unit.unit_of_measure_id, -99) = nvl(t.unit_of_measure_id, -99))
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.repair_unit_code_id is not null
				 and t.quantity_vs_cost = 1 --quantity
				 and t.use_actuals = 1 -- using actuals
				 and t.estimate_total_by_uop = 'NO';

			/**** USING ACTUALS ****/
			-- tesing by COST and "Repairs - CWIP Method - Estimate Total Qty by UOP" = YES
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set (t.total_cost, t.total_cost_retire, total_cost_debug) =
						 (select sum(decode(cpr_activity.ferc_activity_code, 1, activity_cost, 0)),
										 sum(decode(cpr_activity.ferc_activity_code, 2, activity_cost, 0)),
										 'C1'
								from cpr_ledger,
										 cpr_activity,
										 retirement_unit,
										 wo_est_processing_temp,
										 work_order_control,
										 util_acct_prop_unit
							 where cpr_ledger.asset_id = cpr_activity.asset_id
								 and cpr_ledger.company_id = work_order_control.company_id
								 and cpr_ledger.retirement_unit_id = retirement_unit.retirement_unit_id
								 and util_acct_prop_unit.utility_account_id = cpr_ledger.utility_account_id
								 and util_acct_prop_unit.bus_segment_id = cpr_ledger.bus_segment_id
								 and util_acct_prop_unit.property_unit_id = retirement_unit.property_unit_id
								 and cpr_activity.ferc_activity_code in (1, 2)
								 and cpr_activity.work_order_number = work_order_control.work_order_number
								 and work_order_control.work_order_id = wo_est_processing_temp.work_order_id
								 and work_order_control.work_order_id = t.work_order_id
								 and t.batch_id = a_batch_id
								 and util_acct_prop_unit.repair_unit_code_id = t.repair_unit_code_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.repair_unit_code_id is not null
				 and t.quantity_vs_cost = 2 --COST\
				 and t.use_actuals = 1 -- using actuals
				 and t.estimate_total_by_uop = 'YES';

			/**** USING ACTUALS ****/
			-- tesing by COST and "Repairs - CWIP Method - Estimate Total Qty by UOP" = NO
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set (t.total_cost, t.total_cost_retire, total_cost_debug) =
						 (select sum(decode(cpr_activity.ferc_activity_code, 1, activity_cost, 0)),
										 sum(decode(cpr_activity.ferc_activity_code, 2, activity_cost, 0)),
										 'C2'
								from cpr_ledger, cpr_activity, wo_est_processing_temp, work_order_control
							 where cpr_ledger.asset_id = cpr_activity.asset_id
								 and cpr_ledger.company_id = work_order_control.company_id
								 and cpr_activity.work_order_number = work_order_control.work_order_number
								 and work_order_control.work_order_id = wo_est_processing_temp.work_order_id
								 and cpr_activity.ferc_activity_code in (1, 2)
								 and work_order_control.work_order_id = wo_est_processing_temp.work_order_id
								 and work_order_control.work_order_id = t.work_order_id
								 and t.batch_id = a_batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.quantity_vs_cost = 2 --COST
				 and t.use_actuals = 1 -- using actuals
				 and t.estimate_total_by_uop = 'NO';

			/***** ESTIMATES *****/
			-- tesing by COST and budget estimates

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set (t.total_cost, t.total_cost_retire, total_cost_debug) =
						 (select sum(decode(expenditure_type_id, 1, total, 0)), sum(decode(expenditure_type_id, 2, total, 0)), 'C3'
								from wo_est_monthly a, wo_est_processing_temp b
							 where a.work_order_id = b.work_order_id
								 and a.revision = b.revision
								 and expenditure_type_id in (1, 2)
								 and a.work_order_id = t.work_order_id
								 and t.batch_id = a_batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.use_actuals = 0 -- using estimates
				 and lower(trim(t.estimate_dollars_vs_units)) = 'dollar';

			/***** ESTIMATES *****/
			-- tesing by QUANTITY and "Repairs - CWIP Method - Estimate Total Qty by UOP" = YES
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set (t.total_quantity, t.total_quantity_retire, total_quantity_debug) =
						 (select sum(decode(expenditure_type_id, 1, quantity * nvl(ru.repair_qty_include, 1), 0)),
										 sum(decode(expenditure_type_id, 2, quantity * nvl(ru.repair_qty_include, 1), 0)),
										 'Q3'
								from wo_estimate a, wo_est_processing_temp b, retirement_unit ru, util_acct_prop_unit
							 where a.work_order_id = b.work_order_id
								 and a.revision = b.revision
								 and expenditure_type_id in (1, 2)
								 and a.retirement_unit_id = ru.retirement_unit_id
								 and util_acct_prop_unit.utility_account_id = a.utility_account_id
								 and util_acct_prop_unit.bus_segment_id = a.bus_segment_id
								 and util_acct_prop_unit.property_unit_id = ru.property_unit_id
								 and a.work_order_id = t.work_order_id
								 and t.batch_id = a_batch_id
								 and util_acct_prop_unit.repair_unit_code_id = t.repair_unit_code_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.quantity_vs_cost = 1 --quantity
				 and t.use_actuals = 0 -- using estimates
				 and (lower(trim(t.estimate_dollars_vs_units)) = 'unit' or
						 lower(trim(t.estimate_dollars_vs_units)) = 'unit-asbuilt')
				 and t.estimate_total_by_uop = 'YES';

			/***** ESTIMATES *****/
			-- tesing by QUANTITY and "Repairs - CWIP Method - Estimate Total Qty by UOP" = NO
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set (t.total_quantity, t.total_quantity_retire, total_quantity_debug) =
						 (select sum(decode(expenditure_type_id, 1, quantity * nvl(ru.repair_qty_include, 1), 0)),
										 sum(decode(expenditure_type_id, 2, quantity * nvl(ru.repair_qty_include, 1), 0)),
										 'Q4'
								from wo_estimate a, wo_est_processing_temp b, retirement_unit ru, property_unit pu
							 where a.work_order_id = b.work_order_id
								 and a.revision = b.revision
								 and expenditure_type_id in (1, 2)
								 and a.retirement_unit_id = ru.retirement_unit_id
								 and ru.property_unit_id = pu.property_unit_id
								 and a.work_order_id = t.work_order_id
								 and t.batch_id = a_batch_id
								 and nvl2(t.unit_of_measure_id, pu.unit_of_measure_id, -99) = nvl(t.unit_of_measure_id, -99))
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.quantity_vs_cost = 1 --quantity
				 and t.use_actuals = 0 -- using estimates
				 and (lower(trim(t.estimate_dollars_vs_units)) = 'unit' or
						 lower(trim(t.estimate_dollars_vs_units)) = 'unit-asbuilt')
				 and t.estimate_total_by_uop = 'NO';

			/***** ESTIMATES *****/
			-- tesing by COST and "Repairs - CWIP Method - Estimate Total Qty by UOP" = YES
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set (t.total_cost, t.total_cost_retire, total_cost_debug) =
						 (select sum(decode(expenditure_type_id, 1, amount, 0)),
										 sum(decode(expenditure_type_id, 2, amount, 0)),
										 'C4'
								from wo_estimate a, wo_est_processing_temp b, retirement_unit ru, util_acct_prop_unit
							 where a.work_order_id = b.work_order_id
								 and a.revision = b.revision
								 and expenditure_type_id in (1, 2)
								 and a.retirement_unit_id = ru.retirement_unit_id
								 and util_acct_prop_unit.utility_account_id = a.utility_account_id
								 and util_acct_prop_unit.bus_segment_id = a.bus_segment_id
								 and util_acct_prop_unit.property_unit_id = ru.property_unit_id
								 and a.work_order_id = t.work_order_id
								 and t.batch_id = a_batch_id
								 and util_acct_prop_unit.repair_unit_code_id = t.repair_unit_code_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.quantity_vs_cost = 2 -- cost
				 and t.use_actuals = 0 -- using estimates
				 and (lower(trim(t.estimate_dollars_vs_units)) = 'unit' or
						 lower(trim(t.estimate_dollars_vs_units)) = 'unit-asbuilt')
				 and t.estimate_total_by_uop = 'YES';

			/***** ESTIMATES *****/
			-- tesing by COST and "Repairs - CWIP Method - Estimate Total Qty by UOP" = NO
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set (t.total_cost, t.total_cost_retire, total_cost_debug) =
						 (select sum(decode(expenditure_type_id, 1, amount, 0)),
										 sum(decode(expenditure_type_id, 2, amount, 0)),
										 'C5'
								from wo_estimate a, wo_est_processing_temp b
							 where a.work_order_id = b.work_order_id
								 and a.revision = b.revision
								 and expenditure_type_id in (1, 2)
								 and a.work_order_id = t.work_order_id
								 and t.batch_id = a_batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.quantity_vs_cost = 2 -- cost
				 and t.use_actuals = 0 -- using estimates
				 and (lower(trim(t.estimate_dollars_vs_units)) = 'unit' or
						 lower(trim(t.estimate_dollars_vs_units)) = 'unit-asbuilt')
				 and t.estimate_total_by_uop = 'NO';

			--------------------------------
			-- CALCULATE ADD/RETIRE RATIO
			--------------------------------

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set add_retire_ratio = abs(total_quantity / total_quantity_retire)
			 where t.new_tax_status_id is null
				 and t.quantity_vs_cost = 1 -- quantity
				 and total_quantity_retire is not null
				 and t.total_quantity_retire <> 0
				 and t.batch_id = a_batch_id;

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set add_retire_ratio = abs(total_cost / total_cost_retire)
			 where t.new_tax_status_id is null
				 and t.quantity_vs_cost = 2 -- cost
				 and total_cost_retire is not null
				 and t.total_cost_retire <> 0
				 and t.batch_id = a_batch_id;

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set add_retire_ratio = 0
			 where t.new_tax_status_id is null
				 and t.quantity_vs_cost = 2 -- cost
				 and add_retire_ratio is null
				 and t.batch_id = a_batch_id;

			----------------------------------------------------------------
			-- CHECK IF WE ARE OVER ADD/RETIRE TOLERANCE PERCENT
			----------------------------------------------------------------

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Tested Tax Status set as Over Tolerance for work order: ' || t.work_order_number || '.',
										 test_explanation ||
										 ',  Tested Tax Status derived from "Over Tolerance" status.  Test treatment set to "Always Evaluate".',
										 t.tax_status_over_tolerance,
										 '11'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and add_retire_ratio > add_retire_tolerance_pct
				 and tax_status_over_tolerance is not null
				 and add_retire_tolerance_pct is not null;

			----------------------------------------------------------------
			-- CHECK MINOR UNITS OF PROPERTY
			----------------------------------------------------------------
			-- get major/minor counts bases on actuals
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set (t.count_minor_uop, t.count_major_uop) =
						 (select sum(decode(nvl(retirement_unit.repair_qty_include, 1), 0, 1, 0)),
										 sum(decode(nvl(retirement_unit.repair_qty_include, 1), 1, 1, 0))
								from cpr_ledger,
										 cpr_activity,
										 wo_est_processing_temp,
										 retirement_unit,
										 property_unit,
										 work_order_control
							 where cpr_ledger.asset_id = cpr_activity.asset_id
								 and cpr_ledger.company_id = work_order_control.company_id
								 and cpr_activity.ferc_activity_code = 1
								 and cpr_activity.work_order_number = work_order_control.work_order_number
								 and work_order_control.work_order_id = wo_est_processing_temp.work_order_id
								 and cpr_ledger.retirement_unit_id = retirement_unit.retirement_unit_id
								 and retirement_unit.property_unit_id = property_unit.property_unit_id
								 and nvl2(t.unit_of_measure_id, property_unit.unit_of_measure_id, -99) = nvl(t.unit_of_measure_id, -99)
								 and work_order_control.work_order_id = t.work_order_id
								 and t.batch_id = a_batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.use_actuals = 1;

			-- get major/minor counts bases on estiamtes
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set (t.count_minor_uop, t.count_major_uop) =
						 (select sum(decode(nvl(ru.repair_qty_include, 1), 0, 1, 0)),
										 sum(decode(nvl(ru.repair_qty_include, 1), 1, 1, 0))
								from wo_estimate a, wo_est_processing_temp b, retirement_unit ru, property_unit pu
							 where a.work_order_id = b.work_order_id
								 and a.revision = b.revision
								 and expenditure_type_id = 1
								 and a.retirement_unit_id = ru.retirement_unit_id
								 and ru.property_unit_id = pu.property_unit_id
								 and nvl2(t.unit_of_measure_id, pu.unit_of_measure_id, -99) = nvl(t.unit_of_measure_id, -99)
								 and a.work_order_id = t.work_order_id
								 and t.batch_id = a_batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.use_actuals = 0;

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Tested Tax Status set as Minor Unit of Property for work order: ' || t.work_order_number || '.',
										 test_explanation ||
										 ',  Tested Tax Status derived from "Minor Unit of Property status.  Test treatment set to "Always Evaluate".',
										 t.tax_status_minor_uop,
										 '12'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.quantity_vs_cost = 1
				 and (t.total_quantity is null or t.total_quantity = 0)
				 and (t.count_minor_uop > 0 and t.count_major_uop = 0);

			/*
        if count_minor_uop > 0 and count_major_uop = 0 then //We only have minor uops in play. Let's proceed to expense the work order
          new_tax_status = tax_status_minor_uop
          total_cost = 0
          replacement_cost = 0
          setnull(repair_location_id)
          setnull(revision)
          tax_status_explanation = 'Tested Tax Status set as Minor Unit of Property.'
      */

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Tested Tax Status set as Minor Unit of Property for work order: ' || t.work_order_number || '.',
										 test_explanation ||
										 ',  Tested Tax Status derived from "Minor Unit of Property" status.  Test treatment set to "Always Evaluate".',
										 t.tax_status_minor_uop,
										 '13'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.quantity_vs_cost = 1
				 and (t.total_quantity is null or t.total_quantity = 0)
				 and (t.count_minor_uop > 0 and t.count_major_uop = 0);

			----------------------------------------------------------------
			-- check for repair location
			----------------------------------------------------------------
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Could not find a repair location for work order: ' || t.work_order_number || '.',
										 test_explanation ||
										 ',  Could not find a repair location.  Tested Tax Status derived from "Default Status" status.  Test treatment set to "Always Evaluate".',
										 t.tax_status_default_capital,
										 '14'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and (t.repair_location_id is null or t.repair_location_id = 0);

			----------------------------------------------------------------
			-- Lookup replacement cost
			-- Pulling max_proj_amt to see if estimates are over the maximum
			-- allowed amount for expensing per project
			----------------------------------------------------------------

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set (replacement_cost, base_year, replacement_quantity, max_proj_amt) =
						 (select replacement_cost, base_year, replacement_quantity, max_proj_amt
								from repair_thresholds
							 where repair_unit_code_id = t.repair_unit_code_id
								 and repair_location_id = t.repair_location_id
								 and company_id = (select nvl(max(company_id), -1)
																		 from repair_thresholds
																		where company_id = t.company_id
																			and t.batch_id = a_batch_id
																			and repair_unit_code_id = t.repair_unit_code_id
																			and repair_location_id = t.repair_location_id)
								 and effective_date =
										 (select max(effective_date)
												from repair_thresholds
											 where repair_unit_code_id = t.repair_unit_code_id
												 and repair_location_id = t.repair_location_id
												 and t.batch_id = a_batch_id
												 and company_id = (select nvl(max(company_id), -1)
																						 from repair_thresholds
																						where company_id = t.company_id
																							and repair_unit_code_id = t.repair_unit_code_id
																							and t.batch_id = a_batch_id
																							and repair_location_id = t.repair_location_id)
												 and effective_date <= to_number(to_char(t.effective_accounting_month, 'yyyymm'))))
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id;

			--   NCH 09.01.2015 START (2 of 2) - For Mixed Repair Location Method, we need to try and get threshold
			--   from wotr.repair_location_id

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set t.mixed_repair_location_id =
						 (select repair_location_id
								from work_order_tax_repairs
							 where work_order_id = t.work_order_id
								 and t.batch_id = a_batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and ((replacement_cost = 0 or replacement_cost is null) and
						 (replacement_quantity = 0 or replacement_quantity is null))
				 and t.repair_location_method_id = 4;

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set (replacement_cost, base_year, replacement_quantity, max_proj_amt) =
						 (select replacement_cost, base_year, replacement_quantity, max_proj_amt
								from repair_thresholds
							 where repair_unit_code_id = t.repair_unit_code_id
								 and repair_location_id = t.repair_location_id
								 and t.batch_id = a_batch_id
								 and company_id = (select nvl(max(company_id), -1)
																		 from repair_thresholds
																		where company_id = t.company_id
																			and t.batch_id = a_batch_id
																			and repair_unit_code_id = t.repair_unit_code_id
																			and repair_location_id = t.mixed_repair_location_id)
								 and effective_date =
										 (select max(effective_date)
												from repair_thresholds
											 where repair_unit_code_id = t.repair_unit_code_id
												 and repair_location_id = t.mixed_repair_location_id
												 and t.batch_id = a_batch_id
												 and company_id = (select nvl(max(company_id), -1)
																						 from repair_thresholds
																						where company_id = t.company_id
																							and t.batch_id = a_batch_id
																							and repair_unit_code_id = t.repair_unit_code_id
																							and repair_location_id = t.mixed_repair_location_id)
												 and effective_date <= to_number(to_char(t.effective_accounting_month, 'yyyymm'))))
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and ((replacement_cost = 0 or replacement_cost is null) and
						 (replacement_quantity = 0 or replacement_quantity is null))
				 and t.repair_location_method_id = 4; --4 = mixed location method id

			----------------------------------------------------------------
			-- see if we have a replacement cost or quantity
			----------------------------------------------------------------
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Could not find a replacement cost or quantity for work order: ' || t.work_order_number || '.',
										 test_explanation ||
										 ',  Could not find a replacement cost or quantity.  Tested Tax Status derived from "Default Status" status.  Test treatment set to "Always Evaluate".',
										 t.tax_status_default_capital,
										 '15'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and (t.replacement_cost is null or t.replacement_cost = 0)
				 and (t.replacement_quantity is null and t.replacement_quantity = 0);

			----------------------------------------------------------------
			-- test to see if we are over the max amount for the project/wo
			----------------------------------------------------------------
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Tested Tax Status set as Over Max Amount for work order: ' || t.work_order_number || '.',
										 test_explanation ||
										 ',  Tested Tax Status derived from "Over Max Amount" status.  Test treatment set to "Always Evaluate".',
										 t.tax_status_over_max_amount,
										 '16'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and (max_proj_amt > 0 and total_cost > 0 and max_proj_amt <= total_cost);

			----------------------------------------------------------------
			-- Lookup handy whitman line and range test by tax uop
			----------------------------------------------------------------
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (range_test_id, hw_table_line_id) =
						 (select ruc.range_test_id, ruc.hw_table_line_id
								from repair_unit_code ruc
							 where ruc.repair_unit_code_id = t.repair_unit_code_id
								 and t.batch_id = a_batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id;

			----------------------------------------------------------------
			-- Lookup handy whitman region info
			----------------------------------------------------------------
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (hw_region_id) =
						 (select s.hw_region_id
								from asset_location al, state s
							 where al.state_id = s.state_id
								 and al.asset_location_id = t.asset_location_id
								 and t.batch_id = a_batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id;

			-- Asset Location is not a required on WOC and State Id is not required on Asset Location
			-- However Major Location is required on WOC and State Id is required on Major Location.
			-- This fix proceeds as follows
			--       1) Check if hw_region_id is null or 0
			--       2) Derive State Id from Major Location on the Work Order

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (hw_region_id) =
						 (select s.hw_region_id
								from major_location ml, state s
							 where ml.state_id = s.state_id
								 and ml.major_location_id = t.major_location_id
								 and t.batch_id = a_batch_id)
			 where t.new_tax_status_id is null
				 and (hw_region_id is null or hw_region_id = 0)
				 and t.batch_id = a_batch_id;

			----------------------------------------------------------------
			-- Lookup the handy whitman rates
			-- (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0) IS TRUE
			----------------------------------------------------------------
			-- get hw base rate
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (base_rate) =
						 (select r.rate
								from handy_whitman_rates r
							 where r.hw_region_id = t.hw_region_id
								 and r.hw_table_line_id = t.hw_table_line_id
								 and t.batch_id = a_batch_id
								 and year = t.base_year)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and (base_rate is null or base_rate = 0)
				 and (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0);

			-- get hw new rate
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (new_rate) =
						 (select r.rate
								from handy_whitman_rates r
							 where r.hw_region_id = t.hw_region_id
								 and r.hw_table_line_id = t.hw_table_line_id
								 and t.batch_id = a_batch_id
								 and year = substr(t.end_year_month, 1, 4))
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and (new_rate is null or new_rate = 0)
				 and (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0);

			-- look for new rate using max rate for region/line
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (new_rate) =
						 (select nvl(max(r.rate), 0)
								from handy_whitman_rates r
							 where r.hw_region_id = t.hw_region_id
								 and t.batch_id = a_batch_id
								 and r.hw_table_line_id = t.hw_table_line_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and (new_rate is null or new_rate = 0)
				 and (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0);

			-- use current year as new year for rate determination
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (new_rate) =
						 (select r.rate
								from handy_whitman_rates r
							 where r.hw_region_id = t.hw_region_id
								 and t.batch_id = a_batch_id
								 and r.hw_table_line_id = t.hw_table_line_id
								 and year = (select to_number(to_char(sysdate, 'yyyy')) from dual))
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and (new_rate is null or new_rate = 0)
				 and (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0);

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (new_rate) =
						 (select max(r.rate)
								from handy_whitman_rates r
							 where r.hw_region_id = t.hw_region_id
								 and t.batch_id = a_batch_id
								 and r.hw_table_line_id = t.hw_table_line_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and (new_rate is null or new_rate = 0)
				 and (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0);

			-- get hw region name
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (hw_region_descr) =
						 (select nvl(min(trim(r.description)), '<No HW region>')
								from handy_whitman_region r
							 where r.hw_region_id = t.hw_region_id
								 and t.batch_id = a_batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and hw_region_descr is null;

			-- get hw line number
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (hw_line_descr) =
						 (select nvl(min(trim(h.description)), '<No HW line>')
								from handy_whitman_index h
							 where h.hw_table_line_id = t.hw_table_line_id
								 and t.batch_id = a_batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and hw_line_descr is null;

			-- set replacement cost to zero where are not allowing "Repairs - Allow Non-Indexed Replace"
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET t.replacement_cost = 0
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and (base_rate is null or base_rate = 0 or new_rate is null or new_rate = 0)
				 AND (replacement_cost <> 0 or replacement_quantity <> 0)
				 and t.hw_allow_non_index_repairs = 'NO'
				 and (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0);

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET replacement_cost = replacement_cost * new_rate / base_rate
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and NOT (base_rate is null or base_rate = 0 or new_rate is null or new_rate = 0)
				 and (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0);

			-- Determine percentage of replacement
			-- testing with cost where replacement cost is not zero
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET est_percent = total_cost / replacement_cost
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and replacement_cost <> 0
				 and (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0)
				 and t.quantity_vs_cost = 2; -- cost

			-- testing with cost where replacement cost is zero
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET est_percent = 0
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and replacement_cost = 0
				 and (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0)
				 and t.quantity_vs_cost = 2; -- cost

			-- testing with quantity where replacement quantity is not zero
			--NCH 06.19.18 - Handy Whitman doesn't matter when testing with quantity
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET est_percent = total_quantity / replacement_quantity
			 where t.new_tax_status_id is null
				 and replacement_quantity <> 0 /* and (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0) */
				 and t.quantity_vs_cost = 1; -- quantity

			-- testing with quantity where replacement quantity is zero
			--NCH 06.19.18 - Handy Whitman doesn't matter when testing with quantity
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET est_percent = 0
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and replacement_quantity = 0 /* and (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0) */
				 and t.quantity_vs_cost = 1; -- quantity

			----------------------------------------------------------------
			-- HW Values are indeterminate - use existing values
			-- (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0) IS NOT TRUE
			----------------------------------------------------------------
			-- testing with cost where replacement cost is not zero
			-- hw_allow_non_index_repairs = 'YES'
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET est_percent = total_cost / replacement_cost
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and replacement_cost <> 0
				 and NOT (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0)
				 and hw_allow_non_index_repairs = 'YES'
				 and t.quantity_vs_cost = 2; -- cost

			-- testing with cost where replacement cost is zero
			-- hw_allow_non_index_repairs = 'YES'
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET est_percent = 0
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and replacement_cost = 0
				 and NOT (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0)
				 and hw_allow_non_index_repairs = 'YES'
				 and t.quantity_vs_cost = 2; -- cost

			--NCH 06.19.18 - Handy Whitman doesn't matter when testing with quantity - this is redundant
			/*
			-- testing with quantity where replacement quantity is not zero
			-- hw_allow_non_index_repairs = 'YES'
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET est_percent = total_quantity / replacement_quantity
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and replacement_quantity <> 0
				 and NOT (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0)
				 and hw_allow_non_index_repairs = 'YES'
				 and t.quantity_vs_cost = 1; -- quantity

			-- testing with quantity where replacement quantity is zero
			-- hw_allow_non_index_repairs = 'YES'
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET est_percent = 0
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and replacement_quantity = 0
				 and NOT (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0)
				 and hw_allow_non_index_repairs = 'YES'
				 and t.quantity_vs_cost = 1; -- quantity

			-- testing with quantity where replacement quantity is zero
			-- hw_allow_non_index_repairs = 'NO'
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET est_percent = total_quantity / replacement_quantity
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and replacement_quantity = 0
				 and NOT (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0)
				 and hw_allow_non_index_repairs = 'NO'
				 and t.quantity_vs_cost = 1; -- quantity

			-- testing with quantity where replacement quantity is zero
			-- hw_allow_non_index_repairs = 'YES'
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET est_percent = 0
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and replacement_quantity = 0
				 and NOT (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0)
				 and hw_allow_non_index_repairs = 'NO'
				 and t.quantity_vs_cost = 1; -- quantity
			*/
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.est_percent, t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 0,
										 'HW Index Error: Make sure Base Year exists in Threshold Config and HW Line/Region are set up for work order: ' ||
										 t.work_order_number || '.',
										 test_explanation ||
										 ',  HW Index Error: Make sure Base Year exists in Threshold Config and HW Line/Region are set up.  Tested Tax Status derived from "Default Status" status.  Test treatment set to "Always Evaluate".',
										 t.tax_status_default_capital,
										 '17'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and NOT (hw_table_line_id <> 0 and hw_region_id <> 0 and base_year <> 0)
				 and hw_allow_non_index_repairs = 'NO'
				 and t.quantity_vs_cost = 2; -- cost

			-- set percenage to one when over one
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET est_percent = 1
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and est_percent > 1;

			----------------------------------------------------------------
			-- Determine tax_status_id from range test table
			----------------------------------------------------------------
			-- It does not make sense to expense these...Perhaps no replacement cost was found
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Replacment Percentage iz Zero for work order: ' || t.work_order_number || '.',
										 test_explanation ||
										 ',  Replacement percentage is zero.  Tested Tax Status derived from "Default Status" status.  Test treatment set to "Always Evaluate".',
										 t.tax_status_default_capital,
										 '18'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.est_percent = 0
				 and range_test_id is not null
				 and range_test_id <> 0;

			-- test status from range
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'Tested Tax Status retrieved from Range Test for work order: ' || t.work_order_number || '.',
										 test_explanation ||
										 ',  Tested Tax Status derived from "Range Test".  Test treatment set to "Always Evaluate".',
										 rrt.tax_status_id,
										 '19'
								from repair_range_test rrt
							 where range_test_id = t.range_test_id
								 and t.est_percent > rrt.low_Range
								 and t.est_percent <= rrt.high_range
								 and t.batch_id = a_batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.est_percent <> 0
				 and range_test_id is not null
				 and range_test_id <> 0;

			-- No range test is assigned to repair unit code id
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'No range test is assigned to repair unit code id: (null) for work order: ' || t.work_order_number || '.',
										 test_explanation || ',  No range test is assigned to repair unit code id: (null)' ||
										 '.  Tested Tax Status derived from "Default Status" status.  Test treatment set to "Always Evaluate".',
										 w.tax_status_default_capital,
										 '20'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.WORK_ORDER_ID = T.WORK_ORDER_ID
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.repair_unit_code_id is null
				 and (range_test_id is null or range_test_id = 0);

			-- No range test is assigned to repair unit code id
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'No range test is assigned to repair unit code id: ' || to_char(w.repair_unit_code_id) ||
										 ' for work order: ' || w.work_order_number || '.',
										 test_explanation || ',  No range test is assigned to repair unit code id: ' ||
										 to_char(w.repair_unit_code_id) ||
										 '.  Tested Tax Status derived from "Default Status" status.  Test treatment set to "Always Evaluate".',
										 w.tax_status_default_capital,
										 '21'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.WORK_ORDER_ID = t.WORK_ORDER_ID
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and t.repair_unit_code_id is not null
				 and (range_test_id is null or range_test_id = 0);

			----------------------------------------------------------------
			----------------------------------------------------------------
			-- Messages to explain why HW cannot be used
			----------------------------------
			------------------------------
			----------------------------------------------------------------
			-- hw line number (hw_table_line_id)
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'A Handy Whitman Line Number cannot be determined for work order: ' || t.work_order_number || '.',
										 test_explanation || ',  A Handy Whitman Line Number cannot be determined' ||
										 '.  Tested Tax Status derived from "Default Status" status.  Test treatment set to "Always Evaluate".',
										 t.tax_status_default_capital,
										 '22'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and (hw_table_line_id is null or hw_table_line_id = 0);

			-- hw_region_id
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'A Handy Whitman Region cannot be determined for work order: ' || t.work_order_number || '.',
										 test_explanation || ',  A Handy Whitman Region cannot be determined' ||
										 '.  Tested Tax Status derived from "Default Status" status.  Test treatment set to "Always Evaluate".',
										 t.tax_status_default_capital,
										 '23'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and (hw_region_id is null or hw_region_id = 0);

			-- base year
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'A Handy Whitman Base Year cannot be determined for work order: ' || t.work_order_number || '.',
										 test_explanation || ',  A Handy Whitman Base Year cannot be determined' ||
										 '.  Tested Tax Status derived from "Default Status" status.  Test treatment set to "Always Evaluate".',
										 t.tax_status_default_capital,
										 '24'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and (base_year is null or base_year = 0);

			-- new hw rate
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.new_tax_status_id, t.tax_status_debug) =
						 (select 'No HW Rates found for the current year. Using the MAX HW Rate for Region: ' || hw_region_descr ||
										 ' - Line ID: ' || t.hw_line_descr || ' for work_order: ' || t.work_order_number || '.',
										 test_explanation || ',  No HW Rates found for the current year. Using the MAX HW Rate for Region: ' ||
										 hw_region_descr || ' - Line ID: ' || t.hw_line_descr ||
										 '.  Tested Tax Status derived from "Default Status" status.  Test treatment set to "Always Evaluate".',
										 t.tax_status_default_capital,
										 '25'
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.new_tax_status_id is null
				 and t.batch_id = a_batch_id
				 and (new_rate is null or new_rate = 0);

			----------------------------------------------------------------
			-- A 'new' tax status was found
			----------------------------------------------------------------
			-- get priority and description for new tax status
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (new_tax_status_priority, new_tax_status_desc) =
						 (select nvl(max(s.priority), 999), nvl(max(s.description), 'Tax Status Not Found')
								from wo_tax_status s
							 where tax_status_id = t.new_tax_status_id
								 and t.batch_id = a_batch_id)
			 where t.new_tax_status_id is not null
				 and t.batch_id = a_batch_id
				 and t.new_tax_status_id <> 0;

			-- get priority and description for old tax status
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (tax_status_priority, tax_status_desc) =
						 (select nvl(max(s.priority), 999), nvl(max(s.description), 'Tax Status Not Found')
								from wo_tax_status s
							 where tax_status_id = t.tax_status_id
								 and t.batch_id = a_batch_id)
			 where t.tax_status_id is not null
				 and t.batch_id = a_batch_id
				 and t.tax_status_id <> 0;

			--If test changed from a UOP test to a cwip test, make sure that we allow the status to change.
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET tax_status_priority = 999
			 where t.tax_status_id = -999
				 and upper(t.usage_flag) = 'CWIP'
				 and t.new_tax_status_id is not null
				 and t.new_tax_status_id <> 0
				 and t.batch_id = a_batch_id;

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET tax_status_priority = -999
			 where t.tax_status_id = -999
				 and upper(t.usage_flag) <> 'CWIP'
				 and t.new_tax_status_id is not null
				 and t.new_tax_status_id <> 0
				 and t.batch_id = a_batch_id;

      update WORK_ORDER_TAX_STATUS_PROCESS T
         SET tax_status_priority = 999
       where t.tax_status_id = -998
         and upper(t.usage_flag) = 'CWIP'
         and t.new_tax_status_id is not null
         and t.new_tax_status_id <> 0
         and t.batch_id = a_batch_id;

      update WORK_ORDER_TAX_STATUS_PROCESS T
         SET tax_status_priority = -999
       where t.tax_status_id = -998
         and upper(t.usage_flag) <> 'CWIP'
         and t.new_tax_status_id is not null
         and t.new_tax_status_id <> 0
         and t.batch_id = a_batch_id;
			/*insert into work_order_tax_status
            (work_order_id,
             effective_accounting_month,
             repair_unit_code_id,
             replacement_flag,
             repair_unit_source,
             replacement_source,
             REPAIR_LOC_ROLLUP_ID)
            select woc.work_order_id,
                   to_date(current_open_month_number, 'yyyymm'),
                   wot.repair_unit_code_id,
                   wot.replacement_flag,
                   decode(wot.repair_unit_code_id, null, null, 'WO Type'),
                   decode(wot.replacement_flag, null, null, 'WO Type'),
                   -1
              from work_order_control woc, work_order_type wot, WORK_ORDER_TAX_STATUS_PROCESS w
             where woc.work_order_type_id = wot.work_order_type_id
               and woc.work_order_id = w.work_order_id
               and woc.funding_wo_id = w.funding_wo_id
               and w.batch_id = a_batch_id
               and (w.reviewed_flag <> 1 and w.external_reviewed_flag <> 1)
               and w.new_tax_status_id is not null
               and w.new_tax_status_id <> 0
               and exists
             (select 1
                      from (select w.work_order_id,
                                   to_date(w.current_open_month_number, 'yyyymm') effective_accounting_month,
                                   w.batch_id
                              from work_order_tax_status_process w
                             where w.effective_accounting_month < to_date(w.current_open_month_number, 'yyyymm')) x
                     where x.work_order_id = w.work_order_id
                       and x.batch_id = w.batch_id
                       and x.effective_accounting_month <> w.effective_accounting_month)
               and (w.work_order_id, to_date(w.current_open_month_number, 'yyyymm')) not in
                   (select a.work_order_id, max(a.effective_accounting_month) effective_accounting_month
                      from work_order_tax_status a
                     where a.work_order_id = w.work_order_id
                     group by a.work_order_id);
      */
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.final_tax_status_id) =
						 (select 'This Tax Status has been internally reviewed for a future month and cannot be changed for work_order: ' ||
										 t.work_order_number || '.',
										 test_explanation ||
										 ', This Tax Status has been internally reviewed for a future month and cannot be changed',
										 t.tax_status_id
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.reviewed_flag = 1
				 and t.batch_id = a_batch_id
				 and t.final_tax_status_id is null
				 and t.effective_accounting_month < to_date(t.current_open_month_number, 'yyyymm');

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.final_tax_status_id) =
						 (select 'This Tax Status has been externally reviewed for a future month and cannot be changed for work_order: ' ||
										 t.work_order_number || '.',
										 test_explanation ||
										 ',  This Tax Status has been externally reviewed for a future month and cannot be changed',
										 t.tax_status_id
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.external_reviewed_flag = 1
				 and t.batch_id = a_batch_id
				 and t.final_tax_status_id is null
				 and t.effective_accounting_month < to_date(t.current_open_month_number, 'yyyymm');

			--------------------------------------------------------------------------------------------------------------

			----------------------------------------------------------------
			-- A 'new' tax status was NOT found
			-- We could not get a new tax status, so we will set it to be
			-- the default Capital status if the priority permits
			----------------------------------------------------------------
			-- get priority and description for default capital tax status
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (new_tax_status_priority, new_tax_status_desc) =
						 (select nvl(max(s.priority), 999), nvl(max(s.description), 'Tax Status Not Found')
								from wo_tax_status s
							 where tax_status_id = t.tax_status_default_capital
								 and t.batch_id = a_batch_id)
			 where (t.new_tax_status_id is null OR t.new_tax_status_id = 0)
				 and t.batch_id = a_batch_id;

			-- get priority and description for old tax status
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (tax_status_priority, tax_status_desc) =
						 (select nvl(max(s.priority), 999), nvl(max(s.description), 'Tax Status Not Found')
								from wo_tax_status s
							 where tax_status_id = t.tax_status_id
								 and t.batch_id = a_batch_id)
			 where (t.new_tax_status_id is null OR t.new_tax_status_id = 0)
				 and t.batch_id = a_batch_id;

			--If test changed from a UOP test to a cwip test, make sure that we allow the status to change.
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET tax_status_priority = 999
			 where t.tax_status_id = -999
				 and upper(t.usage_flag) = 'CWIP'
				 and (t.new_tax_status_id is null OR t.new_tax_status_id = 0)
				 and t.batch_id = a_batch_id;

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET tax_status_priority = -999
			 where t.tax_status_id = -999
				 and upper(t.usage_flag) <> 'CWIP'
				 and (t.new_tax_status_id is null OR t.new_tax_status_id = 0)
				 and t.batch_id = a_batch_id;

      update WORK_ORDER_TAX_STATUS_PROCESS T
         SET tax_status_priority = 999
       where t.tax_status_id = -998
         and upper(t.usage_flag) = 'CWIP'
         and (t.new_tax_status_id is null OR t.new_tax_status_id = 0)
         and t.batch_id = a_batch_id;

      update WORK_ORDER_TAX_STATUS_PROCESS T
         SET tax_status_priority = -999
       where t.tax_status_id = -998
         and upper(t.usage_flag) <> 'CWIP'
         and (t.new_tax_status_id is null OR t.new_tax_status_id = 0)
         and t.batch_id = a_batch_id;

			/*
      insert into work_order_tax_status
         (work_order_id,
          effective_accounting_month,
          repair_unit_code_id,
          replacement_flag,
          repair_unit_source,
          replacement_source,
          REPAIR_LOC_ROLLUP_ID)
         select woc.work_order_id,
                to_date(current_open_month_number, 'yyyymm'),
                wot.repair_unit_code_id,
                wot.replacement_flag,
                decode(wot.repair_unit_code_id, null, null, 'WO Type'),
                decode(wot.replacement_flag, null, null, 'WO Type'),
                -2
           from work_order_control woc, work_order_type wot, WORK_ORDER_TAX_STATUS_PROCESS T
          where woc.work_order_type_id = wot.work_order_type_id
            and woc.work_order_id = t.work_order_id
            and woc.funding_wo_id = t.funding_wo_id
            and t.batch_id = a_batch_id
            and (t.reviewed_flag <> 1 and t.external_reviewed_flag <> 1)
            and (t.new_tax_status_id is null OR t.new_tax_status_id = 0)
            and exists
          (select 1
                   from (select t.work_order_id,
                                to_date(t.current_open_month_number, 'yyyymm') effective_accounting_month,
                                t.batch_id
                           from work_order_tax_status_process t
                          where t.effective_accounting_month < to_date(t.current_open_month_number, 'yyyymm')) w
                  where w.work_order_id = t.work_order_id
                    and t.batch_id = w.batch_id
                    and w.effective_accounting_month <> t.effective_accounting_month)
            and (t.work_order_id, to_date(t.current_open_month_number, 'yyyymm')) not in
                (select a.work_order_id, max(a.effective_accounting_month) effective_accounting_month
                   from work_order_tax_status a
                  where a.work_order_id = t.work_order_id
                    and t.batch_id = a_batch_id
                  group by a.work_order_id);*/

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.final_tax_status_id) =
						 (select 'This Tax Status has been internally reviewed for a future month and cannot be changed for work_order: ' ||
										 t.work_order_number || '.',
										 test_explanation ||
										 ',  This Tax Status has been internally reviewed for a future month and cannot be changed',
										 t.tax_status_id
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.reviewed_flag = 1
				 and t.batch_id = a_batch_id
				 and t.final_tax_status_id is null
				 and t.effective_accounting_month < to_date(t.current_open_month_number, 'yyyymm');

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET (t.logfile, t.test_explanation, t.final_tax_status_id) =
						 (select 'This Tax Status has been externally reviewed for a future month and cannot be changed for work_order: ' ||
										 t.work_order_number || '.',
										 test_explanation ||
										 ',  This Tax Status has been externally reviewed for a future month and cannot be changed',
										 t.tax_status_id
								from WORK_ORDER_TAX_STATUS_PROCESS w
							 where w.work_order_id = t.work_order_id
								 and t.batch_id = w.batch_id)
			 where t.external_reviewed_flag = 1
				 and t.batch_id = a_batch_id
				 and t.final_tax_status_id is null
				 and t.effective_accounting_month < to_date(t.current_open_month_number, 'yyyymm');

			--  new_priority <= priority and new_tax_status <> tax_status_id
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET t.final_tax_status_id = t.new_tax_status_id
			 where t.new_tax_status_id is not null
				 and t.batch_id = a_batch_id
				 and t.new_tax_status_id <> 0
				 AND t.final_tax_status_id is null
				 and t.new_tax_status_priority <= t.tax_status_priority
				 and t.new_tax_status_id <> t.tax_status_id;

			--  new_priority > priority and new_tax_status <> tax_status_id
			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET t.final_tax_status_id = t.tax_status_id
			 where t.new_tax_status_id is not null
				 and t.batch_id = a_batch_id
				 and t.new_tax_status_id <> 0
				 AND t.final_tax_status_id is null
				 and t.new_tax_status_priority > t.tax_status_priority
				 and t.new_tax_status_id <> t.tax_status_id;

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET t.final_tax_status_id = t.new_tax_status_id
			 where (t.new_tax_status_id is not null and t.new_tax_status_id <> 0)
				 and t.final_tax_status_id is null
				 and t.batch_id = a_batch_id;

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET t.final_tax_status_id = t.tax_status_id
			 where (t.new_tax_status_id = T.TAX_STATUS_ID)
				 and t.final_tax_status_id is null
				 and t.batch_id = a_batch_id;

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 SET t.test_tax_status_id = decode(t.new_tax_status_id, 0, null, t.new_tax_status_id)
			 where t.batch_id = a_batch_id;

			update WORK_ORDER_TAX_STATUS_PROCESS T
				 set t.test_explanation = substr(t.test_explanation, 4)
			 where substr(t.test_explanation, 1, 3) = ',  '
				 and t.batch_id = a_batch_id;

			update work_order_tax_status_process t
				 set estimated_costs   = total_cost,
						 test_replace_cost = replacement_cost,
						 estimated_qty     = total_quantity,
						 test_replace_qty  = replacement_quantity
			 where t.batch_id = a_batch_id;

			update work_order_tax_status_process t
				 set t.logfile =
						 (select 'Work order ' || t.work_order_number || ' / ' || c.DESCRIPTION || ' evaluated using ' ||
										 decode(t.use_actuals, 1, 'ACTUALS', 'ESTIMATES') || ' for tax status: "' || s.description ||
										 '".   (' || t.test_explanation || ')'
								from wo_tax_status s, company c
							 where t.final_tax_status_id = s.tax_status_id
								 and t.company_id = c.COMPANY_ID)
			 where t.batch_id = a_batch_id;

			delete from WORK_ORDER_TAX_STATUS_PROC_ARC where batch_id = a_batch_id;

			insert into WORK_ORDER_TAX_STATUS_PROC_ARC
				 select * from WORK_ORDER_TAX_STATUS_PROCESS where batch_id = a_batch_id;

			/*   update work_order_tax_status w
            set (tax_status_id,
                 replacement_flag,
                 repair_unit_code_id,
                 repair_location_id,
                 estimated_costs,
                 test_replace_cost,
                 est_revision,
                 test_date,
                 test_tax_status_id,
                 reviewed_date,
                 review_user,
                 external_reviewed_date,
                 external_review_user,
                 override_note,
                 processing_month,
                 replacement_source,
                 repair_unit_source,
                 estimated_qty,
                 test_replace_qty,
                 fp_percent_expense,
                 repair_loc_rollup_id,
                 test_explanation,
                 tested_actuals) =
                (select t.final_tax_status_id as tax_status_id,
                        t.replacement_flag,
                        t.repair_unit_code_id,
                        t.repair_location_id,
                        t.total_cost as estimated_costs,
                        t.replacement_cost as test_replace_cost,
                        t.wo_current_revision as est_revision,
                        to_date(to_char(sysdate, 'yyyymmdd'), 'yyyymmdd') as test_date,
                        t.new_tax_status_id as test_tax_status_id,
                        t.reviewed_date,
                        t.review_user,
                        t.external_reviewed_date,
                        t.external_review_user,
                        null as override_note,
                        t.current_open_month_number,
                        t.replacement_source,
                        t.repair_unit_source,
                        t.estimated_qty,
                        t.test_replace_qty,
                        t.fp_percent_expense,
                        null as repair_loc_rollup_id,
                        t.test_explanation,
                        t.tested_actuals
                   from WORK_ORDER_TAX_STATUS_PROCESS T
                  where t.work_order_id = w.work_order_id
                    AND T.BATCH_ID = a_batch_id
                    AND (t.reviewed_flag <> 1 and t.external_reviewed_flag <> 1)
                    and to_date(current_open_month_number, 'yyyymm') = w.effective_accounting_month)
          where (w.work_order_id, w.effective_accounting_month) in
                (select x.work_order_id, to_date(current_open_month_number, 'yyyymm')
                   from WORK_ORDER_TAX_STATUS_PROCESS x
                  where x.batch_id = a_batch_id
                    AND (x.reviewed_flag <> 1 and x.external_reviewed_flag <> 1));
      */
			MERGE into work_order_tax_status mytarget
			using (select t.work_order_id,
										t.effective_accounting_month,
										t.final_tax_status_id as tax_status_id,
										t.replacement_flag,
										t.repair_unit_code_id,
										t.repair_location_id,
										t.total_cost as estimated_costs,
										t.replacement_cost as test_replace_cost,
										t.wo_current_revision as est_revision,
										to_date(to_char(sysdate, 'yyyymmdd'), 'yyyymmdd') as test_date,
										t.new_tax_status_id as test_tax_status_id,
										t.reviewed_date,
										t.review_user,
										t.external_reviewed_date,
										t.external_review_user,
										null as override_note,
										t.current_open_month_number,
										t.replacement_source,
										t.repair_unit_source,
										t.estimated_qty,
										t.test_replace_qty,
										t.fp_percent_expense,
										null as repair_loc_rollup_id,
										t.test_explanation,
										t.tested_actuals
							 from WORK_ORDER_TAX_STATUS_PROCESS T
							where BATCH_ID = a_batch_id
								AND (reviewed_flag <> 1 and external_reviewed_flag <> 1)) mySource
			ON (myTarget.work_order_id = mysource.work_order_id AND myTarget.effective_accounting_month = to_date(mysource.current_open_month_number, 'yyyymm'))
			WHEN MATCHED THEN
				 UPDATE
						SET tax_status_id          = mySource.tax_status_id,
								replacement_flag       = mySource.replacement_flag,
								repair_unit_code_id    = mySource.repair_unit_code_id,
								repair_location_id     = mySource.repair_location_id,
								estimated_costs        = mySource.estimated_costs,
								test_replace_cost      = mySource.test_replace_cost,
								est_revision           = mySource.est_revision,
								test_date              = mySource.test_date,
								test_tax_status_id     = mySource.test_tax_status_id,
								reviewed_date          = mySource.reviewed_date,
								review_user            = mySource.review_user,
								external_reviewed_date = mySource.external_reviewed_date,
								external_review_user   = mySource.external_review_user,
								override_note          = mySource.override_note,
								processing_month       = mySource.current_open_month_number,
								replacement_source     = mySource.replacement_source,
								repair_unit_source     = mySource.repair_unit_source,
								estimated_qty          = mySource.estimated_qty,
								test_replace_qty       = mySource.test_replace_qty,
								fp_percent_expense     = mySource.fp_percent_expense,
								repair_loc_rollup_id   = mySource.repair_loc_rollup_id,
								test_explanation       = SubStr(mySource.test_explanation,1,254),
								tested_actuals         = mySource.tested_actuals
			WHEN NOT MATCHED THEN
				 INSERT
						(work_order_id,
						 effective_accounting_month,
						 tax_status_id,
						 replacement_flag,
						 repair_unit_code_id,
						 repair_location_id,
						 estimated_costs,
						 test_replace_cost,
						 est_revision,
						 test_date,
						 test_tax_status_id,
						 reviewed_date,
						 review_user,
						 external_reviewed_date,
						 external_review_user,
						 override_note,
						 processing_month,
						 replacement_source,
						 repair_unit_source,
						 estimated_qty,
						 test_replace_qty,
						 fp_percent_expense,
						 repair_loc_rollup_id,
						 test_explanation,
						 tested_actuals)
				 VALUES
						(mysource.work_order_id,
						 to_date(mysource.current_open_month_number, 'yyyymm'),
						 mysource.tax_status_id,
						 mysource.replacement_flag,
						 mysource.repair_unit_code_id,
						 mysource.repair_location_id,
						 mysource.estimated_costs,
						 mysource.test_replace_cost,
						 mysource.est_revision,
						 mysource.test_date,
						 mysource.test_tax_status_id,
						 mysource.reviewed_date,
						 mysource.review_user,
						 mysource.external_reviewed_date,
						 mysource.external_review_user,
						 mysource.override_note,
						 mysource.current_open_month_number,
						 mysource.replacement_source,
						 mysource.repair_unit_source,
						 mysource.estimated_qty,
						 mysource.test_replace_qty,
						 mysource.fp_percent_expense,
						 mysource.repair_loc_rollup_id,
						 SubStr(mysource.test_explanation,1,254),
						 mysource.tested_actuals);

			delete from WORK_ORDER_TAX_STATUS_PROCESS where batch_id = a_batch_id;

			commit;

	 end p_batch_process_status;

end RPR_WO_STATUS_PKG;
/

GRANT EXECUTE ON rpr_wo_status_pkg TO public;
GRANT EXECUTE ON rpr_wo_status_pkg TO pwrplant_role_dev;