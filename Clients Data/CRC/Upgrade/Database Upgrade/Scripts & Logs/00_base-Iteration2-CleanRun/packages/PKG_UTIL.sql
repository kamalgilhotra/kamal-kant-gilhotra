/*
||============================================================================
|| Application: PowerPlan
|| Object Name: PKG_UTIL.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| PP Version  Version    Date           Revised By     Reason for Change
|| ----------  ---------  -------------  -------------  -------------------------
|| 2015.1      1.0        11/5/2014      Luke Warren    Transitioning PKG_UTIL from SAP Adapter
||                                                      to the PowerPlan project so everyone can
||                                                      benefit from it.
||============================================================================
*/

-- Create utility package
create or replace package PKG_UTIL is

   TABLE_NOT_EXISTS exception;
   pragma exception_init(TABLE_NOT_EXISTS, -00942);
   SYNONYM_TRANSLATION_INVALID exception;
   pragma exception_init(SYNONYM_TRANSLATION_INVALID, -00980);
   LOOPING_CHAIN_OF_SYNONYMS exception;
   pragma exception_init(LOOPING_CHAIN_OF_SYNONYMS, -01775);
   INSUFFICIENT_PRIVILEGE exception;
   pragma exception_init(INSUFFICIENT_PRIVILEGE, -01031);
   SYNONYM_NOT_EXIST exception;
   pragma exception_init(SYNONYM_NOT_EXIST, -01432);
   INVALID_IDENTIFIER exception;
   pragma exception_init(INVALID_IDENTIFIER, -00904);
   NAME_ALREADY_USED exception;
   pragma exception_init(NAME_ALREADY_USED, -00955);
   INDEX_NOT_EXISTS exception;
   pragma exception_init(INDEX_NOT_EXISTS, -01418);
   COLUMN_EXISTS exception;
   pragma exception_init(COLUMN_EXISTS, -01430);

   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';
   PPCORA varchar2(10) := 'PPC-ORA' || '-> ';

   /*****************************************************************
   Function: TABLE_EXISTS
   Returns: 1 if table exists
            0 if table does not exist
            Raises application error on any other condition.
   *****************************************************************/
   function TABLE_EXISTS(TABLE_NAME  varchar2,
                         SCHEMA_NAME varchar2 default 'PWRPLANT') return number;

   /*****************************************************************
   Function: DROP_TABLE
   Returns: 1 if table is dropped
            0 if table does not exist
            Raises application error on any other condition.
   *****************************************************************/
   function DROP_TABLE(TABLE_NAME  varchar2,
                       SCHEMA_NAME varchar2 default 'PWRPLANT') return number;

   /*****************************************************************
   Function: DROP_COLUMN
   Returns: 1 if column is dropped
            0 if column does not exist
            Raises application error on any other condition.
   *****************************************************************/
   function DROP_COLUMN(TABLE_NAME  varchar2,
                        COLUMN_NAME varchar2,
                        SCHEMA_NAME varchar2 default 'PWRPLANT') return number;

   /*****************************************************************
   Function: DROP_FK
   Returns: 1 if FK is dropped
            0 if FK does not exist
            Raises application error on any other condition.
   *****************************************************************/
   function DROP_FK(FK_TABLE_NAME         varchar2,
                    REFERENCED_TABLE_NAME varchar2,
                    FK_COLUMN_NAME        varchar2 default '%',
                    SCHEMA_NAME           varchar2 default 'PWRPLANT') return number;

   /*****************************************************************
   Function: DROP_SYNONYM
   Returns: 1 if SYNONYM is dropped
            0 if SYNONYM does not exist
            0 on all errors, exception is not raised but sqlcode is output.
   *****************************************************************/
   function DROP_SYNONYM(TABLE_NAME varchar2) return number;

   /*****************************************************************
   Function: DROP_INDEX
   Returns: 1 if index is dropped
            0 if index does not exist
            Raises application error on any other condition.
   *****************************************************************/
   function DROP_INDEX(INDEX_NAME  varchar2,
                       SCHEMA_NAME varchar2 default 'PWRPLANT') return number;

   /*****************************************************************
   Function: BACKUP_TABLE
   Returns: 1 if table is backed up
            0 if table does not exist
            Raises application error on any other condition.
   *****************************************************************/
   function BACKUP_TABLE(TABLE_NAME_TO_BACKUP varchar2,
                         BACKUP_TABLE_NAME    varchar2 default 'D1_',
                         SCHEMA_NAME          varchar2 default 'PWRPLANT') return number;

   /*****************************************************************
   Function: CREATE_TABLE_AS
   Returns: 1 if table is created
            0 if table does not exist
            Raises application error on any other condition.
   *****************************************************************/
   function CREATE_TABLE_AS(TABLE_NAME_TO_CREATE_FROM varchar2,
                            NEW_TABLE_NAME            varchar2,
                            SCHEMA_NAME               varchar2 default 'PWRPLANT') return number;

   /*****************************************************************
   Function: TRUNCATE_TABLE
   Returns: 1 if table is truncated
            0 if table does not exist
            Raises application error on any other condition.
   *****************************************************************/
   function TRUNCATE_TABLE(TABLE_NAME  varchar2,
                           SCHEMA_NAME varchar2 default 'PWRPLANT') return number;

   /*****************************************************************
   Function: DELETE_FROM_TABLE
   Returns: 1 if table delete is processed
            0 if table does not exist
            Raises application error on any other condition.
   *****************************************************************/
   function DELETE_FROM_TABLE(TABLE_NAME         varchar2,
                              COMMIT_TRANSACTION boolean default false,
                              SCHEMA_NAME        varchar2 default 'PWRPLANT') return number;

   /*****************************************************************
   Function: ADD_COLUMN
   Returns: 1 if success
            0 if column already exists
            Raises application error on any other condition.
   *****************************************************************/
   function ADD_COLUMN(TABLE_NAME  varchar2,
                       COLUMN_NAME varchar2,
                       DATA_TYPE   varchar2,
                       V_COMMENT   varchar2 default null,
                       SCHEMA_NAME varchar2 default 'PWRPLANT') return number;
                       
end PKG_UTIL;
/

create or replace package body PKG_UTIL is

   --
   -- TABLE_EXISTS
   --
   function TABLE_EXISTS(TABLE_NAME  varchar2,
                         SCHEMA_NAME varchar2 default 'PWRPLANT') return number is
      RTN number;

   begin
      select count(*)
        into RTN
        from ALL_TABLES
       where OWNER = UPPER(SCHEMA_NAME)
         and TABLE_NAME = UPPER(TABLE_NAME);

      return RTN;

   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('In TABLE_EXISTS - "' || SCHEMA_NAME || '"."' || NVL(TABLE_NAME, '"<null>') || '"');
         raise;
   end;

   --
   -- DROP_TABLE
   --
   function DROP_TABLE(TABLE_NAME  varchar2,
                       SCHEMA_NAME varchar2 default 'PWRPLANT') return number is
      RTN number;

   begin
      execute immediate 'drop table ' || SCHEMA_NAME || '.' || TABLE_NAME;
      DBMS_OUTPUT.PUT_LINE('Table "' || SCHEMA_NAME || '"."' || TABLE_NAME || '" was dropped.');
      RTN := DROP_SYNONYM(TABLE_NAME);
      return 1;

   exception
      when TABLE_NOT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('In DROP_TABLE - Table "' || SCHEMA_NAME || '"."' || TABLE_NAME || '" did not exist.');
         RTN := DROP_SYNONYM(TABLE_NAME);
         return 0; --Ignore but report back failure
      when LOOPING_CHAIN_OF_SYNONYMS then
         DBMS_OUTPUT.PUT_LINE('Table "' || SCHEMA_NAME || '"."' || TABLE_NAME || '" did not exist.');
         RTN := DROP_SYNONYM(TABLE_NAME);
         return 0; --Ignore but report back failure
      when others then
         DBMS_OUTPUT.PUT_LINE('In DROP_TABLE - "' || SCHEMA_NAME || '"."' || NVL(TABLE_NAME, '"<null>') || '"');
         raise; --Some unknown issue raise the error
   end;

   --
   -- DROP_COLUMN
   --

   function DROP_COLUMN(TABLE_NAME  varchar2,
                        COLUMN_NAME varchar2,
                        SCHEMA_NAME varchar2 default 'PWRPLANT') return number is

   begin
      execute immediate 'alter table ' || SCHEMA_NAME || '.' || TABLE_NAME || ' drop column ' || COLUMN_NAME;
      DBMS_OUTPUT.PUT_LINE('Sucessfully dropped column "' || COLUMN_NAME || '" from table "' || SCHEMA_NAME || '"."' ||
                           TABLE_NAME || '".');
      return 1;

   exception
      when INVALID_IDENTIFIER then
         -- Ignore if column does not exist.
         DBMS_OUTPUT.PUT_LINE('Column "' || NVL(COLUMN_NAME, '<null>') || '" does not exist on table "' || SCHEMA_NAME ||
                              '"."' || TABLE_NAME || '".');
         return 0;
      when others then
         DBMS_OUTPUT.PUT_LINE('In DROP_COLUMN - "' || SCHEMA_NAME || '"."' || NVL(TABLE_NAME, '<null>') ||
                              '" Column: "' || NVL(COLUMN_NAME, '<null>') || '"');
         raise;
   end;

   --
   -- DROP_FK
   --
   function DROP_FK(FK_TABLE_NAME         varchar2,
                    REFERENCED_TABLE_NAME varchar2,
                    FK_COLUMN_NAME        varchar2 default '%',
                    SCHEMA_NAME           varchar2 default 'PWRPLANT') return number as

      FK_NAME varchar2(30);

   begin
      select AC.CONSTRAINT_NAME
        into FK_NAME
        from ALL_CONSTRAINTS AC, ALL_CONS_COLUMNS ACC
       where AC.OWNER = 'PWRPLANT'
         and AC.CONSTRAINT_TYPE = 'R'
         and AC.TABLE_NAME = FK_TABLE_NAME
         and R_CONSTRAINT_NAME = (select CONSTRAINT_NAME
                                    from ALL_CONSTRAINTS AC
                                   where AC.OWNER = SCHEMA_NAME
                                     and AC.CONSTRAINT_TYPE = 'P'
                                     and AC.TABLE_NAME = REFERENCED_TABLE_NAME)
         and AC.CONSTRAINT_NAME = ACC.CONSTRAINT_NAME
         and ACC.COLUMN_NAME like FK_COLUMN_NAME;

      execute immediate 'alter table ' || FK_TABLE_NAME || ' drop constraint ' || FK_NAME;
      DBMS_OUTPUT.PUT_LINE('The "' || FK_NAME || '" constraint on the "' || FK_TABLE_NAME || '" table was dropped.');
      return 1;

   exception
      when TOO_MANY_ROWS then
         DBMS_OUTPUT.PUT_LINE('The "' || SCHEMA_NAME || '"."' || NVL(FK_TABLE_NAME, '<null>') ||
                              '" table has more than one FK that references ' || '"' ||
                              NVL(REFERENCED_TABLE_NAME, '<null>') || '".');
         DBMS_OUTPUT.PUT_LINE('You will need to add the column name as a parameter to the function call.');
         raise;
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('No FK on the "' || SCHEMA_NAME || '"."' || NVL(FK_TABLE_NAME, '<null>') ||
                              '" table references the "' || NVL(REFERENCED_TABLE_NAME, '<null>') || '" table.');
         return 0;
      when others then
         DBMS_OUTPUT.PUT_LINE('In DROP_FK - FK Table Name: "' || SCHEMA_NAME || '"."' || NVL(FK_TABLE_NAME, '<null>') || '"' ||
                              ' Referenced Table Name: "' ||
                              NVL(REFERENCED_TABLE_NAME,
                                  '<null>' || '"' || ' FK Column Name: "' || NVL(FK_COLUMN_NAME, '<null>') || '"'));
         raise;
   end;

   --
   -- DROP_SYNONYM
   --
   function DROP_SYNONYM(TABLE_NAME varchar2) return number is

   begin
      execute immediate 'drop public synonym ' || TABLE_NAME;
      DBMS_OUTPUT.PUT_LINE('Public synonym "' || TABLE_NAME || '" was dropped.');
      return 1;

   exception
      when SYNONYM_NOT_EXIST then
         DBMS_OUTPUT.PUT_LINE('Public synonym "' || NVL(TABLE_NAME, '<null>') || '" did not exist.');
         return 0;
      when others then
         -- Ignore all errors - output sqlcode
         DBMS_OUTPUT.PUT_LINE(PPCERR || 'Could not drop public synonym for table "' || NVL(TABLE_NAME, '<null>') || '".');
         DBMS_OUTPUT.PUT_LINE(PPCORA || sqlcode);
         return 0;
   end;

   --
   -- DROP_INDEX
   --
   function DROP_INDEX(INDEX_NAME  varchar2,
                       SCHEMA_NAME varchar2 default 'PWRPLANT') return number is
   begin
      execute immediate 'drop index ' || SCHEMA_NAME || '.' || INDEX_NAME;
      DBMS_OUTPUT.PUT_LINE('Sucessfully dropped index "' || INDEX_NAME || '".');
      return 1;

   exception
      when INDEX_NOT_EXISTS then
         -- Ignore if index does not exist.
         DBMS_OUTPUT.PUT_LINE('Index "' || NVL(INDEX_NAME, '<null>') || '" does not exist.');
         return 0;
      when others then
         DBMS_OUTPUT.PUT_LINE('In DROP_INDEX - "' || SCHEMA_NAME || '"."' || NVL(INDEX_NAME, '<null>'));
         raise;
   end;
   --
   -- BACKUP_TABLE
   --
   function BACKUP_TABLE(TABLE_NAME_TO_BACKUP varchar2,
                         BACKUP_TABLE_NAME    varchar2 default 'D1_',
                         SCHEMA_NAME          varchar2 default 'PWRPLANT') return number is

      L_BACKUP_TABLE_NAME varchar2(30);

   begin

      if (BACKUP_TABLE_NAME = 'D1_') then
         L_BACKUP_TABLE_NAME := BACKUP_TABLE_NAME || TABLE_NAME_TO_BACKUP;
      else
         L_BACKUP_TABLE_NAME := BACKUP_TABLE_NAME;
      end if;
      execute immediate 'create table ' || L_BACKUP_TABLE_NAME || ' as select * from ' || TABLE_NAME_TO_BACKUP;
      DBMS_OUTPUT.PUT_LINE('Table "' || SCHEMA_NAME || '"."' || TABLE_NAME_TO_BACKUP || '" was backed up to the "' ||
                           SCHEMA_NAME || '"."' || L_BACKUP_TABLE_NAME || '" table.');
      return 1;

   exception
      when TABLE_NOT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('In BACKUP_TABLE - Table "' || SCHEMA_NAME || '"."' ||
                              NVL(TABLE_NAME_TO_BACKUP, '<null>') || '" does not exist.');
         return 0;
      when NAME_ALREADY_USED then
         DBMS_OUTPUT.PUT_LINE('The backup table name "' || NVL(L_BACKUP_TABLE_NAME, '<null>') || '" already exists.');
         raise;
      when others then
         DBMS_OUTPUT.PUT_LINE('In BACKUP_TABLE - "' || SCHEMA_NAME || '"."' || NVL(TABLE_NAME_TO_BACKUP, '<null>') ||
                              '" BACKUP_TABLE_NAME: "' || NVL(L_BACKUP_TABLE_NAME, '<null>') || '"');
         raise;
   end;

   --
   -- CREATE_TABLE_AS
   --
   function CREATE_TABLE_AS(TABLE_NAME_TO_CREATE_FROM varchar2,
                            NEW_TABLE_NAME            varchar2,
                            SCHEMA_NAME               varchar2 default 'PWRPLANT') return number is

   begin

      execute immediate 'create table ' || NEW_TABLE_NAME || ' as select * from ' || TABLE_NAME_TO_CREATE_FROM || ' where 0 = 1';
      DBMS_OUTPUT.PUT_LINE('Table "' || SCHEMA_NAME || '"."' || NEW_TABLE_NAME || '" was create from the structure of "' ||
                           SCHEMA_NAME || '"."' || TABLE_NAME_TO_CREATE_FROM || '" table.');
      return 1;

   exception
      when TABLE_NOT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('In CREATE_TABLE_AS - Table "' || SCHEMA_NAME || '"."' ||
                              NVL(TABLE_NAME_TO_CREATE_FROM, '<null>') || '" does not exist.');
         return 0;
      when NAME_ALREADY_USED then
         DBMS_OUTPUT.PUT_LINE('In CREATE_TABLE_AS - The table name to create "' || NVL(NEW_TABLE_NAME, '<null>') || '" already exists.');
         raise;
      when others then
         DBMS_OUTPUT.PUT_LINE('In CREATE_TABLE_AS - TABLE_NAME_TO_CREATE_FROM: "' || SCHEMA_NAME || '"."' || NVL(TABLE_NAME_TO_CREATE_FROM, '<null>') ||
                              '" NEW_TABLE_NAME: "' || NVL(NEW_TABLE_NAME, '<null>') || '"');
         raise;
   end;


   --
   -- TRUNCATE_TABLE
   --
   function TRUNCATE_TABLE(TABLE_NAME  varchar2,
                           SCHEMA_NAME varchar2 default 'PWRPLANT') return number as

   begin
      execute immediate 'truncate table ' || TABLE_NAME;
      DBMS_OUTPUT.PUT_LINE('Table "' || SCHEMA_NAME || '"."' || TABLE_NAME || '" truncated.');
      return 1;

   exception
      when TABLE_NOT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('In TRUNCATE_TABLE - Table "' || SCHEMA_NAME || '"."' || NVL(TABLE_NAME, '<null>') ||
                              '" does not exist.');
         return 0;
      when others then
         DBMS_OUTPUT.PUT_LINE('In TRUNCATE_TABLE - "' || SCHEMA_NAME || '"."' || NVL(TABLE_NAME, '<null>') || '"');
         raise;
   end;

   --
   -- DELETE_FROM_TABLE
   --
   function DELETE_FROM_TABLE(TABLE_NAME         varchar2,
                              COMMIT_TRANSACTION boolean default false,
                              SCHEMA_NAME        varchar2 default 'PWRPLANT') return number as

   begin
      execute immediate 'delete from ' || TABLE_NAME;
      DBMS_OUTPUT.PUT_LINE('All rows deleted from table "' || SCHEMA_NAME || '"."' || TABLE_NAME || '".');

      if COMMIT_TRANSACTION = true then
         commit;
         DBMS_OUTPUT.PUT_LINE('Transaction committed.');
      end if;

      return 1;

   exception
      when TABLE_NOT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('In DELETE_FROM_TABLE - Table "' || SCHEMA_NAME || '"."' || NVL(TABLE_NAME, '<null>') ||
                              '" does not exist.');
         return 0;
      when others then
         DBMS_OUTPUT.PUT_LINE('In DELETE_FROM_TABLE - "' || SCHEMA_NAME || '"."' || NVL(TABLE_NAME, '<null>') || '"');
         raise;
   end;

   --
   -- ADD_COLUMN
   --
   function ADD_COLUMN(TABLE_NAME  varchar2,
                       COLUMN_NAME varchar2,
                       DATA_TYPE   varchar2,
                       V_COMMENT   varchar2 default null,
                       SCHEMA_NAME varchar2 default 'PWRPLANT') return number is
   begin     
      declare
         return_value number;
      begin
      
         begin
            EXECUTE IMMEDIATE 'alter table ' || SCHEMA_NAME || '.' || TABLE_NAME || ' add ' || COLUMN_NAME || ' ' || DATA_TYPE;
            DBMS_OUTPUT.PUT_LINE( 'INFO: COLUMN ' || COLUMN_NAME || ' ADDED TO TABLE ' || TABLE_NAME || ' WITH DATA TYPE ' || DATA_TYPE);
            return_value := 1;
            EXCEPTION
            WHEN COLUMN_EXISTS THEN
               DBMS_OUTPUT.PUT_LINE( 'INFO: COLUMN ' || COLUMN_NAME || ' ALREADY EXISTS IN TABLE ' || TABLE_NAME);
               return_value := 0;
            WHEN OTHERS THEN
               DBMS_OUTPUT.PUT_LINE('ERROR: In ADD_COLUMN - "' || SCHEMA_NAME || '"."' || NVL(TABLE_NAME, '"<null>') || '" adding column ' || COLUMN_NAME);
               raise; --Some unknown issue raise the error
         end;
         
         -- if they passed in a comment then use it, otherwise just leave it as is
         if return_value != 0 then
            begin
               execute immediate 'comment on column ' || SCHEMA_NAME || '.' || TABLE_NAME || '.' || COLUMN_NAME || ' is ''' ||
               V_COMMENT || '''';
               DBMS_OUTPUT.PUT_LINE('Sucessfully added the comment to column ' || COLUMN_NAME ||
               ' to table ' || SCHEMA_NAME || '.' || TABLE_NAME || '.');
               exception
               when others then
                  RAISE_APPLICATION_ERROR(-20002,
                  'Could not add comment to column ' || COLUMN_NAME || ' to ' ||
                  SCHEMA_NAME || '.' || TABLE_NAME || '. SQL Error: ' || sqlerrm);
            end;
         end if;
         
         return return_value;
         
      end;
   end ADD_COLUMN;
   
end PKG_UTIL;
/
