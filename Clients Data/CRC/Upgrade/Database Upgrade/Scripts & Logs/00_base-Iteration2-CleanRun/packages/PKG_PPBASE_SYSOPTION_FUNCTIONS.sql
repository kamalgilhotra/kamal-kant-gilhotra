CREATE OR REPLACE package pkg_ppbase_sysoption_functions is
   G_PKG_VERSION varchar(35) := '2018.2.1.0';

   function F_GETSYSTEMOPTIONCOMPANY(A_SYSTEM_OPTION_ID varchar2,
							  		A_COMPANY_ID number) return varchar2;

   function F_GETSYSTEMOPTION(A_SYSTEM_OPTION_ID varchar2) return varchar2;

   function F_GETSYSTEMOPTIONUSER(A_SYSTEM_OPTION_ID varchar2) return varchar2;

   function F_GETUSEROPTION(A_OBJECT            varchar2,
                            A_OPTION_IDENTIFIER varchar2) return varchar2;

end PKG_PPBASE_SYSOPTION_FUNCTIONS;
/

CREATE OR REPLACE package body pkg_ppbase_sysoption_functions is

   ----
   ----  The object identifying system options overridden by users in the user options table.
   ----
   I_SYSOPT_USER_OBJECT PPBASE_USER_OPTIONS.OBJECT%type := 'System Option';

   ----
   ----  Constants for the different system option value types.
   ----
   I_SYSOPT_VALUE_TYPE_FREEFORM constant varchar2(8) := 'FREEFORM';
   I_SYSOPT_VALUE_TYPE_DROPDOWN constant varchar2(8) := 'DROPDOWN';

   function F_GETSYSTEMOPTIONCOMPANY(A_SYSTEM_OPTION_ID varchar2,
							  		A_COMPANY_ID number) return varchar2 is
   	/************************************************************************************************************************************************************
      **
      **  f_getSystemOption()
      **
      **  Gets the system option value for the passed in system option and company.  The function first looks to the company system options then the default PowerPlan system option.  Null values
      **  in the database are returned as empty strings.
      **
      **  Parameters  :  string    :  (a_system_option_id) The system option for which we're returning the value.
	  **				 number	   :  (a_company_id) The company for this
      **
      **  Returns    :  string    :  ''    :  the specified system option does not exist or a value could not be found
      **                    else  :  the current value for this system option
      **
      ************************************************************************************************************************************************************/
   		R_CONTROL_VALUE varchar2(254);
   		COUNTER         number;
	begin

		----
		----  Check the company table
		----
   		select count(*), min(OPTION_VALUE)
     		into COUNTER, R_CONTROL_VALUE
     		from PPBASE_SYSTEM_OPTIONS_COMPANY
    		where upper(trim(SYSTEM_OPTION_ID)) = upper(trim(A_SYSTEM_OPTION_ID))
      		and COMPANY_ID = A_COMPANY_ID;

		----
		----  If not in the company table, check the options table
		----
   		if COUNTER = 0 then
      		select count(*), max(nvl(OPTION_VALUE, PP_DEFAULT_VALUE))
        		into COUNTER, R_CONTROL_VALUE
        		from PPBASE_SYSTEM_OPTIONS
       		where upper(trim(SYSTEM_OPTION_ID)) = upper(trim(A_SYSTEM_OPTION_ID));

      		if COUNTER = 0 then
         		R_CONTROL_VALUE := '';
      		end if;
   		end if;

   		return R_CONTROL_VALUE;

	end;

   function F_GETSYSTEMOPTION(A_SYSTEM_OPTION_ID varchar2) return varchar2 is
      /************************************************************************************************************************************************************
      **
      **  f_getSystemOption()
      **
      **  Gets the system option value for the passed in system option.  The function first looks to the user options to find the value then the company system options then the default PowerPlan system option.  Null values
      **  in the database are returned as empty strings.
      **
      **  Parameters  :  string    :  (a_system_option_id) The system option for which we're returning the value.
      **
      **  Returns    :  string    :  ''    :  the specified system option does not exist or a value could not be found
      **                    else  :  the current value for this system option
      **
      ************************************************************************************************************************************************************/
      SYSVAL varchar2(32767);
   begin
      /*
      *  Processing.
      */

      ----
      ----  Look for the system value in the user options table first if we should.
      ----
      SYSVAL := F_GETSYSTEMOPTIONUSER(A_SYSTEM_OPTION_ID);
      if SYSVAL is not null then
         return SYSVAL;
      end if;

      ----
      ----  If we don't find it in the user options table grab it from the system options table first looking in the company values and then in the powerplan default values.  (We could call the two different functions to retrieve the client value then the system value,
      ----  but doing it here in a decode with one SQL statement is quicker.)
      ----
      SYSVAL := null;

      select max(DECODE(trim(PPBASE_SYSTEM_OPTIONS.OPTION_VALUE),
                        null,
                        PPBASE_SYSTEM_OPTIONS.PP_DEFAULT_VALUE,
                        PPBASE_SYSTEM_OPTIONS.OPTION_VALUE))
        into SYSVAL
        from PPBASE_SYSTEM_OPTIONS
       where UPPER(trim(PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID)) = UPPER(trim(A_SYSTEM_OPTION_ID));

      return SYSVAL;
   end;

   function F_GETSYSTEMOPTIONUSER(A_SYSTEM_OPTION_ID varchar2) return varchar2 is
      /************************************************************************************************************************************************************
      **
      **  f_getSystemOptionUser()
      **
      **  Gets the current user's selected value for the passed in system option.  Null values in the database are returned as empty strings.
      **
      **  Parameters  :  string    :  (a_system_option_id) The system option for which we're returning the user's current value.
      **
      **  Returns    :  string    :  ''    :  the specified system option does not exist or a value could not be found
      **                    else  :  the current value for the current user for this system option
      **
      ************************************************************************************************************************************************************/
   begin
      ----
      ----  Look for the system value in the user options table.
      ----
      return F_GETUSEROPTION(I_SYSOPT_USER_OBJECT, A_SYSTEM_OPTION_ID);
   end;

   function F_GETUSEROPTION(A_OBJECT            varchar2,
                            A_OPTION_IDENTIFIER varchar2) return varchar2 is
      /************************************************************************************************************************************************************
      **
      **  of_getUserOption()
      **
      **  Gets the current user's value for the passed in user option.  Null values in the database are returned as empty strings.
      **
      **  Parameters  :  string    :  (a_object) The object for which we're getting a user option.
      **            string    :  (a_option_identifier) The option identifier for the specific option we want for this object.
      **
      **  Returns    :  string    :  ''    :  the specified user option does not exist or a value could not be found
      **                    else  :  the current value for the current user for this option
      **
      ************************************************************************************************************************************************************/
      OPTVAL   varchar2(32767);
      OPTVAL_A PPBASE_USER_OPTIONS.USER_OPTION%type;
      OPTVAL_B PPBASE_USER_OPTIONS.USER_OPTION_B%type;
      OPTVAL_C PPBASE_USER_OPTIONS.USER_OPTION_C%type;
      OPTVAL_D PPBASE_USER_OPTIONS.USER_OPTION_D%type;

   begin
      /*
      *  Processing.
      */

      ----
      ----  Look for the value in the user options table.
      ----
      OPTVAL_A := null;
      OPTVAL_B := null;
      OPTVAL_C := null;
      OPTVAL_D := null;

      select max(PPBASE_USER_OPTIONS.USER_OPTION),
             max(PPBASE_USER_OPTIONS.USER_OPTION_B),
             max(PPBASE_USER_OPTIONS.USER_OPTION_C),
             max(PPBASE_USER_OPTIONS.USER_OPTION_D)
        into OPTVAL_A, OPTVAL_B, OPTVAL_C, OPTVAL_D
        from PPBASE_USER_OPTIONS
       where PPBASE_USER_OPTIONS.OBJECT = A_OBJECT
         and UPPER(trim(PPBASE_USER_OPTIONS.OPTION_IDENTIFIER)) = UPPER(trim(A_OPTION_IDENTIFIER))
         and UPPER(trim(PPBASE_USER_OPTIONS.USERS)) = UPPER(trim(user));

      OPTVAL := OPTVAL_A || OPTVAL_B || OPTVAL_C || OPTVAL_D;

      return OPTVAL;
   end;

end PKG_PPBASE_SYSOPTION_FUNCTIONS;
/

GRANT EXECUTE ON pkg_ppbase_sysoption_functions TO pwrplant_role_dev;
