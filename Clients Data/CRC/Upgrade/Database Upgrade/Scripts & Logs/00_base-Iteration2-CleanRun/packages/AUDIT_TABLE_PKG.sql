  CREATE OR REPLACE PACKAGE "PWRPLANT"."AUDIT_TABLE_PKG" AS
   G_PKG_VERSION varchar(35) := '2018.2.1.0';
   FUNCTION setcontext(parm VARCHAR2, val VARCHAR2) RETURN NUMBER;
   FUNCTION setwindowcontext(winparm     VARCHAR2,
                             windowtitle VARCHAR2,
                             auditparm   VARCHAR2) RETURN NUMBER;
   FUNCTION createpackage(a_name IN VARCHAR2) RETURN NUMBER;
   FUNCTION geterrortext(code NUMBER) RETURN VARCHAR2;
   FUNCTION auditlookup(tab_name     IN VARCHAR2,
                        col_name     IN VARCHAR2,
                        lookup_value IN VARCHAR2,
                        where_clause IN VARCHAR2 DEFAULT '') RETURN VARCHAR2;
   FUNCTION auditlookuppk(tab_name     IN VARCHAR2,
                          col_name     IN VARCHAR2,
                          lookup_value IN VARCHAR2,
                          where_clause IN VARCHAR2 DEFAULT '') RETURN VARCHAR2;
END audit_table_pkg;
/

CREATE OR REPLACE PACKAGE BODY "PWRPLANT"."AUDIT_TABLE_PKG" AS
   FUNCTION setcontext(parm VARCHAR2, val VARCHAR2) RETURN NUMBER AS
      num NUMBER(22,
                 0);

   BEGIN
      dbms_session.set_context('PowerPlant_CTX',
                               parm,
                               val);
      num := 0;
      RETURN num;
   EXCEPTION
      WHEN OTHERS THEN
         num := SQLCODE;
         RETURN num;
   END setcontext;
   FUNCTION setwindowcontext(winparm     VARCHAR2,
                             windowtitle VARCHAR2,
                             auditparm   VARCHAR2) RETURN NUMBER AS
      num      NUMBER(22,
                      0);
      prog     CHAR(60);
      osuser   CHAR(60);
      machine  CHAR(60);
      terminal CHAR(60);

   BEGIN
      dbms_session.set_context('PowerPlant_CTX',
                               'window',
                               winparm);
      dbms_session.set_context('PowerPlant_CTX',
                               'audit',
                               auditparm);
      dbms_session.set_context('PowerPlant_CTX',
                               'windowtitle',
                               windowtitle);

      SELECT program,
             osuser,
             machine,
             terminal
        INTO prog,
             osuser,
             machine,
             terminal
        FROM v$session
       WHERE audsid = userenv('sessionid');
      dbms_session.set_context('PowerPlant_CTX',
                               'program',
                               prog);
      dbms_session.set_context('PowerPlant_CTX',
                               'osuser',
                               osuser);
      dbms_session.set_context('PowerPlant_CTX',
                               'machine',
                               machine);
      dbms_session.set_context('PowerPlant_CTX',
                               'terminal',
                               terminal);
      num := 0;
      RETURN num;
   EXCEPTION
      WHEN OTHERS THEN
         num := SQLCODE;
         RETURN num;
   END setwindowcontext;
   FUNCTION geterrortext(code NUMBER) RETURN VARCHAR2 AS
   BEGIN
      RETURN SQLERRM(code);
   END geterrortext;
   FUNCTION createpackage(a_name IN VARCHAR2) RETURN NUMBER IS
      CURSOR tablist_cur IS
         SELECT sql_str
           FROM temp_procedure_list
          WHERE NAME = a_name
          ORDER BY line_no;
      lines       dbms_sql.varchar2s;
      table_count INT;
      str         VARCHAR2(2000);
      str2        VARCHAR2(2000);
      str3        VARCHAR2(2000);
      code        NUMBER(22,
                         0);
      pos         NUMBER(22,
                         0);
      startpos    NUMBER(22,
                         0);
      cur         INT;

   BEGIN
      table_count := 0;
      OPEN tablist_cur;
      LOOP
         FETCH tablist_cur
            INTO str;
         IF (tablist_cur%NOTFOUND)
         THEN
            EXIT;
         END IF;
         table_count := table_count + 1;
         <<top>>
         IF length(str) > 250
         THEN
            startpos := 200;
            <<try_again>>
            pos := instr(str,
                         ',',
                         startpos);
            IF pos = 0
            THEN
               pos := instr(str,
                            '||',
                            startpos);
               IF pos = 0
               THEN
                  startpos := startpos - 50;
                  GOTO try_again;
               END IF;
               pos := pos + 1;
            END IF;
            str2 := substr(str,
                           1,
                           pos);
            lines(table_count) := str2;
            table_count := table_count + 1;
            str2 := substr(str,
                           pos + 1);
            str := str2;
            GOTO top;
         ELSE
            lines(table_count) := str;
         END IF;
      END LOOP;
      CLOSE tablist_cur;
      cur := dbms_sql.open_cursor();
      dbms_sql.parse(cur,
                     lines,
                     1,
                     table_count,
                     TRUE,
                     2);
      code := dbms_sql.EXECUTE(cur);
      str  := 'delete temp_procedure_list
WHERE name =''' || a_name || '''';
      EXECUTE IMMEDIATE str;
      RETURN 0;
   EXCEPTION
      WHEN OTHERS THEN
         code := SQLCODE;
         RETURN code;
   END createpackage;
   FUNCTION auditlookup(tab_name     IN VARCHAR2,
                        col_name     IN VARCHAR2,
                        lookup_value IN VARCHAR2,
                        where_clause IN VARCHAR2 DEFAULT '') RETURN VARCHAR2 AS
      num          NUMBER(22,
                          0);
      results      VARCHAR2(2000);
      lookup_table VARCHAR2(35);
      lookup_col   VARCHAR2(35);
      code_col     VARCHAR2(35);
      cur          INT;
      num_rec      NUMBER;
      sql_stmt     VARCHAR2(2000);

   BEGIN

      SELECT b.table_name,
             b.display_col,
             b.code_col
        INTO lookup_table,
             lookup_col,
             code_col
        FROM pp_table_audits a,
             powerplant_dddw b
       WHERE a.table_name = tab_name
         AND a.column_name = col_name
         AND a.dropdown_name = b.dropdown_name;
      sql_stmt := 'SELECT ' || lookup_col || ' FROM ' || lookup_table ||
                  ' WHERE ' || code_col || '=' || lookup_value || ' ' ||
                  where_clause;
      cur      := dbms_sql.open_cursor;
      dbms_sql.parse(cur,
                     sql_stmt,
                     dbms_sql.v7);
      dbms_sql.define_column(cur,
                             1,
                             lookup_col,
                             100);
      num := dbms_sql.EXECUTE(cur);
      num := dbms_sql.fetch_rows(cur);
      dbms_sql.column_value(cur,
                            1,
                            results);
      dbms_sql.close_cursor(cur);
      RETURN results;
   EXCEPTION
      WHEN OTHERS THEN
         num := SQLCODE;
         RETURN lookup_value;
   END auditlookup;
   FUNCTION auditlookuppk(tab_name     IN VARCHAR2,
                          col_name     IN VARCHAR2,
                          lookup_value IN VARCHAR2,
                          where_clause IN VARCHAR2 DEFAULT '') RETURN VARCHAR2 AS
      num          NUMBER(22,
                          0);
      results      VARCHAR2(2000);
      lookup_table VARCHAR2(35);
      lookup_col   VARCHAR2(35);
      code_col     VARCHAR2(35);
      cur          INT;
      num_rec      NUMBER;
      sql_stmt     VARCHAR2(2000);

   BEGIN

      SELECT display_table_name,
             display_column_name,
             code_column_name
        INTO lookup_table,
             lookup_col,
             code_col
        FROM pp_table_audits_pk_lookup
       WHERE upper(table_name) = upper(tab_name)
         AND upper(column_name) = upper(col_name);
      sql_stmt := '
SELECT ' || lookup_col || '
    FROM ' || lookup_table || '
            WHERE ' || code_col || '=' || lookup_value || ' ' ||
                  where_clause;
      cur      := dbms_sql.open_cursor;
      dbms_sql.parse(cur,
                     sql_stmt,
                     dbms_sql.v7);
      dbms_sql.define_column(cur,
                             1,
                             lookup_col,
                             100);
      num := dbms_sql.EXECUTE(cur);
      num := dbms_sql.fetch_rows(cur);
      dbms_sql.column_value(cur,
                            1,
                            results);
      dbms_sql.close_cursor(cur);
      RETURN results;
   EXCEPTION
      WHEN OTHERS THEN
         num := SQLCODE;
         RETURN lookup_value;
   END auditlookuppk;
END audit_table_pkg;
/
