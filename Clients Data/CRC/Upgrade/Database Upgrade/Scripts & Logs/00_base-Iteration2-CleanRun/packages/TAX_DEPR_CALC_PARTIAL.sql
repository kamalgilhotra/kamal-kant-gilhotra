create or replace package TAX_DEPR_CALC_PARTIAL is
   --||============================================================================
   --|| Application: PowerPlan
   --|| Object Name: TAX_DEPR_CALC_PARTIAL
   --|| Description: Partial year tax depreciation calculation
   --||============================================================================
   --|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
   --||============================================================================
   --|| Version  Date       Revised By     Reason for Change
   --|| -------- ---------- -------------- ----------------------------------------
   --|| 2015.2   09/16/2015 Sarah Byers    Original Version
   --|| 2016.1   02/05/2016 Paul Champion  January Transfer Calc Fix (PP-45403)
   --||============================================================================

   -- ============================== Tax Program Structure ========================
   --
   --                            calc_depr  -->  read tax_job_params
   --                                |
   --                                |
   --                                |
   --                                |--------------> Get Records
   --                                |
   --                  Loop  ------->|
   --                        ^       |--------------> Filter Records
   --                        |       |
   --                        |       |
   --                        |       |--------------> Calc Records
   --                        |
   --                        |       | <- Update Depreciation Records
   --                        <-------|
   --                                | <- Update Reconcile Records
   --                                |
   --                                | <- Commit
   --                               END
   --
   -- =============================================================================
   G_PKG_VERSION varchar(35) := '2018.2.1.0';
   g_limit constant pls_integer := 10000;
   subtype hash_t is varchar2(100);
   g_sep constant varchar2(1) := '-';
   g_start_time timestamp;

   type tax_bal_rec_rec is record(
      id             number(22, 0),
      reconcile_item number(22, 0),
      tax_year       number(22, 2));

   type tax_rec_bal_type is table of tax_bal_rec_rec index by pls_integer;
   type tax_rec_bal_table_type is table of tax_rec_bal_type index by hash_t;

   g_tax_reconcile_bal_hash tax_rec_bal_table_type;

   type tax_depr_bals_rec is record(
      id       number(22, 0),
      tax_year number(22, 2));

   type tax_depr_bals_rec_type is table of tax_depr_bals_rec index by pls_integer;
   type tax_depr_bals_table_type is table of tax_depr_bals_rec_type index by hash_t;

   g_tax_depr_bals_hash tax_depr_bals_table_type;

   type tax_rec_rec is record(
      id             number(22, 0),
      reconcile_item number(22, 0));

   type tax_rec_type is table of tax_rec_rec index by pls_integer;
   type tax_rec_table_type is table of tax_rec_type index by hash_t;

   g_tax_reconcile_hash tax_rec_table_type;

   type tax_book_activity_type is table of pls_integer index by pls_integer;
   type tax_book_activity_table_type is table of tax_book_activity_type index by hash_t;

   g_tax_book_activity_hash tax_book_activity_table_type;

   type tax_depr_hash_type is table of number(22, 0) index by hash_t;

   g_tax_depr_hash     tax_depr_hash_type;
   g_tax_reconcile_index_hash tax_depr_hash_type;

   type tax_convention_table_type is table of pls_integer index by binary_integer;

   g_tax_convention_hash tax_convention_table_type;

   type tax_rates_rec is record(
      id      number(22, 0),
      rate_id number(22, 0));

   type tax_rates_rec_type is table of tax_rates_rec index by pls_integer;

   type tax_rates_table_type is table of tax_rates_rec_type index by hash_t;

   g_tax_rates_hash  tax_rates_table_type;
   g_tax_limit2_hash tax_rates_table_type;

   g_table_book_ids table_list_id_type;

   g_version    number(22, 0);
   g_start_year number(22, 2);
   g_end_year   number(22, 2);

   g_table_vintage_ids table_list_id_type;
   g_table_class_ids   table_list_id_type;
   g_table_company_ids table_list_id_type;
   g_table_record_ids table_list_id_type;

   g_table_book_count    number(22, 0);
   g_table_vintage_count number(22, 0);
   g_table_class_count   number(22, 0);
   g_table_company_count number(22, 0);

   cursor tax_depr_cur is
      select distinct td.tax_record_id,
                      td.tax_book_id,
                      td.tax_year,
                      nvl(td.book_balance, 0)  book_balance,
                      nvl(td.tax_balance, 0) tax_balance,
                      nvl(remaining_life, 0) remaining_life,
                      nvl(td.accum_reserve, 0) accum_reserve,
                      nvl(td.sl_reserve, 0) sl_reserve,
                      nvl(depreciable_base, 0) depreciable_base,
                      nvl(td.fixed_depreciable_base, 0) fixed_depreciable_base,
                      nvl(actual_salvage, 0)  actual_salvage,
                      nvl(td.estimated_salvage, 0) estimated_salvage,
                      nvl(td.accum_salvage, 0) accum_salvage,
                      nvl(td.additions, 0)  additions,
                      nvl(td.transfers, 0) transfers,
                      nvl(td.adjustments, 0) adjustments,
                      nvl(td.retirements, 0) retirements,
                      nvl(td.extraordinary_retires, 0) extraordinary_retires,
                      nvl(td.accum_ordinary_retires, 0) accum_ordinary_retires,
                      nvl(cost_of_removal, 0) cost_of_removal,
                      nvl(est_salvage_pct, 0) est_salvage_pct,
                      nvl(retire_invol_conv, 0) retire_invol_conv,
                      nvl(salvage_invol_conv, 0) salvage_invol_conv,
                      nvl(salvage_extraord, 0) salvage_extraord,
                      nvl(td.reserve_at_switch, 0) reserve_at_switch,
                      nvl(td.quantity, 0) quantity,
                      nvl(td.quantity_end, 0) quantity_end,
                      nvl(convention_id, 0) convention_id,
                      nvl(extraordinary_convention, 0) extraordinary_convention,
                      nvl(tax_limit_id, -1) tax_limit_id,
                      nvl(tax_rate_id, 0) tax_rate_id,
                      nvl(td.number_months_beg, 0) number_months_beg,
                      nvl(td.number_months_end, 0) number_months_end,
                      nvl(tr.book_balance, 0) book_balance_xfer,
                      nvl(tr.tax_balance, 0) tax_balance_xfer,
                      nvl(tr.accum_reserve, 0) accum_reserve_xfer,
                      nvl(tr.sl_reserve, 0) sl_reserve_xfer,
                      nvl(tr.fixed_depreciable_base, 0) fixed_depreciable_base_xfer,
                      nvl(tr.estimated_salvage, 0) estimated_salvage_xfer,
                      nvl(tr.accum_salvage, 0) accum_salvage_xfer,
                      nvl(tr.accum_ordinary_retires, 0) accum_ordinary_retires_xfer,
                      nvl(tr.reserve_at_switch, 0) reserve_at_switch_xfer,
                      nvl(tr.quantity, 0) quantity_xfer,
                      nvl(tr.additions, 0) additions_xfer,
                      nvl(tr.calc_depreciation, 0) calc_depr_xfer,
                      tr.transfer_month transfer_month,
                      tr.transfer_direction transfer_direction,
                      nvl(td.transfer_depr, 0) transfer_depr,
                      0 transfer_fraction,
                      nvl(td.tax_credit_id, 0) tax_credit_id,
                      nvl(v.year , 0) vintage_year,
                      nvl(td.job_creation_amount, 0) job_creation_amount,
                      nvl(td.cor_res_impact, 0) cor_res_impact,
                      nvl(td.cor_expense, 0) cor_expense,
                      nvl(td.salvage_res_impact, 0) salvage_res_impact,
                      nvl(td.accum_ordin_retires_end, 0) accum_ordin_retires_end,
                      nvl(td.accum_salvage_end, 0) accum_salvage_end,
                      nvl(td.sl_reserve_end, 0) sl_reserve_end,
                      nvl(td.accum_reserve_end, 0) accum_reserve_end,
                      nvl(td.tax_balance_end, 0) tax_balance_end,
                      nvl(td.book_balance_end, 0) book_balance_end,
                      nvl(td.adjusted_retire_basis, 0) adjusted_retire_basis,
                      nvl(td.transfer_res_impact, 0) transfer_res_impact,
                      nvl(td.ex_retire_res_impact, 0) ex_retire_res_impact,
                      nvl(td.retire_res_impact, 0) retire_res_impact,
                      nvl(td.over_adj_depreciation, 0) over_adj_depreciation,
                      nvl(td.calc_depreciation, 0) calc_depreciation,
                      nvl(td.capital_gain_loss, 0) capital_gain_loss,
                      nvl(td.ex_gain_loss, 0) ex_gain_loss,
                      nvl(td.gain_loss, 0) gain_loss,
                      nvl(td.depreciation, 0) depreciation,
                      nvl(td.reserve_at_switch_end, 0) reserve_at_switch_end,
                      nvl(td.estimated_salvage_end, 0) estimated_salvage_end,
                      nvl(td.depreciation_adjust,0) depreciation_adjust,
                      nvl(td.book_balance_adjust,0) book_balance_adjust,
                      nvl(td.accum_reserve_adjust,0) accum_reserve_adjust,
                      nvl(td.depreciable_base_adjust,0) depreciable_base_adjust,
                      nvl(td.gain_loss_adjust,0) gain_loss_adjust,
                      nvl(td.cap_gain_loss_adjust,0) cap_gain_loss_adjust,
                      nvl(td.book_balance_adjust_method,1) book_balance_adjust_method,
                      nvl(td.accum_reserve_adjust_method,1)  accum_reserve_adjust_method,
                      nvl(td.depreciable_base_adjust_method,1) depreciable_base_adjust_method,
                      nvl(td.depreciation_adjust_method,1) depreciation_adjust_method
        from vintage v,
             tax_record_control trc,
             tax_depreciation td,
             (select tax_record_id,
                     tax_book_id,
                     tax_year,
                     transfer_month,
                     transfer_direction,
                     sum(book_balance) book_balance,
                     sum(tax_balance) tax_balance,
                     sum(accum_reserve) accum_reserve,
                     sum(sl_reserve) sl_reserve,
                     sum(fixed_depreciable_base) fixed_depreciable_base,
                     sum(estimated_salvage) estimated_salvage,
                     sum(accum_salvage) accum_salvage,
                     sum(accum_ordinary_retires) accum_ordinary_retires,
                     sum(reserve_at_switch) reserve_at_switch,
                     sum(quantity) quantity,
                     sum(additions) additions,
                     sum(calc_depreciation) calc_depreciation
                from (
                    select d.tax_record_id tax_record_id,
                           d.tax_book_id tax_book_id,
                           d.tax_year tax_year,
                           to_number(to_char(c.transfer_month,'YYYYMM')) transfer_month,
                           'FROM' transfer_direction,
                           -1 * d.book_balance book_balance,
                           -1 * d.tax_balance tax_balance,
                           -1 * d.accum_reserve accum_reserve,
                           -1 * d.sl_reserve sl_reserve,
                           -1 * d.fixed_depreciable_base fixed_depreciable_base,
                           -1 * d.estimated_salvage estimated_salvage,
                           -1 * d.accum_salvage accum_salvage,
                           -1 * d.accum_ordinary_retires accum_ordinary_retires,
                           -1 * d.reserve_at_switch reserve_at_switch,
                           -1 * d.quantity quantity,
                           -1 * d.additions additions,
                           -1 * d.calc_depreciation calc_depreciation
                      from tax_depreciation_transfer d, tax_transfer_control c
                     where d.tax_transfer_id = c.tax_transfer_id
                       and d.tax_year = c.tax_year
                       and d.tax_record_id = c.from_trid
                    union all
                    select c.to_trid tax_record_id,
                           d.tax_book_id tax_book_id,
                           d.tax_year tax_year,
                           to_number(to_char(c.transfer_month,'YYYYMM')) transfer_month,
                           'TO' transfer_direction,
                           -1 * d.book_balance book_balance,
                           -1 * d.tax_balance tax_balance,
                           -1 * d.accum_reserve accum_reserve,
                           -1 * d.sl_reserve sl_reserve,
                           -1 * d.fixed_depreciable_base fixed_depreciable_base,
                           -1 * d.estimated_salvage estimated_salvage,
                           -1 * d.accum_salvage accum_salvage,
                           -1 * d.accum_ordinary_retires accum_ordinary_retires,
                           -1 * d.reserve_at_switch reserve_at_switch,
                           -1 * d.quantity quantity,
                           -1 * d.additions additions,
                           -1 * d.calc_depreciation calc_depreciation
                      from tax_depreciation_transfer d, tax_transfer_control c
                     where d.tax_transfer_id = c.tax_transfer_id
                       and d.tax_year = c.tax_year
                       and d.tax_record_id = c.from_trid)
               group by tax_record_id, tax_book_id, tax_year, transfer_month, transfer_direction) tr
       where (trc.vintage_id in (select * from table(g_table_vintage_ids)) or
             g_table_vintage_count = 0)
         and (td.tax_book_id in (select * from table(g_table_book_ids)) or g_table_book_count = 0)
         and (trc.tax_class_id in (select * from table(g_table_class_ids)) or
             g_table_class_count = 0)
         and (trc.company_id in (select * from table(g_table_company_ids)) or
             g_table_company_count = 0)
         and td.tax_year between g_start_year and g_end_year
         and trc.tax_record_id = td.tax_record_id
         and td.tax_book_id = tr.tax_book_id
         and td.tax_record_id = tr.tax_record_id
         and td.tax_year = tr.tax_year
         and v.vintage_id = trc.vintage_id
         and trc.version_id = g_version
       order by tr.transfer_direction, td.tax_record_id, td.tax_book_id, td.tax_year;

   type type_tax_depr_rec is varray(11000) of tax_depr_cur%rowtype;

   type type_tax_depr_save_rec is varray(1000) of tax_depr_cur%rowtype;

   g_tax_depr_rec       type_tax_depr_rec;
   g_tax_depr_save1_rec type_tax_depr_save_rec;
   g_tax_depr_save2_rec type_tax_depr_save_rec;

   g_tax_depr_last_record_id number(22, 0);
   g_tax_depr_last_book_id   number(22, 0);

   cursor tax_convention_cur is
      select convention_id,
             retire_depr_id,
             retire_bal_id,
             retire_reserve_id,
             gain_loss_id,
             salvage_id,
             est_salvage_id,
             cost_of_removal_id,
             cap_gain_id
        from tax_convention;

   type type_tax_convention_rec is table of tax_convention_cur%rowtype;

   g_tax_convention_rec type_tax_convention_rec;

   cursor tax_rates_cur is
      select nvl(tr.tax_rate_id,0) tax_rate_id,
             nvl(rate,0) rate,
             nvl(rate1,0) rate1,
             nvl(tr.year,0) year,
             net_gross net_gross,
             nvl(life, 0 ) life,
             nvl(remaining_life_plan,0) remaining_life_plan,
             nvl(start_method,0) start_method,
             nvl(rounding_convention,0) rounding_convention,
             nvl(half_year_convention,0) half_year_convention,
             nvl(switched_year,0) switched_year
        from tax_rates tr, tax_rate_control trc
       where tr.tax_rate_id = trc.tax_rate_id
       order by tr.tax_rate_id, tr.year;

   type type_tax_rates_rec is table of tax_rates_cur%rowtype;

   g_tax_rates_rec type_tax_rates_rec;

   cursor tax_book_activity_cur is
      select trc.tax_record_id,
             tia.tax_book_id,
             b.tax_year,
             nvl(b.amount,0) amount,
             nvl(t.tax_activity_code_id,0) tax_activity_code_id,
             nvl(t.tax_activity_type_id,1) tax_activity_type_id
        from basis_amounts        b,
             tax_activity_code    t,
             tax_record_control   trc,
             tax_include_activity tia
       where b.tax_activity_code_id = t.tax_activity_code_id
         and b.tax_record_id = trc.tax_record_id
         and trc.tax_record_id = b.tax_record_id
         and tia.tax_include_id = b.tax_include_id
         and trc.version_id = g_version
         and (trc.vintage_id in (select * from table(g_table_vintage_ids)) or
             g_table_vintage_count = 0)
         and (tia.tax_book_id in (select * from table(g_table_book_ids)) or g_table_book_count = 0)
         and (trc.tax_class_id in (select * from table(g_table_class_ids)) or
             g_table_class_count = 0)
         and (trc.company_id in (select * from table(g_table_company_ids)) or
             g_table_company_count = 0)
         and b.tax_year between g_start_year and g_end_year
       order by trc.tax_record_id, tia.tax_book_id, b.tax_year;


   type type_tax_book_activity_rec is table of tax_book_activity_cur%rowtype;

   g_tax_book_activity_rec type_tax_book_activity_rec;

   cursor tax_reconcile_cur is
      select tbr.tax_record_id,
             tbr.tax_year,
             nvl(tbr.basis_amount_beg,0) basis_amount_beg,
             nvl(tbr.basis_amount_end,0) basis_amount_end,
             nvl(basis_amount_activity,0) basis_amount_activity,
             nvl(tbr.reconcile_item_id,0) reconcile_item_id,
             trim(upper(tri.type)) reconcile_item_type,
             nvl(tbr.tax_include_id,0) tax_include_id,
             nvl(basis_amount_input_retire,0) basis_amount_input_retire,
             nvl(tri.input_retire_ind,0) input_retire_ind,
             nvl(tbrt.basis_amount_beg, 0) basis_amount_transfer,
             nvl(tbrt.additions, 0) additions_transfer,
             nvl(tri.calced,0) calced,
             nvl(tri.depr_deduction,0) depr_deduction
        from tax_book_reconcile tbr,
             (select tax_record_id,
                     tax_include_id,
                     reconcile_item_id,
                     tax_year,
                     sum(basis_amount_beg) basis_amount_beg,
                     sum(additions) additions
                from tax_book_reconcile_transfer
               group by tax_record_id, tax_include_id, reconcile_item_id, tax_year) tbrt,
             tax_record_control trc,
             tax_reconcile_item tri
       where tbr.tax_record_id = trc.tax_record_id
         and tri.reconcile_item_id = tbr.reconcile_item_id
         and tbrt.tax_include_id(+) = tbr.tax_include_id
         and tbrt.tax_year(+) = tbr.tax_year
         and tbrt.tax_record_id(+) = tbr.tax_record_id
         and tbrt.reconcile_item_id(+) = tbr.reconcile_item_id
         and trc.tax_record_id in (select *  from table(g_table_record_ids))
         and (trc.vintage_id in (select * from table(g_table_vintage_ids)) or
             g_table_vintage_count = 0)
         and (trc.tax_class_id in (select * from table(g_table_class_ids)) or
             g_table_class_count = 0)
         and (trc.company_id in (select * from table(g_table_company_ids)) or
             g_table_company_count = 0)
         and tbr.tax_year between g_start_year and g_end_year + 1.1
         and trc.version_id = g_version
       order by tbr.tax_record_id, tbr.tax_include_id, tbr.tax_year;

   type type_tax_reconcile_rec is table of tax_reconcile_cur%rowtype;

   g_tax_reconcile_rec type_tax_reconcile_rec;

   cursor tax_limitation_cur is
      select tax_limitation.tax_limit_id,
             tax_limitation.limitation,
             year,
             nvl(tax_limit.compare_rate, 0) compare_rate
        from tax_limitation, tax_limit
       where tax_limitation.tax_limit_id = tax_limit.tax_limit_id
       order by year;

   type type_tax_limitation_rec is table of tax_limitation_cur%rowtype;

   g_tax_limitation_rec type_tax_limitation_rec;

   cursor tax_includes_activity_cur is
      select tax_book_id, tax_include_id from tax_include_activity order by tax_book_id;
   type tax_includes_type is table of number(22, 0);

   type type_tax_include_activity_tab is record(
      tax_book_id     number(22, 0),
      tax_include_ids tax_includes_type);

   type tax_includes_activity_type is table of type_tax_include_activity_tab;
   g_tax_includes_activity_rec tax_includes_activity_type;

   cursor tax_limit2_cur is
      select tl.tax_limit_id,
             nvl(tl.tax_credit_percent,0) tax_credit_percent,
             nvl(tl.basis_reduction_percent,0) basis_reduction_percent,
             nvl(tl.reconcile_item_id,0) reconcile_item_id,
             nvl(tl.tax_include_id,0) tax_include_id,
             tl.calc_future_years,
             nvl(tcs.ordering,0) ordering,
             nvl(tri.calced,0) calced,
             nvl(tri.depr_deduction,0) depr_deduction,
             nvl(td.tax_book_id,0) tax_book_id,
             nvl(tcs.tax_credit_id,0) tax_credit_id,
             nvl(trc.tax_record_id,0) tax_record_id,
             nvl(td.tax_year,0) tax_year
        from tax_limit          tl,
             tax_depreciation   td,
             tax_credit_schema  tcs,
             tax_reconcile_item tri,
             tax_record_control trc
       where tcs.tax_credit_id = td.tax_credit_id
         and td.tax_record_id = trc.tax_record_id
         and tl.tax_limit_id = tcs.tax_limit_id
         and tl.reconcile_item_id = tri.reconcile_item_id(+)
         and trc.version_id = g_version
         and ( td.tax_book_id in (select * from table(g_table_book_ids)) or g_table_book_count = 0)
         and td.tax_year between g_start_year and g_end_year
       order by trc.tax_record_id, td.tax_book_id, td.tax_year, tax_limit_id;

   type type_tax_limit2_rec is table of tax_limit2_cur%rowtype;

   g_tax_limit2_rec type_tax_limit2_rec;

   function get_version return varchar2;

   function calc_depr(a_job_no number) return integer;

   function get_tax_depr(a_start_row out pls_integer) return integer;

   function get_tax_convention return integer;

   function get_tax_rates return integer;

   function get_tax_book_activity return integer;

   function get_tax_reconcile return integer;

   function get_tax_limitation return integer;

   function get_tax_include_activity return integer;

   function get_tax_job_creation return integer;

   procedure write_log(a_job_no     number,
                       a_error_type number,
                       a_code       number,
                       a_msg        varchar2);

   function update_tax_depr(a_start_row pls_integer,
                            a_num_rec   pls_integer) return number;

   function calc(a_tax_depr_index            integer,
                 a_tax_depr_bals_index       integer,
                 a_tax_depr_bals_rows        pls_integer,
                 a_tax_convention_rec        tax_convention_cur%rowtype,
                 a_tax_convention_rows       pls_integer,
                 a_extraord_convention_rec   tax_convention_cur%rowtype,
                 a_extraord_convention_rows  pls_integer,
                 a_tax_limitation_sub_rec    type_tax_limitation_rec,
                 a_tax_limitation_rows       pls_integer,
                 a_tax_reconcile_sub_rec     in out nocopy type_tax_reconcile_rec,
                 a_tax_reconcile_rows        in out nocopy pls_integer,
                 a_reconcile_bals_sub_rec    in out nocopy type_tax_reconcile_rec,
                 a_reconcile_bals_rows       in out nocopy pls_integer,
                 a_tax_job_creation_sub_rec  type_tax_limit2_rec,
                 a_tax_job_creation_rows     pls_integer,
                 a_tax_book_activity_sub_rec type_tax_book_activity_rec,
                 a_tax_book_activity_rows    pls_integer,
                 a_tax_rates_sub_rec         type_tax_rates_rec,
                 a_tax_rates_rows            pls_integer,
                 a_short_months              in number,
                 a_add_tax_reconcile_rec     in out nocopy type_tax_reconcile_rec,
                 a_add_tax_reconcile_rows    in out nocopy integer,
                 a_nyear                     number) return number;

   procedure set_session_parameter;

   g_job_no  number(22, 0);
   g_line_no number(22, 0);

   g_debug_record_id number(22, 0);
   
end TAX_DEPR_CALC_PARTIAL;
/
create or replace package body TAX_DEPR_CALC_PARTIAL is

   -- =============================================================================
   --  Function GET_VERSION
   -- =============================================================================

   function get_version return varchar2 is
   begin
      return '2016.1';
   end get_version;

   -- =============================================================================
   --  Function FILTER_TAX_BOOK_ACTIVITY
   -- =============================================================================
   function filter_tax_book_activity(a_tax_book_activity_rec in out nocopy type_tax_book_activity_rec,
                                     a_tax_record_id         pls_integer,
                                     a_tax_book_id           pls_integer,
                                     a_tax_year              number) return pls_integer is
      i       pls_integer;
      l_count integer;
      l_row   integer;
      l_code  pls_integer;
   begin

      l_count := g_tax_book_activity_hash(to_char(a_tax_record_id) || g_sep || to_char(a_tax_book_id) || g_sep || to_char(a_tax_year))
                 .count;
      a_tax_book_activity_rec.delete;

      for i in 1 .. l_count
      loop
         l_row := g_tax_book_activity_hash(to_char(a_tax_record_id) || g_sep ||
                                           to_char(a_tax_book_id) || g_sep || to_char(a_tax_year)) (i);
         a_tax_book_activity_rec.extend;
         a_tax_book_activity_rec(i) := g_tax_book_activity_rec(l_row);
      end loop;
      return l_count;

   exception
      when no_data_found then
         return 0;
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_book_activity ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_tax_book_activity;

   -- =============================================================================
   --  Function FILTER_TAX_RECONCILE
   -- =============================================================================
   function filter_tax_reconcile(a_tax_reconcile_rec in out nocopy type_tax_reconcile_rec,
                                 a_tax_record_id     pls_integer,
                                 a_include_ids       tax_includes_type,
                                 l_tax_year          number) return pls_integer is
      i                     pls_integer;
      k                     pls_integer;
      l_count               pls_integer;
      l_index               pls_integer;
      l_row_count           pls_integer;
      l_tax_reconcile_count pls_integer;
      l_include_id_count    pls_integer;
      l_include_id          pls_integer;
      l_code                pls_integer;
   begin
      l_row_count := 0;
      a_tax_reconcile_rec.delete;
      l_include_id_count := a_include_ids.count;
      for i in 1 .. l_include_id_count
      loop
         l_include_id := a_include_ids(i);
         begin
            l_tax_reconcile_count := g_tax_reconcile_hash(to_char(a_tax_record_id) || g_sep || to_char(l_include_id) || g_sep || to_char(l_tax_year))
                                     .count;
         exception
            when no_data_found then
               l_tax_reconcile_count := 0;
         end;

         for k in 1 .. l_tax_reconcile_count
         loop
            l_index     := g_tax_reconcile_hash(to_char(a_tax_record_id) || g_sep ||
                                                to_char(l_include_id) || g_sep ||
                                                to_char(l_tax_year))(k).id;
            l_row_count := l_row_count + 1;
            a_tax_reconcile_rec.extend;
            a_tax_reconcile_rec(l_row_count) := g_tax_reconcile_rec(l_index);

            /** START Breakpoint for Debugger.**/
            if a_tax_record_id = g_debug_record_id then
               WRITE_LOG(G_JOB_NO, 1, 0, ' *** tr '||g_debug_record_id||' discovered. FILTER_TAX_RECONCILE. end of loop, l_row_count : '||l_row_count||', l_tax_year : '||l_tax_year||', l_include_id : '||l_include_id);
            end if;
            /** END Breakpoint for Debugger.**/
         end loop;
      end loop;
      i := 0;
      return l_row_count;
   exception
      when no_data_found then
         return 0;
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_reconcile_bals ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_tax_reconcile;

   -- =============================================================================
   --  Function ADD_TAX_RECONCILE
   -- =============================================================================
   function add_tax_reconcile(   a_tax_record_id     pls_integer,
                                 a_include_id        pls_integer,
                                 a_tax_year          number,
                                 a_reconcile_item_id  pls_integer,
                                 a_record_index pls_integer) return pls_integer is
      k                     pls_integer;
      l_tax_reconcile_count pls_integer;
      l_include_id_count    pls_integer;
      l_include_id          pls_integer;
      l_code                pls_integer;
   begin
      l_include_id := a_include_id;
      begin
            l_tax_reconcile_count := g_tax_reconcile_hash(to_char(a_tax_record_id) || g_sep ||
                 to_char(l_include_id) ||
                 g_sep || to_char(a_tax_year)).count;
      exception
            when no_data_found then
               l_tax_reconcile_count := 0;
      end;
      if l_tax_reconcile_count > 0 then
            k := l_tax_reconcile_count + 1;
            g_tax_reconcile_hash(to_char(a_tax_record_id) || g_sep ||
                                                to_char(l_include_id) || g_sep ||
                                             to_char(a_tax_year))(k).id := a_record_index;
      else
            g_tax_reconcile_hash(to_char(a_tax_record_id) || g_sep ||
                                                to_char(l_include_id) || g_sep ||
                                             to_char(a_tax_year))(1).id := a_record_index;
      end if;
      g_tax_reconcile_index_hash( to_char(a_tax_record_id) || g_sep ||
                                  to_char(l_include_id) || g_sep ||
                                  to_char(a_tax_year) || g_sep ||
                                  to_char(a_reconcile_item_id) ) := a_record_index;
      return 1;
   exception
      when no_data_found then
         return 0;
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_reconcile_bals ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end add_tax_reconcile;

   -- =============================================================================
   --  Function FILTER_TAX_RECONCILE_BALS
   -- =============================================================================
   function filter_tax_reconcile_bals(a_tax_reconcile_rec in out nocopy type_tax_reconcile_rec,
                                      a_tax_record_id     pls_integer,
                                      a_include_ids       tax_includes_type,
                                      a_tax_year          number) return pls_integer is
      i                     pls_integer;
      l_code                pls_integer;
      l_count               pls_integer;
      l_include_id          pls_integer;
      l_index               pls_integer;
      l_row_count           pls_integer;
      l_tax_reconcile_count pls_integer;
      l_include_id_count    pls_integer;
      l_low_year            number(22,2); -- 5.30 Fix
      l_tax_year            number(22,2);  -- 5.30 Fix
   begin

      a_tax_reconcile_rec.delete;
      l_include_id_count := a_include_ids.count;
      l_low_year         := 0;
      -- find the bal year
      for i in 1 .. l_include_id_count
      loop
         l_include_id := a_include_ids(i);
         begin
            l_tax_reconcile_count := g_tax_reconcile_bal_hash(to_char(a_tax_record_id) || g_sep || to_char(l_include_id))
                                     .count;
         exception
            when no_data_found then
               l_tax_reconcile_count := 0;
         end;

         for k in 1 .. l_tax_reconcile_count
         loop
            l_index := g_tax_reconcile_bal_hash(to_char(a_tax_record_id) || g_sep ||
                                                to_char(l_include_id))(k).id;
            if (g_tax_reconcile_rec(l_index)
               .tax_year > a_tax_year and
                (g_tax_reconcile_rec(l_index).tax_year < l_low_year or l_low_year = 0)) then
               l_low_year := g_tax_reconcile_rec(l_index).tax_year;
            end if;
         end loop;
      end loop;

      l_row_count := 0;
      l_tax_year  := l_low_year;
      for i in 1 .. l_include_id_count
      loop
         l_include_id := a_include_ids(i);
         begin
            l_tax_reconcile_count := g_tax_reconcile_hash(to_char(a_tax_record_id) || g_sep || to_char(l_include_id) || g_sep || to_char(l_tax_year))
                                     .count;
         exception
            when no_data_found then
               l_tax_reconcile_count := 0;
         end;
         for k in 1 .. l_tax_reconcile_count
         loop
            l_index     := g_tax_reconcile_hash(to_char(a_tax_record_id) || g_sep ||
                                                to_char(l_include_id) || g_sep ||
                                                to_char(l_tax_year))(k).id;
            l_row_count := l_row_count + 1;
            a_tax_reconcile_rec.extend;
            a_tax_reconcile_rec(l_row_count) := g_tax_reconcile_rec(l_index);
         end loop;
      end loop;

      i := 0;
      return l_row_count;

   exception
      when no_data_found then
         return 0;
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_reconcile_bals ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_tax_reconcile_bals;

   -- =============================================================================
   --  Function FILTER_TAX_LIMITATION
   -- =============================================================================
   function filter_tax_limitation(a_tax_limitation_sub_rec in out nocopy type_tax_limitation_rec,
                                  a_limit_id               pls_integer) return pls_integer is
      i       pls_integer;
      l_count integer;
      l_row   integer;
      l_code  pls_integer;
   begin

      l_count := g_tax_limitation_rec.count;
      a_tax_limitation_sub_rec.delete;
      l_row := 0;
      for i in 1 .. l_count
      loop
         if g_tax_limitation_rec(i).tax_limit_id = a_limit_id then
            l_row := 1 + l_row;
            a_tax_limitation_sub_rec.extend;
            a_tax_limitation_sub_rec(l_row) := g_tax_limitation_rec(i);
         end if;
      end loop;
      return l_row;
   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_limitation ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_tax_limitation;

   -- =============================================================================
   --  Function FILTER_TAX_LIMIT
   -- =============================================================================
   function filter_tax_limit(a_tax_limit_sub_rec in out nocopy type_tax_limit2_rec,
                             a_record_id         pls_integer,
                             a_book_id           pls_integer,
                             a_tax_year          number) return pls_integer is
      i       pls_integer;
      l_count integer;
      l_row   integer;
      l_index pls_integer;
      l_code  pls_integer;
   begin

      l_count := g_tax_limit2_rec.count;
      if a_tax_limit_sub_rec.exists(1) then
         a_tax_limit_sub_rec.delete;
      end if;
      l_row := 0;

      l_count := g_tax_limit2_hash(to_char(a_record_id) || g_sep || to_char(a_book_id) || g_sep || to_char(a_tax_year)).count;
      for i in 1 .. l_count
      loop
         l_index := g_tax_limit2_hash(to_char(a_record_id) || g_sep || to_char(a_book_id) || g_sep || to_char(a_tax_year))(i).id;
         a_tax_limit_sub_rec.extend;
         a_tax_limit_sub_rec(i) := g_tax_limit2_rec(l_index);
      end loop;
      return l_count;
   exception
      when no_data_found then
         return 0;
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_limit ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_tax_limit;

   -- =============================================================================
   --  Function FILTER_TAX_RATES
   -- =============================================================================
   function filter_tax_rates(a_tax_rates_sub_rec in out nocopy type_tax_rates_rec,
                             a_rate_id           pls_integer) return pls_integer is
      i       pls_integer;
      l_count integer;
      l_row   integer;
      l_index pls_integer;
      l_code  pls_integer;
   begin

      l_count := g_tax_rates_rec.count;
      if a_tax_rates_sub_rec.exists(1) then
         a_tax_rates_sub_rec.delete;
      end if;
      l_row := 0;

      begin
         l_count := g_tax_rates_hash(a_rate_id).count;
      exception
         when no_data_found then
            WRITE_LOG(G_JOB_NO, 1, 0, 'Error!  No tax rates found for tax rate id = '||a_rate_id||' ! Exiting...');
            return - 1;
      end;

      for i in 1 .. l_count
      loop
         l_index := g_tax_rates_hash(a_rate_id)(i).id;
         a_tax_rates_sub_rec.extend(1);
         a_tax_rates_sub_rec(i) := g_tax_rates_rec(l_index);
      end loop;
      return l_count;
   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_rates ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_tax_rates;

   -- =============================================================================
   --  Function CALC_DEPR
   -- =============================================================================
   function calc_depr(a_job_no number) return integer is
      cursor tax_job_params_cur is
         select version, tax_year, vintage, tax_book_id, tax_class_id, company_id
           from tax_job_params
          where job_no = a_job_no;
      l_version        number(22, 0);
      l_tax_year       number(22, 2);
      l_vintage        number(22, 0);
      l_transfer_month number(22, 0);
      l_tax_book_id    number(22, 0);
      l_tax_record_id  number(22, 0);
      l_tax_class_id   number(22, 0);
      l_company_id     number(22, 0);
      type tax_years_type is table of number(22, 2) index by pls_integer;
      tax_years             tax_years_type;
      tax_year_count        pls_integer;
      l_current_year        number(22, 2);
      l_depr_index          pls_integer;
      l_tax_depr_rows       pls_integer;
      l_tax_depr_bals_index pls_integer;
      code                  integer;
      rows                  pls_integer;
      l_short_months        pls_integer;
      l_next_year           number(22, 2);
      l_year                number(22, 2);
      l_year_count          pls_integer;
      k                     pls_integer;

      l_tax_book_activity_sub_rec  type_tax_book_activity_rec;
      l_tax_book_activity_sub_rows pls_integer;
      l_count                      pls_integer;
      l_tax_include_activity_index pls_integer;
      l_tax_book_reconcile_rows    pls_integer;
      l_tax_reconcile_sub_rows     pls_integer;
      l_tax_reconcile_sub_rec      type_tax_reconcile_rec;
      l_reconcile_bals_sub_rows    pls_integer;
      l_reconcile_bals_sub_rec     type_tax_reconcile_rec;
      l_tax_limitation_rows        pls_integer;
      l_tax_limitation_sub_rows    pls_integer;
      l_tax_rates_rows             pls_integer;
      l_tax_book_activity_rows     pls_integer;
      l_tax_include_activity_rows  pls_integer;
      l_tax_convention_rows        pls_integer;
      l_extraordinary_conv_index   pls_integer;
      l_tax_convention_index       pls_integer;
      l_tax_limitation_sub_rec     type_tax_limitation_rec;
      l_tax_job_creation_sub_rec   type_tax_limit2_rec;
      l_tax_job_creation_sub_rows  pls_integer;
      l_tax_rates_sub_rec          type_tax_rates_rec;
      l_tax_rates_sub_rows         pls_integer;
      l_add_tax_reconcile_rec      type_tax_reconcile_rec;
      l_add_tax_reconcile_rows     pls_integer;
      l_add_total_tax_reconcile_rows     pls_integer := 0;
      l_code                       pls_integer;
      l_start_row                  pls_integer;
      l_total_rows                 pls_integer := 0;
      m                            pls_integer;
      rtn_code                     number(22,0);

   begin
      g_start_time := current_timestamp;
      g_job_no  := a_job_no;
      g_line_no := 1;
      dbms_output.enable(100000);
      write_log(g_job_no, 0, 0, 'Partial Year Tax Depreciation  Started Version=' || get_version());
      set_session_parameter();

      rtn_code := AUDIT_TABLE_PKG.setcontext('audit','no');

      g_table_book_ids         := table_list_id_type(-1);
      g_table_vintage_ids      := table_list_id_type();
      g_table_class_ids        := table_list_id_type(-1);
      g_table_company_ids      := table_list_id_type(-1);
      g_table_record_ids       := table_list_id_type(-1);
      g_tax_depr_save1_rec     := type_tax_depr_save_rec();
      g_tax_depr_save2_rec     := type_tax_depr_save_rec();

      tax_year_count        := 0;
      g_table_book_count    := 0;
      g_table_vintage_count := 0;
      g_table_class_count   := 0;
      g_table_company_count := 0;

      l_tax_limitation_sub_rec    := type_tax_limitation_rec();
      l_tax_rates_sub_rec         := type_tax_rates_rec();
      l_tax_job_creation_sub_rec  := type_tax_limit2_rec();
      l_add_tax_reconcile_rec     := type_tax_reconcile_rec();
      l_reconcile_bals_sub_rec    := type_tax_reconcile_rec();
      l_tax_book_activity_sub_rec := type_tax_book_activity_rec();

      /**enter the record id to follow through the Debugger and/or logs here: **/
      g_debug_record_id := 999999999999;

      open tax_job_params_cur;
      loop
         fetch tax_job_params_cur
            into l_version, l_tax_year, l_vintage, l_tax_book_id, l_tax_class_id, l_company_id;
         if not l_version is null then
            g_version := l_version;
         end if;
         if not l_tax_year is null then
            tax_year_count := tax_year_count + 1;
            tax_years(tax_year_count) := l_tax_year;
         end if;
         if not l_vintage is null then
            g_table_vintage_count := 1 + g_table_vintage_count;
            g_table_vintage_ids.extend;
            g_table_vintage_ids(g_table_vintage_count) := l_vintage;
         end if;
         if not l_tax_book_id is null then
            g_table_book_count := 1 + g_table_book_count;
            g_table_book_ids.extend;
            g_table_book_ids(g_table_book_count) := l_tax_book_id;
         end if;
         if not l_tax_class_id is null then
            g_table_class_count := 1 + g_table_class_count;
            g_table_class_ids.extend;
            g_table_class_ids(g_table_class_count) := l_tax_class_id;
         end if;
         if not l_company_id is null then
            g_table_company_count := 1 + g_table_company_count;
            g_table_company_ids.extend;
            g_table_company_ids(g_table_company_count) := l_company_id;
         end if;
         exit when tax_job_params_cur%notfound;
      end loop;

      g_start_year := tax_years(tax_years.first);
      g_end_year   := tax_years(tax_years.last);

      l_tax_convention_rows       := get_tax_convention();
      l_tax_rates_rows            := get_tax_rates();
      l_tax_book_activity_rows    := get_tax_book_activity();
      l_tax_limitation_rows       := get_tax_limitation();
      l_tax_include_activity_rows := get_tax_include_activity();
      l_tax_reconcile_sub_rec     := type_tax_reconcile_rec();
      
      delete from tax_depr_transfer_audit;

      rows := get_tax_job_creation();
      loop
         l_tax_depr_rows := get_tax_depr(l_start_row);
         exit when l_tax_depr_rows = 0;

         l_tax_book_reconcile_rows   := get_tax_reconcile();

         l_current_year := 0;
         l_total_rows   := l_total_rows + (l_tax_depr_rows - l_start_row + 1);

         for l_depr_index in l_start_row .. l_tax_depr_rows
         loop
            l_tax_year       := g_tax_depr_rec(l_depr_index).tax_year;
            l_tax_record_id  := g_tax_depr_rec(l_depr_index).tax_record_id;
            l_tax_book_id    := g_tax_depr_rec(l_depr_index).tax_book_id;
            l_transfer_month := g_tax_depr_rec(l_depr_index).transfer_month;

            /** START Breakpoint for Debugger.**/
            if l_tax_record_id = g_debug_record_id then
               WRITE_LOG(G_JOB_NO, 1, 0, ' *** tr '||g_debug_record_id||' discovered. CALC_DEPR. start of inner loop, tax book : '||l_tax_book_id||', tax year : '||l_tax_year);
            end if;
            /** END Breakpoint for Debugger.**/

            -- see if the the tax year has changed from the previous record
            if l_current_year <> g_tax_depr_rec(l_depr_index).tax_year then
               l_current_year := g_tax_depr_rec(l_depr_index).tax_year;
               begin
                  select nvl(months, 0)
                    into l_short_months
                    from tax_year_version
                   where version_id = g_version
                     and tax_year = l_current_year;
                  if (l_short_months <= 0 or l_short_months >= 12) then
                     l_short_months := 12;
                  end if;
               exception
                  when no_data_found then
                     l_short_months := 12;
               end;
               select nvl(min(tax_year), 0)
                 into l_next_year
                 from tax_year_version
                where version_id = g_version
                  and tax_year > l_current_year;
            end if;

            l_tax_depr_bals_index := 0;
            if g_tax_depr_bals_hash(to_char(l_tax_record_id) || g_sep || to_char(l_tax_book_id))
             .exists(1) then
               l_year_count := g_tax_depr_bals_hash(to_char(l_tax_record_id) || g_sep || to_char(l_tax_book_id))
                               .count;

               for k in 1 .. l_year_count
               loop
                  l_year := g_tax_depr_bals_hash(to_char(l_tax_record_id) || g_sep ||
                                                 to_char(l_tax_book_id))(k).tax_year;
                  if l_year = l_next_year  then
                     l_tax_depr_bals_index := g_tax_depr_bals_hash(to_char(l_tax_record_id) ||
                                                g_sep || to_char(l_tax_book_id))(k).id;
                  end if;
               end loop;
            end if;

            -- get tax book activity record
            l_tax_book_activity_sub_rows := filter_tax_book_activity(l_tax_book_activity_sub_rec,
                                                                     l_tax_record_id,
                                                                     l_tax_book_id,
                                                                     l_tax_year);


            -- check for any activity
            l_tax_include_activity_index := 0;
            for k in 1 .. l_tax_include_activity_rows
            loop
               if g_tax_includes_activity_rec(k).tax_book_id = l_tax_book_id then
                  l_tax_include_activity_index := k;
                  exit;
               end if;
            end loop;

            -- if no activity index was found, it means that there was no row in tax include activity, which is
            -- a config issue/bad data.  This may have other unintended consequences, so error out with a clear
            -- message.
            if l_tax_include_activity_index = 0 or l_tax_include_activity_index is null then
               WRITE_LOG(G_JOB_NO, 1, 0, 'Error!  Tax book id '||l_tax_book_id||' was not found in tax_include_activity.  Please fix your system configuration for this tax book and try again.');
               return - 1;
            end if;

            if l_tax_include_activity_index <= l_tax_include_activity_rows then
               l_tax_reconcile_sub_rows := filter_tax_reconcile(l_tax_reconcile_sub_rec,
                                                                l_tax_record_id,
                                                                g_tax_includes_activity_rec(l_tax_include_activity_index)
                                                                .tax_include_ids,
                                                                l_tax_year);

               l_reconcile_bals_sub_rows := filter_tax_reconcile_bals(l_reconcile_bals_sub_rec,
                                                                      l_tax_record_id,
                                                                      g_tax_includes_activity_rec(l_tax_include_activity_index)
                                                                      .tax_include_ids,
                                                                      l_tax_year);
            else
               l_tax_reconcile_sub_rows  := 0;
               l_reconcile_bals_sub_rows := 0;
            end if;

            l_tax_convention_index := g_tax_convention_hash(g_tax_depr_rec(l_depr_index)
                                                            .convention_id);

            l_extraordinary_conv_index := g_tax_convention_hash(g_tax_depr_rec(l_depr_index)
                                                                .extraordinary_convention);

            l_tax_limitation_sub_rows := filter_tax_limitation(l_tax_limitation_sub_rec,
                                                               g_tax_depr_rec(l_depr_index)
                                                               .tax_limit_id);

            l_tax_job_creation_sub_rows := filter_tax_limit(l_tax_job_creation_sub_rec,
                                                            l_tax_record_id,
                                                            l_tax_book_id,
                                                            l_tax_year);

            l_tax_rates_sub_rows := filter_tax_rates(l_tax_rates_sub_rec,
                                                     g_tax_depr_rec(l_depr_index).tax_rate_id);

            ----filter tax rates function was enhanced to exit if there were no rates to use
            ----bad config, but force the exit if encountered, don't continue on.
            ----error message for this situation is in the filter tax rates function.
            if l_tax_rates_sub_rows < 0 then
               return -1;
            end if;
            
            -- Update depreciation so that we aren't double counting in case transfers is run multiple times before depreciation is run again
            update tax_depreciation
               set depreciation = depreciation - nvl(transfer_depr,0)
             where tax_record_id = l_tax_record_id
               and tax_book_id = l_tax_book_id
               and tax_year = l_tax_year;

            if l_tax_rates_sub_rows <> 0 then
               l_code := calc(l_depr_index,
                              l_tax_depr_bals_index,
                              l_tax_depr_bals_index,
                              g_tax_convention_rec(l_tax_convention_index),
                              l_tax_convention_index,
                              g_tax_convention_rec(l_extraordinary_conv_index),
                              l_extraordinary_conv_index,
                              l_tax_limitation_sub_rec,
                              l_tax_limitation_sub_rows,
                              l_tax_reconcile_sub_rec,
                              l_tax_reconcile_sub_rows,
                              l_reconcile_bals_sub_rec,
                              l_reconcile_bals_sub_rows,
                              l_tax_job_creation_sub_rec,
                              l_tax_job_creation_sub_rows,
                              l_tax_book_activity_sub_rec,
                              l_tax_book_activity_sub_rows,
                              l_tax_rates_sub_rec,
                              l_tax_rates_sub_rows,
                              l_short_months,
                              l_add_tax_reconcile_rec,
                              l_add_tax_reconcile_rows,
                              l_next_year);


               if l_code = -1 then
                  return - 1;
               end if;
               l_add_total_tax_reconcile_rows := l_add_tax_reconcile_rows + l_add_total_tax_reconcile_rows;
            end if;
         end loop;
         l_code := update_tax_depr(l_start_row, l_tax_depr_rows);
         l_add_total_tax_reconcile_rows := 0;
         l_add_tax_reconcile_rec.delete;
         if l_code = -1 then
            return - 1;
         end if;
      end loop;

      write_log(g_job_no, 0, 0, 'Total Calculation Rows=' || to_char(l_total_rows));
      commit;
      rtn_code := AUDIT_TABLE_PKG.setcontext('audit','yes');
      write_log(g_job_no, 0, 0, 'Finished');
      return 0;

   exception
      when others then
         code := sqlcode;
         write_log(g_job_no, 4, code, sqlerrm(code) || ', tax record : ' || l_tax_record_id || dbms_utility.format_error_backtrace);
         return 0;
   end;

   -- =============================================================================
   --  Function GET_TAX_DEPR
   -- =============================================================================
   function get_tax_depr(a_start_row out pls_integer) return integer is
      l_count             integer;
      l_save_count        pls_integer;
      i                   pls_integer;
      j                   pls_integer;
      k                   pls_integer;
      code                pls_integer;
      l_current_record_id pls_integer;
      l_current_book_id   pls_integer;
      l_year_count        pls_integer;
      type array_of_intgers is table of pls_integer;
      l_records_not_users            array_of_intgers;
      l_tax_depr_last_save_record_id pls_integer;
      l_tax_depr_last_save_book_id   pls_integer;
      l_move_row_count               pls_integer;
   begin
      if not tax_depr_cur%isopen then
         write_log(g_job_no, 1, 0, 'Tax Depreciation Rows: 0 ');
         open tax_depr_cur;
      else
         g_tax_depr_hash.delete;
         g_tax_depr_bals_hash.delete;
      end if;
      a_start_row := 1;
      fetch tax_depr_cur bulk collect
         into g_tax_depr_rec limit g_limit;

      i       := g_tax_depr_rec.first;
      l_count := g_tax_depr_rec.count;

      if l_count = 0 then
         close tax_depr_cur;

      else
         g_tax_depr_last_record_id := g_tax_depr_rec(l_count).tax_record_id;
         g_tax_depr_last_book_id   := g_tax_depr_rec(l_count).tax_book_id;
      end if;

      /** START Breakpoint for Debugger.**/
      if g_tax_depr_last_record_id = g_debug_record_id then
         WRITE_LOG(G_JOB_NO, 1, 0, ' *** tr '||g_debug_record_id||' discovered. GET_TAX_DEPR. equal to g_tax_depr_last_record_id.');
      end if;
      /** END Breakpoint for Debugger.**/

      -- save the last records that are not complete
      k := 0;
      g_tax_depr_save2_rec.delete;
      if l_count = g_limit then
         j := 1;
         for j in reverse 1 .. l_count
         loop
            if g_tax_depr_last_record_id = g_tax_depr_rec(j).tax_record_id then
               g_tax_depr_save2_rec.extend;
               k := k + 1;
               g_tax_depr_save2_rec(k) := g_tax_depr_rec(j);
            else
               exit;
            end if;
         end loop;
         l_count := l_count - k;
      else
         g_tax_depr_last_record_id := 0;
         g_tax_depr_last_book_id   := 0;
      end if;

      -- restore the save records
      j := 0;

      if g_tax_depr_save1_rec.count > 0 then
         l_save_count := g_tax_depr_save1_rec.count;
         k            := 0;
         for j in reverse 1 .. l_save_count
         loop
            g_tax_depr_rec.extend;
            g_tax_depr_rec(l_count + k + 1) := g_tax_depr_save1_rec(j);
            k := k + 1;
         end loop;
         l_count := l_count + l_save_count;
         -- we must reorganize the collection
         l_tax_depr_last_save_record_id := g_tax_depr_save1_rec(1).tax_record_id;
         l_tax_depr_last_save_book_id   := g_tax_depr_save1_rec(1).tax_book_id;
         l_move_row_count               := 0;
         for j in 1 .. l_count
         loop
            if l_tax_depr_last_save_record_id = g_tax_depr_rec(j).tax_record_id then
               l_move_row_count := l_move_row_count + 1;
            else
               exit;
            end if;
         end loop;

         for j in 1 .. l_move_row_count
         loop
            g_tax_depr_rec.extend;
            g_tax_depr_rec(l_count + j) := g_tax_depr_rec(j);
         end loop;
         l_count     := l_count + l_move_row_count;
         a_start_row := l_move_row_count + 1;
      end if;

      g_tax_depr_save1_rec := g_tax_depr_save2_rec;

      -- create an index to the data

      i                   := a_start_row;
      l_current_record_id := 0;
      l_current_book_id   := 0;
      k := 1;
      g_table_record_ids.delete;

      while i <= l_count
      loop

         if  l_current_record_id <> g_tax_depr_rec(i).tax_record_id or
            l_current_record_id = 0 then
             g_table_record_ids.extend;
             g_table_record_ids(k) := g_tax_depr_rec(i).tax_record_id;
             k := k + 1;
         end if;

         g_tax_depr_hash(to_char(g_tax_depr_rec(i).tax_record_id) || g_sep || to_char(g_tax_depr_rec(i).tax_book_id) || g_sep || to_char(g_tax_depr_rec(i).tax_year)) := i;

         if l_current_record_id = g_tax_depr_rec(i).tax_record_id and
            l_current_book_id = g_tax_depr_rec(i).tax_book_id then

            /** START Breakpoint for Debugger.**/
            if l_current_record_id = g_debug_record_id then
               WRITE_LOG(G_JOB_NO, 1, 0, ' *** tr '||g_debug_record_id||' discovered. GET_TAX_DEPR, "while" statement creating the index, the if part. ID : '||I||', L_COUNT : '||L_COUNT||', TAX_YEAR : '||g_tax_depr_rec(I).TAX_YEAR||', L_CURRENT_BOOK_ID : '||L_CURRENT_BOOK_ID);
            end if;
            /** END Breakpoint for Debugger.**/

            l_year_count := l_year_count + 1;
            g_tax_depr_bals_hash(to_char(g_tax_depr_rec(i).tax_record_id) || g_sep || to_char(g_tax_depr_rec(i).tax_book_id))(l_year_count).tax_year := g_tax_depr_rec(i)
                                                                                                                                                        .tax_year;
            g_tax_depr_bals_hash(to_char(g_tax_depr_rec(i).tax_record_id) || g_sep || to_char(g_tax_depr_rec(i).tax_book_id))(l_year_count).id := i;
         else
            l_year_count        := 1;
            l_current_record_id := g_tax_depr_rec(i).tax_record_id;
            l_current_book_id   := g_tax_depr_rec(i).tax_book_id;

            /** START Breakpoint for Debugger.**/
            if l_current_record_id = g_debug_record_id then
               WRITE_LOG(G_JOB_NO, 1, 0, ' *** tr '||g_debug_record_id||' discovered. GET_TAX_DEPR, "while" statement creating the index, the else part. ID : '||I||', L_COUNT : '||L_COUNT||', TAX_YEAR : '||g_tax_depr_rec(I).TAX_YEAR||', L_CURRENT_BOOK_ID : '||L_CURRENT_BOOK_ID);
            end if;
            /** END Breakpoint for Debugger.**/

            g_tax_depr_bals_hash(to_char(l_current_record_id) || g_sep || to_char(l_current_book_id))(l_year_count).tax_year := g_tax_depr_rec(i)
                                                                                                                                .tax_year;
            g_tax_depr_bals_hash(to_char(l_current_record_id) || g_sep || to_char(l_current_book_id))(l_year_count).id := i;
         end if;
         i := i + 1;
      end loop;

      -- g_tax_depr_rec.delete;
      write_log(g_job_no, 1, 0, 'Tax Depreciation Rows: ' || to_char(l_count - a_start_row + 1));
      return l_count;

   exception
      when others then
         code := sqlcode;
         write_log(g_job_no,
                   4,
                   code,
                   ' Tax Depr ' || sqlerrm(code) || ', Record '||l_current_record_id || ' ' || dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_depr;

   -- =============================================================================
   --  Function GET_TAX_CONVENTION
   -- =============================================================================
   function get_tax_convention return integer is
      l_count integer;
      i       pls_integer;
      code    pls_integer;
   begin
      if not tax_convention_cur%isopen then
         open tax_convention_cur;
      end if;

      fetch tax_convention_cur bulk collect
         into g_tax_convention_rec;

      i       := g_tax_convention_rec.first;
      l_count := g_tax_convention_rec.count;

      if l_count = 0 then
         close tax_convention_cur;
      end if;

      i := 1;
      while i <= l_count
      loop
         g_tax_convention_hash(g_tax_convention_rec(i).convention_id) := i;
         i := i + 1;
      end loop;

      write_log(g_job_no, 1, 0, 'Tax Convention Rows: ' || to_char(l_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         write_log(g_job_no, 4, code, sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_convention;

   -- =============================================================================
   --  Function GET_TAX_RATES
   -- =============================================================================
   function get_tax_rates return integer is
      l_count        integer;
      i              pls_integer;
      code           pls_integer;
      l_last_rate_id pls_integer;
      l_rows         pls_integer;
   begin
      if not tax_rates_cur%isopen then
         open tax_rates_cur;
      end if;

      fetch tax_rates_cur bulk collect
         into g_tax_rates_rec;

      i       := g_tax_rates_rec.first;
      l_count := g_tax_rates_rec.count;

      if l_count = 0 then
         close tax_rates_cur;
      end if;

      -- create an index to the data
      l_count        := g_tax_rates_rec.count;
      i              := 1;
      l_last_rate_id := 0;
      while i <= l_count
      loop
         if g_tax_rates_rec(i).tax_rate_id = l_last_rate_id and i <> 1 then
            l_rows := l_rows + 1;
            g_tax_rates_hash(to_char(l_last_rate_id))(l_rows).id := i;
         else
            l_rows := 1;
            l_last_rate_id := g_tax_rates_rec(i).tax_rate_id;
            g_tax_rates_hash(to_char(l_last_rate_id))(l_rows).id := i;
         end if;
         i := i + 1;
      end loop;
      write_log(g_job_no, 1, 0, 'Tax Rates Rows: ' || to_char(l_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         write_log(g_job_no, 4, code, sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_rates;

   -- =============================================================================
   --  Function GET_TAX_BOOK_ACTIVITY
   -- =============================================================================
   function get_tax_book_activity return integer is
      l_count     integer;
      i           pls_integer;
      code        pls_integer;
      l_row_count pls_integer;
      l_found     boolean;
   begin
      if not tax_book_activity_cur%isopen then
         open tax_book_activity_cur;
      end if;

      fetch tax_book_activity_cur bulk collect
         into g_tax_book_activity_rec;

      i       := g_tax_book_activity_rec.first;
      l_count := g_tax_book_activity_rec.count;

      close tax_book_activity_cur;

      -- create an index to the data
      l_count     := g_tax_book_activity_rec.count;
      i           := 1;
      l_row_count := 0;
      while i <= l_count
      loop
         begin

            /** START Breakpoint for Debugger.**/
            if g_tax_book_activity_rec(i).tax_record_id = g_debug_record_id then
               WRITE_LOG(G_JOB_NO, 1, 0, ' *** tr '||g_debug_record_id||' discovered. GET_TAX_BOOK_ACTIVITY, "while" statement creating the index. ID : '||I||', L_COUNT : '||L_COUNT||', TAX_YEAR : '||g_tax_book_activity_rec(i).tax_year||', TAX BOOK : '||g_tax_book_activity_rec(i).tax_book_id);
            end if;
            /** END Breakpoint for Debugger.**/

            l_found := g_tax_book_activity_hash(to_char(g_tax_book_activity_rec(i).tax_record_id) || g_sep || to_char(g_tax_book_activity_rec(i).tax_book_id) || g_sep || to_char(g_tax_book_activity_rec(i).tax_year))
                       .exists(1);
         exception
            when no_data_found then
               l_found := false;
         end;
         if l_found = true then
            l_row_count := 1 + l_row_count;
         else
            l_row_count := 1;
         end if;
         g_tax_book_activity_hash(to_char(g_tax_book_activity_rec(i).tax_record_id) || g_sep || to_char(g_tax_book_activity_rec(i).tax_book_id) || g_sep || to_char(g_tax_book_activity_rec(i).tax_year))(l_row_count) := i;
         i := i + 1;
      end loop;
      write_log(g_job_no, 1, 0, 'Tax Book Activity Rows: ' || to_char(l_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         write_log(g_job_no,
                   4,
                   code,
                   'Tax Book Activity i=' || to_char(i) || ' ' || sqlerrm(code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_book_activity;

   -- =============================================================================
   --  Function GET_TAX_RECONCILE
   -- =============================================================================
   function get_tax_reconcile return integer is
      l_count                  integer;
      i                        pls_integer;
      code                     pls_integer;
      l_current_record_id      pls_integer;
      l_current_include_id     pls_integer;
      l_current_bal_record_id  pls_integer;
      l_current_bal_include_id pls_integer;

      l_year_count              pls_integer;
      l_tax_reconcile_bal_count pls_integer;
      l_tax_reconcile_count     pls_integer;
      l_current_year            number(22, 2);
   begin
      if not tax_reconcile_cur%isopen then
         open tax_reconcile_cur;
      end if;

      g_tax_reconcile_index_hash.delete;
      g_tax_reconcile_hash.delete;
      g_tax_reconcile_bal_hash.delete;

      fetch tax_reconcile_cur bulk collect
         into g_tax_reconcile_rec;

      i       := g_tax_reconcile_rec.first;
      l_count := g_tax_reconcile_rec.count;

      close tax_reconcile_cur;

      -- create an index to the data
      l_count                   := g_tax_reconcile_rec.count;
      i                         := 1;
      l_current_bal_record_id   := 0;
      l_current_bal_include_id  := 0;
      l_current_record_id       := 0;
      l_current_include_id      := 0;
      l_current_year            := 0;
      l_tax_reconcile_bal_count := 0;
      while i <= l_count
      loop
         if l_current_record_id = g_tax_reconcile_rec(i).tax_record_id and
                l_current_include_id = g_tax_reconcile_rec(i).tax_include_id and
                g_tax_reconcile_rec(i).tax_year = l_current_year then
            l_tax_reconcile_count := 1 + l_tax_reconcile_count;

         else
            l_tax_reconcile_count := 1;
            l_current_record_id   := g_tax_reconcile_rec(i).tax_record_id;
            l_current_include_id  := g_tax_reconcile_rec(i).tax_include_id;
            l_current_year        := g_tax_reconcile_rec(i).tax_year;

         end if;

         g_tax_reconcile_index_hash( to_char(l_current_record_id) || g_sep ||
                                  to_char(l_current_include_id) || g_sep ||
                                  to_char(l_current_year) || g_sep ||
                                  to_char(g_tax_reconcile_rec(i).reconcile_item_id) ) := i;


         g_tax_reconcile_hash(to_char(l_current_record_id) || g_sep ||
                to_char(l_current_include_id) || g_sep ||
                to_char(l_current_year))(l_tax_reconcile_count).reconcile_item := g_tax_reconcile_rec(i).reconcile_item_id;

         g_tax_reconcile_hash(to_char(l_current_record_id) || g_sep ||
                 to_char(l_current_include_id) || g_sep ||
                 to_char(l_current_year))(l_tax_reconcile_count).id := i;

         if l_current_bal_record_id = g_tax_reconcile_rec(i).tax_record_id and
            l_current_bal_include_id = g_tax_reconcile_rec(i).tax_include_id then
            l_tax_reconcile_bal_count := l_tax_reconcile_bal_count + 1;
         else
            l_tax_reconcile_bal_count := 1;
            l_current_bal_include_id  := g_tax_reconcile_rec(i).tax_include_id;
            l_current_bal_record_id   := g_tax_reconcile_rec(i).tax_record_id;
         end if;

         g_tax_reconcile_bal_hash(to_char(l_current_bal_record_id) || g_sep ||
               to_char(l_current_bal_include_id))(l_tax_reconcile_bal_count).tax_year := g_tax_reconcile_rec(i).tax_year;

         g_tax_reconcile_bal_hash(to_char(l_current_bal_record_id) || g_sep ||
              to_char(l_current_bal_include_id))(l_tax_reconcile_bal_count).reconcile_item := g_tax_reconcile_rec(i).reconcile_item_id;

         g_tax_reconcile_bal_hash(to_char(l_current_bal_record_id) || g_sep ||
              to_char(l_current_bal_include_id))(l_tax_reconcile_bal_count).id := i;

         i := i + 1;
      end loop;
      write_log(g_job_no, 1, 0, 'Tax Reconcile Rows: ' || to_char(l_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         write_log(g_job_no, 4, code, sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_reconcile;

   -- =============================================================================
   --  Function GET_TAX_LIMITATION
   -- =============================================================================
   function get_tax_limitation return integer is
      l_count integer;
      i       pls_integer;
      code    pls_integer;
   begin
      if not tax_limitation_cur%isopen then
         open tax_limitation_cur;
      end if;

      fetch tax_limitation_cur bulk collect
         into g_tax_limitation_rec;

      i       := g_tax_limitation_rec.first;
      l_count := g_tax_limitation_rec.count;

      if l_count = 0 then
         close tax_limitation_cur;
      end if;

      write_log(g_job_no, 1, 0, 'Tax limitation Rows: ' || to_char(l_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         write_log(g_job_no, 4, code, sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_limitation;

   -- =============================================================================
   --  Function GET_TAX_INCLUDE_ACTIVITY
   -- =============================================================================
   function get_tax_include_activity return integer is
      l_count          integer;
      i                pls_integer;
      code             pls_integer;
      l_tax_book_id    pls_integer;
      l_prev_book_id   pls_integer;
      l_tax_include_id pls_integer;
      l_id_count       pls_integer;
      l_row_count      pls_integer := 0;
   begin
      i                           := 0;
      g_tax_includes_activity_rec := tax_includes_activity_type();
      l_prev_book_id              := -111111;
      open tax_includes_activity_cur;
      loop
         fetch tax_includes_activity_cur
            into l_tax_book_id, l_tax_include_id;
         exit when tax_includes_activity_cur%notfound;
         l_row_count := l_row_count + 1;
         if l_tax_book_id <> l_prev_book_id then
            i := 1 + i;
            g_tax_includes_activity_rec.extend;
            g_tax_includes_activity_rec(i).tax_book_id := l_tax_book_id;
            g_tax_includes_activity_rec(i).tax_include_ids := tax_includes_type();
            g_tax_includes_activity_rec(i).tax_include_ids.extend;
            g_tax_includes_activity_rec(i).tax_include_ids(1) := l_tax_include_id;
            l_id_count := 1;
            l_prev_book_id := l_tax_book_id;
         else
            g_tax_includes_activity_rec(i).tax_include_ids.extend;
            l_id_count := l_id_count + 1;
            g_tax_includes_activity_rec(i).tax_include_ids(l_id_count) := l_tax_include_id;

         end if;
      end loop;
      l_count := g_tax_includes_activity_rec.count;
      close tax_includes_activity_cur;
      write_log(g_job_no, 1, 0, 'Tax Include Activity Rows: ' || to_char(l_row_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         dbms_output.put_line('Tax Include Activity ' || sqlerrm(code) || ' ' ||
                              dbms_utility.format_error_backtrace);
         write_log(g_job_no, 4, code, sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_include_activity;

   -- =============================================================================
   --  Function GET_TAX_JOB_CREATION
   -- =============================================================================
   function get_tax_job_creation return integer is
      l_count          integer;
      i                pls_integer;
      code             pls_integer;
      l_row            pls_integer := 0;
      l_last_book_id   pls_integer;
      l_last_record_id pls_integer;
      l_last_tax_year  number;
   begin
      if not tax_limit2_cur%isopen then
         open tax_limit2_cur;
      end if;

      fetch tax_limit2_cur bulk collect
         into g_tax_limit2_rec;

      i       := g_tax_limit2_rec.first;
      l_count := g_tax_limit2_rec.count;

      close tax_limit2_cur;

      -- create an index to the data
      l_count          := g_tax_limit2_rec.count;
      i                := 1;
      l_last_record_id := 0;
      l_last_book_id   := 0;
      l_last_tax_year   := 0;
      while i <= l_count
      loop

         if g_tax_limit2_rec(i)
          .tax_record_id = l_last_record_id and g_tax_limit2_rec(i).tax_book_id = l_last_book_id and
           g_tax_limit2_rec(i).tax_year = l_last_tax_year then
            l_row := 1 + l_row;
         else
            l_row            := 1;
            l_last_record_id := g_tax_limit2_rec(i).tax_record_id;
            l_last_book_id   := g_tax_limit2_rec(i).tax_book_id;
            l_last_tax_year   := g_tax_limit2_rec(i).tax_year;
         end if;
         g_tax_limit2_hash(to_char(g_tax_limit2_rec(i).tax_record_id) || g_sep ||
                           to_char(g_tax_limit2_rec(i).tax_book_id) || g_sep ||
                           to_char(g_tax_limit2_rec(i).tax_year))(l_row).id := i;
         i := i + 1;
      end loop;
      write_log(g_job_no, 1, 0, 'Tax Job Creation Rows: ' || to_char(l_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         write_log(g_job_no, 4, code, sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_job_creation;

   -- =============================================================================
   --  Function UPDATE_TAX_DEPR
   -- =============================================================================
   function update_tax_depr(a_start_row pls_integer,
                            a_num_rec   pls_integer) return number as
      i      pls_integer;
      l_code pls_integer;

   begin
      for i in a_start_row .. a_num_rec
      loop
         update tax_depreciation
            set depreciation = depreciation + nvl(g_tax_depr_rec(i).calc_depr_xfer, 0),
                transfer_depr =  nvl(g_tax_depr_rec(i).calc_depr_xfer, 0)
          where tax_book_id = g_tax_depr_rec(i).tax_book_id
            and tax_record_id = g_tax_depr_rec(i).tax_record_id
            and tax_year = g_tax_depr_rec(i).tax_year;
      
          if not g_tax_depr_rec(i).transfer_month is null then
            if upper(g_tax_depr_rec(i).transfer_direction) = 'FROM' then
              update tax_depreciation_transfer
                set calc_depreciation = nvl(g_tax_depr_rec(i).calc_depr_xfer, 0)
              where tax_book_id = g_tax_depr_rec(i).tax_book_id
                and tax_record_id = g_tax_depr_rec(i).tax_record_id
                and tax_year = g_tax_depr_rec(i).tax_year;
            else
              -- The TO side is calculated as -1 * FROM side, but we want to reflect the TO amount on tax_depreciation_transfer
              update tax_depreciation_transfer x
                set calc_depreciation = (decode(g_tax_depr_rec(i).transfer_fraction,0,0,(
                      select -1 * (nvl(g_tax_depr_rec(i).calc_depr_xfer, 0) 
                                * (-1 / g_tax_depr_rec(i).transfer_fraction) - nvl(g_tax_depr_rec(i).calc_depr_xfer, 0))
                                * decode(total.amount, 0, 1, (from_amt.amount / total.amount))
                        from (
                                select c.tax_transfer_id, t.calc_depreciation amount
                                  from tax_transfer_control c, tax_depreciation_transfer t
                                 where c.from_trid = t.tax_record_id
                                   and c.tax_year = t.tax_year
                                   and c.tax_transfer_id = t.tax_transfer_id
                                   and t.tax_book_id = g_tax_depr_rec(i).tax_book_id
                                   and t.tax_year = g_tax_depr_rec(i).tax_year
                                   and c.to_trid = g_tax_depr_rec(i).tax_record_id
                             ) from_amt,
                             (
                                select sum(distinct t.calc_depreciation) amount
                                  from tax_transfer_control c, tax_depreciation_transfer t
                                 where c.from_trid = t.tax_record_id
                                   and c.tax_year = t.tax_year
                                   and t.tax_book_id = g_tax_depr_rec(i).tax_book_id
                                   and t.tax_year = g_tax_depr_rec(i).tax_year
                                   and c.to_trid = g_tax_depr_rec(i).tax_record_id
                             ) total
                       where x.tax_transfer_id = from_amt.tax_transfer_id)))    
              where tax_book_id = g_tax_depr_rec(i).tax_book_id
                and tax_record_id = g_tax_depr_rec(i).tax_record_id
                and tax_year = g_tax_depr_rec(i).tax_year
                and exists (
                    select 1
                        from (
                                select c.tax_transfer_id, t.calc_depreciation amount
                                  from tax_transfer_control c, tax_depreciation_transfer t
                                 where c.from_trid = t.tax_record_id
                                   and c.tax_year = t.tax_year
                                   and c.tax_transfer_id = t.tax_transfer_id
                                   and t.tax_book_id = g_tax_depr_rec(i).tax_book_id
                                   and t.tax_year = g_tax_depr_rec(i).tax_year
                                   and c.to_trid = g_tax_depr_rec(i).tax_record_id
                             ) from_amt
                       where x.tax_transfer_id = from_amt.tax_transfer_id);
            end if;
          end if;
      end loop;
      write_log(g_job_no, 3, 0, 'Update Tax Depreciation Rows=' || to_char(a_num_rec - a_start_row + 1));
      write_log(g_job_no, 3, 0, 'Update Tax Depreciation Transer Rows=' || to_char(a_num_rec - a_start_row + 1));
      return 0;
   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'Update Tax Depr ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return -1;
   end update_tax_depr;

  
   -- ************************************** Calc ************************************************
   function calc(a_tax_depr_index            integer,
                 a_tax_depr_bals_index       integer,
                 a_tax_depr_bals_rows        pls_integer,
                 a_tax_convention_rec        tax_convention_cur%rowtype,
                 a_tax_convention_rows       pls_integer,
                 a_extraord_convention_rec   tax_convention_cur%rowtype,
                 a_extraord_convention_rows  pls_integer,
                 a_tax_limitation_sub_rec    type_tax_limitation_rec,
                 a_tax_limitation_rows       pls_integer,
                 a_tax_reconcile_sub_rec     in out nocopy type_tax_reconcile_rec,
                 a_tax_reconcile_rows        in out nocopy pls_integer,
                 a_reconcile_bals_sub_rec    in out nocopy type_tax_reconcile_rec,
                 a_reconcile_bals_rows       in out nocopy pls_integer,
                 a_tax_job_creation_sub_rec  type_tax_limit2_rec,
                 a_tax_job_creation_rows     pls_integer,
                 a_tax_book_activity_sub_rec type_tax_book_activity_rec,
                 a_tax_book_activity_rows    pls_integer,
                 a_tax_rates_sub_rec         type_tax_rates_rec,
                 a_tax_rates_rows            pls_integer,
                 a_short_months              in number,
                 a_add_tax_reconcile_rec     in out nocopy type_tax_reconcile_rec,
                 a_add_tax_reconcile_rows    in out nocopy integer,
                 a_nyear                     number) return number is

      l_tax_job_creation_ordered    type_tax_limit2_rec;
      l_add_tax_reconcile_bals_flag boolean;
      l_net                         character(10);
      l_method                      pls_integer;
      l_code                        pls_integer;
      l_rlife                       pls_integer;
      l_wlife                       pls_integer;
      l_switched                    pls_integer;
      l_hconv                       pls_integer;
      l_trid                        pls_integer;
      l_jrow                        pls_integer;
      l_calc_future_years           pls_integer;
      l_calced                      pls_integer;
      l_depr_deduction              pls_integer;
      l_compare_rate                pls_integer;
      l_reconcile_index             pls_integer;

      l_life      binary_double;
      l_add_ratio binary_double;
      
      l_reserve            binary_double := 0;

      l_rem_life                   binary_double := 0;
      l_flife                      binary_double := 0;
      l_reserve_at_switch_init     binary_double;
      l_basis_reduction_percent    binary_double;
      l_tax_credit_base            binary_double;
      l_tax_credit_amount          binary_double;
      l_tax_credit_basis_reduction binary_double;

      l_years_used            binary_double := 0;
      l_months_used           binary_double := 0;
      l_yr1_fraction          binary_double := 0;
      l_yr2_fraction          binary_double := 0;
      l_orig_short_months     binary_double := 0;
      l_short_year_net_adjust binary_double := 0;
      l_yr1_rate              binary_double := 0;
      l_yr2_rate              binary_double := 0;

      l_scoop boolean;

      l_tax_limitation binary_double := 0;
      i                pls_integer;
      j                pls_integer;
      k                pls_integer;
      l_num            pls_integer;
      l_pos            pls_integer;
      l_vintage        pls_integer;
      l_jc             pls_integer;
      r                pls_integer;
      l_found          boolean;
      l_i              pls_integer;

      l_ratio     binary_double := 0;
      l_ratio1    binary_double := 0;
      l_change    binary_double := 0;
      l_amount    binary_double := 0;
      l_depr_rate binary_double;
      type num_ary200_type is table of binary_double index by binary_integer;
      l_tax_rates num_ary200_type;

      l_rounding           pls_integer;
      
      l_salvage_conv         binary_double := 0;
      
      l_tax_reconcile_beg      num_ary200_type;
      l_tax_reconcile_act      num_ary200_type;
      l_tax_reconcile_end      num_ary200_type;
      l_tax_reconcile_ret      num_ary200_type;
      l_tax_reconcile_transfer num_ary200_type;
      l_tax_reconcile_type     num_ary200_type;
      
      type pls_integer_ary_type is table of pls_integer index by binary_integer;
      l_input_retire_ind pls_integer_ary_type;

      --* EXEC SQL BEGIN DECLARE SECTION;
      l_rowid                  binary_double;
      l_tax_reconcile_beg_val  binary_double;
      l_tax_reconcile_act_val  binary_double;
      l_tax_reconcile_end_val  binary_double;
      l_tax_year               binary_double := 0;
      l_tax_year_1             binary_double := 0;
      l_tax_book_id            binary_double := 0;
      l_tax_record_id          binary_double := 0;
      l_reconcile_item_id      binary_double := 0;
      l_book_balance           binary_double := 0;
      l_tax_balance            binary_double := 0;
      l_remaining_life         binary_double := 0;
      l_depreciable_base       binary_double := 0;
      l_sl_reserve             binary_double := 0;

      l_depreciation            binary_double := 0;
      l_number_months_beg       binary_double := 0;
      l_number_months_end       binary_double := 0;

      --* Transfer Variables

      l_book_balance_transfer        binary_double;
      l_tax_balance_transfer         binary_double;
      l_accum_reserve_transfer       binary_double;
      l_fixed_depreciable_base_trans binary_double;
      l_sl_reserve_transfer          binary_double;
      l_reserve_at_switch_transfer   binary_double;
      l_quantity_transfer            binary_double;
      l_additions_transfer           binary_double;
      l_calc_depr_transfer           binary_double := 0;
      l_transfer_month               number(22,0);
      l_xfer_fraction                number(22,8) := 0;
      l_transfer_direction           character(4);

      /* EXEC SQL END DECLARE SECTION;        */
     
      l_rows  pls_integer;
      l_nyear pls_integer;
      l_short_months         binary_double;
      l_msg varchar2(100);

   begin

      l_short_months := a_short_months;
      
      /* move the items in the tax_depreciation table to variables
      get rid of the statements we don't end up needing  */
      l_book_balance_transfer        := nvl(g_tax_depr_rec(a_tax_depr_index).book_balance_xfer, 0);
      l_tax_balance_transfer         := nvl(g_tax_depr_rec(a_tax_depr_index).tax_balance_xfer, 0);
      l_accum_reserve_transfer       := nvl(g_tax_depr_rec(a_tax_depr_index).accum_reserve_xfer, 0);
      l_fixed_depreciable_base_trans := nvl(g_tax_depr_rec(a_tax_depr_index)
                                            .fixed_depreciable_base_xfer,
                                            0);
      l_sl_reserve_transfer          := nvl(g_tax_depr_rec(a_tax_depr_index).sl_reserve_xfer, 0);                                      
      l_reserve_at_switch_transfer   := nvl(g_tax_depr_rec(a_tax_depr_index).reserve_at_switch_xfer,
                                            0);                                      
      l_additions_transfer           := nvl(g_tax_depr_rec(a_tax_depr_index).additions_xfer, 0);
      l_transfer_month               := nvl(g_tax_depr_rec(a_tax_depr_index).transfer_month, 0);
      l_transfer_direction           := g_tax_depr_rec(a_tax_depr_index).transfer_direction;
      
      if l_transfer_month is null then
        return 0;
      end if;
 
      a_add_tax_reconcile_rows := 0;

      l_tax_year               := g_tax_depr_rec(a_tax_depr_index).tax_year;
   
      l_add_ratio := 1;
                                            
      --* Get The Tax Conventions
      if (a_tax_convention_rows = 0) then
         l_salvage_conv     := 0;
      else
         l_salvage_conv         := a_tax_convention_rec.salvage_id;
      end if;                                      

      --* get the tax rates, and lives and conventions for remaining life rates.
      l_num := a_tax_rates_rows;

      for i in 1 .. l_num
      loop
         l_tax_rates(i) := a_tax_rates_sub_rec(i).rate;
      end loop;
      l_net      := a_tax_rates_sub_rec(1).net_gross;
      l_rounding := nvl(a_tax_rates_sub_rec(1).rounding_convention, 0);
      l_life     := nvl(a_tax_rates_sub_rec(1).life, 0);
      l_rlife    := nvl(a_tax_rates_sub_rec(1).remaining_life_plan, 0);
      l_method   := nvl(a_tax_rates_sub_rec(1).start_method, 0);
      l_vintage  := nvl(g_tax_depr_rec(a_tax_depr_index).vintage_year, 0);
      l_switched := nvl(a_tax_rates_sub_rec(1).switched_year, 0);
      l_hconv    := nvl(a_tax_rates_sub_rec(1).half_year_convention, 0);

      --* upper is important for good match

      l_net := upper(l_net);

      -- which depreciation rates should we use for this tax year, if not on remaining life.

      if (trunc(l_tax_year) <> l_vintage) then
         if (l_number_months_beg = -1 or l_number_months_beg = 0) then
            l_number_months_beg := (round(l_tax_year) - l_vintage) * 12.0;
         end if;
      end if;

      l_orig_short_months := l_short_months;

      if (trunc(l_tax_year) = l_vintage) then
         if (l_number_months_beg = -1 or l_number_months_beg = 0) then
            if (l_hconv <> 1) then
               l_short_months := 12;
            else
               l_short_months := l_short_months / 2;
            end if;
         end if;
      end if;

      l_number_months_end := l_number_months_beg + l_short_months;
      l_years_used        := trunc((l_number_months_beg / 12));
      --l_months_used              =  (l_number_months_beg % 12);
      l_months_used := mod(l_number_months_beg, 12.0);
      --yr1_fraction                :=  mid(12.0 - l_months_used,l_short_month);
      if (12.0 - l_months_used) > l_short_months then
         l_yr1_fraction := l_short_months;
      else
         l_yr1_fraction := 12.0 - l_months_used;
      end if;
      l_yr2_fraction := l_short_months - l_yr1_fraction;

      l_yr1_fraction := l_yr1_fraction / 12.0;
      l_yr2_fraction := l_yr2_fraction / 12.0;
      
      -- Set the Partial Year Transfer Fraction
      if trunc(l_tax_year) = trunc(l_transfer_month/100) then
        if (l_transfer_month - (trunc(l_transfer_month/100) * 100)) = 1 then
          if l_transfer_direction = 'FROM' then
            l_xfer_fraction := 0;
          else
            l_xfer_fraction := 0;
          end if;
        else
          if l_transfer_direction = 'FROM' then
            l_xfer_fraction := (l_transfer_month - 1 - (trunc(l_transfer_month/100) * 100))/12.0;
          else
            l_xfer_fraction := -1 * (l_transfer_month - 1 - (trunc(l_transfer_month/100) * 100))/12.0;
          end if;
        end if;
      else
        l_xfer_fraction := 1;
      end if;

      i := l_years_used + 1;

      --i =  l_tax_year - l_vintage + 1;

      l_scoop := false;
      if (i != 1) then
         if (i >= a_tax_rates_rows and (l_months_used + l_short_months) >= 12 and
            l_accum_reserve_transfer <> 0) then
            l_scoop := true;
         end if;
         if (i >= (a_tax_rates_rows + 1) and l_accum_reserve_transfer <> 0) then
            l_scoop := true;
         end if;
      else
         if (l_tax_rates(i) = 0 and l_accum_reserve_transfer <> 0) then
            l_scoop := true;
         end if;
      end if;
      --* If a life rate, then there are only 2 rates, the first year and all subsequent years
      --* there us never scoop

      if (l_method = 6) then
         if (i > 1) then
            i := 2;
         end if;
         l_scoop := false;
      end if;

      --* If a vintage rate, match the tax_year with the designated year

      if (l_method = 7) then
         l_num := a_tax_rates_rows;
         for ti in 1 .. l_num
         loop
            --maint 38243
            if trunc(l_tax_year) = a_tax_rates_sub_rec(ti).year then
               i := ti;
               exit;
            end if;
         end loop;

         if (i > l_num) then
            i := a_tax_rates_rows + 1;
         else
            -- IF this is the first year, then replace the first year rates.
            if (trunc(l_tax_year) = l_vintage and l_number_months_beg = 0) then
               l_tax_rates(i) := a_tax_rates_sub_rec(i).rate1 * (l_orig_short_months / 12);
            end if;
         end if;

         l_scoop := false;
         if (i = a_tax_rates_rows and (l_months_used + l_short_months) >= 12 and
            l_accum_reserve_transfer <> 0) then
            l_scoop := true;
         end if;
      end if;

      if (l_rlife <> 1 ) then
         if ((i > a_tax_rates_rows) or (i < 1)) then
            l_depr_rate := 0;
         else
            if (l_method = 6 and i = 2) then
               --  rate should just be the 2nd year of a like rate
               l_depr_rate := l_tax_rates(i) * (l_yr1_fraction + l_yr2_fraction);
            else
               l_depr_rate := l_tax_rates(i) * l_yr1_fraction;
            end if;

            if (i + 1 <= a_tax_rates_rows) then -- Maint-34525  fixed the wrong rate used for Maint-29675
               l_depr_rate := l_depr_rate + (l_tax_rates(i + 1) * l_yr2_fraction);
            end if;
            if (i > a_tax_rates_rows) then
               l_yr1_rate := 0;
            else
               l_yr1_rate := l_tax_rates(i);
            end if;
            if ((i + 1) > a_tax_rates_rows) then
               l_yr2_rate := 0;
            else
               l_yr2_rate := l_tax_rates(i + 1);
            end if;
            if (trim(l_net) = 'GRNET') then
               if (i < l_switched) then
                  l_net := 'GROSS';
               else
                  l_net := 'NET';
               end if;
            end if;
            if (trim(l_net) = 'NETGR') then
               if (i = l_switched) then
                  l_reserve_at_switch_init     := l_accum_reserve_transfer;
                  l_reserve_at_switch_transfer := l_reserve_at_switch_transfer + l_reserve_at_switch_init;
               end if;
               if (i < l_switched) then
                  l_net := 'NET';
               else
                  l_net := 'GROSS';
               end if;
            end if;
         end if;
      end if;

      --* calculate the depreciation rate to use this tax year, if on remaining life plan.

      if (l_rlife = 1) then
         l_net := 'NET';
         if (i < 1) then
            l_depr_rate := 0;
            l_yr1_rate  := 0;
            l_yr2_rate  := 0;
         else
            if (l_tax_balance_transfer = 0) then
               --* check for divide by zero
               l_rem_life := 0;
            else
               l_rem_life := l_life * (l_sl_reserve_transfer / l_tax_balance_transfer);
            end if;
            if (l_rem_life > l_life) then
               l_rem_life := l_life;
            end if;

            if (l_rem_life < 1) then
               l_rem_life := 1;
            end if;
            --* sl case is easy

            if (l_method != 2) then
               --* sl case
               if (l_rem_life != 0) then
                  --* div by 0 protect
                  l_depr_rate := (1.0 / l_rem_life) * (l_short_months / 12.0);
                  l_yr1_rate  := 1 / l_rem_life;
                  l_yr2_rate  := 1 / l_rem_life;
               else
                  l_depr_rate := 0;
                  l_yr1_rate  := 0;
                  l_yr2_rate  := 0;
               end if;
               --*syd case requires syd calc
            else
               --* syd case

               l_wlife := trunc(l_rem_life); --* whole number part
               l_flife := l_rem_life - l_wlife; --* fractional part

               if (((l_wlife = 0) and (l_flife = 0)) or l_wlife = -1) then
                  --* div by zero check
                  l_depr_rate := 0;
                  l_yr1_rate  := 0;
                  l_yr2_rate  := 0;
               else
                  l_depr_rate := l_rem_life /
                                 (((l_wlife + 1) * l_wlife / 2) + ((l_wlife + 1) * l_flife));
                  l_yr1_rate  := l_depr_rate;
                  l_yr2_rate  := l_depr_rate;
                  l_depr_rate := l_depr_rate * (l_short_months / 12.0);
               end if;

            end if; --* syd case
         end if;

         if (abs(l_depr_rate) > 1) then
            l_depr_rate := 1;
         end if;
         if (abs(l_yr1_rate) > 1) then
            l_yr1_rate := 1;
         end if;
         if (abs(l_yr2_rate) > 1) then
            l_yr2_rate := 1;
         end if;
         l_depr_rate := round(l_depr_rate, 5);
         l_yr1_rate  := round(l_yr1_rate, 5);
         l_yr2_rate  := round(l_yr2_rate, 5);

         --* dont scoop remaining life plan stuff if the depreciation rate is still alive
         --* otherwise let the scoop decision stand as before.

         if (l_depr_rate <> 0) then
            l_scoop := false;
         end if;
      end if; --* rlife = 1

      --*
      --* establish tax limitations (e.g. luxury auto if applicable)
      --*
      l_tax_limitation := -1;

      if (a_tax_limitation_rows <> 0) then
         /*dw_tax_limitation.setsort('year a')
         dw_tax_limitation.sort() */

         i := trunc(l_tax_year) - l_vintage + 1;

         if (i > a_tax_limitation_rows or i < 1) then
            l_tax_limitation := -1;
         else
            l_tax_limitation := nvl(a_tax_limitation_sub_rec(i).limitation, 0) *
                                (l_short_months / 12.0);
            l_compare_rate   := nvl(a_tax_limitation_sub_rec(i).compare_rate, 0);
            if (l_compare_rate != 1) then
               l_compare_rate := 0;
            end if;
         end if;
      end if;

      -- Zero out any activity in calculated basis difference before starting
      for j in 1 .. a_tax_reconcile_rows
      loop
         if (a_tax_reconcile_sub_rec(j).calced = 1) then
            a_tax_reconcile_sub_rec(j).basis_amount_activity := 0;
         end if;
      end loop;
      
      --*
      --* process current year activity:
      --*
      if l_quantity_transfer is null then
         l_quantity_transfer := 0;
      end if;

      --*
      --* put the book to tax basis reconciliation activity in an array and also
      --* put the beginning tax to book differences in an array. pay attention to sign
      --* book difference is negative, tax difference is positive
      --*


      for i in 1 .. a_tax_reconcile_rows
      loop
         l_tax_reconcile_beg(i) := nvl(a_tax_reconcile_sub_rec(i).basis_amount_beg,0) + nvl(a_tax_reconcile_sub_rec(i)
                                   .basis_amount_transfer,0);
         l_tax_reconcile_act(i) := nvl(a_tax_reconcile_sub_rec(i).basis_amount_activity,0);
         l_tax_reconcile_ret(i) := nvl(a_tax_reconcile_sub_rec(i).basis_amount_input_retire,0);
         l_tax_reconcile_transfer(i) := nvl(a_tax_reconcile_sub_rec(i).basis_amount_transfer,0);
         l_input_retire_ind(i) := nvl(a_tax_reconcile_sub_rec(i).input_retire_ind,0);

         if (l_input_retire_ind(i) != 0) then
            l_input_retire_ind(i) := 1;
         end if;

         if instr(upper(nvl(a_tax_reconcile_sub_rec(i).reconcile_item_type, ' ')), 'CREDIT') = 0 then
            l_tax_reconcile_type(i) := 1;
         else
            l_tax_reconcile_type(i) := 0;
         end if;

      end loop;
      
      l_i := i;

      --*
      --* Determine Tax Basis Additions.  If any tax book reconciling items are input,
      --* treat as adjustments if there are no book additions
      --*
      i := a_tax_reconcile_rows; /*upperbound(l_tax_reconcile_beg) */

      for j in 1 .. i
      loop
         -- if(fabs(book_addition) > 1)
         l_additions_transfer := l_additions_transfer + l_tax_reconcile_act(j);
         --   else
      --       adjustments := adjustments + tax_reconcile_act[j - 1];
      end loop;

      --* Set The Salvage Convention
      case
         when trunc(l_salvage_conv) = 5 then
            l_add_ratio          := .5;
         else 
            l_add_ratio := 1;
      end case;
      
      l_depreciable_base := l_tax_balance_transfer + l_additions_transfer * l_add_ratio;

      if (l_tax_balance_transfer <> 0) then
         l_depreciable_base := l_depreciable_base -
                               (l_reserve_at_switch_transfer * l_depreciable_base / l_tax_balance_transfer);
      end if;

      if (l_net = 'NET') then
         l_reserve := l_accum_reserve_transfer;
      end if;

      if ((l_reserve * l_depreciable_base) >= 0 and abs(l_reserve) >= abs(l_depreciable_base)) then
         l_depreciable_base := 0;
      else
         if (l_net = 'NET') then
            l_depreciable_base := l_depreciable_base - l_reserve;
         end if;
      end if;

      if (l_fixed_depreciable_base_trans <> 0) then
         l_depreciable_base := l_fixed_depreciable_base_trans;
      end if;

      if (l_net = 'NET') then
         l_calc_depr_transfer := l_depreciable_base * l_yr1_rate * l_yr1_fraction;
         l_depreciable_base  := l_depreciable_base - l_calc_depr_transfer;
         l_calc_depr_transfer := l_calc_depr_transfer +
                                (l_depreciable_base * l_yr2_rate * l_yr2_fraction);
         l_calc_depr_transfer := l_calc_depr_transfer * l_xfer_fraction;
         if ((l_yr1_rate * l_yr1_fraction) + (l_yr2_rate * l_yr2_fraction)) <> 0 then
            -- div by 0 check
            l_depreciable_base := l_calc_depr_transfer /
                                  ((l_yr1_rate * l_yr1_fraction) + (l_yr2_rate * l_yr2_fraction));
         end if;
      else
         l_calc_depr_transfer := l_depreciable_base * l_depr_rate * l_xfer_fraction;
      end if;
      --*
      --* Check tax limitation e.g. luxury autos
      --*

      if (l_tax_limitation != -1 and l_quantity_transfer > 0) then
         --  if( (l_quantity * tax_limitation) < l_calc_depreciation)
         if (l_compare_rate = 1) then
            if ((l_quantity_transfer * l_tax_limitation) < l_calc_depr_transfer) then
               l_calc_depr_transfer := l_quantity_transfer * l_tax_limitation;
            end if;
         else
            l_calc_depr_transfer := l_quantity_transfer * l_tax_limitation;
         end if;
      end if;

      if (abs(l_calc_depr_transfer) < .0000001) then
         l_calc_depr_transfer := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).calc_depr_xfer := l_calc_depr_transfer;
      
      g_tax_depr_rec(a_tax_depr_index).transfer_fraction := l_xfer_fraction;
      
      -- Save audit records
      insert into tax_depr_transfer_audit (
      tax_depr_index, tax_record_id, tax_year, tax_book_id, short_months, transfer_month, add_ratio, salvage_conv, 
      tax_rates_rows, net_gross, rlife, start_method, vintage, yr1_fraction, yr2_fraction, xfer_fraction, depr_rate, 
      i, tax_balance_transfer, additions_transfer, depreciable_base, reserve, calc_depr_transfer, transfer_direction, 
      calc_xfer_fraction,
      user_id, time_stamp)
      values (
      a_tax_depr_index, g_tax_depr_rec(a_tax_depr_index).tax_record_id, l_tax_year, g_tax_depr_rec(a_tax_depr_index).tax_book_id,
      l_short_months, l_transfer_month, l_add_ratio, l_salvage_conv,
      a_tax_rates_rows, l_net, l_rlife, l_method, l_vintage, l_yr1_fraction, l_yr2_fraction, l_xfer_fraction, l_depr_rate,
      l_i, l_tax_balance_transfer, l_additions_transfer, l_depreciable_base, l_reserve, l_calc_depr_transfer, l_transfer_direction, 
      decode(l_transfer_direction,'FROM',round((l_transfer_month - 1 - (trunc(l_transfer_month/100) * 100))/12.0,8),
                                         round(-1 * (l_transfer_month - 1 - (trunc(l_transfer_month/100) * 100))/12.0,8)),
      user, sysdate);

      return 0;

   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'Calc Record ID=' || to_char(g_tax_depr_rec(a_tax_depr_index).tax_record_id) || '  ' || l_msg ||
                   sqlerrm(l_code) || ' ' || dbms_utility.format_error_backtrace);
         return - 1;
   end calc;

   -- =============================================================================
   --  Procedure SET_SESSION_PARAMETER
   -- =============================================================================
   procedure set_session_parameter is
      cursor session_param_cur is
         select parameter, value, type
           from pp_session_parameters
          where lower(users) = 'all'
             or lower(users) = 'depr';
      l_count pls_integer;
      i       pls_integer;
      type type_session_param_rec is table of session_param_cur%rowtype;
      l_session_param_rec type_session_param_rec;
      l_code              pls_integer;
      l_sql               varchar2(200);
   begin

      open session_param_cur;
      fetch session_param_cur bulk collect
         into l_session_param_rec;
      close session_param_cur;

      l_count := l_session_param_rec.count;
      for i in 1 .. l_count
      loop
         if l_session_param_rec(i).type = 'number' or l_session_param_rec(i).type = 'boolean' then
            l_sql := 'alter session set ' || l_session_param_rec(i).parameter || ' = ' || l_session_param_rec(i)
                    .value;
         elsif l_session_param_rec(i).type = 'string' then
            l_sql := 'alter session set ' || l_session_param_rec(i).parameter || ' = ''' || l_session_param_rec(i)
                    .value || '''';
         end if;
         execute immediate l_sql;
         write_log(g_job_no,
                   0,
                   0,
                   'Set Session Parameter ' || l_session_param_rec(i).parameter || ' = ' || l_session_param_rec(i)
                   .value);
      end loop;
      return;
   exception
      when no_data_found then
         return;
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   5,
                   l_code,
                   'Set Session Parameter ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return;
   end set_session_parameter;

   -- =============================================================================
   --  Procedure WRITE_LOG
   -- =============================================================================
   procedure write_log(a_job_no     number,
                       a_error_type number,
                       a_code       number,
                       a_msg        varchar2) as
      pragma autonomous_transaction;
      l_code     pls_integer;
      l_msg      varchar2(2000);
      l_cur_ts   timestamp;
      l_date_str varchar2(100);
   begin
      l_cur_ts   := current_timestamp;
      l_date_str := to_char(sysdate, 'HH24:MI:SS');
      l_msg      := a_msg;
      dbms_output.put_line(l_msg);
      insert into tax_job_log
         (job_no, line_no, log_date, error_type, error_code, msg)
      values
         (a_job_no, g_line_no, sysdate, a_error_type, a_code, l_msg);
      commit;
      g_line_no := g_line_no + 1;
      return;
   exception
      when others then
         l_code := sqlcode;
         dbms_output.put_line('Write Log ' || sqlerrm(l_code) || ' ' ||
                              dbms_utility.format_error_backtrace);
         return;
   end write_log;

end TAX_DEPR_CALC_PARTIAL;
/
