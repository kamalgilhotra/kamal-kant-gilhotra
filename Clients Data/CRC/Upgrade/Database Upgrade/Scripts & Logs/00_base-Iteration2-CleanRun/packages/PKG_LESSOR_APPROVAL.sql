create or replace package pkg_lessor_approval AS
   /*
   ||============================================================================
   || Application: PowerPlan
   || Object Name: PKG_LESSOR_APPROVAL
   || Description:
   ||============================================================================
   || Copyright (C) 2017 by PowerPlan Inc, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 2017.1.0 10/02/2015 Shane "C" Ward Original Version
   || 2017.1.0 10/04/2015 Shane "C" Ward Add ILR Workflows
   || 2017.3.0 31/01/2018 Anand R        Add JEs to Lessor ILR
   || 2017.3.0 02/07/2018 J Sisouphanh   Add Function to Derecognized CPR Assets
   || 2017.3.0 02/19/2018 Anand R        PP-50513 Remove ILR approval logic from Send
   || 2017.3.0 02/22/2018 Anand R        PP-505376 Add JEs for Direct Finance ILR approval
   || 2017.3.0 05/07/2018 Anand R        PP-51092 Do not restrict Initial direct cost by In service date
   || 2018.1.0 09/13/2018 Anand R        PP-50961 update ilr revision statuses after approval
   || 2018.1.0 11/14/2018 Anand R        PP-52545 Add JEs when re measurement revision is approved
   || 2018.2.1 04/22/2018 B. Beck	     Add Asset Re-Recognition
   || 2018.2.1 05/13/2018 B. Beck	     Add IFRS Sales Type remeasurement JEs.
   ||============================================================================
   */
   G_PKG_VERSION varchar(35) := '2018.2.1.0';

   function F_APPROVE_MLA(A_LEASE_ID number,
                          A_REVISION number) return number;

   function F_REJECT_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_SEND_MLA(A_LEASE_ID number,
                       A_REVISION number) return number;

   function F_UNREJECT_MLA(A_LEASE_ID number,
                           A_REVISION number) return number;

   function F_UNSEND_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID number,
                                  A_REVISION number) return number;

   function F_APPROVE_ILR(A_ILR_ID   number,
                          A_REVISION NUMBER,
                          A_SEND_JES BOOLEAN DEFAULT TRUE) return number;

   function F_REJECT_ILR(A_ILR_ID   number,
                         A_REVISION number) return number;

   function F_SEND_ILR(A_ILR_ID   number,
                       A_REVISION number) return number;
					   
   function F_SEND_ILR_NO_COMMIT(A_ILR_ID   in number,
                                 A_REVISION in number,
                                 A_STATUS   OUT VARCHAR2) return number;

   function F_UNREJECT_ILR(A_ILR_ID   number,
                           A_REVISION number) return number;

   function F_UNSEND_ILR(A_ILR_ID   number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID   number,
                                  A_REVISION number) return number;


   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   OUT VARCHAR2,
                                    A_SEND_JES IN BOOLEAN DEFAULT TRUE) return number;

   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2,
                                    A_LOG in boolean,
                                    a_is_transfer IN BOOLEAN DEFAULT FALSE,
                                    A_SEND_JES IN BOOLEAN DEFAULT TRUE) return number;

   function F_INVOICE_APPROVE(A_COMPANY_ID in number,
                              A_MONTH      in date,
                              A_END_LOG    in number:=null) return varchar2;

   function F_SEND_INVOICE(A_INVOICE_ID number) return number;

   function F_UPDATE_WORKFLOW_INVOICE(A_INVOICE_ID number) return number;

   function F_REJECT_INVOICE(A_INVOICE_ID number) return number;

   function F_UNREJECT_INVOICE(A_INVOICE_ID number) return number;

   function F_APPROVE_INVOICE(A_INVOICE_ID number) return number;

   function F_UNSEND_INVOICE(A_INVOICE_ID number) return number;

   function F_CREATE_JE_FOR_ILR(A_ILR_ID   in number,
                                A_REVISION in number) return number;
								
   /*****************************************************************************
   * Function: F_CREATE_JE_RECLASS_ILR
   * PURPOSE: creates JEs for set of books that have a classification change
   *			from Operating to either Direct Fincance or Sales Type
   * PARAMETERS:
   *   a_ilr_id: The ILR ID for the ILR for which to retrieve information
   *   a_revision: The revision of the ILR for which to retrieve information
   *   a_prior_revision: The prior revision of the ILR for which to retrieve information
   * RETURNS: Number
   ******************************************************************************/
   function F_CREATE_JE_RECLASS_ILR(A_ILR_ID   in number,
                                A_REVISION in number,
								A_PRIOR_REVISION in number) return number;

   function F_CREATE_JE_REMEASUREMENT_ILR(A_ILR_ID in number,
                                          A_REVISION in number,
										  A_PRIOR_REVISION in number) return number;

   PROCEDURE p_mass_approve_ilrs( a_ilrs t_lsr_ilr_id_revision_tab,
                                  a_send_jes_0_1 number);


   FUNCTION F_GET_PAY_START_DATE
	  (A_ILR_ID in number, A_REVISION in number)
	 RETURN date;

   /*****************************************************************************
   * Function: f_has_class_change
   * PURPOSE: Looks up and returns 1 if this set of books has a classification change
   * PARAMETERS:
   *   a_ilr_id: The ILR ID for the ILR for which to retrieve information
   *   a_revision: The revision of the ILR for which to retrieve information
   *   a_prior_revision: The prior revision of the ILR for which to retrieve information
   *   a_set_of_books_id: The set of books id to check
   * RETURNS: 0 if no classification change.  1 if a classification change
   ******************************************************************************/
   FUNCTION f_has_class_change(a_ilr_id NUMBER, a_revision NUMBER, a_prior_revision NUMBER, a_set_of_books_id NUMBER)
   RETURN NUMBER DETERMINISTIC;

end PKG_Lessor_approval;
/
   --**************************************************************************
--                            Initialize Package
--**************************************************************************

CREATE OR REPLACE PACKAGE BODY pkg_lessor_approval AS

   /*
   ||============================================================================
   || Application: PowerPlan
   || Object Name: PKG_LESSOR_APPROVAL
   || Description:
   ||============================================================================
   || Copyright (C) 2017 by PowerPlan Inc, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 2017.1.0 10/02/2015 Shane "C" Ward Original Version
   || 2017.3.0 02/07/2018 J Sisouphanh   Add Function to Derecognized CPR Assets
   ||============================================================================
   */


  TYPE t_kickout IS RECORD (ilr_id lsr_ilr_schedule_kickouts.ilr_id%TYPE,
                            revision lsr_ilr_schedule_kickouts.revision%TYPE,
                            message lsr_ilr_schedule_kickouts.message%TYPE,
                            occurrence_id lsr_ilr_schedule_kickouts.occurrence_id%type);
  TYPE t_kickout_tab IS TABLE OF t_kickout;

  L_ILR_ID number;

  /*****************************************************************************
  * Procedure: p_log_kickouts
  * PURPOSE: Logs kickouts to the lsr_ilr_approvals_kickouts table
  * PARAMETERS:
  *   a_kickouts: The kickouts to log to the table
  * NOTES: Uses autonomous transaction
  ******************************************************************************/
  PROCEDURE p_log_kickouts(a_kickouts t_kickout_tab) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    l_ids t_number_22_2_tab;
    l_count number;
  BEGIN
    l_count := a_kickouts.COUNT;

    SELECT lsr_ilr_approval_kickouts_seq.NEXTVAL
    BULK COLLECT INTO l_ids
    FROM dual
    WHERE l_count <> 0
    CONNECT BY LEVEL <= l_count;

    FORALL I IN 1..a_kickouts.COUNT
    INSERT INTO lsr_ilr_approval_kickouts(kickout_id,
                                          ilr_id,
                                          revision,
                                          message,
                                          occurrence_id)
    VALUES( l_ids(I),
            a_kickouts(I).ilr_id,
            a_kickouts(I).revision,
            a_kickouts(I).message,
            a_kickouts(i).occurrence_id);

    FOR I IN 1..a_kickouts.COUNT
    LOOP
      pkg_pp_log.p_write_message('ILR ID/Revision: ' || to_char(a_kickouts(I).ilr_id || '/' || to_char(a_kickouts(I).revision) || ' - ' || a_kickouts(I).message));
    END LOOP;

    COMMIT;
  END p_log_kickouts;


   --**************************************************************************
   --                            P_SET_ILR_ID
   --**************************************************************************

   procedure P_SET_ILR_ID(A_ILR_ID number) is

   begin
      L_ILR_ID := A_ILR_ID;
   end P_SET_ILR_ID;

   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************
   procedure P_LOGERRORMESSAGE(A_ILR_ID in number,
                               A_MSG    in varchar2) is
      pragma autonomous_transaction;
   begin
      update LS_PEND_TRANSACTION set ERROR_MESSAGE = A_MSG where ILR_ID = A_ILR_ID;

      commit;
   end P_LOGERRORMESSAGE;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************

  function F_EXECUTE_IMMEDIATE(A_SQLS in varchar2, A_START_LOG in number:=null) return varchar2
    is
    counter number;
    begin
    if nvl(a_start_log,0) = 1 then
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
    end if;

    if lower(trim(pkg_pp_system_control.f_pp_system_control('Lease Debug Execute Immediate'))) = 'yes' then
      pkg_pp_log.p_write_message(a_sqls);
    end if;

    execute immediate A_SQLS;

    return 'OK';

    exception when others then
      return 'Error Executing SQL: ' || sqlerrm || ' ' || sqlcode || chr(13) || chr(10) || a_sqls;
    end f_execute_immediate;

	/*****************************************************************************
	* Function: f_is_remeasure
	* PURPOSE: Looks up and returns 1 if this is a remeasurement.
	* PARAMETERS:
	*   a_ilr_id: The ILR ID for the ILR for which to retrieve information
	*   a_revision: The revision of the ILR for which to retrieve information
	* RETURNS: Number
	******************************************************************************/
   	FUNCTION f_is_remeasure(a_ilr_id NUMBER, a_revision NUMBER)
	RETURN NUMBER DETERMINISTIC
	IS
		l_is_remeasure NUMBER;
	BEGIN
		--Check if the ILR is a remeasurement revision.
        SELECT COUNT(1) INTO l_is_remeasure
        FROM LSR_ILR_OPTIONS LIO
        WHERE LIO.ILR_ID = A_ILR_ID
          AND LIO.REVISION = A_REVISION
          AND LIO.REMEASUREMENT_DATE IS NOT NULL;

		return l_is_remeasure;
	END f_is_remeasure;


	/*****************************************************************************
    * Function: f_has_class_change
    * PURPOSE: Looks up and returns 1 if this set of books has a classification change
	* PARAMETERS:
	*   a_ilr_id: The ILR ID for the ILR for which to retrieve information
	*   a_revision: The revision of the ILR for which to retrieve information
	*   a_prior_revision: The prior revision of the ILR for which to retrieve information
	*   a_set_of_books_id: The set of books id to check
	* RETURNS: 0 if no classification change.  1 if a classification change
	******************************************************************************/
	FUNCTION f_has_class_change(a_ilr_id NUMBER, a_revision NUMBER, a_prior_revision NUMBER, a_set_of_books_id NUMBER)
	RETURN NUMBER DETERMINISTIC
	IS
		l_class_change NUMBER;
	BEGIN
		-- only check for changes if the old revision has a fasb cap type of Operating.
		select case when count(1) > 0 then 1 else 0 end as classification_change
		into l_class_change
		from 
		(
			  select lsr_ilr.ilr_id, fasb.fasb_cap_type_id as approved_fasb_cap_type_id, 
					fasb.set_of_books_id, cur_fasb.fasb_cap_type_id
			  from lsr_fasb_type_sob fasb, lsr_ilr_options, lsr_ilr, 
				lsr_ilr_options cur_opt, lsr_fasb_type_sob cur_fasb
			  where fasb.cap_type_id = lsr_ilr_options.lease_cap_type_id
			  and lsr_ilr_options.revision = a_prior_revision
			  and lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
			  and lsr_ilr_options.ilr_id = a_ilr_id
			  and cur_opt.ilr_id = a_ilr_id
			  and cur_opt.revision = a_revision
			  and cur_fasb.cap_type_id = cur_opt.lease_cap_type_id
			  and cur_fasb.set_of_books_id = fasb.set_of_books_id
			  and cur_fasb.fasb_cap_type_id <> fasb.fasb_cap_type_id
			  and cur_fasb.set_of_books_id = a_set_of_books_id
			  and cur_opt.remeasurement_date is not null
		);

		return l_class_change;
	END f_has_class_change;

   /*--**************************************************************************
                               F_DERECOGNIZE_CPR_ASSETS
  * PURPOSE: Automatically derecognizes CPR assets when the ILR is approved and goes In Service
  * PARAMETERS:
  *   a_ilr_id: ilr_id of the ILR for which to process the schedule
  *   a_revision: Revision of the ILR for which to process the schedule
  * RETURNS: 1
   --**************************************************************************/

   function F_DERECOGNIZE_CPR_ASSETS(A_ILR_ID   in number, A_REVISION in number) return number is

     L_MSG              varchar2(2000);
     L_GL_JE_CODE       varchar(35);
     L_RTN              number;
     L_DEPR_IND         number;
     L_RESERVE_AMT      number;
     L_GAINLOSS_AMT     number;
     L_GL_POSTING_MO_YR date;
     L_MIN_MONTH        date;
     L_DEPR_MIN_MONTH   date;
     L_PEND_TRANS_ID    pend_transaction.pend_trans_id%TYPE;
	 L_IS_REMEASURE		number(1,0);

   begin
     PKG_PP_LOG.P_WRITE_MESSAGE('  Derecognizing CPR Assets for ILR: ' || TO_CHAR(A_ILR_ID) || ' Revision: ' || TO_CHAR(A_REVISION));

     L_MSG := '  Getting the minimum open month for Lessor';
     PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

	 L_IS_REMEASURE := 1;
	
	 -- if this is a remeasurement, use that as the accounting month and min month
	 select trunc(options.remeasurement_date, 'MM')
	 into L_MIN_MONTH
	 from lsr_ilr_options options
	 where options.ilr_id = a_ilr_id
	 and options.revision = a_revision
	 ;
	 
	 if L_MIN_MONTH is null THEN
		 L_IS_REMEASURE := 0;
		 
		 select MIN(ACCOUNTING_MONTH)
		 into L_GL_POSTING_MO_YR
		 from CPR_CONTROL, LSR_ILR ILR
		 where CPR_CONTROL.COMPANY_ID = ILR.COMPANY_ID
		   and ILR.ILR_ID = A_ILR_ID
		   and ACCOUNTING_MONTH > (
			   select max(accounting_month) from cpr_control
			   where company_id = ILR.COMPANY_ID
			   and cpr_closed is not null
			   );

		 if L_GL_POSTING_MO_YR is null then
		   L_MSG := '    Error getting Minimum Accounting Month from CPR Control: Null';
		   PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
		   return -1;
		 end if;
		 
		 -- Get the minimum month on the schedule to determine if we need to book a reversal for depreciation
		 L_MSG := 'Getting the first month of the schedule';
		 select MIN(lis.month)
		   into L_MIN_MONTH
		   from LSR_ILR_SCHEDULE LIS
		  where LIS.ILR_ID = A_ILR_ID
			and LIS.REVISION = A_REVISION;

		 if L_MIN_MONTH is null then
		   L_MSG := 'Error getting first month of the ILR schedule';
		   PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
		   return -1;
		 end if;
	 else
		L_GL_POSTING_MO_YR := L_MIN_MONTH;
	 end if;
     
     L_MSG := '    Accounting Month: ' || TO_CHAR(L_GL_POSTING_MO_YR);
     PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

     
     -- Get the min month where cpr depr has not yet been approved
     L_MSG := '  Getting the minimum month where depreciation has not yet been approved in the CPR';
     select MIN(ACCOUNTING_MONTH)
     into L_DEPR_MIN_MONTH
     from CPR_CONTROL, LSR_ILR ILR
     where CPR_CONTROL.COMPANY_ID = ILR.COMPANY_ID
       and ILR.ILR_ID = A_ILR_ID
       and ACCOUNTING_MONTH > (
           select max(accounting_month) from cpr_control
           where company_id = ILR.COMPANY_ID
           and depr_approved is not null
           );
           
     if L_DEPR_MIN_MONTH is null then
       L_MSG := '    Error getting the minimum month where depreciation has not yet been approved in the CPR';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
       return -1;
     end if;

     SELECT s.gl_je_code into L_GL_JE_CODE
     FROM standard_journal_entries s, gl_je_control g
     WHERE g.process_id = 'LSR Derecognition'
       and g.je_id = s.je_id;

      L_MSG := '  GL JE Code: ' || L_GL_JE_CODE;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

     if L_GL_JE_CODE is null then
       L_MSG := '  Error getting GL JE Code from Standard Journal Entries: Null';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
       return -1;
     end if;

     --FOR LOOP for each CPR Asset
      for L_CPRS in (
									select distinct asset.cpr_asset_id,
												 asset.description,
												 cpr.work_order_number as Work_Order,
												 cpr.accum_quantity * -1 as Posting_Quantity,
												 cpr.accum_cost * -1 as Posting_Amount,
												 subledger_indicator,
												 cpr.retirement_unit_id,
												 cpr.utility_account_id,
												 cpr.bus_segment_id,
												 cpr.func_class_id,
												 cpr.sub_account_id,
												 cpr.asset_location_id,
												 cpr.company_id,
												 cpr.eng_in_service_year,
												 cpr.depr_group_id,
												 cpr.gl_account_id,
												 cpr.long_description,
												 cpr.property_group_id,
												 case when L_IS_REMEASURE = 0 then ilr.est_in_svc_date else L_MIN_MONTH end as est_in_svc_date
									from lsr_asset asset
												INNER JOIN cpr_ledger cpr ON asset.cpr_asset_id = cpr.asset_id
												INNER JOIN lsr_ilr ilr ON asset.ilr_id = ilr.ilr_id
												INNER JOIN lsr_ilr_options options on options.ilr_id = ilr.ilr_id and options.revision = asset.revision
			                  INNER JOIN lsr_fasb_type_SOB sob on sob.cap_type_id = options.lease_cap_type_id
			                  INNER JOIN lsr_fasb_cap_type cap_type on cap_type.fasb_cap_type_id = sob.fasb_cap_type_id
									where asset.ilr_id = A_ILR_ID
									and asset.revision = a_revision
									  and cap_type.fasb_cap_type_id in (2,3)
                     )
       loop

           --Read the depreciation indicator
           L_MSG:='  Pulling Depreciation Indicator';
           PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
           begin
             select S.DEPRECIATION_INDICATOR
             into L_DEPR_IND
             from SUBLEDGER_CONTROL S
             where S.SUBLEDGER_TYPE_ID = L_CPRS.Subledger_Indicator;

           exception
             when NO_DATA_FOUND then
              L_MSG := '    Depreciation Indicator From SUBLEDGER_CONTROL not found.';
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              L_MSG := '  Continuing to the next CPR Asset.';
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             continue;
           end;

           --Calculate Reserve Depending on Subledger Indicator
           IF L_DEPR_IND = 3 THEN
             -- If L_MONTH < L_DEPR_MIN_MONTH then we need to reverse the depr expense and salvage expense for 
             -- the months between L_MONTH and L_GL_POSTING_MO_YR - 1
             if L_MIN_MONTH < L_DEPR_MIN_MONTH then
               -- Add the Depr Expense records to depr_activity
               insert into depr_activity (
                 depr_activity_id, depr_group_id, set_of_books_id, gl_post_mo_yr,
                 user_id, user_id1, user_id2, depr_activity_code_id, amount, 
                 description, long_description, 
                 gl_post_flag, asset_id, source)
               select pwrplant1.nextval, L_CPRS.DEPR_GROUP_ID, d.set_of_books_id, L_DEPR_MIN_MONTH,
                      user, user, user, 1, -1 * d.amount,
                      'Lessor Backdated Commencement', 'Lessor Backdated Commencement', 
                      0, d.asset_id, 'Lessor Backdated Commencement'
                 from (select asset_id, set_of_books_id, sum(curr_depr_expense) amount
                         from cpr_depr
                        where asset_id = L_CPRS.CPR_ASSET_ID
                          and gl_posting_mo_yr between L_MIN_MONTH and add_months(L_DEPR_MIN_MONTH, -1) 
                        group by asset_id, set_of_books_id) d
                where d.amount <> 0 ;
                       
               -- Add the Salvage Expense records to depr_activity
               insert into depr_activity (
                 depr_activity_id, depr_group_id, set_of_books_id, gl_post_mo_yr,
                 user_id, user_id1, user_id2, depr_activity_code_id, amount, 
                 description, long_description, 
                 gl_post_flag, asset_id, source)
               select pwrplant1.nextval, L_CPRS.DEPR_GROUP_ID, d.set_of_books_id, L_DEPR_MIN_MONTH,
                      user, user, user, 15, -1 * d.amount,
                      'Lessor Backdated Commencement', 'Lessor Backdated Commencement', 
                      0, d.asset_id, 'Lessor Backdated Commencement'
                 from (select asset_id, set_of_books_id, sum(salvage_expense) amount
                         from cpr_depr
                        where asset_id = L_CPRS.CPR_ASSET_ID
                          and gl_posting_mo_yr between L_MIN_MONTH and add_months(L_DEPR_MIN_MONTH, -1) 
                        group by asset_id, set_of_books_id) d
                where d.amount <> 0 ;
                  
               -- Update CPR_DEPR
               -- DEPR_EXP_ADJUST
               update cpr_depr d
                  set depr_exp_adjust = depr_exp_adjust + (
                        select a.amount
                          from depr_activity a
                         where a.asset_id = d.asset_id
                           and a.set_of_books_id = d.set_of_books_id
                           and a.gl_post_mo_yr = d.gl_posting_mo_yr
                           and a.source = 'Lessor Backdated Commencement'
                           and a.depr_activity_code_id = 1
                           and a.asset_id = L_CPRS.CPR_ASSET_ID
                           and a.gl_post_mo_yr = L_DEPR_MIN_MONTH)
                where asset_id = L_CPRS.CPR_ASSET_ID
                  and gl_posting_mo_yr = L_DEPR_MIN_MONTH
                  and exists (
                        select 1
                          from depr_activity a
                         where a.asset_id = d.asset_id
                           and a.set_of_books_id = d.set_of_books_id
                           and a.gl_post_mo_yr = d.gl_posting_mo_yr
                           and a.source = 'Lessor Backdated Commencement'
                           and a.depr_activity_code_id = 1
                           and a.asset_id = L_CPRS.CPR_ASSET_ID
                           and a.gl_post_mo_yr = L_DEPR_MIN_MONTH);
               
               -- SALVAGE_EXP_ADJUST         
               update cpr_depr d
                  set salvage_exp_adjust = salvage_exp_adjust + (
                        select a.amount
                          from depr_activity a
                         where a.asset_id = d.asset_id
                           and a.set_of_books_id = d.set_of_books_id
                           and a.gl_post_mo_yr = d.gl_posting_mo_yr
                           and a.source = 'Lessor Backdated Commencement'
                           and a.depr_activity_code_id = 15
                           and a.asset_id = L_CPRS.CPR_ASSET_ID
                           and a.gl_post_mo_yr = L_DEPR_MIN_MONTH)
                where asset_id = L_CPRS.CPR_ASSET_ID
                  and gl_posting_mo_yr = L_DEPR_MIN_MONTH
                  and exists (
                        select 1
                          from depr_activity a
                         where a.asset_id = d.asset_id
                           and a.set_of_books_id = d.set_of_books_id
                           and a.gl_post_mo_yr = d.gl_posting_mo_yr
                           and a.source = 'Lessor Backdated Commencement'
                           and a.depr_activity_code_id = 15
                           and a.asset_id = L_CPRS.CPR_ASSET_ID
                           and a.gl_post_mo_yr = L_DEPR_MIN_MONTH);
               
               -- Update depr_ledger
               -- DEPR_EXP_ADJUST
               update depr_ledger d
                  set depr_exp_adjust = depr_exp_adjust + (
                        select a.amount
                          from depr_activity a
                         where a.depr_group_id = d.depr_group_id
                           and a.set_of_books_id = d.set_of_books_id
                           and a.gl_post_mo_yr = d.gl_post_mo_yr
                           and a.source = 'Lessor Backdated Commencement'
                           and a.depr_activity_code_id = 1
                           and a.asset_id = L_CPRS.CPR_ASSET_ID
                           and a.gl_post_mo_yr = L_DEPR_MIN_MONTH)
                where exists (
                        select 1
                          from depr_activity a
                         where a.depr_group_id = d.depr_group_id
                           and a.set_of_books_id = d.set_of_books_id
                           and a.gl_post_mo_yr = d.gl_post_mo_yr
                           and a.source = 'Lessor Backdated Commencement'
                           and a.depr_activity_code_id = 1
                           and a.asset_id = L_CPRS.CPR_ASSET_ID
                           and a.gl_post_mo_yr = L_DEPR_MIN_MONTH);
               
               -- SALVAGE_EXP_ADJUST  
               update depr_ledger d
                  set salvage_exp_adjust = salvage_exp_adjust + (
                        select a.amount
                          from depr_activity a
                         where a.depr_group_id = d.depr_group_id
                           and a.set_of_books_id = d.set_of_books_id
                           and a.gl_post_mo_yr = d.gl_post_mo_yr
                           and a.source = 'Lessor Backdated Commencement'
                           and a.depr_activity_code_id = 15
                           and a.asset_id = L_CPRS.CPR_ASSET_ID
                           and a.gl_post_mo_yr = L_DEPR_MIN_MONTH)
                where exists (
                        select 1
                          from depr_activity a
                         where a.depr_group_id = d.depr_group_id
                           and a.set_of_books_id = d.set_of_books_id
                           and a.gl_post_mo_yr = d.gl_post_mo_yr
                           and a.source = 'Lessor Backdated Commencement'
                           and a.depr_activity_code_id = 15
                           and a.asset_id = L_CPRS.CPR_ASSET_ID
                           and a.gl_post_mo_yr = L_DEPR_MIN_MONTH);  
             end if;  
           
           
             begin
               L_MSG:='    Depreciation Indicator = 3: Calculate Reserve Calculation via Individual Depreciation Method';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               select (cpr_depr.beg_reserve_month +
                      cpr_depr.retirements +
                      cpr_depr.depr_exp_adjust +
                      cpr_depr.salvage_dollars +
                      cpr_depr.salvage_exp_adjust + 
                      cpr_depr.cost_of_removal +
                      cpr_depr.other_credits_and_adjust +
                      cpr_depr.gain_loss +
                      cpr_depr.reserve_trans_in +
                      cpr_depr.reserve_trans_out) into L_RESERVE_AMT
               from cpr_depr
               where cpr_depr.set_of_books_id = 1
                     and cpr_depr.asset_id = L_CPRS.CPR_ASSET_ID
                     and cpr_depr.gl_posting_mo_yr = L_DEPR_MIN_MONTH;

               L_MSG := '    Reserve Amount Calculation:' || TO_CHAR(L_RESERVE_AMT);
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);


             exception
               when NO_DATA_FOUND then
                L_MSG := '    No data found while calculating the reserve for ILR:' || TO_CHAR(A_ILR_ID);
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                L_MSG := '    Continuing to the next CPR Asset.';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               continue;
             end;

           ELSE

             begin
               L_MSG:='    Depreciation Indicator = 0. Calculate Reserve Calculation via Group Depreciation Method';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               select depr_res_allo_factors.factor into L_RESERVE_AMT
               from depr_res_allo_factors
               where L_CPRS.depr_group_id = depr_res_allo_factors.depr_group_id
                and depr_res_allo_factors.set_of_books_id = 1
                and to_number(to_char(L_CPRS.eng_in_service_year,'YYYY')) = depr_res_allo_factors.vintage
                and depr_res_allo_factors.month = L_GL_POSTING_MO_YR;
             exception
               when NO_DATA_FOUND then
                L_MSG := '    No data found while calculating reserve through the table depr_res_allo_factors for ILR:' || TO_CHAR(A_ILR_ID);
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
			 			   L_MSG := '  Continuing to the next CPR Asset.';
			         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
							 continue;
						 end;

					END IF;

					 L_GAINLOSS_AMT := (L_CPRS.POSTING_AMOUNT * -1) - L_RESERVE_AMT;
					 L_MSG := '    Gain Loss Calculation:' || TO_CHAR(L_GAINLOSS_AMT);
	         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

			 		L_PEND_TRANS_ID := pwrplant1.NEXTVAL;

					 --Insert into Pending Transaction
					 INSERT INTO pend_transaction(
									pend_trans_id,
									ldg_asset_id,
									ldg_activity_id,
									gl_posting_mo_yr,
									work_order_number,
									activity_code,
									user_id1,
									gl_je_code,
									description,
									posting_quantity,
									posting_amount,
									retire_method_id,
									posting_status,
									reserve,
									gain_loss,
									misc_description,
									retirement_unit_id,
									utility_account_id,
									bus_segment_id,
									func_class_id,
									sub_account_id,
									asset_location_id,
									company_id,
									ferc_activity_code,
									disposition_code,
									gl_account_id,
									subledger_indicator,
									in_service_year,
									long_description,
									property_group_id,
									ldg_depr_group_id,
									adjusted_reserve,
									gain_loss_reversal,
									books_schema_id)
						SELECT
									L_PEND_TRANS_ID,
									L_CPRS.CPR_ASSET_ID,
									NULL,
									L_GL_POSTING_MO_YR,
									L_CPRS.WORK_ORDER,
									'SAGL',
									user,
									L_GL_JE_CODE,
									'Asset Derecognition',
									L_CPRS.POSTING_QUANTITY,
									L_CPRS.POSTING_AMOUNT,
									1,
									1,
									L_RESERVE_AMT,
									L_GAINLOSS_AMT,
									'Lessor ILR:' || TO_CHAR(A_ILR_ID) || '; REVISION:' || TO_CHAR(A_REVISION),
									L_CPRS.RETIREMENT_UNIT_ID,
									L_CPRS.UTILITY_ACCOUNT_ID,
									L_CPRS.BUS_SEGMENT_ID,
									L_CPRS.FUNC_CLASS_ID,
									L_CPRS.SUB_ACCOUNT_ID,
									L_CPRS.ASSET_LOCATION_ID,
									L_CPRS.COMPANY_ID,
									2 as FERC_ACTIVITY_CODE,
									19 as DISPOSITION_CODE,
									L_CPRS.GL_ACCOUNT_ID,
									L_CPRS.SUBLEDGER_INDICATOR,
									L_CPRS.EST_IN_SVC_DATE,
									L_CPRS.LONG_DESCRIPTION,
									L_CPRS.PROPERTY_GROUP_ID,
									L_CPRS.DEPR_GROUP_ID,
									L_RESERVE_AMT,
									0,
									1
						FROM DUAL;

						PKG_PP_LOG.P_WRITE_MESSAGE('  Records inserted into Pend_Transaction: ' || TO_CHAR(sql%rowcount));
						PKG_PP_LOG.P_WRITE_MESSAGE('  CPR Asset ID: ' || TO_CHAR(L_CPRS.CPR_ASSET_ID));

						INSERT INTO pend_basis (pend_trans_id, basis_1, basis_2, basis_3, basis_4, basis_5, basis_6, basis_7, basis_8, basis_9, basis_10,
												basis_11, basis_12, basis_13, basis_14, basis_15, basis_16, basis_17, basis_18, basis_19, basis_20,
												basis_21, basis_22, basis_23, basis_24, basis_25, basis_26, basis_27, basis_28, basis_29, basis_30,
												basis_31, basis_32, basis_33, basis_34, basis_35, basis_36, basis_37, basis_38, basis_39, basis_40,
												basis_41, basis_42, basis_43, basis_44, basis_45, basis_46, basis_47, basis_48, basis_49, basis_50,
												basis_51, basis_52, basis_53, basis_54, basis_55, basis_56, basis_57, basis_58, basis_59, basis_60,
												basis_61, basis_62, basis_63, basis_64, basis_65, basis_66, basis_67, basis_68, basis_69, basis_70)
						SELECT L_PEND_TRANS_ID, -1 * basis_1, -1 * basis_2, -1 * basis_3, -1 * basis_4, -1 * basis_5, -1 * basis_6, -1 * basis_7, -1 * basis_8, -1 * basis_9, -1 * basis_10,
												-1 * basis_11, -1 * basis_12, -1 * basis_13, -1 * basis_14, -1 * basis_15, -1 * basis_16, -1 * basis_17, -1 * basis_18, -1 * basis_19, -1 * basis_20,
												-1 * basis_21, -1 * basis_22, -1 * basis_23, -1 * basis_24, -1 * basis_25, -1 * basis_26, -1 * basis_27, -1 * basis_28, -1 * basis_29, -1 * basis_30,
												-1 * basis_31, -1 * basis_32, -1 * basis_33, -1 * basis_34, -1 * basis_35, -1 * basis_36, -1 * basis_37, -1 * basis_38, -1 * basis_39, -1 * basis_40,
												-1 * basis_41, -1 * basis_42, -1 * basis_43, -1 * basis_44, -1 * basis_45, -1 * basis_46, -1 * basis_47, -1 * basis_48, -1 * basis_49, -1 * basis_50,
												-1 * basis_51, -1 * basis_52, -1 * basis_53, -1 * basis_54, -1 * basis_55, -1 * basis_56, -1 * basis_57, -1 * basis_58, -1 * basis_59, -1 * basis_60,
												-1 * basis_61, -1 * basis_62, -1 * basis_63, -1 * basis_64, -1 * basis_65, -1 * basis_66, -1 * basis_67, -1 * basis_68, -1 * basis_69, -1 * basis_70
						FROM cpr_ldg_basis
						WHERE asset_id = L_CPRS.CPR_ASSET_ID;

						PKG_PP_LOG.P_WRITE_MESSAGE('  Records inserted into PEND_BASIS: ' || TO_CHAR(sql%rowcount));
			 end loop;

			 return 1;

		 exception
				when others then
					 rollback;
					 RAISE_APPLICATION_ERROR(-20000, 'Error Auto Derecognizing CPR in function F_DERECOGNIZE_CPR_ASSETS()' || ': ' || SQLERRM);
					 return -1;

	 end F_DERECOGNIZE_CPR_ASSETS;

	/***************************************************************************
	*			   F_RERECOGNIZE_CPR_ASSETS
	* PURPOSE: Automatically rerecognizes CPR assets when the ILR is approved and is reclassified from
	*				Sales Type or Direct Finance to Operating
	* PARAMETERS:
	*   a_ilr_id: ilr_id of the ILR for which to process the schedule
	*   a_revision: Revision of the ILR for which to process the schedule
	*	a_date: the remeasurement date
	**************************************************************************/
	function F_RERECOGNIZE_CPR_ASSETS(A_ILR_ID   in number, A_REVISION in number, a_date date) return number
	IS
		l_asset_rerecognition_tbl pkg_lessor_common.asset_rerecognition_table;
		l_numRows number;
	BEGIN
		SELECT 
			asset.cpr_asset_id AS ldg_asset_id, -99 /* this prevents Post from recalculating basis amounts */ AS ldg_activity_id, arc.ldg_depr_group_id, arc.books_schema_id, arc.retirement_unit_id,
			arc.utility_account_id, arc.bus_segment_id, arc.func_class_id, arc.sub_account_id, arc.asset_location_id, arc.gl_account_id, ilr.company_id, To_Date(To_Char(a_date, 'YYYYMM'), 'YYYYMM') AS gl_posting_mo_yr,
			arc.subledger_indicator, arc.activity_code, arc.work_order_number, arc.posting_quantity, SYS_CONTEXT ('USERENV', 'SESSION_USER') AS user_id1,
			sch.beg_receivable + sd.beg_unguaranteed_residual as posting_amount, To_Date(To_Char(a_date, 'YYYYMM'), 'YYYYMM') AS in_service_year, 
			'Asset Rerecognition' AS description,
			' LSR Remeasurement - Re-recognition of PP asset' as long_description,
			arc.property_group_id, arc.retire_method_id, 1 AS posting_status, 0 AS cost_of_removal, 0 AS salvage_cash, 0 AS salvage_returns, 0 AS reserve,
			'Lessor ILR:' || ilr.ilr_id || '; REVISION:' || a_revision AS misc_description, arc.ferc_activity_code, arc.serial_number, 0 AS reserve_credits, 0 AS gain_loss_reversal, arc.disposition_code,
			act.asset_activity_id, arc.posting_amount AS orig_posting_amount, sch.set_of_books_id
		bulk collect into l_asset_rerecognition_tbl
		from lsr_ilr_schedule sch
		join lsr_ilr_schedule_sales_direct sd 
			on sd.ilr_id = sch.ilr_id 
			and sd.revision = sch.revision 
			and sd.set_of_books_id = sch.set_of_books_id
			and sd.month = sch.month
		join lsr_ilr ilr
		 	on ilr.ilr_id = sch.ilr_id
		join lsr_asset asset 
			on asset.ilr_id = sch.ilr_id 
			and asset.revision = sch.revision
		JOIN cpr_ledger cpr
			ON asset.cpr_asset_id = cpr.asset_id
		JOIN cpr_activity act
			ON asset.cpr_asset_id = act.asset_id
		JOIN pend_transaction_archive arc
			ON act.activity_status = arc.pend_trans_id
		WHERE sch.ilr_id = a_ilr_id
		AND sch.revision = a_revision
		and asset.cpr_asset_id is not null
		AND act.asset_activity_id = (
			SELECT Max(act2.asset_activity_id)
			FROM cpr_activity act2
			WHERE act.asset_id = act2.asset_id
			AND act.description = 'Asset Derecognition' )
		and sch.month = a_date
		and sch.set_of_books_id =
		(
			select min(aa.set_of_books_id)
			from lsr_ilr_schedule aa
			where aa.ilr_id = a_ilr_id
			AND aa.revision = a_revision
			and aa.month = a_date
		)
		;
		
		l_numRows := SQL%ROWCOUNT;
	
		if l_numRows > 0 then
			pkg_lessor_common.p_rerecognize_cpr_assets(l_asset_rerecognition_tbl);
		end if;
	
		return 1;
	
		EXCEPTION
			WHEN OTHERS THEN
			IF SQLCODE BETWEEN -20999 AND -20000 THEN
				ROLLBACK;
				pkg_pp_log.p_write_message(SQLERRM);
				RAISE;
				--PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
			ELSIF SQLCODE = 100 THEN
				ROLLBACK;
				pkg_pp_log.p_write_message('Error creating Lessor Asset Re-recognition pending transactions - ' || SQLERRM || f_get_call_stack);
				Raise_Application_Error(-20000, SubStr('Error creating Lessor Asset Re-recognition pending transactions - ORA-01403 - NO DATA FOUND' || Chr(10) || f_get_call_stack, 1, 2000));
			ELSE
				ROLLBACK;
				pkg_pp_log.p_write_message('Error creating Lessor Asset Re-recognition pending transactions - ORA' || SQLCODE || ' - ' || SQLERRM || Chr(10) || f_get_call_stack);
				RAISE;
			END IF;
	END F_RERECOGNIZE_CPR_ASSETS;
	


   --**************************************************************************
   --                            F_APPROVE_ILR_NO_COMMIT
   --**************************************************************************

   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   OUT VARCHAR2,
                                    A_SEND_JES IN BOOLEAN DEFAULT TRUE) return number is
      RTN    number;
      MY_STR varchar2(2000);

   begin
      rtn := F_APPROVE_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, A_STATUS, TRUE, FALSE, A_SEND_JES);
      return rtn;
   exception
      when others then
         P_LOGERRORMESSAGE(A_ILR_ID, A_STATUS);

         return -1;
   end F_APPROVE_ILR_NO_COMMIT;


   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2,
                                    A_LOG in boolean,
                                    a_is_transfer IN BOOLEAN DEFAULT FALSE,
                                    A_SEND_JES IN BOOLEAN DEFAULT TRUE) return number is
      RTN    number;
      MY_STR varchar2(2000);
	    L_COUNT number;
	    L_LOCK_CHECK number;
	    PAY_START date;
	    OPEN_MONTH date;
		L_IS_RECLASS NUMBER;
		L_PRIOR_CURRENT_REVISION NUMBER;

   begin

      PKG_PP_LOG.P_START_LOG(f_get_pp_process_id('Lessor - ILR Commencement'));

      A_STATUS := 'APPROVE ILR:';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := '   ILR:' || TO_CHAR(A_ILR_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := '   REVISION: ' || TO_CHAR(A_REVISION);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

	  A_STATUS := 'Getting open month';
	  PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
	  select min(gl_posting_mo_yr)
	   into OPEN_MONTH
	  from lsr_process_control
	  where company_id = (select company_id from ls_ilr where ilr_id = A_ILR_ID)
	  and lsr_closed is null;

	  PKG_PP_LOG.P_WRITE_MESSAGE('   Open Month: ' || to_char(OPEN_MONTH, 'YYYY-MM-DD'));

	  A_STATUS := 'Checking Pay Start Date';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

	  PAY_START := PKG_LESSOR_APPROVAL.F_GET_PAY_START_DATE(A_ILR_ID,A_REVISION);

     A_STATUS := 'Checking for Month End Close in Progress';
     PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
     select count(*)
      into L_LOCK_CHECK
     from lsr_process_control
     where company_id = (select company_id from ls_ilr where ilr_id = A_ILR_ID)
     and lsr_closed is not null and open_next is null;


    if L_LOCK_CHECK > 0 then
	  -- Stop if the pay start date  is before the current open month
      if trunc(PAY_START, 'mm') < trunc(OPEN_MONTH, 'mm') then
        A_STATUS := 'The Payment Start Date (with Payment Shift) of ' || to_char(PAY_START, 'MM/YYYY') || ' is before the Current Open Month of ' || to_char(OPEN_MONTH, 'MM/YYYY') || ' and a prior month end close is in progress. Complete Month End Close, then Route for Approval';
        PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
        return -1;
      end if;
     end if;

      A_STATUS := 'Updating LSR_ILR_APPROVAL1';

      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LSR_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := '   Getting prior current revision.';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

	select current_revision
	into L_PRIOR_CURRENT_REVISION
	from LSR_ILR
	where ilr_id = a_ilr_id;

      A_STATUS := 'Updating LSR_ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      update LSR_ILR L set ILR_STATUS_ID = 2, CURRENT_REVISION = A_REVISION where ILR_ID = A_ILR_ID;

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      --Derecognize CPR Assets for any Sales Type or Direct Finance ILR--
      SELECT COUNT(cap_type.fasb_cap_type_id)
          INTO l_count
          FROM lsr_ilr
         INNER JOIN lsr_ilr_options options
            ON options.ilr_id = lsr_ilr.ilr_id
         INNER JOIN lsr_fasb_type_sob sob
            ON sob.cap_type_id = options.lease_cap_type_id
         INNER JOIN lsr_fasb_cap_type cap_type
            ON cap_type.fasb_cap_type_id = sob.fasb_cap_type_id
         WHERE lsr_ilr.ilr_id = A_ILR_ID
           AND lsr_ilr.current_revision = A_REVISION
           AND cap_type.description IN ('Sales Type', 'Direct Finance');

	   if l_count > 0 then
			RTN := F_DERECOGNIZE_CPR_ASSETS(A_ILR_ID, A_REVISION);
			If RTN = -1 then
       			PKG_PP_LOG.P_WRITE_MESSAGE('F_DERECOGNIZE_CPR_ASSETS returned an error.');
				return -1;
			end if;
	   end if;

      IF A_SEND_JES THEN
		L_COUNT := f_is_remeasure(a_ilr_id, a_revision);
          
		-- if not a remeasure, then create for new ILR
		IF l_count = 0 THEN
			PKG_PP_LOG.P_WRITE_MESSAGE('Processing Commencement ILRs.');
           RTN := F_CREATE_JE_FOR_ILR(A_ILR_ID, A_REVISION);
           If RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE('F_CREATE_JE_FOR_ILR returned an error.');
              return -1;
           end if;
        ELSE
			PKG_PP_LOG.P_WRITE_MESSAGE('Processing Remeasurements.');
			
			-- process remeasurement / relcass ILRs.  The check for relcass vs remeasure is done by Set of book
			-- and is part of the where clause of the Set of books cursor
			RTN := F_CREATE_JE_RECLASS_ILR(A_ILR_ID, A_REVISION, L_PRIOR_CURRENT_REVISION ); 
           If RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE('F_CREATE_JE_REMEASUREMENT_ILR returned an error.');
              return -1;
           end if;
        
			RTN := F_CREATE_JE_REMEASUREMENT_ILR(A_ILR_ID, A_REVISION, L_PRIOR_CURRENT_REVISION); 
			If RTN = -1 then
			  PKG_PP_LOG.P_WRITE_MESSAGE('F_CREATE_JE_REMEASUREMENT_ILR returned an error.');
			  return -1;
			end if;
      END IF;
      END IF;

      return 1;
   exception
      when others then
       PKG_PP_LOG.P_WRITE_MESSAGE(sqlerrm);

         return -1;
   end F_APPROVE_ILR_NO_COMMIT;

   --**************************************************************************
   --                            F_CREATE_JE_PROFIT_LOSS()
   -- @@ DESCRIPTION
   --    This function will create a JE for Profit and Loss depending on the below logic:
   --
   --     If Selling Profit Loss < 0 then TT = 4006 and account = SELL_PROFIT_LOSS_ACCOUNT_ID
   --     If Selling Profit Loss > 0 and Direct Finance then TT = 4055 and account = DEF_SELLING_PROFIT_ACCOUNT_ID
   --     If Selling Profit Loss > 0 and Sales Type then TT = 4005 and account = SELL_PROFIT_LOSS_ACCOUNT_ID
   --
   -- @@PARAMS
   --          A_LSR_ILR_ID: The ILR ID
   --          A_AMT: The Amount
   --          A_SELL_PL_ACCOUNT_ID: Selling Profit Loss Account ID
   --          A_DEF_SELL_PL_ACCOUNT_ID: Deferred Selling Profit Loss Account ID
   --          A_GAIN_LOSS: Gain Loss
   --          A_COMPANY_ID: Company ID
   --          A_MONTH: The Accounting Month
   --          A_DR_CR: Debit or Credit Indicator
   --          A_GL_JC: GL Journal Code
   --          A_SOB_ID: Set of Books ID
   --          A_JE_METHOD_ID: Journal Entry Method ID
   --          A_AMOUNT_TYPE: Amount Type
   --          A_REVERSAL_CONVENTION: Reversal Convention
   --          A_EXCHANGE_RATE: Exchange Rate
   --          A_CURRENCY_FROM: Currency From
   --          A_CURRENCY_TO: Currency To
   --          A_SELLING_PROFIT_LOSS: Selling Profit Loss Amount
   --          A_FASB_CAP_TYPE_ID: Sales Type(2), Direct Finance (3)
   --          A_SEPARATE_COST_OF_GOODS_SW: Switch indicating if 4005/4006 journals should split out to 4074-4077
   --          A_CALL_TYPE: Which amount this is being called for: PPE, ST RECEIVABLE, UNGUARANTEED RESIDUAL, or IDC
   --          A_COST_OF_GOODS_SOLD_ACCT_ID: Cost of Goods Sold Account ID
   --          A_REVENUE_ACCOUNT_ID: Revenue Account ID
   --          A_MSG: Message String for Error Description
   -- @@RETURN
   --        1 = SUCCESS
   --       -1 = FAILURE
   --
   --**************************************************************************
   function F_CREATE_JE_PROFIT_LOSS(A_LSR_ILR_ID                 in number,
                                    A_AMT                        in number,
                                    A_SELL_PL_ACCOUNT_ID         in number,
                                    A_DEF_PL_ACCOUNT_ID          in number,
                                    A_GAIN_LOSS                  in number,
                                    A_COMPANY_ID                 in number,
                                    A_MONTH                      in date,
                                    A_DR_CR                      in number,
                                    A_GL_JC                      in varchar2,
                                    A_SOB_ID                     in number,
                                    A_JE_METHOD_ID               in number,
                                    A_AMOUNT_TYPE                in number,
                                    A_REVERSAL_CONVENTION        in number,
                                    A_EXCHANGE_RATE              in number,
                                    A_CURRENCY_FROM              in number,
                                    A_CURRENCY_TO                in number,
                                    A_SELLING_PROFIT_LOSS        in number,
                                    A_FASB_CAP_TYPE_ID           in number,
                                    A_SEPARATE_COST_OF_GOODS_SW  IN NUMBER,
                                    A_CALL_TYPE                  IN VARCHAR2,
                                    A_COST_OF_GOODS_SOLD_ACCT_ID IN NUMBER,
                                    A_REVENUE_ACCOUNT_ID         IN NUMBER,
                                    A_MSG                        out varchar2) return number is
     L_RTN number;
     L_TRANS_TYPE number;
     L_ACCOUNT_ID number;
   BEGIN

     IF A_FASB_CAP_TYPE_ID = 2 AND A_SEPARATE_COST_OF_GOODS_SW = 1 THEN
       IF A_CALL_TYPE = 'PPE' THEN
         L_TRANS_TYPE := 4074;
         L_ACCOUNT_ID := A_COST_OF_GOODS_SOLD_ACCT_ID;
       ELSIF A_CALL_TYPE = 'ST RECEIVABLE' THEN
         L_TRANS_TYPE := 4075;
         L_ACCOUNT_ID := A_REVENUE_ACCOUNT_ID;
       ELSIF A_CALL_TYPE = 'UNGUARANTEED RESIDUAL' THEN
         L_TRANS_TYPE := 4076;
         L_ACCOUNT_ID := A_REVENUE_ACCOUNT_ID;
       ELSIF A_CALL_TYPE = 'IDC' THEN
         L_TRANS_TYPE := 4077;
         L_ACCOUNT_ID := A_COST_OF_GOODS_SOLD_ACCT_ID;
	   ELSIF A_CALL_TYPE = 'ACCRUED' THEN
         L_TRANS_TYPE := 4044;
         L_ACCOUNT_ID := A_SELL_PL_ACCOUNT_ID;
	   ELSIF A_CALL_TYPE = 'DEFERRED' THEN
         L_TRANS_TYPE := 4043;
         L_ACCOUNT_ID := A_SELL_PL_ACCOUNT_ID;
       ELSE
         PKG_PP_LOG.P_WRITE_MESSAGE('Unhandled call location/type for F_CREATE_JE_PROFIT_LOSS');
         return -1;
       END IF;
     ELSE
       IF A_SELLING_PROFIT_LOSS > 0 THEN
	       --If Selling Profit Loss > 0 and Sales Type then TT = 4005 and account = SELL_PROFIT_LOSS_ACCOUNT_ID
	       if A_FASB_CAP_TYPE_ID = 2 THEN
		       IF A_CALL_TYPE = 'ACCRUED' THEN
					L_TRANS_TYPE := 4044;
				ELSIF A_CALL_TYPE = 'DEFERRED' THEN
					L_TRANS_TYPE := 4043;
				ELSE
					L_TRANS_TYPE := 4005;
				END IF;
		       
		       L_ACCOUNT_ID := A_SELL_PL_ACCOUNT_ID;
	       --If Selling Profit Loss > 0 and Direct Finance then TT = 4055 and account = DEF_SELLING_PROFIT_ACCOUNT_ID
		     elsif A_FASB_CAP_TYPE_ID = 3 THEN
		       L_TRANS_TYPE := 4055;
		       L_ACCOUNT_ID := A_DEF_PL_ACCOUNT_ID;
	       end if;
	  
         --If Selling Profit Loss < 0 then TT = 4006 and account = SELL_PROFIT_LOSS_ACCOUNT_ID
	     ELSIF A_SELLING_PROFIT_LOSS <= 0 THEN
			IF A_CALL_TYPE = 'ACCRUED' THEN
				L_TRANS_TYPE := 4044;
			ELSIF A_CALL_TYPE = 'DEFERRED' THEN
				L_TRANS_TYPE := 4043;
			ELSE
				L_TRANS_TYPE := 4006;
			END IF;
			
         L_ACCOUNT_ID := A_SELL_PL_ACCOUNT_ID;
       END IF;
     END IF;

     L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_LSR_ILR_ID, 
                                            L_TRANS_TYPE, 
                                            A_AMT,
                                            L_ACCOUNT_ID, 
                                            0, 
                                            A_COMPANY_ID, 
                                            A_MONTH,
                                            A_DR_CR, 
                                            A_GL_JC, 
                                            A_SOB_ID, 
                                            A_JE_METHOD_ID, 
                                            A_AMOUNT_TYPE, 
                                            A_REVERSAL_CONVENTION, 
                                            A_EXCHANGE_RATE,
                                            A_CURRENCY_FROM, 
                                            A_CURRENCY_TO, 
                                            A_MSG);
     if L_RTN = -1 then
       PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
       return -1;
     end if;

     return 1;

   end F_CREATE_JE_PROFIT_LOSS;
   
   --**************************************************************************
   --                            F_CREATE_JE_WITH_PROFIT_LOSS()
   -- @@ DESCRIPTION
   --    This function will create a JE for the passed in GL account
   --		AND create a JE for the associated Profit and Loss:
   --
   -- @@PARAMS
   --          A_ILR_ID: The ILR ID
   --		   A_TRANS_TYPE: The trans type
   --          A_AMT: The Amount
   --          A_GL_ACCT_ID: The GL ACCOUNT for the "main" JE.  This is not the profit/loss account
   --          A_SELL_PL_ACCOUNT_ID: Selling Profit Loss Account ID
   --          A_DEF_SELL_PL_ACCOUNT_ID: Deferred Selling Profit Loss Account ID
   --          A_GAIN_LOSS: Gain Loss
   --          A_COMPANY_ID: Company ID
   --          A_MONTH: The Accounting Month
   --          A_DR_CR: Debit or Credit Indicator of the main JE.  The profit / loss will be the opposite
   --          A_GL_JC: GL Journal Code
   --          A_SOB_ID: Set of Books ID
   --          A_JE_METHOD_ID: Journal Entry Method ID
   --          A_AMOUNT_TYPE: Amount Type
   --          A_REVERSAL_CONVENTION: Reversal Convention
   --          A_EXCHANGE_RATE: Exchange Rate
   --          A_CURRENCY_FROM: Currency From
   --          A_CURRENCY_TO: Currency To
   --          A_SELLING_PROFIT_LOSS: Selling Profit Loss Amount
   --          A_FASB_CAP_TYPE_ID: Sales Type(2), Direct Finance (3)
   --          A_SEPARATE_COST_OF_GOODS_SW: Switch indicating if 4005/4006 journals should split out to 4074-4077
   --          A_CALL_TYPE: Which amount this is being called for: PPE, ST RECEIVABLE, UNGUARANTEED RESIDUAL, or IDC
   --          A_COST_OF_GOODS_SOLD_ACCT_ID: Cost of Goods Sold Account ID
   --          A_REVENUE_ACCOUNT_ID: Revenue Account ID
   --          A_MSG: Message String for Error Description
   -- @@RETURN
   --        1 = SUCCESS
   --       -1 = FAILURE
   --
   --**************************************************************************
   function F_CREATE_JE_WITH_PROFIT_LOSS(A_ILR_ID                 in number,
									A_TRANS_TYPE				 in number,
                                    A_AMT                        in number,
									A_GL_ACCT_ID				 in number,
                                    A_SELL_PL_ACCOUNT_ID         in number,
                                    A_DEF_PL_ACCOUNT_ID          in number,
                                    A_GAIN_LOSS                  in number,
                                    A_COMPANY_ID                 in number,
                                    A_MONTH                      in date,
                                    A_DR_CR                      in number,
                                    A_GL_JC                      in varchar2,
                                    A_SOB_ID                     in number,
                                    A_JE_METHOD_ID               in number,
                                    A_AMOUNT_TYPE                in number,
                                    A_REVERSAL_CONVENTION        in number,
                                    A_EXCHANGE_RATE              in number,
                                    A_CURRENCY_FROM              in number,
                                    A_CURRENCY_TO                in number,
                                    A_SELLING_PROFIT_LOSS        in number,
                                    A_FASB_CAP_TYPE_ID           in number,
                                    A_SEPARATE_COST_OF_GOODS_SW  IN NUMBER,
                                    A_CALL_TYPE                  IN VARCHAR2,
                                    A_COST_OF_GOODS_SOLD_ACCT_ID IN NUMBER,
                                    A_REVENUE_ACCOUNT_ID         IN NUMBER,
                                    A_MSG                        out varchar2) return number is
     L_SUCCESS number;
	 L_FAILURE number;
	 L_RTN number;
	BEGIN
		L_SUCCESS := 1;
		L_FAILURE := -1;
		
		-- create the "main" je
		L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
											  A_TRANS_TYPE, 
											  A_AMT ,
											  A_GL_ACCT_ID, 
											  A_GAIN_LOSS, 
											  A_COMPANY_ID, 
											  A_MONTH,
											  A_DR_CR, 
											  A_GL_JC, 
											  A_SOB_ID, 
											  A_JE_METHOD_ID, 
											  A_AMOUNT_TYPE, 
											  A_REVERSAL_CONVENTION, 
											  A_EXCHANGE_RATE,
											  A_CURRENCY_FROM, 
											  A_CURRENCY_TO, 
											  A_MSG);
		
		if L_RTN = -1 then
			return L_FAILURE;
		end if;
		
		--Profit/Loss Selling Profit and Loss Account ID
        L_RTN := PKG_LESSOR_APPROVAL.F_CREATE_JE_PROFIT_LOSS(A_ILR_ID, 
												A_AMT,
												A_SELL_PL_ACCOUNT_ID, 
												A_DEF_PL_ACCOUNT_ID, 
												A_GAIN_LOSS, 
												A_COMPANY_ID, 
												A_MONTH,
												-- if the "main" je is a dr then this is a cr.
												-- if the "main" je is a cr then this is a dr.
												(1 - A_DR_CR),
												A_GL_JC, 
												A_SOB_ID, 
												A_JE_METHOD_ID, 
												A_AMOUNT_TYPE, 
												A_REVERSAL_CONVENTION, 
												A_EXCHANGE_RATE,
												A_CURRENCY_FROM, 
												A_CURRENCY_TO, 
												A_SELLING_PROFIT_LOSS, 
												A_FASB_CAP_TYPE_ID, 
												A_SEPARATE_COST_OF_GOODS_SW, 
												A_CALL_TYPE,
												A_COST_OF_GOODS_SOLD_ACCT_ID, 
												A_REVENUE_ACCOUNT_ID, 
												A_MSG);
		if L_RTN = -1 then
			return L_FAILURE;
		end if;
		
		return L_SUCCESS;
	END F_CREATE_JE_WITH_PROFIT_LOSS;

   --**************************************************************************
   --                            F_JE_DR_CR()
   -- @@ DESCRIPTION
   --    This function will create a JE for both debit and credit sides
   --
   -- @@PARAMS
   --          A_ILR_ID: The ILR ID
   --          A_AMT: The Amount
   --          A_DR_GL_ACCT_ID: The Debit GL ACCOUNT for the JE.  This is not the profit/loss account
   --          A_CR_GL_ACCT_ID: The Credit GL ACCOUNT for the JE.  This is not the profit/loss account
   --          A_DR_TRANS_TYPE: The Debit Trans Types
   --          A_CR_TRANS_TYPE: The Credit Trans Type
   --          A_COMPANY_ID: Company ID
   --          A_MONTH: The Accounting Month
   --          A_GL_JC: GL Journal Code
   --          A_SOB_ID: Set of Books ID
   --          A_JE_METHOD_ID: Journal Entry Method ID
   --          A_AMOUNT_TYPE: Amount Type
   --          A_REVERSAL_CONVENTION: Reversal Convention
   --          A_EXCHANGE_RATE: Exchange Rate
   --          A_CURRENCY_FROM: Currency From
   --          A_CURRENCY_TO: Currency To
   --          A_MSG: Message String for Error Description
   -- @@RETURN
   --        1 = SUCCESS
   --       -1 = FAILURE
   --
   --**************************************************************************	
	function F_JE_DR_CR(A_ILR_ID   in number,
                        A_AMT           in number,
                        A_DR_GL_ACCOUNT_ID in number,
                        A_CR_GL_ACCOUNT_ID in number,
						A_DR_TRANS_TYPE in number,
						A_CR_TRANS_TYPE in number,
						A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_GL_JC         in varchar2,
                        A_SOB_ID        in number,
                        A_JE_METHOD_ID    in number,
                        A_AMOUNT_TYPE     in number,
                        A_REVERSAL_CONVENTION in number,
                        A_EXCHANGE_RATE in number,
                        A_CURRENCY_FROM in number,
                        A_CURRENCY_TO   in number,
                        A_MSG           out varchar2) return number
	IS
		L_SUCCESS number;
		L_FAILURE number;
		L_RTN number;
	BEGIN
		L_SUCCESS := 1;
		L_FAILURE := -1;
		
		--DEBIT SIDE
		L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
											  A_DR_TRANS_TYPE, 
											  A_AMT,
											  A_DR_GL_ACCOUNT_ID, 
											  0, 
											  A_COMPANY_ID, 
											  A_MONTH,
											  1,
											  A_GL_JC, 
											  A_SOB_ID, 
											  A_JE_METHOD_ID, 
											  A_AMOUNT_TYPE, 
											  A_REVERSAL_CONVENTION, 
											  A_EXCHANGE_RATE,
											  A_CURRENCY_FROM, 
											  A_CURRENCY_TO, 
											  A_MSG);
		if L_RTN = -1 then
			return L_FAILURE;
		end if;

		--CREDIT SIDE
		L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
											  A_CR_TRANS_TYPE, 
											  A_AMT,
											  A_CR_GL_ACCOUNT_ID, 
											  0, 
											  A_COMPANY_ID, 
											  A_MONTH,
											  0, 
											  A_GL_JC, 
											  A_SOB_ID, 
											  A_JE_METHOD_ID, 
											  A_AMOUNT_TYPE, 
											  A_REVERSAL_CONVENTION, 
											  A_EXCHANGE_RATE,
											  A_CURRENCY_FROM, 
											  A_CURRENCY_TO, 
											  A_MSG);
		if L_RTN = -1 then
			return L_FAILURE;
		end if;
		
		return L_SUCCESS;
	END F_JE_DR_CR;

   --**************************************************************************
   --                            F_JE_IDC()
   -- @@ DESCRIPTION
   --    This function will create a JE for the Initial Direct Costs
   --
   -- @@PARAMS
   --          A_ILR_ID: The ILR ID
   --		   A_TRANS_TYPE: The trans type for the crdit side of the transaction
   --          A_AMT: The Amount
   --          A_DR_GL_ACCT_ID: The Debit GL ACCOUNT for the JE.  This is not the profit/loss account
   --          A_CR_GL_ACCT_ID: The Credit GL ACCOUNT for the JE.  This is not the profit/loss account
   --          A_SELL_PL_ACCOUNT_ID: Selling Profit Loss Account ID
   --          A_DEF_SELL_PL_ACCOUNT_ID: Deferred Selling Profit Loss Account ID
   --          A_GAIN_LOSS: Gain Loss
   --          A_COMPANY_ID: Company ID
   --          A_MONTH: The Accounting Month
   --          A_DR_CR: Debit or Credit Indicator of the main JE.  The profit / loss will be the opposite
   --          A_GL_JC: GL Journal Code
   --          A_SOB_ID: Set of Books ID
   --          A_JE_METHOD_ID: Journal Entry Method ID
   --          A_AMOUNT_TYPE: Amount Type
   --          A_REVERSAL_CONVENTION: Reversal Convention
   --          A_EXCHANGE_RATE: Exchange Rate
   --          A_CURRENCY_FROM: Currency From
   --          A_CURRENCY_TO: Currency To
   --          A_SELLING_PROFIT_LOSS: Selling Profit Loss Amount
   --          A_FASB_CAP_TYPE_ID: Sales Type(2), Direct Finance (3)
   --          A_SEPARATE_COST_OF_GOODS_SW: Switch indicating if 4005/4006 journals should split out to 4074-4077
   --          A_COST_OF_GOODS_SOLD_ACCT_ID: Cost of Goods Sold Account ID
   --          A_REVENUE_ACCOUNT_ID: Revenue Account ID
   --		   A_BOOK_PROFIT_LOSS: True if the dedit side of this transaction should book to profit/loss instead of the DR gl acct
   --          A_MSG: Message String for Error Description
   -- @@RETURN
   --        1 = SUCCESS
   --       -1 = FAILURE
   --
   --**************************************************************************	
	function F_JE_IDC(A_ILR_ID                 in number,
					A_CR_TRANS_TYPE				in number,
					A_AMT                        in number,
					A_DR_GL_ACCT_ID				 in number,
					A_CR_GL_ACCT_ID				 in number,
					A_SELL_PL_ACCOUNT_ID         in number,
					A_DEF_PL_ACCOUNT_ID          in number,
					A_GAIN_LOSS                  in number,
					A_COMPANY_ID                 in number,
					A_MONTH                      in date,
					A_GL_JC                      in varchar2,
					A_SOB_ID                     in number,
					A_JE_METHOD_ID               in number,
					A_AMOUNT_TYPE                in number,
					A_REVERSAL_CONVENTION        in number,
					A_EXCHANGE_RATE              in number,
					A_CURRENCY_FROM              in number,
					A_CURRENCY_TO                in number,
					A_SELLING_PROFIT_LOSS        in number,
					A_FASB_CAP_TYPE_ID           in number,
					A_SEPARATE_COST_OF_GOODS_SW  IN NUMBER,
					A_COST_OF_GOODS_SOLD_ACCT_ID IN NUMBER,
					A_REVENUE_ACCOUNT_ID         IN NUMBER,
					A_BOOK_PROFIT_LOSS			  IN BOOLEAN,
					A_MSG                        out varchar2) return number
	IS
		L_SUCCESS number;
		L_FAILURE number;
		L_RTN number;
	BEGIN
		L_SUCCESS := 1;
		L_FAILURE := -1;
		
		-- Initial Direct Costs Credit(0)
		L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
											  A_CR_TRANS_TYPE, 
											  A_AMT,
											  A_CR_GL_ACCT_ID, 
											  0, 
											  A_COMPANY_ID, 
											  A_MONTH,
											  0,
											  A_GL_JC, 
											  A_SOB_ID, 
											  A_JE_METHOD_ID, 
											  A_AMOUNT_TYPE, 
											  A_REVERSAL_CONVENTION, 
											  A_EXCHANGE_RATE,
											  A_CURRENCY_FROM, 
											  A_CURRENCY_TO, 
											  A_MSG);
		if L_RTN = -1 then
			return L_FAILURE;
		end if;

		if A_BOOK_PROFIT_LOSS then
             --If Sales Type and Carrying Cost <> FMV then Zero(0) else sum(amount) for the ILR from lsr_ilr_initial_direct_cost
             --Profit/Loss Selling Profit and Loss Account ID Debit(1)
			 L_RTN := PKG_LESSOR_APPROVAL.F_CREATE_JE_PROFIT_LOSS(A_ILR_ID, 
												A_AMT,
												A_SELL_PL_ACCOUNT_ID, 
												A_DEF_PL_ACCOUNT_ID, 
												0, 
												A_COMPANY_ID, 
												A_MONTH,
												1,
												A_GL_JC, 
												A_SOB_ID, 
												A_JE_METHOD_ID, 
												A_AMOUNT_TYPE, 
												A_REVERSAL_CONVENTION, 
												A_EXCHANGE_RATE,
												A_CURRENCY_FROM, 
												A_CURRENCY_TO, 
												A_SELLING_PROFIT_LOSS, 
												A_FASB_CAP_TYPE_ID, 
												A_SEPARATE_COST_OF_GOODS_SW, 
												'IDC',
												A_COST_OF_GOODS_SOLD_ACCT_ID, 
												A_REVENUE_ACCOUNT_ID, 
												A_MSG);
												
        else --L_SOBS.FASB_CAP_TYPE_ID = 2 and L_CARRYING_COST <> L_FAIR_MARKET_VALUE and L_SOBS.INCLUDE_IDC_SW = 0
             --4008 Initial Direct Costs DEF_COSTS_ACCOUNT_ID Debit(1)
             --If Sales Type and Carrying Cost <> FMV then sum(amount) for the ILR from lsr_ilr_initial_direct_cost else Zero (0)
				L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
											  4008, 
											  A_AMT,
											  A_DR_GL_ACCT_ID, 
											  0, 
											  A_COMPANY_ID, 
											  A_MONTH,
											  1,
											  A_GL_JC, 
											  A_SOB_ID, 
											  A_JE_METHOD_ID, 
											  A_AMOUNT_TYPE, 
											  A_REVERSAL_CONVENTION, 
											  A_EXCHANGE_RATE,
											  A_CURRENCY_FROM, 
											  A_CURRENCY_TO, 
											  A_MSG);
        end if;
           
		if L_RTN = -1 then
			return L_FAILURE;
		end if;
		
		return L_SUCCESS;
	END F_JE_IDC;

   --**************************************************************************
   --                            F_CREATE_BACKDATED_JE_FOR_ILR()
   -- DESCRIPTION
   --   This function will create True-Up commencement journals for ILRs that are backdated.
   --
   -- PARAMETERS
   --          A_ILR_ID: The ILR ID
   --          A_REVISION: Revision
   --          A_LSR_ACCOUNT: Row from LSR_ILR_ACCOUNT
   --          A_GAIN_LOSS: Gain Loss
   --          A_COMPANY_ID: Company ID
   --          A_MONTH: The Schedule Month
   --          A_GL_POSTING_MO_YR: The Accounting Month
   --          A_GL_JC: GL Journal Code
   --          A_SOB_ID: Set of Books ID
   --          A_JE_METHOD_ID: Journal Entry Method ID
   --          A_AMOUNT_TYPE: Amount Type
   --          A_REVERSAL_CONVENTION: Reversal Convention
   --          A_CURRENCY_FROM: Currency From
   --          A_CURRENCY_TO: Currency To
   --          A_FASB_CAP_TYPE_ID: Operating (1), Sales Type (2), Direct Finance (3)
   --          A_MSG: Message String for Error Description
   -- RETURN
   --        1 = SUCCESS
   --       -1 = FAILURE
   --
   --**************************************************************************
   function F_CREATE_BACKDATED_JE_FOR_ILR(A_ILR_ID                     in number,
                                          A_REVISION                   in number,
                                          A_LSR_ACCOUNT                in LSR_ILR_ACCOUNT%rowtype,
                                          A_GAIN_LOSS                  in number,
                                          A_COMPANY_ID                 in number,
                                          A_MONTH                      in date,
                                          A_GL_POSTING_MO_YR           in date,
                                          A_GL_JC                      in varchar2,
                                          A_SOB_ID                     in number,
                                          A_JE_METHOD_ID               in number,
                                          A_AMOUNT_TYPE                in number,
                                          A_REVERSAL_CONVENTION        in number,
                                          A_CURRENCY_FROM              in number,
                                          A_CURRENCY_TO                in number,
                                          A_FASB_CAP_TYPE_ID           in number,
                                          A_MSG                        out varchar2) return number is
     L_RTN                   NUMBER; 
     L_RECEIVABLE_RECLASS    NUMBER(22,2); 
     L_INTEREST_ACCRUED      NUMBER(22,2);
     L_PRINCIPAL_RECEIVED    NUMBER(22,2);
     L_INTEREST_RECEIVED     NUMBER(22,2);
     L_EXEC_ACCRUAL          NUMBER(22,2);
     L_EXEC_PAID             NUMBER(22,2);
     L_CONT_ACCRUAL          NUMBER(22,2);	
     L_CONT_PAID             NUMBER(22,2);
     L_INTEREST_UNGUARAN_RES NUMBER(22,2);
     L_RECOGNIZED_PROFIT     NUMBER(22,2);
     L_DEF_PROFIT_RECLASS    NUMBER(22,2);
     L_ST_FX_GAIN_LOSS       NUMBER(22,2);
     L_LT_FX_GAIN_LOSS       NUMBER(22,2);
     L_BALANCE_RATE          NUMBER(22,8);
     L_ACTIVITY_RATE         NUMBER(22,8);
     L_USE_AVG_RATE          VARCHAR2(3);
     L_DEFERRED_RENT         NUMBER(22,2);
     L_ACCRUED_RENT          NUMBER(22,2);
     L_IDC_AMOUNT            NUMBER(22,2);
   begin
     
     -- Collect the amounts necessary for all potential journals rather than by FASB Cap Type
     A_MSG := 'Retrieve the schedule amounts for month '||to_char(A_MONTH);
     begin
       select nvl(s.beg_lt_receivable,0) - nvl(s.end_lt_receivable,0),
              nvl(s.interest_income_accrued,0),
              nvl(sd.principal_received,0),
              nvl(s.interest_income_received,0),
              nvl(s.executory_accrual1,0) + nvl(s.executory_accrual2,0) + nvl(s.executory_accrual3,0) + 
                nvl(s.executory_accrual4,0) + nvl(s.executory_accrual5,0) +
                nvl(s.executory_accrual6,0) + nvl(s.executory_accrual7,0) + nvl(s.executory_accrual8,0) + 
                nvl(s.executory_accrual9,0) + nvl(s.executory_accrual10,0),
              nvl(s.executory_paid1,0) + nvl(s.executory_paid2,0) + nvl(s.executory_paid3,0) + 
                nvl(s.executory_paid4,0) + nvl(s.executory_paid5,0) +
                nvl(s.executory_paid6,0) + nvl(s.executory_paid7,0) + nvl(s.executory_paid8,0) + 
                nvl(s.executory_paid9,0) + nvl(s.executory_paid10,0),
              nvl(s.contingent_accrual1,0) + nvl(s.contingent_accrual2,0) + nvl(s.contingent_accrual3,0) + 
                nvl(s.contingent_accrual4,0) + nvl(s.contingent_accrual5,0) +
                nvl(s.contingent_accrual6,0) + nvl(s.contingent_accrual7,0) + nvl(s.contingent_accrual8,0) + 
                nvl(s.contingent_accrual9,0) + nvl(s.contingent_accrual10,0),
              nvl(s.contingent_paid1,0) + nvl(s.contingent_paid2,0) + nvl(s.contingent_paid3,0) + 
                nvl(s.contingent_paid4,0) + nvl(s.contingent_paid5,0) +
                nvl(s.contingent_paid6,0) + nvl(s.contingent_paid7,0) + nvl(s.contingent_paid8,0) + 
                nvl(s.contingent_paid9,0) + nvl(s.contingent_paid10,0),   
              nvl(sd.interest_unguaranteed_residual,0),
              nvl(df.recognized_profit,0),
              nvl(df.begin_lt_deferred_profit,0) - nvl(df.end_lt_deferred_profit,0),
              nvl(s.deferred_rent,0),
              nvl(s.accrued_rent,0),
              nvl(s.initial_direct_cost,0)
         into L_RECEIVABLE_RECLASS, 
              L_INTEREST_ACCRUED,
              L_PRINCIPAL_RECEIVED,
              L_INTEREST_RECEIVED,
              L_EXEC_ACCRUAL,
              L_EXEC_PAID,
              L_CONT_ACCRUAL,
              L_CONT_PAID,
              L_INTEREST_UNGUARAN_RES,
              L_RECOGNIZED_PROFIT,
              L_DEF_PROFIT_RECLASS,
              L_DEFERRED_RENT,
              L_ACCRUED_RENT,
              L_IDC_AMOUNT
         from lsr_ilr_schedule s
         left outer join lsr_ilr_schedule_sales_direct sd on sd.ilr_id = s.ilr_id
                                                         and sd.revision = s.revision
                                                         and sd.set_of_books_id = s.set_of_books_id
                                                         and sd.month = s.month
         left outer join lsr_ilr_schedule_direct_fin df on df.ilr_id = s.ilr_id
                                                       and df.revision = s.revision
                                                       and df.set_of_books_id = s.set_of_books_id
                                                       and df.month = s.month
        where s.ilr_id = A_ILR_ID
          and s.revision = A_REVISION
          and s.set_of_books_id = A_SOB_ID
          and s.month = A_MONTH;  
     exception
       when NO_DATA_FOUND then
         PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: '||A_MSG||': '||sqlerrm);
         return -1;
     end;
        
     -- Get the Actual Locked Rate for the month
     A_MSG := 'Retrieve the Actual Rate for month '||to_char(A_MONTH);
     begin
       select rate
         into L_BALANCE_RATE
         from ls_lease_calculated_date_rates
        where company_id = A_COMPANY_ID
          and contract_currency_id = A_CURRENCY_FROM
          and company_currency_id = A_CURRENCY_TO
          and exchange_rate_type_id = 1
          and trunc(accounting_month,'fmmonth') = trunc(A_MONTH,'fmmonth');
     exception
       when NO_DATA_FOUND then
         A_MSG := 'ERROR: Retrieving the Actual Locked exchange rate for company '||to_char(A_COMPANY_ID)||' and month '||to_char(A_MONTH);
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
     end;
          
     IF L_BALANCE_RATE IS NULL THEN
       A_MSG := 'ERROR: Retrieving the Actual Locked exchange rate for company '||to_char(A_COMPANY_ID)||' and month '||to_char(A_MONTH);
       PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
       RETURN -1;
     END IF;
       
     -- Look up the Average Exchange Rate if it is used
     L_USE_AVG_RATE := NVL(UPPER(TRIM(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Lease MC: Use Average Rates',
                                                                                        A_COMPANY_ID))), 
                           'NO');
     IF L_USE_AVG_RATE = 'YES' THEN
       A_MSG := 'Retrieve the Average Rate for month '||to_char(A_MONTH);
       begin
         select nvl(rate, L_BALANCE_RATE)
           into L_ACTIVITY_RATE
           from ls_lease_calculated_date_rates
          where company_id = A_COMPANY_ID
            and contract_currency_id = A_CURRENCY_FROM
            and company_currency_id = A_CURRENCY_TO
            and exchange_rate_type_id = 4
            and trunc(accounting_month,'fmmonth') = trunc(A_MONTH,'fmmonth');
       exception
         when NO_DATA_FOUND then
           A_MSG := 'ERROR: Retrieving the Average Locked exchange rate for company '||to_char(A_COMPANY_ID)||' and month '||to_char(A_MONTH);
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
       end;
     ELSE
       -- Avg Rates aren't used, so set the value to the actual rate
       L_ACTIVITY_RATE := L_BALANCE_RATE;
     END IF;
     
     -- Look up the FX Gain/Loss if the Currency From <> Currency To
     IF A_CURRENCY_FROM <> A_CURRENCY_TO THEN
       select st_currency_gain_loss, lt_currency_gain_loss
         into L_ST_FX_GAIN_LOSS, L_LT_FX_GAIN_LOSS
         from v_lsr_ilr_mc_schedule
        where ilr_id = A_ILR_ID
          and revision = A_REVISION
          and set_of_books_id = A_SOB_ID
          and month = A_MONTH
          and ls_cur_type = 2;
     ELSE
       L_ST_FX_GAIN_LOSS := 0;
       L_LT_FX_GAIN_LOSS := 0;
     END IF;
     
     -- Common journal entries between FASB Cap Types
     -- 4010/4011 Interest Accrual DR/CR
     -- LSR_ILR_SCHEDULE:	INTEREST_INCOME_ACCRUED
     -- DR Acct: Interest Accrual (INT_ACCRUAL_ACCOUNT_ID)
     -- CR Acct: Interest Expense (INT_EXPENSE_ACCOUNT_ID)
     A_MSG := 'Processing 4010/4011 Interest Accrual DR/CR';
     IF L_INTEREST_ACCRUED <> 0 THEN
       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4010, 
                                              L_INTEREST_ACCRUED, 
                                              A_LSR_ACCOUNT.INT_ACCRUAL_ACCOUNT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              1, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              L_ACTIVITY_RATE,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;

       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4011, 
                                              L_INTEREST_ACCRUED, 
                                              A_LSR_ACCOUNT.INT_EXPENSE_ACCOUNT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              0, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              L_ACTIVITY_RATE,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;
     END IF;
     
     -- 4018/4019 Accounts Receivable DR/Interest Invoice CR
     -- LSR_ILR_SCHEDULE:	INTEREST_INCOME_RECEIVED
     -- DR Acct: Accounts Receivable (AR_ACCOUNT_ID)
     -- CR Acct: Interest Accrual    (INT_ACCRUAL_ACCOUNT_ID)
     A_MSG := 'Processing 4018/4019 Accounts Receivable DR/Interest Invoice CR';
     IF L_INTEREST_RECEIVED <> 0 THEN
       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4018, 
                                              L_INTEREST_RECEIVED, 
                                              A_LSR_ACCOUNT.AR_ACCOUNT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              1, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              L_ACTIVITY_RATE,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;

       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4019, 
                                              L_INTEREST_RECEIVED, 
                                              A_LSR_ACCOUNT.INT_ACCRUAL_ACCOUNT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              0, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              L_ACTIVITY_RATE,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;
     END IF;
     
     -- 4012/4013 Executory Accrual DR/CR
     -- LSR_ILR_SCHEDULE:	EXECUTORY_ACCRUAL1-10
     -- DR Acct: Executory Accrual (EXEC_ACCRUAL_ACCOUNT_ID)
     -- CR Acct: Executory Expense (EXEC_EXPENSE_ACCOUNT_ID)
     A_MSG := 'Processing 4012/4013 Executory Accrual DR/CR';
     IF L_EXEC_ACCRUAL <> 0 THEN
       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4012, 
                                              L_EXEC_ACCRUAL, 
                                              A_LSR_ACCOUNT.EXEC_ACCRUAL_ACCOUNT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              1, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              L_ACTIVITY_RATE,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;

       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4013, 
                                              L_EXEC_ACCRUAL, 
                                              A_LSR_ACCOUNT.EXEC_EXPENSE_ACCOUNT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              0, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              L_ACTIVITY_RATE,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;
     END IF;
       
     -- 4018/4020: Accounts Receivable DR/Executory Invoice CR
     -- LSR_ILR_SCHEDULE:	EXECUTORY_PAID1-10
     -- DR Acct: Accounts Receivable (AR_ACCOUNT_ID)
     -- CR Acct: Executory Accrual   (EXEC_ACCRUAL_ACCOUNT_ID)
     A_MSG := 'Processing 4018/4020: Accounts Receivable DR/Executory Invoice CR';
     IF L_EXEC_PAID <> 0 THEN
       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4018, 
                                              L_EXEC_PAID, 
                                              A_LSR_ACCOUNT.AR_ACCOUNT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              1, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              L_ACTIVITY_RATE,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;

       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4020, 
                                              L_EXEC_PAID, 
                                              A_LSR_ACCOUNT.EXEC_ACCRUAL_ACCOUNT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              0, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              L_ACTIVITY_RATE,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;
     END IF;
       
     -- 4014/4015: Contingent Accrual DR/CR
     -- LSR_ILR_SCHEDULE:	CONTINGENT_ACCRUAL1-10
     -- DR Acct: Contingent Accrual (CONT_ACCRUAL_ACCOUNT_ID)
     -- CR Acct: Contingent Expense (CONT_EXPENSE_ACCOUNT_ID)
     A_MSG := 'Processing 4014/4015: Contingent Accrual DR/CR';
     IF L_CONT_ACCRUAL <> 0 THEN
       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4014, 
                                              L_CONT_ACCRUAL, 
                                              A_LSR_ACCOUNT.CONT_ACCRUAL_ACCOUNT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              1, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              L_ACTIVITY_RATE,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;

       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4015, 
                                              L_CONT_ACCRUAL, 
                                              A_LSR_ACCOUNT.CONT_EXPENSE_ACCOUNT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              0, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              L_ACTIVITY_RATE,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;
     END IF;
       
     -- 4018/4021 Accounts Receivable DR/Contingent Invoice CR
     -- LSR_ILR_SCHEDULE:	CONTINGENT_PAID1-10
     -- DR Acct: Accounts Receivable (AR_ACCOUNT_ID)
     -- CR Acct: Contingent Accrual  (CONT_ACCRUAL_ACCOUNT_ID)
     A_MSG := 'Processing 4018/4021 Accounts Receivable DR/Contingent Invoice CR';
     IF L_CONT_PAID <> 0 THEN
       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4018, 
                                              L_CONT_PAID, 
                                              A_LSR_ACCOUNT.AR_ACCOUNT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              1, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              L_ACTIVITY_RATE,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;

       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4021, 
                                              L_CONT_PAID, 
                                              A_LSR_ACCOUNT.CONT_ACCRUAL_ACCOUNT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              0, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              L_ACTIVITY_RATE,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;
     END IF;
   
     -- Separate sets of journals based on FASB Cap Type
     IF A_FASB_CAP_TYPE_ID = 1 THEN
       -- Operating
       
       -- 4036/4037 Monthly Deferred Rent DR/CR
       -- LSR_ILR_SCHEDULE:	DEFERRED_RENT
       -- DR Acct: Interest Accrual      (INT_ACCRUAL_ACCOUNT_ID)
       -- CR Acct: Deferred Rent Balance (DEFERRED_RENT_ACCT_ID)
       A_MSG := 'Processing 4036/4037 Monthly Deferred Rent DR/CR';
       IF L_DEFERRED_RENT <> 0 THEN
         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4036, 
                                                L_DEFERRED_RENT, 
                                                A_LSR_ACCOUNT.INT_ACCRUAL_ACCOUNT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                1, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_ACTIVITY_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;

         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4037, 
                                                L_DEFERRED_RENT, 
                                                A_LSR_ACCOUNT.DEFERRED_RENT_ACCT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                0, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_ACTIVITY_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;
       END IF;
       
       -- 4034/4035 Monthly Accrued Rent DR/CR
       -- LSR_ILR_SCHEDULE:	ACCRUED_RENT
       -- DR Acct: Accrued Rent Baalnce (ACCRUED_RENT_ACCT_ID)
       -- CR Acct: Interest Accrual     (INT_ACCRUAL_ACCOUNT_ID)
       A_MSG := 'Processing 4034/4035 Monthly Accrued Rent DR/CR';
       IF L_ACCRUED_RENT <> 0 THEN
         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4034, 
                                                L_ACCRUED_RENT, 
                                                A_LSR_ACCOUNT.ACCRUED_RENT_ACCT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                1, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_ACTIVITY_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;

         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4035, 
                                                L_ACCRUED_RENT, 
                                                A_LSR_ACCOUNT.INT_ACCRUAL_ACCOUNT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                0, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_ACTIVITY_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;
       END IF;
       
       -- 4032/4033 Initial Direct Cost Amortization DR/CR
       -- LSR_ILR_SCHEDULE:	INITIAL_DIRECT_COST
       -- DR Acct: Initial Direct Cost Expense (INI_DIRECT_COST_ACCOUNT_ID)
       -- CR Acct: Deferred Lease Costs        (DEF_COSTS_ACCOUNT_ID)
       A_MSG := 'Processing 4032/4033 Initial Direct Cost Amortization DR/CR';
       IF L_IDC_AMOUNT <> 0 THEN
         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4032, 
                                                L_IDC_AMOUNT, 
                                                A_LSR_ACCOUNT.INI_DIRECT_COST_ACCOUNT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                1, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_ACTIVITY_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;

         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4033, 
                                                L_IDC_AMOUNT, 
                                                A_LSR_ACCOUNT.DEF_COSTS_ACCOUNT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                0, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_ACTIVITY_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;
       END IF;
       
     ELSIF A_FASB_CAP_TYPE_ID = 2 OR A_FASB_CAP_TYPE_ID = 3 THEN
       -- Sales Type and Direct Finance
       
       -- 4027/4028 ST/LT Receivable Reclass DR/CR
       -- LSR_ILR_SCHEDULE:	BEG_LT_RECEIVABLE - END_LT_RECEIVABLE
       -- DR Acct: ST Receivables (ST_RECEIVABLE_ACCOUNT_ID)
       -- CR Acct: LT Receivables (LT_RECEIVABLE_ACCOUNT_ID)
       A_MSG := 'Processing 4027/4028 ST/LT Receivable Reclass DR/CR';
       IF L_RECEIVABLE_RECLASS <> 0 THEN
         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4027, 
                                                L_RECEIVABLE_RECLASS, 
                                                A_LSR_ACCOUNT.ST_RECEIVABLE_ACCOUNT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                1, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_BALANCE_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;

         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4028, 
                                                L_RECEIVABLE_RECLASS, 
                                                A_LSR_ACCOUNT.LT_RECEIVABLE_ACCOUNT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                0, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_BALANCE_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;
       END IF;
       
       -- 4018/4022 Accounts Receivable DR/ST Receivable CR
       -- LSR_ILR_SCHEDULE_SALES_DIRECT:	PRINCIPAL_RECEIVED
       -- DR Acct: Accounts Receivable (AR_ACCOUNT_ID)
       -- CR Acct: ST Receivables      (ST_RECEIVABLE_ACCOUNT_ID)
       A_MSG := 'Processing 4018/4022 Accounts Receivable DR/ST Receivable CR';
       IF L_PRINCIPAL_RECEIVED <> 0 THEN
         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4018, 
                                                L_PRINCIPAL_RECEIVED, 
                                                A_LSR_ACCOUNT.AR_ACCOUNT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                1, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_ACTIVITY_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;

         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4022, 
                                                L_PRINCIPAL_RECEIVED, 
                                                A_LSR_ACCOUNT.ST_RECEIVABLE_ACCOUNT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                0, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_ACTIVITY_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;
       END IF;
       
       -- 4016/4017 Unguaranteed Residual Accretion DR/CR
       -- LSR_ILR_SCHEDULE_SALES_DIRECT:	INTEREST_UNGUARANTEED_RESIDUAL
       -- DR Acct: Unguaranteed Residual             (UNGUARAN_RES_ACCOUNT_ID)
       -- CR Acct: Interest on Unguaranteed Residual (INT_UNGUARAN_RES_ACCOUNT_ID)
       A_MSG := 'Processing 4016/4017 Unguaranteed Residual Accretion DR/CR';
       IF L_INTEREST_UNGUARAN_RES <> 0 THEN
         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4016, 
                                                L_INTEREST_UNGUARAN_RES, 
                                                A_LSR_ACCOUNT.UNGUARAN_RES_ACCOUNT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                1, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_ACTIVITY_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;

         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4017, 
                                                L_INTEREST_UNGUARAN_RES, 
                                                A_LSR_ACCOUNT.INT_UNGUARAN_RES_ACCOUNT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                0, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_ACTIVITY_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;
       END IF;
       
       -- 4056/4057 Deferred Selling Profit DR/Recognize Selling Profit CR
       -- LSR_ILR_SCHEDULE:	RECOGNIZED_PROFIT
       -- DR Acct: Deferred Selling Profit (DEF_SELLING_PROFIT_ACCOUNT_ID)
       -- CR Acct: Selling Profit/Loss     (SELL_PROFIT_LOSS_ACCOUNT_ID)
       A_MSG := 'Processing 4056/4057 Deferred Selling Profit DR/Recognize Selling Profit CR';
       IF L_RECOGNIZED_PROFIT <> 0 THEN
         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4056, 
                                                L_RECOGNIZED_PROFIT, 
                                                A_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                1, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_ACTIVITY_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;

         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4057, 
                                                L_RECOGNIZED_PROFIT, 
                                                A_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                0, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_ACTIVITY_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;
       END IF;
       
       -- 4080/4081 LT Deferred Profit DR/ST Deferred Profit CR
       -- LSR_ILR_SCHEDULE:	BEGIN_LT_DEFERRED_PROFIT - END_LT_DEFERRED_PROFIT
       -- DR Acct: LT Deferred Selling Profit (LT_DEF_SELL_PROFIT_ACCOUNT_ID)
       -- CR Acct: ST Deferred Selling Profit (DEF_SELLING_PROFIT_ACCOUNT_ID)
       A_MSG := 'Processing 4080/4081 LT Deferred Profit DR/ST Deferred Profit CR';
       IF L_DEF_PROFIT_RECLASS <> 0 THEN
         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4080, 
                                                L_DEF_PROFIT_RECLASS, 
                                                A_LSR_ACCOUNT.LT_DEF_SELL_PROFIT_ACCOUNT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                1, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_BALANCE_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;

         L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                4081, 
                                                L_DEF_PROFIT_RECLASS, 
                                                A_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
                                                A_GAIN_LOSS, 
                                                A_COMPANY_ID, 
                                                A_GL_POSTING_MO_YR,
                                                0, 
                                                A_GL_JC, 
                                                A_SOB_ID, 
                                                A_JE_METHOD_ID, 
                                                A_AMOUNT_TYPE, 
                                                A_REVERSAL_CONVENTION, 
                                                L_BALANCE_RATE,
                                                A_CURRENCY_FROM, 
                                                A_CURRENCY_TO, 
                                                A_MSG);
         IF L_RTN = -1 THEN
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           RETURN -1;
         END IF;
       END IF;
     ELSE
       A_MSG := 'Backdated JEs: ERROR: Unsupported FASB Cap Type '||to_char(A_FASB_CAP_TYPE_ID);
       RETURN -1; 
     END IF;
     
     -- Common Multi-Currency journal entries
     -- 4051/4052 LT Currency Gain/Loss DR/CR
     -- V_LSR_ILR_MC_SCHEDULE:	LT_CURRENCY_GAIN_LOSS
     -- DR Acct: Currency Gain/Loss (CURR_GAIN_LOSS_ACCT_ID)
     -- CR Acct: LT Receivable      (LT_RECEIVABLE_ACCOUNT_ID)
     A_MSG := 'Processing LT Currency Gain/Loss DR/CR';
     IF L_LT_FX_GAIN_LOSS <> 0 THEN
       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4051, 
                                              L_LT_FX_GAIN_LOSS, 
                                              A_LSR_ACCOUNT.CURR_GAIN_LOSS_ACCT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              1, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              1,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;

       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4052, 
                                              L_LT_FX_GAIN_LOSS, 
                                              A_LSR_ACCOUNT.LT_RECEIVABLE_ACCOUNT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              0, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              1,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;
     END IF;
     
     -- 4068/4069 ST Currency Gain/Loss DR/CR
     -- V_LSR_ILR_MC_SCHEDULE:	ST_CURRENCY_GAIN_LOSS
     -- DR Acct: Currency Gain/Loss (CURR_GAIN_LOSS_ACCT_ID)
     -- CR Acct: ST Receivable      (ST_RECEIVABLE_ACCOUNT_ID)
     A_MSG := 'Processing ST Currency Gain/Loss DR/CR';
     IF L_ST_FX_GAIN_LOSS <> 0 THEN
       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4068, 
                                              L_ST_FX_GAIN_LOSS, 
                                              A_LSR_ACCOUNT.CURR_GAIN_LOSS_ACCT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              1, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              1,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;

       L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                              4069, 
                                              L_ST_FX_GAIN_LOSS, 
                                              A_LSR_ACCOUNT.ST_RECEIVABLE_ACCOUNT_ID, 
                                              A_GAIN_LOSS, 
                                              A_COMPANY_ID, 
                                              A_GL_POSTING_MO_YR,
                                              0, 
                                              A_GL_JC, 
                                              A_SOB_ID, 
                                              A_JE_METHOD_ID, 
                                              A_AMOUNT_TYPE, 
                                              A_REVERSAL_CONVENTION, 
                                              1,
                                              A_CURRENCY_FROM, 
                                              A_CURRENCY_TO, 
                                              A_MSG);
       IF L_RTN = -1 THEN
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         RETURN -1;
       END IF;
     END IF;
     
     RETURN 1;
     
   exception
     when OTHERS then
       PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
       RETURN -1;
   end F_CREATE_BACKDATED_JE_FOR_ILR;

   --**************************************************************************
   --                            F_CREATE_JE_FOR_ILR
   --**************************************************************************

   function F_CREATE_JE_FOR_ILR(A_ILR_ID   in number,
                                A_REVISION in number) return number is
     L_MSG                 varchar2(2000);
     L_RTN                 number;
     L_GL_POSTING_MO_YR    date;
     L_MONTH               date;
     L_CONT_COMP_RATE      number;
     L_CURRENCY_FROM       number;
     L_CURRENCY_TO         number;
     L_GL_JE_CODE          VARCHAR2(35);
     L_COMPANY_ID          NUMBER;
     L_LSR_ACCOUNT         LSR_ILR_ACCOUNT%rowtype;
     L_LSR_ILR_SCHEDULE    LSR_ILR_SCHEDULE%rowtype;
     L_AMOUNT              NUMBER(22,2);
     L_FAIR_MARKET_VALUE   NUMBER(22,2);
     L_CARRYING_COST       NUMBER(22,2);
     L_SELLING_PROFIT_LOSS NUMBER(22,2);
     L_INI_DIRECT_COST     NUMBER(22,2);
     L_POST_BACKDATED      VARCHAR2(3);
     L_BEG_LT_DEF_PROFIT   NUMBER(22,2);
   begin

     L_MSG := 'Getting the first month of the schedule';
     select MIN(lis.month)
       into L_MONTH
       from LSR_ILR_SCHEDULE LIS
      where LIS.ILR_ID = A_ILR_ID
        and LIS.REVISION = A_REVISION;

     if L_MONTH is null then
       L_MSG := 'Error getting first month of the ILR schedule';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
       return -1;
     end if;

     L_MSG:='Retrieving L_COMPANY_ID';
     select company_id into L_COMPANY_ID
     from lsr_ilr
     where ilr_id = A_ILR_ID ;

     L_MSG := 'Getting the minimum open month for Lessor';
     select min(gl_posting_mo_yr)
       into L_GL_POSTING_MO_YR
       from lsr_process_control
      where open_next is null
        and company_id = L_COMPANY_ID;

     if L_GL_POSTING_MO_YR is null then
       L_MSG := 'Error getting minimum open month for Lessor company';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
       return -1;
     end if;

     -- Lessor ILR accounts
     select * 
       into L_LSR_ACCOUNT 
       from LSR_ILR_ACCOUNT
      where ILR_ID = A_ILR_ID;

     -- L_CURRENCY_FROM
     L_MSG:='Retrieving L_CURRENCY_FROM';
     SELECT contract_currency_id
       INTO L_CURRENCY_FROM
       FROM lsr_lease l, lsr_ilr i
      WHERE l.lease_id = i.lease_id
        AND i.ilr_id = A_ILR_ID;

     -- L_CURRENCY_TO
     L_MSG:='Retrieving L_CURRENCY_TO';
     SELECT currency_id
       INTO L_CURRENCY_TO
       FROM lsr_ilr ilr, currency_schema cs
      WHERE ilr.company_id = cs.company_id
        AND currency_type_id = 1
        AND ilr_id = A_ILR_ID;

     L_MSG:='Retrieving L_GL_JE_CODE';
     select gl_je_code
       into L_GL_JE_CODE
       from standard_journal_entries, gl_je_control
      where standard_journal_entries.je_id = gl_je_control.je_id
        and upper(ltrim(rtrim(process_id))) = 'LSR COMMENCEMENT';

     IF L_GL_JE_CODE IS NULL THEN
       L_MSG := 'Error Retrieving L_GL_JE_CODE';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
       RETURN -1;
     END IF;
     
     -- L_CONT_COMP_RATE.
     L_MSG:='Retrieving contract to company currency rate';
     SELECT cont_to_comp_rate
       INTO L_CONT_COMP_RATE
       FROM (
              SELECT DISTINCT First_Value(rate) OVER (PARTITION BY currency_from, currency_to 
                                                          ORDER BY exchange_date DESC) cont_to_comp_rate
               FROM currency_rate_default
              WHERE trunc(exchange_date) <= L_MONTH
                AND currency_from = L_CURRENCY_FROM
                AND currency_to = L_CURRENCY_TO
			          AND exchange_rate_type_id = 1 -- Actual exchange rate
            );

     IF L_CONT_COMP_RATE IS NULL THEN
       L_MSG := 'Error Retrieving exchange rate for contract to company currencies';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
       RETURN -1;
     END IF;

     --PKG_PP_LOG.P_WRITE_MESSAGE('CONVERSION RATE: ' || TO_CHAR(L_CONT_COMP_RATE));
     
     -- Check to see whether to handle backdated commencement journals
     L_POST_BACKDATED := NVL(UPPER(TRIM(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Lessor: Allow Backdated JEs',
                                                                                          L_COMPANY_ID))), 
                             'YES');

     L_MSG:='Retrieving JE Method, Set of books for ILR approval';
     for L_SOBS in ( 
       select distinct JM.JE_METHOD_ID        as JE_METHOD_ID,
              JM.AMOUNT_TYPE         as AMOUNT_TYPE,
              JMSOB.SET_OF_BOOKS_ID     as SET_OF_BOOKS_ID,
              JMSOB.REVERSAL_CONVENTION as REVERSAL_CONVENTION,
              LFTS.FASB_CAP_TYPE_ID as FASB_CAP_TYPE_ID,
              LFCT.DESCRIPTION as CAP_TYPE_DESCRIPTION,
              LFTS.INCLUDE_IDC_SW,
              LFTS.SEPARATE_COST_OF_GOODS_SW
         from JE_METHOD JM
        inner join JE_METHOD_SET_OF_BOOKS JMSOB ON JMSOB.JE_METHOD_ID = JM.JE_METHOD_ID
        inner join JE_METHOD_TRANS_TYPE JMTT ON JMTT.JE_METHOD_ID = JM.JE_METHOD_ID
        inner join LSR_FASB_TYPE_SOB LFTS ON LFTS.SET_OF_BOOKS_ID = JMSOB.SET_OF_BOOKS_ID
        INNER JOIN LSR_FASB_CAP_TYPE LFCT ON LFCT.FASB_CAP_TYPE_ID = LFTS.FASB_CAP_TYPE_ID
        INNER JOIN LSR_ILR_OPTIONS LIO ON LIO.LEASE_CAP_TYPE_ID = LFTS.CAP_TYPE_ID
        INNER JOIN LSR_ILR ILR ON ILR.ILR_ID = LIO.ILR_ID
        INNER JOIN COMPANY_JE_METHOD_VIEW CJMV ON CJMV.COMPANY_ID = ILR.COMPANY_ID AND CJMV.JE_METHOD_ID = JM.JE_METHOD_ID
        where ILR.ILR_ID = A_ILR_ID
          and LIO.revision = A_REVISION
          and JMTT.TRANS_TYPE IN (
                SELECT DISTINCT JE_METHOD_ID FROM JE_METHOD_TRANS_TYPE
                 WHERE TRANS_TYPE BETWEEN 4001 and 4022
                    OR TRANS_TYPE in (4027, 4028, 4051, 4052, 4055, 4056, 4057, 4068, 4069, 4079, 4080, 4081))
          AND LFCT.DESCRIPTION IN ('Operating', 'Sales Type', 'Direct Finance') 
     )
     loop
       L_MSG:='Retrieving Schedule for the first month';
       select LIS.*
         into L_LSR_ILR_SCHEDULE
         from LSR_ILR_SCHEDULE LIS
        where LIS.ILR_ID = A_ILR_ID
          and LIS.REVISION = A_REVISION
          and LIS.SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID
          and LIS.MONTH = L_MONTH;

       --Carrying Cost
       L_MSG:='Retrieving carrying cost in company currency';
       select sum(nvl(CARRYING_COST_COMP_CURR,0)) 
         into L_AMOUNT
         from lsr_asset
        where ilr_id = A_ILR_ID
          and revision = A_REVISION;
       L_CARRYING_COST := L_AMOUNT;

       --Fair Market Value
       L_MSG:='Retrieving fair market value in company currency';
       select sum(nvl(FAIR_MARKET_VALUE_COMP_CURR,0)) 
         into L_FAIR_MARKET_VALUE
         from lsr_asset
        where ilr_id = A_ILR_ID
          and revision = A_REVISION;

       --Operating Schedule Specific JE Creations
       IF L_SOBS.CAP_TYPE_DESCRIPTION = 'Operating' THEN
         L_MSG:='Retrieving INITIAL DIRECT COST';
         SELECT NVL(SUM(AMOUNT), 0) 
           INTO L_AMOUNT
           FROM LSR_ILR_INITIAL_DIRECT_COST
          WHERE ILR_ID = A_ILR_ID
            AND REVISION  = A_REVISION;

          -- Initial Direct Cost DR/CR  
          if L_AMOUNT <> 0 then
            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                   4007, 
                                                   L_AMOUNT, 
                                                   L_LSR_ACCOUNT.INCURRED_COSTS_ACCOUNT_ID, 
                                                   0, 
                                                   L_COMPANY_ID, 
                                                   L_GL_POSTING_MO_YR,
                                                   0, 
                                                   L_GL_JE_CODE, 
                                                   L_SOBS.SET_OF_BOOKS_ID, 
                                                   L_SOBS.JE_METHOD_ID, 
                                                   L_SOBS.AMOUNT_TYPE, 
                                                   L_SOBS.REVERSAL_CONVENTION, 
                                                   L_CONT_COMP_RATE,
                                                   L_CURRENCY_FROM, 
                                                   L_CURRENCY_TO, 
                                                   L_MSG);
            if L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              return -1;
            end if;

            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                   4008, 
                                                   L_AMOUNT, 
                                                   L_LSR_ACCOUNT.DEF_COSTS_ACCOUNT_ID, 
                                                   0, 
                                                   L_COMPANY_ID, 
                                                   L_GL_POSTING_MO_YR,
                                                   1, 
                                                   L_GL_JE_CODE, 
                                                   L_SOBS.SET_OF_BOOKS_ID, 
                                                   L_SOBS.JE_METHOD_ID, 
                                                   L_SOBS.AMOUNT_TYPE, 
                                                   L_SOBS.REVERSAL_CONVENTION, 
                                                   L_CONT_COMP_RATE,
                                                   L_CURRENCY_FROM, 
                                                   L_CURRENCY_TO, 
                                                   L_MSG);
            if L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              return -1;
            end if;
          end if; -- Initial Direct Cost DR/CR  

       --Sales Type and Direct Finance Specific JE Creations
       ELSIF (L_SOBS.CAP_TYPE_DESCRIPTION = 'Sales Type' OR L_SOBS.CAP_TYPE_DESCRIPTION = 'Direct Finance') then
         L_MSG:='Retrieving SELLING PROFIT LOSS';
         SELECT NVL(SELLING_PROFIT_LOSS,0) 
           INTO L_SELLING_PROFIT_LOSS
           FROM LSR_ILR_AMOUNTS
          WHERE ILR_ID = A_ILR_ID
            AND REVISION  = A_REVISION
            AND SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID;

         --Send Carrying Cost JE's 4001 and then either 4005 or 4006 or 4055
         if L_CARRYING_COST <> 0 then
           L_RTN := F_CREATE_JE_WITH_PROFIT_LOSS
					(
						A_ILR_ID,
						4001, 
						L_CARRYING_COST, 
						L_LSR_ACCOUNT.PROP_PLANT_ACCOUNT_ID, 
						L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID, 
						L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
						0 /*GAIN_LOSS*/, 
						L_COMPANY_ID, 
						L_GL_POSTING_MO_YR,
						0 /* DR_CR_INDICATOR*/,
						L_GL_JE_CODE, 
						L_SOBS.SET_OF_BOOKS_ID, 
						L_SOBS.JE_METHOD_ID, 
						L_SOBS.AMOUNT_TYPE, 
						L_SOBS.REVERSAL_CONVENTION, 
						1.0 /*exchange raate*/,
						L_CURRENCY_FROM, 
						L_CURRENCY_TO, 
						L_SELLING_PROFIT_LOSS, 
						L_SOBS.FASB_CAP_TYPE_ID, 
						L_SOBS.SEPARATE_COST_OF_GOODS_SW, 
						'PPE',
						L_LSR_ACCOUNT.COST_OF_GOODS_SOLD_ACCOUNT_ID, 
						L_LSR_ACCOUNT.REVENUE_ACCOUNT_ID, 
						L_MSG
					);
					
           if L_RTN = -1 then
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             return -1;
           end if;
         end if; --Send Carrying Cost JE's 4001 and then either 4005 or 4006 or 4055

         --Send Beginning Receivable JE's 4002 and then either 4005 or 4006 or 4055
         L_AMOUNT := NVL( L_LSR_ILR_SCHEDULE.BEG_RECEIVABLE, 0);
         if L_AMOUNT <> 0 then
           L_RTN := F_CREATE_JE_WITH_PROFIT_LOSS
					(
						A_ILR_ID,
						4002, 
						L_AMOUNT, 
						L_LSR_ACCOUNT.ST_RECEIVABLE_ACCOUNT_ID,
						L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID, 
						L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
						0 /*GAIN_LOSS*/, 
						L_COMPANY_ID, 
						L_GL_POSTING_MO_YR,
						1 /* DR_CR_INDICATOR*/,
						L_GL_JE_CODE, 
						L_SOBS.SET_OF_BOOKS_ID, 
						L_SOBS.JE_METHOD_ID, 
						L_SOBS.AMOUNT_TYPE, 
						L_SOBS.REVERSAL_CONVENTION, 
						L_CONT_COMP_RATE /*exchange raate*/,
						L_CURRENCY_FROM, 
						L_CURRENCY_TO, 
						L_SELLING_PROFIT_LOSS, 
						L_SOBS.FASB_CAP_TYPE_ID, 
						L_SOBS.SEPARATE_COST_OF_GOODS_SW, 
						'ST RECEIVABLE',
						L_LSR_ACCOUNT.COST_OF_GOODS_SOLD_ACCOUNT_ID, 
						L_LSR_ACCOUNT.REVENUE_ACCOUNT_ID, 
						L_MSG
					);
					
           if L_RTN = -1 then
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             return -1;
           end if;
         end if; --Send Beginning Receivable JE's 4002 and then either 4005 or 4006 or 4055

         --Send Long Term Receivable JE's 4003 Debit(1), but Short Term Receivable 4009 Credit(0)
         L_AMOUNT := nvl(L_LSR_ILR_SCHEDULE.BEG_LT_RECEIVABLE, 0);
         if L_AMOUNT <> 0 then
           L_RTN := F_JE_DR_CR(A_ILR_ID, 
							L_AMOUNT,
							L_LSR_ACCOUNT.LT_RECEIVABLE_ACCOUNT_ID, 
							L_LSR_ACCOUNT.ST_RECEIVABLE_ACCOUNT_ID,
							4003,
							4009,
							L_COMPANY_ID, 
							L_GL_POSTING_MO_YR,
							L_GL_JE_CODE, 
							L_SOBS.SET_OF_BOOKS_ID, 
							L_SOBS.JE_METHOD_ID, 
							L_SOBS.AMOUNT_TYPE, 
							L_SOBS.REVERSAL_CONVENTION, 
							L_CONT_COMP_RATE,
							L_CURRENCY_FROM, 
							L_CURRENCY_TO, 
							L_MSG);
           if L_RTN = -1 then
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             return -1;
           end if;

         end if; --Send Long Term Receivable JE's 4003 Debit(1), but Short Term Receivable 4009 Credit(0)

         --Send Unguaranteed Residual Account JE's 4004 and then either 4005 or 4006 or 4055
         L_MSG:='Retrieving NPV UNGUARANTEED RESIDUAL';
         SELECT NVL(NPV_UNGUARANTEED_RESIDUAL, 0) 
           INTO L_AMOUNT
           FROM LSR_ILR_AMOUNTS
          WHERE ILR_ID = A_ILR_ID
            AND REVISION  = A_REVISION
            AND SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID;

         if L_AMOUNT <> 0 then
           L_RTN := F_CREATE_JE_WITH_PROFIT_LOSS
					(
						A_ILR_ID,
						4004, 
						L_AMOUNT,
						L_LSR_ACCOUNT.UNGUARAN_RES_ACCOUNT_ID,
						L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID, 
						L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
						0 /*GAIN_LOSS*/, 
						L_COMPANY_ID, 
						L_GL_POSTING_MO_YR,
						1 /* DR_CR_INDICATOR*/,
						L_GL_JE_CODE, 
						L_SOBS.SET_OF_BOOKS_ID, 
						L_SOBS.JE_METHOD_ID, 
						L_SOBS.AMOUNT_TYPE, 
						L_SOBS.REVERSAL_CONVENTION, 
						L_CONT_COMP_RATE /*exchange raate*/,
						L_CURRENCY_FROM, 
						L_CURRENCY_TO, 
						L_SELLING_PROFIT_LOSS, 
						L_SOBS.FASB_CAP_TYPE_ID, 
						L_SOBS.SEPARATE_COST_OF_GOODS_SW, 
						'UNGUARANTEED RESIDUAL',
						L_LSR_ACCOUNT.COST_OF_GOODS_SOLD_ACCOUNT_ID, 
						L_LSR_ACCOUNT.REVENUE_ACCOUNT_ID, 
						L_MSG
					);
	   
           if L_RTN = -1 then
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             return -1;
           end if;

         end if; --Send Unguaranteed Residual Account JE's 4004 and then either 4005 or 4006 or 4055

         --Send Initial Direct Costs JE's 4007 and then either 4005 or 4006 or 4055
         L_MSG:='Retrieving INITIAL DIRECT COST';
         SELECT NVL(SUM(AMOUNT), 0) 
           INTO L_AMOUNT
           FROM LSR_ILR_INITIAL_DIRECT_COST
          WHERE ILR_ID = A_ILR_ID
            AND REVISION  = A_REVISION;

         -- Initial Direct Cost DR/CR  
         if L_AMOUNT <> 0 then
           L_RTN := F_JE_IDC
					(
						A_ILR_ID,
						4007,
						L_AMOUNT,
						L_LSR_ACCOUNT.DEF_COSTS_ACCOUNT_ID, 
						L_LSR_ACCOUNT.INI_DIRECT_COST_ACCOUNT_ID,
						L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID, 
						L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
						0 /*GAIN_LOSS*/, 
						L_COMPANY_ID, 
						L_GL_POSTING_MO_YR,
						L_GL_JE_CODE, 
						L_SOBS.SET_OF_BOOKS_ID, 
						L_SOBS.JE_METHOD_ID, 
						L_SOBS.AMOUNT_TYPE, 
						L_SOBS.REVERSAL_CONVENTION, 
						L_CONT_COMP_RATE /*exchange raate*/,
						L_CURRENCY_FROM, 
						L_CURRENCY_TO, 
						L_SELLING_PROFIT_LOSS, 
						L_SOBS.FASB_CAP_TYPE_ID, 
						L_SOBS.SEPARATE_COST_OF_GOODS_SW, 
						L_LSR_ACCOUNT.COST_OF_GOODS_SOLD_ACCOUNT_ID, 
						L_LSR_ACCOUNT.REVENUE_ACCOUNT_ID,
						(L_SOBS.FASB_CAP_TYPE_ID <> 2 or (L_SOBS.FASB_CAP_TYPE_ID = 2 and (L_CARRYING_COST = L_FAIR_MARKET_VALUE OR L_SOBS.INCLUDE_IDC_SW = 1))),
						L_MSG
					);
			
           if L_RTN = -1 then
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             return -1;
           end if;
         end if; -- Initial Direct Cost DR/CR  

         -- 4055/4079 Deferred Selling Profit DR/LT Deferred Selling Profit CR
         -- LSR_ILR_SCHEDULE:	BEGIN_LT_DEFERRED_PROFIT
         -- DR Acct: ST Deferred Selling Profit (DEF_SELLING_PROFIT_ACCOUNT_ID)
         -- CR Acct: LT Deferred Selling Profit (LT_DEF_SELL_PROFIT_ACCOUNT_ID)
         begin
           select BEGIN_LT_DEFERRED_PROFIT
             into L_BEG_LT_DEF_PROFIT
             from LSR_ILR_SCHEDULE_DIRECT_FIN
            where ILR_ID = A_ILR_ID
              and REVISION = A_REVISION
              and SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID
              and MONTH = L_MONTH;
         exception
           when no_data_found then
             L_BEG_LT_DEF_PROFIT := 0;
         end;
          
         L_MSG := 'Processing 4055/4079 Deferred Selling Profit DR/LT Deferred Selling Profit CR';
         IF L_BEG_LT_DEF_PROFIT <> 0 THEN
           L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                  4055, 
                                                  L_BEG_LT_DEF_PROFIT, 
                                                  L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
                                                  0, 
                                                  L_COMPANY_ID, 
                                                  L_GL_POSTING_MO_YR,
                                                  0, 
                                                  L_GL_JE_CODE, 
                                                  L_SOBS.SET_OF_BOOKS_ID, 
                                                  L_SOBS.JE_METHOD_ID, 
                                                  L_SOBS.AMOUNT_TYPE, 
                                                  L_SOBS.REVERSAL_CONVENTION, 
                                                  L_CONT_COMP_RATE,
                                                  L_CURRENCY_FROM, 
                                                  L_CURRENCY_TO, 
                                                  L_MSG);
           IF L_RTN = -1 THEN
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             RETURN -1;
           END IF;

           L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                  4079, 
                                                  L_BEG_LT_DEF_PROFIT, 
                                                  L_LSR_ACCOUNT.LT_DEF_SELL_PROFIT_ACCOUNT_ID, 
                                                  0, 
                                                  L_COMPANY_ID, 
                                                  L_GL_POSTING_MO_YR,
                                                  1, 
                                                  L_GL_JE_CODE, 
                                                  L_SOBS.SET_OF_BOOKS_ID, 
                                                  L_SOBS.JE_METHOD_ID, 
                                                  L_SOBS.AMOUNT_TYPE, 
                                                  L_SOBS.REVERSAL_CONVENTION, 
                                                  L_CONT_COMP_RATE,
                                                  L_CURRENCY_FROM, 
                                                  L_CURRENCY_TO, 
                                                  L_MSG);
           IF L_RTN = -1 THEN
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             RETURN -1;
           END IF;
         END IF;
       END IF; --Operating Schedule Specific JE Creations
       
       -- Process Backdated True-Up journals
       IF L_MONTH < L_GL_POSTING_MO_YR and L_POST_BACKDATED = 'YES' THEN
         FOR BACK_MONTH in (
           select add_months(start_date, (level - 1)) sched_month
             from (select L_MONTH start_date, 
                          L_GL_POSTING_MO_YR end_date 
                     from dual)
            where end_date > start_date
          connect by level < ceil(months_between(add_months(end_date,1), start_date))	
         )
         LOOP
           L_RTN := PKG_LESSOR_APPROVAL.F_CREATE_BACKDATED_JE_FOR_ILR(A_ILR_ID,
                                                                      A_REVISION,
                                                                      L_LSR_ACCOUNT, 
                                                                      0, 
                                                                      L_COMPANY_ID, 
                                                                      BACK_MONTH.SCHED_MONTH,
                                                                      L_GL_POSTING_MO_YR,
                                                                      L_GL_JE_CODE, 
                                                                      L_SOBS.SET_OF_BOOKS_ID, 
                                                                      L_SOBS.JE_METHOD_ID, 
                                                                      L_SOBS.AMOUNT_TYPE, 
                                                                      L_SOBS.REVERSAL_CONVENTION, 
                                                                      L_CURRENCY_FROM, 
                                                                      L_CURRENCY_TO,  
                                                                      L_SOBS.FASB_CAP_TYPE_ID, 
                                                                      L_MSG);
           
           IF L_RTN = -1 THEN
             PKG_PP_LOG.P_WRITE_MESSAGE('BACKDATED JEs for Month '||to_char(BACK_MONTH.SCHED_MONTH)||': '||L_MSG);
             return -1;
           END IF;
         END LOOP;
       END IF;

     end loop;
     
     return 1;
   exception
     when others then
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG ||': '|| sqlerrm);
       RAISE_APPLICATION_ERROR(-20000, 'Error Approving ILR: ' || L_MSG ||': '|| sqlerrm);
       ROLLBACK;
       return -1;

   end F_CREATE_JE_FOR_ILR;

   --**************************************************************************
   --                            F_CREATE_JE_RECLASS_ILR
   --**************************************************************************
   /*****************************************************************************
   * Function: F_CREATE_JE_RECLASS_ILR
   * PURPOSE: creates JEs for set of books that have a classification change
   *			from Operating to either Direct Fincance or Sales Type
   * PARAMETERS:
   *   a_ilr_id: The ILR ID for the ILR for which to retrieve information
   *   a_revision: The revision of the ILR for which to retrieve information
   *   a_prior_revision: The prior revision of the ILR for which to retrieve information
   * RETURNS: Number
   ******************************************************************************/
   function F_CREATE_JE_RECLASS_ILR(A_ILR_ID   in number,
                                A_REVISION in number,
								A_PRIOR_REVISION in number) return number is
     L_MSG                 varchar2(2000);
     L_RTN                 number;
     L_GL_POSTING_MO_YR    date;
	 L_MONTH               date;
	 L_COUNT				number;
     L_CONT_COMP_RATE      number;
     L_CURRENCY_FROM       number;
     L_CURRENCY_TO         number;
     L_GL_JE_CODE          VARCHAR2(35);
     L_COMPANY_ID          NUMBER;
     L_LSR_ACCOUNT         LSR_ILR_ACCOUNT%rowtype;
     L_LSR_ILR_SCHEDULE    LSR_ILR_SCHEDULE%rowtype;
     L_LSR_ILR_SCHEDULE_PR LSR_ILR_SCHEDULE%rowtype;
     L_AMOUNT              NUMBER(22,2);
     L_FAIR_MARKET_VALUE   NUMBER(22,2);
     L_CARRYING_COST       NUMBER(22,2);
     L_SELLING_PROFIT_LOSS NUMBER(22,2);
     L_INI_DIRECT_COST     NUMBER(22,2);
     L_POST_BACKDATED      VARCHAR2(3);
     L_BEG_LT_DEF_PROFIT   NUMBER(22,2);
	 L_PRIOR_FASB		NUMBER;
	 L_HAS_RERECOGNIZED	NUMBER;
   begin
	 L_RTN := 0;
     L_HAS_RERECOGNIZED := 0;
	 
	 L_MSG := 'Getting the first month of the schedule';
     select REMEASUREMENT_DATE
     into L_MONTH
     from LSR_ILR_OPTIONS LIO
     where LIO.ILR_ID = A_ILR_ID
          and LIO.REVISION = A_REVISION;
		  
	 if L_MONTH is null then
       L_MSG := 'Error getting remeasurement month for the ILR revision';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
       return -1;
     end if;
          
     L_MSG:='Retrieving L_COMPANY_ID';
     select company_id into L_COMPANY_ID
     from lsr_ilr
     where ilr_id = A_ILR_ID ;

     if L_MONTH is null then
       L_MSG := 'Error getting remeasurement month for the ILR revision';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
       return -1;
     end if;

     select min(gl_posting_mo_yr)
     into L_GL_POSTING_MO_YR
     from lsr_process_control
     where open_next is null
     and company_id=L_COMPANY_ID;

     if L_GL_POSTING_MO_YR is null then
       L_MSG := 'Error getting minimum open month for Lessor company';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
       return -1;
     end if;


     -- Lessor ILR accounts
     select * into L_LSR_ACCOUNT from LSR_ILR_ACCOUNT where ILR_ID = A_ILR_ID;

     -- L_CURRENCY_FROM
     L_MSG:='Retrieving L_CURRENCY_FROM';
     SELECT contract_currency_id
     INTO L_CURRENCY_FROM
     FROM lsr_lease l, lsr_ilr i
     WHERE l.lease_id = i.lease_id
       AND i.ilr_id = A_ILR_ID;

            -- L_CURRENCY_TO
     L_MSG:='Retrieving L_CURRENCY_TO';
     SELECT currency_id
     INTO L_CURRENCY_TO
     FROM lsr_ilr ilr, currency_schema cs
     WHERE ilr.company_id = cs.company_id
       AND currency_type_id = 1
       AND ilr_id = A_ILR_ID;

     -- L_CONT_COMP_RATE.
     L_MSG:='Retrieving contract to company currency rate';
     SELECT cont_to_comp_rate
     INTO L_CONT_COMP_RATE
     FROM (
        SELECT DISTINCT First_Value(rate) OVER (PARTITION BY currency_from, currency_to ORDER BY exchange_date DESC) cont_to_comp_rate
        FROM currency_rate_default
        WHERE trunc(exchange_date) <= L_MONTH
          AND currency_from = L_CURRENCY_FROM
          AND currency_to = L_CURRENCY_TO
		  AND exchange_rate_type_id = 1 -- Actual exchange rate
          ) ;

      IF L_CONT_COMP_RATE IS NULL THEN
        L_MSG := 'Error Retrieving exchange rate for contract to company currencies';
        PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
        RETURN -1;
      END IF;
	  
	  -- now that the exhcange rate has been gotten, trunc the month
	  L_MONTH := trunc(L_MONTH, 'mm');

      L_MSG:='Retrieving L_GL_JE_CODE';
      select gl_je_code
      into L_GL_JE_CODE
      from standard_journal_entries, gl_je_control
      where standard_journal_entries.je_id = gl_je_control.je_id
      and upper(ltrim(rtrim(process_id))) = 'LSR REMEASUREMENT';

      IF L_GL_JE_CODE IS NULL THEN
        L_MSG := 'Error Retrieving L_GL_JE_CODE';
        PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
        RETURN -1;
      END IF;

     L_MSG:='Retrieving JE Method, Set of books for ILR approval';
     for L_SOBS in ( 
       select distinct JM.JE_METHOD_ID        as JE_METHOD_ID,
              JM.AMOUNT_TYPE         as AMOUNT_TYPE,
              JMSOB.SET_OF_BOOKS_ID     as SET_OF_BOOKS_ID,
              JMSOB.REVERSAL_CONVENTION as REVERSAL_CONVENTION,
              LFTS.FASB_CAP_TYPE_ID as FASB_CAP_TYPE_ID,
              LFCT.DESCRIPTION as CAP_TYPE_DESCRIPTION,
              LFTS.INCLUDE_IDC_SW,
              LFTS.SEPARATE_COST_OF_GOODS_SW
         from JE_METHOD JM
        inner join JE_METHOD_SET_OF_BOOKS JMSOB ON JMSOB.JE_METHOD_ID = JM.JE_METHOD_ID
        inner join JE_METHOD_TRANS_TYPE JMTT ON JMTT.JE_METHOD_ID = JM.JE_METHOD_ID
        inner join LSR_FASB_TYPE_SOB LFTS ON LFTS.SET_OF_BOOKS_ID = JMSOB.SET_OF_BOOKS_ID
        INNER JOIN LSR_FASB_CAP_TYPE LFCT ON LFCT.FASB_CAP_TYPE_ID = LFTS.FASB_CAP_TYPE_ID
        INNER JOIN LSR_ILR_OPTIONS LIO ON LIO.LEASE_CAP_TYPE_ID = LFTS.CAP_TYPE_ID
        INNER JOIN LSR_ILR ILR ON ILR.ILR_ID = LIO.ILR_ID
        INNER JOIN COMPANY_JE_METHOD_VIEW CJMV ON CJMV.COMPANY_ID = ILR.COMPANY_ID AND CJMV.JE_METHOD_ID = JM.JE_METHOD_ID
        where ILR.ILR_ID = A_ILR_ID
          and LIO.revision = A_REVISION
		  -- check has reclass is yes
		  and f_has_class_change(a_ilr_id, a_revision, a_prior_revision, JMSOB.SET_OF_BOOKS_ID) = 1
		  and JMTT.TRANS_TYPE IN (
                SELECT DISTINCT JE_METHOD_ID FROM JE_METHOD_TRANS_TYPE
                 WHERE TRANS_TYPE in (4001, 4002, 4003, 4004, 4005, 4006, 4008, 4009, 4041, 4042, 4043, 4044, 4045, 4055,
									4060, 4061, 4062, 4063, 4066, 4067, 4070, 4071, 4072, 4073, 4079, 4080, 4081, 4084, 4085))
          AND LFCT.DESCRIPTION IN ('Operating', 'Sales Type', 'Direct Finance')
		order by JMSOB.SET_OF_BOOKS_ID
     )
     loop
		PKG_PP_LOG.P_WRITE_MESSAGE('Processing Reclassification for Set of books: ' || to_char(L_SOBS.SET_OF_BOOKS_ID));
	  
	  select fasb.fasb_cap_type_id
	  into L_PRIOR_FASB
	  from lsr_fasb_type_sob fasb
	  join lsr_ilr_options ilr_opt on ilr_opt.lease_cap_type_id = fasb.cap_type_id
	  where ilr_opt.ilr_id = A_ILR_ID
	  and ilr_opt.revision = A_PRIOR_REVISION
	  and fasb.set_of_books_id = L_SOBS.SET_OF_BOOKS_ID
	  ;

       L_MSG:='Retrieving Schedule for the reclassification month';
       select LIS.*
         into L_LSR_ILR_SCHEDULE
         from LSR_ILR_SCHEDULE LIS
        where LIS.ILR_ID = A_ILR_ID
          and LIS.REVISION = A_REVISION
          and LIS.SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID
          and LIS.MONTH = L_MONTH;
		  
	   L_MSG:='Retrieving Schedule for the month prior to reclassification';
       select LIS.*
         into L_LSR_ILR_SCHEDULE_PR
         from LSR_ILR_SCHEDULE LIS
        where LIS.ILR_ID = A_ILR_ID
          and LIS.REVISION = A_REVISION
          and LIS.SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID
          and LIS.MONTH = ADD_MONTHS(L_MONTH, -1);

       --Carrying Cost
       L_MSG:='Retrieving carrying cost in company currency';
       select sum(nvl(CARRYING_COST_COMP_CURR,0)) 
         into L_AMOUNT
         from lsr_asset
        where ilr_id = A_ILR_ID
          and revision = A_REVISION;
       L_CARRYING_COST := L_AMOUNT;

       --Fair Market Value
       L_MSG:='Retrieving fair market value in company currency';
       select sum(nvl(FAIR_MARKET_VALUE_COMP_CURR,0)) 
         into L_FAIR_MARKET_VALUE
         from lsr_asset
        where ilr_id = A_ILR_ID
          and revision = A_REVISION;

		L_MSG:='Retrieving SELLING PROFIT LOSS';
		begin
			SELECT NVL(SELLING_PROFIT_LOSS,0) 
			INTO L_SELLING_PROFIT_LOSS
			FROM LSR_ILR_AMOUNTS
			WHERE ILR_ID = A_ILR_ID
			AND REVISION  = A_REVISION
			AND SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID;
		exception
		when no_data_found then
			L_SELLING_PROFIT_LOSS := 0;
		end;

		--Operating Schedule Specific JE Creations
       IF L_SOBS.CAP_TYPE_DESCRIPTION = 'Operating' THEN
			-- This is from Sales type or DF to Operating.  RE recognize the asset
			SELECT COUNT(1)
			INTO l_count
			FROM lsr_asset
			WHERE ilr_id = A_ILR_ID
			AND revision = A_REVISION
			AND cpr_asset_id is not null;

			if l_count > 0 and L_HAS_RERECOGNIZED = 0 then
				L_RTN := F_RERECOGNIZE_CPR_ASSETS(A_ILR_ID, A_REVISION, L_MONTH);
				If L_RTN = -1 then
					PKG_PP_LOG.P_WRITE_MESSAGE('P_RERECOGNIZE_CPR_ASSETS returned an error.');
					return -1;
				end if;
				L_HAS_RERECOGNIZED := 1;
			end if;
	   
	   
			-- Receivable Remeasurement.
			L_AMOUNT := -1 * NVL( L_LSR_ILR_SCHEDULE.RECEIVABLE_REMEASUREMENT, 0);
			if L_AMOUNT <> 0 then
				L_RTN := F_JE_DR_CR(A_ILR_ID, 
									  L_AMOUNT,
									  L_LSR_ACCOUNT.PROP_PLANT_ACCOUNT_ID, 
									  L_LSR_ACCOUNT.ST_RECEIVABLE_ACCOUNT_ID,
									  4060,
									  4061,
									  L_COMPANY_ID, 
									  L_GL_POSTING_MO_YR,
									  L_GL_JE_CODE, 
									  L_SOBS.SET_OF_BOOKS_ID, 
									  L_SOBS.JE_METHOD_ID, 
									  L_SOBS.AMOUNT_TYPE, 
									  L_SOBS.REVERSAL_CONVENTION, 
									  L_CONT_COMP_RATE,
									  L_CURRENCY_FROM, 
									  L_CURRENCY_TO, 
									  L_MSG);
				
				if L_RTN = -1 then
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
					return -1;
				end if;
			end if;
			
			-- Long Term Receivable Remeasurement .
			L_AMOUNT := -1 * NVL( L_LSR_ILR_SCHEDULE.LT_RECEIVABLE_REMEASUREMENT, 0);
			if L_AMOUNT <> 0 then
				L_RTN := F_JE_DR_CR(A_ILR_ID, 
									  L_AMOUNT,
									  L_LSR_ACCOUNT.ST_RECEIVABLE_ACCOUNT_ID, 
									  L_LSR_ACCOUNT.LT_RECEIVABLE_ACCOUNT_ID,
									  4084,
									  4085,
									  L_COMPANY_ID, 
									  L_GL_POSTING_MO_YR,
									  L_GL_JE_CODE, 
									  L_SOBS.SET_OF_BOOKS_ID, 
									  L_SOBS.JE_METHOD_ID, 
									  L_SOBS.AMOUNT_TYPE, 
									  L_SOBS.REVERSAL_CONVENTION, 
									  L_CONT_COMP_RATE,
									  L_CURRENCY_FROM, 
									  L_CURRENCY_TO, 
									  L_MSG);
				
				if L_RTN = -1 then
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
					return -1;
				end if;
			end if;
			
			-- Unguaranteed Remeasurement.
			L_MSG:='Retrieving Unguaranteed Residual Remeasure';
			select -1 * nvl(UNGUARAN_RESIDUAL_REMEASURE, 0)
			into L_AMOUNT
			from lsr_ilr_schedule_sales_direct
			where ILR_ID = A_ILR_ID
			and REVISION = A_REVISION
			and SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID
			and MONTH = L_MONTH;
			
			if L_AMOUNT <> 0 then
				L_RTN := F_JE_DR_CR(A_ILR_ID, 
									  L_AMOUNT,
									  L_LSR_ACCOUNT.PROP_PLANT_ACCOUNT_ID, 
									  L_LSR_ACCOUNT.UNGUARAN_RES_ACCOUNT_ID,
									  4062,
									  4063,
									  L_COMPANY_ID, 
									  L_GL_POSTING_MO_YR,
									  L_GL_JE_CODE, 
									  L_SOBS.SET_OF_BOOKS_ID, 
									  L_SOBS.JE_METHOD_ID, 
									  L_SOBS.AMOUNT_TYPE, 
									  L_SOBS.REVERSAL_CONVENTION, 
									  L_CONT_COMP_RATE,
									  L_CURRENCY_FROM, 
									  L_CURRENCY_TO, 
									  L_MSG);
				
				if L_RTN = -1 then
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
					return -1;
				end if;
			end if;
			
			-- Deferred Profit Remeasurement.
			L_MSG:='Retrieving Deferred Profit Remeasure';
			select -1 * nvl(deferred_profit_remeasurement, 0)
			into L_AMOUNT
			from lsr_ilr_schedule_direct_fin
			where ILR_ID = A_ILR_ID
			and REVISION = A_REVISION
			and SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID
			and MONTH = L_MONTH;
				
			if L_AMOUNT <> 0 then
				L_RTN := F_JE_DR_CR(A_ILR_ID, 
									  L_AMOUNT,
									  L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
									  L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID,
									  4066,
									  4067,
									  L_COMPANY_ID, 
									  L_GL_POSTING_MO_YR,
									  L_GL_JE_CODE, 
									  L_SOBS.SET_OF_BOOKS_ID, 
									  L_SOBS.JE_METHOD_ID, 
									  L_SOBS.AMOUNT_TYPE, 
									  L_SOBS.REVERSAL_CONVENTION, 
									  L_CONT_COMP_RATE,
									  L_CURRENCY_FROM, 
									  L_CURRENCY_TO, 
									  L_MSG);
					
				if L_RTN = -1 then
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
					return -1;
				end if;
			end if;
				
			-- LT Deferred Profit Remeasurement.
			L_MSG:='Retrieving LT Deferred Profit Remeasure';
			select -1 * nvl(lt_deferred_profit_remeasure, 0)
			into L_AMOUNT
			from lsr_ilr_schedule_direct_fin
			where ILR_ID = A_ILR_ID
			and REVISION = A_REVISION
			and SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID
			and MONTH = L_MONTH;
				
			if L_AMOUNT <> 0 then
				L_RTN := F_JE_DR_CR(A_ILR_ID, 
									  L_AMOUNT,
									  L_LSR_ACCOUNT.LT_DEF_SELL_PROFIT_ACCOUNT_ID, 
									  L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID,
									  4080,
									  4081,
									  L_COMPANY_ID, 
									  L_GL_POSTING_MO_YR,
									  L_GL_JE_CODE, 
									  L_SOBS.SET_OF_BOOKS_ID, 
									  L_SOBS.JE_METHOD_ID, 
									  L_SOBS.AMOUNT_TYPE, 
									  L_SOBS.REVERSAL_CONVENTION, 
									  L_CONT_COMP_RATE,
									  L_CURRENCY_FROM, 
									  L_CURRENCY_TO, 
									  L_MSG);
					
				if L_RTN = -1 then
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
					return -1;
				end if;
			end if;
	   
	   --Sales Type and Direct Finance Specific JE Creations
       ELSIF (L_SOBS.CAP_TYPE_DESCRIPTION = 'Sales Type' OR L_SOBS.CAP_TYPE_DESCRIPTION = 'Direct Finance') then
         --Send Carrying Cost JE's 4001 and then either 4005 or 4006 or 4055
         if L_CARRYING_COST <> 0 AND L_PRIOR_FASB = 1 then
			L_RTN := F_CREATE_JE_WITH_PROFIT_LOSS
					(
						A_ILR_ID,
						4001, 
						L_CARRYING_COST, 
						L_LSR_ACCOUNT.PROP_PLANT_ACCOUNT_ID,
						L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID, 
						L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
						0 /*GAIN_LOSS*/, 
						L_COMPANY_ID, 
						L_GL_POSTING_MO_YR,
						0 /* DR_CR_INDICATOR*/,
						L_GL_JE_CODE, 
						L_SOBS.SET_OF_BOOKS_ID, 
						L_SOBS.JE_METHOD_ID, 
						L_SOBS.AMOUNT_TYPE, 
						L_SOBS.REVERSAL_CONVENTION, 
						1.0 /*exchange raate*/,
						L_CURRENCY_FROM, 
						L_CURRENCY_TO, 
						L_SELLING_PROFIT_LOSS, 
						L_SOBS.FASB_CAP_TYPE_ID, 
						L_SOBS.SEPARATE_COST_OF_GOODS_SW, 
						'PPE',
						L_LSR_ACCOUNT.COST_OF_GOODS_SOLD_ACCOUNT_ID, 
						L_LSR_ACCOUNT.REVENUE_ACCOUNT_ID, 
						L_MSG
					);

           if L_RTN = -1 then
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             return -1;
           end if;
         end if; --Send Carrying Cost JE's 4001 and then either 4005 or 4006 or 4055

         --Send Beginning Receivable JE's 4002 and then either 4005 or 4006 or 4055
         L_AMOUNT := NVL( L_LSR_ILR_SCHEDULE.RECEIVABLE_REMEASUREMENT, 0);
         if L_AMOUNT <> 0 then		 
			L_RTN := F_CREATE_JE_WITH_PROFIT_LOSS
					(
						A_ILR_ID,
						case when L_PRIOR_FASB = 1 then 4002 else 4070 end, 
						L_AMOUNT,
						L_LSR_ACCOUNT.ST_RECEIVABLE_ACCOUNT_ID,
						L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID, 
						L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
						0 /*GAIN_LOSS*/, 
						L_COMPANY_ID, 
						L_GL_POSTING_MO_YR,
						1 /* DR_CR_INDICATOR*/,
						L_GL_JE_CODE, 
						L_SOBS.SET_OF_BOOKS_ID, 
						L_SOBS.JE_METHOD_ID, 
						L_SOBS.AMOUNT_TYPE, 
						L_SOBS.REVERSAL_CONVENTION, 
						L_CONT_COMP_RATE /*exchange raate*/,
						L_CURRENCY_FROM, 
						L_CURRENCY_TO, 
						L_SELLING_PROFIT_LOSS, 
						L_SOBS.FASB_CAP_TYPE_ID, 
						L_SOBS.SEPARATE_COST_OF_GOODS_SW, 
						'ST RECEIVABLE',
						L_LSR_ACCOUNT.COST_OF_GOODS_SOLD_ACCOUNT_ID, 
						L_LSR_ACCOUNT.REVENUE_ACCOUNT_ID, 
						L_MSG
					);
		 
           if L_RTN = -1 then
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             return -1;
           end if;
         end if; --Send Beginning Receivable JE's 4002 and then either 4005 or 4006 or 4055

         --Send Long Term Receivable JE's 4003 Debit(1), but Short Term Receivable 4009 Credit(0)
         L_AMOUNT := nvl(L_LSR_ILR_SCHEDULE.LT_RECEIVABLE_REMEASUREMENT, 0);
         if L_AMOUNT <> 0 then
			L_RTN := F_JE_DR_CR(A_ILR_ID, 
									  L_AMOUNT,
									  L_LSR_ACCOUNT.LT_RECEIVABLE_ACCOUNT_ID, 
									  L_LSR_ACCOUNT.ST_RECEIVABLE_ACCOUNT_ID,
									  case when L_PRIOR_FASB = 1 then 4003 else 4072 end,
									  case when L_PRIOR_FASB = 1 then 4009 else 4071 end,
									  L_COMPANY_ID, 
									  L_GL_POSTING_MO_YR,
									  L_GL_JE_CODE, 
									  L_SOBS.SET_OF_BOOKS_ID, 
									  L_SOBS.JE_METHOD_ID, 
									  L_SOBS.AMOUNT_TYPE, 
									  L_SOBS.REVERSAL_CONVENTION, 
									  L_CONT_COMP_RATE,
									  L_CURRENCY_FROM, 
									  L_CURRENCY_TO, 
									  L_MSG);
           if L_RTN = -1 then
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             return -1;
           end if;

         end if; --Send Long Term Receivable JE's 4003 Debit(1), but Short Term Receivable 4009 Credit(0)

         --Send Unguaranteed Residual Account JE's 4004 and then either 4005 or 4006 or 4055
         L_MSG:='Retrieving Unguaranteed Residual Remeasure';
         select nvl(UNGUARAN_RESIDUAL_REMEASURE, 0)
             into L_AMOUNT
             from lsr_ilr_schedule_sales_direct
            where ILR_ID = A_ILR_ID
              and REVISION = A_REVISION
              and SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID
              and MONTH = L_MONTH;

         if L_AMOUNT <> 0 then
			L_RTN := F_CREATE_JE_WITH_PROFIT_LOSS
					(
						A_ILR_ID,
						case when L_PRIOR_FASB = 1 then 4004 else 4073 end, 
						L_AMOUNT,
						L_LSR_ACCOUNT.UNGUARAN_RES_ACCOUNT_ID,
						L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID, 
						L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
						0 /*GAIN_LOSS*/, 
						L_COMPANY_ID, 
						L_GL_POSTING_MO_YR,
						1 /* DR_CR_INDICATOR*/,
						L_GL_JE_CODE, 
						L_SOBS.SET_OF_BOOKS_ID, 
						L_SOBS.JE_METHOD_ID, 
						L_SOBS.AMOUNT_TYPE, 
						L_SOBS.REVERSAL_CONVENTION, 
						L_CONT_COMP_RATE /*exchange raate*/,
						L_CURRENCY_FROM, 
						L_CURRENCY_TO, 
						L_SELLING_PROFIT_LOSS, 
						L_SOBS.FASB_CAP_TYPE_ID, 
						L_SOBS.SEPARATE_COST_OF_GOODS_SW, 
						'UNGUARANTEED RESIDUAL',
						L_LSR_ACCOUNT.COST_OF_GOODS_SOLD_ACCOUNT_ID, 
						L_LSR_ACCOUNT.REVENUE_ACCOUNT_ID, 
						L_MSG
					);
	   
           if L_RTN = -1 then
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             return -1;
           end if;
        
         end if; --Send Unguaranteed Residual Account JE's 4004 and then either 4005 or 4006 or 4055

         --Send Initial Direct Costs JE's 4041 and then either 4005 or 4006 or 4055
		 -- unless it is an ST with L_CARRYING_COST <> L_FAIR_MARKET_VALUE and L_SOBS.INCLUDE_IDC_SW = 0
		 -- then use 4008
         L_MSG:='Retrieving UNAMORTIZED DIRECT COST';
		 SELECT NVL(unamortized_idc,0) 
           INTO L_AMOUNT
           FROM LSR_ILR_AMOUNTS
          WHERE ILR_ID = A_ILR_ID
            AND REVISION  = A_REVISION
            AND SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID;

         if L_AMOUNT <> 0 AND L_PRIOR_FASB = 1 then
			L_RTN := F_JE_IDC
					(
						A_ILR_ID,
						4041,
						L_AMOUNT, 
						L_LSR_ACCOUNT.DEF_COSTS_ACCOUNT_ID, 
						L_LSR_ACCOUNT.INI_DIRECT_COST_ACCOUNT_ID,
						L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID, 
						L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
						0 /*GAIN_LOSS*/, 
						L_COMPANY_ID, 
						L_GL_POSTING_MO_YR,
						L_GL_JE_CODE, 
						L_SOBS.SET_OF_BOOKS_ID, 
						L_SOBS.JE_METHOD_ID, 
						L_SOBS.AMOUNT_TYPE, 
						L_SOBS.REVERSAL_CONVENTION, 
						L_CONT_COMP_RATE /*exchange raate*/,
						L_CURRENCY_FROM, 
						L_CURRENCY_TO, 
						L_SELLING_PROFIT_LOSS, 
						L_SOBS.FASB_CAP_TYPE_ID, 
						L_SOBS.SEPARATE_COST_OF_GOODS_SW, 
						L_LSR_ACCOUNT.COST_OF_GOODS_SOLD_ACCOUNT_ID, 
						L_LSR_ACCOUNT.REVENUE_ACCOUNT_ID,
						(L_SOBS.FASB_CAP_TYPE_ID <> 2 or (L_SOBS.FASB_CAP_TYPE_ID = 2 and (L_CARRYING_COST = L_FAIR_MARKET_VALUE OR L_SOBS.INCLUDE_IDC_SW = 1))),
						L_MSG
					);
			
           if L_RTN = -1 then
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             return -1;
           end if;

         end if; -- Unamortized Direct Cost DR/CR  
         
		 --Send the Ending Accrued Rent from the period prior to reclass 
         L_AMOUNT := NVL( L_LSR_ILR_SCHEDULE_PR.END_ACCRUED_RENT, 0);
		 if L_AMOUNT <> 0 AND L_PRIOR_FASB = 1 then
			L_RTN := F_CREATE_JE_WITH_PROFIT_LOSS
					(
						A_ILR_ID,
						4045, 
						L_AMOUNT,
						L_LSR_ACCOUNT.ACCRUED_RENT_ACCT_ID,
						L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID, 
						L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
						0 /*GAIN_LOSS*/, 
						L_COMPANY_ID, 
						L_GL_POSTING_MO_YR,
						0 /* DR_CR_INDICATOR*/,
						L_GL_JE_CODE, 
						L_SOBS.SET_OF_BOOKS_ID, 
						L_SOBS.JE_METHOD_ID, 
						L_SOBS.AMOUNT_TYPE, 
						L_SOBS.REVERSAL_CONVENTION, 
						L_CONT_COMP_RATE /*exchange raate*/,
						L_CURRENCY_FROM, 
						L_CURRENCY_TO, 
						L_SELLING_PROFIT_LOSS, 
						L_SOBS.FASB_CAP_TYPE_ID, 
						L_SOBS.SEPARATE_COST_OF_GOODS_SW, 
						'ACCRUED',
						L_LSR_ACCOUNT.COST_OF_GOODS_SOLD_ACCOUNT_ID, 
						L_LSR_ACCOUNT.REVENUE_ACCOUNT_ID, 
						L_MSG
					);
			
		   if L_RTN = -1 then
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             return -1;
           end if;
         end if; -- ACCRUED RENT
		 
		 --Send the Ending Deferred Rent from the period prior to reclass 
         L_AMOUNT := NVL( L_LSR_ILR_SCHEDULE_PR.END_DEFERRED_RENT, 0);
         if L_AMOUNT <> 0 AND L_PRIOR_FASB = 1 then
			L_RTN := F_CREATE_JE_WITH_PROFIT_LOSS
					(
						A_ILR_ID,
						4042, 
						L_AMOUNT, 
						L_LSR_ACCOUNT.DEFERRED_RENT_ACCT_ID,
						L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID, 
						L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
						0 /*GAIN_LOSS*/, 
						L_COMPANY_ID, 
						L_GL_POSTING_MO_YR,
						1 /* DR_CR_INDICATOR*/,
						L_GL_JE_CODE, 
						L_SOBS.SET_OF_BOOKS_ID, 
						L_SOBS.JE_METHOD_ID, 
						L_SOBS.AMOUNT_TYPE, 
						L_SOBS.REVERSAL_CONVENTION, 
						L_CONT_COMP_RATE /*exchange raate*/,
						L_CURRENCY_FROM, 
						L_CURRENCY_TO, 
						L_SELLING_PROFIT_LOSS, 
						L_SOBS.FASB_CAP_TYPE_ID, 
						L_SOBS.SEPARATE_COST_OF_GOODS_SW, 
						'ACCRUED',
						L_LSR_ACCOUNT.COST_OF_GOODS_SOLD_ACCOUNT_ID, 
						L_LSR_ACCOUNT.REVENUE_ACCOUNT_ID, 
						L_MSG
					);
           
           if L_RTN = -1 then
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             return -1;
           end if;
         
         end if;
		 
		if L_PRIOR_FASB = 3 then
			-- Deferred Profit Remeasurement.
			L_MSG:='Retrieving Deferred Profit Remeasure';
			select -1 * nvl(deferred_profit_remeasurement, 0)
			into L_AMOUNT
			from lsr_ilr_schedule_direct_fin
			where ILR_ID = A_ILR_ID
			and REVISION = A_REVISION
			and SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID
			and MONTH = L_MONTH;
				
			if L_AMOUNT <> 0 then
				L_RTN := F_JE_DR_CR(A_ILR_ID, 
									  L_AMOUNT,
									  L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
									  L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID,
									  4066,
									  4067,
									  L_COMPANY_ID, 
									  L_GL_POSTING_MO_YR,
									  L_GL_JE_CODE, 
									  L_SOBS.SET_OF_BOOKS_ID, 
									  L_SOBS.JE_METHOD_ID, 
									  L_SOBS.AMOUNT_TYPE, 
									  L_SOBS.REVERSAL_CONVENTION, 
									  L_CONT_COMP_RATE,
									  L_CURRENCY_FROM, 
									  L_CURRENCY_TO, 
									  L_MSG);
					
				if L_RTN = -1 then
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
					return -1;
				end if;
			end if;
		end if;
		
		 
         -- 4055/4079 Deferred Selling Profit DR/LT Deferred Selling Profit CR
         -- LSR_ILR_SCHEDULE:	BEGIN_LT_DEFERRED_PROFIT
         -- DR Acct: ST Deferred Selling Profit (DEF_SELLING_PROFIT_ACCOUNT_ID)
         -- CR Acct: LT Deferred Selling Profit (LT_DEF_SELL_PROFIT_ACCOUNT_ID)
         begin
           select nvl(lt_deferred_profit_remeasure, 0)
             into L_BEG_LT_DEF_PROFIT
             from LSR_ILR_SCHEDULE_DIRECT_FIN
            where ILR_ID = A_ILR_ID
              and REVISION = A_REVISION
              and SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID
              and MONTH = L_MONTH;
         exception
           when no_data_found then
             L_BEG_LT_DEF_PROFIT := 0;
         end;
          
         L_MSG := 'Processing 4055/4079 Deferred Selling Profit DR/LT Deferred Selling Profit CR';
         IF L_BEG_LT_DEF_PROFIT <> 0 THEN
           L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                  case when L_PRIOR_FASB = 3 then 4080 else 4055 end, 
                                                  L_BEG_LT_DEF_PROFIT * case when L_PRIOR_FASB = 3 then -1 else 1 end, 
												  case when L_PRIOR_FASB = 3 
													then L_LSR_ACCOUNT.LT_DEF_SELL_PROFIT_ACCOUNT_ID 
													else L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID end, 
                                                  0, 
                                                  L_COMPANY_ID, 
                                                  L_GL_POSTING_MO_YR,
                                                  1, 
                                                  L_GL_JE_CODE, 
                                                  L_SOBS.SET_OF_BOOKS_ID, 
                                                  L_SOBS.JE_METHOD_ID, 
                                                  L_SOBS.AMOUNT_TYPE, 
                                                  L_SOBS.REVERSAL_CONVENTION, 
                                                  L_CONT_COMP_RATE,
                                                  L_CURRENCY_FROM, 
                                                  L_CURRENCY_TO, 
                                                  L_MSG);
           IF L_RTN = -1 THEN
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             RETURN -1;
           END IF;

           L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
												  case when L_PRIOR_FASB = 3 then 4081 else 4079 end, 
                                                  L_BEG_LT_DEF_PROFIT * case when L_PRIOR_FASB = 3 then -1 else 1 end, 
												  case when L_PRIOR_FASB = 3 
													then L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID 
													else L_LSR_ACCOUNT.LT_DEF_SELL_PROFIT_ACCOUNT_ID end,
                                                  0, 
                                                  L_COMPANY_ID, 
                                                  L_GL_POSTING_MO_YR,
                                                  0, 
                                                  L_GL_JE_CODE, 
                                                  L_SOBS.SET_OF_BOOKS_ID, 
                                                  L_SOBS.JE_METHOD_ID, 
                                                  L_SOBS.AMOUNT_TYPE, 
                                                  L_SOBS.REVERSAL_CONVENTION, 
                                                  L_CONT_COMP_RATE,
                                                  L_CURRENCY_FROM, 
                                                  L_CURRENCY_TO, 
                                                  L_MSG);
           IF L_RTN = -1 THEN
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             RETURN -1;
           END IF;
         END IF;
      END IF;

     end loop;
     
     return 1;
   exception
     when others then
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG ||': '|| sqlerrm);
       RAISE_APPLICATION_ERROR(-20000, 'Error Approving ILR: ' || L_MSG ||': '|| sqlerrm);
       ROLLBACK;
       return -1;

   end F_CREATE_JE_RECLASS_ILR;
   
   --**************************************************************************
   --                            F_CREATE_JE_REMEASUREMENT_ILR
   -- This function creates JEs for remeasured revision
   --**************************************************************************   
   
   function F_CREATE_JE_REMEASUREMENT_ILR(A_ILR_ID in number,
                                          A_REVISION in number,
										  A_PRIOR_REVISION in number) return number is
   L_MSG varchar2(2000);
   L_RTN number;                                          
   L_REMEASUREMENT_MONTH date;
   L_GL_POSTING_MO_YR date;
   L_CONT_COMP_RATE  number;
   L_CURRENCY_FROM  number;
   L_CURRENCY_TO    number;
   L_GL_JE_CODE     VARCHAR2(35);
   L_COMPANY_ID     NUMBER;
   L_LSR_ACCOUNT       LSR_ILR_ACCOUNT%rowtype;
   L_LSR_ILR_SCHEDULE  LSR_ILR_SCHEDULE%rowtype;
   L_UNGUARAN_RESIDUAL_REM_AMOUNT         NUMBER(22,2);
   L_LT_DEF_PROFIT_REM_AMOUNT NUMBER(22,2);
   L_SELLING_PROFIT_LOSS NUMBER;
   begin
     L_RTN := 0;
     L_MSG := 'Getting the first month of the schedule';
     select trunc(REMEASUREMENT_DATE, 'MONTH')
     into L_REMEASUREMENT_MONTH
     from LSR_ILR_OPTIONS LIO
     where LIO.ILR_ID = A_ILR_ID
          and LIO.REVISION = A_REVISION;
          
     L_MSG:='Retrieving L_COMPANY_ID';
     select company_id into L_COMPANY_ID
     from lsr_ilr
     where ilr_id = A_ILR_ID ;

     if L_REMEASUREMENT_MONTH is null then
       L_MSG := 'Error getting remeasurement month for the ILR revision';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
       return -1;
     end if;
     
     select min(gl_posting_mo_yr)
     into L_GL_POSTING_MO_YR
     from lsr_process_control
     where open_next is null
     and company_id=L_COMPANY_ID;

     if L_GL_POSTING_MO_YR is null then
       L_MSG := 'Error getting minimum open month for Lessor company';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
       return -1;
     end if;

     -- Lessor ILR accounts
     select * into L_LSR_ACCOUNT from LSR_ILR_ACCOUNT where ILR_ID = A_ILR_ID;

     -- L_CURRENCY_FROM
     L_MSG:='Retrieving L_CURRENCY_FROM';
     SELECT contract_currency_id
     INTO L_CURRENCY_FROM
     FROM lsr_lease l, lsr_ilr i
     WHERE l.lease_id = i.lease_id
       AND i.ilr_id = A_ILR_ID;

            -- L_CURRENCY_TO
     L_MSG:='Retrieving L_CURRENCY_TO';
     SELECT currency_id
     INTO L_CURRENCY_TO
     FROM lsr_ilr ilr, currency_schema cs
     WHERE ilr.company_id = cs.company_id
       AND currency_type_id = 1
       AND ilr_id = A_ILR_ID;

     -- L_CONT_COMP_RATE.
     L_MSG:='Retrieving contract to company currency rate';
     SELECT cont_to_comp_rate
     INTO L_CONT_COMP_RATE
     FROM (
        SELECT DISTINCT First_Value(rate) OVER (PARTITION BY currency_from, currency_to ORDER BY exchange_date DESC) cont_to_comp_rate
        FROM currency_rate_default
        WHERE trunc(exchange_date) <= L_REMEASUREMENT_MONTH
          AND currency_from = L_CURRENCY_FROM
          AND currency_to = L_CURRENCY_TO
		  AND exchange_rate_type_id = 1 -- Actual exchange rate
          ) ;

      IF L_CONT_COMP_RATE IS NULL THEN
        L_MSG := 'Error Retrieving exchange rate for contract to company currencies';
        PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
        RETURN -1;
      END IF;

      L_MSG:='Retrieving L_GL_JE_CODE';
      select gl_je_code
      into L_GL_JE_CODE
      from standard_journal_entries, gl_je_control
      where standard_journal_entries.je_id = gl_je_control.je_id
      and upper(ltrim(rtrim(process_id))) = 'LSR REMEASUREMENT';

      IF L_GL_JE_CODE IS NULL THEN
        L_MSG := 'Error Retrieving L_GL_JE_CODE';
        PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
        RETURN -1;
      END IF;

      L_MSG:='Retrieving JE Method, Set of books for ILR approval';
      
      for L_SOBS in
        ( select distinct JM.JE_METHOD_ID        as JE_METHOD_ID,
			  JM.AMOUNT_TYPE         as AMOUNT_TYPE,
			  JMSOB.SET_OF_BOOKS_ID     as SET_OF_BOOKS_ID,
			  JMSOB.REVERSAL_CONVENTION as REVERSAL_CONVENTION,
			  LFTS.FASB_CAP_TYPE_ID as FASB_CAP_TYPE_ID,
			  LFTS.USE_ORIG_RATE as USE_ORIG_RATE,
			  LFCT.DESCRIPTION as CAP_TYPE_DESCRIPTION
          from JE_METHOD JM
          inner join JE_METHOD_SET_OF_BOOKS JMSOB ON JMSOB.JE_METHOD_ID = JM.JE_METHOD_ID
          inner join JE_METHOD_TRANS_TYPE JMTT ON JMTT.JE_METHOD_ID = JM.JE_METHOD_ID
          inner join LSR_FASB_TYPE_SOB LFTS ON LFTS.SET_OF_BOOKS_ID = JMSOB.SET_OF_BOOKS_ID
          INNER JOIN LSR_FASB_CAP_TYPE LFCT ON LFCT.FASB_CAP_TYPE_ID = LFTS.FASB_CAP_TYPE_ID
          INNER JOIN LSR_ILR_OPTIONS LIO ON LIO.LEASE_CAP_TYPE_ID = LFTS.CAP_TYPE_ID
          INNER JOIN LSR_ILR ILR ON ILR.ILR_ID = LIO.ILR_ID AND ILR.CURRENT_REVISION = LIO.REVISION
          INNER JOIN COMPANY_JE_METHOD_VIEW CJMV ON CJMV.COMPANY_ID = ILR.COMPANY_ID AND CJMV.JE_METHOD_ID = JM.JE_METHOD_ID
          where ILR.ILR_ID = A_ILR_ID
          and LIO.revision = A_REVISION
		  -- We only want to do the remeasurement JEs if not a relcass
		  and f_has_class_change(a_ilr_id, a_revision, a_prior_revision, JMSOB.SET_OF_BOOKS_ID) = 0
          and JMTT.TRANS_TYPE IN
              ( SELECT DISTINCT JE_METHOD_ID FROM JE_METHOD_TRANS_TYPE
                WHERE TRANS_TYPE BETWEEN 4070 and 4073 OR TRANS_TYPE IN (4082, 4083, 4002, 4003, 4004, 4005, 4006, 4009))
		  AND LFCT.DESCRIPTION IN ('Sales Type', 'Direct Finance') 
		)
      LOOP
		PKG_PP_LOG.P_WRITE_MESSAGE('Processing Remeasurement for Set of books: ' || to_char(L_SOBS.SET_OF_BOOKS_ID));
		
		L_MSG:='Retrieving SELLING PROFIT LOSS';
		SELECT NVL(SELLING_PROFIT_LOSS,0) 
		INTO L_SELLING_PROFIT_LOSS
		FROM LSR_ILR_AMOUNTS
		WHERE ILR_ID = A_ILR_ID
		AND REVISION  = A_REVISION
		AND SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID;
	  
        L_MSG:='Retrieving Schedule for the first month';
        select LIS.*
        into L_LSR_ILR_SCHEDULE
        from LSR_ILR_SCHEDULE LIS
        where LIS.ILR_ID = A_ILR_ID
          and LIS.REVISION = A_REVISION
          and LIS.SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID
          and LIS.MONTH = L_REMEASUREMENT_MONTH;
          
       IF L_LSR_ILR_SCHEDULE.RECEIVABLE_REMEASUREMENT <> 0 THEN
			IF L_SOBS.USE_ORIG_RATE = 0 THEN
				--4070
				L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 4070, L_LSR_ILR_SCHEDULE.RECEIVABLE_REMEASUREMENT,
						 L_LSR_ACCOUNT.ST_RECEIVABLE_ACCOUNT_ID, 0, L_COMPANY_ID, L_GL_POSTING_MO_YR,
						 1, L_GL_JE_CODE, L_SOBS.SET_OF_BOOKS_ID, L_SOBS.JE_METHOD_ID, L_SOBS.AMOUNT_TYPE, 
						 L_SOBS.REVERSAL_CONVENTION, L_CONT_COMP_RATE, L_CURRENCY_FROM, L_CURRENCY_TO, L_MSG);
			ELSE
				L_RTN := F_CREATE_JE_WITH_PROFIT_LOSS
					(
						A_ILR_ID,
						4002, 
						L_LSR_ILR_SCHEDULE.RECEIVABLE_REMEASUREMENT, 
						L_LSR_ACCOUNT.ST_RECEIVABLE_ACCOUNT_ID,
						L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID, 
						L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
						0 /*GAIN_LOSS*/, 
						L_COMPANY_ID, 
						L_GL_POSTING_MO_YR,
						1 /* DR_CR_INDICATOR*/,
						L_GL_JE_CODE, 
						L_SOBS.SET_OF_BOOKS_ID, 
						L_SOBS.JE_METHOD_ID, 
						L_SOBS.AMOUNT_TYPE, 
						L_SOBS.REVERSAL_CONVENTION, 
						L_CONT_COMP_RATE /*exchange raate*/,
						L_CURRENCY_FROM, 
						L_CURRENCY_TO, 
						L_SELLING_PROFIT_LOSS, 
						L_SOBS.FASB_CAP_TYPE_ID, 
						0, 
						'ST_REMEASURE',
						L_LSR_ACCOUNT.COST_OF_GOODS_SOLD_ACCOUNT_ID, 
						L_LSR_ACCOUNT.REVENUE_ACCOUNT_ID, 
						L_MSG
					);
			END IF;

			if L_RTN = -1 then
				PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				return -1;
			end if;
       END IF;
         
       IF L_LSR_ILR_SCHEDULE.LT_RECEIVABLE_REMEASUREMENT <> 0 THEN
		IF L_SOBS.USE_ORIG_RATE = 0 THEN
			L_RTN := F_JE_DR_CR(A_ILR_ID, 
							L_LSR_ILR_SCHEDULE.LT_RECEIVABLE_REMEASUREMENT,
							L_LSR_ACCOUNT.LT_RECEIVABLE_ACCOUNT_ID, 
							L_LSR_ACCOUNT.ST_RECEIVABLE_ACCOUNT_ID,
							4072,
							4071,
							L_COMPANY_ID, 
							L_GL_POSTING_MO_YR,
							L_GL_JE_CODE, 
							L_SOBS.SET_OF_BOOKS_ID, 
							L_SOBS.JE_METHOD_ID, 
							L_SOBS.AMOUNT_TYPE, 
							L_SOBS.REVERSAL_CONVENTION, 
							L_CONT_COMP_RATE,
							L_CURRENCY_FROM, 
							L_CURRENCY_TO, 
							L_MSG);
		ELSE
			L_RTN := F_JE_DR_CR(A_ILR_ID, 
							L_LSR_ILR_SCHEDULE.LT_RECEIVABLE_REMEASUREMENT,
							L_LSR_ACCOUNT.LT_RECEIVABLE_ACCOUNT_ID, 
							L_LSR_ACCOUNT.ST_RECEIVABLE_ACCOUNT_ID,
							4003,
							4009,
							L_COMPANY_ID, 
							L_GL_POSTING_MO_YR,
							L_GL_JE_CODE, 
							L_SOBS.SET_OF_BOOKS_ID, 
							L_SOBS.JE_METHOD_ID, 
							L_SOBS.AMOUNT_TYPE, 
							L_SOBS.REVERSAL_CONVENTION, 
							L_CONT_COMP_RATE,
							L_CURRENCY_FROM, 
							L_CURRENCY_TO, 
							L_MSG);
		END IF;

		if L_RTN = -1 then
			PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
			return -1;
		end if;  
       END IF;
       
       SELECT NVL(SUM(UNGUARAN_RESIDUAL_REMEASURE),0)
       INTO L_UNGUARAN_RESIDUAL_REM_AMOUNT
       FROM LSR_ILR_SCHEDULE_SALES_DIRECT
       WHERE ILR_ID = A_ILR_ID
         AND REVISION = A_REVISION
         AND SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID;
       
       IF L_UNGUARAN_RESIDUAL_REM_AMOUNT <> 0 THEN
			IF L_SOBS.USE_ORIG_RATE = 0 THEN
				--4073
				L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 4073, L_UNGUARAN_RESIDUAL_REM_AMOUNT,
						 L_LSR_ACCOUNT.UNGUARAN_RES_ACCOUNT_ID, 0, L_COMPANY_ID, L_GL_POSTING_MO_YR,
						 1, L_GL_JE_CODE, L_SOBS.SET_OF_BOOKS_ID, L_SOBS.JE_METHOD_ID, L_SOBS.AMOUNT_TYPE, 
						 L_SOBS.REVERSAL_CONVENTION, L_CONT_COMP_RATE, L_CURRENCY_FROM, L_CURRENCY_TO, L_MSG);
			ELSE
				L_RTN := F_CREATE_JE_WITH_PROFIT_LOSS
					(
						A_ILR_ID,
						4004, 
						L_UNGUARAN_RESIDUAL_REM_AMOUNT, 
						L_LSR_ACCOUNT.UNGUARAN_RES_ACCOUNT_ID,
						L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID, 
						L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
						0 /*GAIN_LOSS*/, 
						L_COMPANY_ID, 
						L_GL_POSTING_MO_YR,
						1 /* DR_CR_INDICATOR*/,
						L_GL_JE_CODE, 
						L_SOBS.SET_OF_BOOKS_ID, 
						L_SOBS.JE_METHOD_ID, 
						L_SOBS.AMOUNT_TYPE, 
						L_SOBS.REVERSAL_CONVENTION, 
						L_CONT_COMP_RATE /*exchange raate*/,
						L_CURRENCY_FROM, 
						L_CURRENCY_TO, 
						L_SELLING_PROFIT_LOSS, 
						L_SOBS.FASB_CAP_TYPE_ID, 
						0, 
						'UNG_RESID_REMEASURE',
						L_LSR_ACCOUNT.COST_OF_GOODS_SOLD_ACCOUNT_ID, 
						L_LSR_ACCOUNT.REVENUE_ACCOUNT_ID, 
						L_MSG
					);
			END IF;

			if L_RTN = -1 then
				PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				return -1;
			end if;
       END IF;   
       
       IF L_SOBS.FASB_CAP_TYPE_ID = 3 THEN
         -- Direct Finance, Book LT Deferred Profit Remeasurement Amount
         -- 4082/4083 ST Deferred Profit Remeasurement DR/LT Deferred Profit Remeasurement CR
         -- LSR_ILR_SCHEDULE_DIRECT_FIN:	LT_DEFERRED_PROFIT_REMEASURE
         -- DR Acct: ST Deferred Selling Profit (DEF_SELLING_PROFIT_ACCOUNT_ID)
         -- CR Acct: LT Deferred Selling Profit (LT_DEF_SELL_PROFIT_ACCOUNT_ID
         SELECT NVL(LT_DEFERRED_PROFIT_REMEASURE, 0)
           INTO L_LT_DEF_PROFIT_REM_AMOUNT
           FROM LSR_ILR_SCHEDULE_DIRECT_FIN
          WHERE ILR_ID = A_ILR_ID
            AND REVISION = A_REVISION
            AND SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID
            AND MONTH = L_REMEASUREMENT_MONTH;
            
         IF L_LT_DEF_PROFIT_REM_AMOUNT <> 0 THEN
             L_MSG := 'Processing 4082/4083 ST Deferred Profit Remeasurement DR/LT Deferred Profit Remeasurement CR';
             IF L_LT_DEF_PROFIT_REM_AMOUNT <> 0 THEN
               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                      4082, 
                                                      L_LT_DEF_PROFIT_REM_AMOUNT, 
                                                      L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 
                                                      0, 
                                                      L_COMPANY_ID, 
                                                      L_GL_POSTING_MO_YR,
                                                      0, 
                                                      L_GL_JE_CODE, 
                                                      L_SOBS.SET_OF_BOOKS_ID, 
                                                      L_SOBS.JE_METHOD_ID, 
                                                      L_SOBS.AMOUNT_TYPE, 
                                                      L_SOBS.REVERSAL_CONVENTION, 
                                                      L_CONT_COMP_RATE,
                                                      L_CURRENCY_FROM, 
                                                      L_CURRENCY_TO, 
                                                      L_MSG);
             IF L_RTN = -1 THEN
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               RETURN -1;
             END IF;

             L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 
                                                    4083, 
                                                    L_LT_DEF_PROFIT_REM_AMOUNT, 
                                                    L_LSR_ACCOUNT.LT_DEF_SELL_PROFIT_ACCOUNT_ID, 
                                                    0, 
                                                    L_COMPANY_ID, 
                                                    L_GL_POSTING_MO_YR,
                                                    1, 
                                                    L_GL_JE_CODE, 
                                                    L_SOBS.SET_OF_BOOKS_ID, 
                                                    L_SOBS.JE_METHOD_ID, 
                                                    L_SOBS.AMOUNT_TYPE, 
                                                    L_SOBS.REVERSAL_CONVENTION, 
                                                    L_CONT_COMP_RATE,
                                                    L_CURRENCY_FROM, 
                                                    L_CURRENCY_TO, 
                                                    L_MSG);
             IF L_RTN = -1 THEN
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               RETURN -1;
             END IF;
           END IF;
         END IF;
       END IF;
        
      END LOOP;
     
     RETURN 1;
     exception
      when others then
           ROLLBACK;
           PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
           RAISE_APPLICATION_ERROR(-20000, 'Error Approving ILR: ' || L_MSG);
           return -1;
           
   end F_CREATE_JE_REMEASUREMENT_ILR;                                 

   --**************************************************************************
   --                            F_APPROVE_ILR
   --**************************************************************************
   function F_APPROVE_ILR(A_ILR_ID   in number,
                          A_REVISION IN NUMBER,
                          A_SEND_JES IN BOOLEAN DEFAULT TRUE) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN      number;
      MY_STR   varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      -- start by clearing our pend transaction
      RTN := F_APPROVE_ILR_NO_COMMIT( a_ilr_id => A_ILR_ID,
                                      a_revision => A_REVISION,
                                      a_status => L_STATUS,
                                      a_send_jes => A_SEND_JES);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending For Approval: ' || L_STATUS);
         return -1;
      end if;

      commit;
      return 1;
   exception
      when others then
         rollback;
         P_LOGERRORMESSAGE(A_ILR_ID, L_STATUS);
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving ILR: ' || L_STATUS);
         return -1;
   end F_APPROVE_ILR;


   --**************************************************************************
   --                            F_REJECT_ILR
   --**************************************************************************

   function F_REJECT_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin

      update LSR_ILR_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_REJECT_ILR;

   --**************************************************************************
   --                            F_SEND_ILR
   -- Called when a user clicks send for approval (on an ILR)
   -- Need to mark the approval status to be sent.  And update the ILR to
   -- pending approval.  IN addition need to update the capitalized cost to
   -- be the beginning obligation on the asset schedule for the revision being
   -- sent for approval
   --**************************************************************************
   function F_SEND_ILR_NO_COMMIT(A_ILR_ID   in number,
                                 A_REVISION in number,
                                 A_STATUS   OUT VARCHAR2) return number is
      RTN     number;
      ILR     number;
      WFS     number;
      SQLS    varchar2(32000);
      IS_AUTO WORKFLOW_TYPE.EXTERNAL_WORKFLOW_TYPE%type;
      APPROVED_DATE date;
      START_DATE date;
	    GL_POSTING_MO_YR date;
      v_component_count number;
	    L_LOCK_CHECK number;
	    PAY_START date;
	    OPEN_MONTH date;
   begin
      PKG_PP_LOG.P_START_LOG(f_get_pp_process_id('Lessor - ILR Commencement'));
      A_STATUS := 'SEND for approval:';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   ILR_ID: ' || TO_CHAR(A_ILR_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   REVISION: ' || TO_CHAR(A_REVISION);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

     --Check that the lease is valid ie in Open/New Revision status
	 -- /* WMD Have to be able to create new revisions on "Closed" leases
	 -- since new revisions are created for retirements and transfers */
     select count(1)
     into RTN
     from LSR_LEASE LL, LSR_ILR LI
     where LI.ILR_ID = A_ILR_ID
     and LI.LEASE_ID = LL.LEASE_ID
     and (LL.LEASE_STATUS_ID in (3,7)
	 or (LL.LEASE_STATUS_ID = 5
           and A_REVISION <> 1));

     if RTN <> 1 then
      A_STATUS := 'MLA for this ILR is not in Open or New Revision status, so the ILR cannot be posted to the MLA.';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      return -1;
     end if;

      A_STATUS := 'Getting start date';
      select min(PAYMENT_TERM_DATE)
        into START_DATE
      from LSR_ILR_PAYMENT_TERM
        where ILR_ID = A_ILR_ID
          and REVISION = A_REVISION;

      A_STATUS := 'Getting approved date';
      select min(PAYMENT_TERM_DATE)
        into APPROVED_DATE
      from lsr_ilr_payment_term pt, lsr_ilr_approval appr
      where pt.ilr_id = appr.ilr_id
      and pt.revision = appr.revision
      and appr.ilr_id = A_ILR_ID
      and appr.revision = (
         select max(revision)
         from lsr_ilr_approval
         where ilr_id = appr.ilr_id
           and approval_status_id in (3,6)
           and revision <> A_REVISION
      );

	 A_STATUS := 'Getting open month';
     PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
     select min(gl_posting_mo_yr)
      into OPEN_MONTH
     from lsr_process_control
     where company_id = (select company_id from ls_ilr where ilr_id = A_ILR_ID)
     and lsr_closed is null;

      PKG_PP_LOG.P_WRITE_MESSAGE('   Start Date: ' || to_char(START_DATE, 'YYYY-MM-DD'));
      PKG_PP_LOG.P_WRITE_MESSAGE('   Approved Date: ' || to_char(APPROVED_DATE, 'YYYY-MM-DD'));
	    PKG_PP_LOG.P_WRITE_MESSAGE('   Open Month: ' || to_char(OPEN_MONTH, 'YYYY-MM-DD'));

	   --  Can change the start date as long as the prior start date is not yet been reached
      if APPROVED_DATE <> START_DATE and trunc(APPROVED_DATE, 'mm') < trunc(sysdate, 'mm') and APPROVED_DATE is not null then
        A_STATUS := 'Cannot change the in service date of the ILR after the in service date has been reached';
        PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

        return -1;
      end if;

	  A_STATUS := 'Checking for Pay Start Date';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

	  PAY_START := PKG_LESSOR_APPROVAL.F_GET_PAY_START_DATE(A_ILR_ID,A_REVISION);

    A_STATUS := 'Checking for Month End Close in Progress';
    PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
    select count(*)
      into L_LOCK_CHECK
    from lsr_process_control
    where company_id = (select company_id from ls_ilr where ilr_id = A_ILR_ID)
    and lsr_closed is not null and open_next is null;

    if L_LOCK_CHECK > 0 then
	    -- Stop if the pay start date  is before the current open month
        if trunc(PAY_START, 'mm') < trunc(OPEN_MONTH, 'mm') then
          A_STATUS := 'The Payment Start Date (with Payment Shift) of ' || to_char(PAY_START, 'MM/YYYY') || ' is before the Current Open Month of ' || to_char(OPEN_MONTH, 'MM/YYYY') || ' and a prior month end close is in progress. Complete Month End Close, then Route for Approval';
          PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
          return -1;
        end if;
    end if;

      A_STATUS := 'updating LSR_ILR_APPROVAL';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LSR_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      --Update status of non-current revisions
      A_STATUS := 'update workflow';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update WORKFLOW
         set APPROVAL_STATUS_ID = 5
       where ID_FIELD1 = A_ILR_ID
         and ID_FIELD2 <> A_REVISION
         and APPROVAL_STATUS_ID = 2
         and Lower(SUBSYSTEM) = 'lessor_ilr_approval';

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'update lsr_ilr_approval- set other Initiated or Pending Approval revisions to Inactive';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LSR_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 5
       where ILR_ID = A_ILR_ID
         and REVISION <> A_REVISION
         and APPROVAL_STATUS_ID IN (1, 2);

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      PKG_PP_LOG.P_WRITE_MESSAGE('DONE');

      return 1;
   exception
      when others then
         PKG_PP_LOG.P_WRITE_MESSAGE('Error Sending For Approval: ' || sqlerrm);

         return -1;
   end F_SEND_ILR_NO_COMMIT;


   function F_SEND_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL
      RTN      number;
      L_STATUS varchar2(2000);
   begin
      -- start by clearing our pend transaction
      RTN := F_SEND_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending For Approval: ' || L_STATUS);
         return -1;
      end if;

      commit;

      return 1;

   exception
      when others then
         rollback;
         return -1;
   end F_SEND_ILR;

   --**************************************************************************
   --                            F_UNSEND_ILR
   --**************************************************************************

   function F_UNSEND_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin

      update LSR_ILR_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_UNSEND_ILR;

   --**************************************************************************
   --                            F_UNREJECT_ILR
   --**************************************************************************

   function F_UNREJECT_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_UNREJECT_ILR;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_ILR
   --**************************************************************************

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_ILR_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_ILR_ID)
                                        and ID_FIELD2 = TO_CHAR(A_REVISION)
                                        and Lower(SUBSYSTEM) = 'lessor_ilr_approval'),
                                     0)
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;

   exception
      when others then
         rollback;
         return -1;
   end F_UPDATE_WORKFLOW_ILR;

   --**************************************************************************
   --                            F_APPROVE_MLA
   --**************************************************************************

   function F_APPROVE_MLA(A_LEASE_ID in number,
                          A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this revision
      update LSR_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LSR_LEASE L
         set LEASE_STATUS_ID = 3, APPROVAL_DATE = sysdate, CURRENT_REVISION = A_REVISION
       where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving MLA');
         return -1;
   end F_APPROVE_MLA;

   --**************************************************************************
   --                            F_REJECT_MLA
   --**************************************************************************

   function F_REJECT_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_LEASE_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LSR_LEASE L set LEASE_STATUS_ID = 4 where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Rejecting MLA');
         return -1;
   end F_REJECT_MLA;

   --**************************************************************************
   --                            F_SEND_MLA
   --**************************************************************************

   function F_SEND_MLA(A_LEASE_ID in number,
                       A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN     number;
      IS_AUTO WORKFLOW_TYPE.EXTERNAL_WORKFLOW_TYPE%type;
   begin

      update LSR_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LSR_LEASE L set LEASE_STATUS_ID = 2 where L.LEASE_ID = A_LEASE_ID;

      --Reject other revisions
      update LSR_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 5, APPROVAL_DATE = sysdate, APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) <> NVL(A_REVISION, 0)
         and APPROVAL_STATUS_ID in (1, 2);

      --Call F_APPROVE_MLA for auto-approved assets
      commit;

      select NVL(EXTERNAL_WORKFLOW_TYPE, 'NA')
        into IS_AUTO
        from WORKFLOW_TYPE A, LSR_LEASE L
       where A.WORKFLOW_TYPE_ID = L.WORKFLOW_TYPE_ID
         and L.LEASE_ID = A_LEASE_ID;

      if IS_AUTO = 'AUTO' then
         return F_APPROVE_MLA(A_LEASE_ID, A_REVISION);
      end if;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending MLA');
         return -1;
   end F_SEND_MLA;

   --**************************************************************************
   --                            F_UNREJECT_MLA
   --**************************************************************************

   function F_UNREJECT_MLA(A_LEASE_ID in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_LEASE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 7
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LSR_LEASE L set LEASE_STATUS_ID = 2 where L.LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Unrejecting MLA');
         return -1;
   end F_UNREJECT_MLA;

   --**************************************************************************
   --                            F_UNSEND_MLA
   --**************************************************************************

   function F_UNSEND_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_LEASE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LSR_LEASE L set LEASE_STATUS_ID = 1, APPROVAL_DATE = null where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error UnSending MLA');
         return -1;
   end F_UNSEND_MLA;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_MLA
   --**************************************************************************

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID in number,
                                  A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LSR_LEASE_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_LEASE_ID)
                                        and ID_FIELD2 = TO_CHAR(A_REVISION)
                                        and SUBSYSTEM = 'lessor_mla_approval'),
                                     0)
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Updating Workflow MLA');
         return -1;
   end F_UPDATE_WORKFLOW_MLA;

   --**************************************************************************
   --                            F_INVOICE_APPROVE
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will approve Lessor Invoices
   -- @@PARAMS
   --    date: a_month
   --       The month to process accruals for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_INVOICE_APPROVE(A_COMPANY_ID in number,
                              A_MONTH      in date,
                              A_END_LOG    in number:=null) return varchar2 is
      L_STATUS varchar2(30000);
	  L_LOCATION varchar2(30000);
      L_RTN    number;
      L_COUNTER number;
      L_GL_JE_CODE   varchar2(35);
	  BUCKET LS_RENT_BUCKET_ADMIN%rowtype;
	  COUNTER number;
	  TAX_PAYMENT_TYPE number;
	  L_ACCTS PKG_LEASE_BUCKET.BUCKET_ACCTS;
   begin
      return 'OK';
   end F_INVOICE_APPROVE;

   --**************************************************************************
   --                            F_SEND_INVOICE
   --**************************************************************************

   function F_SEND_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   BEGIN

      update LSR_INVOICE_APPROVAL set APPROVAL_STATUS_ID = 2 where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending Invoice');
         return -1;
   end F_SEND_INVOICE;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_INVOICE
   --**************************************************************************

   function F_UPDATE_WORKFLOW_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LSR_INVOICE_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_INVOICE_ID)
                                        and SUBSYSTEM = 'lsr_invoice_approval'),
                                     0)
       where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;

   exception
      when others then
         rollback;
         return -1;
   end F_UPDATE_WORKFLOW_INVOICE;

   --**************************************************************************
   --                            F_REJECT_INVOICE
   --**************************************************************************

   function F_REJECT_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_INVOICE_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Rejecting Invoice');
         return -1;
   end F_REJECT_INVOICE;

   --**************************************************************************
   --                            F_UNREJECT_INVOICE
   --**************************************************************************

   function F_UNREJECT_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_INVOICE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 7
       where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Unrejecting Invoice');
         return -1;
   end F_UNREJECT_INVOICE;

   --**************************************************************************
   --                            F_APPROVE_INVOICE
   --**************************************************************************

   function F_APPROVE_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this LSR_INVOICE_APPROVAL
      update LSR_INVOICE_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving Invoice');
         return -1;
   end F_APPROVE_INVOICE;

   --**************************************************************************
   --                            F_UNSEND_INVOICE
   --**************************************************************************

   function F_UNSEND_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_INVOICE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error UnSending Invoice');
         return -1;
   end F_UNSEND_INVOICE;

   PROCEDURE p_mass_approve_ilrs( a_ilrs t_lsr_ilr_id_revision_tab,
                                  a_send_jes_0_1 number) IS
    l_ret NUMBER;
    l_send_jes boolean;
   BEGIN
    DELETE FROM lsr_ilr_approval_kickouts
    WHERE t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs;

    IF a_send_jes_0_1 = 0 THEN
      l_send_jes := false;
    ELSE
      l_send_jes := true;
    END IF;

    pkg_lessor_schedule.p_process_ilrs(a_ilrs);

    --Approval runs in autonomous transaction. We want the schedule results committed
    --in order to match what is sent to approval
    commit;


    FOR ilr IN (SELECT ilr_id, revision
                FROM TABLE(a_ilrs)
                WHERE (ilr_id, revision) NOT IN ( SELECT ilr_id, revision
                                                  FROM lsr_ilr_schedule_kickouts))
    LOOP
      BEGIN
        l_ret := pkg_lessor_approval.f_send_ilr(ilr.ilr_id, ilr.revision);
        IF l_ret <> 1 THEN
          raise_application_error(-20000, 'Error sending ILR for approval');
        END IF;

        l_ret := pkg_lessor_approval.f_approve_ilr(ilr.ilr_id, ilr.revision, l_send_jes);
        IF l_ret <> 1 THEN
          raise_application_error(-20000, 'Error approving ILR');
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          DECLARE
            l_kickout t_kickout;
          BEGIN
            l_kickout.ilr_id := ilr.ilr_id;
            l_kickout.revision := ilr.revision;
            l_kickout.message := 'Error processing approval: ' || sqlerrm;
            l_kickout.occurrence_id := pkg_pp_log.g_occurrence_id;
            p_log_kickouts(t_kickout_tab(l_kickout));
          END;
          CONTINUE;
      END;

    END LOOP;
   END;
   --**************************************************************************g
   --                            F_GET_PAY_START_DATE
   --**************************************************************************

   function F_GET_PAY_START_DATE
   (A_ILR_ID in number, A_REVISION in number)
   return date is
       L_STATUS varchar2(5000);
       PAY_START date;
   begin
      L_STATUS:= 'Get pay start date for ILR ID ' || to_char(A_ILR_ID) || ', Revision ' ||  to_char(A_REVISION) || '.';

      select decode(lease.pre_payment_sw,1,
      Add_Months(payment_term_date,-opt.payment_shift),
      Add_Months(terms.payment_term_date,-opt.payment_shift + Decode(terms.payment_freq_id,1,12,2,6,3,3,4,1,1)-1))
      into PAY_START
      from lsr_ilr ilr, lsr_lease lease, lsr_ilr_payment_term terms, lsr_ilr_options opt
      where ilr.lease_id = lease.lease_id
      and ilr.ilr_id = terms.ilr_id
      and terms.revision = A_REVISION
      and ilr.ilr_id = opt.ilr_id
      and opt.revision = A_REVISION
      and terms.payment_Term_id = 1
      and ilr.ilr_id = A_ILR_ID;

      return PAY_START;
end F_GET_PAY_START_DATE;


END pkg_lessor_approval;
/
