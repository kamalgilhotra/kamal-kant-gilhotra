create or replace package PP_REPORT_PKG as
   G_PKG_VERSION varchar(35) := '2018.2.1.0';
   function PP_REPORT_START_MONTH_NUMBER return number;
   function PP_REPORT_END_MONTH_NUMBER return number;
   function PP_REPORT_START_MONTH return date;
   function PP_REPORT_END_MONTH return date;
end PP_REPORT_PKG;
/

create or replace package body PP_REPORT_PKG as
   --**************************************************************************
   --                      PP_REPORT_START_MONTH_NUMBER
   --**************************************************************************
   function PP_REPORT_START_MONTH_NUMBER return number is
      START_MONTH number;

   begin
      select TO_NUMBER(TO_CHAR(START_MONTH, 'yyyymm'))
        into START_MONTH
        from REPORT_TIME
       where SESSION_ID = USERENV('sessionid')
         and LOWER(USER_ID) = LOWER(user);

      return(START_MONTH);
   end;

   --**************************************************************************
   --                     PP_REPORT_END_MONTH_NUMBER
   --**************************************************************************
   function PP_REPORT_END_MONTH_NUMBER return number is
      END_MONTH number;

   begin
      select TO_NUMBER(TO_CHAR(END_MONTH, 'yyyymm'))
        into END_MONTH
        from REPORT_TIME
       where SESSION_ID = USERENV('sessionid')
         and LOWER(USER_ID) = LOWER(user);

      return(END_MONTH);
   end;

   --**************************************************************************
   --                      PP_REPORT_START_MONTH
   --**************************************************************************
   function PP_REPORT_START_MONTH return date is
      START_MONTH date;

   begin
      select START_MONTH
        into START_MONTH
        from REPORT_TIME
       where SESSION_ID = USERENV('sessionid')
         and LOWER(USER_ID) = LOWER(user);

      return(START_MONTH);
   end;

   --**************************************************************************
   --                     PP_REPORT_END_MONTH
   --**************************************************************************
   function PP_REPORT_END_MONTH return date is
      END_MONTH date;

   begin
      select END_MONTH
        into END_MONTH
        from REPORT_TIME
       where SESSION_ID = USERENV('sessionid')
         and LOWER(USER_ID) = LOWER(user);

      return(END_MONTH);
   end;

end PP_REPORT_PKG;
/
