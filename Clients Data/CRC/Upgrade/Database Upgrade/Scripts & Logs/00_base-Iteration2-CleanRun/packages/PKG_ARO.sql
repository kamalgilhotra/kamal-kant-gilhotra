/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_039366_aro_PKG_ARO.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 03/17/2014 Brandon Beck
|| 10.4.3.0 08/14/2014 Brandon Beck
||============================================================================
*/

CREATE OR REPLACE package PKG_ARO as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_ARO
   || Description:
   ||============================================================================
   || Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.2.0 01/20/2013 Ryan Oliveria  Original Version
   ||============================================================================
   */
    G_PKG_VERSION varchar(35) := '2018.2.1.0';
	
	function F_GET_ACC_MONTH(A_COMPANY_ID in number) return date;

	function F_VALIDATE_ADJUSTMENTS(A_RUN_ID in number) return varchar2;

	function F_IMPORT_ADJUSTMENTS(A_RUN_ID in number) return varchar2;

	function F_CALC_REGULATED(A_COMPANY_ID   number,
									A_MONTH date) return varchar2;

	function F_APPROVE_REGULATED(A_COMPANY_ID   number,
										A_MONTH date) return varchar2;

end PKG_ARO;
/

CREATE OR REPLACE package body PKG_ARO as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_ARO
   || Description:
   ||============================================================================
   || Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.2.0 01/20/2013 Ryan Oliveria  Original Version
   ||============================================================================
   */

   --**************************************************************************
   --                            VARIABLES
   --**************************************************************************
	G_COMPANY_ID number;
	G_MONTH date;
	G_PROCESS_ID number;


   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************


   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************

	function F_GET_ACC_MONTH(A_COMPANY_ID in number) return date is
		L_MONTH date;
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('F_GET_ACC_MONTH');

		select min(ACCOUNTING_MONTH)
		into L_MONTH
		from CPR_CONTROL ctrl
		where company_id = A_COMPANY_ID
		and aro_approved is null;

		PKG_PP_ERROR.REMOVE_MODULE_NAME;

		return L_MONTH;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('Getting Accounting Month', 'Error retrieving current accounting month');
	end;



	function F_VALIDATE_ADJUSTMENTS(A_RUN_ID in number) return varchar2 is
		L_MONTH date;
		INVALID_DATE exception;
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('F_VALIDATE_ADJUSTMENTS');

		--
		-- Verify date formats for LAYER_STREAM_DATE
		--
		for L_IMPORT in (select LAYER_STREAM_DATE, IMPORT_RUN_ID, LINE_ID from ARO_IMPORT_LIABILITY_ADJ
							where IMPORT_RUN_ID = A_RUN_ID
							and LAYER_STREAM_DATE is not null)
		loop
			begin
				L_MONTH := to_date(L_IMPORT.LAYER_STREAM_DATE, 'mm/dd/yyyy');
			exception
				when INVALID_DATE then
					-- set error message
					update ARO_IMPORT_LIABILITY_ADJ adj
						set ERROR_MESSAGE = ERROR_MESSAGE || ' Layer Stream Date format is not valid; reformat to mm/dd/yyyy.'
					where IMPORT_RUN_ID = L_IMPORT.IMPORT_RUN_ID
						and LINE_ID = L_IMPORT.LINE_ID;
			end;
		end loop;

		--
		-- Verify that we can get the current accounting month
		--
		update ARO_IMPORT_LIABILITY_ADJ adj
			set ERROR_MESSAGE = ERROR_MESSAGE || ' There is no valid accounting month for processing ARO''s on this ARO''s company.'
		where IMPORT_RUN_ID = A_RUN_ID
			and (select F_GET_ACC_MONTH(COMPANY_ID) from ARO where ARO_ID = adj.ARO_ID) is null;

		--
		-- Validate the Layer ID
		--
		update ARO_IMPORT_LIABILITY_ADJ adj
			set ERROR_MESSAGE = ERROR_MESSAGE || ' Invalid layer ID.'
		where ARO_ID is not null
			and adj.IMPORT_RUN_ID = A_RUN_ID
			and not exists (
				select 1
				from ARO_LAYER
				where ARO_ID = adj.ARO_ID
				and LAYER_ID = adj.LAYER_ID
			);

		--
		-- For each ARO, all rows must have Layer Stream Date or null
		--
		update ARO_IMPORT_LIABILITY_ADJ adj
			set ERROR_MESSAGE = ERROR_MESSAGE || ' Only some of the rows for this ARO have a Layer Stream Date; they must all have one or all be left blank.'
		where ARO_XLATE in (
			select ARO_XLATE
			from ARO_IMPORT_LIABILITY_ADJ
			where IMPORT_RUN_ID = A_RUN_ID
			group by ARO_XLATE
			having count(LAYER_STREAM_DATE) < count(*)
			and count(LAYER_STREAM_DATE) > 0
		);

		--
		-- Validate the layer status
		--
		update ARO_IMPORT_LIABILITY_ADJ adj
			set ERROR_MESSAGE = ERROR_MESSAGE || ' Specified layer is not active.'
		where ARO_ID is not null
			and adj.IMPORT_RUN_ID = A_RUN_ID
			and exists (
				select 1
				from ARO_LAYER
				where ARO_ID = adj.ARO_ID
				and LAYER_ID = adj.LAYER_ID
			)
			and (
				select ARO_STATUS_ID
				from ARO_LAYER
				where ARO_ID = adj.ARO_ID
				and LAYER_ID = adj.LAYER_ID
			) <> 3;

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
		return 'OK';
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('Validating Adjustments', sqlerrm);
	end;



	function F_IMPORT_ADJUSTMENTS(A_RUN_ID in number) return varchar2 is
		--Your variables here
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('F_IMPORT_ADJUSTMENTS');

		--
		-- Set the MONTH_YR on the import table
		--
		update ARO_IMPORT_LIABILITY_ADJ
		set MONTH_YR = (
			select F_GET_ACC_MONTH(ARO.COMPANY_ID)
			from ARO
			where ARO.ARO_ID = ARO_IMPORT_LIABILITY_ADJ.ARO_ID
		)
		where MONTH_YR is null
			and IMPORT_RUN_ID = A_RUN_ID;

		--
		-- Set default values on the import table
		--
		update ARO_IMPORT_LIABILITY_ADJ
		set LAYER_STREAM_DATE = nvl(LAYER_STREAM_DATE, '01/01/1901'),
			ADD_TO_EXISTING = nvl(ADD_TO_EXISTING, 0)
		where IMPORT_RUN_ID = A_RUN_ID;

		--
		-- Insert blank rows in ARO_LIABILITY_ADJ where needed
		--
		insert into ARO_LIABILITY_ADJ
			(ARO_ID, LAYER_ID, MONTH_YR, ACCRETION_ADJUST, LIABILITY_ADJUST,
			NOTES, LAYER_STREAM_DATE, SETTLEMENT_ADJUST, SETTLEMENT_ADJUST_ARC,
			GAIN_LOSS_ADJUST_ARC, DEPR_LEDGER_INCLUDE)
		select adj.ARO_ID, adj.LAYER_ID, adj.MONTH_YR, 0, 0,
			null, to_date(adj.LAYER_STREAM_DATE, 'mm/dd/yyyy'), 0, 0,
			0, null
		from ARO_IMPORT_LIABILITY_ADJ adj
		where IMPORT_RUN_ID = A_RUN_ID
		and ARO_ID is not null
		and not exists (
			select 1
			from ARO_LIABILITY_ADJ
			where ARO_ID = adj.ARO_ID
			and LAYER_ID = adj.LAYER_ID
			and MONTH_YR = adj.MONTH_YR
			and LAYER_STREAM_DATE = to_date(adj.LAYER_STREAM_DATE, 'mm/dd/yyyy')
		);

		--
		-- Update all ARO_LIABILITY_ADJ rows
		--
		update ARO_LIABILITY_ADJ adj
		set (ACCRETION_ADJUST, LIABILITY_ADJUST, SETTLEMENT_ADJUST, NOTES, DEPR_LEDGER_INCLUDE) = (
			select ACCRETION_ADJUST + (ADD_TO_EXISTING * adj.ACCRETION_ADJUST),
				LIABILITY_ADJUST + (ADD_TO_EXISTING * adj.LIABILITY_ADJUST),
				SETTLEMENT_ADJUST + (ADD_TO_EXISTING * adj.SETTLEMENT_ADJUST),
				NOTES,
				DEPR_LEDGER_INCLUDE
			from ARO_IMPORT_LIABILITY_ADJ
			where ARO_ID = adj.ARO_ID
			and LAYER_ID = adj.LAYER_ID
			and MONTH_YR = adj.MONTH_YR
			and to_date(LAYER_STREAM_DATE, 'mm/dd/yyyy') = adj.LAYER_STREAM_DATE
			and IMPORT_RUN_ID = A_RUN_ID
		)
		where exists (
			select 1
			from ARO_IMPORT_LIABILITY_ADJ
			where ARO_ID = adj.ARO_ID
			and LAYER_ID = adj.LAYER_ID
			and MONTH_YR = adj.MONTH_YR
			and to_date(LAYER_STREAM_DATE, 'mm/dd/yyyy') = adj.LAYER_STREAM_DATE
			and IMPORT_RUN_ID = A_RUN_ID
		);

		--
		-- Update ARO_LIABILITY_ACCR_DTL for the detailed entries
		--
		update ARO_LIABILITY_ACCR_DTL dtl
		set (ACCRETION_ADJUST, LIABILITY_ADJUST, SETTLEMENT_ADJUST) = (
			select ACCRETION_ADJUST + (ADD_TO_EXISTING * dtl.ACCRETION_ADJUST),
				LIABILITY_ADJUST + (ADD_TO_EXISTING * dtl.LIABILITY_ADJUST),
				SETTLEMENT_ADJUST + (ADD_TO_EXISTING * dtl.SETTLEMENT_ADJUST)
			from ARO_IMPORT_LIABILITY_ADJ
			where ARO_ID = dtl.ARO_ID
			and LAYER_ID = dtl.LAYER_ID
			and MONTH_YR = dtl.MONTH_YR
			and to_date(LAYER_STREAM_DATE, 'mm/dd/yyyy') = dtl.LAYER_STREAM_DATE
			and IMPORT_RUN_ID = A_RUN_ID
		)
		where exists (
			select 1
			from ARO_IMPORT_LIABILITY_ADJ
			where ARO_ID = dtl.ARO_ID
			and LAYER_ID = dtl.LAYER_ID
			and MONTH_YR = dtl.MONTH_YR
			and to_date(LAYER_STREAM_DATE, 'mm/dd/yyyy') = dtl.LAYER_STREAM_DATE
			and IMPORT_RUN_ID = A_RUN_ID
		);


		--
		-- Update ARO_LIABILITY
		--
		update	aro_liability liab
		set 		( liab.accretion_adjust, liab.liability_adjust ) =
					(	select		sum( nvl( adj.accretion_adjust, 0 ) ), sum( nvl( adj.liability_adjust, 0 ) )
						from 		aro_liability_adj adj
						where 		adj.aro_id = liab.aro_id
						and 		adj.layer_id = liab.layer_id
						and 		adj.month_yr = liab.month_yr
					)
		where exists (
			select 1
			from ARO_IMPORT_LIABILITY_ADJ imp
			where imp.ARO_ID = liab.ARO_ID
			and imp.LAYER_ID = liab.LAYER_ID
			and imp.MONTH_YR = liab.MONTH_YR
		);

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
		return 'OK';
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('Importing Adjustments', sqlerrm);
	end;


   function F_VALIDATE_REGULATED return number is
		RETURN_CODE number;
		RTN number;
		RTN_STR varchar2(2000);
		STATUS varchar2(2000);
		MSG varchar2(2000);
	begin
		RETURN_CODE := 1;

		--
		--	Validate that all regulated ARO's have trans sets
		--
		STATUS := 'Verifying all regulated ARO''s have trans sets';
		RTN := 0;
		select count(1) into RTN
		from aro
		where regulatory_obligation = 1
			and depr_trans_set_id is null
			and company_id = G_COMPANY_ID;

		if RTN > 0 then
			PKG_PP_LOG.P_WRITE_MESSAGE('There are ' || to_char(RTN) || ' Regulated ARO''s with no Depreciation Trans Set.');
			PKG_PP_LOG.P_WRITE_MESSAGE('    This issue must be fixed before ARO''s can be processed.');
			RETURN_CODE := -1;
		end if;

		--
		--	Validate there are depr_ledger rows for this company/month, so that we can foreign key to them
		--
		STATUS := 'Validating depr ledger rows exist for this company and month';
		RTN := 0;
		select count(1)
		into RTN
		from depr_ledger dl, depr_group dg
		where dg.depr_group_id = dl.depr_group_id
			and dg.company_id = G_COMPANY_ID
			and dl.gl_post_mo_yr = G_MONTH;

		if RTN = 0 then
			PKG_PP_LOG.P_WRITE_MESSAGE('There are no records in the depr ledger for this company and month.');
			--PKG_PP_LOG.P_WRITE_MESSAGE('    Depr Ledger records are required for processing regulated ARO''s.');
			RETURN_CODE := 2;
		end if;

		--
		--	Validate that we only use Manual and Current Asset Balance trans sets
		-- 	This validation may be removed or modified if we add support for more trans set methods
		--
		STATUS := 'Validating trans set allocation methods';
		RTN := 0;
		RTN_STR := '';
		select count(1), min(aro.description)
		into RTN, RTN_STR
		from aro, depr_trans_set ts
		where ts.depr_trans_set_id = aro.depr_trans_set_id
			and aro.company_id = G_COMPANY_ID
			and aro.regulatory_obligation = 1
			and ts.depr_trans_allo_method_id not in (1,3);

		if RTN > 0 then
			MSG := 'ARO "' || rtn_str || '" ';
			if RTN > 1 then
				MSG := MSG || 'and ' + to_char(RTN - 1) || ' others are ';
			else
				MSG := MSG || 'is ';
			end if;
			MSG := MSG || 'related to a Depr Trans Set with an invalid allocation method.';
			PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
			PKG_PP_LOG.P_WRITE_MESSAGE('    Only "Manual" and "Current Asset Balance" are allowed.');
			RETURN_CODE := -1;
		end if;

		--
		--	Validate that all work orders on Reg ARO's have a Closing Option of "Clearing" (9)
		--
		STATUS := 'Validating work order closing options';
		RTN := 0;
		RTN_STR := '';

		select count(1), min(aro.description)
		into RTN, RTN_STR
		from work_order_account woa, aro_work_order arowo, aro
		where arowo.aro_id = aro.aro_id
			and woa.work_order_id = arowo.work_order_id
			and aro.regulatory_obligation = 1
			and aro.company_id = G_COMPANY_ID
			and woa.closing_option_id <> 9;

		if rtn > 0 then
			MSG := 'ARO "' || rtn_str || '" ';
			if rtn > 1 then
				MSG := MSG || 'and ' || to_char(RTN - 1) || ' others are ';
			else
				MSG := MSG || 'is ';
			end if;
			MSG := MSG || 'related to a Work Order with an invalid closing option.';
			PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
			PKG_PP_LOG.P_WRITE_MESSAGE('    Regulated ARO work orders must have a closing option of "Clearing."');
			RETURN_CODE := -1;
		end if;

		return RETURN_CODE;

	exception
		when others then
			PKG_PP_LOG.P_WRITE_MESSAGE('Error during Regulated ARO validation: ' || STATUS);
			return -1;
	end;



	function F_CALC_REGULATED_FOR_SOB( A_SOB_ID number) return number is
		RTN number;
		CURR_FACTOR number;
		SUM_FACTOR number;
		LEDGER_AMOUNT number;
		ARO_AMOUNT_ACCOUNTED_FOR number;
		FIRST_ROW_FOR_ARO number;
		LAST_ARO_ID number;
		LAST_DG_ID number;
		LAST_ARO_AMOUNT number;
		STATUS varchar2(2000);
		FACTOR_INDEX number;
	begin
		LAST_ARO_ID := -1;
		LAST_ARO_AMOUNT := 0;
		ARO_AMOUNT_ACCOUNTED_FOR := 0;
		FACTOR_INDEX := 0;

		for FACTOR in (select aro.aro_id aro_id,
						ts.depr_trans_set_id depr_trans_set_id,
						tsdg.depr_group_id depr_group_id,
						dl.set_of_books_id set_of_books_id,
						decode( ts.depr_trans_allo_method_id,
								1, tsdg.factor,
								2, dl.begin_balance * (1 - dmr.net_salvage_pct + dmr.cost_of_removal_pct) - dl.begin_reserve - dl.cor_beg_reserve,
								3, begin_balance,
								4, cor_beg_reserve,
								5, dl.cor_expense + dl.cor_exp_adjust + dl.cor_exp_alloc_adjust
							) factor,
						sum(decode( ts.depr_trans_allo_method_id,
								1, tsdg.factor,
								2, dl.begin_balance * (1 - dmr.net_salvage_pct + dmr.cost_of_removal_pct) - dl.begin_reserve - dl.cor_beg_reserve,
								3, begin_balance,
								4, cor_beg_reserve,
								5, dl.cor_expense + dl.cor_exp_adjust + dl.cor_exp_alloc_adjust
							)) over (partition by aro.aro_id, dl.set_of_books_id) sum_factor,
						count(1) over (partition by aro.aro_id, dl.set_of_books_id) factor_cnt,
						liab.settled - (
							select nvl(sum(settlement_adjust),0)
							from aro_liability_adj
							where aro_id = aro.aro_id
							and depr_ledger_include = 0
						) aro_amount
					from aro,
						depr_trans_set ts,
						depr_trans_set_dg tsdg,
						depr_group dg,
						depr_ledger dl,
						depr_method_rates dmr,
						aro_liability liab,
						aro_sob_view asob
					where ts.depr_trans_set_id = aro.depr_trans_set_id
					and tsdg.depr_trans_set_id = ts.depr_trans_set_id
					and dg.depr_group_id = tsdg.depr_group_id
					and dl.depr_group_id = dg.depr_group_id
					and dl.gl_post_mo_yr = G_MONTH
					and dmr.depr_method_id = dg.depr_method_id
					and dmr.set_of_books_id = dl.set_of_books_id
					and dmr.effective_date = (
						select max(x.effective_date) from depr_method_rates x
						where x.depr_method_id = dmr.depr_method_id
						and x.set_of_books_id = dmr.set_of_books_id
						and x.effective_date <= dl.gl_post_mo_yr
					)
					and liab.aro_id = aro.aro_id
					and liab.month_yr = dl.gl_post_mo_yr
					and aro.regulatory_obligation = 1
					and aro.company_id = G_COMPANY_ID
					and asob.aro_id = aro.aro_id
					and asob.set_of_books_id = dl.set_of_books_id
					and dl.set_of_books_id = A_SOB_ID
					order by 1 asc, 5 asc, dg.depr_group_id asc)
		loop



--			if LAST_ARO_ID <> FACTOR.aro_id then
--				FIRST_ROW_FOR_ARO := 1;
--			else
--				FIRST_ROW_FOR_ARO := 0;
--			end if;

--			if FIRST_ROW_FOR_ARO = 1 then
--				if LAST_ARO_AMOUNT <> ARO_AMOUNT_ACCOUNTED_FOR then
--					STATUS := 'Updating amount to fix rounding error. ARO ID ' || to_char(LAST_ARO_ID);
--					STATUS := STATUS || ', Depr Group ' || to_char(LAST_DG_ID);
--					update PEND_ARO_REG_ACTIVITY
--					set AMOUNT = AMOUNT + (LAST_ARO_AMOUNT - ARO_AMOUNT_ACCOUNTED_FOR)
--					where ARO_ID = LAST_ARO_ID
--						and SET_OF_BOOKS_ID = A_SOB_ID
--						and DEPR_GROUP_ID = LAST_DG_ID
--						and GL_POST_MO_YR = G_MONTH;
--				end if;

--				ARO_AMOUNT_ACCOUNTED_FOR := 0;
--			end if;

			--
			--	Calculate amount to be inserted
			--	If factors are all zeroes, make it an even split
			--
			if FACTOR.sum_factor <> 0 then
				SUM_FACTOR := FACTOR.sum_factor;
				CURR_FACTOR := FACTOR.factor;
			else
				SUM_FACTOR := FACTOR.factor_cnt;
				CURR_FACTOR := 1;
			end if;
			LEDGER_AMOUNT := ROUND(FACTOR.aro_amount * CURR_FACTOR / SUM_FACTOR, 2);
			ARO_AMOUNT_ACCOUNTED_FOR := ARO_AMOUNT_ACCOUNTED_FOR + LEDGER_AMOUNT;

			--
			--	If this is the last factor for the ARO, and this ARO's amounts didn't add up (rounding errors),
			--	Then update this record (which has max amount) with the difference
			--
			FACTOR_INDEX := FACTOR_INDEX + 1;
			if FACTOR_INDEX = FACTOR.factor_cnt then
				if ARO_AMOUNT_ACCOUNTED_FOR <> FACTOR.aro_amount then
					LEDGER_AMOUNT := LEDGER_AMOUNT + (FACTOR.aro_amount - ARO_AMOUNT_ACCOUNTED_FOR);
				end if;

				FACTOR_INDEX := 0;
				ARO_AMOUNT_ACCOUNTED_FOR := 0;
			end if;

			--
			--	Insert new activity
			--
			STATUS := 'Inserting pend activity.  ARO ' || to_char(FACTOR.aro_id);
			STATUS := STATUS || ', Depr Group ' || to_char(FACTOR.depr_group_id);
			STATUS := STATUS || ', Amount ' || to_char(LEDGER_AMOUNT);
			insert into PEND_ARO_REG_ACTIVITY
				(ARO_ID, SET_OF_BOOKS_ID, DEPR_GROUP_ID, GL_POST_MO_YR, AMOUNT)
			values
				(FACTOR.aro_id, A_SOB_ID, FACTOR.depr_group_id, G_MONTH, LEDGER_AMOUNT);


--			LAST_ARO_AMOUNT := FACTOR.aro_amount;
--			LAST_ARO_ID := FACTOR.aro_id;
--			LAST_DG_ID := FACTOR.depr_group_id;
		end loop;

		return 1;

	exception
		when others then
			PKG_PP_LOG.P_WRITE_MESSAGE('Error while calculating regulated ARO for SOB ' || to_char(A_SOB_ID) || ': ' || STATUS);
			PKG_PP_LOG.P_WRITE_MESSAGE(SQLERRM);
			return -1;
	end;



	function F_FLAG_USED_TRANS_SETS return number is
	begin
		update depr_trans_set
		set used_in_aro_calc = 1
		where depr_trans_set_id in (
			select aro.depr_trans_set_id
			from aro, pend_aro_reg_activity pend
			where pend.aro_id = aro.aro_id
				and aro.company_id = G_COMPANY_ID
				and pend.gl_post_mo_yr = G_MONTH
		)
		and nvl(used_in_aro_calc,0) = 0;

		return 1;

	exception
		when others then
			PKG_PP_LOG.P_WRITE_MESSAGE('Error while flagging used trans sets');
			return -1;
	end;



	function F_CALC_REGULATED( A_COMPANY_ID number, A_MONTH date ) return varchar2 is
		RTN number;
		STATUS varchar2(2000);
	begin
		PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => G_PROCESS_ID);
		PKG_PP_LOG.P_WRITE_MESSAGE('Starting ARO Reg Calculation for Company ' || to_char(A_COMPANY_ID) || ' Month ' || to_char(A_MONTH, 'yyyymm'));

		G_COMPANY_ID := A_COMPANY_ID;
		G_MONTH := A_MONTH;

		--
		--	Validation
		--
		STATUS := 'Validating regulated ARO';
		RTN := F_VALIDATE_REGULATED;
		if RTN = 2 then
			--  If RTN = 2, there are no depr ledger rows for regulated AROs. Don't try to calc, but return OK so month-end processing can continue
			PKG_PP_LOG.P_END_LOG();
			return 'OK';
		end if;
		if RTN <> 1 then
			return 'ERROR';
		end if;

		--
		--	Clear any old pending activities
		--
		STATUS := 'Clearing old pending activities';
		delete from PEND_ARO_REG_ACTIVITY
		where gl_post_mo_yr = G_MONTH
		and aro_id in (
			select aro_id
			from aro
			where company_id = G_COMPANY_ID
		);

		--
		--	Run the calculation for each set of books
		--
		STATUS := 'Calculating by set of books';
		for SOB in (select set_of_books_id from set_of_books order by set_of_books_id asc)
		loop
			RTN := F_CALC_REGULATED_FOR_SOB(SOB.set_of_books_id);
			if RTN <> 1 then
				return 'ERROR';
			end if;
		end loop;

		--
		--	Flag used depr trans sets
		--
		STATUS := 'Flagging used depr trans sets';
		RTN := F_FLAG_USED_TRANS_SETS;
		if RTN <> 1 then
			return 'ERROR';
		end if;

		PKG_PP_LOG.P_END_LOG();
		return 'OK';

	exception
		when others then
			PKG_PP_LOG.P_WRITE_MESSAGE('Error while calculating regulated ARO: ' || STATUS);
			return 'ERROR';
	end;

	function F_CREATE_REGULATED_JES(A_COMPANY_ID number, A_MONTH date) return number is
		RTN number;
		STATUS varchar2(2000);
		GL_ACC_STR_DEBIT varchar2(2000);
		GL_ACC_STR_CREDIT varchar2(2000);
	begin
		for ACT in (select pend.set_of_books_id,
			  			pend.depr_group_id,
						dg.cor_reserve_acct_id,
						aro.cor_credit_acct_id,
						pend.amount,
						aro.asset_id,
						msob.reversal_convention,
						m.amount_type,
						m.je_method_id
					from pend_aro_reg_activity pend,
						aro,
						depr_group dg,
						je_method m,
						je_method_set_of_books msob,
						je_method_trans_type mtype
					where aro.aro_id = pend.aro_id
						and dg.depr_group_id = pend.depr_group_id
						and m.je_method_id = mtype.je_method_id
						and msob.set_of_books_id = pend.set_of_books_id
						and msob.je_method_id = m.je_method_id
						and mtype.trans_type = 41
						and aro.company_id = A_COMPANY_ID
						and pend.gl_post_mo_yr = A_MONTH)
		loop
			RTN := 1;

			STATUS := 'Getting debit account string';
			GL_ACC_STR_DEBIT := PP_GL_TRANSACTION(41, ACT.asset_id, 0, ACT.depr_group_id, 0, ACT.cor_reserve_acct_id,
													0, 0, ACT.je_method_id, ACT.set_of_books_id, ACT.reversal_convention,
													ACT.amount_type);
			if substr(GL_ACC_STR_DEBIT, 1, 5) = 'ERROR' then
				PKG_PP_LOG.P_WRITE_MESSAGE(GL_ACC_STR_DEBIT);
				return -1;
			end if;

			STATUS := 'Getting credit account string';
			GL_ACC_STR_CREDIT := PP_GL_TRANSACTION(42, ACT.asset_id, 0, ACT.depr_group_id, 0, ACT.cor_credit_acct_id,
													0, 0, ACT.je_method_id, ACT.set_of_books_id, ACT.reversal_convention,
													ACT.amount_type);
			if substr(GL_ACC_STR_CREDIT, 1, 5) = 'ERROR' then
				PKG_PP_LOG.P_WRITE_MESSAGE(GL_ACC_STR_CREDIT);
				return -1;
			end if;

			STATUS := 'Debit Insert';
			insert into gl_transaction
				(gl_trans_id, month, company_number, gl_account, debit_credit_indicator, amount,
				gl_je_code, gl_status_id, description, source, asset_id, amount_type, je_method_id,
				trans_type)
			select
				pwrplant1.nextval, A_MONTH, c.gl_company_no, GL_ACC_STR_DEBIT, 1, ACT.amount,
				'ARO', 1, 'Regulated ARO Debit', 'ARO APPROVAL', ACT.asset_id, ACT.amount_type, ACT.je_method_id, 41
			from company c
			where c.company_id = A_COMPANY_ID;

			STATUS := 'Credit Insert';
			insert into gl_transaction
				(gl_trans_id, month, company_number, gl_account, debit_credit_indicator, amount,
				gl_je_code, gl_status_id, description, source, asset_id, amount_type, je_method_id,
				trans_type)
			select
				pwrplant1.nextval, A_MONTH, c.gl_company_no, GL_ACC_STR_CREDIT, 0, ACT.amount,
				'ARO', 1, 'Regulated ARO Credit', 'ARO APPROVAL', ACT.asset_id, ACT.amount_type, ACT.je_method_id, 42
			from company c
			where c.company_id = A_COMPANY_ID;

		end loop;
		return 1;
	exception
		when others then
			PKG_PP_LOG.P_WRITE_MESSAGE('Error while creating regulated JE''s: ' || STATUS);
			return -1;
	end;

	function F_APPROVE_REGULATED( A_COMPANY_ID number, A_MONTH date ) return varchar2 is
		RTN number;
		STATUS varchar2(2000);
	begin
		PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => G_PROCESS_ID);
		PKG_PP_LOG.P_WRITE_MESSAGE('Starting ARO Reg Approval for Company ' || to_char(A_COMPANY_ID) || ' Month ' || to_char(A_MONTH, 'yyyymm'));

		G_COMPANY_ID := A_COMPANY_ID;
		G_MONTH := A_MONTH;

		--
		--	Update the depr_ledger with amounts from pend_aro_reg_activity
		--
		STATUS := 'Updating COR on depr_ledger';
		update depr_ledger dl
		set cost_of_removal = cost_of_removal - (
			select nvl(sum(amount),0)
			from pend_aro_reg_activity pend
			where pend.set_of_books_id = dl.set_of_books_id
				and pend.depr_group_id = dl.depr_group_id
				and pend.gl_post_mo_yr = dl.gl_post_mo_yr
		)
		where gl_post_mo_yr = A_MONTH
		and depr_group_id in (select depr_group_id from depr_group where company_id = A_COMPANY_ID)
		and exists (
			select 1
			from pend_aro_reg_activity pend2
			where pend2.set_of_books_id = dl.set_of_books_id
				and pend2.depr_group_id = dl.depr_group_id
				and pend2.gl_post_mo_yr = dl.gl_post_mo_yr
		);

		--
		--	Create Journal Entries
		--
		STATUS := 'Creating journal entries';
		RTN := F_CREATE_REGULATED_JES(A_COMPANY_ID, A_MONTH);
		if RTN <> 1 then
			return 'ERROR';
		end if;

		--
		--	Clear the archived activities
		--
		STATUS := 'Clearing archived activities';
		delete from pend_aro_reg_activity_arc
		where gl_post_mo_yr = A_MONTH
		and depr_group_id in (select depr_group_id from depr_group where company_id = A_COMPANY_ID);

		--
		--	Archive the current pending activities
		--
		STATUS := 'Archiving pending activities';
		insert into pend_aro_reg_activity_arc (ARO_ID, SET_OF_BOOKS_ID, DEPR_GROUP_ID, GL_POST_MO_YR, AMOUNT)
		select ARO_ID, SET_OF_BOOKS_ID, DEPR_GROUP_ID, GL_POST_MO_YR, AMOUNT
		from pend_aro_reg_activity
		where gl_post_mo_yr = A_MONTH
		and depr_group_id in (select depr_group_id from depr_group where company_id = A_COMPANY_ID);

		--
		--	Clear the current activities
		--
		STATUS := 'Clearing current pending activities';
		delete from pend_aro_reg_activity
		where gl_post_mo_yr = A_MONTH
		and depr_group_id in (select depr_group_id from depr_group where company_id = A_COMPANY_ID);

		PKG_PP_LOG.P_END_LOG();
		return 'OK';

	exception
		when others then
			PKG_PP_LOG.P_WRITE_MESSAGE('Error while approving regulated ARO: ' || STATUS);
			return 'ERROR';
	end;


--**************************************************************************
--                            Initialize Package
--**************************************************************************

begin
   select PROCESS_ID
   into G_PROCESS_ID
   from PP_PROCESSES
   where DESCRIPTION = 'CPR MONTHLY CLOSE';

end PKG_ARO;
/

