SET ECHO OFF
SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''
HOST MKDIR &&PP_SCRIPT_PATH.UPGRADE_UTILITIES\LOGS
SPOOL &&PP_SCRIPT_PATH.UPGRADE_UTILITIES\LOGS\create_synonyms.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: create_synonyms.sql
|| Description: Create Public Synonyms for TABLES, VIEWS, SEQUENCES,
||              PROCEDURES, PACKAGES, FUNCTIONS, and TYPES.
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| V10     10/23/2007 Lee Quinn      Created
|| V10.0.2 12/19/2007 Lee Quinn      Added logging
||         01/23/2008 Lee Quinn      Added OWNER='PUBLIC'
|| V10.1.0 08/19/2008 Lee Quinn      Removed PLANT_TABLE
||                                   Enhanced performance
|| V10.2   03/24/2009 Lee Quinn      Added support for invalid SYNONYMS
|| V2017.3 04/09/2018 Andrew Hill    Added TYPE objects 
||============================================================================
*/

DECLARE
   PPCMSG     varchar2(10) := 'PPC-MSG> ';
   PPCERR     varchar2(10) := 'PPC-ERR> ';
   PPCSQL     varchar2(10) := 'PPC-SQL> ';

   v_rcount   NUMBER;
   v_rskipped NUMBER;
   v_rerrors  NUMBER;

BEGIN
   DBMS_OUTPUT.ENABLE(buffer_size => NULL);
   /* Log the start of the run. */
   INSERT INTO PWRPLANT.PP_UPDATE_FLEX
               (COL_ID,
                TYPE_NAME,
                FLEXVCHAR1,
                FLEXVCHAR2)
         SELECT NVL(MAX(COL_ID), 0) + 1,
                'UPDATE_SCRIPTS',
                'create_synonyms.sql',
                'Status = Running'
           FROM PWRPLANT.PP_UPDATE_FLEX;
   COMMIT;

   v_rerrors := 0;
/*
|| TABLE SYNONYMS
*/
   DBMS_OUTPUT.PUT_LINE('/* TABLE Synonyms */');
   v_rcount := 0;
   v_rskipped := 0;
   FOR csr_create_sql IN
   (SELECT a.OBJECT_NAME,
           'CREATE PUBLIC SYNONYM '||upper(a.OBJECT_NAME)||' FOR PWRPLANT."'||a.OBJECT_NAME||'"' CREATE_SQL,
           b.TABLE_OWNER,
           b.TABLE_NAME,
           b.DB_LINK
      FROM ALL_OBJECTS a,
           (SELECT TABLE_OWNER,
                   TABLE_NAME,
                   SYNONYM_NAME,
                   DB_LINK
              FROM ALL_SYNONYMS
             WHERE OWNER = 'PUBLIC') b
     WHERE a.OWNER = 'PWRPLANT'
       AND a.OBJECT_TYPE = 'TABLE'
       AND a.OBJECT_NAME NOT LIKE 'BIN$%'
       AND a.OBJECT_NAME <> 'PLAN_TABLE'
       AND b.SYNONYM_NAME(+) = a.OBJECT_NAME
     ORDER BY a.OBJECT_NAME
   )
   LOOP
      BEGIN
         IF csr_create_sql.db_link IS NOT NULL THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is being used on a database link.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name||'@'||csr_create_sql.db_link);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSIF csr_create_sql.table_name <> csr_create_sql.object_name THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is not on the correct PWRPLANT object.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSIF csr_create_sql.table_owner <> 'PWRPLANT' THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is not for a PWRPLANT object.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSE
            IF csr_create_sql.table_name IS NOT NULL THEN
               v_rskipped := v_rskipped + 1;
            ELSE
               v_rcount := v_rcount + 1;
               EXECUTE IMMEDIATE csr_create_sql.create_sql;
               DBMS_OUTPUT.PUT_LINE(PPCSQL || csr_create_sql.create_sql || ';');
            END IF;
         END IF;
      EXCEPTION
      WHEN OTHERS THEN
         v_rerrors := v_rerrors + 1;
         DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || csr_create_sql.create_sql);
         DBMS_OUTPUT.PUT_LINE('SQLCODE = '||SQLCODE||' SQLERRM = '||SQLERRM);
      END;
   END LOOP;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rcount || ' TABLE Synonym(s) created.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rskipped || ' TABLE Synonym(s) already existed.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || (v_rcount + v_rskipped) || ' Total TABLE Synonym(s) processed.');
   DBMS_OUTPUT.NEW_LINE;
/*
|| VIEW SYNONYMS
*/
   DBMS_OUTPUT.PUT_LINE('/* VIEW Synonyms */');
   v_rcount := 0;
   v_rskipped := 0;
   FOR csr_create_sql IN
   ( SELECT a.OBJECT_NAME,
            'CREATE PUBLIC SYNONYM '||upper(a.OBJECT_NAME)||' FOR PWRPLANT."'||a.OBJECT_NAME||'"' CREATE_SQL,
            b.TABLE_OWNER,
            b.TABLE_NAME,
            b.DB_LINK
       FROM ALL_OBJECTS a,
            (SELECT TABLE_OWNER,
                               TABLE_NAME,
                               SYNONYM_NAME,
                               DB_LINK
                          FROM ALL_SYNONYMS
             WHERE OWNER = 'PUBLIC') b
      WHERE a.OWNER = 'PWRPLANT'
        AND a.OBJECT_TYPE = 'VIEW'
        AND a.OBJECT_NAME NOT LIKE 'BIN$%'
        AND b.SYNONYM_NAME(+) = a.OBJECT_NAME
      ORDER BY a.OBJECT_NAME
   )
   LOOP
      BEGIN
         IF csr_create_sql.db_link IS NOT NULL THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is being used on a database link.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name||'@'||csr_create_sql.db_link);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSIF csr_create_sql.table_name <> csr_create_sql.object_name THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is not on the correct PWRPLANT object.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSIF csr_create_sql.table_owner <> 'PWRPLANT' THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is not for a PWRPLANT object.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSE
            IF csr_create_sql.table_name IS NOT NULL THEN
               v_rskipped := v_rskipped + 1;
            ELSE
               v_rcount := v_rcount + 1;
               EXECUTE IMMEDIATE csr_create_sql.create_sql;
               DBMS_OUTPUT.PUT_LINE(PPCSQL || csr_create_sql.create_sql || ';');
            END IF;
         END IF;
      EXCEPTION
      WHEN OTHERS THEN
         v_rerrors := v_rerrors + 1;
         DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || csr_create_sql.create_sql);
         DBMS_OUTPUT.PUT_LINE('SQLCODE = '||SQLCODE||' SQLERRM = '||SQLERRM);
      END;
   END LOOP;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rcount || ' VIEW Synonym(s) created.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rskipped || ' VIEW Synonym(s) already existed.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || (v_rcount + v_rskipped) || ' Total VIEW Synonym(s) processed.');
   DBMS_OUTPUT.NEW_LINE;
/*
|| SEQUENCE SYNONYMS
*/
   DBMS_OUTPUT.PUT_LINE('/* SEQUENCE Synonyms */');
   v_rcount := 0;
   v_rskipped := 0;
   FOR csr_create_sql IN
   ( SELECT a.OBJECT_NAME,
            'CREATE PUBLIC SYNONYM '||upper(a.OBJECT_NAME)||' FOR PWRPLANT."'||a.OBJECT_NAME||'"' CREATE_SQL,
            b.TABLE_OWNER,
            b.TABLE_NAME,
            b.DB_LINK
       FROM ALL_OBJECTS a,
           (SELECT TABLE_OWNER,
                   TABLE_NAME,
                   SYNONYM_NAME,
                   DB_LINK
              FROM ALL_SYNONYMS
             WHERE OWNER = 'PUBLIC') b
      WHERE a.OWNER = 'PWRPLANT'
        AND a.OBJECT_TYPE = 'SEQUENCE'
        AND a.OBJECT_NAME NOT LIKE 'BIN$%'
        AND b.SYNONYM_NAME(+) = a.OBJECT_NAME
      ORDER BY a.OBJECT_NAME
   )
   LOOP
      BEGIN
         IF csr_create_sql.db_link IS NOT NULL THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is being used on a database link.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name||'@'||csr_create_sql.db_link);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSIF csr_create_sql.table_name <> csr_create_sql.object_name THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is not on the correct PWRPLANT object.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSIF csr_create_sql.table_owner <> 'PWRPLANT' THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is not for a PWRPLANT object.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSE
            IF csr_create_sql.table_name IS NOT NULL THEN
               v_rskipped := v_rskipped + 1;
            ELSE
               v_rcount := v_rcount + 1;
               EXECUTE IMMEDIATE csr_create_sql.create_sql;
               DBMS_OUTPUT.PUT_LINE(PPCSQL || csr_create_sql.create_sql || ';');
            END IF;
         END IF;
      EXCEPTION
      WHEN OTHERS THEN
         v_rerrors := v_rerrors + 1;
         DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || csr_create_sql.create_sql);
         DBMS_OUTPUT.PUT_LINE('SQLCODE = '||SQLCODE||' SQLERRM = '||SQLERRM);
      END;
   END LOOP;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rcount || ' SEQUENCE Synonym(s) created.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rskipped || ' SEQUENCE Synonym(s) already existed.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || (v_rcount + v_rskipped) || ' Total SEQUENCE Synonym(s) processed.');
   DBMS_OUTPUT.NEW_LINE;
/*
|| PROCEDURE SYNONYMS
*/
   DBMS_OUTPUT.PUT_LINE('/* PROCEUDRE Synonyms */');
   v_rcount := 0;
   v_rskipped := 0;
   FOR csr_create_sql IN
   ( SELECT a.OBJECT_NAME,
            'CREATE PUBLIC SYNONYM '||upper(a.OBJECT_NAME)||' FOR PWRPLANT."'||a.OBJECT_NAME||'"' CREATE_SQL,
            b.TABLE_OWNER,
            b.TABLE_NAME,
            b.DB_LINK
       FROM ALL_OBJECTS a,
            (SELECT TABLE_OWNER,
                    TABLE_NAME,
                    SYNONYM_NAME,
                    DB_LINK
               FROM ALL_SYNONYMS
              WHERE OWNER = 'PUBLIC') b
      WHERE a.OWNER = 'PWRPLANT'
        AND a.OBJECT_TYPE = 'PROCEDURE'
        AND a.OBJECT_NAME NOT LIKE 'BIN$%'
        AND b.SYNONYM_NAME(+) = a.OBJECT_NAME
      ORDER BY a.OBJECT_NAME
   )
   LOOP
      BEGIN
         IF csr_create_sql.db_link IS NOT NULL THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is being used on a database link.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name||'@'||csr_create_sql.db_link);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSIF csr_create_sql.table_name <> csr_create_sql.object_name THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is not on the correct PWRPLANT object.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSIF csr_create_sql.table_owner <> 'PWRPLANT' THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is not for a PWRPLANT object.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSE
            IF csr_create_sql.table_name IS NOT NULL THEN
               v_rskipped := v_rskipped + 1;
            ELSE
               v_rcount := v_rcount + 1;
               EXECUTE IMMEDIATE csr_create_sql.create_sql;
               DBMS_OUTPUT.PUT_LINE(PPCSQL || csr_create_sql.create_sql || ';');
            END IF;
         END IF;
      EXCEPTION
      WHEN OTHERS THEN
         v_rerrors := v_rerrors + 1;
         DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || csr_create_sql.create_sql);
         DBMS_OUTPUT.PUT_LINE('SQLCODE = '||SQLCODE||' SQLERRM = '||SQLERRM);
      END;
   END LOOP;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rcount || ' PROCEDURE Synonym(s) created.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rskipped || ' PROCEDURE Synonym(s) already existed.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || (v_rcount + v_rskipped) || ' Total PROCEDURE Synonym(s) processed.');
   DBMS_OUTPUT.NEW_LINE;
/*
|| PACKAGE SYNONYMS
*/
   DBMS_OUTPUT.PUT_LINE('/* PACKAGE Synonyms */');
   v_rcount := 0;
   v_rskipped := 0;
   FOR csr_create_sql IN
   ( SELECT a.OBJECT_NAME,
            'CREATE PUBLIC SYNONYM '||upper(a.OBJECT_NAME)||' FOR PWRPLANT."'||a.OBJECT_NAME||'"' CREATE_SQL,
            b.TABLE_OWNER,
            b.TABLE_NAME,
            b.DB_LINK
       FROM ALL_OBJECTS a,
            (SELECT TABLE_OWNER,
                    TABLE_NAME,
                    SYNONYM_NAME,
                    DB_LINK
               FROM ALL_SYNONYMS
              WHERE OWNER = 'PUBLIC') b
      WHERE a.OWNER = 'PWRPLANT'
        AND a.OBJECT_TYPE = 'PACKAGE'
        AND a.OBJECT_NAME NOT LIKE 'BIN$%'
        AND b.SYNONYM_NAME(+) = a.OBJECT_NAME
      ORDER BY a.OBJECT_NAME
   )
   LOOP
      BEGIN
         IF csr_create_sql.db_link IS NOT NULL THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is being used on a database link.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name||'@'||csr_create_sql.db_link);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSIF csr_create_sql.table_name <> csr_create_sql.object_name THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is not on the correct PWRPLANT object.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSIF csr_create_sql.table_owner <> 'PWRPLANT' THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is not for a PWRPLANT object.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSE
            IF csr_create_sql.table_name IS NOT NULL THEN
               v_rskipped := v_rskipped + 1;
            ELSE
               v_rcount := v_rcount + 1;
               EXECUTE IMMEDIATE csr_create_sql.create_sql;
               DBMS_OUTPUT.PUT_LINE(PPCSQL || csr_create_sql.create_sql || ';');
            END IF;
         END IF;
      EXCEPTION
      WHEN OTHERS THEN
         v_rerrors := v_rerrors + 1;
         DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || csr_create_sql.create_sql);
         DBMS_OUTPUT.PUT_LINE('SQLCODE = '||SQLCODE||' SQLERRM = '||SQLERRM);
      END;
   END LOOP;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rcount || ' PACKAGE Synonym(s) created.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rskipped || ' PACKAGE Synonym(s) already existed.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || (v_rcount + v_rskipped) || ' Total PACKAGE Synonym(s) processed.');
   DBMS_OUTPUT.NEW_LINE;
/*
|| FUNCTION SYNONYMS
*/
   DBMS_OUTPUT.PUT_LINE('/* FUNCTION Synonyms */');
   v_rcount := 0;
   v_rskipped := 0;
   FOR csr_create_sql IN
   ( SELECT a.OBJECT_NAME,
            'CREATE PUBLIC SYNONYM '||upper(a.OBJECT_NAME)||' FOR PWRPLANT."'||a.OBJECT_NAME||'"' CREATE_SQL,
            b.TABLE_OWNER,
            b.TABLE_NAME,
            b.DB_LINK
       FROM ALL_OBJECTS a,
            (SELECT TABLE_OWNER,
                    TABLE_NAME,
                    SYNONYM_NAME,
                    DB_LINK
               FROM ALL_SYNONYMS
              WHERE OWNER = 'PUBLIC') b
      WHERE a.OWNER = 'PWRPLANT'
        AND a.OBJECT_TYPE = 'FUNCTION'
        AND a.OBJECT_NAME NOT LIKE 'BIN$%'
        AND b.SYNONYM_NAME(+) = a.OBJECT_NAME
      ORDER BY a.OBJECT_NAME
   )
   LOOP
      BEGIN
         IF csr_create_sql.db_link IS NOT NULL THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is being used on a database link.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name||'@'||csr_create_sql.db_link);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSIF csr_create_sql.table_name <> csr_create_sql.object_name THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is not on the correct PWRPLANT object.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSIF csr_create_sql.table_owner <> 'PWRPLANT' THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is not for a PWRPLANT object.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSE
            IF csr_create_sql.table_name IS NOT NULL THEN
               v_rskipped := v_rskipped + 1;
            ELSE
               v_rcount := v_rcount + 1;
               EXECUTE IMMEDIATE csr_create_sql.create_sql;
               DBMS_OUTPUT.PUT_LINE(PPCSQL || csr_create_sql.create_sql || ';');
            END IF;
         END IF;
      EXCEPTION
      WHEN OTHERS THEN
         v_rerrors := v_rerrors + 1;
         DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || csr_create_sql.create_sql);
         DBMS_OUTPUT.PUT_LINE('SQLCODE = '||SQLCODE||' SQLERRM = '||SQLERRM);
      END;
   END LOOP;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rcount || ' FUNCTION Synonym(s) created.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rskipped || ' FUNCTION Synonym(s) already existed.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || (v_rcount + v_rskipped) || ' Total FUNCTION Synonym(s) processed.');
   DBMS_OUTPUT.NEW_LINE;
   
/*
|| TYPE SYNONYMS
*/
   DBMS_OUTPUT.PUT_LINE('/* TYPE Synonyms */');
   v_rcount := 0;
   v_rskipped := 0;
   FOR csr_create_sql IN
   ( SELECT a.OBJECT_NAME,
            'CREATE PUBLIC SYNONYM '||upper(a.OBJECT_NAME)||' FOR PWRPLANT."'||a.OBJECT_NAME||'"' CREATE_SQL,
            b.TABLE_OWNER,
            b.TABLE_NAME,
            b.DB_LINK
       FROM ALL_OBJECTS a,
            (SELECT TABLE_OWNER,
                    TABLE_NAME,
                    SYNONYM_NAME,
                    DB_LINK
               FROM ALL_SYNONYMS
              WHERE OWNER = 'PUBLIC') b
      WHERE A.OWNER = 'PWRPLANT'
        AND A.OBJECT_TYPE = 'TYPE'
        AND A.OBJECT_NAME NOT LIKE 'SYS_PLSQL%'
        AND A.OBJECT_NAME NOT LIKE 'BIN$%'
        AND b.SYNONYM_NAME(+) = A.OBJECT_NAME
      ORDER BY a.OBJECT_NAME
   )
   LOOP
      BEGIN
         IF csr_create_sql.db_link IS NOT NULL THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is being used on a database link.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name||'@'||csr_create_sql.db_link);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSIF csr_create_sql.table_name <> csr_create_sql.object_name THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is not on the correct PWRPLANT object.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSIF csr_create_sql.table_owner <> 'PWRPLANT' THEN
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Synonym already exists and is not for a PWRPLANT object.');
            DBMS_OUTPUT.PUT_LINE(PPCERR ||'Existing Synonym is defined on '||csr_create_sql.table_owner||
                                 '.'||csr_create_sql.table_name);
            v_rcount := v_rcount + 1;
            EXECUTE IMMEDIATE csr_create_sql.create_sql;
         ELSE
            IF csr_create_sql.table_name IS NOT NULL THEN
               v_rskipped := v_rskipped + 1;
            ELSE
               v_rcount := v_rcount + 1;
               EXECUTE IMMEDIATE csr_create_sql.create_sql;
               DBMS_OUTPUT.PUT_LINE(PPCSQL || csr_create_sql.create_sql || ';');
            END IF;
         END IF;
      EXCEPTION
      WHEN OTHERS THEN
         v_rerrors := v_rerrors + 1;
         DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || csr_create_sql.create_sql);
         DBMS_OUTPUT.PUT_LINE('SQLCODE = '||SQLCODE||' SQLERRM = '||SQLERRM);
      END;
   END LOOP;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rcount || ' TYPE Synonym(s) created.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rskipped || ' TYPE Synonym(s) already existed.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || (v_rcount + v_rskipped) || ' Total TYPE Synonym(s) processed.');
   DBMS_OUTPUT.NEW_LINE;
   
   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rerrors || ' Errors in Create Synonyms.');

   /* Look for SYNONYMS that are no longer valid. */
   /* Output the SQL to drop the SYNONYMS. */
   DBMS_OUTPUT.PUT_LINE('/* The following Public SYNONYMS are no longer reference a PWRPLANT object and can be removed. */');
   DBMS_OUTPUT.PUT_LINE('/* Note: The DROP SYNONYMS DDL statements have not been executed.  They should be verified     */');
   DBMS_OUTPUT.PUT_LINE('/* and then executed with a user id that has the DROP PUBLIC SYNONYM privilege.                */');
   v_rcount := 0;

   FOR csr_create_sql IN
   (select SYNONYM_NAME, TABLE_NAME,
          'drop public synonym '||SYNONYM_NAME drop_sql
      from ALL_SYNONYMS
     where TABLE_OWNER = 'PWRPLANT'
       and OWNER = 'PUBLIC'
       and TABLE_NAME not in (select OBJECT_NAME
                                from ALL_OBJECTS
                               where OWNER = 'PWRPLANT')
      order by SYNONYM_NAME
   )
   LOOP
      BEGIN
         v_rcount := v_rcount + 1;
         DBMS_OUTPUT.PUT_LINE(PPCSQL || csr_create_sql.drop_sql || ';');
      END;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE(PPCMSG || v_rcount || ' Synonym(s) need to be dropped.');

   /* Log the end of the run. */
   UPDATE PWRPLANT.PP_UPDATE_FLEX
      SET FLEXVCHAR2 = 'Status = Complete',
          FLEXVCHAR3 = v_rerrors || ' ERROR(s)'
    WHERE TYPE_NAME = 'UPDATE_SCRIPTS'
          AND COL_ID IN (SELECT MAX(COL_ID)
                           FROM PWRPLANT.PP_UPDATE_FLEX
                          WHERE FLEXVCHAR1 = 'create_synonyms.sql'
                            AND TYPE_NAME = 'UPDATE_SCRIPTS');
   COMMIT;
END;
/
/*
|| END Create Synonyms
*/
SPOOL OFF
SET ECHO ON