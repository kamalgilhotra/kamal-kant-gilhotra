SET ECHO ON
SET SERVEROUTPUT ON
SET LINESIZE 300
SET TIME ON
SET SQLPROMPT '_CONNECT_IDENTIFIER> '

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''
HOST MKDIR &&PP_SCRIPT_PATH.UPGRADE_UTILITIES\LOGS
SPOOL &&PP_SCRIPT_PATH.UPGRADE_UTILITIES\LOGS\create_timestamp_triggers.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: create_timestamp_triggers.sql
|| Description: Create the user/timestamp triggers that don't exist already
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version   Date       Revised By     Reason for Change
|| --------  ---------- -------------- -----------------------------------------
|| V10       10/24/2007 Lee Quinn      Created
|| V10.1.    12/19/2007 Lee Quinn      Added logging
|| V10.1.3   03/16/2009 Lee Quinn      Added drops for temporary tables.
|| V10.2     03/24/2009 Lee Quinn      Version update
|| V10.2.1.6 03/23/2011 Lee Quinn      Added triggers to TEMP_WORK_ORDER, TEMP_BUDGET 
||                                     Global Temp Tables.
|| 10.3.5.0  07/30/2012 Lee Quinn      Point release
||============================================================================
*/

declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   V_RCOUNT      number;
   V_DROP_RCOUNT number;
   V_RERRORS     number;
   V_RSKIPPED    number;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);
   -- Log the start of the run
   insert into PWRPLANT.PP_UPDATE_FLEX
      (COL_ID, TYPE_NAME, FLEXVCHAR1, FLEXVCHAR2)
      select NVL(max(COL_ID), 0) + 1,
             'UPDATE_SCRIPTS',
             'create_timestamp_triggers.sql',
             'Status = Running'
        from PWRPLANT.PP_UPDATE_FLEX;
   commit;

   V_RERRORS     := 0;
   V_DROP_RCOUNT := 0;
   V_RCOUNT   := 0;
   V_RSKIPPED := 0;

   DBMS_OUTPUT.PUT_LINE('/* Drop User/Timestamp Triggers on TEMP Tables */');

   for CSR_DROP_DDL in (select 'DROP TRIGGER ' || TRIGGER_NAME DROP_DDL
                          from ALL_TRIGGERS T1, ALL_TABLES T2
                         where T1.TABLE_OWNER = 'PWRPLANT'
                           and T2.OWNER = 'PWRPLANT'
                           and T2.TABLE_NAME = T1.TRIGGER_NAME
                           and T2.TEMPORARY = 'Y'
                           and T1.TRIGGER_TYPE = 'BEFORE EACH ROW'
                           and T1.TRIGGERING_EVENT = 'INSERT OR UPDATE'
                           and T1.TABLE_NAME not in ('TEMP_WORK_ORDER', 'TEMP_BUDGET')
                         order by TRIGGER_NAME)
   loop
      begin
         -- If you don't want to automatically DROP the triggers then comment
         -- out the following line "execute immediate" and the script will just
         -- return the DDL that would have been run.
         execute immediate CSR_DROP_DDL.DROP_DDL;
         V_DROP_RCOUNT := V_DROP_RCOUNT + 1;
         DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_DROP_DDL.DROP_DDL || ';');
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL > ' || CSR_DROP_DDL.DROP_DDL);
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
      end;
   end loop;

   if V_DROP_RCOUNT < 1 then
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'No triggers needed to be dropped.');
   end if;

   DBMS_OUTPUT.PUT_LINE('/* Create User/Timestamp Triggers */');

   for CSR_CREATE_DDL in (select 'create or replace trigger PWRPLANT."' || ATST.TABLE_NAME || '"' ||
                                 CHR(13) || '   before update or insert on PWRPLANT."' ||
                                 ATST.TABLE_NAME || '"' || CHR(13) || '   for each row' || CHR(13) ||
                                 'begin ' ||
                                 '   :new.user_id := SYS_CONTEXT (''USERENV'', ''SESSION_USER''); ' ||
                                 '   :new.time_stamp := SYSDATE; ' || 'end;' CREATE_DDL,
                                 'create or replace trigger PWRPLANT."' || ATST.TABLE_NAME || '"' ||
                                 CHR(13) LINE1,
                                 '   before update or insert on PWRPLANT."' || ATST.TABLE_NAME || '"' ||
                                 CHR(13) LINE2,
                                 '   for each row' || CHR(13) LINE3,
                                 'begin ' || CHR(13) LINE4,
                                 '   :new.user_id := SYS_CONTEXT (''USERENV'', ''SESSION_USER''); ' ||
                                 CHR(13) LINE5,
                                 '   :new.time_stamp := SYSDATE; ' || CHR(13) LINE6,
                                 'end;' LINE7,
                                 NVL(TRIGGER_EXISTS, 'FALSE') TRIGGER_EXISTS
                            from ((select T.TABLE_NAME
                                     from ALL_TAB_COLUMNS C, ALL_TABLES T
                                    where T.OWNER = 'PWRPLANT'
                                      and T.OWNER = C.OWNER
                                      and COLUMN_NAME = 'TIME_STAMP'
                                      and T.TABLE_NAME = C.TABLE_NAME
                                      and T.TEMPORARY <> 'Y'
                                   intersect
                                   select T.TABLE_NAME
                                     from ALL_TAB_COLUMNS C, ALL_TABLES T
                                    where T.OWNER = 'PWRPLANT'
                                      and T.OWNER = C.OWNER
                                      and COLUMN_NAME = 'USER_ID'
                                      and T.TABLE_NAME = C.TABLE_NAME
                                      and T.TEMPORARY <> 'Y') union all
                                  (select T.TABLE_NAME
                                     from ALL_TAB_COLUMNS C, ALL_TABLES T
                                    where T.OWNER = 'PWRPLANT'
                                      and T.OWNER = C.OWNER
                                      and COLUMN_NAME = 'TIME_STAMP'
                                      and T.TABLE_NAME = C.TABLE_NAME
                                      and T.TEMPORARY = 'Y'
                                      and T.TABLE_NAME in ('TEMP_WORK_ORDER', 'TEMP_BUDGET')
                                   intersect
                                   select T.TABLE_NAME
                                     from ALL_TAB_COLUMNS C, ALL_TABLES T
                                    where T.OWNER = 'PWRPLANT'
                                      and T.OWNER = C.OWNER
                                      and COLUMN_NAME = 'USER_ID'
                                      and T.TABLE_NAME = C.TABLE_NAME
                                      and T.TEMPORARY = 'Y'
                                      and T.TABLE_NAME in ('TEMP_WORK_ORDER', 'TEMP_BUDGET'))) ATST,
                                 (select AT.TABLE_NAME, 'TRUE' TRIGGER_EXISTS
                                    from ALL_TRIGGERS AT
                                   where AT.OWNER = 'PWRPLANT'
                                     and AT.TABLE_NAME = AT.TRIGGER_NAME) AT
                           where ATST.TABLE_NAME = AT.TABLE_NAME(+)
                           order by TRIGGER_EXISTS DESC, AT.TABLE_NAME)
   loop
      begin
         -- If the trigger already exists don't recreate
         if CSR_CREATE_DDL.TRIGGER_EXISTS = 'TRUE' then
            V_RSKIPPED := V_RSKIPPED + 1;
         else
            V_RCOUNT := V_RCOUNT + 1;
            -- If you don't want to automatically create the triggers then comment
            -- out the following line "execute immediate" and the script will just
            -- return the DDL that would have been run.
            execute immediate CSR_CREATE_DDL.CREATE_DDL;
            DBMS_OUTPUT.PUT_LINE(PPCSQL);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE1);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE2);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE3);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE4);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE5);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE6);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE7);
            DBMS_OUTPUT.PUT_LINE('/');
         end if;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL > ');
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE1);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE2);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE3);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE4);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE5);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE6);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE7);
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
      end;
   end loop;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_DROP_RCOUNT || ' TABLE TRIGGER(s) dropped.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' TABLE Trigger(s) created.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RSKIPPED || ' TABLE Trigger(s) already existed.');
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RERRORS || ' Errors in Create Triggers.');

   update PWRPLANT.PP_UPDATE_FLEX
      set FLEXVCHAR2 = 'Status = Complete',
          FLEXVCHAR3 = V_DROP_RCOUNT || ' TRIGGER(s) dropped.',
          FLEXVCHAR4 = V_RCOUNT || ' TRIGGER(s) created.',
          FLEXVCHAR5 = V_RERRORS || ' ERROR(s)',
          FLEXVCHAR6 = V_RSKIPPED || ' TABLE TRIGGER(s) already existed.'
    where TYPE_NAME = 'UPDATE_SCRIPTS'
      and COL_ID in (select max(COL_ID)
                       from PWRPLANT.PP_UPDATE_FLEX
                      where FLEXVCHAR1 = 'create_timestamp_triggers.sql'
                        and TYPE_NAME = 'UPDATE_SCRIPTS');
   commit;
end;
/

/*
|| END Create User/Timespamp Triggers
*/
SPOOL OFF
SET ECHO ON