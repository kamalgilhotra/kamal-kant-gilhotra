SET SERVEROUTPUT ON

declare
   L_COUNT number;
begin
   select count(*) into L_COUNT from PEND_TRANSACTION;
   if L_COUNT > 0 then
      RAISE_APPLICATION_ERROR(-20000,
                              'PEND_TRANSACTION has data.  Do not continue the upgrade until this has been processed and cleared.');
   else
      DBMS_OUTPUT.PUT_LINE('PEND_TRANSACTION has 0 rows so upgrade can continue.');
   end if;
end;
/