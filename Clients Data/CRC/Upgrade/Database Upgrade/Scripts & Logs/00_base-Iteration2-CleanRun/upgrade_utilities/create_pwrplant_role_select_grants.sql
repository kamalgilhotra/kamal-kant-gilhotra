SET ECHO OFF
SET SERVEROUTPUT ON
SET LINESIZE 120
/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

/*
||============================================================================
|| Application: PowerPlan
|| Object Name: create_pwrplant_role_select_grants.sql
|| Description: Create the PWRPLANT_ROLE_SELECT and assign select grants.
||              This role can be used to alow specific users to Select from
||              PWRPLANT tables outside of the system.  The role is not password
||              protected and should be given as a defaut role to specif users.
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.6.0   11/07/2012 Lee Quinn      Created
||============================================================================
*/
HOST MKDIR &&PP_SCRIPT_PATH.UPGRADE_UTILITIES\LOGS
SPOOL &&PP_SCRIPT_PATH.UPGRADE_UTILITIES\LOGS\create_pwrplant_role_select_grants.log
SET ECHO ON
SET SERVEROUTPUT ON

-- Create the PWRPLANT_ROLE_SELECT if it doesn't exist
-- Ignore error if it already exists
declare
   ROLE_EXISTS exception;
   pragma exception_init(ROLE_EXISTS, -01921);

begin
   execute immediate 'create role PWRPLANT_ROLE_SELECT';
exception
   when ROLE_EXISTS then
      DBMS_OUTPUT.PUT_LINE('The PWRPLANT_ROLE_SELECT role already exists.');
end;
/

SET ECHO OFF
declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   V_RCOUNT  number;
   V_RERRORS number;

begin
   DBMS_OUTPUT.ENABLE(2000000);
   -- Log the start of the run
   insert into PWRPLANT.PP_UPDATE_FLEX
      (COL_ID,
       TYPE_NAME,
       FLEXVCHAR1,
       FLEXVCHAR2)
      select NVL(max(COL_ID), 0) + 1,
             'UPDATE_SCRIPTS',
             'create_pwrplant_role_select.sql',
             'Status = Running'
        from PWRPLANT.PP_UPDATE_FLEX;
   commit;

   V_RERRORS := 0;
   /*
   || TABLE GRANTS for PWRPLANT_ROLE_SELECT
   */
   DBMS_OUTPUT.PUT_LINE('/* TABLE Grants for PWRPLANT_ROLE_SELECT */');
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select 'grant select on PWRPLANT."' || OBJECT_NAME ||
                                 '" to PWRPLANT_ROLE_SELECT' CREATE_SQL
                            from ALL_OBJECTS
                           where OWNER = 'PWRPLANT'
                             and OBJECT_TYPE = 'TABLE'
                             and OBJECT_NAME not like 'BIN$%'
                             and GENERATED <> 'Y'
                           order by OBJECT_NAME)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.CREATE_SQL || ';');
      begin
         execute immediate CSR_CREATE_SQL.CREATE_SQL;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_CREATE_SQL.CREATE_SQL);
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
            null;
      end;
   end loop;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' TABLE Grants created for PWRPLANT_ROLE_SELECT.');
   DBMS_OUTPUT.NEW_LINE;
   /*
   || VIEW GRANTS
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE('/* VIEW Grants for PWRPLANT_ROLE_SELECT */');
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select 'grant select on PWRPLANT."' || OBJECT_NAME ||
                                 '" to PWRPLANT_ROLE_SELECT' CREATE_SQL
                            from ALL_OBJECTS
                           where OWNER = 'PWRPLANT'
                             and OBJECT_TYPE = 'VIEW'
                             and OBJECT_NAME not like 'BIN$%'
                           order by OBJECT_NAME)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.CREATE_SQL || ';');
      begin
         execute immediate CSR_CREATE_SQL.CREATE_SQL;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_CREATE_SQL.CREATE_SQL);
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
            null;
      end;
   end loop;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' VIEW Grants created for PWRPLANT_ROLE_SELECT.');
   DBMS_OUTPUT.NEW_LINE;
   /*
   || SHOW ERRORS and Log Run
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RERRORS || ' Errors in Create Grants.');

   update PWRPLANT.PP_UPDATE_FLEX
      set FLEXVCHAR2 = 'Status = Complete', FLEXVCHAR3 = V_RERRORS || ' ERROR(s)'
    where TYPE_NAME = 'UPDATE_SCRIPTS'
      and COL_ID in (select max(COL_ID)
                       from PWRPLANT.PP_UPDATE_FLEX
                      where FLEXVCHAR1 = 'create_pwrplant_role_select.sql'
                        and TYPE_NAME = 'UPDATE_SCRIPTS');
   commit;
end;
/
/*
|| END Create Grants
*/

SET ECHO ON
SPOOL OFF