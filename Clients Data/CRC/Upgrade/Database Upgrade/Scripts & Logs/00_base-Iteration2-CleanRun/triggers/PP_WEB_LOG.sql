
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."PP_WEB_LOG" 
  BEFORE INSERT ON PP_WEB_LOG
  FOR EACH ROW
BEGIN
    :new.WEB_LOG_ID := PP_WEB_LOG_SEQ.nextval;
    :new.TIME_STAMP := SYSDATE;
END;

/
ALTER TRIGGER "PWRPLANT"."PP_WEB_LOG" ENABLE;