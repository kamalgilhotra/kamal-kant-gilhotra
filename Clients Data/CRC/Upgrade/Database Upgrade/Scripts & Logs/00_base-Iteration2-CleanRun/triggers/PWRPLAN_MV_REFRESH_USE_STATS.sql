
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."PWRPLAN_MV_REFRESH_USE_STATS" 
AFTER LOGON ON DATABASE
BEGIN
  IF LOWER(sys_context('USERENV', 'MODULE')) IN ('powerpla.exe', 'ssp_lease_control.exe')
      OR LOWER(sys_context('USERENV', 'MODULE')) LIKE 'post%.exe'
      OR LOWER(sys_context('USERENV', 'SESSION_USER')) = 'pwrplant'
  THEN
    EXECUTE IMMEDIATE 'alter session set "_mv_refresh_use_stats" = TRUE';
  END IF;
END;

/
ALTER TRIGGER "PWRPLANT"."PWRPLAN_MV_REFRESH_USE_STATS" ENABLE;