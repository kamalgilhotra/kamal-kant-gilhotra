
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."PP_AU_COMP_UNIT_HIERARCHY_DEL" 
   after delete or update on COMP_UNIT_HIERARCHY
   for each row
declare
   NUMBER_OF_CHILDREN number(22, 0);
   pragma autonomous_transaction;
begin
   select count(1)
     into NUMBER_OF_CHILDREN
     from COMP_UNIT_HIERARCHY
    where COMP_UNIT_ID <> :OLD.COMP_UNIT_ID
      and PARENT_COMP_UNIT_ID = :OLD.PARENT_COMP_UNIT_ID;
   if NUMBER_OF_CHILDREN = 0 then
      update COMPATIBLE_UNIT set HAS_CHILDREN = 0 where COMP_UNIT_ID = :OLD.PARENT_COMP_UNIT_ID;
   end if;
   commit;
end;


/
ALTER TRIGGER "PWRPLANT"."PP_AU_COMP_UNIT_HIERARCHY_DEL" ENABLE;