
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."LOGON_AUDIT_TRIGGER" 
   after LOGON on database

   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: LOGON_AUDIT_TRIGGER
   || Description: Fill in the PWRPLANT_CTX context with user information upon login
   ||              This will facilitate audit information when users update PWRPLANT
   ||              tables outside of the application.
   ||============================================================================
   || Copyright (C) 2010 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version    Date       Revised By     Reason for Change
   || ---------- ---------- -------------- -----------------------------------------
   || 10.2.1.5.0 08/10/2010 Lee Quinn      Created
   ||============================================================================
   */

declare
   I number;

begin
   I := PWRPLANT.AUDIT_TABLE_PKG.SETWINDOWCONTEXT('<External>', '<External>', 'yes');
end;






/
ALTER TRIGGER "PWRPLANT"."LOGON_AUDIT_TRIGGER" ENABLE;