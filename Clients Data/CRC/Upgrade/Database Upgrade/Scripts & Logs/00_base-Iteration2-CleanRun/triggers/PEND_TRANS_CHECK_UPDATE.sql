
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."PEND_TRANS_CHECK_UPDATE" 
   after update on PEND_TRANSACTION
   for each row
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PEND_TRANS_CHECK_UPDATE
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/11/2013 Charlie Shilling Maint-29286
   || 10.4.2.0 09/30/2013 Charlie Shilling Maint-29713
   ||============================================================================
   */
declare
   V_OWNER    varchar2(2000);
   V_NAME     varchar2(2000);
   V_LINENO   number;
   V_CALLER_T varchar2(2000);
begin
   OWA_UTIL.WHO_CALLED_ME(V_OWNER, V_NAME, V_LINENO, V_CALLER_T);

   --used for debugging
   --  Dbms_Output.put_line('Owner: '|| v_owner);
   --  Dbms_Output.put_line('Name: '|| v_name);
   --  Dbms_Output.put_line('Line No.: '|| v_lineno);
   --  Dbms_Output.put_line('Caller Type: '|| v_caller_t);

   if V_NAME is null or (V_NAME is not null and V_NAME <> 'P_UPDATE_PEND_TRANS_RES_GL_REV') then
      if :NEW.RESERVE <> :OLD.RESERVE then
         RAISE_APPLICATION_ERROR(-20000,
                                 'Updating RESERVE on PEND_TRANSACTION is not permitted. Use ADJUSTED_RESERVE instead.');
      elsif :NEW.COST_OF_REMOVAL <> :OLD.COST_OF_REMOVAL then
         RAISE_APPLICATION_ERROR(-20000,
                                 'Updating COST_OF_REMOVAL on PEND_TRANSACTION is not permitted. Use ADJUSTED_COST_OF_REMOVAL instead.');
      elsif :NEW.SALVAGE_CASH <> :OLD.SALVAGE_CASH then
         RAISE_APPLICATION_ERROR(-20000,
                                 'Updating SALVAGE_CASH on PEND_TRANSACTION is not permitted. Use ADJUSTED_SALVAGE_CASH instead.');
      elsif :NEW.SALVAGE_RETURNS <> :OLD.SALVAGE_RETURNS then
         RAISE_APPLICATION_ERROR(-20000,
                                 'Updating SALVAGE_RETURNS on PEND_TRANSACTION is not permitted. Use ADJUSTED_SALVAGE_RETURNS instead.');
      elsif :NEW.RESERVE_CREDITS <> :OLD.RESERVE_CREDITS then
         RAISE_APPLICATION_ERROR(-20000,
                                 'Updating RESERVE_CREDITS on PEND_TRANSACTION is not permitted. Use ADJUSTED_RESERVE_CREDITS instead.');
      end if;
   end if;
end;


/
ALTER TRIGGER "PWRPLANT"."PEND_TRANS_CHECK_UPDATE" ENABLE;