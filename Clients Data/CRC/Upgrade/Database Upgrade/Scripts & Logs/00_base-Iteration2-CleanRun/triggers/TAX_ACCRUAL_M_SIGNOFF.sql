
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."TAX_ACCRUAL_M_SIGNOFF" 
before update of amount_act
on tax_accrual_m_item
for each row
begin
if :OLD.amount_act <> :NEW.amount_act AND :OLD.signoff_ind = 1
then
	:NEW.signoff_ind := 0;
end if;
end;

/
ALTER TRIGGER "PWRPLANT"."TAX_ACCRUAL_M_SIGNOFF" ENABLE;