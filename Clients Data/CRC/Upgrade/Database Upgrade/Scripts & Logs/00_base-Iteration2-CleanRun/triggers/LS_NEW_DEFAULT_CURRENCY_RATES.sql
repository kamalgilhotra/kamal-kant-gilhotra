
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."LS_NEW_DEFAULT_CURRENCY_RATES" 
AFTER INSERT ON currency
FOR EACH ROW
  BEGIN
	--Add defaults for daily rates
    INSERT INTO currency_rate_default (exchange_date, currency_from, currency_to, exchange_rate_type_id, rate)
    VALUES ('01-Jan-1900', :new.currency_id, :new.currency_id, 1, 1);
    
    INSERT INTO currency_rate_default (exchange_date, currency_from, currency_to, exchange_rate_type_id, rate)
    SELECT '01-Jan-1900', :new.currency_id, currency_to, 1, 0
    FROM currency_rate_default
    WHERE currency_to <> :new.currency_id
    UNION
    SELECT '01-Jan-1900', currency_from, :new.currency_id, 1, 0
    FROM currency_rate_default
    WHERE currency_from <> :new.currency_id;
	
	--Add defaults for average rates
    INSERT INTO currency_rate_default (exchange_date, currency_from, currency_to, exchange_rate_type_id, rate)
    VALUES ('01-Jan-1900', :new.currency_id, :new.currency_id, 4, 1);
    
    INSERT INTO currency_rate_default (exchange_date, currency_from, currency_to, exchange_rate_type_id, rate)
    SELECT '01-Jan-1900', :new.currency_id, currency_to, 4, 0
    FROM currency_rate_default
    WHERE currency_to <> :new.currency_id
    UNION
    SELECT '01-Jan-1900', currency_from, :new.currency_id, 4, 0
    FROM currency_rate_default
    WHERE currency_from <> :new.currency_id;
  END;
/
ALTER TRIGGER "PWRPLANT"."LS_NEW_DEFAULT_CURRENCY_RATES" ENABLE;