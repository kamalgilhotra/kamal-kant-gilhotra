
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."PP_WEB_VIEW_CHANGE_ID" 
                        BEFORE INSERT ON "PWRPLANT"."PP_WEB_VIEW_CHANGE"
                        FOR EACH ROW
                        BEGIN
                            SELECT pwrplant.PP_WEB_VIEW_CHANGE_SEQ.nextval into :new.CHANGE_ID
                           FROM dual;
                        END;


/
ALTER TRIGGER "PWRPLANT"."PP_WEB_VIEW_CHANGE_ID" ENABLE;