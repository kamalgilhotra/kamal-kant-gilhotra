
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."DELETE_DISTINCT_ASSET_ILRS" 
after delete or update on lsr_asset
declare
begin
  delete from lsr_distinct_asset_ilr a
  where not exists (select 1 from lsr_asset b where a.asset_id = b.lsr_asset_id and a.ilr_id = b.ilr_id);
end;
/
ALTER TRIGGER "PWRPLANT"."DELETE_DISTINCT_ASSET_ILRS" ENABLE;