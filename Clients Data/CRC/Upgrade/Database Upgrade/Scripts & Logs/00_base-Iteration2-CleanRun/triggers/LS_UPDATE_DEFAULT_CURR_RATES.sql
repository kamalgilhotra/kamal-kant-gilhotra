
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."LS_UPDATE_DEFAULT_CURR_RATES" 
AFTER UPDATE ON currency
FOR EACH ROW
  BEGIN
    IF :new.currency_id <> :old.currency_id THEN
      UPDATE currency_rate_default
      SET currency_from = :new.currency_id,
          currency_to = :new.currency_id
      WHERE currency_from = :old.currency_id
      AND currency_to = :old.currency_id
      AND rate = 1;

      UPDATE currency_rate_default
      SET currency_from = :new.currency_id
      WHERE currency_from = :old.currency_id
      AND currency_to <> :old.currency_id
      AND rate = 0;

      UPDATE currency_rate_default
      SET currency_to = :new.currency_id
      WHERE currency_to = :old.currency_id
      AND currency_from <> :old.currency_id
      AND rate = 0;
    END IF;
  END;
/
ALTER TRIGGER "PWRPLANT"."LS_UPDATE_DEFAULT_CURR_RATES" ENABLE;