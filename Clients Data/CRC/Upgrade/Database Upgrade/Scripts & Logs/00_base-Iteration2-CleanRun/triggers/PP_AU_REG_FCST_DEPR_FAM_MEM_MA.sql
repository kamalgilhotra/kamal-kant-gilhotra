
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."PP_AU_REG_FCST_DEPR_FAM_MEM_MA" AFTER
 INSERT  OR  UPDATE  OR  DELETE  OF FCST_DEPR_GROUP_ID, FORECAST_VERSION_ID, REG_FAMILY_ID, REG_MEMBER_ID
 ON reg_fcst_depr_fam_mem_map FOR EACH ROW
DECLARE
  prog varchar2(60);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF UPDATING THEN
   IF :old.FCST_DEPR_GROUP_ID <> :new.FCST_DEPR_GROUP_ID OR
     (:old.FCST_DEPR_GROUP_ID is null AND :new.FCST_DEPR_GROUP_ID is not null) OR
     (:new.FCST_DEPR_GROUP_ID is null AND :old.FCST_DEPR_GROUP_ID is not null) THEN


      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_FAM_MEM_MAP', 'FCST_DEPR_GROUP_ID',
        ''|| 'FCST_DEPR_GROUP_ID=' || :new.FCST_DEPR_GROUP_ID||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; ', pk_lookup, :old.FCST_DEPR_GROUP_ID, :new.FCST_DEPR_GROUP_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.FORECAST_VERSION_ID <> :new.FORECAST_VERSION_ID OR
     (:old.FORECAST_VERSION_ID is null AND :new.FORECAST_VERSION_ID is not null) OR
     (:new.FORECAST_VERSION_ID is null AND :old.FORECAST_VERSION_ID is not null) THEN


      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_FAM_MEM_MAP', 'FORECAST_VERSION_ID',
        ''|| 'FCST_DEPR_GROUP_ID=' || :new.FCST_DEPR_GROUP_ID||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; ', pk_lookup, :old.FORECAST_VERSION_ID, :new.FORECAST_VERSION_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.REG_FAMILY_ID <> :new.REG_FAMILY_ID OR
     (:old.REG_FAMILY_ID is null AND :new.REG_FAMILY_ID is not null) OR
     (:new.REG_FAMILY_ID is null AND :old.REG_FAMILY_ID is not null) THEN


      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_FAM_MEM_MAP', 'REG_FAMILY_ID',
        ''|| 'FCST_DEPR_GROUP_ID=' || :new.FCST_DEPR_GROUP_ID||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; ', pk_lookup, :old.REG_FAMILY_ID, :new.REG_FAMILY_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.REG_MEMBER_ID <> :new.REG_MEMBER_ID OR
     (:old.REG_MEMBER_ID is null AND :new.REG_MEMBER_ID is not null) OR
     (:new.REG_MEMBER_ID is null AND :old.REG_MEMBER_ID is not null) THEN


      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_FAM_MEM_MAP', 'REG_MEMBER_ID',
        ''|| 'FCST_DEPR_GROUP_ID=' || :new.FCST_DEPR_GROUP_ID||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; ', pk_lookup, :old.REG_MEMBER_ID, :new.REG_MEMBER_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
END IF;
IF INSERTING THEN
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_FAM_MEM_MAP', 'FCST_DEPR_GROUP_ID',
        ''|| 'FCST_DEPR_GROUP_ID=' || :new.FCST_DEPR_GROUP_ID||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; ', pk_lookup, :new.FCST_DEPR_GROUP_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_FAM_MEM_MAP', 'FORECAST_VERSION_ID',
        ''|| 'FCST_DEPR_GROUP_ID=' || :new.FCST_DEPR_GROUP_ID||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; ', pk_lookup, :new.FORECAST_VERSION_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_FAM_MEM_MAP', 'REG_FAMILY_ID',
        ''|| 'FCST_DEPR_GROUP_ID=' || :new.FCST_DEPR_GROUP_ID||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; ', pk_lookup, :new.REG_FAMILY_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_FAM_MEM_MAP', 'REG_MEMBER_ID',
        ''|| 'FCST_DEPR_GROUP_ID=' || :new.FCST_DEPR_GROUP_ID||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; ', pk_lookup, :new.REG_MEMBER_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
END IF;
IF DELETING THEN
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_FAM_MEM_MAP', 'FCST_DEPR_GROUP_ID',
        ''|| 'FCST_DEPR_GROUP_ID=' || :old.FCST_DEPR_GROUP_ID||'; '|| 'FORECAST_VERSION_ID=' || :old.FORECAST_VERSION_ID||'; ', pk_lookup, :old.FCST_DEPR_GROUP_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_FAM_MEM_MAP', 'FORECAST_VERSION_ID',
        ''|| 'FCST_DEPR_GROUP_ID=' || :old.FCST_DEPR_GROUP_ID||'; '|| 'FORECAST_VERSION_ID=' || :old.FORECAST_VERSION_ID||'; ', pk_lookup, :old.FORECAST_VERSION_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_FAM_MEM_MAP', 'REG_FAMILY_ID',
        ''|| 'FCST_DEPR_GROUP_ID=' || :old.FCST_DEPR_GROUP_ID||'; '|| 'FORECAST_VERSION_ID=' || :old.FORECAST_VERSION_ID||'; ', pk_lookup, :old.REG_FAMILY_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_FAM_MEM_MAP', 'REG_MEMBER_ID',
        ''|| 'FCST_DEPR_GROUP_ID=' || :old.FCST_DEPR_GROUP_ID||'; '|| 'FORECAST_VERSION_ID=' || :old.FORECAST_VERSION_ID||'; ', pk_lookup, :old.REG_MEMBER_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
END IF;
END;


/
ALTER TRIGGER "PWRPLANT"."PP_AU_REG_FCST_DEPR_FAM_MEM_MA" ENABLE;