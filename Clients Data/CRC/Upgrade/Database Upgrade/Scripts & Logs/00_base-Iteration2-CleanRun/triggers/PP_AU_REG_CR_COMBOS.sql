
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."PP_AU_REG_CR_COMBOS" AFTER
 INSERT  OR  UPDATE  OR  DELETE  OF CR_AMT_TYPE, CR_COMBO_ID, CR_COMPANY, CR_TABLE_ID, DESCRIPTION, HIST_COMBO_ID, PULL_TYPE_ID
 ON reg_cr_combos FOR EACH ROW
DECLARE
  prog varchar2(60);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF UPDATING THEN
   IF :old.CR_AMT_TYPE <> :new.CR_AMT_TYPE OR
     (:old.CR_AMT_TYPE is null AND :new.CR_AMT_TYPE is not null) OR
     (:new.CR_AMT_TYPE is null AND :old.CR_AMT_TYPE is not null) THEN


      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'CR_AMT_TYPE',
        ''|| 'CR_COMBO_ID=' || :new.CR_COMBO_ID||'; ', pk_lookup, :old.CR_AMT_TYPE, :new.CR_AMT_TYPE,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.CR_COMBO_ID <> :new.CR_COMBO_ID OR
     (:old.CR_COMBO_ID is null AND :new.CR_COMBO_ID is not null) OR
     (:new.CR_COMBO_ID is null AND :old.CR_COMBO_ID is not null) THEN


      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'CR_COMBO_ID',
        ''|| 'CR_COMBO_ID=' || :new.CR_COMBO_ID||'; ', pk_lookup, :old.CR_COMBO_ID, :new.CR_COMBO_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.CR_COMPANY <> :new.CR_COMPANY OR
     (:old.CR_COMPANY is null AND :new.CR_COMPANY is not null) OR
     (:new.CR_COMPANY is null AND :old.CR_COMPANY is not null) THEN


      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'CR_COMPANY',
        ''|| 'CR_COMBO_ID=' || :new.CR_COMBO_ID||'; ', pk_lookup, :old.CR_COMPANY, :new.CR_COMPANY,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.CR_TABLE_ID <> :new.CR_TABLE_ID OR
     (:old.CR_TABLE_ID is null AND :new.CR_TABLE_ID is not null) OR
     (:new.CR_TABLE_ID is null AND :old.CR_TABLE_ID is not null) THEN


      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'CR_TABLE_ID',
        ''|| 'CR_COMBO_ID=' || :new.CR_COMBO_ID||'; ', pk_lookup, :old.CR_TABLE_ID, :new.CR_TABLE_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.DESCRIPTION <> :new.DESCRIPTION OR
     (:old.DESCRIPTION is null AND :new.DESCRIPTION is not null) OR
     (:new.DESCRIPTION is null AND :old.DESCRIPTION is not null) THEN


      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'DESCRIPTION',
        ''|| 'CR_COMBO_ID=' || :new.CR_COMBO_ID||'; ', pk_lookup, :old.DESCRIPTION, :new.DESCRIPTION,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.HIST_COMBO_ID <> :new.HIST_COMBO_ID OR
     (:old.HIST_COMBO_ID is null AND :new.HIST_COMBO_ID is not null) OR
     (:new.HIST_COMBO_ID is null AND :old.HIST_COMBO_ID is not null) THEN


      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'HIST_COMBO_ID',
        ''|| 'CR_COMBO_ID=' || :new.CR_COMBO_ID||'; ', pk_lookup, :old.HIST_COMBO_ID, :new.HIST_COMBO_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.PULL_TYPE_ID <> :new.PULL_TYPE_ID OR
     (:old.PULL_TYPE_ID is null AND :new.PULL_TYPE_ID is not null) OR
     (:new.PULL_TYPE_ID is null AND :old.PULL_TYPE_ID is not null) THEN


      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'PULL_TYPE_ID',
        ''|| 'CR_COMBO_ID=' || :new.CR_COMBO_ID||'; ', pk_lookup, :old.PULL_TYPE_ID, :new.PULL_TYPE_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
END IF;
IF INSERTING THEN
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'CR_AMT_TYPE',
        ''|| 'CR_COMBO_ID=' || :new.CR_COMBO_ID||'; ', pk_lookup, :new.CR_AMT_TYPE,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'CR_COMBO_ID',
        ''|| 'CR_COMBO_ID=' || :new.CR_COMBO_ID||'; ', pk_lookup, :new.CR_COMBO_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'CR_COMPANY',
        ''|| 'CR_COMBO_ID=' || :new.CR_COMBO_ID||'; ', pk_lookup, :new.CR_COMPANY,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'CR_TABLE_ID',
        ''|| 'CR_COMBO_ID=' || :new.CR_COMBO_ID||'; ', pk_lookup, :new.CR_TABLE_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'DESCRIPTION',
        ''|| 'CR_COMBO_ID=' || :new.CR_COMBO_ID||'; ', pk_lookup, :new.DESCRIPTION,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'HIST_COMBO_ID',
        ''|| 'CR_COMBO_ID=' || :new.CR_COMBO_ID||'; ', pk_lookup, :new.HIST_COMBO_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'PULL_TYPE_ID',
        ''|| 'CR_COMBO_ID=' || :new.CR_COMBO_ID||'; ', pk_lookup, :new.PULL_TYPE_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
END IF;
IF DELETING THEN
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'CR_AMT_TYPE',
        ''|| 'CR_COMBO_ID=' || :old.CR_COMBO_ID||'; ', pk_lookup, :old.CR_AMT_TYPE,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'CR_COMBO_ID',
        ''|| 'CR_COMBO_ID=' || :old.CR_COMBO_ID||'; ', pk_lookup, :old.CR_COMBO_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'CR_COMPANY',
        ''|| 'CR_COMBO_ID=' || :old.CR_COMBO_ID||'; ', pk_lookup, :old.CR_COMPANY,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'CR_TABLE_ID',
        ''|| 'CR_COMBO_ID=' || :old.CR_COMBO_ID||'; ', pk_lookup, :old.CR_TABLE_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'DESCRIPTION',
        ''|| 'CR_COMBO_ID=' || :old.CR_COMBO_ID||'; ', pk_lookup, :old.DESCRIPTION,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'HIST_COMBO_ID',
        ''|| 'CR_COMBO_ID=' || :old.CR_COMBO_ID||'; ', pk_lookup, :old.HIST_COMBO_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CR_COMBOS', 'PULL_TYPE_ID',
        ''|| 'CR_COMBO_ID=' || :old.CR_COMBO_ID||'; ', pk_lookup, :old.PULL_TYPE_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
END IF;
END;


/
ALTER TRIGGER "PWRPLANT"."PP_AU_REG_CR_COMBOS" ENABLE;