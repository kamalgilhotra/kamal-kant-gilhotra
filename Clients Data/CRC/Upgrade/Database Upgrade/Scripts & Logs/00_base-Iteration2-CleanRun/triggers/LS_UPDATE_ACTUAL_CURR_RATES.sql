
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."LS_UPDATE_ACTUAL_CURR_RATES" 
AFTER UPDATE ON currency_rate
FOR EACH ROW
  BEGIN
    IF :new.exchange_rate_type_id in (1,4) AND :old.exchange_rate_type_id in (1,4) THEN
      UPDATE currency_rate_default
      SET currency_from = :new.currency_from,
          currency_to = :new.currency_to,
          exchange_date = :new.exchange_date,
		  exchange_rate_type_id = :new.exchange_rate_type_id,
          rate = :new.rate
      WHERE currency_from = :old.currency_from
      AND currency_to = :old.currency_to
      AND exchange_date = :old.exchange_date
	  AND exchange_rate_type_id = :old.exchange_rate_type_id
      AND rate = :old.rate;
    END IF;
    
    IF :new.exchange_rate_type_id in (1,4) AND :old.exchange_rate_type_id not in (1,4) THEN
      INSERT INTO currency_rate_default (exchange_date, currency_from, currency_to, exchange_rate_type_id, rate)
      VALUES (:new.exchange_date, :new.currency_from, :new.currency_to, :new.exchange_rate_type_id, :new.rate);
    END IF;
    
    IF :new.exchange_rate_type_id not in (1,4) AND :old.exchange_rate_type_id in (1,4) THEN
      DELETE from currency_rate_default
      WHERE currency_from = :old.currency_from
      AND currency_to = :old.currency_to
      AND exchange_date = :old.exchange_date
      AND rate = :old.rate;
    END IF;
  END;
/
ALTER TRIGGER "PWRPLANT"."LS_UPDATE_ACTUAL_CURR_RATES" ENABLE;