
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."PEND_TRANS_SOB_CHECK_UPDATE" 
   after update on PEND_TRANSACTION_SET_OF_BOOKS
   for each row
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PEND_TRANS_SOB_CHECK_UPDATE
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/11/2013 Charlie Shilling Maint-29286
   ||============================================================================
   */

begin
   if :NEW.RESERVE <> :OLD.RESERVE then
      RAISE_APPLICATION_ERROR(-20000,
                              'Updating RESERVE on PEND_TRANSACTION_SET_OF_BOOKS is not permitted. Use ADJUSTED_RESERVE instead.');
   end if;
end;


/
ALTER TRIGGER "PWRPLANT"."PEND_TRANS_SOB_CHECK_UPDATE" ENABLE;