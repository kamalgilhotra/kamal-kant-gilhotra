
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."CURR_REFRESH_RATE_DFLT_DENSE" after insert or update or delete on currency
begin
  p_refresh_curr_rate_dflt_dense;
end;

/
ALTER TRIGGER "PWRPLANT"."CURR_REFRESH_RATE_DFLT_DENSE" ENABLE;