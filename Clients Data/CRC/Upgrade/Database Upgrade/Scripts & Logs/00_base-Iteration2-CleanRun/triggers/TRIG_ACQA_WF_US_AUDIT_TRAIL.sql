
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."TRIG_ACQA_WF_US_AUDIT_TRAIL" AFTER
 INSERT OR  DELETE  --OF company_id, major_location_id */
 ON approval_auth_level FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.approval_type_id <> :new.approval_type_id OR
      (:old.approval_type_id is null AND :new.approval_type_id is not null) OR
      (:new.approval_type_id is null AND :old.approval_type_id is not null) OR
      (:old.auth_level <> :new.auth_level ) OR
      (:old.auth_level is null AND :new.auth_level is not null) OR
      (:new.auth_level is null AND :old.auth_level is not null)    OR
      (:old.users <> :new.users ) OR
      (:old.users is null AND :new.users is not null) OR
      (:new.users is null AND :old.users is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail_stg    stg
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (1, batch_id, 'approval_auth_level', 'approval_type_id,auth_level,users', 'insert', :old.approval_type_id||','||:old.auth_level||','||:old.users,:new.approval_type_id||','||:new.auth_level||','||:new.users, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
 --$$$AKM
  IF DELETING THEN
      IF :old.approval_type_id <> :new.approval_type_id OR
      (:old.approval_type_id is null AND :new.approval_type_id is not null) OR
      (:new.approval_type_id is null AND :old.approval_type_id is not null) OR
      (:old.auth_level <> :new.auth_level ) OR
      (:old.auth_level is null AND :new.auth_level is not null) OR
      (:new.auth_level is null AND :old.auth_level is not null) OR
      (:old.users <> :new.users ) OR
      (:old.users is null AND :new.users is not null) OR
      (:new.users is null AND :old.users is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail_stg   stg
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (1, batch_id, 'approval_auth_level', 'approval_type_id,auth_level,users', 'delete', :old.approval_type_id||','||:old.auth_level||','||:old.users,:new.approval_type_id||','||:new.auth_level||','||:new.users, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;

--$$$AKM
END IF;
END;

/
ALTER TRIGGER "PWRPLANT"."TRIG_ACQA_WF_US_AUDIT_TRAIL" ENABLE;