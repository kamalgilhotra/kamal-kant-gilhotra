
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."WORKFLOW_DETAIL_APPROVALS" 
   after update of APPROVAL_STATUS_ID on WORKFLOW_DETAIL
   for each row

declare
   PROG        varchar2(60);
   OSUSER      varchar2(60);
   MACHINE     varchar2(60);
   TERMINAL    varchar2(60);
   OLD_LOOKUP  varchar2(250);
   NEW_LOOKUP  varchar2(250);
   PK_LOOKUP   varchar2(1500);
   WINDOW      varchar2(60);
   WINDOWTITLE varchar2(250);
   COMMENTS    varchar2(250);
   TRANS       varchar2(35);

begin
   if NVL(SYS_CONTEXT('powerplant_ctx', 'audit'), 'yes') = 'no' then
      return;
   end if;
   WINDOW      := NVL(SYS_CONTEXT('powerplant_ctx', 'window'), 'unknown window');
   WINDOWTITLE := NVL(SYS_CONTEXT('powerplant_ctx', 'windowtitle'), '');
   TRANS       := NVL(SYS_CONTEXT('powerplant_ctx', 'process'), '');
   COMMENTS    := NVL(SYS_CONTEXT('powerplant_ctx', 'comments'), '');
   PROG        := NVL(SYS_CONTEXT('powerplant_ctx', 'program'), '');
   OSUSER      := NVL(SYS_CONTEXT('powerplant_ctx', 'osuser'), '');
   MACHINE     := NVL(SYS_CONTEXT('powerplant_ctx', 'machine'), '');
   TERMINAL    := NVL(SYS_CONTEXT('powerplant_ctx', 'terminal'), '');
   COMMENTS    := COMMENTS || '; OSUSER=' || trim(OSUSER) || '; MACHINE=' || trim(MACHINE) ||
                  '; TERMINAL=' || trim(TERMINAL);
   if trim(PROG) is null then
      select PROGRAM into PROG from V$SESSION where AUDSID = USERENV('sessionid');
   end if;

   if UPDATING then
      if :OLD.APPROVAL_STATUS_ID <> :NEW.APPROVAL_STATUS_ID or
         (:OLD.APPROVAL_STATUS_ID is null and :NEW.APPROVAL_STATUS_ID is not null) or
         (:NEW.APPROVAL_STATUS_ID is null and :OLD.APPROVAL_STATUS_ID is not null) then

         insert into PP_AUDITS_PROJECT_TRAIL
            (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE,
             OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, PROGRAM, ACTION,
             MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
         values
            (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'WORKFLOW_DETAIL', 'APPROVAL_STATUS_ID',
             '' || 'WORKFLOW_ID=' || :NEW.WORKFLOW_ID || '; ' || 'ID=' || :NEW.ID || '; ', PK_LOOKUP,
             :OLD.APPROVAL_STATUS_ID, :NEW.APPROVAL_STATUS_ID, OLD_LOOKUP, NEW_LOOKUP, user, sysdate,
             PROG, 'U', TO_CHAR(sysdate, 'yyyymm'), COMMENTS, WINDOW, WINDOWTITLE, TRANS);
      end if;
   end if;
end;


/
ALTER TRIGGER "PWRPLANT"."WORKFLOW_DETAIL_APPROVALS" ENABLE;