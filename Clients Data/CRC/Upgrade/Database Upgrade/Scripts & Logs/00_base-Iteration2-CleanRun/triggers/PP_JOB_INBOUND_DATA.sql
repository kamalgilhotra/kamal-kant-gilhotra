
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."PP_JOB_INBOUND_DATA" 
								BEFORE INSERT ON PP_JOB_INBOUND_DATA
								FOR EACH ROW
								BEGIN
									:new.JOB_INPUT_DATA_ID := PP_JOB_INBOUND_DATA_SEQ.nextval;
								END;

/
ALTER TRIGGER "PWRPLANT"."PP_JOB_INBOUND_DATA" ENABLE;