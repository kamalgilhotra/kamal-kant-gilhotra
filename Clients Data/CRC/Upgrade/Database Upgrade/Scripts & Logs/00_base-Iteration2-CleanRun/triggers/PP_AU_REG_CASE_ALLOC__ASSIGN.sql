
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."PP_AU_REG_CASE_ALLOC__ASSIGN" AFTER
 UPDATE  OR  DELETE  OF CASE_YEAR, DIRECT_ASSIGN_AMOUNT, REG_ALLOC_ACCT_ID, REG_ALLOC_CATEGORY_ID, REG_ALLOC_TARGET_ID, REG_CASE_ID
 ON reg_case_alloc_direct_assign FOR EACH ROW
DECLARE
  prog varchar2(60);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF UPDATING THEN
   IF :old.CASE_YEAR <> :new.CASE_YEAR OR
     (:old.CASE_YEAR is null AND :new.CASE_YEAR is not null) OR
     (:new.CASE_YEAR is null AND :old.CASE_YEAR is not null) THEN


      INSERT INTO PP_AUDITS_PROJECT_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CASE_ALLOC_DIRECT_ASSIGN', 'CASE_YEAR',
        ''|| 'REG_CASE_ID=' || :new.REG_CASE_ID||'; '|| 'REG_ALLOC_ACCT_ID=' || :new.REG_ALLOC_ACCT_ID||'; '|| 'REG_ALLOC_CATEGORY_ID=' || :new.REG_ALLOC_CATEGORY_ID||'; '|| 'REG_ALLOC_TARGET_ID=' || :new.REG_ALLOC_TARGET_ID||'; '|| 'CASE_YEAR=' || :new.CASE_YEAR||'; ', pk_lookup, :old.CASE_YEAR, :new.CASE_YEAR,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.DIRECT_ASSIGN_AMOUNT <> :new.DIRECT_ASSIGN_AMOUNT OR
     (:old.DIRECT_ASSIGN_AMOUNT is null AND :new.DIRECT_ASSIGN_AMOUNT is not null) OR
     (:new.DIRECT_ASSIGN_AMOUNT is null AND :old.DIRECT_ASSIGN_AMOUNT is not null) THEN


      INSERT INTO PP_AUDITS_PROJECT_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CASE_ALLOC_DIRECT_ASSIGN', 'DIRECT_ASSIGN_AMOUNT',
        ''|| 'REG_CASE_ID=' || :new.REG_CASE_ID||'; '|| 'REG_ALLOC_ACCT_ID=' || :new.REG_ALLOC_ACCT_ID||'; '|| 'REG_ALLOC_CATEGORY_ID=' || :new.REG_ALLOC_CATEGORY_ID||'; '|| 'REG_ALLOC_TARGET_ID=' || :new.REG_ALLOC_TARGET_ID||'; '|| 'CASE_YEAR=' || :new.CASE_YEAR||'; ', pk_lookup, :old.DIRECT_ASSIGN_AMOUNT, :new.DIRECT_ASSIGN_AMOUNT,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.REG_ALLOC_ACCT_ID <> :new.REG_ALLOC_ACCT_ID OR
     (:old.REG_ALLOC_ACCT_ID is null AND :new.REG_ALLOC_ACCT_ID is not null) OR
     (:new.REG_ALLOC_ACCT_ID is null AND :old.REG_ALLOC_ACCT_ID is not null) THEN


      INSERT INTO PP_AUDITS_PROJECT_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CASE_ALLOC_DIRECT_ASSIGN', 'REG_ALLOC_ACCT_ID',
        ''|| 'REG_CASE_ID=' || :new.REG_CASE_ID||'; '|| 'REG_ALLOC_ACCT_ID=' || :new.REG_ALLOC_ACCT_ID||'; '|| 'REG_ALLOC_CATEGORY_ID=' || :new.REG_ALLOC_CATEGORY_ID||'; '|| 'REG_ALLOC_TARGET_ID=' || :new.REG_ALLOC_TARGET_ID||'; '|| 'CASE_YEAR=' || :new.CASE_YEAR||'; ', pk_lookup, :old.REG_ALLOC_ACCT_ID, :new.REG_ALLOC_ACCT_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.REG_ALLOC_CATEGORY_ID <> :new.REG_ALLOC_CATEGORY_ID OR
     (:old.REG_ALLOC_CATEGORY_ID is null AND :new.REG_ALLOC_CATEGORY_ID is not null) OR
     (:new.REG_ALLOC_CATEGORY_ID is null AND :old.REG_ALLOC_CATEGORY_ID is not null) THEN


      INSERT INTO PP_AUDITS_PROJECT_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CASE_ALLOC_DIRECT_ASSIGN', 'REG_ALLOC_CATEGORY_ID',
        ''|| 'REG_CASE_ID=' || :new.REG_CASE_ID||'; '|| 'REG_ALLOC_ACCT_ID=' || :new.REG_ALLOC_ACCT_ID||'; '|| 'REG_ALLOC_CATEGORY_ID=' || :new.REG_ALLOC_CATEGORY_ID||'; '|| 'REG_ALLOC_TARGET_ID=' || :new.REG_ALLOC_TARGET_ID||'; '|| 'CASE_YEAR=' || :new.CASE_YEAR||'; ', pk_lookup, :old.REG_ALLOC_CATEGORY_ID, :new.REG_ALLOC_CATEGORY_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.REG_ALLOC_TARGET_ID <> :new.REG_ALLOC_TARGET_ID OR
     (:old.REG_ALLOC_TARGET_ID is null AND :new.REG_ALLOC_TARGET_ID is not null) OR
     (:new.REG_ALLOC_TARGET_ID is null AND :old.REG_ALLOC_TARGET_ID is not null) THEN


      INSERT INTO PP_AUDITS_PROJECT_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CASE_ALLOC_DIRECT_ASSIGN', 'REG_ALLOC_TARGET_ID',
        ''|| 'REG_CASE_ID=' || :new.REG_CASE_ID||'; '|| 'REG_ALLOC_ACCT_ID=' || :new.REG_ALLOC_ACCT_ID||'; '|| 'REG_ALLOC_CATEGORY_ID=' || :new.REG_ALLOC_CATEGORY_ID||'; '|| 'REG_ALLOC_TARGET_ID=' || :new.REG_ALLOC_TARGET_ID||'; '|| 'CASE_YEAR=' || :new.CASE_YEAR||'; ', pk_lookup, :old.REG_ALLOC_TARGET_ID, :new.REG_ALLOC_TARGET_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.REG_CASE_ID <> :new.REG_CASE_ID OR
     (:old.REG_CASE_ID is null AND :new.REG_CASE_ID is not null) OR
     (:new.REG_CASE_ID is null AND :old.REG_CASE_ID is not null) THEN


      INSERT INTO PP_AUDITS_PROJECT_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CASE_ALLOC_DIRECT_ASSIGN', 'REG_CASE_ID',
        ''|| 'REG_CASE_ID=' || :new.REG_CASE_ID||'; '|| 'REG_ALLOC_ACCT_ID=' || :new.REG_ALLOC_ACCT_ID||'; '|| 'REG_ALLOC_CATEGORY_ID=' || :new.REG_ALLOC_CATEGORY_ID||'; '|| 'REG_ALLOC_TARGET_ID=' || :new.REG_ALLOC_TARGET_ID||'; '|| 'CASE_YEAR=' || :new.CASE_YEAR||'; ', pk_lookup, :old.REG_CASE_ID, :new.REG_CASE_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
END IF;
IF DELETING THEN
      INSERT INTO PP_AUDITS_PROJECT_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CASE_ALLOC_DIRECT_ASSIGN', 'CASE_YEAR',
        ''|| 'REG_CASE_ID=' || :old.REG_CASE_ID||'; '|| 'REG_ALLOC_ACCT_ID=' || :old.REG_ALLOC_ACCT_ID||'; '|| 'REG_ALLOC_CATEGORY_ID=' || :old.REG_ALLOC_CATEGORY_ID||'; '|| 'REG_ALLOC_TARGET_ID=' || :old.REG_ALLOC_TARGET_ID||'; '|| 'CASE_YEAR=' || :old.CASE_YEAR||'; ', pk_lookup, :old.CASE_YEAR,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_PROJECT_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CASE_ALLOC_DIRECT_ASSIGN', 'DIRECT_ASSIGN_AMOUNT',
        ''|| 'REG_CASE_ID=' || :old.REG_CASE_ID||'; '|| 'REG_ALLOC_ACCT_ID=' || :old.REG_ALLOC_ACCT_ID||'; '|| 'REG_ALLOC_CATEGORY_ID=' || :old.REG_ALLOC_CATEGORY_ID||'; '|| 'REG_ALLOC_TARGET_ID=' || :old.REG_ALLOC_TARGET_ID||'; '|| 'CASE_YEAR=' || :old.CASE_YEAR||'; ', pk_lookup, :old.DIRECT_ASSIGN_AMOUNT,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_PROJECT_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CASE_ALLOC_DIRECT_ASSIGN', 'REG_ALLOC_ACCT_ID',
        ''|| 'REG_CASE_ID=' || :old.REG_CASE_ID||'; '|| 'REG_ALLOC_ACCT_ID=' || :old.REG_ALLOC_ACCT_ID||'; '|| 'REG_ALLOC_CATEGORY_ID=' || :old.REG_ALLOC_CATEGORY_ID||'; '|| 'REG_ALLOC_TARGET_ID=' || :old.REG_ALLOC_TARGET_ID||'; '|| 'CASE_YEAR=' || :old.CASE_YEAR||'; ', pk_lookup, :old.REG_ALLOC_ACCT_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_PROJECT_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CASE_ALLOC_DIRECT_ASSIGN', 'REG_ALLOC_CATEGORY_ID',
        ''|| 'REG_CASE_ID=' || :old.REG_CASE_ID||'; '|| 'REG_ALLOC_ACCT_ID=' || :old.REG_ALLOC_ACCT_ID||'; '|| 'REG_ALLOC_CATEGORY_ID=' || :old.REG_ALLOC_CATEGORY_ID||'; '|| 'REG_ALLOC_TARGET_ID=' || :old.REG_ALLOC_TARGET_ID||'; '|| 'CASE_YEAR=' || :old.CASE_YEAR||'; ', pk_lookup, :old.REG_ALLOC_CATEGORY_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_PROJECT_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CASE_ALLOC_DIRECT_ASSIGN', 'REG_ALLOC_TARGET_ID',
        ''|| 'REG_CASE_ID=' || :old.REG_CASE_ID||'; '|| 'REG_ALLOC_ACCT_ID=' || :old.REG_ALLOC_ACCT_ID||'; '|| 'REG_ALLOC_CATEGORY_ID=' || :old.REG_ALLOC_CATEGORY_ID||'; '|| 'REG_ALLOC_TARGET_ID=' || :old.REG_ALLOC_TARGET_ID||'; '|| 'CASE_YEAR=' || :old.CASE_YEAR||'; ', pk_lookup, :old.REG_ALLOC_TARGET_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_PROJECT_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_CASE_ALLOC_DIRECT_ASSIGN', 'REG_CASE_ID',
        ''|| 'REG_CASE_ID=' || :old.REG_CASE_ID||'; '|| 'REG_ALLOC_ACCT_ID=' || :old.REG_ALLOC_ACCT_ID||'; '|| 'REG_ALLOC_CATEGORY_ID=' || :old.REG_ALLOC_CATEGORY_ID||'; '|| 'REG_ALLOC_TARGET_ID=' || :old.REG_ALLOC_TARGET_ID||'; '|| 'CASE_YEAR=' || :old.CASE_YEAR||'; ', pk_lookup, :old.REG_CASE_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
END IF;
END;


/
ALTER TRIGGER "PWRPLANT"."PP_AU_REG_CASE_ALLOC__ASSIGN" ENABLE;