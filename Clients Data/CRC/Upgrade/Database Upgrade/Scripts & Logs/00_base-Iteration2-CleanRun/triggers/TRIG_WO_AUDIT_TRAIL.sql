
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."TRIG_WO_AUDIT_TRAIL" AFTER
 INSERT  OR  UPDATE  OR  DELETE  OF COMPLETION_DATE, IN_SERVICE_DATE, OUT_OF_SERVICE_DATE, WO_STATUS_ID, EST_IN_SERVICE_DATE, EST_COMPLETE_DATE
 ON WORK_ORDER_CONTROL FOR EACH ROW
DECLARE
  prog varchar2(60);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF UPDATING OR INSERTING THEN
   IF :old.COMPLETION_DATE <> :new.COMPLETION_DATE OR
     (:old.COMPLETION_DATE is null AND :new.COMPLETION_DATE is not null) OR
     (:new.COMPLETION_DATE is null AND :old.COMPLETION_DATE is not null) THEN


      INSERT INTO WO_STATUS_TRAIL
        (SEQUENCE_KEY, WORK_ORDER_ID, WO_STATUS_ID, IN_SERVICE_DATE, OUT_OF_SERVICE_DATE, COMPLETION_DATE, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, PROGRAM, PROCESS, ACTIVE_WINDOW, WINDOW_TITLE, EST_IN_SERVICE_DATE, EST_COMPLETE_DATE)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, :new.WORK_ORDER_ID, null, null, null, :new.COMPLETION_DATE, USER, SYSDATE, prog, trans, window, windowtitle, NULL, NULL);
   END IF;
   IF :old.IN_SERVICE_DATE <> :new.IN_SERVICE_DATE OR
     (:old.IN_SERVICE_DATE is null AND :new.IN_SERVICE_DATE is not null) OR
     (:new.IN_SERVICE_DATE is null AND :old.IN_SERVICE_DATE is not null) THEN


      INSERT INTO WO_STATUS_TRAIL
        (SEQUENCE_KEY, WORK_ORDER_ID, WO_STATUS_ID, IN_SERVICE_DATE, OUT_OF_SERVICE_DATE, COMPLETION_DATE, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, PROGRAM, PROCESS, ACTIVE_WINDOW, WINDOW_TITLE, EST_IN_SERVICE_DATE, EST_COMPLETE_DATE)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, :new.WORK_ORDER_ID, null, :new.IN_SERVICE_DATE, null, null, USER, SYSDATE, prog, trans, window, windowtitle, NULL, NULL);

   END IF;
   IF :old.OUT_OF_SERVICE_DATE <> :new.OUT_OF_SERVICE_DATE OR
     (:old.OUT_OF_SERVICE_DATE is null AND :new.OUT_OF_SERVICE_DATE is not null) OR
     (:new.OUT_OF_SERVICE_DATE is null AND :old.OUT_OF_SERVICE_DATE is not null) THEN


      INSERT INTO WO_STATUS_TRAIL
        (SEQUENCE_KEY, WORK_ORDER_ID, WO_STATUS_ID, IN_SERVICE_DATE, OUT_OF_SERVICE_DATE, COMPLETION_DATE, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, PROGRAM, PROCESS, ACTIVE_WINDOW, WINDOW_TITLE, EST_IN_SERVICE_DATE, EST_COMPLETE_DATE)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, :new.WORK_ORDER_ID, null, null, :new.OUT_OF_SERVICE_DATE, null, USER, SYSDATE, prog, trans, window, windowtitle, NULL, NULL);
   END IF;
   IF :old.WO_STATUS_ID <> :new.WO_STATUS_ID OR
     (:old.WO_STATUS_ID is null AND :new.WO_STATUS_ID is not null) OR
     (:new.WO_STATUS_ID is null AND :old.WO_STATUS_ID is not null) THEN


      INSERT INTO WO_STATUS_TRAIL
        (SEQUENCE_KEY, WORK_ORDER_ID, WO_STATUS_ID, IN_SERVICE_DATE, OUT_OF_SERVICE_DATE, COMPLETION_DATE, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, PROGRAM, PROCESS, ACTIVE_WINDOW, WINDOW_TITLE, EST_IN_SERVICE_DATE, EST_COMPLETE_DATE)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, :new.WORK_ORDER_ID, :new.WO_STATUS_ID, null, null, null, USER, SYSDATE, prog, trans, window, windowtitle, NULL, NULL);
   END IF;
   IF :old.EST_IN_SERVICE_DATE <> :new.EST_IN_SERVICE_DATE OR
     (:old.EST_IN_SERVICE_DATE is null AND :new.EST_IN_SERVICE_DATE is not null) OR
     (:new.EST_IN_SERVICE_DATE is null AND :old.EST_IN_SERVICE_DATE is not null) THEN


      INSERT INTO WO_STATUS_TRAIL
        (SEQUENCE_KEY, WORK_ORDER_ID, WO_STATUS_ID, IN_SERVICE_DATE, OUT_OF_SERVICE_DATE, COMPLETION_DATE, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, PROGRAM, PROCESS, ACTIVE_WINDOW, WINDOW_TITLE, EST_IN_SERVICE_DATE, EST_COMPLETE_DATE)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, :new.WORK_ORDER_ID, null, null, null, null, USER, SYSDATE, prog, trans, window, windowtitle, :new.EST_IN_SERVICE_DATE, NULL);
   END IF;
   IF :old.EST_COMPLETE_DATE <> :new.EST_COMPLETE_DATE OR
     (:old.EST_COMPLETE_DATE is null AND :new.EST_COMPLETE_DATE is not null) OR
     (:new.EST_COMPLETE_DATE is null AND :old.EST_COMPLETE_DATE is not null) THEN


      INSERT INTO WO_STATUS_TRAIL
        (SEQUENCE_KEY, WORK_ORDER_ID, WO_STATUS_ID, IN_SERVICE_DATE, OUT_OF_SERVICE_DATE, COMPLETION_DATE, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, PROGRAM, PROCESS, ACTIVE_WINDOW, WINDOW_TITLE, EST_IN_SERVICE_DATE, EST_COMPLETE_DATE)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, :new.WORK_ORDER_ID, null, null, null, null, USER, SYSDATE, prog, trans, window, windowtitle, NULL, :new.EST_COMPLETE_DATE);
   END IF;
END IF;

END;

/
ALTER TRIGGER "PWRPLANT"."TRIG_WO_AUDIT_TRAIL" ENABLE;