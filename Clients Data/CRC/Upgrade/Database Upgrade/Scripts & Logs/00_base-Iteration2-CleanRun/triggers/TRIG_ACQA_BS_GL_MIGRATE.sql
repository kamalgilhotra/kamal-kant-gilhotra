
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."TRIG_ACQA_BS_GL_MIGRATE" AFTER
 INSERT OR  DELETE
 ON gl_acct_bus_segment FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  prog varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
		IF INSERTING THEN
			SELECT batch_id
			INTO batch_id
			FROM acqa_current_user_batch
			WHERE user_id = USER
			AND session_id = USERENV('sessionid');


INSERT INTO acqa_migrate_stg (id,batch_id,batch_run_order,table_name,column_name,action,old_value,new_value,sql,include_flag,user_id,time_stamp,ACTIVE_WINDOW,WINDOW_TITLE,comments)
VALUES (pp_acqa_migrate_stg_seq.NEXTVAL,batch_id,(SELECT nvl(max(batch_run_order) + 1, 1)FROM acqa_migrate_stg WHERE batch_id = batch_id)
	,'GL_ACCT_BUS_SEGMENT'
	,'BUS_SEGMENT_ID'
	,'insert'
	,:old.BUS_SEGMENT_ID
	,:new.BUS_SEGMENT_ID
	,(
		SELECT 'insert into GL_ACCT_BUS_SEGMENT (BUS_SEGMENT_ID, GL_ACCOUNT_ID) values (' || '(select BUS_SEGMENT_ID from BUSINESS_SEGMENT where description =' || (
				SELECT '''' || description || ''''
				FROM BUSINESS_SEGMENT
				WHERE BUS_SEGMENT_ID = (:new.BUS_SEGMENT_ID)
				) || ')' || ',' || '(select GL_ACCOUNT_ID from GL_ACCOUNT where description =' || (
				SELECT '''' || description || ''''
				FROM GL_ACCOUNT
				WHERE GL_ACCOUNT_ID = (:new.GL_ACCOUNT_ID)
				) || '));'
		FROM dual
		),1,USER,SYSDATE,window,windowtitle,comments);
    END IF ;

IF DELETING THEN

		SELECT batch_id
		INTO batch_id
		FROM acqa_current_user_batch
		WHERE user_id = USER
		AND session_id = USERENV('sessionid');


INSERT INTO acqa_migrate_stg (id,batch_id,batch_run_order,table_name,column_name,action,old_value,new_value,sql,include_flag,user_id,time_stamp,ACTIVE_WINDOW,WINDOW_TITLE,comments)
VALUES (pp_acqa_migrate_stg_seq.NEXTVAL,batch_id,(SELECT nvl(max(batch_run_order) + 1, 1)FROM acqa_migrate_stg WHERE batch_id = batch_id)
	,'GL_ACCT_BUS_SEGMENT'
	,'BUS_SEGMENT_ID'
	,'delete'
	,:old.BUS_SEGMENT_ID
	,:new.BUS_SEGMENT_ID
	,(
			SELECT 'delete from GL_ACCT_BUS_SEGMENT where BUS_SEGMENT_ID = '|| '(select BUS_SEGMENT_ID from BUSINESS_SEGMENT where description =' || (
				SELECT '''' || description || ''''
				FROM BUSINESS_SEGMENT
				WHERE BUS_SEGMENT_ID = (:old.BUS_SEGMENT_ID)
				) || ')' || ' and GL_ACCOUNT_ID = (select GL_ACCOUNT_ID from GL_ACCOUNT where description =' || (
				SELECT '''' || description || ''''
				FROM GL_ACCOUNT
				WHERE GL_ACCOUNT_ID = (:old.GL_ACCOUNT_ID)
				) || ');'
		FROM dual
		),1,USER,SYSDATE,window,windowtitle,comments);
    END IF ;
    END IF;
END;

/
ALTER TRIGGER "PWRPLANT"."TRIG_ACQA_BS_GL_MIGRATE" ENABLE;