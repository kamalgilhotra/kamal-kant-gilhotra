
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."WO_EST_USER_UPDATE" 
   before update or insert of ESTIMATE_ID, REVISION, WORK_ORDER_ID, EXPENDITURE_TYPE_ID, UTILITY_ACCOUNT_ID, EST_CHG_TYPE_ID, SUB_ACCOUNT_ID, RETIREMENT_UNIT_ID,
                              BUS_SEGMENT_ID, ASSET_ID, QUANTITY, AMOUNT, PROPERTY_GROUP_ID, ASSET_LOCATION_ID on PWRPLANT.WO_ESTIMATE
   for each row
begin
   :NEW.UNIT_LOAD_USER_ID    := user;
   :NEW.UNIT_LOAD_TIME_STAMP := sysdate;
end;




/
ALTER TRIGGER "PWRPLANT"."WO_EST_USER_UPDATE" ENABLE;