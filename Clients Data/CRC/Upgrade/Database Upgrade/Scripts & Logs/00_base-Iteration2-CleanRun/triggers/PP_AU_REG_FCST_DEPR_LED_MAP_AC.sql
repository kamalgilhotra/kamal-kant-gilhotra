
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."PP_AU_REG_FCST_DEPR_LED_MAP_AC" AFTER
 INSERT  OR  UPDATE  OR  DELETE  OF COLUMN_NAME, FORECAST_VERSION_ID, REG_ACTIVITY_ID, REG_COMPONENT_ID
 ON reg_fcst_depr_ledger_map_act FOR EACH ROW
DECLARE
  prog varchar2(60);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF UPDATING THEN
   IF :old.COLUMN_NAME <> :new.COLUMN_NAME OR
     (:old.COLUMN_NAME is null AND :new.COLUMN_NAME is not null) OR
     (:new.COLUMN_NAME is null AND :old.COLUMN_NAME is not null) THEN


      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_LEDGER_MAP_ACT', 'COLUMN_NAME',
        ''|| 'COLUMN_NAME=' || :new.COLUMN_NAME||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; '|| 'REG_COMPONENT_ID=' || :new.REG_COMPONENT_ID||'; ', pk_lookup, :old.COLUMN_NAME, :new.COLUMN_NAME,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.FORECAST_VERSION_ID <> :new.FORECAST_VERSION_ID OR
     (:old.FORECAST_VERSION_ID is null AND :new.FORECAST_VERSION_ID is not null) OR
     (:new.FORECAST_VERSION_ID is null AND :old.FORECAST_VERSION_ID is not null) THEN


      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_LEDGER_MAP_ACT', 'FORECAST_VERSION_ID',
        ''|| 'COLUMN_NAME=' || :new.COLUMN_NAME||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; '|| 'REG_COMPONENT_ID=' || :new.REG_COMPONENT_ID||'; ', pk_lookup, :old.FORECAST_VERSION_ID, :new.FORECAST_VERSION_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.REG_ACTIVITY_ID <> :new.REG_ACTIVITY_ID OR
     (:old.REG_ACTIVITY_ID is null AND :new.REG_ACTIVITY_ID is not null) OR
     (:new.REG_ACTIVITY_ID is null AND :old.REG_ACTIVITY_ID is not null) THEN


      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_LEDGER_MAP_ACT', 'REG_ACTIVITY_ID',
        ''|| 'COLUMN_NAME=' || :new.COLUMN_NAME||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; '|| 'REG_COMPONENT_ID=' || :new.REG_COMPONENT_ID||'; ', pk_lookup, :old.REG_ACTIVITY_ID, :new.REG_ACTIVITY_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.REG_COMPONENT_ID <> :new.REG_COMPONENT_ID OR
     (:old.REG_COMPONENT_ID is null AND :new.REG_COMPONENT_ID is not null) OR
     (:new.REG_COMPONENT_ID is null AND :old.REG_COMPONENT_ID is not null) THEN


      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_LEDGER_MAP_ACT', 'REG_COMPONENT_ID',
        ''|| 'COLUMN_NAME=' || :new.COLUMN_NAME||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; '|| 'REG_COMPONENT_ID=' || :new.REG_COMPONENT_ID||'; ', pk_lookup, :old.REG_COMPONENT_ID, :new.REG_COMPONENT_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
END IF;
IF INSERTING THEN
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_LEDGER_MAP_ACT', 'COLUMN_NAME',
        ''|| 'COLUMN_NAME=' || :new.COLUMN_NAME||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; '|| 'REG_COMPONENT_ID=' || :new.REG_COMPONENT_ID||'; ', pk_lookup, :new.COLUMN_NAME,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_LEDGER_MAP_ACT', 'FORECAST_VERSION_ID',
        ''|| 'COLUMN_NAME=' || :new.COLUMN_NAME||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; '|| 'REG_COMPONENT_ID=' || :new.REG_COMPONENT_ID||'; ', pk_lookup, :new.FORECAST_VERSION_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_LEDGER_MAP_ACT', 'REG_ACTIVITY_ID',
        ''|| 'COLUMN_NAME=' || :new.COLUMN_NAME||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; '|| 'REG_COMPONENT_ID=' || :new.REG_COMPONENT_ID||'; ', pk_lookup, :new.REG_ACTIVITY_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_LEDGER_MAP_ACT', 'REG_COMPONENT_ID',
        ''|| 'COLUMN_NAME=' || :new.COLUMN_NAME||'; '|| 'FORECAST_VERSION_ID=' || :new.FORECAST_VERSION_ID||'; '|| 'REG_COMPONENT_ID=' || :new.REG_COMPONENT_ID||'; ', pk_lookup, :new.REG_COMPONENT_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
END IF;
IF DELETING THEN
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_LEDGER_MAP_ACT', 'COLUMN_NAME',
        ''|| 'COLUMN_NAME=' || :old.COLUMN_NAME||'; '|| 'FORECAST_VERSION_ID=' || :old.FORECAST_VERSION_ID||'; '|| 'REG_COMPONENT_ID=' || :old.REG_COMPONENT_ID||'; ', pk_lookup, :old.COLUMN_NAME,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_LEDGER_MAP_ACT', 'FORECAST_VERSION_ID',
        ''|| 'COLUMN_NAME=' || :old.COLUMN_NAME||'; '|| 'FORECAST_VERSION_ID=' || :old.FORECAST_VERSION_ID||'; '|| 'REG_COMPONENT_ID=' || :old.REG_COMPONENT_ID||'; ', pk_lookup, :old.FORECAST_VERSION_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_LEDGER_MAP_ACT', 'REG_ACTIVITY_ID',
        ''|| 'COLUMN_NAME=' || :old.COLUMN_NAME||'; '|| 'FORECAST_VERSION_ID=' || :old.FORECAST_VERSION_ID||'; '|| 'REG_COMPONENT_ID=' || :old.REG_COMPONENT_ID||'; ', pk_lookup, :old.REG_ACTIVITY_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_REG_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_FCST_DEPR_LEDGER_MAP_ACT', 'REG_COMPONENT_ID',
        ''|| 'COLUMN_NAME=' || :old.COLUMN_NAME||'; '|| 'FORECAST_VERSION_ID=' || :old.FORECAST_VERSION_ID||'; '|| 'REG_COMPONENT_ID=' || :old.REG_COMPONENT_ID||'; ', pk_lookup, :old.REG_COMPONENT_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
END IF;
END;


/
ALTER TRIGGER "PWRPLANT"."PP_AU_REG_FCST_DEPR_LED_MAP_AC" ENABLE;