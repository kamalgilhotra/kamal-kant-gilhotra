
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."INSERT_DISTINCT_ASSET_ILRS" 
after insert or update on lsr_asset
declare
begin
  insert into lsr_distinct_asset_ilr(asset_id, ilr_id)
  select distinct lsr_asset_id, ilr_id
  from lsr_asset a
  where not exists (select 1 from lsr_distinct_asset_ilr b where a.lsr_asset_id = b.asset_id and a.ilr_id = b.ilr_id);
end;
/
ALTER TRIGGER "PWRPLANT"."INSERT_DISTINCT_ASSET_ILRS" ENABLE;