
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."PP_WEB_LOCALIZATION_STR" before
                        UPDATE OR
                        INSERT ON PWRPLANT."PP_WEB_LOCALIZATION_STRING2" FOR EACH row BEGIN :new.user_id := USER;
                        :new.time_stamp                                                                  := SYSDATE;
                      END;


/
ALTER TRIGGER "PWRPLANT"."PP_WEB_LOCALIZATION_STR" ENABLE;