
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."PP_WEB_LOCALIZATION_STR_REV" BEFORE
                        DELETE OR
                        INSERT OR
                        UPDATE ON "PWRPLANT"."PP_WEB_LOCALIZATION_STRING2" FOR EACH ROW DECLARE BEGIN
                        UPDATE PP_WEB_LOCALIZATION_REVISION
                        SET REVISION_DATE         = SYSDATE
                        WHERE TRIM(LOWER(LOCALE)) = TRIM(LOWER(NVL(:new.locale, :old.locale)));
                        END;


/
ALTER TRIGGER "PWRPLANT"."PP_WEB_LOCALIZATION_STR_REV" ENABLE;