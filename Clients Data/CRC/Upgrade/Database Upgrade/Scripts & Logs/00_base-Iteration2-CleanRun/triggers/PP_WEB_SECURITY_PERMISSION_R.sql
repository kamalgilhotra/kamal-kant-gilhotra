
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."PP_WEB_SECURITY_PERMISSION_R" 
                          AFTER INSERT OR UPDATE OR DELETE ON PP_WEB_SECURITY_PERMISSION
                          FOR EACH ROW
                        BEGIN
                          IF NVL(:OLD.PRINCIPAL_TYPE, :NEW.PRINCIPAL_TYPE) = 'U' THEN
                            DELETE FROM PP_WEB_SECURITY_REVISION
                             WHERE "USERS" = NVL(:OLD."PRINCIPAL", :NEW."PRINCIPAL");

                            INSERT INTO PP_WEB_SECURITY_REVISION ("USERS", "REVISION_DATE")
                              VALUES(NVL(:OLD."PRINCIPAL", :NEW."PRINCIPAL"), SYSDATE);

                         ELSIF NVL(:OLD.PRINCIPAL_TYPE, :NEW.PRINCIPAL_TYPE) = 'G' THEN
                           FOR rec IN (SELECT "USERS" FROM PP_SECURITY_USERS_GROUPS WHERE "GROUPS" = NVL(:OLD."PRINCIPAL", :NEW."PRINCIPAL"))
                            LOOP
                              DELETE FROM PP_WEB_SECURITY_REVISION
                               WHERE "USERS" =  rec."USERS";

                              INSERT INTO PP_WEB_SECURITY_REVISION ("USERS", "REVISION_DATE")
                                VALUES(rec."USERS", SYSDATE);
                              END LOOP;
                          END IF;
                        END;


/
ALTER TRIGGER "PWRPLANT"."PP_WEB_SECURITY_PERMISSION_R" ENABLE;