
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."LS_NEW_ACTUAL_CURRENCY_RATES" 
AFTER INSERT ON currency_rate
FOR EACH ROW
  BEGIN
    IF :new.exchange_rate_type_id in (1,4) THEN
      INSERT INTO currency_rate_default (exchange_date, currency_from, currency_to, exchange_rate_type_id, rate)
      VALUES (:new.exchange_date, :new.currency_from, :new.currency_to, :new.exchange_rate_type_id, :new.rate);
    END IF;
  END;
/
ALTER TRIGGER "PWRPLANT"."LS_NEW_ACTUAL_CURRENCY_RATES" ENABLE;