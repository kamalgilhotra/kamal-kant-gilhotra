/*
||============================================================================
|| Application: PowerPlant
|| File Name:   PP_AU_REPAIR_SCHEMA.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.0   02/21/2013 Blake Andrews   maint_011206_PP_AU_REPAIR_SCHEMA.sql
||============================================================================
*/

CREATE OR REPLACE TRIGGER
PP_AU_REPAIR_SCHEMA AFTER
 INSERT  OR  UPDATE  OR  DELETE  OF DESCRIPTION, LONG_DESCRIPTION, REPAIR_SCHEMA_ID, STATUS, USAGE_FLAG
 ON REPAIR_SCHEMA FOR EACH ROW
DECLARE
  prog varchar2(60);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF UPDATING THEN
   IF :old.DESCRIPTION <> :new.DESCRIPTION OR
     (:old.DESCRIPTION is null AND :new.DESCRIPTION is not null) OR
     (:new.DESCRIPTION is null AND :old.DESCRIPTION is not null) THEN


      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_SCHEMA', 'DESCRIPTION',
        ''|| 'REPAIR_SCHEMA_ID=' || :new.REPAIR_SCHEMA_ID||'; ', pk_lookup, :old.DESCRIPTION, :new.DESCRIPTION,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.LONG_DESCRIPTION <> :new.LONG_DESCRIPTION OR
     (:old.LONG_DESCRIPTION is null AND :new.LONG_DESCRIPTION is not null) OR
     (:new.LONG_DESCRIPTION is null AND :old.LONG_DESCRIPTION is not null) THEN


      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_SCHEMA', 'LONG_DESCRIPTION',
        ''|| 'REPAIR_SCHEMA_ID=' || :new.REPAIR_SCHEMA_ID||'; ', pk_lookup, :old.LONG_DESCRIPTION, :new.LONG_DESCRIPTION,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.REPAIR_SCHEMA_ID <> :new.REPAIR_SCHEMA_ID OR
     (:old.REPAIR_SCHEMA_ID is null AND :new.REPAIR_SCHEMA_ID is not null) OR
     (:new.REPAIR_SCHEMA_ID is null AND :old.REPAIR_SCHEMA_ID is not null) THEN


      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_SCHEMA', 'REPAIR_SCHEMA_ID',
        ''|| 'REPAIR_SCHEMA_ID=' || :new.REPAIR_SCHEMA_ID||'; ', pk_lookup, :old.REPAIR_SCHEMA_ID, :new.REPAIR_SCHEMA_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.STATUS <> :new.STATUS OR
     (:old.STATUS is null AND :new.STATUS is not null) OR
     (:new.STATUS is null AND :old.STATUS is not null) THEN


      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_SCHEMA', 'STATUS',
        ''|| 'REPAIR_SCHEMA_ID=' || :new.REPAIR_SCHEMA_ID||'; ', pk_lookup, :old.STATUS, :new.STATUS,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.USAGE_FLAG <> :new.USAGE_FLAG OR
     (:old.USAGE_FLAG is null AND :new.USAGE_FLAG is not null) OR
     (:new.USAGE_FLAG is null AND :old.USAGE_FLAG is not null) THEN


      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_SCHEMA', 'USAGE_FLAG',
        ''|| 'REPAIR_SCHEMA_ID=' || :new.REPAIR_SCHEMA_ID||'; ', pk_lookup, :old.USAGE_FLAG, :new.USAGE_FLAG,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
END IF;
IF INSERTING THEN
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_SCHEMA', 'DESCRIPTION',
        ''|| 'REPAIR_SCHEMA_ID=' || :new.REPAIR_SCHEMA_ID||'; ', pk_lookup, :new.DESCRIPTION,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_SCHEMA', 'LONG_DESCRIPTION',
        ''|| 'REPAIR_SCHEMA_ID=' || :new.REPAIR_SCHEMA_ID||'; ', pk_lookup, :new.LONG_DESCRIPTION,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_SCHEMA', 'REPAIR_SCHEMA_ID',
        ''|| 'REPAIR_SCHEMA_ID=' || :new.REPAIR_SCHEMA_ID||'; ', pk_lookup, :new.REPAIR_SCHEMA_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_SCHEMA', 'STATUS',
        ''|| 'REPAIR_SCHEMA_ID=' || :new.REPAIR_SCHEMA_ID||'; ', pk_lookup, :new.STATUS,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_SCHEMA', 'USAGE_FLAG',
        ''|| 'REPAIR_SCHEMA_ID=' || :new.REPAIR_SCHEMA_ID||'; ', pk_lookup, :new.USAGE_FLAG,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
END IF;
IF DELETING THEN
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_SCHEMA', 'DESCRIPTION',
        ''|| 'REPAIR_SCHEMA_ID=' || :old.REPAIR_SCHEMA_ID||'; ', pk_lookup, :old.DESCRIPTION,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_SCHEMA', 'LONG_DESCRIPTION',
        ''|| 'REPAIR_SCHEMA_ID=' || :old.REPAIR_SCHEMA_ID||'; ', pk_lookup, :old.LONG_DESCRIPTION,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_SCHEMA', 'REPAIR_SCHEMA_ID',
        ''|| 'REPAIR_SCHEMA_ID=' || :old.REPAIR_SCHEMA_ID||'; ', pk_lookup, :old.REPAIR_SCHEMA_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_SCHEMA', 'STATUS',
        ''|| 'REPAIR_SCHEMA_ID=' || :old.REPAIR_SCHEMA_ID||'; ', pk_lookup, :old.STATUS,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_SCHEMA', 'USAGE_FLAG',
        ''|| 'REPAIR_SCHEMA_ID=' || :old.REPAIR_SCHEMA_ID||'; ', pk_lookup, :old.USAGE_FLAG,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
END IF;
END;
/

DELETE FROM PP_TABLE_AUDITS WHERE TABLE_NAME = 'REPAIR_SCHEMA';
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('REPAIR_SCHEMA', 'DESCRIPTION', 'PWRPLANT', to_date('2013-02-12 14:33:56', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_REPAIR_SCHEMA', 'IUD', null, 'TABLES', null, 0) ;
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('REPAIR_SCHEMA', 'LONG_DESCRIPTION', 'PWRPLANT', to_date('2013-02-12 14:33:56', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_REPAIR_SCHEMA', 'IUD', null, 'TABLES', null, 0) ;
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('REPAIR_SCHEMA', 'REPAIR_SCHEMA_ID', 'PWRPLANT', to_date('2013-02-12 14:33:56', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_REPAIR_SCHEMA', 'IUD', null, 'TABLES', null, 0) ;
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('REPAIR_SCHEMA', 'STATUS', 'PWRPLANT', to_date('2013-02-12 14:33:56', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_REPAIR_SCHEMA', 'IUD', null, 'TABLES', null, 0) ;
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('REPAIR_SCHEMA', 'USAGE_FLAG', 'PWRPLANT', to_date('2013-02-12 14:33:56', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_REPAIR_SCHEMA', 'IUD', null, 'TABLES', null, 0) ;
DELETE FROM PP_TABLE_AUDITS_OBJ_ACTIONS WHERE TRIGGER_NAME = 'PP_AU_REPAIR_SCHEMA';
DELETE FROM PP_TABLE_AUDITS_PK_LOOKUP WHERE TABLE_NAME = 'REPAIR_SCHEMA';
INSERT INTO "PP_TABLE_AUDITS_PK_LOOKUP" ("TABLE_NAME", "COLUMN_NAME", "DISPLAY_TABLE_NAME", "DISPLAY_COLUMN_NAME", "CODE_COLUMN_NAME", "TIME_STAMP", "USER_ID") VALUES ('REPAIR_SCHEMA', 'REPAIR_SCHEMA_ID', null, null, null, to_date('2013-02-12 14:33:56', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;
COMMIT;