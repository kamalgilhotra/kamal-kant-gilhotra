
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."PP_WEB_VIEW_SESSION_ID" 
                BEFORE INSERT ON PWRPLANT.pp_web_view_session
                FOR EACH ROW
                BEGIN
                  SELECT pwrplant.pp_web_view_session_seq.nextval into :new.session_id
                 FROM dual;
                END;


/
ALTER TRIGGER "PWRPLANT"."PP_WEB_VIEW_SESSION_ID" ENABLE;