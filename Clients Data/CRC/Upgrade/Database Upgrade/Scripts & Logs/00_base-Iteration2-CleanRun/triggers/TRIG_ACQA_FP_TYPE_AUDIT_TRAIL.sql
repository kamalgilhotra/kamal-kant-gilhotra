
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."TRIG_ACQA_FP_TYPE_AUDIT_TRAIL" AFTER
 INSERT OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON work_order_type FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
  IF :NEW.funding_wo=1 then
    IF :old.work_order_type_id <> :new.work_order_type_id OR
      (:old.work_order_type_id is null AND :new.work_order_type_id is not null) OR
      (:new.work_order_type_id is null AND :old.work_order_type_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'work_order_type_fp', 'work_order_type_id', 'insert', :old.work_order_type_id,:new.work_order_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
  END IF;


  IF DELETING THEN
  IF :old.funding_wo=1 then
    IF :old.work_order_type_id <> :new.work_order_type_id OR
      (:old.work_order_type_id is null AND :new.work_order_type_id is not null) OR
      (:new.work_order_type_id is null AND :old.work_order_type_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'work_order_type_fp', 'work_order_type_id', 'delete', :old.work_order_type_id,:new.work_order_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
  END IF;

END IF;
END;

/
ALTER TRIGGER "PWRPLANT"."TRIG_ACQA_FP_TYPE_AUDIT_TRAIL" ENABLE;