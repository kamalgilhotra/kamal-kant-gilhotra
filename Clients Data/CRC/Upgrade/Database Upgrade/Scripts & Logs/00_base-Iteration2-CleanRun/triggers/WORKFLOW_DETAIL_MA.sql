
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."WORKFLOW_DETAIL_MA" 
   before update of APPROVAL_STATUS_ID on WORKFLOW_DETAIL
   for each row

declare
   PROCESS  varchar2(60);
   COMMENTS varchar2(250);

begin
   PROCESS  := NVL(SYS_CONTEXT('powerplant_ctx', 'process'), '');
   COMMENTS := NVL(SYS_CONTEXT('powerplant_ctx', 'comments'), '');
   if :NEW.APPROVAL_STATUS_ID in (3, 4) and LOWER(trim(PROCESS)) = 'web' and
      (INSTR(UPPER(COMMENTS), UPPER('Mobile')) > 0 or INSTR(UPPER(COMMENTS), UPPER('iPhone')) > 0 or
      INSTR(UPPER(COMMENTS), UPPER('iPod')) > 0 or INSTR(UPPER(COMMENTS), UPPER('iPad')) > 0 or
      INSTR(UPPER(COMMENTS), UPPER('Android')) > 0 or
      INSTR(UPPER(COMMENTS), UPPER('BlackBerry')) > 0 or
      INSTR(UPPER(COMMENTS), UPPER('IEMobile')) > 0) then
      :NEW.MOBILE_APPROVAL := 1;
   elsif :NEW.APPROVAL_STATUS_ID in (3, 4) and LOWER(trim(PROCESS)) = 'web' then
      :NEW.MOBILE_APPROVAL := 2;
   elsif :NEW.APPROVAL_STATUS_ID in (3, 4) then
      :NEW.MOBILE_APPROVAL := 0;
   else
      :NEW.MOBILE_APPROVAL := null;
   end if;
end;


/
ALTER TRIGGER "PWRPLANT"."WORKFLOW_DETAIL_MA" ENABLE;