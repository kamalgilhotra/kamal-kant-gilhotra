
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."LS_REMOVE_DEFAULT_CURR_RATES" 
BEFORE DELETE ON currency
FOR EACH ROW
  BEGIN
    DELETE from currency_rate_default
    WHERE currency_from = :old.currency_id
    AND currency_to = :old.currency_id
    AND rate = 1;

    DELETE from currency_rate_default
    WHERE (currency_from = :old.currency_id
        or currency_to = :old.currency_id)
    AND rate = 0;
  END;
/
ALTER TRIGGER "PWRPLANT"."LS_REMOVE_DEFAULT_CURR_RATES" ENABLE;