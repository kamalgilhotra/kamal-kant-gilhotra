CREATE OR REPLACE TRIGGER 
PP_AU_REPAIR_THRESHOLDS AFTER 
 INSERT  OR  UPDATE  OR  DELETE  OF ADD_TEST, BASE_YEAR, COMPANY_ID, EFFECTIVE_DATE, REPAIR_LOCATION_ID, REPAIR_UNIT_CODE_ID, REPLACEMENT_COST
 ON REPAIR_THRESHOLDS FOR EACH ROW 
DECLARE  
  prog varchar2(60);  
  osuser varchar2(60);  
  machine varchar2(60);  
  terminal varchar2(60);  
  old_lookup varchar2(250);  
  new_lookup varchar2(250);  
  pk_lookup varchar2(1500);  
  pk_temp varchar2(250);  
  window varchar2(60);  
  windowtitle varchar2(250);  
  comments varchar2(250);  
  trans varchar2(35);  
  ret number(22,0);  
BEGIN  
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then  
   return;  
end if; 
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window'); 
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),''); 
  trans        := nvl(sys_context('powerplant_ctx','process'     ),''); 
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),''); 
  prog         := nvl(sys_context('powerplant_ctx','program'     ),''); 
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),''); 
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),''); 
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),''); 
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF; 
IF UPDATING THEN 
   IF :old.ADD_TEST <> :new.ADD_TEST OR 
     (:old.ADD_TEST is null AND :new.ADD_TEST is not null) OR 
     (:new.ADD_TEST is null AND :old.ADD_TEST is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'ADD_TEST', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :new.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :new.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :new.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :new.EFFECTIVE_DATE||'; ', pk_lookup, :old.ADD_TEST, :new.ADD_TEST, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.BASE_YEAR <> :new.BASE_YEAR OR 
     (:old.BASE_YEAR is null AND :new.BASE_YEAR is not null) OR 
     (:new.BASE_YEAR is null AND :old.BASE_YEAR is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'BASE_YEAR', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :new.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :new.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :new.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :new.EFFECTIVE_DATE||'; ', pk_lookup, :old.BASE_YEAR, :new.BASE_YEAR, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.COMPANY_ID <> :new.COMPANY_ID OR 
     (:old.COMPANY_ID is null AND :new.COMPANY_ID is not null) OR 
     (:new.COMPANY_ID is null AND :old.COMPANY_ID is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'COMPANY_ID', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :new.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :new.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :new.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :new.EFFECTIVE_DATE||'; ', pk_lookup, :old.COMPANY_ID, :new.COMPANY_ID, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.EFFECTIVE_DATE <> :new.EFFECTIVE_DATE OR 
     (:old.EFFECTIVE_DATE is null AND :new.EFFECTIVE_DATE is not null) OR 
     (:new.EFFECTIVE_DATE is null AND :old.EFFECTIVE_DATE is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'EFFECTIVE_DATE', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :new.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :new.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :new.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :new.EFFECTIVE_DATE||'; ', pk_lookup, :old.EFFECTIVE_DATE, :new.EFFECTIVE_DATE, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.REPAIR_LOCATION_ID <> :new.REPAIR_LOCATION_ID OR 
     (:old.REPAIR_LOCATION_ID is null AND :new.REPAIR_LOCATION_ID is not null) OR 
     (:new.REPAIR_LOCATION_ID is null AND :old.REPAIR_LOCATION_ID is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'REPAIR_LOCATION_ID', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :new.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :new.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :new.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :new.EFFECTIVE_DATE||'; ', pk_lookup, :old.REPAIR_LOCATION_ID, :new.REPAIR_LOCATION_ID, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.REPAIR_UNIT_CODE_ID <> :new.REPAIR_UNIT_CODE_ID OR 
     (:old.REPAIR_UNIT_CODE_ID is null AND :new.REPAIR_UNIT_CODE_ID is not null) OR 
     (:new.REPAIR_UNIT_CODE_ID is null AND :old.REPAIR_UNIT_CODE_ID is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'REPAIR_UNIT_CODE_ID', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :new.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :new.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :new.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :new.EFFECTIVE_DATE||'; ', pk_lookup, :old.REPAIR_UNIT_CODE_ID, :new.REPAIR_UNIT_CODE_ID, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.REPLACEMENT_COST <> :new.REPLACEMENT_COST OR 
     (:old.REPLACEMENT_COST is null AND :new.REPLACEMENT_COST is not null) OR 
     (:new.REPLACEMENT_COST is null AND :old.REPLACEMENT_COST is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'REPLACEMENT_COST', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :new.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :new.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :new.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :new.EFFECTIVE_DATE||'; ', pk_lookup, :old.REPLACEMENT_COST, :new.REPLACEMENT_COST, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
END IF; 
IF INSERTING THEN 
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'ADD_TEST', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :new.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :new.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :new.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :new.EFFECTIVE_DATE||'; ', pk_lookup, :new.ADD_TEST, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'BASE_YEAR', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :new.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :new.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :new.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :new.EFFECTIVE_DATE||'; ', pk_lookup, :new.BASE_YEAR, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'COMPANY_ID', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :new.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :new.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :new.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :new.EFFECTIVE_DATE||'; ', pk_lookup, :new.COMPANY_ID, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'EFFECTIVE_DATE', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :new.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :new.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :new.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :new.EFFECTIVE_DATE||'; ', pk_lookup, :new.EFFECTIVE_DATE, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'REPAIR_LOCATION_ID', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :new.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :new.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :new.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :new.EFFECTIVE_DATE||'; ', pk_lookup, :new.REPAIR_LOCATION_ID, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'REPAIR_UNIT_CODE_ID', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :new.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :new.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :new.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :new.EFFECTIVE_DATE||'; ', pk_lookup, :new.REPAIR_UNIT_CODE_ID, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'REPLACEMENT_COST', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :new.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :new.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :new.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :new.EFFECTIVE_DATE||'; ', pk_lookup, :new.REPLACEMENT_COST, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
END IF; 
IF DELETING THEN 
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'ADD_TEST', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :old.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :old.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :old.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :old.EFFECTIVE_DATE||'; ', pk_lookup, :old.ADD_TEST, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'BASE_YEAR', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :old.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :old.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :old.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :old.EFFECTIVE_DATE||'; ', pk_lookup, :old.BASE_YEAR, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'COMPANY_ID', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :old.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :old.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :old.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :old.EFFECTIVE_DATE||'; ', pk_lookup, :old.COMPANY_ID, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'EFFECTIVE_DATE', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :old.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :old.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :old.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :old.EFFECTIVE_DATE||'; ', pk_lookup, :old.EFFECTIVE_DATE, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'REPAIR_LOCATION_ID', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :old.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :old.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :old.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :old.EFFECTIVE_DATE||'; ', pk_lookup, :old.REPAIR_LOCATION_ID, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'REPAIR_UNIT_CODE_ID', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :old.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :old.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :old.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :old.EFFECTIVE_DATE||'; ', pk_lookup, :old.REPAIR_UNIT_CODE_ID, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_TABLES_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REPAIR_THRESHOLDS', 'REPLACEMENT_COST', 
        ''|| 'REPAIR_UNIT_CODE_ID=' || :old.REPAIR_UNIT_CODE_ID||'; '|| 'REPAIR_LOCATION_ID=' || :old.REPAIR_LOCATION_ID||'; '|| 'COMPANY_ID=' || :old.COMPANY_ID||'; '|| 'EFFECTIVE_DATE=' || :old.EFFECTIVE_DATE||'; ', pk_lookup, :old.REPLACEMENT_COST, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
END IF; 
END; 
/
DELETE FROM PP_TABLE_AUDITS WHERE TABLE_NAME = 'REPAIR_THRESHOLDS';
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('REPAIR_THRESHOLDS', 'ADD_TEST', 'PWRPLANT', to_date('2013-03-26 17:22:38', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_REPAIR_THRESHOLDS', 'IUD', null, 'TABLES', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('REPAIR_THRESHOLDS', 'BASE_YEAR', 'PWRPLANT', to_date('2013-03-26 17:22:38', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_REPAIR_THRESHOLDS', 'IUD', null, 'TABLES', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('REPAIR_THRESHOLDS', 'COMPANY_ID', 'PWRPLANT', to_date('2013-03-26 17:22:38', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_REPAIR_THRESHOLDS', 'IUD', 'company', 'TABLES', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('REPAIR_THRESHOLDS', 'EFFECTIVE_DATE', 'PWRPLANT', to_date('2013-03-26 17:22:38', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_REPAIR_THRESHOLDS', 'IUD', null, 'TABLES', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('REPAIR_THRESHOLDS', 'REPAIR_LOCATION_ID', 'PWRPLANT', to_date('2013-03-26 17:22:38', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_REPAIR_THRESHOLDS', 'IUD', null, 'TABLES', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('REPAIR_THRESHOLDS', 'REPAIR_UNIT_CODE_ID', 'PWRPLANT', to_date('2013-03-26 17:22:38', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_REPAIR_THRESHOLDS', 'IUD', null, 'TABLES', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('REPAIR_THRESHOLDS', 'REPLACEMENT_COST', 'PWRPLANT', to_date('2013-03-26 17:22:38', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_REPAIR_THRESHOLDS', 'IUD', null, 'TABLES', null, 0) ; 
DELETE FROM PP_TABLE_AUDITS_OBJ_ACTIONS WHERE TRIGGER_NAME = 'PP_AU_REPAIR_THRESHOLDS';
DELETE FROM PP_TABLE_AUDITS_PK_LOOKUP WHERE TABLE_NAME = 'REPAIR_THRESHOLDS';
INSERT INTO "PP_TABLE_AUDITS_PK_LOOKUP" ("TABLE_NAME", "COLUMN_NAME", "DISPLAY_TABLE_NAME", "DISPLAY_COLUMN_NAME", "CODE_COLUMN_NAME", "TIME_STAMP", "USER_ID") VALUES ('REPAIR_THRESHOLDS', 'COMPANY_ID', 'company_setup', 'description', 'company_id', to_date('2013-03-26 17:22:38', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ; 
INSERT INTO "PP_TABLE_AUDITS_PK_LOOKUP" ("TABLE_NAME", "COLUMN_NAME", "DISPLAY_TABLE_NAME", "DISPLAY_COLUMN_NAME", "CODE_COLUMN_NAME", "TIME_STAMP", "USER_ID") VALUES ('REPAIR_THRESHOLDS', 'EFFECTIVE_DATE', null, null, null, to_date('2013-03-26 17:22:38', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ; 
INSERT INTO "PP_TABLE_AUDITS_PK_LOOKUP" ("TABLE_NAME", "COLUMN_NAME", "DISPLAY_TABLE_NAME", "DISPLAY_COLUMN_NAME", "CODE_COLUMN_NAME", "TIME_STAMP", "USER_ID") VALUES ('REPAIR_THRESHOLDS', 'REPAIR_LOCATION_ID', null, null, null, to_date('2013-03-26 17:22:38', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ; 
INSERT INTO "PP_TABLE_AUDITS_PK_LOOKUP" ("TABLE_NAME", "COLUMN_NAME", "DISPLAY_TABLE_NAME", "DISPLAY_COLUMN_NAME", "CODE_COLUMN_NAME", "TIME_STAMP", "USER_ID") VALUES ('REPAIR_THRESHOLDS', 'REPAIR_UNIT_CODE_ID', null, null, null, to_date('2013-03-26 17:22:38', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ; 
COMMIT;
