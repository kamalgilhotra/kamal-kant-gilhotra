
  CREATE OR REPLACE EDITIONABLE TRIGGER "PWRPLANT"."LS_REMOVE_ACTUAL_CURR_RATES" 
BEFORE DELETE ON currency_rate
FOR EACH ROW
  BEGIN
    DELETE from currency_rate_default
    WHERE exchange_date = :old.exchange_date
    AND currency_from = :old.currency_from
    AND currency_to = :old.currency_to
    AND exchange_rate_type_id = :old.exchange_rate_type_id
    AND rate = :old.rate;
  END;
/
ALTER TRIGGER "PWRPLANT"."LS_REMOVE_ACTUAL_CURR_RATES" ENABLE;