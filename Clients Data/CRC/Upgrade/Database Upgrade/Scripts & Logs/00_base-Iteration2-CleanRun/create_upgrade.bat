REM /*
REM ||============================================================================
REM || Application: PowerPlan
REM || Object Name: v2018.1.0.0_create_upgrade.bat
REM || Description: Bat file to start SQLPLUS and run the main upgrade script.
REM ||============================================================================
REM || Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved.
REM ||============================================================================
REM || Version    Date       Revised By               Reason for Change
REM || ---------- ---------- --------------           -------------------------------------
REM || 2018.1.0.0  12/12/2018 build script         2018.1.0.0 Release
REM ||============================================================================
REM */
REM
REM Replace <SID> with the database SID name.
REM Replace the PWRPLANT password as needed.
REM

sqlplus pwrplant/pwrplant@hopplant @create_upgrade.sql

pause