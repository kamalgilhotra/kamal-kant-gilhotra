/* Database */
create table pwrplant.pp_web_datatypes("NAME" varchar2(200) ,"EXT" varchar2(10) ,"BINARY" number(22,0)) TABLESPACE PWRPLANT;
alter table  pwrplant.PP_WEB_DATATYPES ADD ( constraint  PK_PP_WEB_DATATYPES PRIMARY KEY (NAME) using index tablespace PWRPLANT_IDX );


create table pwrplant.pp_web_data("NAME" varchar2(200) ,"DATATYPE" varchar2(100) ,"DATALEN" number(22),"DATA" blob, textdata clob) TABLESPACE PWRPLANT;
alter table  pwrplant.PP_WEB_data ADD ( constraint pk_pp_web_data  PRIMARY KEY (NAME) using index tablespace PWRPLANT_IDX );

create table pwrplant.pp_web_sites("NAME" varchar2(200) ,site varchar(2000));
alter table  pwrplant.PP_WEB_sites ADD ( constraint pk_PP_WEB_sites  PRIMARY KEY (NAME) using index tablespace PWRPLANT_IDX );


INSERT INTO pp_web_datatypes VALUES (
	'text/cmd',
	'cmd',
	0);
INSERT INTO pp_web_datatypes VALUES (
	'text/css',
	'css',
	0);
INSERT INTO pp_web_datatypes VALUES (
	'text/csv',
	'csv',
	0);
INSERT INTO pp_web_datatypes VALUES (
	'text/htm',
	'htm',
	0);
INSERT INTO pp_web_datatypes VALUES (
	'text/html',
	'html',
	0);
INSERT INTO pp_web_datatypes VALUES (
	'application/javascript',
	'js',
	0);
INSERT INTO pp_web_datatypes VALUES (
	'text/plain',
	'txt',
	0);
INSERT INTO pp_web_datatypes VALUES (
	'text/xml',
	'xml',
	0);
INSERT INTO pp_web_datatypes VALUES (
	'image/gif',
	'gif',
	1);
INSERT INTO pp_web_datatypes VALUES (
	'image/jpeg',
	'jpg',
	1);
INSERT INTO pp_web_datatypes VALUES (
	'image/png',
	'pbg',
	1);
INSERT INTO pp_web_datatypes VALUES (
	'image/vnd.microsoft.icon',
	'ico',
	1);
INSERT INTO pp_web_datatypes VALUES (
	'image/tiff',
	'tif',
	1);
INSERT INTO pp_web_datatypes VALUES (
	'application/pdf',
	'pdf',
	1);
INSERT INTO pp_web_datatypes VALUES (
	'application/postscript',
	'ps',
	0);
INSERT INTO pp_web_datatypes VALUES (
	'application/x-shockware-flash',
	'swf',
	1);
INSERT INTO pp_web_datatypes VALUES (
	'application/json',
	'json',
	0);

commit;