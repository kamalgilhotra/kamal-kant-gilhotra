------------------------------
--Lessor Asset Data (All Non-CPR Assets)
--Since this adds multiple assets for a large amount of ILRs
--	the additions will be grouped by the ILR receiving the asset records
------------------------------

------------------------------
--Add 3 Assets for AUTO-ILR-Monthly-Arr-ST
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Arr-ST1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Monthly-Arr-ST1', 'AUTO-Asset-Monthly-Arr-ST1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Arr-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Arr-ST2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Monthly-Arr-ST2', 'AUTO-Asset-Monthly-Arr-ST2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Arr-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Arr-ST3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Monthly-Arr-ST3', 'AUTO-Asset-Monthly-Arr-ST3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Arr-ST';

------------------------------
--Add 3 Assets for AUTO-ILR-Quarterly-Arr-ST
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Arr-ST1', 20000, 19000, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Quarterly-Arr-ST1', 'AUTO-Asset-Quarterly-Arr-ST1', 20000 / in_service_exchange_rate, 19000 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Arr-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Arr-ST2', 21000, 21000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Arr-ST2', 'AUTO-Asset-Quarterly-Arr-ST2', 21000 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Arr-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Arr-ST3', 21000, 22000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Arr-ST3', 'AUTO-Asset-Quarterly-Arr-ST3', 21000 / in_service_exchange_rate, 22000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Arr-ST';

------------------------------
--Add 3 Assets for AUTO-ILR-Semi-Annual-Arr-ST
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Arr-ST1', 17500, 20000, 4200, 0, 6500/17500/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Semi-Annual-Arr-ST1', 'AUTO-Asset-Semi-Annual-Arr-ST1', 17500 / in_service_exchange_rate, 20000 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Arr-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Arr-ST2', 18500, 21000, 4400, 0, 6750/18500/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Semi-Annual-Arr-ST2', 'AUTO-Asset-Semi-Annual-Arr-ST2', 18500 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Arr-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Arr-ST3', 18000, 21000, 4400, 0, 6750/18000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Semi-Annual-Arr-ST3', 'AUTO-Asset-Semi-Annual-Arr-ST3', 18000 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Arr-ST';

------------------------------
--Add 3 Assets for AUTO-ILR-Annual-Arr-ST
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Arr-ST1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Annual-Arr-ST1', 'AUTO-Asset-Annual-Arr-ST1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Arr-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Arr-ST2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Annual-Arr-ST2', 'AUTO-Asset-Annual-Arr-ST2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Arr-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Arr-ST3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Annual-Arr-ST3', 'AUTO-Asset-Annual-Arr-ST3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Arr-ST';

------------------------------
--Add 3 Assets for AUTO-ILR-Monthly-Arr-DF
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Arr-DF1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Monthly-Arr-DF1', 'AUTO-Asset-Monthly-Arr-DF1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Arr-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Arr-DF2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Monthly-Arr-DF2', 'AUTO-Asset-Monthly-Arr-DF2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Arr-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Arr-DF3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Monthly-Arr-DF3', 'AUTO-Asset-Monthly-Arr-DF3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Arr-DF';

------------------------------
--Add 3 Assets for AUTO-ILR-Quarterly-Arr-DF
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Arr-DF1', 17500, 20000, 4200, 0, 6500/17500/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Quarterly-Arr-DF1', 'AUTO-Asset-Quarterly-Arr-DF1', 17500 / in_service_exchange_rate, 20000 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Arr-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Arr-DF2', 18500, 21000, 4400, 0, 6750/18500/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Arr-DF2', 'AUTO-Asset-Quarterly-Arr-DF2', 18500 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Arr-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Arr-DF3', 18000, 21000, 4400, 0, 6750/18000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Arr-DF3', 'AUTO-Asset-Quarterly-Arr-DF3', 18000 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Arr-DF';

------------------------------
--Add 3 Assets for AUTO-ILR-Semi-Annual-Arr-DF
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Arr-DF1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Semi-Annual-Arr-DF1', 'AUTO-Asset-Semi-Annual-Arr-DF1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Arr-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Arr-DF2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Semi-Annual-Arr-DF2', 'AUTO-Asset-Semi-Annual-Arr-DF2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Arr-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Arr-DF3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Semi-Annual-Arr-DF3', 'AUTO-Asset-Semi-Annual-Arr-DF3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Arr-DF';

------------------------------
--Add 3 Assets for AUTO-ILR-Annual-Arr-DF
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Arr-DF1', 20000, 19000, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Annual-Arr-DF1', 'AUTO-Asset-Annual-Arr-DF1', 20000 / in_service_exchange_rate, 19000 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Arr-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Arr-DF2', 21000, 21000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Annual-Arr-DF2', 'AUTO-Asset-Annual-Arr-DF2', 21000 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Arr-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Arr-DF3', 21000, 22000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Annual-Arr-DF3', 'AUTO-Asset-Annual-Arr-DF3', 21000 / in_service_exchange_rate, 22000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Arr-DF';

------------------------------
--Add 3 Assets for AUTO-ILR-Monthly-Arr-Op
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Arr-Op1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Monthly-Arr-Op1', 'AUTO-Asset-Monthly-Arr-Op1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Arr-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Arr-Op2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Monthly-Arr-Op2', 'AUTO-Asset-Monthly-Arr-Op2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Arr-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Arr-Op3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Monthly-Arr-Op3', 'AUTO-Asset-Monthly-Arr-Op3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Arr-Op';

------------------------------
--Add 3 Assets for AUTO-ILR-Quarterly-Arr-Op
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Arr-Op1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Quarterly-Arr-Op1', 'AUTO-Asset-Quarterly-Arr-Op1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Arr-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Arr-Op2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Arr-Op2', 'AUTO-Asset-Quarterly-Arr-Op2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Arr-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Arr-Op3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Arr-Op3', 'AUTO-Asset-Quarterly-Arr-Op3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Arr-Op';

------------------------------
--Add 3 Assets for AUTO-ILR-Semi-Annual-Arr-Op
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Arr-Op1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Semi-Annual-Arr-Op1', 'AUTO-Asset-Semi-Annual-Arr-Op1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Arr-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Arr-Op2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Semi-Annual-Arr-Op2', 'AUTO-Asset-Semi-Annual-Arr-Op2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Arr-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Arr-Op3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Semi-Annual-Arr-Op3', 'AUTO-Asset-Semi-Annual-Arr-Op3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Arr-Op';

------------------------------
--Add 3 Assets for AUTO-ILR-Annual-Arr-Op
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Arr-Op1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Annual-Arr-Op1', 'AUTO-Asset-Annual-Arr-Op1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Arr-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Arr-Op2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Annual-Arr-Op2', 'AUTO-Asset-Annual-Arr-Op2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Arr-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Arr-Op3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Annual-Arr-Op3', 'AUTO-Asset-Annual-Arr-Op3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Arr-Op';

------------------------------
--Add 3 Assets for AUTO-ILR-Monthly-Pre-ST
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Pre-ST1', 20000, 19000, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Monthly-Pre-ST1', 'AUTO-Asset-Monthly-Pre-ST1', 5000 / in_service_exchange_rate, 8000 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Pre-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Pre-ST2', 21000, 21000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Monthly-Pre-ST2', 'AUTO-Asset-Monthly-Pre-ST2', 21000 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Pre-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Pre-ST3', 21000, 22000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Monthly-Pre-ST3', 'AUTO-Asset-Monthly-Pre-ST3', 21000 / in_service_exchange_rate, 22000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Pre-ST';

------------------------------
--Add 3 Assets for AUTO-ILR-Quarterly-Pre-ST
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-ST1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Quarterly-Pre-ST1', 'AUTO-Asset-Quarterly-Pre-ST1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Pre-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-ST2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Pre-ST2', 'AUTO-Asset-Quarterly-Pre-ST2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Pre-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-ST3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Pre-ST3', 'AUTO-Asset-Quarterly-Pre-ST3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Pre-ST';

------------------------------
--Add 3 Assets for AUTO-ILR-Semi-Annual-Pre-ST
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Pre-ST1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Semi-Annual-Pre-ST1', 'AUTO-Asset-Semi-Annual-Pre-ST1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Pre-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Pre-ST2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Semi-Annual-Pre-ST2', 'AUTO-Asset-Semi-Annual-Pre-ST2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Pre-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Pre-ST3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Semi-Annual-Pre-ST3', 'AUTO-Asset-Semi-Annual-Pre-ST3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Pre-ST';

------------------------------
--Add 3 Assets for AUTO-ILR-Annual-Pre-ST
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Pre-ST1', 17500, 20000, 4200, 0, 6500/17500/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Annual-Pre-ST1', 'AUTO-Asset-Annual-Pre-ST1', 17500 / in_service_exchange_rate, 20000 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Pre-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Pre-ST2', 18500, 21000, 4400, 0, 6750/18500/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Annual-Pre-ST2', 'AUTO-Asset-Annual-Pre-ST2', 18500 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Pre-ST';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Pre-ST3', 18000, 21000, 4400, 0, 6750/18000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Annual-Pre-ST3', 'AUTO-Asset-Annual-Pre-ST3', 18000 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Pre-ST';

------------------------------
--Add 3 Assets for AUTO-ILR-Monthly-Pre-DF
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Pre-DF1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Monthly-Pre-DF1', 'AUTO-Asset-Monthly-Pre-DF1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Pre-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Pre-DF2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Monthly-Pre-DF2', 'AUTO-Asset-Monthly-Pre-DF2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Pre-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Pre-DF3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Monthly-Pre-DF3', 'AUTO-Asset-Monthly-Pre-DF3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Pre-DF';

------------------------------
--Add 3 Assets for AUTO-ILR-Quarterly-Pre-DF
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-DF1', 17500, 20000, 4200, 0, 6500/17500/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Quarterly-Pre-DF1', 'AUTO-Asset-Quarterly-Pre-DF1', 17500 / in_service_exchange_rate, 20000 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Pre-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-DF2', 18500, 21000, 4400, 0, 6750/18500/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Pre-DF2', 'AUTO-Asset-Quarterly-Pre-DF2', 18500 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Pre-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-DF3', 18000, 21000, 4400, 0, 6750/18000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Pre-DF3', 'AUTO-Asset-Quarterly-Pre-DF3', 18000 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Pre-DF';

------------------------------
--Add 3 Assets for AUTO-ILR-Semi-Annual-Pre-DF
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Pre-DF1', 20000, 19000, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Semi-Annual-Pre-DF1', 'AUTO-Asset-Semi-Annual-Pre-DF1', 20000 / in_service_exchange_rate, 19000 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Pre-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Pre-DF2', 21000, 21000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Semi-Annual-Pre-DF2', 'AUTO-Asset-Semi-Annual-Pre-DF2', 21000 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Pre-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Pre-DF3', 21000, 22000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Semi-Annual-Pre-DF3', 'AUTO-Asset-Semi-Annual-Pre-DF3', 21000 / in_service_exchange_rate, 22000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Pre-DF';

------------------------------
--Add 3 Assets for AUTO-ILR-Annual-Pre-DF
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Pre-DF1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Annual-Pre-DF1', 'AUTO-Asset-Annual-Pre-DF1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Pre-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Pre-DF2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Annual-Pre-DF2', 'AUTO-Asset-Annual-Pre-DF2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Pre-DF';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Pre-DF3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Annual-Pre-DF3', 'AUTO-Asset-Annual-Pre-DF3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Pre-DF';

------------------------------
--Add 3 Assets for AUTO-ILR-Monthly-Pre-Op
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Pre-Op1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Monthly-Pre-Op1', 'AUTO-Asset-Monthly-Pre-Op1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Pre-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Pre-Op2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Monthly-Pre-Op2', 'AUTO-Asset-Monthly-Pre-Op2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Pre-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Pre-Op3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Monthly-Pre-Op3', 'AUTO-Asset-Monthly-Pre-Op3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Pre-Op';

------------------------------
--Add 3 Assets for AUTO-ILR-Quarterly-Pre-Op
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-Op1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Quarterly-Pre-Op1', 'AUTO-Asset-Quarterly-Pre-Op1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Pre-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-Op2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Pre-Op2', 'AUTO-Asset-Quarterly-Pre-Op2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Pre-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-Op3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Pre-Op3', 'AUTO-Asset-Quarterly-Pre-Op3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Pre-Op';

------------------------------
--Add 3 Assets for AUTO-ILR-Semi-Annual-Pre-Op
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Pre-Op1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Semi-Annual-Pre-Op1', 'AUTO-Asset-Semi-Annual-Pre-Op1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Pre-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Pre-Op2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Semi-Annual-Pre-Op2', 'AUTO-Asset-Semi-Annual-Pre-Op2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Pre-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Pre-Op3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Semi-Annual-Pre-Op3', 'AUTO-Asset-Semi-Annual-Pre-Op3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Pre-Op';

------------------------------
--Add 3 Assets for AUTO-ILR-Annual-Pre-Op
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Pre-Op1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Annual-Pre-Op1', 'AUTO-Asset-Annual-Pre-Op1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Pre-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Pre-Op2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Annual-Pre-Op2', 'AUTO-Asset-Annual-Pre-Op2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Pre-Op';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Pre-Op3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Annual-Pre-Op3', 'AUTO-Asset-Annual-Pre-Op3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Pre-Op';

------------------------------
--Add 3 Assets for AUTO-ILR-Monthly-Arr-Mixed
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Arr-Mixed1', 17500, 20000, 4200, 0, 6500/17500/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Monthly-Arr-Mixed1', 'AUTO-Asset-Monthly-Arr-Mixed1', 17500 / in_service_exchange_rate, 20000 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Arr-Mixed';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Arr-Mixed2', 18500, 21000, 4400, 0, 6750/18500/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Monthly-Arr-Mixed2', 'AUTO-Asset-Monthly-Arr-Mixed2', 18500 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Arr-Mixed';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Monthly-Arr-Mixed3', 18000, 21000, 4400, 0, 6750/18000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Monthly-Arr-Mixed3', 'AUTO-Asset-Monthly-Arr-Mixed3', 18000 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Monthly-Arr-Mixed';

------------------------------
--Add 3 Assets for AUTO-ILR-Quarterly-Pre-Mixed
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-Mixed1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Quarterly-Pre-Mixed1', 'AUTO-Asset-Quarterly-Pre-Mixed1', 20000 / in_service_exchange_rate, 17500 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Pre-Mixed';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-Mixed2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Pre-Mixed2', 'AUTO-Asset-Quarterly-Pre-Mixed2', 21000 / in_service_exchange_rate, 18500 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Pre-Mixed';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-Mixed3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Pre-Mixed3', 'AUTO-Asset-Quarterly-Pre-Mixed3', 21000 / in_service_exchange_rate, 18000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Quarterly-Pre-Mixed';

------------------------------
--Add 3 Assets for AUTO-ILR-Semi-Annual-Arr-Mixed
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Arr-Mixed1', 20000, 19000, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Semi-Annual-Arr-Mixed1', 'AUTO-Asset-Semi-Annual-Arr-Mixed1', 1000 / in_service_exchange_rate, 3000 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Arr-Mixed';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Arr-Mixed2', 21000, 21000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Semi-Annual-Arr-Mixed2', 'AUTO-Asset-Semi-Annual-Arr-Mixed2', 21000 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Arr-Mixed';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Semi-Annual-Arr-Mixed3', 21000, 22000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Semi-Annual-Arr-Mixed3', 'AUTO-Asset-Semi-Annual-Arr-Mixed3', 21000 / in_service_exchange_rate, 22000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Semi-Annual-Arr-Mixed';

------------------------------
--Add 3 Assets for AUTO-ILR-Annual-Pre-Mixed
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Pre-Mixed1', 17500, 20000, 4200, 0, 6500/17500/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Annual-Pre-Mixed1', 'AUTO-Asset-Annual-Pre-Mixed1', 17500 / in_service_exchange_rate, 20000 / in_service_exchange_rate, 4200 / in_service_exchange_rate, 0, 2300 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Pre-Mixed';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Pre-Mixed2', 18500, 21000, 4400, 0, 6750/18500/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Annual-Pre-Mixed2', 'AUTO-Asset-Annual-Pre-Mixed2', 18500 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Pre-Mixed';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Annual-Pre-Mixed3', 18000, 21000, 4400, 0, 6750/18000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Annual-Pre-Mixed3', 'AUTO-Asset-Annual-Pre-Mixed3', 18000 / in_service_exchange_rate, 21000 / in_service_exchange_rate, 4400 / in_service_exchange_rate, 0, 2350 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-ILR-Annual-Pre-Mixed';

------------------------------
--Add 3 Assets for AUTO-ILR-Monthly-Arr-ST-JPY
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-ST-JPY1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Quarterly-Pre-ST-JPY1', 'AUTO-Asset-Quarterly-Pre-ST-JPY1', NULL, NULL, NULL, NULL, NULL
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Asset-Quarterly-Pre-ST-JPY1';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-ST-JPY2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Pre-ST-JPY2', 'AUTO-Asset-Quarterly-Pre-ST-JPY2', NULL, NULL, NULL, NULL, NULL
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Asset-Quarterly-Pre-ST-JPY2';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-ST-JPY3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Pre-ST-JPY3', 'AUTO-Asset-Quarterly-Pre-ST-JPY3', NULL, NULL, NULL, NULL, NULL
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Asset-Quarterly-Pre-ST-JPY3';

------------------------------
--Add 3 Assets for AUTO-ILR-Monthly-Arr-DF-JPY
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-DF-JPY1', 20000, 17500, 4200, 0, 6500/20000/*estimated amount over the FMV*/, 2300,
30, 30, 'AUTO-Asset-Quarterly-Pre-DF-JPY1', 'AUTO-Asset-Quarterly-Pre-DF-JPY1', NULL, NULL, NULL, NULL, NULL
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Asset-Quarterly-Pre-DF-JPY1';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-DF-JPY2', 21000, 18500, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Pre-DF-JPY2', 'AUTO-Asset-Quarterly-Pre-DF-JPY2', NULL, NULL, NULL, NULL, NULL
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Asset-Quarterly-Pre-DF-JPY2';

insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Asset-Quarterly-Pre-DF-JPY3', 21000, 18000, 4400, 0, 6750/21000/*estimated amount over the FMV*/, 2350,
30, 30, 'AUTO-Asset-Quarterly-Pre-DF-JPY3', 'AUTO-Asset-Quarterly-Pre-DF-JPY3', NULL, NULL, NULL, NULL, NULL
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Asset-Quarterly-Pre-DF-JPY3';