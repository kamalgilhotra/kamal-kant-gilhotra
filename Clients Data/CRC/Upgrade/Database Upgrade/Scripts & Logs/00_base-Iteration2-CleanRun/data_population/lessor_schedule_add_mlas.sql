------------------------
--MLA Header Table Data
------------------------
--add the Fixed-MLA header record
insert into lsr_lease(lease_id, lease_number, description, long_description, lessee_id, lease_type_id, lease_status_id, lease_group_id, lease_cap_type_id,
workflow_type_id, contract_currency_id, payment_due_day, pre_payment_sw, master_agreement_date, initiation_date, lease_end_date, days_in_year, current_revision, approval_date)
select ls_lease_seq.nextval, 'AUTO Fixed MLA1', 'AUTO Fixed MLA1', 'AUTO Fixed MLA1', lessee_id, lease_type_id, lease_status_id, lsr_lease_group.lease_group_id, cap_type_id,
(select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), currency_id, 31, 0 /*for No*/, sysdate, sysdate, sysdate, 360, 1, sysdate
from lsr_lessee, ls_lease_type, ls_lease_status, lsr_lease_group, lsr_cap_type, currency
where lsr_lessee.description = 'AUTO Lessee1'
and ls_lease_type.description = 'Fixed'
and ls_lease_status.description = 'Open'
and lsr_lease_group.description = 'AUTO LG1'
and lsr_cap_type.description = 'AUTO Cap Type Sales'
and currency.iso_code = 'GBP';

--add the Variable-MLA header record
insert into lsr_lease(lease_id, lease_number, description, long_description, lessee_id, lease_type_id, lease_status_id, lease_group_id, lease_cap_type_id,
workflow_type_id, contract_currency_id, payment_due_day, pre_payment_sw, master_agreement_date, initiation_date, lease_end_date, days_in_year, current_revision, approval_date)
select ls_lease_seq.nextval, 'AUTO Variable MLA1', 'AUTO Variable MLA1', 'AUTO Variable MLA1', lessee_id, lease_type_id, lease_status_id, lsr_lease_group.lease_group_id, cap_type_id,
(select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), currency_id, 31, 0 /*for No*/, sysdate, sysdate, sysdate, 360, 1, sysdate
from lsr_lessee, ls_lease_type, ls_lease_status, lsr_lease_group, lsr_cap_type, currency
where lsr_lessee.description = 'AUTO Lessee1'
and ls_lease_type.description = 'Variable'
and ls_lease_status.description = 'Open'
and lsr_lease_group.description = 'AUTO LG1'
and lsr_cap_type.description = 'AUTO Cap Type Sales'
and currency.iso_code = 'GBP';

--add the Prepay-MLA header record
insert into lsr_lease(lease_id, lease_number, description, long_description, lessee_id, lease_type_id, lease_status_id, lease_group_id, lease_cap_type_id,
workflow_type_id, contract_currency_id, payment_due_day, pre_payment_sw, master_agreement_date, initiation_date, lease_end_date, days_in_year, current_revision, approval_date)
select ls_lease_seq.nextval, 'AUTO Prepay MLA1', 'AUTO Prepay MLA1', 'AUTO Prepay MLA1', lessee_id, lease_type_id, lease_status_id, lsr_lease_group.lease_group_id, cap_type_id,
(select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), currency_id, 31, 1 /*for Yes*/, sysdate, sysdate, sysdate, 360, 1, sysdate
from lsr_lessee, ls_lease_type, ls_lease_status, lsr_lease_group, lsr_cap_type, currency
where lsr_lessee.description = 'AUTO Lessee1'
and ls_lease_type.description = 'Variable'
and ls_lease_status.description = 'Open'
and lsr_lease_group.description = 'AUTO LG1'
and lsr_cap_type.description = 'AUTO Cap Type Sales'
and currency.iso_code = 'GBP';

--add the Arrears-MLA header record
insert into lsr_lease(lease_id, lease_number, description, long_description, lessee_id, lease_type_id, lease_status_id, lease_group_id, lease_cap_type_id,
workflow_type_id, contract_currency_id, payment_due_day, pre_payment_sw, master_agreement_date, initiation_date, lease_end_date, days_in_year, current_revision, approval_date)
select ls_lease_seq.nextval, 'AUTO Arrears MLA1', 'AUTO Arrears MLA1', 'AUTO Arrears MLA1', lessee_id, lease_type_id, lease_status_id, lsr_lease_group.lease_group_id, cap_type_id,
(select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), currency_id, 31, 0 /*for No*/, sysdate, sysdate, sysdate, 360, 1, sysdate
from lsr_lessee, ls_lease_type, ls_lease_status, lsr_lease_group, lsr_cap_type, currency
where lsr_lessee.description = 'AUTO Lessee1'
and ls_lease_type.description = 'Variable'
and ls_lease_status.description = 'Open'
and lsr_lease_group.description = 'AUTO LG1'
and lsr_cap_type.description = 'AUTO Cap Type Sales'
and currency.iso_code = 'GBP';

--add the JPY-MLA header record (for missing exchange rates)
insert into lsr_lease(lease_id, lease_number, description, long_description, lessee_id, lease_type_id, lease_status_id, lease_group_id, lease_cap_type_id,
workflow_type_id, contract_currency_id, payment_due_day, pre_payment_sw, master_agreement_date, initiation_date, lease_end_date, days_in_year, current_revision, approval_date)
select ls_lease_seq.nextval, 'AUTO JPY MLA1', 'AUTO JPY MLA1', 'AUTO JPY MLA1', lessee_id, lease_type_id, lease_status_id, lsr_lease_group.lease_group_id, cap_type_id,
(select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), currency_id, 31, 0 /*for No*/, sysdate, sysdate, sysdate, 360, 1, sysdate
from lsr_lessee, ls_lease_type, ls_lease_status, lsr_lease_group, lsr_cap_type, currency
where lsr_lessee.description = 'AUTO Lessee1'
and ls_lease_type.description = 'Variable'
and ls_lease_status.description = 'Open'
and lsr_lease_group.description = 'AUTO LG1'
and lsr_cap_type.description = 'AUTO Cap Type Sales'
and currency.iso_code = 'JPY';

------------------------------------------
--MLA Approvals Data (so we can add ILRs)
------------------------------------------
insert into lsr_lease_approval(lease_id, revision, approval_type_id, approval_status_id, approver, approval_date)
select lease_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), ls_lease_status.lease_status_id, 'PWRPLANT', sysdate
from lsr_lease, ls_lease_status
where lsr_lease.lease_number in ('AUTO Fixed MLA1','AUTO Variable MLA1','AUTO Prepay MLA1','AUTO Arrears MLA1','AUTO JPY MLA1')
and ls_lease_status.description = 'Open';

-------------------------------
--MLA Company Association Data
-------------------------------
insert into lsr_lease_company(lease_id, company_id)
select lease_id, 1 /*we want company 100, may need to may this more generic in the future*/
from lsr_lease
where description in ('AUTO Fixed MLA1','AUTO Variable MLA1','AUTO Prepay MLA1','AUTO Arrears MLA1','AUTO JPY MLA1');

------------------------
--MLA Options Table Data
------------------------
insert into lsr_lease_options(lease_id, revision, renewal_option_type_id, purchase_option_type_id, purchase_option_amt,
cancelable_type_id, itc_sw, intent_to_purchase, likely_to_collect, specialized_asset, sublease)
select lease_id, 1, renewal_option_type_id, purchase_option_type_id, 0,
cancelable_type_id, 0 /*no investment tax credit*/, 0 /*no intent to purchase*/, 1 /*yes likely to collect*/, 0 /*no specialised asset*/, 0 /*no sublease*/
from lsr_lease, ls_renewal_option_type, ls_purchase_option_type, ls_cancelable_type
where lsr_lease.lease_number in ('AUTO Fixed MLA1','AUTO Variable MLA1','AUTO Prepay MLA1','AUTO Arrears MLA1','AUTO JPY MLA1')
and ls_renewal_option_type.description = 'None'
and ls_purchase_option_type.description = 'None'
and ls_cancelable_type.description = 'Not Cancelable';