------------------------------
--Lessor Asset Data (All Non-CPR Assets)
--Since this is really annoying to write in any other way
--	the additions will be grouped by the ILR receiving the asset records
------------------------------

------------------------------
--Add 1 Asset for AUTO-Arr-MC-DF-ILR1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-MC-DF-ILR1-Asset1', 900000, 900000, 15000, 0, 20000/900000/*estimated amount over FMV*/, 5000,
30, 30, 'AUTO-Arr-MC-DF-ILR1-Asset1', 'AUTO-Arr-MC-DF-ILR1-Asset1', 900000 / in_service_exchange_rate, 900000 / in_service_exchange_rate, 15000 / in_service_exchange_rate, 0, 5000 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-MC-DF-ILR1';

------------------------------
--Add 1 Asset for AUTO-Arr-NonMC-DF-ILR1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-NonMC-DF-ILR1-Asset1', 325000, 350000, 15000, 0, 20000/325000/*estimated amount over FMV*/, 5000,
30, 30, 'AUTO-Arr-NonMC-DF-ILR1-Asset1', 'AUTO-Arr-NonMC-DF-ILR1-Asset1', 325000 / in_service_exchange_rate, 350000 / in_service_exchange_rate, 15000 / in_service_exchange_rate, 0, 5000 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-NonMC-DF-ILR1';

------------------------------
--Add 1 Asset for AUTO-Pre-NonMC-DF-ILR1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-NonMC-DF-ILR1-Asset1', 900000, 850000, 15000, 0, 20000/900000/*estimated amount over FMV*/, 5000,
30, 30, 'AUTO-Pre-NonMC-DF-ILR1-Asset1', 'AUTO-Pre-NonMC-DF-ILR1-Asset1', 900000 / in_service_exchange_rate, 850000 / in_service_exchange_rate, 15000 / in_service_exchange_rate, 0, 5000 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-NonMC-DF-ILR1';

------------------------------
--Add 1 Asset for AUTO-Pre-NonMC-DF-ILR2
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-NonMC-DF-ILR2-Asset1', 175000, 185000, 15000, 0, 20000/175000/*estimated amount over FMV*/, 5000,
30, 30, 'AUTO-Pre-NonMC-DF-ILR2-Asset1', 'AUTO-Pre-NonMC-DF-ILR2-Asset1', 175000 / in_service_exchange_rate, 185000 / in_service_exchange_rate, 15000 / in_service_exchange_rate, 0, 5000 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-NonMC-DF-ILR2';

------------------------------
--Add 1 Asset for AUTO-Pre-MC-DF-ILR1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-MC-DF-ILR1-Asset1', 95000, 95000, 15000, 0, 20000/95000/*estimated amount over FMV*/, 5000,
30, 30, 'AUTO-Pre-MC-DF-ILR1-Asset1', 'AUTO-Pre-MC-DF-ILR1-Asset1', 95000 / in_service_exchange_rate, 95000 / in_service_exchange_rate, 15000 / in_service_exchange_rate, 0, 5000 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-MC-DF-ILR1';

------------------------------
--Add 1 Asset for AUTO-Arr-MC-ST-ILR1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-MC-ST-ILR1-Asset1', 325000, 250000, 15000, 0, 20000/325000/*estimated amount over FMV*/, 5000,
30, 30, 'AUTO-Arr-MC-ST-ILR1-Asset1', 'AUTO-Arr-MC-ST-ILR1-Asset1', 325000 / in_service_exchange_rate, 250000 / in_service_exchange_rate, 15000 / in_service_exchange_rate, 0, 5000 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-MC-ST-ILR1';

------------------------------
--Add 1 Asset for AUTO-Arr-NonMC-ST-ILR2
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-NonMC-ST-ILR2-Asset1', 160000, 250000, 15000, 0, 20000/160000/*estimated amount over FMV*/, 5000,
30, 30, 'AUTO-Arr-NonMC-ST-ILR2-Asset1', 'AUTO-Arr-NonMC-ST-ILR2-Asset1', 160000 / in_service_exchange_rate, 250000 / in_service_exchange_rate, 15000 / in_service_exchange_rate, 0, 5000 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-NonMC-ST-ILR2';

------------------------------
--Add 1 Asset for AUTO-Arr-MC-ST-ILR2
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-MC-ST-ILR2-Asset1', 90000, 90000, 15000, 0, 20000/90000/*estimated amount over FMV*/, 5000,
30, 30, 'AUTO-Arr-MC-ST-ILR2-Asset1', 'AUTO-Arr-MC-ST-ILR2-Asset1', 90000 / in_service_exchange_rate, 90000 / in_service_exchange_rate, 15000 / in_service_exchange_rate, 0, 5000 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-MC-ST-ILR2';

------------------------------
--Add 1 Asset for AUTO-Pre-NonMC-ST-ILR1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-NonMC-ST-ILR1-Asset1', 325000, 200000, 15000, 0, 20000/325000/*estimated amount over FMV*/, 5000,
30, 30, 'AUTO-Pre-NonMC-ST-ILR1-Asset1', 'AUTO-Pre-NonMC-ST-ILR1-Asset1', 325000 / in_service_exchange_rate, 200000 / in_service_exchange_rate, 15000 / in_service_exchange_rate, 0, 5000 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-NonMC-ST-ILR1';

------------------------------
--Add 1 Asset for AUTO-Pre-MC-ST-ILR1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-MC-ST-ILR1-Asset1', 175000, 200000, 15000, 0, 20000/175000/*estimated amount over FMV*/, 5000,
30, 30, 'AUTO-Pre-MC-ST-ILR1-Asset1', 'AUTO-Pre-MC-ST-ILR1-Asset1', 175000 / in_service_exchange_rate, 200000 / in_service_exchange_rate, 15000 / in_service_exchange_rate, 0, 5000 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-MC-ST-ILR1';

------------------------------
--Add 1 Asset for AUTO-Pre-NonMC-ST-ILR2
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-NonMC-ST-ILR2-Asset1', 90000, 90000, 15000, 0, 20000/90000/*estimated amount over FMV*/, 5000,
30, 30, 'AUTO-Pre-NonMC-ST-ILR2-Asset1', 'AUTO-Pre-NonMC-ST-ILR2-Asset1', 90000 / in_service_exchange_rate, 90000 / in_service_exchange_rate, 15000 / in_service_exchange_rate, 0, 5000 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-NonMC-ST-ILR2';

------------------------------
--Add 1 Asset for AUTO-Arr-NonMC-OP-ILR1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-NonMC-OP-ILR1-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Arr-NonMC-OP-ILR1-Asset1', 'AUTO-Arr-NonMC-OP-ILR1-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-NonMC-OP-ILR1';

------------------------------
--Add 1 Asset for AUTO-Arr-MC-OP-ILR1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-MC-OP-ILR1-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Arr-MC-OP-ILR1-Asset1', 'AUTO-Arr-MC-OP-ILR1-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-MC-OP-ILR1';

------------------------------
--Add 1 Asset for AUTO-Pre-NonMC-OP-ILR1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-NonMC-OP-ILR1-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Pre-NonMC-OP-ILR1-Asset1', 'AUTO-Pre-NonMC-OP-ILR1-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-NonMC-OP-ILR1';

------------------------------
--Add 1 Asset for AUTO-Pre-MC-OP-ILR1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-MC-OP-ILR1-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Pre-MC-OP-ILR1-Asset1', 'AUTO-Pre-MC-OP-ILR1-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-MC-OP-ILR1';

------------------------------
--Add 1 Asset for AUTO-Arr-MC-OP-ILR2
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-MC-OP-ILR2-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Arr-MC-OP-ILR2-Asset1', 'AUTO-Arr-MC-OP-ILR2-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-MC-OP-ILR1';

------------------------------
--Add 1 Asset for AUTO-Pre-MC-OP-ILR2
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-MC-OP-ILR2-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Pre-MC-OP-ILR2-Asset1', 'AUTO-Pre-MC-OP-ILR2-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-MC-OP-ILR2';

------------------------------
--Add 1 Asset for AUTO-Pre-MC-OP-ILR3
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-MC-OP-ILR3-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Pre-MC-OP-ILR3-Asset1', 'AUTO-Pre-MC-OP-ILR3-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-MC-OP-ILR3';

------------------------------
--Add 1 Asset for AUTO-Arr-MC-OP-ILR3
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-MC-OP-ILR3-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Arr-MC-OP-ILR3-Asset1', 'AUTO-Arr-MC-OP-ILR3-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-MC-OP-ILR3';

------------------------------
--Add 1 Asset for AUTO-Arr-NonMC-OP-ILR4
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-NonMC-OP-ILR4-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Arr-NonMC-OP-ILR4-Asset1', 'AUTO-Arr-NonMC-OP-ILR4-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-NonMC-OP-ILR4';

------------------------------
--Add 1 Asset for AUTO-Arr-NonMC-OP-ILR2
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-NonMC-OP-ILR2-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Arr-NonMC-OP-ILR2-Asset1', 'AUTO-Arr-NonMC-OP-ILR2-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-NonMC-OP-ILR2';

------------------------------
--Add 1 Asset for AUTO-Pre-NonMC-OP-ILR4
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-NonMC-OP-ILR4-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Pre-NonMC-OP-ILR4-Asset1', 'AUTO-Pre-NonMC-OP-ILR4-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-NonMC-OP-ILR4';

------------------------------
--Add 1 Asset for AUTO-Pre-NonMC-OP-ILR3
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-NonMC-OP-ILR3-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Pre-NonMC-OP-ILR3-Asset1', 'AUTO-Pre-NonMC-OP-ILR3-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-NonMC-OP-ILR3';

--pjones new
------------------------------
--Add 1 Asset for AUTO-Pre-NonMC-OP-ILR3
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-MC-OP-ILR5-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Pre-NonMC-OP-ILR3-Asset1', 'AUTO-Arr-MC-OP-ILR5-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-MC-OP-ILR5';

------------------------------
--Add 1 Asset for AUTO-Pre-NonMC-OP-ILR3
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-MC-OP-ILR5-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Pre-MC-OP-ILR5-Asset1', 'AUTO-Pre-MC-OP-ILR5-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-MC-OP-ILR5';

------------------------------
--Add 1 Asset for AUTO-Pre-NonMC-OP-ILR3
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-MC-OP-ILR6-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Arr-MC-OP-ILR6-Asset1', 'AUTO-Arr-MC-OP-ILR6-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-MC-OP-ILR6';

------------------------------
--Add 1 Asset for AUTO-Pre-NonMC-OP-ILR3
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-MC-OP-ILR6-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Pre-MC-OP-ILR6-Asset1', 'AUTO-Pre-MC-OP-ILR6-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-MC-OP-ILR6';

------------------------------
--Add 1 Asset for AUTO-Pre-NonMC-OP-ILR3
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-MC-OP-ILR7-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Arr-MC-OP-ILR7-Asset1', 'AUTO-Arr-MC-OP-ILR7-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-MC-OP-ILR7';

------------------------------
--Add 1 Asset for AUTO-Pre-NonMC-OP-ILR3
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-MC-OP-ILR7-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Pre-MC-OP-ILR7-Asset1', 'AUTO-Pre-MC-OP-ILR7-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-MC-OP-ILR7';

------------------------------
--Add 1 Asset for AUTO-Pre-NonMC-OP-ILR3
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-MC-OP-ILR8-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Arr-MC-OP-ILR8-Asset1', 'AUTO-Arr-MC-OP-ILR8-Asset1', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-MC-OP-ILR8';

------------------------------
--Add 1 Asset for AUTO-Pre-NonMC-OP-ILR3
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-MC-OP-ILR8-Asset1', 0, 0, 0, 0, 0, 0,
0, 0, 'AUTO-Pre-MC-OP-ILR8', 'AUTO-Pre-MC-OP-ILR8', 0, 0, 0, 0, 0
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-MC-OP-ILR8';

--PJONES ADDING AUTO TERMINATIONS ASSETS--
------------------------------
--Add 1 Asset for AUTO-Arr-MC-OP-ILR9
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-MC-OP-ILR9-Asset1', 1, 0, 0, 0, 0/1/*estimated amount over FMV*/, 0,
30, 30, 'AUTO-Arr-MC-OP-ILR9-Asset1', 'AUTO-Arr-MC-OP-ILR9-Asset1', 1 / in_service_exchange_rate, 0 / in_service_exchange_rate, 0 / in_service_exchange_rate, 0, 0 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-MC-OP-ILR9';

------------------------------
--Add 1 Asset for AUTO-Arr-MC-ST-ILR9
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-MC-ST-ILR9-Asset1', 325000, 200000, 15000, 0, 20000/325000/*estimated amount over FMV*/, 5000,
30, 30, 'AUTO-Arr-MC-ST-ILR9-Asset1', 'AUTO-Arr-MC-ST-ILR9-Asset1', 325000 / in_service_exchange_rate, 200000 / in_service_exchange_rate, 15000 / in_service_exchange_rate, 0, 5000 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-MC-ST-ILR9';

------------------------------
--Add 1 Asset for AUTO-Arr-MC-DF-ILR9
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-MC-DF-ILR9-Asset1', 90000, 90000, 15000, 0, 20000/90000/*estimated amount over FMV*/, 5000,
30, 30, 'AUTO-Arr-MC-DF-ILR9-Asset1', 'AUTO-Arr-MC-DF-ILR9-Asset1', 90000 / in_service_exchange_rate, 90000 / in_service_exchange_rate, 15000 / in_service_exchange_rate, 0, 5000 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-MC-DF-ILR9';

------------------------------
--Add 1 Asset for AUTO-Pre-MC-OP-ILR9
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-MC-OP-ILR9-Asset1', 1, 0, 0, 0, 0/1/*estimated amount over FMV*/, 0,
30, 30, 'AUTO-Pre-MC-OP-ILR9-Asset1', 'AUTO-Pre-MC-OP-ILR9-Asset1', 1 / in_service_exchange_rate, 0 / in_service_exchange_rate, 0 / in_service_exchange_rate, 0, 0 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-MC-OP-ILR9';

------------------------------
--Add 1 Asset for AUTO-Pre-MC-ST-ILR9
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-MC-ST-ILR9-Asset1', 900000, 900000, 15000, 0, 20000/900000/*estimated amount over FMV*/, 5000,
30, 30, 'AUTO-Pre-MC-ST-ILR9-Asset1', 'AUTO-Pre-MC-ST-ILR9-Asset1', 900000 / in_service_exchange_rate, 900000 / in_service_exchange_rate, 15000 / in_service_exchange_rate, 0, 5000 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-MC-ST-ILR9';

------------------------------
--Add 1 Asset for AUTO-Pre-MC-DF-ILR9
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-MC-DF-ILR9-Asset1', 325000, 200000, 15000, 0, 20000/325000/*estimated amount over FMV*/, 5000,
30, 30, 'AUTO-Pre-MC-DF-ILR9-Asset1', 'AUTO-Pre-MC-DF-ILR9-Asset1', 325000 / in_service_exchange_rate, 200000 / in_service_exchange_rate, 15000 / in_service_exchange_rate, 0, 5000 / in_service_exchange_rate
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-MC-DF-ILR9';