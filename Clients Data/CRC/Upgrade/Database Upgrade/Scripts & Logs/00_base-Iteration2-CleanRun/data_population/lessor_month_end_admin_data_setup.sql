---------------
--Pre-MLA Data
---------------
--add the lease group for the MLAs
insert into lsr_lease_group(lease_group_id, description, long_description, workflow_type_id, send_invoices, requires_approval)
select ls_lease_group_seq.nextval, 'AUTO Month End LG1', 'Script-Added Asset Month End Lease Group 1', workflow_type_id, 0, 1
from workflow_type
where lower(description) like '%auto%approve%' and subsystem like '%lessor%mla%';

--add the lessee for the MLA
insert into lsr_lessee(lessee_id, description, long_description, status_code_id, lease_group_id)
select ls_lessor_seq.nextval,'AUTO Month End Lessee1','Script-Added Asset Month End Lessee 1', 1, lease_group_id
from lsr_lease_group
where description = 'AUTO Month End LG1';

--add the new cap types to use for the MLA/ILR
insert into lsr_cap_type(cap_type_id, description, long_description, active)
select nvl((select max(cap_type_id) from lsr_cap_type),0) + 1, 'AUTO MEC SalesType w/ DirectFinance', 'Script-Added Mixed Cap Type for testing Month End; SOB 3 is DF and all others are ST', 1 from dual;
insert into lsr_cap_type(cap_type_id, description, long_description, active)
select nvl((select max(cap_type_id) from lsr_cap_type),0) + 1, 'AUTO MEC DirectFinance w/ SalesType', 'Script-Added Mixed Cap Type for testing Month End; SOB 3 is ST and all others are DF', 1 from dual;
insert into lsr_cap_type(cap_type_id, description, long_description, active)
select nvl((select max(cap_type_id) from lsr_cap_type),0) + 1, 'AUTO MEC Sales Type', 'Script-Added Sales Type Cap Type for testing Month End', 1 from dual;
insert into lsr_cap_type(cap_type_id, description, long_description, active)
select nvl((select max(cap_type_id) from lsr_cap_type),0) + 1, 'AUTO MEC Direct Finance', 'Script-Added Direct Finance Cap Type for testing Month End', 1 from dual;
insert into lsr_cap_type(cap_type_id, description, long_description, active)
select nvl((select max(cap_type_id) from lsr_cap_type),0) + 1, 'AUTO MEC Operating', 'Script-Added Operating Cap Type for testing Month End', 1 from dual;

--add the FASB associations by SOB for the cap types
insert into lsr_fasb_type_sob(cap_type_id, fasb_cap_type_id, set_of_books_id)
select cap_type_id, fasb_cap_type_id, set_of_books_id
from lsr_cap_type, lsr_fasb_cap_type, set_of_books
where lsr_cap_type.description = 'AUTO MEC SalesType w/ DirectFinance'
and ((lsr_fasb_cap_type.description = 'Sales Type' and set_of_books.set_of_books_id not in (3))
or (lsr_fasb_cap_type.description = 'Direct Finance' and set_of_books.set_of_books_id in (3)));

insert into lsr_fasb_type_sob(cap_type_id, fasb_cap_type_id, set_of_books_id)
select cap_type_id, fasb_cap_type_id, set_of_books_id
from lsr_cap_type, lsr_fasb_cap_type, set_of_books
where lsr_cap_type.description = 'AUTO MEC DirectFinance w/ SalesType'
and ((lsr_fasb_cap_type.description = 'Sales Type' and set_of_books.set_of_books_id in (3))
or (lsr_fasb_cap_type.description = 'Direct Finance' and set_of_books.set_of_books_id not in (3)));

insert into lsr_fasb_type_sob(cap_type_id, fasb_cap_type_id, set_of_books_id)
select cap_type_id, fasb_cap_type_id, set_of_books_id
from lsr_cap_type, lsr_fasb_cap_type, set_of_books
where lsr_cap_type.description = 'AUTO MEC Sales Type'
and lsr_fasb_cap_type.description = 'Sales Type';

insert into lsr_fasb_type_sob(cap_type_id, fasb_cap_type_id, set_of_books_id)
select cap_type_id, fasb_cap_type_id, set_of_books_id
from lsr_cap_type, lsr_fasb_cap_type, set_of_books
where lsr_cap_type.description = 'AUTO MEC Direct Finance'
and lsr_fasb_cap_type.description = 'Direct Finance';

insert into lsr_fasb_type_sob(cap_type_id, fasb_cap_type_id, set_of_books_id)
select cap_type_id, fasb_cap_type_id, set_of_books_id
from lsr_cap_type, lsr_fasb_cap_type, set_of_books
where lsr_cap_type.description = 'AUTO MEC Operating'
and lsr_fasb_cap_type.description = 'Operating';

---------------
--Pre-ILR Data
---------------
--add the ILR group for the ILR
insert into lsr_ilr_group(ilr_group_id, description, long_description, workflow_type_id, payment_shift, int_accrual_account_id, int_expense_account_id,
exec_accrual_account_id, exec_expense_account_id, cont_accrual_account_id, cont_expense_account_id,
st_receivable_account_id, lt_receivable_account_id, ar_account_id,
unguaran_res_account_id, int_unguaran_res_account_id, sell_profit_loss_account_id,
ini_direct_cost_account_id, prop_plant_account_id, deferred_rent_acct_id,
accrued_rent_acct_id, curr_gain_loss_acct_id, curr_gain_loss_offset_acct_id, incurred_costs_account_id, def_costs_account_id, def_selling_profit_account_id)
select ls_ilr_group_seq.nextval, 'AUTO Month End ILRG1', 'Script-Added Month End ILR Group 1', workflow_type_id, 0, 43142000, 43142001,
43142004, 43142005, 43142006, 43142007,
43142003, 43142001, 43142006,
43142004, 43142005, 43142008,
43142007, 43142009, 43142002,
43142000, 43142008, 43142009,
43142007, 43142009, 43142002
from workflow_type
where lower(workflow_type.description) like '%auto%approve%'
and workflow_type.subsystem like '%lessor%ilr%';

--add the Initial Direct Cost Group for the ILRs
insert into lsr_initial_direct_cost_group(idc_group_id, description, long_description)
select nvl((select max(idc_group_id) from lsr_initial_direct_cost_group),0) + 1, 'AUTO Lessor Month End IDCG', 'Script-Added Initial Direct Cost Group for Lessor Month End testing' from dual;