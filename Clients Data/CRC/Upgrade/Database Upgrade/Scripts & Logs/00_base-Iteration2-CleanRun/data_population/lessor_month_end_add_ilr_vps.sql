--let me try to remember how VPs work

---------------------------------------
--Add Index/Rate Component Header Data
---------------------------------------
insert into lsr_formula_component(formula_component_id, description, long_description, component_type_id, number_format_id)
select ls_formula_component_seq.nextval, 'AUTO-IR1', 'AUTO-IR1 (Added by script)', 1, 1 from dual;

-------------------------------------
--Add Variable Component Header Data
-------------------------------------
--Annual Arrears
insert into lsr_formula_component(formula_component_id, description, long_description, component_type_id, number_format_id)
select ls_formula_component_seq.nextval, 'AUTO-VC1', 'AUTO-VC1 (Added by script)', 2, 1 from dual;
--Semi-Annual Prepay
insert into lsr_formula_component(formula_component_id, description, long_description, component_type_id, number_format_id)
select ls_formula_component_seq.nextval, 'AUTO-VC2', 'AUTO-VC2 (Added by script)', 2, 1 from dual;
--Monthly
insert into lsr_formula_component(formula_component_id, description, long_description, component_type_id, number_format_id)
select ls_formula_component_seq.nextval, 'AUTO-VC3', 'AUTO-VC3 (Added by script)', 2, 1 from dual;
--Annual Arrears
insert into lsr_formula_component(formula_component_id, description, long_description, component_type_id, number_format_id)
select ls_formula_component_seq.nextval, 'AUTO-VC4', 'AUTO-VC4 (Added by script)', 2, 1 from dual;

-------------------------------------
--Add Historic Component Header Data
-------------------------------------
--Quarterly Prepay
insert into lsr_formula_component(formula_component_id, description, long_description, component_type_id, number_format_id)
select ls_formula_component_seq.nextval, 'AUTO-HC1', 'AUTO-HC1 (Added by script)', 3, 1 from dual;
--Annual Arrears
insert into lsr_formula_component(formula_component_id, description, long_description, component_type_id, number_format_id)
select ls_formula_component_seq.nextval, 'AUTO-HC2', 'AUTO-HC2 (Added by script)', 3, 1 from dual;
--Monthly
insert into lsr_formula_component(formula_component_id, description, long_description, component_type_id, number_format_id)
select ls_formula_component_seq.nextval, 'AUTO-HC3', 'AUTO-HC3 (Added by script)', 3, 1 from dual;

-------------------------------------
--Add Variable Component Payment Calendars (Monthly)
-------------------------------------
insert into lsr_var_component_calendar(formula_component_id, load_month_number, payment_month_number)
select formula_component_id, 1, 1 from lsr_formula_component where description in ('AUTO-VC3') union
select formula_component_id, 2, 2 from lsr_formula_component where description in ('AUTO-VC3') union
select formula_component_id, 3, 3 from lsr_formula_component where description in ('AUTO-VC3') union
select formula_component_id, 4, 4 from lsr_formula_component where description in ('AUTO-VC3') union
select formula_component_id, 5, 5 from lsr_formula_component where description in ('AUTO-VC3') union
select formula_component_id, 6, 6 from lsr_formula_component where description in ('AUTO-VC3') union
select formula_component_id, 7, 7 from lsr_formula_component where description in ('AUTO-VC3') union
select formula_component_id, 8, 8 from lsr_formula_component where description in ('AUTO-VC3') union
select formula_component_id, 9, 9 from lsr_formula_component where description in ('AUTO-VC3') union
select formula_component_id, 10, 10 from lsr_formula_component where description in ('AUTO-VC3') union
select formula_component_id, 11, 11 from lsr_formula_component where description in ('AUTO-VC3') union
select formula_component_id, 12, 12 from lsr_formula_component where description in ('AUTO-VC3');

-------------------------------------
--Add Variable Component Payment Calendars (Semi-Annual/Prepay)
-------------------------------------
insert into lsr_var_component_calendar(formula_component_id, load_month_number, payment_month_number)
select formula_component_id, 1, 1 from lsr_formula_component where description in ('AUTO-VC2') union
select formula_component_id, 2, 7 from lsr_formula_component where description in ('AUTO-VC2') union
select formula_component_id, 3, 7 from lsr_formula_component where description in ('AUTO-VC2') union
select formula_component_id, 4, 7 from lsr_formula_component where description in ('AUTO-VC2') union
select formula_component_id, 5, 7 from lsr_formula_component where description in ('AUTO-VC2') union
select formula_component_id, 6, 7 from lsr_formula_component where description in ('AUTO-VC2') union
select formula_component_id, 7, 7 from lsr_formula_component where description in ('AUTO-VC2') union
select formula_component_id, 8, 1 from lsr_formula_component where description in ('AUTO-VC2') union
select formula_component_id, 9, 1 from lsr_formula_component where description in ('AUTO-VC2') union
select formula_component_id, 10, 1 from lsr_formula_component where description in ('AUTO-VC2') union
select formula_component_id, 11, 1 from lsr_formula_component where description in ('AUTO-VC2') union
select formula_component_id, 12, 1 from lsr_formula_component where description in ('AUTO-VC2');

-------------------------------------
--Add Variable Component Payment Calendars (Annual/Arrears)
-------------------------------------
insert into lsr_var_component_calendar(formula_component_id, load_month_number, payment_month_number)
select formula_component_id, 1, 12 from lsr_formula_component where description in ('AUTO-VC1','AUTO-VC4') union
select formula_component_id, 2, 12 from lsr_formula_component where description in ('AUTO-VC1','AUTO-VC4') union
select formula_component_id, 3, 12 from lsr_formula_component where description in ('AUTO-VC1','AUTO-VC4') union
select formula_component_id, 4, 12 from lsr_formula_component where description in ('AUTO-VC1','AUTO-VC4') union
select formula_component_id, 5, 12 from lsr_formula_component where description in ('AUTO-VC1','AUTO-VC4') union
select formula_component_id, 6, 12 from lsr_formula_component where description in ('AUTO-VC1','AUTO-VC4') union
select formula_component_id, 7, 12 from lsr_formula_component where description in ('AUTO-VC1','AUTO-VC4') union
select formula_component_id, 8, 12 from lsr_formula_component where description in ('AUTO-VC1','AUTO-VC4') union
select formula_component_id, 9, 12 from lsr_formula_component where description in ('AUTO-VC1','AUTO-VC4') union
select formula_component_id, 10, 12 from lsr_formula_component where description in ('AUTO-VC1','AUTO-VC4') union
select formula_component_id, 11, 12 from lsr_formula_component where description in ('AUTO-VC1','AUTO-VC4') union
select formula_component_id, 12, 12 from lsr_formula_component where description in ('AUTO-VC1','AUTO-VC4');

-------------------------------------
--Add Historic Component Payment Calendars (Monthly)
-------------------------------------
insert into lsr_hist_component_calendar (formula_component_id, calc_month_number, timing, payment_month_number)
select formula_component_id, 1, 1, 1 from lsr_formula_component where description in ('AUTO-HC3') union
select formula_component_id, 2, 1, 2 from lsr_formula_component where description in ('AUTO-HC3') union
select formula_component_id, 3, 1, 3 from lsr_formula_component where description in ('AUTO-HC3') union
select formula_component_id, 4, 1, 4 from lsr_formula_component where description in ('AUTO-HC3') union
select formula_component_id, 5, 1, 5 from lsr_formula_component where description in ('AUTO-HC3') union
select formula_component_id, 6, 1, 6 from lsr_formula_component where description in ('AUTO-HC3') union
select formula_component_id, 7, 1, 7 from lsr_formula_component where description in ('AUTO-HC3') union
select formula_component_id, 8, 1, 8 from lsr_formula_component where description in ('AUTO-HC3') union
select formula_component_id, 9, 1, 9 from lsr_formula_component where description in ('AUTO-HC3') union
select formula_component_id, 10, 1, 10 from lsr_formula_component where description in ('AUTO-HC3') union
select formula_component_id, 11, 1, 11 from lsr_formula_component where description in ('AUTO-HC3') union
select formula_component_id, 12, 1, 12 from lsr_formula_component where description in ('AUTO-HC3');

-------------------------------------
--Add Historic Component Payment Calendars (Quarterly/Prepay)
-------------------------------------
insert into lsr_hist_component_calendar(formula_component_id, calc_month_number, timing, payment_month_number)
select formula_component_id, 1, 1, 1 from lsr_formula_component where description in ('AUTO-HC1') union
select formula_component_id, 2, 1, 4 from lsr_formula_component where description in ('AUTO-HC1') union
select formula_component_id, 3, 1, 4 from lsr_formula_component where description in ('AUTO-HC1') union
select formula_component_id, 4, 1, 4 from lsr_formula_component where description in ('AUTO-HC1') union
select formula_component_id, 5, 1, 7 from lsr_formula_component where description in ('AUTO-HC1') union
select formula_component_id, 6, 1, 7 from lsr_formula_component where description in ('AUTO-HC1') union
select formula_component_id, 7, 1, 7 from lsr_formula_component where description in ('AUTO-HC1') union
select formula_component_id, 8, 1, 10 from lsr_formula_component where description in ('AUTO-HC1') union
select formula_component_id, 9, 1, 10 from lsr_formula_component where description in ('AUTO-HC1') union
select formula_component_id, 10, 1, 10 from lsr_formula_component where description in ('AUTO-HC1') union
select formula_component_id, 11, 1, 1 from lsr_formula_component where description in ('AUTO-HC1') union
select formula_component_id, 12, 1, 1 from lsr_formula_component where description in ('AUTO-HC1');

-------------------------------------
--Add Historic Component Payment Calendars (Annual/Arrears)
-------------------------------------
insert into lsr_hist_component_calendar(formula_component_id, calc_month_number, timing, payment_month_number)
select formula_component_id, 1, 1, 12 from lsr_formula_component where description in ('AUTO-HC2') union
select formula_component_id, 2, 1, 12 from lsr_formula_component where description in ('AUTO-HC2') union
select formula_component_id, 3, 1, 12 from lsr_formula_component where description in ('AUTO-HC2') union
select formula_component_id, 4, 1, 12 from lsr_formula_component where description in ('AUTO-HC2') union
select formula_component_id, 5, 1, 12 from lsr_formula_component where description in ('AUTO-HC2') union
select formula_component_id, 6, 1, 12 from lsr_formula_component where description in ('AUTO-HC2') union
select formula_component_id, 7, 1, 12 from lsr_formula_component where description in ('AUTO-HC2') union
select formula_component_id, 8, 1, 12 from lsr_formula_component where description in ('AUTO-HC2') union
select formula_component_id, 9, 1, 12 from lsr_formula_component where description in ('AUTO-HC2') union
select formula_component_id, 10, 1, 12 from lsr_formula_component where description in ('AUTO-HC2') union
select formula_component_id, 11, 1, 12 from lsr_formula_component where description in ('AUTO-HC2') union
select formula_component_id, 12, 1, 12 from lsr_formula_component where description in ('AUTO-HC2');

--------------------------------------------------------------
--Add Variable Payment Header Data (Formulas are added later)
--------------------------------------------------------------
--VP for the new Index/Rate Component we added
insert into lsr_variable_payment(variable_payment_id, description, long_description, active_for_new_ilrs, preview_option)
select ls_variable_payment_seq.nextval, 'AUTO-IR-VP1', 'AUTO-IR-VP1 (Added by script)', 1, -1 from dual;

--VP for the new Variable Components we added
insert into lsr_variable_payment(variable_payment_id, description, long_description, active_for_new_ilrs, preview_option)
select ls_variable_payment_seq.nextval, 'AUTO-VC-VP1', 'AUTO-VC-VP1 (Added by script)', 1, -1 from dual;
insert into lsr_variable_payment(variable_payment_id, description, long_description, active_for_new_ilrs, preview_option)
select ls_variable_payment_seq.nextval, 'AUTO-VC-VP2', 'AUTO-VC-VP2 (Added by script)', 1, -1 from dual;
insert into lsr_variable_payment(variable_payment_id, description, long_description, active_for_new_ilrs, preview_option)
select ls_variable_payment_seq.nextval, 'AUTO-VC-VP3', 'AUTO-VC-VP3 (Added by script)', 1, -1 from dual;
insert into lsr_variable_payment(variable_payment_id, description, long_description, active_for_new_ilrs, preview_option)
select ls_variable_payment_seq.nextval, 'AUTO-VC-VP4', 'AUTO-VC-VP4 (Added by script)', 1, -1 from dual;

--VP for the new Historic Components we added
insert into lsr_variable_payment(variable_payment_id, description, long_description, active_for_new_ilrs, preview_option)
select ls_variable_payment_seq.nextval, 'AUTO-HC-VP1', 'AUTO-HC-VP1 (Added by script)', 1, -1 from dual;
insert into lsr_variable_payment(variable_payment_id, description, long_description, active_for_new_ilrs, preview_option)
select ls_variable_payment_seq.nextval, 'AUTO-HC-VP2', 'AUTO-HC-VP2 (Added by script)', 1, -1 from dual;
insert into lsr_variable_payment(variable_payment_id, description, long_description, active_for_new_ilrs, preview_option)
select ls_variable_payment_seq.nextval, 'AUTO-HC-VP3', 'AUTO-HC-VP3 (Added by script)', 1, -1 from dual;

-------------------------------------
--Add Historic Component/Variable Payment Association
-------------------------------------
insert into lsr_historic_component_values(formula_component_id, variable_payment_id)
select formula_component_id, variable_payment_id from lsr_formula_component, lsr_variable_payment
where
   (lsr_formula_component.description = 'AUTO-HC1' and lsr_variable_payment.description = 'AUTO-IR-VP1')
or (lsr_formula_component.description = 'AUTO-HC2' and lsr_variable_payment.description = 'AUTO-VC-VP1')
or (lsr_formula_component.description = 'AUTO-HC3' and lsr_variable_payment.description = 'AUTO-VC-VP3');

------------------------------------------------------
--Add Variable Payment/Formula Component Associations
------------------------------------------------------
insert into lsr_variable_payment_component(variable_payment_id, formula_component_id)
select variable_payment_id, formula_component_id
from lsr_variable_payment, lsr_formula_component
where
   (lsr_variable_payment.description = 'AUTO-IR-VP1' and lsr_formula_component.description in ('AUTO-IR1'))
or (lsr_variable_payment.description = 'AUTO-VC-VP1' and lsr_formula_component.description in ('AUTO-VC1'))
or (lsr_variable_payment.description = 'AUTO-VC-VP2' and lsr_formula_component.description in ('AUTO-VC2'))
or (lsr_variable_payment.description = 'AUTO-VC-VP3' and lsr_formula_component.description in ('AUTO-VC3'))
or (lsr_variable_payment.description = 'AUTO-VC-VP4' and lsr_formula_component.description in ('AUTO-VC4'))
or (lsr_variable_payment.description = 'AUTO-HC-VP1' and lsr_formula_component.description in ('AUTO-HC1'))
or (lsr_variable_payment.description = 'AUTO-HC-VP2' and lsr_formula_component.description in ('AUTO-HC2'))
or (lsr_variable_payment.description = 'AUTO-HC-VP3' and lsr_formula_component.description in ('AUTO-HC3'));

-------------------------------------
--Add Variable Payment Formulas
-------------------------------------
--formulas for Index/Rate Component Variable Payments
update lsr_variable_payment set payment_formula = ' ''AUTO-IR1'' + 5 ', asset_level_formula = 0,
payment_formula_db = ' PKG_LESSOR_VAR_PAYMENTS.F_CALC_INDEX('|| (select formula_component_id from lsr_formula_component where description = 'AUTO-IR1') ||',lio.ilr_id,lio.revision,b.initial_amt_flag,a.month) + 5 '
where description = 'AUTO-IR-VP1';

--formulas for Variable Component Variable Payments
update lsr_variable_payment set payment_formula = ' ''AUTO-VC1'' + 5 ', asset_level_formula = 1,
payment_formula_db = ' PKG_LESSOR_VAR_PAYMENTS.F_CALC_VAR_COMPONENT('|| (select formula_component_id from lsr_formula_component where description = 'AUTO-VC1') ||',a.lsr_asset_id,a.month) + 5 '
where description = 'AUTO-VC-VP1';
update lsr_variable_payment set payment_formula = ' ''AUTO-VC2'' + 5 ', asset_level_formula = 1,
payment_formula_db = ' PKG_LESSOR_VAR_PAYMENTS.F_CALC_VAR_COMPONENT('|| (select formula_component_id from lsr_formula_component where description = 'AUTO-VC2') ||',a.lsr_asset_id,a.month) + 5 '
where description = 'AUTO-VC-VP2';
update lsr_variable_payment set payment_formula = ' ''AUTO-VC3'' + 5 ', asset_level_formula = 1,
payment_formula_db = ' PKG_LESSOR_VAR_PAYMENTS.F_CALC_VAR_COMPONENT('|| (select formula_component_id from lsr_formula_component where description = 'AUTO-VC3') ||',a.lsr_asset_id,a.month) + 5 '
where description = 'AUTO-VC-VP3';
update lsr_variable_payment set payment_formula = ' ''AUTO-VC4'' + 5 ', asset_level_formula = 1,
payment_formula_db = ' PKG_LESSOR_VAR_PAYMENTS.F_CALC_VAR_COMPONENT('|| (select formula_component_id from lsr_formula_component where description = 'AUTO-VC4') ||',a.lsr_asset_id,a.month) + 5 '
where description = 'AUTO-VC-VP4';

--formulas for Historic Component Variable Payments
update lsr_variable_payment set payment_formula = ' ''AUTO-HC1'' + 5 ', asset_level_formula = 0 /*based on IR Component formula*/,
payment_formula_db = ' PKG_LESSOR_VAR_PAYMENTS.F_CALC_HISTORIC('|| (select formula_component_id from lsr_formula_component where description = 'AUTO-HC1') ||',a.lsr_asset_id,a.revision,1,a.set_of_books_id,a.month) + 5 '
where description = 'AUTO-HC-VP1';
update lsr_variable_payment set payment_formula = ' ''AUTO-HC2'' + 5 ', asset_level_formula = 1 /*based on VC Component formula*/,
payment_formula_db = ' PKG_LESSOR_VAR_PAYMENTS.F_CALC_HISTORIC('|| (select formula_component_id from lsr_formula_component where description = 'AUTO-HC2') ||',a.lsr_asset_id,a.revision,1,a.set_of_books_id,a.month) + 5 '
where description = 'AUTO-HC-VP2';
update lsr_variable_payment set payment_formula = ' ''AUTO-HC3'' + 5 ', asset_level_formula = 1 /*based on VC Component formula*/,
payment_formula_db = ' PKG_LESSOR_VAR_PAYMENTS.F_CALC_HISTORIC('|| (select formula_component_id from lsr_formula_component where description = 'AUTO-HC3') ||',a.lsr_asset_id,a.revision,1,a.set_of_books_id,a.month) + 5 '
where description = 'AUTO-HC-VP3';

-------------------------------------
--Add ILR/Variable Payment Associations
-------------------------------------
--add associations for AUTO-Pre-NonMC-DF-ILR1
insert into lsr_ilr_payment_term_var_pay(ilr_id, revision, payment_term_id, receivable_type, bucket_number, variable_payment_id, incl_in_initial_measure, run_order)
select ilr_id, 1, 1, 'Contingent', 1, variable_payment_id, 1, 1
from lsr_ilr, lsr_variable_payment
where (lsr_ilr.ilr_number in ('AUTO-Pre-NonMC-DF-ILR1') and lsr_variable_payment.description = 'AUTO-IR-VP1');

--add associations for AUTO-Pre-NonMC-DF-ILR2
insert into lsr_ilr_payment_term_var_pay(ilr_id, revision, payment_term_id, receivable_type, bucket_number, variable_payment_id, incl_in_initial_measure, run_order)
select ilr_id, 1, 1, 'Contingent', 1, variable_payment_id, 0, 1
from lsr_ilr, lsr_variable_payment
where (lsr_ilr.ilr_number in ('AUTO-Pre-NonMC-DF-ILR2') and lsr_variable_payment.description = 'AUTO-VC-VP2');

--add associations for AUTO-Arr-MC-ST-ILR2
insert into lsr_ilr_payment_term_var_pay(ilr_id, revision, payment_term_id, receivable_type, bucket_number, variable_payment_id, incl_in_initial_measure, run_order)
select ilr_id, 1, 1, 'Contingent', 1, variable_payment_id, 0, 1
from lsr_ilr, lsr_variable_payment
where (lsr_ilr.ilr_number in ('AUTO-Arr-MC-ST-ILR2') and lsr_variable_payment.description = 'AUTO-VC-VP1')
union
select ilr_id, 1, 1, 'Contingent', 5, variable_payment_id, 0, 2
from lsr_ilr, lsr_variable_payment
where (lsr_ilr.ilr_number in ('AUTO-Arr-MC-ST-ILR2') and lsr_variable_payment.description = 'AUTO-HC-VP2');

--add associations for AUTO-Pre-NonMC-ST-ILR1
insert into lsr_ilr_payment_term_var_pay(ilr_id, revision, payment_term_id, receivable_type, bucket_number, variable_payment_id, incl_in_initial_measure, run_order)
select ilr_id, 1, 1, 'Contingent', 1, variable_payment_id, 1, 1
from lsr_ilr, lsr_variable_payment
where (lsr_ilr.ilr_number in ('AUTO-Pre-NonMC-ST-ILR1') and lsr_variable_payment.description = 'AUTO-IR-VP1');

--add associations for AUTO-Pre-NonMC-OP-ILR1
insert into lsr_ilr_payment_term_var_pay(ilr_id, revision, payment_term_id, receivable_type, bucket_number, variable_payment_id, incl_in_initial_measure, run_order)
select ilr_id, 1, 1, 'Contingent', 1, variable_payment_id, 0, 1
from lsr_ilr, lsr_variable_payment
where (lsr_ilr.ilr_number in ('AUTO-Pre-NonMC-OP-ILR1') and lsr_variable_payment.description = 'AUTO-VC-VP3')
union
select ilr_id, 1, 1, 'Contingent', 5, variable_payment_id, 0, 2
from lsr_ilr, lsr_variable_payment
where (lsr_ilr.ilr_number in ('AUTO-Pre-NonMC-OP-ILR1') and lsr_variable_payment.description = 'AUTO-HC-VP3');

--add associations for AUTO-Arr-MC-DF-CPR-ILR1
insert into lsr_ilr_payment_term_var_pay(ilr_id, revision, payment_term_id, receivable_type, bucket_number, variable_payment_id, incl_in_initial_measure, run_order)
select ilr_id, 1, 1, 'Contingent', 1, variable_payment_id, 0, 1
from lsr_ilr, lsr_variable_payment
where (lsr_ilr.ilr_number in ('AUTO-Arr-MC-DF-CPR-ILR1') and lsr_variable_payment.description = 'AUTO-IR-VP1');

--add associations for AUTO-Arr-MC-ST-CPR-ILR2
insert into lsr_ilr_payment_term_var_pay(ilr_id, revision, payment_term_id, receivable_type, bucket_number, variable_payment_id, incl_in_initial_measure, run_order)
select ilr_id, 1, 1, 'Contingent', 1, variable_payment_id, 0, 1
from lsr_ilr, lsr_variable_payment
where (lsr_ilr.ilr_number in ('AUTO-Arr-MC-ST-CPR-ILR2') and lsr_variable_payment.description = 'AUTO-VC-VP4');

--add associations for AUTO-Pre-NonMC-ST-CPR-ILR1
insert into lsr_ilr_payment_term_var_pay(ilr_id, revision, payment_term_id, receivable_type, bucket_number, variable_payment_id, incl_in_initial_measure, run_order)
select ilr_id, 1, 1, 'Contingent', 1, variable_payment_id, 1, 1
from lsr_ilr, lsr_variable_payment
where (lsr_ilr.ilr_number in ('AUTO-Pre-NonMC-ST-CPR-ILR1') and lsr_variable_payment.description = 'AUTO-IR-VP1');

-------------------------------------
--Add Index/Rate Component Values
-------------------------------------
insert into lsr_index_component_values(formula_component_id, effective_date, amount)
select formula_component_id, month, amount
from lsr_formula_component,
(
select to_char(add_months(open_month,0), 'dd-mon-yyyy') month, 5 amount
	from (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null)
union
select to_char(add_months(open_month,1), 'dd-mon-yyyy') month, 10 amount
	from (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null)
union
select to_char(add_months(open_month,2), 'dd-mon-yyyy') month, 15 amount
	from (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null))
where lsr_formula_component.description = 'AUTO-IR1';

-------------------------------------
--Add Variable Component Values
-------------------------------------
insert into lsr_variable_component_values(formula_component_id, ilr_id, asset_id, incurred_month_number, amount)
select x.formula_component_id, ilr_id, lsr_asset_id, x.month, x.amount
from
(
	select formula_component_id, 5 amount, to_char(add_months(open_month,2), 'yyyymm') month, ilr_id, lsr_asset_id
	from lsr_asset, lsr_formula_component, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null)
	where lsr_formula_component.description in ('AUTO-VC1') and lsr_asset.description in ('AUTO-Arr-MC-ST-ILR1-Asset1')
union
	select formula_component_id, 5 amount, to_char(add_months(open_month,0), 'yyyymm') month, ilr_id, lsr_asset_id
	from lsr_asset, lsr_formula_component, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null)
	where lsr_formula_component.description in ('AUTO-VC2') and lsr_asset.description in ('AUTO-Pre-NonMC-DF-ILR2-Asset1')
union
	select formula_component_id, 5 amount, to_char(add_months(open_month,0), 'yyyymm') month, ilr_id, lsr_asset_id
	from lsr_asset, lsr_formula_component, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null)
	where lsr_formula_component.description in ('AUTO-VC3') and lsr_asset.description in ('AUTO-Pre-NonMC-OP-ILR1-Asset1')
union
	select formula_component_id, 10 amount, to_char(add_months(open_month,1), 'yyyymm') month, ilr_id, lsr_asset_id
	from lsr_asset, lsr_formula_component, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null)
	where lsr_formula_component.description in ('AUTO-VC3') and lsr_asset.description in ('AUTO-Pre-NonMC-OP-ILR1-Asset1')
union
	select formula_component_id, 15 amount, to_char(add_months(open_month,2), 'yyyymm') month, ilr_id, lsr_asset_id
	from lsr_asset, lsr_formula_component, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null)
	where lsr_formula_component.description in ('AUTO-VC3') and lsr_asset.description in ('AUTO-Pre-NonMC-OP-ILR1-Asset1')
) x;