------------------------------
--Lessor Asset Data (All Non-CPR Assets)
--Since this adds multiple assets for a large amount of ILRs
--	the additions will be grouped by the ILR receiving the asset records
------------------------------

------------------------------
--Add 1 Asset for AUTO-Arr-In-Service-ILR-ST1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-In-Service-Asset-ST1', 300000 * in_service_exchange_rate, 200000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 20000/300000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Arr-In-Service-Asset-ST1', 'AUTO-Arr-In-Service-Asset-ST1', 300000, 200000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-In-Service-ILR-ST1';

------------------------------
--Add 1 Asset for AUTO-Arr-In-Service-ILR-ST2
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-In-Service-Asset-ST2', 200000 * in_service_exchange_rate, 300000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 20000/200000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Arr-In-Service-Asset-ST2', 'AUTO-Arr-In-Service-Asset-ST2', 200000, 300000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-In-Service-ILR-ST2';

------------------------------
--Add 1 Asset for AUTO-Arr-In-Service-ILR-ST3
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-In-Service-Asset-ST3', 200000 * in_service_exchange_rate, 200000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 20000/200000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Arr-In-Service-Asset-ST3', 'AUTO-Arr-In-Service-Asset-ST3', 200000, 200000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-In-Service-ILR-ST3';

------------------------------
--Add 1 Asset for AUTO-Arr-In-Service-ILR-ST4
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-In-Service-Asset-ST4', 200000 * in_service_exchange_rate, 200000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 20000/200000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Arr-In-Service-Asset-ST4', 'AUTO-Arr-In-Service-Asset-ST4', 200000, 200000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-In-Service-ILR-ST4';

------------------------------
--Add 1 Asset for AUTO-Pre-In-Service-ILR-ST1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-In-Service-Asset-ST1', 300000 * in_service_exchange_rate, 200000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 20000/300000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Pre-In-Service-Asset-ST1', 'AUTO-Pre-In-Service-Asset-ST1', 300000, 200000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-In-Service-ILR-ST1';

------------------------------
--Add 1 Asset for AUTO-Pre-In-Service-ILR-ST2
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-In-Service-Asset-ST2', 200000 * in_service_exchange_rate, 300000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 20000/200000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Pre-In-Service-Asset-ST2', 'AUTO-Pre-In-Service-Asset-ST2', 200000, 300000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-In-Service-ILR-ST2';

------------------------------
--Add 1 Asset for AUTO-Pre-In-Service-ILR-ST3
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-In-Service-Asset-ST3', 200000 * in_service_exchange_rate, 200000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 20000/200000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Pre-In-Service-Asset-ST3', 'AUTO-Pre-In-Service-Asset-ST3', 200000, 200000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-In-Service-ILR-ST3';

------------------------------
--Add 1 Asset for AUTO-Pre-In-Service-ILR-ST4
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-In-Service-Asset-ST4', 200000 * in_service_exchange_rate, 200000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 20000/200000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Pre-In-Service-Asset-ST4', 'AUTO-Pre-In-Service-Asset-ST4', 200000, 200000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-In-Service-ILR-ST4';

------------------------------
--Add 1 Asset for AUTO-Arr-In-Service-ILR-DF1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-In-Service-Asset-DF1', 300000 * in_service_exchange_rate, 200000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 20000/300000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Arr-In-Service-Asset-DF1', 'AUTO-Arr-In-Service-Asset-DF1', 300000, 200000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-In-Service-ILR-DF1';

------------------------------
--Add 1 Asset for AUTO-Arr-In-Service-ILR-DF2
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-In-Service-Asset-DF2', 200000 * in_service_exchange_rate, 300000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 15000/200000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Arr-In-Service-Asset-DF2', 'AUTO-Arr-In-Service-Asset-DF2', 200000, 300000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-In-Service-ILR-DF2';

------------------------------
--Add 1 Asset for AUTO-Arr-In-Service-ILR-DF3
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-In-Service-Asset-DF3', 200000 * in_service_exchange_rate, 200000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 20000/200000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Arr-In-Service-Asset-DF3', 'AUTO-Arr-In-Service-Asset-DF3', 200000, 200000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-In-Service-ILR-DF3';

------------------------------
--Add 1 Asset for AUTO-Arr-In-Service-ILR-DF4
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Arr-In-Service-Asset-DF4', 200000 * in_service_exchange_rate, 200000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 20000/200000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Arr-In-Service-Asset-DF4', 'AUTO-Arr-In-Service-Asset-DF4', 200000, 200000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Arr-In-Service-ILR-DF4';

------------------------------
--Add 1 Asset for AUTO-Pre-In-Service-ILR-DF1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-In-Service-Asset-DF1', 300000 * in_service_exchange_rate, 200000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 20000/300000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Pre-In-Service-Asset-DF1', 'AUTO-Pre-In-Service-Asset-DF1', 300000, 200000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-In-Service-ILR-DF1';

------------------------------
--Add 1 Asset for AUTO-Pre-In-Service-ILR-DF1
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-In-Service-Asset-DF2', 200000 * in_service_exchange_rate, 300000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 20000/200000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Pre-In-Service-Asset-DF2', 'AUTO-Pre-In-Service-Asset-DF2', 200000, 300000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-In-Service-ILR-DF2';

------------------------------
--Add 1 Asset for AUTO-Pre-In-Service-ILR-DF3
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-In-Service-Asset-DF3', 200000 * in_service_exchange_rate, 200000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 20000/200000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Pre-In-Service-Asset-DF3', 'AUTO-Pre-In-Service-Asset-DF3', 200000, 200000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-In-Service-ILR-DF3';

------------------------------
--Add 1 Asset for AUTO-Pre-In-Service-ILR-DF4
------------------------------
insert into lsr_asset(lsr_asset_id, ilr_id, revision, description, fair_market_value, carrying_cost, guaranteed_residual_amount, actual_residual_amount, estimated_residual_pct, unguaranteed_residual_amount,
expected_life, economic_life, serial_number, long_description, fair_market_value_comp_curr, carrying_cost_comp_curr, guaranteed_res_amt_comp_curr, actual_residual_amt_comp_curr, unguaranteed_res_amt_comp_curr)
select ls_asset_seq.nextval, lsr_ilr.ilr_id, 1, 'AUTO-Pre-In-Service-Asset-DF4', 200000 * in_service_exchange_rate, 200000 * in_service_exchange_rate, 15000 * in_service_exchange_rate, 0, 20000/200000/*estimated amount over FMV*/, 5000 * in_service_exchange_rate,
30, 30, 'AUTO-Pre-In-Service-Asset-DF4', 'AUTO-Pre-In-Service-Asset-DF4', 200000, 200000, 15000, 0, 5000
from lsr_ilr
inner join lsr_ilr_options
on lsr_ilr.ilr_id = lsr_ilr_options.ilr_id
and lsr_ilr_options.revision = 1
where lsr_ilr.ilr_number = 'AUTO-Pre-In-Service-ILR-DF4';