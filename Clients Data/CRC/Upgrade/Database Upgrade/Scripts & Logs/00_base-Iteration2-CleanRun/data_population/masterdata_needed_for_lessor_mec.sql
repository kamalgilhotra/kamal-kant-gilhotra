--this script is the stuff that is confirmed necessary for the Lessor Month End data population scripts to run

--need to add the workflow types here to be used in the various groups/MLAs/ILRs
insert into workflow_type(workflow_type_id, description, subsystem, active, use_limits,
sql_approval_amount, external_workflow_type, base_sql)
values(nvl((select max(workflow_type_id) from workflow_type),0) + 1, 'Auto approve LSR', 'lessor_ilr_approval,lessor_mla_approval', 1, 1,
'select nvl(sum(appr_amount),0) appr_amount from (
select nvl(max(beg_receivable),0) appr_amount from LSR_ILR_SCHEDULE
where ilr_id = <<id_field1>> and revision = <<id_field2>>
and month = (select min(month) from lsr_ilr_schedule
where ilr_id = <<id_field1>> and revision = <<id_field2>>)
union all
select 0 appr_amount from dual
)', 'AUTO', 1);

insert into workflow_type(workflow_type_id, description, subsystem, active, use_limits,
sql_approval_amount, external_workflow_type, base_sql)
values(nvl((select max(workflow_type_id) from workflow_type),0) + 1, 'Auto approve LSR Invoice', 'lsr_invoice_approval', 1, 1,
'select nvl(sum(appr_amount),0) appr_amount from (
select nvl(sum(nvl(invoice_principal,0)+nvl(invoice_interest,0)+nvl(invoice_executory,0)+nvl(invoice_contingent,0)+nvl(invoice_recognized_profit,0)),0) appr_amount from lsr_invoice where invoice_id = <<id_field1>>
)', 'AUTO', 1);

insert into workflow_type_rule(workflow_type_id, workflow_rule_id, rule_order)
values((select workflow_type_id from workflow_type where description = 'Auto approve LSR'), (select workflow_rule_id from workflow_rule where lower(description) = 'auto'), 1);

insert into workflow_type_rule(workflow_type_id, workflow_rule_id, rule_order)
values((select workflow_type_id from workflow_type where description = 'Auto approve LSR Invoice'), (select workflow_rule_id from workflow_rule where lower(description) = 'auto'), 1);

--need to add the book summary to be used by cap types
insert into book_summary(book_summary_id, summary_name,
book_summary_type, currency_type_id, cpi_recalc_alloc_basis)
values(1, 'Material', 'Standard', 1, 1);

--add additional sets of books
--also should probably update the default one since every basis indicator is 0
insert into set_of_books(set_of_books_id, description)
select 2,'Second Set of Books' from dual
union all
select 3,'Third Set of Books' from dual;

--add the GL Accounts
insert into gl_account(gl_account_id, account_type_id,
description, status_code_id)
select 43142000, 11, 'GL account 43142000', 1 from dual
union all
select 43142001, 11, 'GL account 43142001', 1 from dual
union all
select 43142002, 11, 'GL account 43142002', 1 from dual
union all
select 43142003, 11, 'GL account 43142003', 1 from dual
union all
select 43142004, 11, 'GL account 43142004', 1 from dual
union all
select 43142005, 11, 'GL account 43142005', 1 from dual
union all
select 43142006, 11, 'GL account 43142006', 1 from dual
union all
select 43142007, 11, 'GL account 43142007', 1 from dual
union all
select 43142008, 11, 'GL account 43142008', 1 from dual
union all
select 43142009, 11, 'GL account 43142009', 1 from dual;

--add companies
insert into company_setup(company_id, gl_company_no, owned, description,
status_code_id,
auto_life_month_monthly, is_lease_company)
values(1, 'Lessor MEC Company', 1, 'Lessor MEC Company',
2,
'1,2,3,4,5,6,7,8,9,10,11,12', 1);

--add the rows to the Lease MEC Process Control tables so we can grab the open month
insert into lsr_process_control(company_id, gl_posting_mo_yr)
values((select company_id from company_setup where description = 'Lessor MEC Company'), to_char(add_months(sysdate,-2),'dd-mon-yyyy'));
insert into ls_process_control(company_id, gl_posting_mo_yr)
values((select company_id from company_setup where description = 'Lessor MEC Company'), to_char(add_months(sysdate,-2),'dd-mon-yyyy'));

--add the company currency for the new company
insert into currency_schema(currency_schema_id, company_id, currency_type_id, currency_id)
values(2, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), 1, 1);

--add some currency rates (need to default in more later) so we can add the ILR options with the exchange rate
insert into currency_rate(currency_from, currency_to, exchange_rate_type_id, rate, exchange_date)
values(4, 1, 1, 2, to_char(add_months(sysdate,-72),'dd-mon-yyyy'));
insert into currency_rate(currency_from, currency_to, exchange_rate_type_id, rate, exchange_date)
values(1, 4, 1, .5, to_char(add_months(sysdate,-72),'dd-mon-yyyy'));
insert into currency_rate(currency_from, currency_to, exchange_rate_type_id, rate, exchange_date)
values(4, 1, 1, 2, to_char(add_months(sysdate,0),'dd-mon-yyyy'));
insert into currency_rate(currency_from, currency_to, exchange_rate_type_id, rate, exchange_date)
values(1, 4, 1, .5, to_char(add_months(sysdate,0),'dd-mon-yyyy'));

--create the contingent buckets we need for VPs (buckets 1 and 5)
insert into lsr_receivable_bucket_admin(receivable_type, bucket_number, bucket_name, status_code_id, accrual_acct_id, receivable_acct_id)
select 'Contingent', 1, 'Contingent Bucket 1', 1, 43142000, 43142001 from dual
union all
select 'Contingent', 5, 'Contingent Bucket 5', 1, 43142008, 43142009 from dual;

insert into company_gl_account(gl_account_id, company_id)
select account_id, company_id
from company_setup, (select gl_account_id account_id from gl_account)
where description = 'Lessor MEC Company';