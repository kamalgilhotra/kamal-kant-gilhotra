--set all this stuff before we start echoing since we don't need to see it

--SET TIME ON

--shows the DB you are working in; may or may not care about this
SET SQLPROMPT '_CONNECT_IDENTIFIER> '

/* Set PP_SCRIPT_PATH = to the path of the update scripts. */
/* If you are running sqlplus from the script directory    */
/* then there is no need to set PP_SCRIPT_PATH.            */
/* Example: DEFINE PP_SCRIPT_PATH='C:/temp/scripts/'       */
DEFINE PP_SCRIPT_PATH='C:/PlasticWks/Powerplant/sql/data_population/'

--SPOOL takes the output of the script running and pushes it into the specified file
--the && is used to grab a defined Macro from earlier in the script
--the @ calls another script

/*
* NOTE: The following statement will exit SQLPlus if any ORA- errors occur.
*       If SQLPlus exists then review last Log file, resolve issues and
*       continue running the rest of the scripts after the error location.
*
*       Certain scripts may turn on and off the SQLERROR so look below for
*       comments about specific scripts.
*/
WHENEVER SQLERROR exit failure rollback

SET ECHO ON

--Lessor Month End Admin Data Setup 
SPOOL &&PP_SCRIPT_PATH.lessor_month_end_merge_rates_mass.log
@&&PP_SCRIPT_PATH.lessor_month_end_merge_rates_mass.sql
SPOOL OFF

--Lessor Month End Admin Data Setup 
SPOOL &&PP_SCRIPT_PATH.lessor_month_end_admin_data_setup.log
@&&PP_SCRIPT_PATH.lessor_month_end_admin_data_setup.sql
SPOOL OFF

--Lessor Month End Add MLAs (includes approvals and options) 
SPOOL &&PP_SCRIPT_PATH.lessor_month_end_add_mlas.log
@&&PP_SCRIPT_PATH.lessor_month_end_add_mlas.sql
SPOOL OFF

--Lessor Month End Add ILRs (includes options, payment terms, accounts, approval records, and initial direct costs) 
SPOOL &&PP_SCRIPT_PATH.lessor_month_end_add_non_cpr_ilrs.log
@&&PP_SCRIPT_PATH.lessor_month_end_add_non_cpr_ilrs.sql
SPOOL OFF
SPOOL &&PP_SCRIPT_PATH.lessor_month_end_add_cpr_ilrs.log
@&&PP_SCRIPT_PATH.lessor_month_end_add_cpr_ilrs.sql
SPOOL OFF

--Lessor Month End Add ILR Assets
SPOOL &&PP_SCRIPT_PATH.lessor_month_end_add_non_cpr_ilr_assets.log
@&&PP_SCRIPT_PATH.lessor_month_end_add_non_cpr_ilr_assets.sql
SPOOL OFF

--Lessor Month End Add Variable Payments
SPOOL &&PP_SCRIPT_PATH.lessor_month_end_add_ilr_vps.log
@&&PP_SCRIPT_PATH.lessor_month_end_add_ilr_vps.sql
SPOOL OFF

--commit;
rollback;