-------------------------------------------
--Variable ILR Header Table Data (Sales-Type/Arrears)
-------------------------------------------
 insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-In-Service-ILR-ST1', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0 /*no idea what this is*/
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%auto%approve%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Arr In-Service MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-In-Service-ILR-ST2', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0  /*no idea what this is*/
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%ilr%approval%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Arr In-Service MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';  

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-In-Service-ILR-ST3', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%auto%approve%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Arr In-Service MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-In-Service-ILR-ST4', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%auto%approve%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Arr In-Service MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';

-------------------------------------------
--Variable ILR Header Table Data (Sales-Type/Prepay)
-------------------------------------------
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-In-Service-ILR-ST1', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%auto%approve%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Pre In-Service MLA2'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-In-Service-ILR-ST2', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%ilr%approval%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Pre In-Service MLA2'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-In-Service-ILR-ST3', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%auto%approve%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Pre In-Service MLA2'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-In-Service-ILR-ST4', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%auto%approve%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Pre In-Service MLA2'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';

-------------------------------------------
--Variable ILR Header Table Data (Direct-Finance/Arrears)
-------------------------------------------
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-In-Service-ILR-DF1', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%auto%approve%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Arr In-Service MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-In-Service-ILR-DF2', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%ilr%approval%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Arr In-Service MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-In-Service-ILR-DF3', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%auto%approve%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Arr In-Service MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-In-Service-ILR-DF4', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%auto%approve%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Arr In-Service MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';

-------------------------------------------
--Variable ILR Header Table Data (Direct-Finance/Prepay)
-------------------------------------------
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-In-Service-ILR-DF1', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%auto%approve%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Pre In-Service MLA2'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-In-Service-ILR-DF2', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%ilr%approval%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Pre In-Service MLA2'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-In-Service-ILR-DF3', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%auto%approve%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Pre In-Service MLA2'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-In-Service-ILR-DF4', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) /*may want to make this more dynamic in the future*/, to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type 
  where lower(workflow_type.description) like '%auto%approve%' 
  and workflow_type.subsystem like '%lessor%'
) x,(select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO Pre In-Service MLA2'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO In-Service ILRG1';

-----------------------
--ILR Approvals Data 
---------------------
--leave as Initiated so we can build schedule
insert into lsr_ilr_approval(ilr_id, revision, approval_type_id, approval_status_id)
select ilr_id, 1, lsr_ilr.workflow_type_id, ls_ilr_status.ilr_status_id
from lsr_ilr, ls_ilr_status
where lsr_ilr.ilr_number in (
'AUTO-Arr-In-Service-ILR-ST1','AUTO-Arr-In-Service-ILR-ST2', 'AUTO-Arr-In-Service-ILR-ST3','AUTO-Arr-In-Service-ILR-ST4',
'AUTO-Pre-In-Service-ILR-ST1','AUTO-Pre-In-Service-ILR-ST2','AUTO-Pre-In-Service-ILR-ST3','AUTO-Pre-In-Service-ILR-ST4',
'AUTO-Arr-In-Service-ILR-DF1','AUTO-Arr-In-Service-ILR-DF2', 'AUTO-Arr-In-Service-ILR-DF3','AUTO-Arr-In-Service-ILR-DF4',
'AUTO-Pre-In-Service-ILR-DF1','AUTO-Pre-In-Service-ILR-DF2','AUTO-Pre-In-Service-ILR-DF3','AUTO-Pre-In-Service-ILR-DF4')
and ls_ilr_status.description = 'Initiated';

-------------------------
--ILR Options Table Data
-------------------------
--add the other options for the ILRs we are adding
insert into lsr_ilr_options(ilr_id, revision, purchase_option_type_id, purchase_option_amt, cancelable_type_id, itc_sw, partial_retire_sw, sublet_sw, lease_cap_type_id,
termination_amt, payment_shift, in_service_exchange_rate, intent_to_purchase, specialized_asset, likely_to_collect, sublease_flag, interco_lease_flag)
select ilr_id, 1, purchase_option_type_id, 0, cancelable_type_id, 0, 0, 0, cap_type_id,
0, 0, 1/rate, 1, 1, 1, 0, 0
from lsr_ilr, ls_purchase_option_type, ls_cancelable_type, lsr_cap_type, currency_rate_default_dense
where ls_purchase_option_type.description = 'None'
and ls_cancelable_type.description = 'Cancelable with Penalty'
and ((lsr_ilr.ilr_number in (
		'AUTO-Arr-In-Service-ILR-ST1','AUTO-Arr-In-Service-ILR-ST2', 'AUTO-Arr-In-Service-ILR-ST3','AUTO-Arr-In-Service-ILR-ST4',
		'AUTO-Pre-In-Service-ILR-ST1','AUTO-Pre-In-Service-ILR-ST2','AUTO-Pre-In-Service-ILR-ST3','AUTO-Pre-In-Service-ILR-ST4') 
	and lsr_cap_type.description = 'AUTO In-Service Sales Cap Type') 
	or (lsr_ilr.ilr_number in (
		'AUTO-Arr-In-Service-ILR-DF1','AUTO-Arr-In-Service-ILR-DF2', 'AUTO-Arr-In-Service-ILR-DF3','AUTO-Arr-In-Service-ILR-DF4',
		'AUTO-Pre-In-Service-ILR-DF1','AUTO-Pre-In-Service-ILR-DF2','AUTO-Pre-In-Service-ILR-DF3','AUTO-Pre-In-Service-ILR-DF4')
	and lsr_cap_type.description = 'AUTO In-Service Direct Cap Type'))
and add_months(currency_rate_default_dense.exchange_date, 1) > lsr_ilr.est_in_svc_date
and currency_rate_default_dense.exchange_date <= lsr_ilr.est_in_svc_date
and currency_rate_default_dense.currency_from = (select currency_id from currency where iso_code = 'GBP')
and currency_rate_default_dense.currency_to = (select currency_id from currency_schema where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and currency_type_id = 1)
and currency_rate_default_dense.exchange_rate_type_id = 1;

--------------------------------------------
--ILR Accounts (Default from the ILR Group)
--------------------------------------------
insert into lsr_ilr_account(ilr_id, int_accrual_account_id, int_expense_account_id, exec_accrual_account_id, exec_expense_account_id, cont_accrual_account_id, 
cont_expense_account_id, st_receivable_account_id, lt_receivable_account_id, ar_account_id, unguaran_res_account_id, int_unguaran_res_account_id, sell_profit_loss_account_id, ini_direct_cost_account_id, prop_plant_account_id, deferred_rent_acct_id, accrued_rent_acct_id, curr_gain_loss_acct_id, curr_gain_loss_offset_acct_id, INCURRED_COSTS_ACCOUNT_ID, DEF_COSTS_ACCOUNT_ID, DEF_SELLING_PROFIT_ACCOUNT_ID)
select ilr_id, int_accrual_account_id, int_expense_account_id, exec_accrual_account_id, exec_expense_account_id, cont_accrual_account_id, cont_expense_account_id, st_receivable_account_id, lt_receivable_account_id, ar_account_id, unguaran_res_account_id, int_unguaran_res_account_id, sell_profit_loss_account_id, ini_direct_cost_account_id, prop_plant_account_id, deferred_rent_acct_id, accrued_rent_acct_id, curr_gain_loss_acct_id, curr_gain_loss_offset_acct_id, INCURRED_COSTS_ACCOUNT_ID, DEF_COSTS_ACCOUNT_ID, DEF_SELLING_PROFIT_ACCOUNT_ID
from lsr_ilr a
inner join lsr_ilr_group b
on a.ilr_group_id = b.ilr_group_id
where ilr_id in (select ilr_id from lsr_ilr where ilr_number in (
'AUTO-Arr-In-Service-ILR-ST1','AUTO-Arr-In-Service-ILR-ST2','AUTO-Arr-In-Service-ILR-ST3','AUTO-Arr-In-Service-ILR-ST4',
'AUTO-Pre-In-Service-ILR-ST1','AUTO-Pre-In-Service-ILR-ST2','AUTO-Pre-In-Service-ILR-ST3','AUTO-Pre-In-Service-ILR-ST4',
'AUTO-Arr-In-Service-ILR-DF1','AUTO-Arr-In-Service-ILR-DF2','AUTO-Arr-In-Service-ILR-DF3','AUTO-Arr-In-Service-ILR-DF4',
'AUTO-Pre-In-Service-ILR-DF1','AUTO-Pre-In-Service-ILR-DF2','AUTO-Pre-In-Service-ILR-DF3','AUTO-Pre-In-Service-ILR-DF4'));

------------------------------
--ILR Payment Terms (Monthly)
------------------------------
--because of how widely varied the numbers are, this insert would be unwieldy if we tried to insert everything up-front
--so instead we insert all the common data and then update the varying values afterwards
insert into lsr_ilr_payment_term(ilr_id, revision, payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms, payment_amount, contingent_amount,
c_bucket_1, c_bucket_2, c_bucket_3, c_bucket_4, c_bucket_5, c_bucket_6, c_bucket_7, c_bucket_8, c_bucket_9, c_bucket_10,
e_bucket_1, e_bucket_2, e_bucket_3, e_bucket_4, e_bucket_5, e_bucket_6, e_bucket_7, e_bucket_8, e_bucket_9, e_bucket_10)
select x.ilr_id, 1, 1, payment_term_type_id, x.payment_start_date, x.payment_freq_id, x.num_terms, x.pymt_amt, 1 cont_amt,
1 cont_amt1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
1 exec_amt1, 0, 0, 0, 0, 0, 0, 0, 0, 0
from ls_payment_term_type,
(
	select ilr_id, to_char(add_months(y.open_month,0), 'dd-mon-yyyy') payment_start_date, 4500 pymt_amt, payment_freq_id, 72 num_terms
	from lsr_ilr, ls_payment_freq, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-In-Service-ILR-ST1','AUTO-Pre-In-Service-ILR-ST1','AUTO-Arr-In-Service-ILR-DF1','AUTO-Pre-In-Service-ILR-DF1')
	and ls_payment_freq.description = 'Monthly'
	union
	select ilr_id, to_char(add_months(y.open_month,0), 'dd-mon-yyyy') payment_start_date, 0 pymt_amt, payment_freq_id, 24 num_terms
	from lsr_ilr, ls_payment_freq, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-In-Service-ILR-ST2','AUTO-Pre-In-Service-ILR-ST2','AUTO-Arr-In-Service-ILR-DF2','AUTO-Pre-In-Service-ILR-DF2')
	and ls_payment_freq.description = 'Quarterly'
	union
	select ilr_id, to_char(add_months(y.open_month,0), 'dd-mon-yyyy') payment_start_date, 35000 pymt_amt, payment_freq_id, 12 num_terms
	from lsr_ilr, ls_payment_freq, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-In-Service-ILR-ST4','AUTO-Pre-In-Service-ILR-ST4','AUTO-Arr-In-Service-ILR-DF4','AUTO-Pre-In-Service-ILR-DF4')
	and ls_payment_freq.description = 'Semi-Annual'
	union
	select ilr_id, to_char(add_months(y.open_month,0), 'dd-mon-yyyy') payment_start_date, 35000 pymt_amt, payment_freq_id, 6 num_terms
	from lsr_ilr, ls_payment_freq, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-In-Service-ILR-ST3','AUTO-Pre-In-Service-ILR-ST3','AUTO-Arr-In-Service-ILR-DF3','AUTO-Pre-In-Service-ILR-DF3')
	and ls_payment_freq.description = 'Annual'
) x
where ls_payment_term_type.description = 'Normal';

--update the payment amounts for the quarterly ILRs
update lsr_ilr_payment_term set payment_amount = 100 where ilr_id = (select ilr_id from lsr_ilr where ilr_number in ('AUTO-Arr-In-Service-ILR-ST2'));
update lsr_ilr_payment_term set payment_amount = 9000 where ilr_id = (select ilr_id from lsr_ilr where ilr_number in ('AUTO-Pre-In-Service-ILR-DF2'));
update lsr_ilr_payment_term set payment_amount = 12500 where ilr_id = (select ilr_id from lsr_ilr where ilr_number in ('AUTO-Arr-In-Service-ILR-ST2'));
update lsr_ilr_payment_term set payment_amount = 12000 where ilr_id = (select ilr_id from lsr_ilr where ilr_number in ('AUTO-Pre-In-Service-ILR-DF2'));

------------------------------------------------
--ILR Initial Direct Costs
------------------------------------------------
insert into lsr_ilr_initial_direct_cost(ilr_initial_direct_cost_id, ilr_id, revision, idc_group_id, date_incurred, amount, description)
select lsr_ilr_idc_seq.nextval, x.ilr_id, 1, x.idc_group_id, '01-Dec-2011', 5000, 'AUTO Lessor In-Service IDC'
from  
(select ilr_id, idc_group_id
	from lsr_ilr, lsr_initial_direct_cost_group
	where lsr_ilr.ilr_number in (
	'AUTO-Arr-In-Service-ILR-ST1','AUTO-Arr-In-Service-ILR-ST2','AUTO-Arr-In-Service-ILR-ST3','AUTO-Arr-In-Service-ILR-ST4',
	'AUTO-Pre-In-Service-ILR-ST1','AUTO-Pre-In-Service-ILR-ST2','AUTO-Pre-In-Service-ILR-ST3','AUTO-Pre-In-Service-ILR-ST4',
    'AUTO-Arr-In-Service-ILR-DF1','AUTO-Arr-In-Service-ILR-DF2','AUTO-Arr-In-Service-ILR-DF3','AUTO-Arr-In-Service-ILR-DF4',
	'AUTO-Pre-In-Service-ILR-DF1','AUTO-Pre-In-Service-ILR-DF2','AUTO-Pre-In-Service-ILR-DF3','AUTO-Pre-In-Service-ILR-DF4')
	and lsr_initial_direct_cost_group.description = 'AUTO Lessor In-Service IDCG'
) x;

--update the initial direct costs on the Annual ILRs
update lsr_ilr_initial_direct_cost set amount = 200
where description = 'AUTO Lessor In-Service IDC' 
and ilr_id in 
(
	select ilr_id from lsr_ilr 
	where ilr_number in ('AUTO-Arr-In-Service-ILR-ST3','AUTO-Pre-In-Service-ILR-ST3','AUTO-Arr-In-Service-ILR-DF3','AUTO-Pre-In-Service-ILR-DF3')
);