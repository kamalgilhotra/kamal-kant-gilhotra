-------------------------------------------
--Non-CPR ILR Header Table Data (Direct Finance)
-------------------------------------------
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-MC-DF-ILR1', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-NonMC-DF-ILR1', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-69), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-NonMC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-NonMC-DF-ILR1', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-NonMC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-NonMC-DF-ILR2', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-NonMC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-MC-DF-ILR1', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-MC-DF-ILR9', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-71), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-MC-DF-ILR9', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-71), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

-------------------------------------------
--Non-CPR ILR Header Table Data (Sales Type)
-------------------------------------------
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-MC-ST-ILR1', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-NonMC-ST-ILR2', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-NonMC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-MC-ST-ILR2', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-9), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-NonMC-ST-ILR1', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-NonMC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-MC-ST-ILR1', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-NonMC-ST-ILR2', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-NonMC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-MC-ST-ILR9', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-71), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-MC-ST-ILR9', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-71), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

-------------------------------------------
--Non-CPR ILR Header Table Data (Operating)
-------------------------------------------
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-NonMC-OP-ILR1', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-69), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-NonMC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-MC-OP-ILR1', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,0), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-MC-OP-ILR1', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-36), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-NonMC-OP-ILR1', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-69), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-NonMC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-MC-OP-ILR2', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-2), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-MC-OP-ILR2', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-2), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-MC-OP-ILR3', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-2), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-MC-OP-ILR3', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-2), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-NonMC-OP-ILR4', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-3), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-NonMC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-NonMC-OP-ILR2', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-3), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-NonMC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-NonMC-OP-ILR4', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-24), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-NonMC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-NonMC-OP-ILR3', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-3), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-NonMC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

--pjones new
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-MC-OP-ILR5', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-3), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-MC-OP-ILR5', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-3), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-MC-OP-ILR6', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-3), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-MC-OP-ILR6', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-3), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-MC-OP-ILR7', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-6), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-MC-OP-ILR7', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-6), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-MC-OP-ILR8', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-12), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-MC-OP-ILR8', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-12), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Arr-MC-OP-ILR9', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-71), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Arr-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-Pre-MC-OP-ILR9', lease_id, nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100), to_char(add_months(y.open_month,-71), 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, x.workflow_type_id, 0
 from lsr_lease, ls_ilr_status, lsr_ilr_group, (
  select min(workflow_type_id) workflow_type_id from workflow_type
  where lower(workflow_type.description) like '%auto%approve%'
  and workflow_type.subsystem like '%lessor%ilr%'
) x, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
where lsr_lease.lease_number = 'AUTO-Pre-MC-MLA'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO Month End ILRG1';


-----------------------
--ILR Approvals Data
---------------------
--leave as Initiated so we can build schedule
insert into lsr_ilr_approval(ilr_id, revision, approval_type_id, approval_status_id)
select ilr_id, 1, lsr_ilr.workflow_type_id, ls_ilr_status.ilr_status_id
from lsr_ilr, ls_ilr_status
where lsr_ilr.ilr_number in (
'AUTO-Arr-MC-DF-ILR1','AUTO-Arr-NonMC-DF-ILR1','AUTO-Pre-NonMC-DF-ILR1','AUTO-Pre-NonMC-DF-ILR2','AUTO-Pre-MC-DF-ILR1',
'AUTO-Arr-MC-ST-ILR1','AUTO-Arr-NonMC-ST-ILR2','AUTO-Arr-MC-ST-ILR2','AUTO-Pre-NonMC-ST-ILR1','AUTO-Pre-MC-ST-ILR1','AUTO-Pre-NonMC-ST-ILR2',
'AUTO-Arr-NonMC-OP-ILR1','AUTO-Arr-MC-OP-ILR1','AUTO-Pre-MC-OP-ILR1','AUTO-Pre-NonMC-OP-ILR1','AUTO-Arr-MC-OP-ILR2','AUTO-Pre-MC-OP-ILR2','AUTO-Pre-MC-OP-ILR3','AUTO-Arr-MC-OP-ILR3',
'AUTO-Arr-NonMC-OP-ILR4','AUTO-Arr-NonMC-OP-ILR2','AUTO-Pre-NonMC-OP-ILR4','AUTO-Pre-NonMC-OP-ILR3','AUTO-Arr-MC-OP-ILR5','AUTO-Pre-MC-OP-ILR5','AUTO-Arr-MC-OP-ILR6',
'AUTO-Pre-MC-OP-ILR6','AUTO-Arr-MC-OP-ILR7','AUTO-Pre-MC-OP-ILR7','AUTO-Arr-MC-OP-ILR8','AUTO-Pre-MC-OP-ILR8','AUTO-Arr-MC-OP-ILR9','AUTO-Pre-MC-OP-ILR9','AUTO-Arr-MC-ST-ILR9','AUTO-Pre-MC-ST-ILR9',
'AUTO-Arr-MC-DF-ILR9','AUTO-Pre-MC-DF-ILR9')
and ls_ilr_status.description = 'Initiated';

-------------------------
--ILR Options Table Data
-------------------------
--add the other options for the ILRs we are adding
insert into lsr_ilr_options(ilr_id, revision, purchase_option_type_id, purchase_option_amt, cancelable_type_id, itc_sw, partial_retire_sw, sublet_sw, lease_cap_type_id,
termination_amt, payment_shift, in_service_exchange_rate, intent_to_purchase, specialized_asset, likely_to_collect, sublease_flag, interco_lease_flag)
select ilr_id, 1, purchase_option_type_id, 0, cancelable_type_id, 0, 0, 0, cap_type_id,
0, 0, CASE WHEN rate IS NULL THEN 1 ELSE 1/rate END, 1, 1, 1, 0, 0
from lsr_ilr a
JOIN lsr_lease ON lsr_lease.lease_id = a.lease_id
JOIN lsr_lease_company ON lsr_lease.lease_id = lsr_lease_company.lease_id
JOIN ls_purchase_option_type ON ls_purchase_option_type.description = 'None'
JOIN ls_cancelable_type ON ls_cancelable_type.description = 'Cancelable with Penalty'
join lsr_cap_type ON ((a.ilr_number in ('AUTO-Arr-NonMC-DF-ILR1') and lsr_cap_type.description = 'AUTO MEC DirectFinance w/ SalesType')
	                    or (a.ilr_number in ('AUTO-Arr-MC-ST-ILR1','AUTO-Pre-NonMC-ST-ILR1') and lsr_cap_type.description = 'AUTO MEC SalesType w/ DirectFinance')
	                    or (a.ilr_number in ('AUTO-Arr-NonMC-ST-ILR2','AUTO-Arr-MC-ST-ILR2','AUTO-Pre-MC-ST-ILR1','AUTO-Pre-NonMC-ST-ILR2','AUTO-Arr-MC-ST-ILR9','AUTO-Pre-MC-ST-ILR9')
                            and lsr_cap_type.description = 'AUTO MEC Sales Type')
	                    or (a.ilr_number in ('AUTO-Arr-MC-DF-ILR1','AUTO-Pre-NonMC-DF-ILR1','AUTO-Pre-NonMC-DF-ILR2','AUTO-Pre-MC-DF-ILR1','AUTO-Arr-MC-DF-ILR9','AUTO-Pre-MC-DF-ILR9')
                            and lsr_cap_type.description = 'AUTO MEC Direct Finance')
	                    or (a.ilr_number in ('AUTO-Arr-NonMC-OP-ILR1','AUTO-Arr-MC-OP-ILR1','AUTO-Pre-MC-OP-ILR1','AUTO-Pre-NonMC-OP-ILR1','AUTO-Arr-MC-OP-ILR2',
                                           'AUTO-Pre-MC-OP-ILR2','AUTO-Pre-MC-OP-ILR3','AUTO-Arr-MC-OP-ILR3','AUTO-Arr-NonMC-OP-ILR4','AUTO-Arr-NonMC-OP-ILR2',
                                           'AUTO-Pre-NonMC-OP-ILR4','AUTO-Pre-NonMC-OP-ILR3','AUTO-Arr-MC-OP-ILR5','AUTO-Pre-MC-OP-ILR5','AUTO-Arr-MC-OP-ILR6',
                                           'AUTO-Pre-MC-OP-ILR6','AUTO-Arr-MC-OP-ILR7','AUTO-Pre-MC-OP-ILR7','AUTO-Arr-MC-OP-ILR8','AUTO-Pre-MC-OP-ILR8',
                                           'AUTO-Arr-MC-OP-ILR9','AUTO-Pre-MC-OP-ILR9')
                            and lsr_cap_type.description = 'AUTO MEC Operating'))
LEFT JOIN currency_schema ON lsr_lease_company.company_id = currency_schema.company_id
LEFT JOIN currency_rate_default ON (currency_rate_default.exchange_date = a.est_in_svc_date
                                      AND currency_rate_default.currency_from = lsr_lease.contract_currency_id
                                      AND currency_rate_default.currency_to = currency_schema.currency_id
                                      AND currency_rate_default.exchange_rate_type_id = 1);

--make some manual updates to purchase option amounts and termination amounts
update lsr_ilr_options set purchase_option_amt = 5000, termination_amt = 4000,
purchase_option_type_id = (select purchase_option_type_id from ls_purchase_option_type where description = 'Flat Amount ILR')
where ilr_id = (select ilr_id from lsr_ilr where ilr_number = 'AUTO-Arr-NonMC-DF-ILR1');
update lsr_ilr_options set purchase_option_amt = 0, termination_amt = 4500,
purchase_option_type_id = (select purchase_option_type_id from ls_purchase_option_type where description = 'Flat Amount ILR')
where ilr_id = (select ilr_id from lsr_ilr where ilr_number = 'AUTO-Arr-NonMC-OP-ILR1');
update lsr_ilr_options set purchase_option_amt = 0, termination_amt = 2500,
purchase_option_type_id = (select purchase_option_type_id from ls_purchase_option_type where description = 'Flat Amount ILR')
where ilr_id in (select ilr_id from lsr_ilr where ilr_number in ('AUTO-Pre-NonMC-OP-ILR1','AUTO-Arr-MC-OP-ILR9','AUTO-Pre-MC-OP-ILR9'));
update lsr_ilr_options set purchase_option_amt = 4000, termination_amt = 2500,
purchase_option_type_id = (select purchase_option_type_id from ls_purchase_option_type where description = 'Flat Amount ILR')
where ilr_id in (select ilr_id from lsr_ilr where ilr_number in ('AUTO-Arr-MC-ST-ILR9','AUTO-Pre-MC-ST-ILR9','AUTO-Arr-MC-DF-ILR9','AUTO-Pre-MC-DF-ILR9'));

--------------------------------------------
--ILR Accounts (Default from the ILR Group)
--------------------------------------------
insert into lsr_ilr_account(ilr_id, int_accrual_account_id, int_expense_account_id, exec_accrual_account_id, exec_expense_account_id, cont_accrual_account_id,
cont_expense_account_id, st_receivable_account_id, lt_receivable_account_id, ar_account_id, unguaran_res_account_id, int_unguaran_res_account_id, sell_profit_loss_account_id, ini_direct_cost_account_id, prop_plant_account_id, deferred_rent_acct_id, accrued_rent_acct_id, curr_gain_loss_acct_id, curr_gain_loss_offset_acct_id, INCURRED_COSTS_ACCOUNT_ID, DEF_COSTS_ACCOUNT_ID, DEF_SELLING_PROFIT_ACCOUNT_ID)
select ilr_id, int_accrual_account_id, int_expense_account_id, exec_accrual_account_id, exec_expense_account_id, cont_accrual_account_id, cont_expense_account_id, st_receivable_account_id, lt_receivable_account_id, ar_account_id, unguaran_res_account_id, int_unguaran_res_account_id, sell_profit_loss_account_id, ini_direct_cost_account_id, prop_plant_account_id, deferred_rent_acct_id, accrued_rent_acct_id, curr_gain_loss_acct_id, curr_gain_loss_offset_acct_id, INCURRED_COSTS_ACCOUNT_ID, DEF_COSTS_ACCOUNT_ID, DEF_SELLING_PROFIT_ACCOUNT_ID
from lsr_ilr a
inner join lsr_ilr_group b
on a.ilr_group_id = b.ilr_group_id
where ilr_id in (select ilr_id from lsr_ilr where ilr_number in (
'AUTO-Arr-MC-DF-ILR1','AUTO-Arr-NonMC-DF-ILR1','AUTO-Pre-NonMC-DF-ILR1','AUTO-Pre-NonMC-DF-ILR2','AUTO-Pre-MC-DF-ILR1',
'AUTO-Arr-MC-ST-ILR1','AUTO-Arr-NonMC-ST-ILR2','AUTO-Arr-MC-ST-ILR2','AUTO-Pre-NonMC-ST-ILR1','AUTO-Pre-MC-ST-ILR1','AUTO-Pre-NonMC-ST-ILR2',
'AUTO-Arr-NonMC-OP-ILR1','AUTO-Arr-MC-OP-ILR1','AUTO-Pre-MC-OP-ILR1','AUTO-Pre-NonMC-OP-ILR1','AUTO-Arr-MC-OP-ILR2','AUTO-Pre-MC-OP-ILR2','AUTO-Pre-MC-OP-ILR3','AUTO-Arr-MC-OP-ILR3',
'AUTO-Arr-NonMC-OP-ILR4','AUTO-Arr-NonMC-OP-ILR2','AUTO-Pre-NonMC-OP-ILR4','AUTO-Pre-NonMC-OP-ILR3','AUTO-Arr-MC-OP-ILR5','AUTO-Pre-MC-OP-ILR5',
'AUTO-Arr-MC-OP-ILR6','AUTO-Pre-MC-OP-ILR6','AUTO-Arr-MC-OP-ILR7','AUTO-Pre-MC-OP-ILR7','AUTO-Arr-MC-OP-ILR8','AUTO-Pre-MC-OP-ILR8','AUTO-Arr-MC-OP-ILR9',
'AUTO-Pre-MC-OP-ILR9','AUTO-Arr-MC-ST-ILR9','AUTO-Pre-MC-ST-ILR9','AUTO-Arr-MC-DF-ILR9','AUTO-Pre-MC-DF-ILR9'));

------------------------------
--ILR Payment Terms (Monthly)
------------------------------
insert into lsr_ilr_payment_term(ilr_id, revision, payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms, payment_amount, contingent_amount,
c_bucket_1, c_bucket_2, c_bucket_3, c_bucket_4, c_bucket_5, c_bucket_6, c_bucket_7, c_bucket_8, c_bucket_9, c_bucket_10,
e_bucket_1, e_bucket_2, e_bucket_3, e_bucket_4, e_bucket_5, e_bucket_6, e_bucket_7, e_bucket_8, e_bucket_9, e_bucket_10)
select x.ilr_id, 1, x.pay_term_id, payment_term_type_id, x.payment_start_date, payment_freq_id, x.num_terms, x.pymt_amt, 0 cont_amt,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0
from ls_payment_term_type, ls_payment_freq,
(
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,0), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 72 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-DF-ILR1','AUTO-Pre-NonMC-DF-ILR1')
	union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-69), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 72 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-NonMC-OP-ILR1')
	union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-69), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 60 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-NonMC-OP-ILR1')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,-9), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 12 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-NonMC-OP-ILR1')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-2), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 3 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR2')
  union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,+1), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 66 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR2')
  union
	select ilr_id, 3 pay_term_id, to_char(add_months(y.open_month,+67), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 3 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR2')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-3), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 36 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR5')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,+33), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 36 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR5')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-3), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 36 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR5')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,+33), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 36 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR5')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-71), 'dd-mon-yyyy') payment_start_date, 18000 pymt_amt, 48 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR9')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,-23), 'dd-mon-yyyy') payment_start_date, 9000 pymt_amt, 24 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR9')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-71), 'dd-mon-yyyy') payment_start_date, 9000 pymt_amt, 24 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-ST-ILR9')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,-47), 'dd-mon-yyyy') payment_start_date, 18000 pymt_amt, 48 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-ST-ILR9')
) x
where ls_payment_term_type.description = 'Normal'
and ls_payment_freq.description = 'Monthly';

------------------------------
--ILR Payment Terms (Quarterly)
------------------------------
insert into lsr_ilr_payment_term(ilr_id, revision, payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms, payment_amount, contingent_amount,
c_bucket_1, c_bucket_2, c_bucket_3, c_bucket_4, c_bucket_5, c_bucket_6, c_bucket_7, c_bucket_8, c_bucket_9, c_bucket_10,
e_bucket_1, e_bucket_2, e_bucket_3, e_bucket_4, e_bucket_5, e_bucket_6, e_bucket_7, e_bucket_8, e_bucket_9, e_bucket_10)
select x.ilr_id, 1, x.pay_term_id, payment_term_type_id, x.payment_start_date, payment_freq_id, x.num_terms, x.pymt_amt, 0 cont_amt,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0
from ls_payment_term_type, ls_payment_freq,
(
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-69), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 24 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-NonMC-DF-ILR1')
	union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,0), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 24 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-ST-ILR1','AUTO-Pre-NonMC-ST-ILR1','AUTO-Arr-MC-OP-ILR1')
	union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-36), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 12 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR1')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,0), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 12 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR1')
	union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-2), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 2 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR2')
  union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,+4), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 20 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR2')
  union
	select ilr_id, 3 pay_term_id, to_char(add_months(y.open_month,+61), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 2 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR2')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-3), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 12 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-NonMC-OP-ILR4')
  union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,+33), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 12 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-NonMC-OP-ILR4')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-3), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 12 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR6')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,+33), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 12 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR6')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-3), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 12 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR6')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,+33), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 12 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR6')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-71), 'dd-mon-yyyy') payment_start_date, 18000 pymt_amt, 16 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-ST-ILR9')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,-23), 'dd-mon-yyyy') payment_start_date, 9000 pymt_amt, 8 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-ST-ILR9')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-71), 'dd-mon-yyyy') payment_start_date, 9000 pymt_amt, 8 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-DF-ILR9')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,-47), 'dd-mon-yyyy') payment_start_date, 18000 pymt_amt, 16 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-DF-ILR9')
) x
where ls_payment_term_type.description = 'Normal'
and ls_payment_freq.description = 'Quarterly';

------------------------------
--ILR Payment Terms (Semi-Annual)
------------------------------
insert into lsr_ilr_payment_term(ilr_id, revision, payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms, payment_amount, contingent_amount,
c_bucket_1, c_bucket_2, c_bucket_3, c_bucket_4, c_bucket_5, c_bucket_6, c_bucket_7, c_bucket_8, c_bucket_9, c_bucket_10,
e_bucket_1, e_bucket_2, e_bucket_3, e_bucket_4, e_bucket_5, e_bucket_6, e_bucket_7, e_bucket_8, e_bucket_9, e_bucket_10)
select x.ilr_id, 1, x.pay_term_id, payment_term_type_id, x.payment_start_date, payment_freq_id, x.num_terms, x.pymt_amt, 0 cont_amt,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0
from ls_payment_term_type, ls_payment_freq,
(
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,0), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 12 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-NonMC-DF-ILR2','AUTO-Arr-NonMC-ST-ILR2','AUTO-Pre-MC-ST-ILR1')
  union
  select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-2), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 1 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR3')
  union
  select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,+4), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 10 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR3')
  union
  select ilr_id, 3 pay_term_id, to_char(add_months(y.open_month,+61), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 1 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR3')
  union
  select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-2), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 1 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR3')
  union
  select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,+4), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 10 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR3')
  union
  select ilr_id, 3 pay_term_id, to_char(add_months(y.open_month,+61), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 1 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR3')
  union
  select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month, -24), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 4 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-NonMC-OP-ILR4')
  union
  select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,0), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 8 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-NonMC-OP-ILR4')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-3), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 6 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR7')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,+33), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 6 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR7')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-3), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 6 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR7')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,+33), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 6 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR7')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-71), 'dd-mon-yyyy') payment_start_date, 18000 pymt_amt, 8 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR9')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,-23), 'dd-mon-yyyy') payment_start_date, 9000 pymt_amt, 4 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR9')
) x
where ls_payment_term_type.description = 'Normal'
and ls_payment_freq.description = 'Semi-Annual';

------------------------------
--ILR Payment Terms (Annual)
------------------------------
insert into lsr_ilr_payment_term(ilr_id, revision, payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms, payment_amount, contingent_amount,
c_bucket_1, c_bucket_2, c_bucket_3, c_bucket_4, c_bucket_5, c_bucket_6, c_bucket_7, c_bucket_8, c_bucket_9, c_bucket_10,
e_bucket_1, e_bucket_2, e_bucket_3, e_bucket_4, e_bucket_5, e_bucket_6, e_bucket_7, e_bucket_8, e_bucket_9, e_bucket_10)
select x.ilr_id, 1, x.pay_term_id, payment_term_type_id, x.payment_start_date, payment_freq_id, x.num_terms, x.pymt_amt, 0 cont_amt,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0
from ls_payment_term_type, ls_payment_freq,
(
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,0), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 6 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-DF-ILR1','AUTO-Pre-NonMC-ST-ILR2')
	union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-9), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 6 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-ST-ILR2')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-3), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 3 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-NonMC-OP-ILR2')
  union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,-9), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 3 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-NonMC-OP-ILR2')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,+33), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 2 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-NonMC-OP-ILR3')
  union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,-0), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 4 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-NonMC-OP-ILR3')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-3), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 3 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR8')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,+33), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 3 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-OP-ILR8')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-3), 'dd-mon-yyyy') payment_start_date, 15000 pymt_amt, 3 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR8')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,+33), 'dd-mon-yyyy') payment_start_date, 7000 pymt_amt, 3 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Pre-MC-OP-ILR8')
  union
	select ilr_id, 1 pay_term_id, to_char(add_months(y.open_month,-71), 'dd-mon-yyyy') payment_start_date, 9000 pymt_amt, 2 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-DF-ILR9')
	union
	select ilr_id, 2 pay_term_id, to_char(add_months(y.open_month,-47), 'dd-mon-yyyy') payment_start_date, 18000 pymt_amt, 4 num_terms
	from lsr_ilr, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = nvl((select company_id from company_setup where description = 'Lessor MEC Company'),100) and lsr_closed is null) y
	where lsr_ilr.ilr_number in ('AUTO-Arr-MC-DF-ILR9')
) x
where ls_payment_term_type.description = 'Normal'
and ls_payment_freq.description = 'Annual';

--------------------------------------
--ILR Executory/Contingent Amounts (Group)
--------------------------------------
--for executory and contingent amounts, we update the subsets of buckets that have common data
--update executory buckets 1-5
update lsr_ilr_payment_term set e_bucket_1 = 5, e_bucket_2 = 5, e_bucket_3 = 5, e_bucket_4 = 5, e_bucket_5 = 5
where ilr_id in (select ilr_id from lsr_ilr where ilr_number in (
'AUTO-Pre-NonMC-DF-ILR1','AUTO-Arr-NonMC-OP-ILR1','AUTO-Pre-NonMC-OP-ILR1','AUTO-Arr-NonMC-DF-ILR1','AUTO-Arr-MC-ST-ILR1',
'AUTO-Pre-NonMC-ST-ILR1','AUTO-Arr-MC-OP-ILR1','AUTO-Pre-MC-DF-ILR1','AUTO-Arr-MC-ST-ILR2','AUTO-Pre-NonMC-ST-ILR2',
'AUTO-Arr-MC-OP-ILR2','AUTO-Pre-MC-OP-ILR2','AUTO-Pre-MC-OP-ILR3','AUTO-Arr-MC-OP-ILR3','AUTO-Arr-NonMC-OP-ILR4',
'AUTO-Arr-NonMC-OP-ILR2','AUTO-Pre-NonMC-OP-ILR4','AUTO-Pre-NonMC-OP-ILR3','AUTO-Arr-MC-OP-ILR5','AUTO-Pre-MC-OP-ILR5',
'AUTO-Arr-MC-OP-ILR6','AUTO-Pre-MC-OP-ILR6','AUTO-Arr-MC-OP-ILR7','AUTO-Pre-MC-OP-ILR7','AUTO-Arr-MC-OP-ILR8','AUTO-Pre-MC-OP-ILR8',
'AUTO-Arr-MC-OP-ILR9','AUTO-Pre-MC-OP-ILR9','AUTO-Arr-MC-ST-ILR9','AUTO-Pre-MC-ST-ILR9','AUTO-Arr-MC-DF-ILR9','AUTO-Pre-MC-DF-ILR9'));

--update executory buckets 6-10
update lsr_ilr_payment_term set e_bucket_6 = 5, e_bucket_7 = 5, e_bucket_8 = 5, e_bucket_9 = 5, e_bucket_10 = 5
where ilr_id in (select ilr_id from lsr_ilr where ilr_number in ('AUTO-Pre-NonMC-DF-ILR1','AUTO-Arr-MC-ST-ILR1','AUTO-Arr-MC-OP-ILR1','AUTO-Arr-MC-OP-ILR2','AUTO-Pre-MC-OP-ILR2','AUTO-Pre-MC-OP-ILR3','AUTO-Arr-MC-OP-ILR3','AUTO-Arr-NonMC-OP-ILR4','AUTO-Arr-NonMC-OP-ILR2','AUTO-Pre-NonMC-OP-ILR4','AUTO-Pre-NonMC-OP-ILR3'));

--update contingent buckets 1-5
update lsr_ilr_payment_term set c_bucket_1 = 5, c_bucket_2 = 5, c_bucket_3 = 5, c_bucket_4 = 5, c_bucket_5 = 5, contingent_amount = contingent_amount + 25
where ilr_id in (select ilr_id from lsr_ilr where ilr_number in (
'AUTO-Arr-MC-DF-ILR1','AUTO-Pre-NonMC-DF-ILR1','AUTO-Arr-NonMC-OP-ILR1','AUTO-Pre-NonMC-OP-ILR1','AUTO-Arr-NonMC-DF-ILR1',
'AUTO-Pre-NonMC-ST-ILR1','AUTO-Pre-MC-OP-ILR1','AUTO-Pre-NonMC-DF-ILR2','AUTO-Arr-NonMC-ST-ILR2','AUTO-Pre-MC-ST-ILR1',
'AUTO-Pre-MC-DF-ILR1','AUTO-Arr-MC-ST-ILR2','AUTO-Pre-NonMC-ST-ILR2','AUTO-Arr-MC-OP-ILR2','AUTO-Pre-MC-OP-ILR2',
'AUTO-Pre-MC-OP-ILR3','AUTO-Arr-MC-OP-ILR3','AUTO-Arr-NonMC-OP-ILR4','AUTO-Arr-NonMC-OP-ILR2','AUTO-Pre-NonMC-OP-ILR4','AUTO-Pre-NonMC-OP-ILR3',
'AUTO-Arr-MC-OP-ILR5','AUTO-Pre-MC-OP-ILR5','AUTO-Arr-MC-OP-ILR6','AUTO-Pre-MC-OP-ILR6','AUTO-Arr-MC-OP-ILR7',
'AUTO-Pre-MC-OP-ILR7','AUTO-Arr-MC-OP-ILR8','AUTO-Pre-MC-OP-ILR8','AUTO-Arr-MC-OP-ILR9',
'AUTO-Pre-MC-OP-ILR9','AUTO-Arr-MC-ST-ILR9','AUTO-Pre-MC-ST-ILR9','AUTO-Arr-MC-DF-ILR9','AUTO-Pre-MC-DF-ILR9'));

--update contingent buckets 6-10
update lsr_ilr_payment_term set c_bucket_6 = 5, c_bucket_7 = 5, c_bucket_8 = 5, c_bucket_9 = 5, c_bucket_10 = 5, contingent_amount = contingent_amount + 25
where ilr_id in (select ilr_id from lsr_ilr where ilr_number in ('AUTO-Arr-MC-DF-ILR1','AUTO-Pre-NonMC-DF-ILR1','AUTO-Pre-MC-OP-ILR1','AUTO-Pre-NonMC-DF-ILR2','AUTO-Arr-NonMC-ST-ILR2','AUTO-Pre-MC-ST-ILR1','AUTO-Arr-MC-OP-ILR2','AUTO-Pre-MC-OP-ILR2','AUTO-Pre-MC-OP-ILR3','AUTO-Arr-MC-OP-ILR3','AUTO-Arr-NonMC-OP-ILR4','AUTO-Arr-NonMC-OP-ILR2','AUTO-Pre-NonMC-OP-ILR4','AUTO-Pre-NonMC-OP-ILR3'));

--------------------------------------
--ILR Executory/Contingent Amounts (Individual)
--------------------------------------
--manual Executory/Contingent updates
--update AUTO-Pre-NonMC-DF-ILR1
update lsr_ilr_payment_term set c_bucket_1 = 0, contingent_amount = contingent_amount - 5
where ilr_id = (select ilr_id from lsr_ilr where ilr_number = 'AUTO-Pre-NonMC-DF-ILR1');

--update AUTO-Arr-NonMC-OP-ILR1; this one is weird because it has 2 terms and no amounts on the first one
update lsr_ilr_payment_term set e_bucket_1 = 0, e_bucket_2 = 0, e_bucket_3 = 0, e_bucket_4 = 0, e_bucket_5 = 0,
c_bucket_1 = 0, c_bucket_2 = 0, c_bucket_3 = 0, c_bucket_4 = 0, c_bucket_5 = 0, contingent_amount = 0
where ilr_id = (select ilr_id from lsr_ilr where ilr_number = 'AUTO-Arr-NonMC-OP-ILR1') and payment_term_id = 1;

--update AUTO-Pre-NonMC-OP-ILR1
update lsr_ilr_payment_term set c_bucket_1 = 0, c_bucket_5 = 0, contingent_amount = contingent_amount - 10
where ilr_id = (select ilr_id from lsr_ilr where ilr_number = 'AUTO-Pre-NonMC-OP-ILR1');

--update AUTO-Pre-NonMC-ST-ILR1
update lsr_ilr_payment_term set c_bucket_1 = 0, contingent_amount = contingent_amount - 5
where ilr_id = (select ilr_id from lsr_ilr where ilr_number = 'AUTO-Pre-NonMC-ST-ILR1');

--update AUTO-Pre-MC-OP-ILR1; also weird because the second term has no amounts but the first has all the amounts
update lsr_ilr_payment_term set c_bucket_1 = 0, c_bucket_2 = 0, c_bucket_3 = 0, c_bucket_4 = 0, c_bucket_5 = 0,
c_bucket_6 = 0, c_bucket_7 = 0, c_bucket_8 = 0, c_bucket_9 = 0, c_bucket_10 = 0, contingent_amount = 0
where ilr_id = (select ilr_id from lsr_ilr where ilr_number = 'AUTO-Pre-MC-OP-ILR1') and payment_term_id = 2;

--update AUTO-Pre-NonMC-DF-ILR2
update lsr_ilr_payment_term set c_bucket_1 = 0, contingent_amount = contingent_amount - 5
where ilr_id = (select ilr_id from lsr_ilr where ilr_number = 'AUTO-Pre-NonMC-DF-ILR2');

--update AUTO-Arr-MC-ST-ILR2
update lsr_ilr_payment_term set c_bucket_1 = 0, c_bucket_5 = 0, contingent_amount = contingent_amount - 10
where ilr_id = (select ilr_id from lsr_ilr where ilr_number = 'AUTO-Arr-MC-ST-ILR2');

------------------------------------------------
--ILR Initial Direct Costs
------------------------------------------------
insert into lsr_ilr_initial_direct_cost(ilr_initial_direct_cost_id, ilr_id, revision, idc_group_id, date_incurred, amount, description)
select lsr_ilr_idc_seq.nextval, x.ilr_id, 1, x.idc_group_id, to_char(add_months(x.est_in_svc_date,-1), 'dd-mon-yyyy'), 100, 'AUTO Lessor Month End IDC'
from
(select ilr_id, idc_group_id, est_in_svc_date
	from lsr_ilr, lsr_initial_direct_cost_group
	where lsr_ilr.ilr_number in (
	'AUTO-Arr-MC-DF-ILR1','AUTO-Arr-MC-ST-ILR1')
	and lsr_initial_direct_cost_group.description = 'AUTO Lessor Month End IDCG'
) x;

insert into lsr_ilr_initial_direct_cost(ilr_initial_direct_cost_id, ilr_id, revision, idc_group_id, date_incurred, amount, description)
select lsr_ilr_idc_seq.nextval, x.ilr_id, 1, x.idc_group_id, to_char(add_months(x.est_in_svc_date,-1), 'dd-mon-yyyy'), 1500, 'AUTO Lessor Month End IDC'
from
(select ilr_id, idc_group_id, est_in_svc_date
	from lsr_ilr, lsr_initial_direct_cost_group
	where lsr_ilr.ilr_number in (
	'AUTO-Arr-NonMC-OP-ILR4','AUTO-Arr-NonMC-OP-ILR2','AUTO-Pre-NonMC-OP-ILR4','AUTO-Pre-NonMC-OP-ILR3','AUTO-Arr-NonMC-OP-ILR1',
  'AUTO-Arr-MC-OP-ILR5','AUTO-Pre-MC-OP-ILR5','AUTO-Arr-MC-OP-ILR6','AUTO-Pre-MC-OP-ILR6','AUTO-Arr-MC-OP-ILR7','AUTO-Pre-MC-OP-ILR7',
  'AUTO-Arr-MC-OP-ILR8','AUTO-Pre-MC-OP-ILR8','AUTO-Arr-MC-OP-ILR9','AUTO-Pre-MC-OP-ILR9','AUTO-Arr-MC-ST-ILR9','AUTO-Pre-MC-ST-ILR9',
  'AUTO-Arr-MC-DF-ILR9','AUTO-Pre-MC-DF-ILR9')
	and lsr_initial_direct_cost_group.description = 'AUTO Lessor Month End IDCG'
) x;

insert into lsr_ilr_initial_direct_cost(ilr_initial_direct_cost_id, ilr_id, revision, idc_group_id, date_incurred, amount, description)
select lsr_ilr_idc_seq.nextval, x.ilr_id, 1, x.idc_group_id, to_char(add_months(x.est_in_svc_date,-1), 'dd-mon-yyyy'), 2000, 'AUTO Lessor Month End IDC'
from
(select ilr_id, idc_group_id, est_in_svc_date
	from lsr_ilr, lsr_initial_direct_cost_group
	where lsr_ilr.ilr_number in (
	'AUTO-Arr-MC-OP-ILR1')
	and lsr_initial_direct_cost_group.description = 'AUTO Lessor Month End IDCG'
) x;