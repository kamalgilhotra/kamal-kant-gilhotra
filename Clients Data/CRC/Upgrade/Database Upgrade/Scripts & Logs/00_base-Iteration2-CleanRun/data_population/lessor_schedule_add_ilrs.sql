-------------------------------------------
--Variable ILR Header Table Data (Sales-Type/Arrears)
-------------------------------------------
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Monthly-Arr-ST', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Arrears MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Quarterly-Arr-ST', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Arrears MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Semi-Annual-Arr-ST', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Arrears MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Annual-Arr-ST', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Arrears MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

------------------------------------------
--Variable ILR Header Table Data (Sales-Type/Prepay)
------------------------------------------
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Monthly-Pre-ST', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Prepay MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Quarterly-Pre-ST', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Prepay MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Semi-Annual-Pre-ST', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Prepay MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Annual-Pre-ST', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Prepay MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

-------------------------------------------
--Variable ILR Header Table Data (Direct-Finance/Arrears)
-------------------------------------------
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Monthly-Arr-DF', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Arrears MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Quarterly-Arr-DF', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Arrears MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Semi-Annual-Arr-DF', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Arrears MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Annual-Arr-DF', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Arrears MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

------------------------------------------
--Variable ILR Header Table Data (Direct-Finance/Prepay)
------------------------------------------
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Monthly-Pre-DF', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Prepay MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Quarterly-Pre-DF', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Prepay MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Semi-Annual-Pre-DF', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Prepay MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Annual-Pre-DF', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Prepay MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

-------------------------------------------
--Variable ILR Header Table Data (Operating/Arrears)
-------------------------------------------
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Monthly-Arr-Op', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Arrears MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Quarterly-Arr-Op', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Arrears MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Semi-Annual-Arr-Op', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Arrears MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Annual-Arr-Op', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Arrears MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

------------------------------------------
--Variable ILR Header Table Data (Operating/Prepay)
------------------------------------------
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Monthly-Pre-Op', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Prepay MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Quarterly-Pre-Op', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Prepay MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Semi-Annual-Pre-Op', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Prepay MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Annual-Pre-Op', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Prepay MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

-------------------------------------------
--Variable ILR Header Table Data (Mixed Sales-Type/Direct-Finance - Arrears)
-------------------------------------------
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Monthly-Arr-Mixed', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Arrears MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Semi-Annual-Arr-Mixed', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Arrears MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

-------------------------------------------
--Variable ILR Header Table Data (Mixed Sales-Type/Direct-Finance - Prepay)
-------------------------------------------
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Quarterly-Pre-Mixed', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Prepay MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Annual-Pre-Mixed', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO Prepay MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

-------------------------------------------
--Variable ILR Header Table Data (Sales-Type and Direct Finance with no exchange rates)
-------------------------------------------
insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Monthly-Arr-ST-JPY', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO JPY MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

insert into lsr_ilr(ilr_id, ilr_number, lease_id, company_id, est_in_svc_date, ilr_status_id, ilr_group_id, current_revision, workflow_type_id, rate_group_id)
select ls_ilr_seq.nextval, 'AUTO-ILR-Monthly-Arr-DF-JPY', lease_id, 1 /*may want to make this more dynamic in the future*/, to_char(x.open_month, 'dd-mon-yyyy'), ilr_status_id, ilr_group_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0 /*no idea what this is*/
from lsr_lease, ls_ilr_status, lsr_ilr_group, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_lease.lease_number = 'AUTO JPY MLA1'
and ls_ilr_status.description = 'Initiated'
and lsr_ilr_group.description = 'AUTO ILRG1';

---------------------
--ILR Approvals Data
---------------------
--leave as Initiated so we can build schedule
insert into lsr_ilr_approval(ilr_id, revision, approval_type_id, approval_status_id)
select ilr_id, 1, (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), ls_ilr_status.ilr_status_id
from lsr_ilr, ls_ilr_status
where lsr_ilr.ilr_number in (
'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed',
'AUTO-ILR-Monthly-Arr-ST-JPY','AUTO-ILR-Monthly-Arr-DF-JPY')
and ls_ilr_status.description = 'Initiated';

-------------------------
--ILR Options Table Data
-------------------------
--add the other options for the ILRs we are adding
insert into lsr_ilr_options(ilr_id, revision, purchase_option_type_id, purchase_option_amt, cancelable_type_id, itc_sw, partial_retire_sw, sublet_sw, lease_cap_type_id,
termination_amt, payment_shift, in_service_exchange_rate, intent_to_purchase, specialized_asset, likely_to_collect, sublease_flag, interco_lease_flag)
select ilr_id, 1, purchase_option_type_id, 500, cancelable_type_id, 0, 0, 0, cap_type_id,
200, 0, 1 / rate /*this is because we store the company-to-contract rate for asset amount conversions*/, 1, 1, 1, 0, 0
from lsr_ilr, ls_purchase_option_type, ls_cancelable_type, lsr_cap_type, currency_rate_default_dense
where ls_purchase_option_type.description = 'Flat Amount ILR'
and ls_cancelable_type.description = 'Not Cancelable'
and ((lsr_ilr.ilr_number in ('AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST','AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST') and lsr_cap_type.description = 'AUTO Cap Type Sales')
	or (lsr_ilr.ilr_number in ('AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF','AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF') and lsr_cap_type.description = 'AUTO Cap Type Direct Finance')
	or (lsr_ilr.ilr_number in ('AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op','AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op') and lsr_cap_type.description = 'AUTO Cap Type Operating')
	or (lsr_ilr.ilr_number in ('AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed') and lsr_cap_type.description = 'AUTO Cap Type Mixed ST-DF'))
and add_months(currency_rate_default_dense.exchange_date, 1) > lsr_ilr.est_in_svc_date /*probably a better way to compare months in the views somewhere*/
and currency_rate_default_dense.exchange_date <= lsr_ilr.est_in_svc_date
and currency_rate_default_dense.currency_from = (select currency_id from currency where iso_code = 'GBP')
and currency_rate_default_dense.currency_to = (select currency_id from currency_schema where company_id = 1 and currency_type_id = 1)
and currency_rate_default_dense.exchange_rate_type_id = 1;

--add the options for the JPY ILRs (no exchange rates)
insert into lsr_ilr_options(ilr_id, revision, purchase_option_type_id, purchase_option_amt, cancelable_type_id, itc_sw, partial_retire_sw, sublet_sw, lease_cap_type_id,
termination_amt, payment_shift, in_service_exchange_rate, intent_to_purchase, specialized_asset, likely_to_collect, sublease_flag, interco_lease_flag)
select ilr_id, 1, purchase_option_type_id, 500, cancelable_type_id, 0, 0, 0, cap_type_id,
200, 0, NULL /*this is because there are no exchange rates for JPY*/, 1, 1, 1, 0, 0
from lsr_ilr, ls_purchase_option_type, ls_cancelable_type, lsr_cap_type
where ls_purchase_option_type.description = 'Flat Amount ILR'
and ls_cancelable_type.description = 'Not Cancelable'
and ((lsr_ilr.ilr_number in ('AUTO-ILR-Monthly-Arr-ST-JPY') and lsr_cap_type.description = 'AUTO Cap Type Sales')
	or (lsr_ilr.ilr_number in ('AUTO-ILR-Monthly-Arr-DF-JPY') and lsr_cap_type.description = 'AUTO Cap Type Direct Finance'));

------------------------------
--ILR Payment Terms (Monthly)
------------------------------
insert into lsr_ilr_payment_term(ilr_id, revision, payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms, payment_amount, contingent_amount,
c_bucket_1, c_bucket_2, c_bucket_3, c_bucket_4, c_bucket_5, c_bucket_6, c_bucket_7, c_bucket_8, c_bucket_9, c_bucket_10,
e_bucket_1, e_bucket_2, e_bucket_3, e_bucket_4, e_bucket_5, e_bucket_6, e_bucket_7, e_bucket_8, e_bucket_9, e_bucket_10)
select ilr_id, 1, 1, payment_term_type_id, to_char(x.open_month, 'dd-mon-yyyy'), payment_freq_id, 60, 800, 400,
200, 200, 0, 0, 0, 0, 0, 0, 0, 0,
200, 200, 0, 0, 0, 0, 0, 0, 0, 0
from lsr_ilr, ls_payment_term_type, ls_payment_freq, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_ilr.ilr_number in ('AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Monthly-Arr-ST-JPY','AUTO-ILR-Monthly-Arr-DF-JPY')
and ls_payment_term_type.description = 'Normal'
and ls_payment_freq.description = 'Monthly'
union
select ilr_id, 1, 2, payment_term_type_id, to_char(add_months(x.open_month, 60), 'dd-mon-yyyy'), payment_freq_id, 12, 750, 800,
400, 400, 0, 0, 0, 0, 0, 0, 0, 0,
400, 400, 0, 0, 0, 0, 0, 0, 0, 0
from lsr_ilr, ls_payment_term_type, ls_payment_freq, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_ilr.ilr_number in ('AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Monthly-Arr-ST-JPY','AUTO-ILR-Monthly-Arr-DF-JPY')
and ls_payment_term_type.description = 'Normal'
and ls_payment_freq.description = 'Monthly';

--------------------------------
--ILR Payment Terms (Quarterly)
--------------------------------
insert into lsr_ilr_payment_term(ilr_id, revision, payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms, payment_amount, contingent_amount,
c_bucket_1, c_bucket_2, c_bucket_3, c_bucket_4, c_bucket_5, c_bucket_6, c_bucket_7, c_bucket_8, c_bucket_9, c_bucket_10,
e_bucket_1, e_bucket_2, e_bucket_3, e_bucket_4, e_bucket_5, e_bucket_6, e_bucket_7, e_bucket_8, e_bucket_9, e_bucket_10)
select ilr_id, 1, 1, payment_term_type_id, to_char(x.open_month, 'dd-mon-yyyy'), payment_freq_id, 12, 2400, 400,
200, 200, 0, 0, 0, 0, 0, 0, 0, 0,
200, 200, 0, 0, 0, 0, 0, 0, 0, 0
from lsr_ilr, ls_payment_term_type, ls_payment_freq, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_ilr.ilr_number in ('AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Quarterly-Pre-Mixed')
and ls_payment_term_type.description = 'Normal'
and ls_payment_freq.description = 'Quarterly'
union
select ilr_id, 1, 2, payment_term_type_id, to_char(add_months(x.open_month, 36), 'dd-mon-yyyy'), payment_freq_id, 12, 2250, 800,
400, 400, 0, 0, 0, 0, 0, 0, 0, 0,
400, 400, 0, 0, 0, 0, 0, 0, 0, 0
from lsr_ilr, ls_payment_term_type, ls_payment_freq, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_ilr.ilr_number in ('AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Quarterly-Pre-Mixed')
and ls_payment_term_type.description = 'Normal'
and ls_payment_freq.description = 'Quarterly';

----------------------------------
--ILR Payment Terms (Semi-Annual)
----------------------------------
insert into lsr_ilr_payment_term(ilr_id, revision, payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms, payment_amount, contingent_amount,
c_bucket_1, c_bucket_2, c_bucket_3, c_bucket_4, c_bucket_5, c_bucket_6, c_bucket_7, c_bucket_8, c_bucket_9, c_bucket_10,
e_bucket_1, e_bucket_2, e_bucket_3, e_bucket_4, e_bucket_5, e_bucket_6, e_bucket_7, e_bucket_8, e_bucket_9, e_bucket_10)
select ilr_id, 1, 1, payment_term_type_id, to_char(x.open_month, 'dd-mon-yyyy'), payment_freq_id, 6, 4800, 400,
200, 200, 0, 0, 0, 0, 0, 0, 0, 0,
200, 200, 0, 0, 0, 0, 0, 0, 0, 0
from lsr_ilr, ls_payment_term_type, ls_payment_freq, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_ilr.ilr_number in ('AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Semi-Annual-Arr-Mixed')
and ls_payment_term_type.description = 'Normal'
and ls_payment_freq.description = 'Semi-Annual'
union
select ilr_id, 1, 2, payment_term_type_id, to_char(add_months(x.open_month, 36), 'dd-mon-yyyy'), payment_freq_id, 6, 4500, 800,
400, 400, 0, 0, 0, 0, 0, 0, 0, 0,
400, 400, 0, 0, 0, 0, 0, 0, 0, 0
from lsr_ilr, ls_payment_term_type, ls_payment_freq, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_ilr.ilr_number in ('AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Semi-Annual-Arr-Mixed')
and ls_payment_term_type.description = 'Normal'
and ls_payment_freq.description = 'Semi-Annual';

-----------------------------
--ILR Payment Terms (Annual)
-----------------------------
insert into lsr_ilr_payment_term(ilr_id, revision, payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms, payment_amount, contingent_amount,
c_bucket_1, c_bucket_2, c_bucket_3, c_bucket_4, c_bucket_5, c_bucket_6, c_bucket_7, c_bucket_8, c_bucket_9, c_bucket_10,
e_bucket_1, e_bucket_2, e_bucket_3, e_bucket_4, e_bucket_5, e_bucket_6, e_bucket_7, e_bucket_8, e_bucket_9, e_bucket_10)
select ilr_id, 1, 1, payment_term_type_id, to_char(x.open_month, 'dd-mon-yyyy'), payment_freq_id, 3, 9600, 400,
200, 200, 0, 0, 0, 0, 0, 0, 0, 0,
200, 200, 0, 0, 0, 0, 0, 0, 0, 0
from lsr_ilr, ls_payment_term_type, ls_payment_freq, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_ilr.ilr_number in ('AUTO-ILR-Annual-Arr-ST','AUTO-ILR-Annual-Pre-ST','AUTO-ILR-Annual-Arr-DF','AUTO-ILR-Annual-Pre-DF','AUTO-ILR-Annual-Arr-Op','AUTO-ILR-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Mixed')
and ls_payment_term_type.description = 'Normal'
and ls_payment_freq.description = 'Annual'
union
select ilr_id, 1, 2, payment_term_type_id, to_char(add_months(x.open_month, 36), 'dd-mon-yyyy'), payment_freq_id, 3, 9000, 800,
400, 400, 0, 0, 0, 0, 0, 0, 0, 0,
400, 400, 0, 0, 0, 0, 0, 0, 0, 0
from lsr_ilr, ls_payment_term_type, ls_payment_freq, (select min(gl_posting_mo_yr) open_month from lsr_process_control where company_id = 1 and lsr_closed is null) x
where lsr_ilr.ilr_number in ('AUTO-ILR-Annual-Arr-ST','AUTO-ILR-Annual-Pre-ST','AUTO-ILR-Annual-Arr-DF','AUTO-ILR-Annual-Pre-DF','AUTO-ILR-Annual-Arr-Op','AUTO-ILR-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Mixed')
and ls_payment_term_type.description = 'Normal'
and ls_payment_freq.description = 'Annual';

--------------------------------------------
--ILR Accounts (Default from the ILR Group)
--------------------------------------------
insert into lsr_ilr_account(ilr_id, int_accrual_account_id, int_expense_account_id, exec_accrual_account_id, exec_expense_account_id, cont_accrual_account_id,
cont_expense_account_id, st_receivable_account_id, lt_receivable_account_id, ar_account_id, unguaran_res_account_id, int_unguaran_res_account_id, sell_profit_loss_account_id, ini_direct_cost_account_id, prop_plant_account_id, curr_gain_loss_acct_id, curr_gain_loss_offset_acct_id, deferred_rent_Acct_id, accrued_rent_acct_id, incurred_costs_account_id, def_costs_account_id, def_selling_profit_account_id)
select ilr_id, int_accrual_account_id, int_expense_account_id, exec_accrual_account_id, exec_expense_account_id, cont_accrual_account_id, cont_expense_account_id, st_receivable_account_id, lt_receivable_account_id, ar_account_id, unguaran_res_account_id, int_unguaran_res_account_id, sell_profit_loss_account_id, ini_direct_cost_account_id, prop_plant_account_id, curr_gain_loss_acct_id, curr_gain_loss_offset_acct_id, deferred_rent_Acct_id, accrued_rent_acct_id, incurred_costs_account_id, def_costs_account_id, def_selling_profit_account_id
from lsr_ilr a
inner join lsr_ilr_group b
on a.ilr_group_id = b.ilr_group_id
where ilr_id in (select ilr_id from lsr_ilr where ilr_number in (
'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed',
'AUTO-ILR-Monthly-Arr-ST-JPY','AUTO-ILR-Monthly-Arr-DF-JPY'));

------------------------------------------------
--ILR Initial Direct Costs
------------------------------------------------
insert into lsr_ilr_initial_direct_cost(ilr_initial_direct_cost_id, ilr_id, revision, idc_group_id, date_incurred, amount, description)
select a.max_idc_id + x.num, x.ilr_id, 1, x.idc_group_id, to_char(y.open_month, 'dd-mon-yyyy'), 2000, 'AUTO Initial Direct Cost'
from
(select Nvl(max(ilr_initial_direct_cost_id),1) max_idc_id
	from lsr_ilr_initial_direct_cost
) a,
(select min(gl_posting_mo_yr) open_month
	from lsr_process_control
	where company_id = 1
	and lsr_closed is null
) y,
(select ilr_id, idc_group_id, rownum as num
	from lsr_ilr, lsr_initial_direct_cost_group
	where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed',
	'AUTO-ILR-Monthly-Arr-ST-JPY','AUTO-ILR-Monthly-Arr-DF-JPY')
	and lsr_initial_direct_cost_group.description = 'AUTO Lessor IDCG'
) x;