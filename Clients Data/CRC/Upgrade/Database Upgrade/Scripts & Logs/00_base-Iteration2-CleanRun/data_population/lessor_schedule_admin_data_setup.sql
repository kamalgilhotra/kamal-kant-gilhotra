---------------
--Pre-MLA Data
---------------
--add the lease group for the MLAs
insert into lsr_lease_group(lease_group_id, description, long_description, workflow_type_id, send_invoices, requires_approval)
select ls_lease_group_seq.nextval, 'AUTO LG1', 'Script-Added Lease Group 1', (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0, 1
from dual;

--add the lessee for the MLA
insert into lsr_lessee(lessee_id, description, long_description, status_code_id, lease_group_id)
select ls_lessor_seq.nextval,'AUTO Lessee1','Script-Added Lessee 1', 1, (select lease_group_id from lsr_lease_group where description = 'AUTO LG1')
from dual;

--add the new cap types to use for the MLA/ILR
insert into lsr_cap_type(cap_type_id, description, long_description, active)
select Nvl(max(cap_type_id) + 1,1) , 'AUTO Cap Type Sales', 'Script-Added Cap Type for Sales-Type Schedules', 1
from lsr_cap_type;
insert into lsr_cap_type(cap_type_id, description, long_description, active)
select Nvl(max(cap_type_id) + 1,1) , 'AUTO Cap Type Direct Finance', 'Script-Added Cap Type for Direct Finance Schedules', 1
from lsr_cap_type;
insert into lsr_cap_type(cap_type_id, description, long_description, active)
select Nvl(max(cap_type_id) + 1,1) , 'AUTO Cap Type Operating', 'Script-Added Cap Type for Operating Schedules',1
from lsr_cap_type;
insert into lsr_cap_type(cap_type_id, description, long_description, active)
select Nvl(max(cap_type_id) + 1,1) , 'AUTO Cap Type Mixed ST-DF', 'Script-Added Cap Type for Operating Schedules', 1
from lsr_cap_type;

--add the FASB associations by SOB for the cap types
insert into lsr_fasb_type_sob(cap_type_id, fasb_cap_type_id, set_of_books_id)
select cap_type_id, fasb_cap_type_id, set_of_books_id
from lsr_cap_type, lsr_fasb_cap_type, set_of_books
where lsr_cap_type.description = 'AUTO Cap Type Sales'
and lsr_fasb_cap_type.description = 'Sales Type';

insert into lsr_fasb_type_sob(cap_type_id, fasb_cap_type_id, set_of_books_id)
select cap_type_id, fasb_cap_type_id, set_of_books_id
from lsr_cap_type, lsr_fasb_cap_type, set_of_books
where lsr_cap_type.description = 'AUTO Cap Type Direct Finance'
and lsr_fasb_cap_type.description = 'Direct Finance';

insert into lsr_fasb_type_sob(cap_type_id, fasb_cap_type_id, set_of_books_id)
select cap_type_id, fasb_cap_type_id, set_of_books_id
from lsr_cap_type, lsr_fasb_cap_type, set_of_books
where lsr_cap_type.description = 'AUTO Cap Type Operating'
and lsr_fasb_cap_type.description = 'Operating';

insert into lsr_fasb_type_sob(cap_type_id, fasb_cap_type_id, set_of_books_id)
select cap_type_id, fasb_cap_type_id, set_of_books_id
from lsr_cap_type, lsr_fasb_cap_type, set_of_books
where lsr_cap_type.description = 'AUTO Cap Type Mixed ST-DF'
and ((lsr_fasb_cap_type.description = 'Sales Type' and set_of_books.set_of_books_id not in (2,4))
or (lsr_fasb_cap_type.description = 'Direct Finance' and set_of_books.set_of_books_id in (2,4)));

---------------
--Pre-ILR Data
---------------
--add the ILR group for the ILR
insert into lsr_ilr_group(ilr_group_id, description, long_description, workflow_type_id, payment_shift, int_accrual_account_id, int_expense_account_id, exec_accrual_account_id, exec_expense_account_id, cont_accrual_account_id, cont_expense_account_id, st_receivable_account_id, lt_receivable_account_id, ar_account_id, unguaran_res_account_id, int_unguaran_res_account_id, sell_profit_loss_account_id, ini_direct_cost_account_id, prop_plant_account_id, curr_gain_loss_acct_id, curr_gain_loss_offset_acct_id, deferred_rent_Acct_id, accrued_rent_acct_id, incurred_costs_account_id, def_costs_account_id, def_selling_profit_account_id)
select ls_ilr_group_seq.nextval, 'AUTO ILRG1', 'Script-Added ILR Group 1', (select workflow_type_id from workflow_type where lower(description) like '%auto approve%' and subsystem like '%lessor%'), 0, account_id, account_id, account_id, account_id, account_id, account_id, account_id, account_id, account_id, account_id, account_id, account_id, account_id, account_id, account_id, account_id, account_id, account_id, account_id, account_id, account_id
from (select min(gl_account_id) account_id
	from gl_account
	where account_type_id = 11);

--add the Initial Direct Cost Group
insert into lsr_initial_direct_cost_group(idc_group_id, description, long_description)
select Nvl(max(idc_group_id) + 1,1), 'AUTO Lessor IDCG', 'Script-Added Initial Direct Cost Group for Lessor testing'
from lsr_initial_direct_cost_group;


------------------------
--Multi-Currency Data
------------------------
--add a new currency to use on some ILRs to make sure schedules cannot be built without rates loaded
merge into currency a
	using (select 6 currency_id, 'JPY' description, 1 currency_display_factor, '¥' currency_display_symbol, 'JPY' iso_code from dual) x
	on (a.currency_id = x.currency_id)
when matched then
	update set description = x.description, currency_display_factor = x.currency_display_factor, currency_display_symbol = x.currency_display_symbol, iso_code = x.iso_code
when not matched then
	insert (currency_id, description, currency_display_factor, currency_display_symbol, iso_code)
	values (x.currency_id, x.description, x.currency_display_factor, x.currency_display_symbol, x.iso_code);