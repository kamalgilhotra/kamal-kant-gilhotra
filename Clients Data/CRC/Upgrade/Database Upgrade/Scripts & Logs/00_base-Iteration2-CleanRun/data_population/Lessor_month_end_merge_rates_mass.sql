/*The following scripts insert rate for every day from January 1st, 1990 until the current Lessor open month (up to 10,000 rates per currency combination).
The rates inserted repeat every 31 days for each different from/to currency combination*/

-----------------------------------------------
--Generate Rates for From/To currencies 1 and 2
-----------------------------------------------
merge into currency_rate a
using
(SELECT exchange_date,
       currency_from,
       currency_to,
       exchange_rate_type_id,
       sysdate,
       user,
       case when to_char(exchange_date, 'DD') between 1 and 2 then 1.23456
       when to_char(exchange_date, 'DD') between 3 and 4 then 1.34567
       when to_char(exchange_date, 'DD') between 5 and 6 then 1.45678
       when to_char(exchange_date, 'DD') between 7 and 8 then 1.56789
       when to_char(exchange_date, 'DD') between 9 and 10 then 1.67890
       when to_char(exchange_date, 'DD') between 11 and 12 then 1.78901
       when to_char(exchange_date, 'DD') between 13 and 14 then 1.89012
       when to_char(exchange_date, 'DD') between 15 and 16 then 1.90123
       when to_char(exchange_date, 'DD') between 17 and 18 then 1.23456
       when to_char(exchange_date, 'DD') between 19 and 20 then 1.34567
       when to_char(exchange_date, 'DD') between 21 and 22 then 1.45678
       when to_char(exchange_date, 'DD') between 23 and 24 then 1.56789
       when to_char(exchange_date, 'DD') between 25 and 26 then 1.67890
       when to_char(exchange_date, 'DD') between 27 and 28 then 1.78901
       when to_char(exchange_date, 'DD') between 29 and 30 then 1.89012
       else 1.90123 end rate
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       1
                       currency_from,
                       2
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);



merge into currency_rate a
using
(SELECT b.exchange_date,
       b.currency_from,
       b.currency_to,
       b.exchange_rate_type_id,
       Round(1 / a.rate, 6) rate,
       sysdate,
       user
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       2
                       currency_from,
                       1
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b
       inner join currency_rate a
               ON a.exchange_date = b.exchange_date
                  AND a.currency_from = b.currency_to
                  AND a.currency_to = b.currency_from) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);


-----------------------------------------------
--Generate Rates for From/To currencies 1 and 3
-----------------------------------------------
merge into currency_rate a
using
(SELECT exchange_date,
       currency_from,
       currency_to,
       exchange_rate_type_id,
       sysdate,
       user,
       case when to_char(exchange_date, 'DD') between 1 and 2 then 1.23456
       when to_char(exchange_date, 'DD') between 3 and 4 then 1.34567
       when to_char(exchange_date, 'DD') between 5 and 6 then 1.45678
       when to_char(exchange_date, 'DD') between 7 and 8 then 1.56789
       when to_char(exchange_date, 'DD') between 9 and 10 then 1.67890
       when to_char(exchange_date, 'DD') between 11 and 12 then 1.78901
       when to_char(exchange_date, 'DD') between 13 and 14 then 1.89012
       when to_char(exchange_date, 'DD') between 15 and 16 then 1.90123
       when to_char(exchange_date, 'DD') between 17 and 18 then 1.23456
       when to_char(exchange_date, 'DD') between 19 and 20 then 1.34567
       when to_char(exchange_date, 'DD') between 21 and 22 then 1.45678
       when to_char(exchange_date, 'DD') between 23 and 24 then 1.56789
       when to_char(exchange_date, 'DD') between 25 and 26 then 1.67890
       when to_char(exchange_date, 'DD') between 27 and 28 then 1.78901
       when to_char(exchange_date, 'DD') between 29 and 30 then 1.89012
       else 1.90123 end rate
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       1
                       currency_from,
                       3
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);

merge into currency_rate a
using
(SELECT b.exchange_date,
       b.currency_from,
       b.currency_to,
       b.exchange_rate_type_id,
       Round(1 / a.rate, 6) rate,
       sysdate,
       user
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       3
                       currency_from,
                       1
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b
       inner join currency_rate a
               ON a.exchange_date = b.exchange_date
                  AND a.currency_from = b.currency_to
                  AND a.currency_to = b.currency_from) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);


-----------------------------------------------
--Generate Rates for From/To currencies 1 and 4
-----------------------------------------------
merge into currency_rate a
using
(SELECT exchange_date,
       currency_from,
       currency_to,
       exchange_rate_type_id,
       sysdate,
       user,
       case when to_char(exchange_date, 'DD') between 1 and 2 then 1.23456
       when to_char(exchange_date, 'DD') between 3 and 4 then 1.34567
       when to_char(exchange_date, 'DD') between 5 and 6 then 1.45678
       when to_char(exchange_date, 'DD') between 7 and 8 then 1.56789
       when to_char(exchange_date, 'DD') between 9 and 10 then 1.67890
       when to_char(exchange_date, 'DD') between 11 and 12 then 1.78901
       when to_char(exchange_date, 'DD') between 13 and 14 then 1.89012
       when to_char(exchange_date, 'DD') between 15 and 16 then 1.90123
       when to_char(exchange_date, 'DD') between 17 and 18 then 1.23456
       when to_char(exchange_date, 'DD') between 19 and 20 then 1.34567
       when to_char(exchange_date, 'DD') between 21 and 22 then 1.45678
       when to_char(exchange_date, 'DD') between 23 and 24 then 1.56789
       when to_char(exchange_date, 'DD') between 25 and 26 then 1.67890
       when to_char(exchange_date, 'DD') between 27 and 28 then 1.78901
       when to_char(exchange_date, 'DD') between 29 and 30 then 1.89012
       else 1.90123 end rate
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       1
                       currency_from,
                       4
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);

merge into currency_rate a
using
(SELECT b.exchange_date,
       b.currency_from,
       b.currency_to,
       b.exchange_rate_type_id,
       Round(1 / a.rate, 6) rate,
       sysdate,
       user
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       4
                       currency_from,
                       1
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b
       inner join currency_rate a
               ON a.exchange_date = b.exchange_date
                  AND a.currency_from = b.currency_to
                  AND a.currency_to = b.currency_from) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);


-----------------------------------------------
--Generate Rates for From/To currencies 1 and 5
-----------------------------------------------
merge into currency_rate a
using
(SELECT exchange_date,
       currency_from,
       currency_to,
       exchange_rate_type_id,
       sysdate,
       user,
       case when to_char(exchange_date, 'DD') between 1 and 2 then 1.23456
       when to_char(exchange_date, 'DD') between 3 and 4 then 1.34567
       when to_char(exchange_date, 'DD') between 5 and 6 then 1.45678
       when to_char(exchange_date, 'DD') between 7 and 8 then 1.56789
       when to_char(exchange_date, 'DD') between 9 and 10 then 1.67890
       when to_char(exchange_date, 'DD') between 11 and 12 then 1.78901
       when to_char(exchange_date, 'DD') between 13 and 14 then 1.89012
       when to_char(exchange_date, 'DD') between 15 and 16 then 1.90123
       when to_char(exchange_date, 'DD') between 17 and 18 then 1.23456
       when to_char(exchange_date, 'DD') between 19 and 20 then 1.34567
       when to_char(exchange_date, 'DD') between 21 and 22 then 1.45678
       when to_char(exchange_date, 'DD') between 23 and 24 then 1.56789
       when to_char(exchange_date, 'DD') between 25 and 26 then 1.67890
       when to_char(exchange_date, 'DD') between 27 and 28 then 1.78901
       when to_char(exchange_date, 'DD') between 29 and 30 then 1.89012
       else 1.90123 end rate
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       1
                       currency_from,
                       5
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);

merge into currency_rate a
using
(SELECT b.exchange_date,
       b.currency_from,
       b.currency_to,
       b.exchange_rate_type_id,
       Round(1 / a.rate, 6) rate,
       sysdate,
       user
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       5
                       currency_from,
                       1
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b
       inner join currency_rate a
               ON a.exchange_date = b.exchange_date
                  AND a.currency_from = b.currency_to
                  AND a.currency_to = b.currency_from) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);


-----------------------------------------------
--Generate Rates for From/To currencies 2 and 3
-----------------------------------------------
merge into currency_rate a
using
(SELECT exchange_date,
       currency_from,
       currency_to,
       exchange_rate_type_id,
       sysdate,
       user,
       case when to_char(exchange_date, 'DD') between 1 and 2 then 1.23456
       when to_char(exchange_date, 'DD') between 3 and 4 then 1.34567
       when to_char(exchange_date, 'DD') between 5 and 6 then 1.45678
       when to_char(exchange_date, 'DD') between 7 and 8 then 1.56789
       when to_char(exchange_date, 'DD') between 9 and 10 then 1.67890
       when to_char(exchange_date, 'DD') between 11 and 12 then 1.78901
       when to_char(exchange_date, 'DD') between 13 and 14 then 1.89012
       when to_char(exchange_date, 'DD') between 15 and 16 then 1.90123
       when to_char(exchange_date, 'DD') between 17 and 18 then 1.23456
       when to_char(exchange_date, 'DD') between 19 and 20 then 1.34567
       when to_char(exchange_date, 'DD') between 21 and 22 then 1.45678
       when to_char(exchange_date, 'DD') between 23 and 24 then 1.56789
       when to_char(exchange_date, 'DD') between 25 and 26 then 1.67890
       when to_char(exchange_date, 'DD') between 27 and 28 then 1.78901
       when to_char(exchange_date, 'DD') between 29 and 30 then 1.89012
       else 1.90123 end rate
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       2
                       currency_from,
                       3
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);

merge into currency_rate a
using
(SELECT b.exchange_date,
       b.currency_from,
       b.currency_to,
       b.exchange_rate_type_id,
       Round(1 / a.rate, 6) rate,
       sysdate,
       user
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       3
                       currency_from,
                       2
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b
       inner join currency_rate a
               ON a.exchange_date = b.exchange_date
                  AND a.currency_from = b.currency_to
                  AND a.currency_to = b.currency_from) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);


-----------------------------------------------
--Generate Rates for From/To currencies 2 and 4
-----------------------------------------------
merge into currency_rate a
using
(SELECT exchange_date,
       currency_from,
       currency_to,
       exchange_rate_type_id,
       sysdate,
       user,
       case when to_char(exchange_date, 'DD') between 1 and 2 then 1.23456
       when to_char(exchange_date, 'DD') between 3 and 4 then 1.34567
       when to_char(exchange_date, 'DD') between 5 and 6 then 1.45678
       when to_char(exchange_date, 'DD') between 7 and 8 then 1.56789
       when to_char(exchange_date, 'DD') between 9 and 10 then 1.67890
       when to_char(exchange_date, 'DD') between 11 and 12 then 1.78901
       when to_char(exchange_date, 'DD') between 13 and 14 then 1.89012
       when to_char(exchange_date, 'DD') between 15 and 16 then 1.90123
       when to_char(exchange_date, 'DD') between 17 and 18 then 1.23456
       when to_char(exchange_date, 'DD') between 19 and 20 then 1.34567
       when to_char(exchange_date, 'DD') between 21 and 22 then 1.45678
       when to_char(exchange_date, 'DD') between 23 and 24 then 1.56789
       when to_char(exchange_date, 'DD') between 25 and 26 then 1.67890
       when to_char(exchange_date, 'DD') between 27 and 28 then 1.78901
       when to_char(exchange_date, 'DD') between 29 and 30 then 1.89012
       else 1.90123 end rate
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       2
                       currency_from,
                       4
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);

merge into currency_rate a
using
(SELECT b.exchange_date,
       b.currency_from,
       b.currency_to,
       b.exchange_rate_type_id,
       Round(1 / a.rate, 6) rate,
       sysdate,
       user
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       4
                       currency_from,
                       2
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b
       inner join currency_rate a
               ON a.exchange_date = b.exchange_date
                  AND a.currency_from = b.currency_to
                  AND a.currency_to = b.currency_from) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);


-----------------------------------------------
--Generate Rates for From/To currencies 2 and 5
-----------------------------------------------
merge into currency_rate a
using
(SELECT exchange_date,
       currency_from,
       currency_to,
       exchange_rate_type_id,
       sysdate,
       user,
       case when to_char(exchange_date, 'DD') between 1 and 2 then 1.23456
       when to_char(exchange_date, 'DD') between 3 and 4 then 1.34567
       when to_char(exchange_date, 'DD') between 5 and 6 then 1.45678
       when to_char(exchange_date, 'DD') between 7 and 8 then 1.56789
       when to_char(exchange_date, 'DD') between 9 and 10 then 1.67890
       when to_char(exchange_date, 'DD') between 11 and 12 then 1.78901
       when to_char(exchange_date, 'DD') between 13 and 14 then 1.89012
       when to_char(exchange_date, 'DD') between 15 and 16 then 1.90123
       when to_char(exchange_date, 'DD') between 17 and 18 then 1.23456
       when to_char(exchange_date, 'DD') between 19 and 20 then 1.34567
       when to_char(exchange_date, 'DD') between 21 and 22 then 1.45678
       when to_char(exchange_date, 'DD') between 23 and 24 then 1.56789
       when to_char(exchange_date, 'DD') between 25 and 26 then 1.67890
       when to_char(exchange_date, 'DD') between 27 and 28 then 1.78901
       when to_char(exchange_date, 'DD') between 29 and 30 then 1.89012
       else 1.90123 end rate
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       2
                       currency_from,
                       5
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);

merge into currency_rate a
using
(SELECT b.exchange_date,
       b.currency_from,
       b.currency_to,
       b.exchange_rate_type_id,
       Round(1 / a.rate, 6) rate,
       sysdate,
       user
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       5
                       currency_from,
                       2
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b
       inner join currency_rate a
               ON a.exchange_date = b.exchange_date
                  AND a.currency_from = b.currency_to
                  AND a.currency_to = b.currency_from) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);


-----------------------------------------------
--Generate Rates for From/To currencies 3 and 4
-----------------------------------------------
merge into currency_rate a
using
(SELECT exchange_date,
       currency_from,
       currency_to,
       exchange_rate_type_id,
       sysdate,
       user,
       case when to_char(exchange_date, 'DD') between 1 and 2 then 1.23456
       when to_char(exchange_date, 'DD') between 3 and 4 then 1.34567
       when to_char(exchange_date, 'DD') between 5 and 6 then 1.45678
       when to_char(exchange_date, 'DD') between 7 and 8 then 1.56789
       when to_char(exchange_date, 'DD') between 9 and 10 then 1.67890
       when to_char(exchange_date, 'DD') between 11 and 12 then 1.78901
       when to_char(exchange_date, 'DD') between 13 and 14 then 1.89012
       when to_char(exchange_date, 'DD') between 15 and 16 then 1.90123
       when to_char(exchange_date, 'DD') between 17 and 18 then 1.23456
       when to_char(exchange_date, 'DD') between 19 and 20 then 1.34567
       when to_char(exchange_date, 'DD') between 21 and 22 then 1.45678
       when to_char(exchange_date, 'DD') between 23 and 24 then 1.56789
       when to_char(exchange_date, 'DD') between 25 and 26 then 1.67890
       when to_char(exchange_date, 'DD') between 27 and 28 then 1.78901
       when to_char(exchange_date, 'DD') between 29 and 30 then 1.89012
       else 1.90123 end rate
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       3
                       currency_from,
                       4
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);

merge into currency_rate a
using
(SELECT b.exchange_date,
       b.currency_from,
       b.currency_to,
       b.exchange_rate_type_id,
       Round(1 / a.rate, 6) rate,
       sysdate,
       user
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       4
                       currency_from,
                       3
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b
       inner join currency_rate a
               ON a.exchange_date = b.exchange_date
                  AND a.currency_from = b.currency_to
                  AND a.currency_to = b.currency_from) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);


-----------------------------------------------
--Generate Rates for From/To currencies 3 and 5
-----------------------------------------------
merge into currency_rate a
using
(SELECT exchange_date,
       currency_from,
       currency_to,
       exchange_rate_type_id,
       sysdate,
       user,
       case when to_char(exchange_date, 'DD') between 1 and 2 then 1.23456
       when to_char(exchange_date, 'DD') between 3 and 4 then 1.34567
       when to_char(exchange_date, 'DD') between 5 and 6 then 1.45678
       when to_char(exchange_date, 'DD') between 7 and 8 then 1.56789
       when to_char(exchange_date, 'DD') between 9 and 10 then 1.67890
       when to_char(exchange_date, 'DD') between 11 and 12 then 1.78901
       when to_char(exchange_date, 'DD') between 13 and 14 then 1.89012
       when to_char(exchange_date, 'DD') between 15 and 16 then 1.90123
       when to_char(exchange_date, 'DD') between 17 and 18 then 1.23456
       when to_char(exchange_date, 'DD') between 19 and 20 then 1.34567
       when to_char(exchange_date, 'DD') between 21 and 22 then 1.45678
       when to_char(exchange_date, 'DD') between 23 and 24 then 1.56789
       when to_char(exchange_date, 'DD') between 25 and 26 then 1.67890
       when to_char(exchange_date, 'DD') between 27 and 28 then 1.78901
       when to_char(exchange_date, 'DD') between 29 and 30 then 1.89012
       else 1.90123 end rate
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       3
                       currency_from,
                       5
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);

merge into currency_rate a
using
(SELECT b.exchange_date,
       b.currency_from,
       b.currency_to,
       b.exchange_rate_type_id,
       Round(1 / a.rate, 6) rate,
       sysdate,
       user
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       5
                       currency_from,
                       3
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b
       inner join currency_rate a
               ON a.exchange_date = b.exchange_date
                  AND a.currency_from = b.currency_to
                  AND a.currency_to = b.currency_from) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);


-----------------------------------------------
--Generate Rates for From/To currencies 4 and 5
-----------------------------------------------
merge into currency_rate a
using
(SELECT exchange_date,
       currency_from,
       currency_to,
       exchange_rate_type_id,
       sysdate,
       user,
       case when to_char(exchange_date, 'DD') between 1 and 2 then 1.23456
       when to_char(exchange_date, 'DD') between 3 and 4 then 1.34567
       when to_char(exchange_date, 'DD') between 5 and 6 then 1.45678
       when to_char(exchange_date, 'DD') between 7 and 8 then 1.56789
       when to_char(exchange_date, 'DD') between 9 and 10 then 1.67890
       when to_char(exchange_date, 'DD') between 11 and 12 then 1.78901
       when to_char(exchange_date, 'DD') between 13 and 14 then 1.89012
       when to_char(exchange_date, 'DD') between 15 and 16 then 1.90123
       when to_char(exchange_date, 'DD') between 17 and 18 then 1.23456
       when to_char(exchange_date, 'DD') between 19 and 20 then 1.34567
       when to_char(exchange_date, 'DD') between 21 and 22 then 1.45678
       when to_char(exchange_date, 'DD') between 23 and 24 then 1.56789
       when to_char(exchange_date, 'DD') between 25 and 26 then 1.67890
       when to_char(exchange_date, 'DD') between 27 and 28 then 1.78901
       when to_char(exchange_date, 'DD') between 29 and 30 then 1.89012
       else 1.90123 end rate
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       4
                       currency_from,
                       5
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);

merge into currency_rate a
using
(SELECT b.exchange_date,
       b.currency_from,
       b.currency_to,
       b.exchange_rate_type_id,
       Round(1 / a.rate, 6) rate,
       sysdate,
       user
FROM   (SELECT exchange_date,
               currency_from,
               currency_to,
               exchange_rate_type_id
        FROM   (SELECT To_date('01-jan-1999', 'DD-MON-YYYY') - 1 + LEVEL AS
                       exchange_date,
                       5
                       currency_from,
                       4
                       currency_to,
                       1
                       exchange_rate_type_id
                FROM   dual
                CONNECT BY LEVEL <= 20000)
        WHERE  exchange_date <= (SELECT Min(Last_Day(gl_posting_mo_yr))
                                 FROM   lsr_process_control
                                 WHERE  company_id = 1
                                        AND lsr_closed IS NULL)) b
       inner join currency_rate a
               ON a.exchange_date = b.exchange_date
                  AND a.currency_from = b.currency_to
                  AND a.currency_to = b.currency_from) c
on (a.exchange_date = c.exchange_date
and a.currency_from = c.currency_from
and a.currency_to = c.currency_to
and a.exchange_rate_type_id = c.exchange_rate_type_id)
when matched then update
set a.rate = c.rate
when not matched then insert values (c.exchange_date, c.currency_from, c.currency_to, c.exchange_rate_type_id, sysdate, user, c.rate);
