-------------------------
--Remove the Lessor Assets added
-------------------------
delete from lsr_asset where lsr_asset.description in (
'AUTO-Asset-Monthly-Arr-ST1','AUTO-Asset-Monthly-Arr-ST2','AUTO-Asset-Monthly-Arr-ST3',
'AUTO-Asset-Quarterly-Arr-ST1','AUTO-Asset-Quarterly-Arr-ST2','AUTO-Asset-Quarterly-Arr-ST3',
'AUTO-Asset-Semi-Annual-Arr-ST1','AUTO-Asset-Semi-Annual-Arr-ST2','AUTO-Asset-Semi-Annual-Arr-ST3',
'AUTO-Asset-Annual-Arr-ST1','AUTO-Asset-Annual-Arr-ST2','AUTO-Asset-Annual-Arr-ST3',
'AUTO-Asset-Monthly-Pre-ST1','AUTO-Asset-Monthly-Pre-ST2','AUTO-Asset-Monthly-Pre-ST3',
'AUTO-Asset-Quarterly-Pre-ST1','AUTO-Asset-Quarterly-Pre-ST2','AUTO-Asset-Quarterly-Pre-ST3',
'AUTO-Asset-Semi-Annual-Pre-ST1','AUTO-Asset-Semi-Annual-Pre-ST2','AUTO-Asset-Semi-Annual-Pre-ST3',
'AUTO-Asset-Annual-Pre-ST1','AUTO-Asset-Annual-Pre-ST2','AUTO-Asset-Annual-Pre-ST3',
'AUTO-Asset-Monthly-Arr-DF1','AUTO-Asset-Monthly-Arr-DF2','AUTO-Asset-Monthly-Arr-DF3',
'AUTO-Asset-Quarterly-Arr-DF1','AUTO-Asset-Quarterly-Arr-DF2','AUTO-Asset-Quarterly-Arr-DF3',
'AUTO-Asset-Semi-Annual-Arr-DF1','AUTO-Asset-Semi-Annual-Arr-DF2','AUTO-Asset-Semi-Annual-Arr-DF3',
'AUTO-Asset-Annual-Arr-DF1','AUTO-Asset-Annual-Arr-DF2','AUTO-Asset-Annual-Arr-DF3',
'AUTO-Asset-Monthly-Pre-DF1','AUTO-Asset-Monthly-Pre-DF2','AUTO-Asset-Monthly-Pre-DF3',
'AUTO-Asset-Quarterly-Pre-DF1','AUTO-Asset-Quarterly-Pre-DF2','AUTO-Asset-Quarterly-Pre-DF3',
'AUTO-Asset-Semi-Annual-Pre-DF1','AUTO-Asset-Semi-Annual-Pre-DF2','AUTO-Asset-Semi-Annual-Pre-DF3',
'AUTO-Asset-Annual-Pre-DF1','AUTO-Asset-Annual-Pre-DF2','AUTO-Asset-Annual-Pre-DF3',
'AUTO-Asset-Monthly-Arr-Op1','AUTO-Asset-Monthly-Arr-Op2','AUTO-Asset-Monthly-Arr-Op3',
'AUTO-Asset-Quarterly-Arr-Op1','AUTO-Asset-Quarterly-Arr-Op2','AUTO-Asset-Quarterly-Arr-Op3',
'AUTO-Asset-Semi-Annual-Arr-Op1','AUTO-Asset-Semi-Annual-Arr-Op2','AUTO-Asset-Semi-Annual-Arr-Op3',
'AUTO-Asset-Annual-Arr-Op1','AUTO-Asset-Annual-Arr-Op2','AUTO-Asset-Annual-Arr-Op3',
'AUTO-Asset-Monthly-Pre-Op1','AUTO-Asset-Monthly-Pre-Op2','AUTO-Asset-Monthly-Pre-Op3',
'AUTO-Asset-Quarterly-Pre-Op1','AUTO-Asset-Quarterly-Pre-Op2','AUTO-Asset-Quarterly-Pre-Op3',
'AUTO-Asset-Semi-Annual-Pre-Op1','AUTO-Asset-Semi-Annual-Pre-Op2','AUTO-Asset-Semi-Annual-Pre-Op3',
'AUTO-Asset-Annual-Pre-Op1','AUTO-Asset-Annual-Pre-Op2','AUTO-Asset-Annual-Pre-Op3',
'AUTO-Asset-Monthly-Arr-Mixed1','AUTO-Asset-Monthly-Arr-Mixed2','AUTO-Asset-Monthly-Arr-Mixed3',
'AUTO-Asset-Quarterly-Pre-Mixed1','AUTO-Asset-Quarterly-Pre-Mixed2','AUTO-Asset-Quarterly-Pre-Mixed3',
'AUTO-Asset-Semi-Annual-Arr-Mixed1','AUTO-Asset-Semi-Annual-Arr-Mixed2','AUTO-Asset-Semi-Annual-Arr-Mixed3',
'AUTO-Asset-Annual-Pre-Mixed1','AUTO-Asset-Annual-Pre-Mixed2','AUTO-Asset-Annual-Pre-Mixed3'
);

------------------------------
--Remove any rows from calculated tables (schedules, rates, amounts)
------------------------------
delete from lsr_ilr_amounts where ilr_id in (select ilr_id from lsr_ilr where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed')
);

delete from lsr_ilr_schedule_sales_direct where ilr_id in (select ilr_id from lsr_ilr where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed')
);

delete from lsr_ilr_schedule_direct_fin where ilr_id in (select ilr_id from lsr_ilr where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed')
);

delete from lsr_ilr_schedule where ilr_id in (select ilr_id from lsr_ilr where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed')
);

delete from lsr_ilr_rates where ilr_id in (select ilr_id from lsr_ilr where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed')
);

--------------------------
--Delete any ILR information that could have been added in the UI
--------------------------

delete from lsr_ilr_document where ilr_id in (select ilr_id from lsr_ilr where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed')
);

delete from lsr_ilr_class_code where ilr_id in (select ilr_id from lsr_ilr where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed')
);

delete from lsr_ilr_renewal_options where ilr_renewal_id in (
	select ilr_renewal_id from lsr_ilr_renewal where ilr_id in (
		select ilr_id from lsr_ilr where lsr_ilr.ilr_number in (
			'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
			'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
			'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
			'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
			'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
			'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
			'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed'
		)
	)
);

delete from lsr_ilr_renewal where ilr_id in (select ilr_id from lsr_ilr where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed')
);

delete from lsr_ilr_payment_term_var_pay where ilr_id in (select ilr_id from lsr_ilr where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed')
);

-------------------------
--Delete the ILR information we added in the scripts
-------------------------
delete from lsr_ilr_initial_direct_cost where ilr_id in (select ilr_id from lsr_ilr where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed')
);

delete from lsr_ilr_account where ilr_id in (select ilr_id from lsr_ilr where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed')
);

delete from lsr_ilr_payment_term where ilr_id in (select ilr_id from lsr_ilr where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed')
);

delete from lsr_ilr_options where ilr_id in (select ilr_id from lsr_ilr where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed')
);

delete from lsr_ilr_approval where ilr_id in (select ilr_id from lsr_ilr where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed')
);

delete from lsr_ilr where lsr_ilr.ilr_number in (
	'AUTO-ILR-Monthly-Arr-ST','AUTO-ILR-Quarterly-Arr-ST','AUTO-ILR-Semi-Annual-Arr-ST','AUTO-ILR-Annual-Arr-ST',
	'AUTO-ILR-Monthly-Pre-ST','AUTO-ILR-Quarterly-Pre-ST','AUTO-ILR-Semi-Annual-Pre-ST','AUTO-ILR-Annual-Pre-ST',
	'AUTO-ILR-Monthly-Arr-DF','AUTO-ILR-Quarterly-Arr-DF','AUTO-ILR-Semi-Annual-Arr-DF','AUTO-ILR-Annual-Arr-DF',
	'AUTO-ILR-Monthly-Pre-DF','AUTO-ILR-Quarterly-Pre-DF','AUTO-ILR-Semi-Annual-Pre-DF','AUTO-ILR-Annual-Pre-DF',
	'AUTO-ILR-Monthly-Arr-Op','AUTO-ILR-Quarterly-Arr-Op','AUTO-ILR-Semi-Annual-Arr-Op','AUTO-ILR-Annual-Arr-Op',
	'AUTO-ILR-Monthly-Pre-Op','AUTO-ILR-Quarterly-Pre-Op','AUTO-ILR-Semi-Annual-Pre-Op','AUTO-ILR-Annual-Pre-Op',
	'AUTO-ILR-Monthly-Arr-Mixed','AUTO-ILR-Semi-Annual-Arr-Mixed','AUTO-ILR-Quarterly-Pre-Mixed','AUTO-ILR-Annual-Pre-Mixed')
);

--------------------------
--Delete any MLA information that could have been added in the UI
--------------------------
delete from lsr_lease_class_code where lease_id in (select lease_id from lsr_lease where lsr_lease.lease_number in (
	'AUTO Fixed MLA1','AUTO Variable MLA1','AUTO Prepay MLA1','AUTO Arrears MLA1'
);

delete from lsr_lease_document where lease_id in (select lease_id from lsr_lease where lsr_lease.lease_number in (
	'AUTO Fixed MLA1','AUTO Variable MLA1','AUTO Prepay MLA1','AUTO Arrears MLA1'
);

-------------------------
--Delete the MLA information we added in the scripts
-------------------------
delete from lsr_lease_options where lease_id in (select lease_id from lsr_lease where lsr_lease.lease_number in (
	'AUTO Fixed MLA1','AUTO Variable MLA1','AUTO Prepay MLA1','AUTO Arrears MLA1'
);

delete from lsr_lease_company where lease_id in (select lease_id from lsr_lease where lsr_lease.lease_number in (
	'AUTO Fixed MLA1','AUTO Variable MLA1','AUTO Prepay MLA1','AUTO Arrears MLA1'
);

delete from lsr_lease_approval where lease_id in (select lease_id from lsr_lease where lsr_lease.lease_number in (
	'AUTO Fixed MLA1','AUTO Variable MLA1','AUTO Prepay MLA1','AUTO Arrears MLA1'
);

delete from lsr_lease where lsr_lease.lease_number in (
	'AUTO Fixed MLA1','AUTO Variable MLA1','AUTO Prepay MLA1','AUTO Arrears MLA1'
);

-------------------------
--Delete the ILR admin data we added
-------------------------
delete from lsr_initial_direct_cost_group where description = 'AUTO Lessor IDCG';

delete from lsr_ilr_group where description = 'AUTO ILRG1';

-------------------------
--Delete the MLA admin data we added
-------------------------
delete from lsr_fasb_type_sob where cap_type_id in (select cap_type_id from lsr_cap_type where description in (
	'AUTO Cap Type Sales','AUTO Cap Type Direct Finance','AUTO Cap Type Operating','AUTO Cap Type Mixed ST-DF'
);

delete from lsr_cap_type where description in (
	'AUTO Cap Type Sales','AUTO Cap Type Direct Finance','AUTO Cap Type Operating','AUTO Cap Type Mixed ST-DF'
);

delete from lsr_lessee where description = 'AUTO Lessee1';

delete from lsr_lease_group where description = 'AUTO LG1';