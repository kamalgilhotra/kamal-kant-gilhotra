/*
||============================================================================
|| Application: PowerPlan
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2017.4.0.0 05/13/2018  Anand R     2017.4.0.0 Release
||============================================================================
*/

To run this Point Release, unzip the files into their own directory.

This upgrade can upgrade a database that is at version 10.3.2.0 and up.
It will dynamically create a master call script according to the databases version
   and what updates have been previously run.  It will warn you if the database version
   is not correct and if the Property Tax or Provision modules are not at the correct
   version to continue.  This version will drop and create the Property Tax module if it isn't
   currently being used.

*** WARNING: Running the v2017.4.0.0_create_upgrade.bat which calls the create_v2017.4.0.0_upgrade.sql
             script will OVERWRITE the master call script (run_auto_generated_v2017.4.0.0_upgrade.sql)
             which is called from the v2017.4.0.0_run_upgrade.bat file.

             You may want to delete the v2017.4.0.0_create_upgrade.bat and create_v2017.4.0.0_upgrade.sql
             files once you have generated the master call script.


Steps for running on a Windows platform:

1. Log onto the database as the SYS user and run the v10_sys_grants.sql script.  This should
   be done by the client DBA.
   This is documented in the create_v2017.4.0.0_upgrade.sql script.
2. Edit the v2017.4.0.0_create_upgrade.bat file and substitute the correct password and database SID.
   The user id (Schema Owner) should be PWRPLANT and should not be changed.
3. Double click the v2017.4.0.0_create_upgrade.bat which will call the create_v2017.4.0.0_upgrade.sql script.
   This script will dynamically generate the run_auto_generated_v2017.4.0.0_upgrade.sql script.
4. Review the run_auto_generated_v2017.4.0.0_upgrade.sql script and verify the versions and the scripts
   that will be executed.  Pay attention to the results for the Property Tax and Provision modules.
5. Edit the v2017.4.0.0_run_upgrade.bat file and substitute the correct password and database SID.
   The user id (Schema Owner) should be PWRPLANT and should not be changed.
6. Double click the v2017.4.0.0_run_upgrade.bat which will call the run_auto_generated_v2017.4.0.0_upgrade.sql
   script that was generated in step 4.
7. Check all log files for errors.


Steps for running on a Unix Platform:

1. Log onto the database as the SYS user and run the v10_sys_grants.sql script.  This should
   be done by the client DBA.
   This is documented in the create_v2017.4.0.0_upgrade.sql script.
2. Log into SQLPlus as the schema owner PWRPLANT and execute the create_v2017.4.0.0_upgrade.sql script.
   This script will dynamically generate the run_auto_generated_v2017.4.0.0_upgrade.sql script.
3. Review the run_auto_generated_v2017.4.0.0_upgrade.sql script and verify the versions and the scripts
   that will be executed.  Pay attention to the results for the Property Tax and Provision modules.
4. Log into SQLPlus as the schema owner PWRPLANT and execute the run_auto_generated_v2017.4.0.0_upgrade.sql
   script that was generated in step 2.
5. Check all log files for errors.

