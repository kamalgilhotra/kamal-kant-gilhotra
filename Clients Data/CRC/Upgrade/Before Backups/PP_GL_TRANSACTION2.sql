create or replace
FUNCTION            "PP_GL_TRANSACTION2" (A_ACTIVITY           number,
                                              A_LDG_ASSET_ID       number,
                                              A_LDG_ASSET_ACT_ID   number,
                                              A_LDG_DEPR_GROUP_ID  number,
                                              A_LDG_WORK_ORDER_ID  number,
                                              A_LDG_GL_ACCOUNT_ID  number,
                                              A_GAIN_LOSS_IND      number,
                                              A_LDG_PEND_TRANS_ID  number,
                                              A_JE_METHOD_ID       number,
                                              A_JE_SET_OF_BOOKS_ID number,
                                              A_JE_REVERSAL        number,
                                              A_JE_AMOUNT_TYPE     number) return varchar2 is

/***************************************************************************************
 FUNCTION:  PP_GL_TRANSACTION2
 CLIENT:    Generic Starting Function

 LAST CHANGE: 01-12-2012

 CUSTOM DESCRIPTION:

 Function Design:

 By default this function will look at PP_JOURNAL_LAYOUTS by looping over the
 CR_ELEMENTS table and create an accounting string that is fixed width and padded to match
 the cr accounting key setup and is delimited by a dash

 For each journal entry type the function will look up the activity in the PP_JOURNAL_LAYOUTS
 table for the specific company.  If no row exists for the company it will look at comapny -1
 (All Companies).  A company value of -2 will result in an "IGNORE" being used for the gl_transaction table.
 If none of those exist an error will be returned.
***************************************************************************************/

   GL_ACCOUNT_STR varchar2(2000);
   PP_ERR_CODE    varchar2(1000);

begin
   PP_ERR_CODE    := 'Error in PP_GL_TRANSACTION Original';
   GL_ACCOUNT_STR := PWRPLANT.PP_GL_TRANSACTION(A_ACTIVITY,
                                                A_LDG_ASSET_ID,
                                                A_LDG_ASSET_ACT_ID,
                                                A_LDG_DEPR_GROUP_ID,
                                                A_LDG_WORK_ORDER_ID,
                                                A_LDG_GL_ACCOUNT_ID,
                                                A_GAIN_LOSS_IND,
                                                A_LDG_PEND_TRANS_ID,
                                                A_JE_METHOD_ID,
                                                A_JE_SET_OF_BOOKS_ID,
                                                A_JE_REVERSAL,
                                                A_JE_AMOUNT_TYPE);

   return GL_ACCOUNT_STR;

exception
   when others then
      /* this catches all SQL errors, including no_data_found */
      return('ERROR SQL: ' || PP_ERR_CODE || ':' || sqlerrm);
end;
 
 
 
 