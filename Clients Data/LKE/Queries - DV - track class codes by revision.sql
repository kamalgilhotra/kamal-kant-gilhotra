-- update query
update work_order_approval
set (user_text13, user_text14, user_text15)
	= (select
	   	(select wcc.value from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'DSIC'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text13,
	        (select wcc.value from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'Location Note'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text14,
                (select wcc.value from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'Pollution Control'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text15
           from dual)
where work_order_id = <arg1>
and revision = <arg2>
and (nvl(user_text13, '.#.#$'), nvl(user_text14, '.#.#$'), nvl(user_text15, '.#.#$'))
	<> (select
	   	(select nvl(wcc.value, '$#.#$') from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'DSIC'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text13,
	        (select nvl(wcc.value, '$#.#$') from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'Location Note'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text14,
                (select nvl(wcc.value, '$#.#$') from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'Pollution Control'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text15
           from dual)


-- select query
select decode(count(*), 0, 0, 1) rec_cnt from
(
select 
(select nvl(wcc.value, '$#.#$') from class_code cc, work_order_class_code wcc
where cc.class_code_id = wcc.class_code_id(+)
and   description = 'DSIC'
and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text13,
(select nvl(wcc.value, '$#.#$') from class_code cc, work_order_class_code wcc
where cc.class_code_id = wcc.class_code_id(+)
and   description = 'Location Note'
and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text14,
(select nvl(wcc.value, '$#.#$') from class_code cc, work_order_class_code wcc
where cc.class_code_id = wcc.class_code_id(+)
and   description = 'Pollution Control'
and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text15
from dual
minus
select nvl(user_text13, '.#.#$'), nvl(user_text14, '.#.#$'), nvl(user_text15, '.#.#$') from work_order_approval
where work_order_id = <arg1>
and work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)
and revision = <arg2>
)