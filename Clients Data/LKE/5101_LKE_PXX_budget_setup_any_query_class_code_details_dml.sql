/*
||==============================================================================================================================
|| Application: PowerPlant
|| File Name:   5101_LKE_PXX_budget_setup_any_query_class_code_details_dml.sql
||==============================================================================================================================
|| Copyright (C) 2019 by PowerPlan Consultants, Inc. All Rights Reserved.
||==============================================================================================================================
|| Version    Date       Revised By     		Reason for Change
|| ---------- ---------- -------------------	--------------------------------------------------------------------------------
|| 2018.2.1.0 07/30/2019 Kamal Kant Gilhotra	Adding the Any Query to pull Class Code details based on Budget Version
||==============================================================================================================================
*/
insert into PP_ANY_QUERY_CRITERIA
   (ID, SOURCE_ID, CRITERIA_FIELD, TABLE_NAME, DESCRIPTION, FEEDER_FIELD, SUBSYSTEM, QUERY_TYPE, sql)
select   (select max(ID) + 1 from PP_ANY_QUERY_CRITERIA) ID,
          1001 SOURCE_ID,
          'none' CRITERIA_FIELD,
          'Dynamic View' TABLE_NAME,
          'Class Code Details' DESCRIPTION,
          'none' FEEDER_FIELD,
          'funding project' SUBSYSTEM,
          'user' QUERY_TYPE,
          'SELECT	WOC.WORK_ORDER_NUMBER, WOC.WORK_ORDER_ID, WOC.DESCRIPTION WORK_ORDER_DESCRIPTION, BV.DESCRIPTION BUDGET_VERSION, WOA.REVISION, USER_TEXT13 CATEGORY, USER_TEXT14 CODE, USER_TEXT15 RAC_CATEGORY, WOA.EST_START_DATE, WOA.EST_COMPLETE_DATE, WOA.EST_IN_SERVICE_DATE, WOA.TIME_STAMP
		   FROM		WORK_ORDER_APPROVAL WOA, WORK_ORDER_CONTROL WOC, COMPANY C, BUDGET_VERSION_FUND_PROJ BVFP, BUDGET_VERSION BV
		   WHERE	WOC.WORK_ORDER_ID = WOA.WORK_ORDER_ID AND WOC.FUNDING_WO_INDICATOR = 1 AND C.COMPANY_ID = WOC.COMPANY_ID AND BVFP.WORK_ORDER_ID = WOC.WORK_ORDER_ID AND BVFP.WORK_ORDER_ID = WOA.WORK_ORDER_ID
		   AND      WOA.REVISION = BVFP.REVISION AND BVFP.BUDGET_VERSION_ID = BV.BUDGET_VERSION_ID' sql
     from dual
    where 0 = (select count(*) from PP_ANY_QUERY_CRITERIA where description = 'Class Code Details');

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, COLUMN_HEADER,
    COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD)
select ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, COLUMN_HEADER,
    COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD
from
   (select ID, 'work_order_number' DETAIL_FIELD, 1 COLUMN_ORDER, 0 AMOUNT_FIELD, 1 INCLUDE_IN_SELECT_CRITERIA, 'Work Order Number' COLUMN_HEADER, 300 COLUMN_WIDTH, 'VARCHAR2' COLUMN_TYPE, 0 QUANTITY_FIELD
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Class Code Details'
   union
   select ID, 'work_order_id', 2, 0, 1, 'Work Order ID', 300, 'NUMBER', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Class Code Details'
   union
   select ID, 'work_order_description', 3, 0, 1, 'Work Order Description', 300, 'VARCHAR2', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Class Code Details'
   union
   select ID, 'budget_version', 4, 0, 1, 'Budget Version', 300, 'VARCHAR2', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Class Code Details'
   union
   select ID, 'revision', 5, 0, 1, 'Revision Number', 300, 'NUMBER', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Class Code Details'
   union
   select ID, 'category', 6, 0, 1, 'Category', 300, 'VARCHAR2', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Class Code Details'
   union
   select ID, 'code', 7, 0, 1, 'Code', 300, 'VARCHAR2', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Class Code Details'
   union
   select ID, 'rac_category', 8, 0, 1, 'RAC Category', 300, 'VARCHAR2', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Class Code Details'
   union
   select ID, 'est_start_date', 9, 0, 1, 'Est Start Date', 300, 'DATE', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Class Code Details'
   union
   select ID, 'est_complete_date', 10, 0, 1, 'Est Complete Date', 300, 'DATE', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Class Code Details'
   union
   select ID, 'est_in_service_date', 11, 0, 1, 'Est In Service Date', 300, 'DATE', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Class Code Details'
   union
   select ID, 'time_stamp', 12, 0, 1, 'Time Stamp', 300, 'DATE', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Class Code Details')
minus
select ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, COLUMN_HEADER,
       COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD
from   PP_ANY_QUERY_CRITERIA_FIELDS
where  ID = (select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION = 'Class Code Details')
;

UPDATE	PP_ANY_QUERY_CRITERIA_FIELDS
SET		DISPLAY_FIELD = 'DESCRIPTION', DISPLAY_TABLE = 'BUDGET_VERSION', DATA_FIELD = 'DESCRIPTION'
WHERE	ID = (SELECT ID FROM PP_ANY_QUERY_CRITERIA WHERE DESCRIPTION = 'Class Code Details')
AND		column_order = 4
;

COMMIT;


/*
|| Log the run of the script
*/
insert into PP_UPDATE_FLEX
                (COL_ID,
                 TYPE_NAME,
                 FLEXVCHAR1)
select NVL(MAX(COL_ID), 0) + 1,
                 'CustomScript',
                '5101_LKE_PXX_budget_setup_any_query_class_code_details_dml.sql' --this should be your script name
from PP_UPDATE_FLEX;
commit;
