/*
||==============================================================================================================================
|| Application: PowerPlant
|| File Name:   5100_LKE_PXX_update_class_codes_in_work_order_approvals_dml.sql
||==============================================================================================================================
|| Copyright (C) 2019 by PowerPlan Consultants, Inc. All Rights Reserved.
||==============================================================================================================================
|| Version    Date       Revised By     		Reason for Change
|| ---------- ---------- -------------------	--------------------------------------------------------------------------------
|| 2018.2.1.0 07/30/2019 Kamal Kant Gilhotra	Dynamic validation update for class code values in Work Order Approval table
||==============================================================================================================================
*/
update work_order_approval
set (user_text13, user_text14, user_text15)
	= (select
	   	(select wcc.value from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'Category'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text13,
	        (select wcc.value from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'Code'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text14,
                (select wcc.value from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'RAC Category'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text15
           from dual)
where work_order_id = <arg1>
and revision = <arg2>
and (nvl(user_text13, '.#.#$'), nvl(user_text14, '.#.#$'), nvl(user_text15, '.#.#$'))
	<> (select
			(select nvl(wcc.value, '$#.#$') from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'Category'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text13,
	        (select nvl(wcc.value, '$#.#$') from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'Code'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text14,
            (select nvl(wcc.value, '$#.#$') from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'RAC Category'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text15
        from dual)
;

COMMIT;


/*
|| Log the run of the script
*/
insert into PP_UPDATE_FLEX
                (COL_ID,
                 TYPE_NAME,
                 FLEXVCHAR1)
select NVL(MAX(COL_ID), 0) + 1,
                 'CustomScript',
                '5100_LKE_PXX_update_class_codes_in_work_order_approvals_dml.sql' --this should be your script name
from PP_UPDATE_FLEX;
commit;
