-- update query
update work_order_approval
set (user_text13, user_text14, user_text15)
	= (select
	   	(select wcc.value from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'DSIC'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text13,
	        (select wcc.value from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'Location Note'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text14,
                (select wcc.value from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'Pollution Control'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text15
           from dual)
where work_order_id = <arg1>
and revision = <arg2>
and (nvl(user_text13, '.#.#$'), nvl(user_text14, '.#.#$'), nvl(user_text15, '.#.#$'))
	<> (select
			(select nvl(wcc.value, '$#.#$') from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'DSIC'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text13,
	        (select nvl(wcc.value, '$#.#$') from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'Location Note'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text14,
            (select nvl(wcc.value, '$#.#$') from class_code cc, work_order_class_code wcc
                  where cc.class_code_id = wcc.class_code_id(+)
                  and   description = 'Pollution Control'
                  and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text15
        from dual)
;

-- select query
select decode(count(*), 0, 0, 1) rec_cnt from
(
select 
(select nvl(wcc.value, '$#.#$') from class_code cc, work_order_class_code wcc
where cc.class_code_id = wcc.class_code_id(+)
and   description = 'DSIC'
and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text13,
(select nvl(wcc.value, '$#.#$') from class_code cc, work_order_class_code wcc
where cc.class_code_id = wcc.class_code_id(+)
and   description = 'Location Note'
and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text14,
(select nvl(wcc.value, '$#.#$') from class_code cc, work_order_class_code wcc
where cc.class_code_id = wcc.class_code_id(+)
and   description = 'Pollution Control'
and   wcc.work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)) user_text15
from dual
minus
select nvl(user_text13, '.#.#$'), nvl(user_text14, '.#.#$'), nvl(user_text15, '.#.#$') from work_order_approval
where work_order_id = <arg1>
and work_order_id in(select work_order_id from work_order_control where funding_wo_indicator = 1 and work_order_id = <arg1>)
and revision = <arg2>
)
;

Add comments on WOA (for the field description)

========================================================================================================================================================================================================


7.2
Update the records in WOA (budget_version_fund_proj) for the latest revision on the current version of (budget version)

Only update 


WITH cc_data AS (
    SELECT work_order_id, mx_revision, user_text13, user_text14, user_text15
    FROM  (SELECT work_order_id,
                  (SELECT MAX(revision) FROM budget_version_fund_proj bvfp WHERE budget_version_id IN (SELECT budget_version_id FROM budget_version WHERE current_version = 1) AND work_order_id = woc.work_order_id ) mx_revision,
                  (SELECT wcc.value FROM class_code cc, work_order_class_code wcc WHERE cc.class_code_id = wcc.class_code_id (+) AND wcc.work_order_id = woc.work_order_id AND description = 'DSIC') user_text13,
                  (SELECT wcc.value FROM class_code cc, work_order_class_code wcc WHERE cc.class_code_id = wcc.class_code_id (+) AND wcc.work_order_id = woc.work_order_id AND description = 'Location Note') user_text14,
                  (SELECT wcc.value FROM class_code cc, work_order_class_code wcc WHERE cc.class_code_id = wcc.class_code_id (+) AND wcc.work_order_id = woc.work_order_id AND description = 'Pollution Control') user_text15
           FROM   work_order_control woc
           WHERE  funding_wo_indicator = 1)
    WHERE mx_revision IS NOT NULL)
SELECT woa.work_order_id, woa.revision, woa.user_text13, cc_data.user_text13, woa.user_text14, cc_data.user_text14, woa.user_text15, cc_data.user_text15
FROM   work_order_approval woa, cc_data
WHERE  woa.work_order_id = cc_data.work_order_id
AND    woa.revision = cc_data.mx_revision
AND    woa.work_order_id IN (1326307, 1258955, 1149261, 15261566);


UPDATE work_order_approval woa
SET    (woa.user_text13, woa.user_text14, woa.user_text15) = 
       (SELECT cc_data.user_text13, cc_data.user_text14, cc_data.user_text15
        FROM   (SELECT work_order_id, mx_revision, user_text13, user_text14, user_text15
                FROM  (SELECT work_order_id,
                              (SELECT MAX(revision) FROM budget_version_fund_proj bvfp WHERE budget_version_id IN (SELECT budget_version_id FROM budget_version WHERE current_version = 1) AND work_order_id = woc.work_order_id) mx_revision,
                              (SELECT wcc.value FROM class_code cc, work_order_class_code wcc WHERE cc.class_code_id = wcc.class_code_id (+) AND wcc.work_order_id = woc.work_order_id AND description = 'DSIC') user_text13,
                              (SELECT wcc.value FROM class_code cc, work_order_class_code wcc WHERE cc.class_code_id = wcc.class_code_id (+) AND wcc.work_order_id = woc.work_order_id AND description = 'Location Note') user_text14,
                              (SELECT wcc.value FROM class_code cc, work_order_class_code wcc WHERE cc.class_code_id = wcc.class_code_id (+) AND wcc.work_order_id = woc.work_order_id AND description = 'Pollution Control') user_text15
                       FROM   work_order_control woc
                       WHERE  funding_wo_indicator = 1)
                WHERE mx_revision IS NOT NULL) cc_data
        WHERE woa.work_order_id = cc_data.work_order_id AND woa.revision = cc_data.mx_revision)
WHERE  (woa.work_order_id, woa.revision) IN(SELECT work_order_id, mx_revision
                                            FROM (SELECT * FROM
                                                       (SELECT work_order_id,
                                                              (SELECT MAX(revision) FROM budget_version_fund_proj bvfp WHERE budget_version_id IN (SELECT budget_version_id FROM budget_version WHERE current_version = 1) AND work_order_id = woc.work_order_id) mx_revision,
                                                              (SELECT wcc.value FROM class_code cc, work_order_class_code wcc WHERE cc.class_code_id = wcc.class_code_id (+) AND wcc.work_order_id = woc.work_order_id AND description = 'DSIC') user_text13,
                                                              (SELECT wcc.value FROM class_code cc, work_order_class_code wcc WHERE cc.class_code_id = wcc.class_code_id (+) AND wcc.work_order_id = woc.work_order_id AND description = 'Location Note') user_text14,
                                                              (SELECT wcc.value FROM class_code cc, work_order_class_code wcc WHERE cc.class_code_id = wcc.class_code_id (+) AND wcc.work_order_id = woc.work_order_id AND description = 'Pollution Control') user_text15
                                                       FROM   work_order_control woc
                                                       WHERE  funding_wo_indicator = 1)
                                                  WHERE user_text13 IS NOT NULL OR user_text14 IS NOT NULL OR user_text15 IS NOT NULL)
                                            WHERE mx_revision IS NOT NULL);


========================================================================================================================================================================================================


7.3

select user_text13, user_text14, user_text15, a.* from work_order_approval a where work_order_Id = 1149261 order by revision desc;

EST_START_DATE, EST_COMPLETE_DATE, EST_IN_SERVICE_DATE;
FUNDING_PROJECT (WORD_ORDER_NUMBER), BUDGET_VERSION, WORK_ORDER_APPROVAL, WOC;

select * from work_order_control where work_order_id = 1149261;

select * from budget_version ;

select * from budget_version_fund_proj where work_order_id = 1149261
and active = 1
;

SELECT	WOC.WORK_ORDER_NUMBER, WOC.DESCRIPTION, WOA.REVISION, USER_TEXT13 CATEGORY, USER_TEXT14 CODE, USER_TEXT15 RAC_CATEGORY, WOA.EST_START_DATE, WOA.EST_COMPLETE_DATE, WOA.EST_IN_SERVICE_DATE, WOA.TIME_STAMP
FROM	WORK_ORDER_APPROVAL WOA, WORK_ORDER_CONTROL WOC, COMPANY_SETUP C
WHERE	WOC.WORK_ORDER_ID = WOA.WORK_ORDER_ID AND WOC.FUNDING_WO_INDICATOR = 1 AND C.COMPANY_ID = WOC.COMPANY_ID
AND		WOC.WORK_ORDER_ID = 1149261 AND WOA.REVISION = 25
ORDER BY REVISION DESC;

SELECT WOC.WORK_ORDER_NUMBER, WOC.WORK_ORDER_ID, WOC.DESCRIPTION, WOA.REVISION, USER_TEXT13 CATEGORY, USER_TEXT14 CODE, USER_TEXT15 RAC_CATEGORY, WOA.EST_START_DATE, WOA.EST_COMPLETE_DATE, WOA.EST_IN_SERVICE_DATE, WOA.TIME_STAMP FROM WORK_ORDER_APPROVAL WOA, WORK_ORDER_CONTROL WOC WHERE WOC.WORK_ORDER_ID = WOA.WORK_ORDER_ID AND WOC.FUNDING_WO_INDICATOR = 1



============================================================================================================================================================================================================

select 'funding_wo' wo_type, est_start_date, est_complete_date, est_in_service_date, completion_date, in_service_date, close_date, w.* from work_order_control w where work_order_number = '5006961'
union
select 'child prjs', est_start_date, est_complete_date, est_in_service_date, completion_date, in_service_date, close_date, w.* from work_order_control w where funding_wo_id = 1149255;