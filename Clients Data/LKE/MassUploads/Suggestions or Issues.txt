Is there a specific reason we are requesting them to enter upper case Work Order Number? We can convert it ourselves - probably they can mention it in their template that it will be auto-converted to upper case.


Why limit of 9 chars for WO Number?

Archive table does not contain the error message & status details

While running a fresh batch, rows not getting deleted from WO_INTERFACE_STAGING from previous run, in case of failure (e.g. cannot identify department id) - but some of the records get removed in next successful run

LGEKU_PROJECT_UPLOAD_ARC table shows budget_id as a next number e.g. shows , is this ok (hopefully yes)? e.g. 414523345 shows up, whereas the previous entries do not have any number work order with this number.

WO_INTERFACE_STAGING table - somehow the work_order_id is getting incremented each time (most probably deleting this record should resolve the issue) - this becomes a new 414523335 with the next run

Maybe we can add status other than -1 (e.g. when we start, we should set a status indicating that we have started processing those records, maybe 1)
When there's a failure we should mark as -1, when process finishes, we should mark status as maybe 2


========================================================================================================================================================

DV18 can be improved
Unnecessary check for NULL value is being made (and ceck for CAP is also bad)... rather exist check can be used to see records starting with CAP
Also, the query for inner join can be modified so that the actual conditions of grp_id can be moved to the where clause

DV19 - appears to be exactly same?

Should we error out duplicate records - same WO Number and Company ID - in the upload table??

70 / 71 should be entered together???

DV [58] does not seem to be working - it shows error but it still saves. --- problem with the way the data is being saved....

Cleanup of project_upload table (failure, successful, ... should the old failure clearup??)
How do error details from wo_interface_staging show back in project_upload??

DELETE THE RECORDS FROM project_upload_CC_VALUES (near end of process)

Add MAX(INTERFACE_BATCH) check, instead of NOT NULL

if error in WO_INTERFACE_STAGING (when EON PROJETC ERROR comes), system is trying to insert the same data in next run as well

Department id being updated based on Budget ID (which is getting updated from 3-4 statements)

check dv63 - is it valid/needed? if so, any corrections???

Job Task ID missing,

Cannot derive xxxx fields (for both project/task upload)

Add interface_batch to the powerplant-columns table

Do we need to add all other missing WO_VALIDATIONS???
Should we call f_wo_validation_control([6])????

Fix the LGEKU_PRJ_TSK_UPLOAD_MSGS table (keep needed fields only)

Also, update the status field - based on where it failed --- -1 initial, -2 WO_INTERFACE_STAGING, 1 f_wo_validate_update, 2 success [1 means data saved, however some error's that need to be resolved, 2 means success]

Link it back to Task updates as well????

Take error info from WO_INTERFACE_STAGING back to LGEKU_PROJECT_UPLOAD_ARC table

Modify 0521_LKE_B50_create_mass_upload_tables_DDL.sql to add company_id and ext_company fields (in LGEKU_TASK_ADMIN_UPLOAD and LGEKU_CONTACT_ADMIN_UPLOAD and the archive tables)
ALTER TABLE lgeku_task_admin_upload ADD (company_id number, ext_company varchar2(35))
ALTER TABLE lgeku_task_admin_upload_arc ADD (company_id number, ext_company varchar2(35))
ALTER TABLE lgeku_contact_admin_upload ADD (company_id number, ext_company varchar2(35))
ALTER TABLE lgeku_contact_admin_upload_arc ADD (company_id number, ext_company varchar2(35))

Fix the these tables.... to align with the existing interface... powerplant_tables/columns 



Fix the class code so it doesn't show on budget screen

Confirm if we should add a condition about pp_security_users sp tat only active proj_mgr is looked for and you cannot add active plant_accountant as a project manager



Add a validation that core fields cannot be 'null' e.g. company id, department etc.



HR Interface - lowercase the users


Create LGEKU_PRJ_MSGS (the new table) has to be in table maintenance.

calling the Mass Upload tool through CR -> Integration -> Interfaces



=======================================================================================================================================

Add validation for class code 70 value correctness - both new inserts and existing data
