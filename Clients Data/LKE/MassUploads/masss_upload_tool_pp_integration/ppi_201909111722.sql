SET DEFINE OFF;
Insert into PWRPLANT.PP_INTEGRATION
   (BATCH, PRIORITY, COMPONENT, ARGUMENT1, STATUS, 
    DESCRIPTION)
 Values
   ('Mass Upload Tool', 9, 'Data Mover Batch', 'PROJ: Stage Data', 1, 
    'PROJ: Stage Data');
Insert into PWRPLANT.PP_INTEGRATION
   (BATCH, PRIORITY, COMPONENT, ARGUMENT1, ARGUMENT2, 
    STATUS, DESCRIPTION)
 Values
   ('Mass Upload Tool', 10, 'Create Funding Project', '-1', 'Create FP', 
    1, 'PROJ: Create Funding Project');
Insert into PWRPLANT.PP_INTEGRATION
   (BATCH, PRIORITY, COMPONENT, ARGUMENT1, STATUS, 
    DESCRIPTION)
 Values
   ('Mass Upload Tool', 20, 'Data Mover Batch', 'PROJ: Post Processing', 1, 
    'PROJ: Post Processing');
Insert into PWRPLANT.PP_INTEGRATION
   (BATCH, PRIORITY, COMPONENT, ARGUMENT1, ARGUMENT2, 
    ARGUMENT3, ARGUMENT4, STATUS, DESCRIPTION)
 Values
   ('Mass Upload Tool', 30, 'Custom FP Validations', '-1', 'Validate', 
    '1', 'wo_interface_staging', 1, 'PROJ: Custom FP Validations');
Insert into PWRPLANT.PP_INTEGRATION
   (BATCH, PRIORITY, COMPONENT, ARGUMENT1, STATUS, 
    DESCRIPTION)
 Values
   ('Mass Upload Tool', 35, 'Data Mover Batch', 'PROJ: Post Processing Validations', 1, 
    'PROJ: Post Processing Validations');
Insert into PWRPLANT.PP_INTEGRATION
   (BATCH, PRIORITY, COMPONENT, ARGUMENT1, STATUS, 
    DESCRIPTION)
 Values
   ('Task Upload Tool', 10, 'Data Mover Batch', 'TASK: Stage Data', 1, 
    'TASK: Stage Data');
Insert into PWRPLANT.PP_INTEGRATION
   (BATCH, PRIORITY, COMPONENT, ARGUMENT1, ARGUMENT2, 
    STATUS, DESCRIPTION)
 Values
   ('Task Upload Tool', 15, 'Create Job Task', '-1', 'Create Job Task', 
    1, 'TASK: Create Job Task');
Insert into PWRPLANT.PP_INTEGRATION
   (BATCH, PRIORITY, COMPONENT, ARGUMENT1, STATUS, 
    DESCRIPTION)
 Values
   ('Task Upload Tool', 20, 'Data Mover Batch', 'TASK: Post Processing', 1, 
    'TASK: Post Processing');
COMMIT;
