delete from powerplant_columns where table_name IN('LGEKU_CONTACT_ADMIN_UPLOAD', 'LGEKU_CONTACT_ADMIN_UPLOAD_ARC');

delete from powerplant_tables where table_name IN('LGEKU_CONTACT_ADMIN_UPLOAD', 'LGEKU_CONTACT_ADMIN_UPLOAD_ARC');


MERGE INTO "POWERPLANT_TABLES" A USING (select 'LGEKU_CONTACT_ADMIN_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:19:13', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", 's' as "PP_TABLE_TYPE_ID", 'LGEKU Contact Admin Upload' as "DESCRIPTION", 'work' as "SUBSYSTEM_SCREEN", null as "SELECT_WINDOW", null as "SUBSYSTEM_DISPLAY", null as 
"SUBSYSTEM", null as "DELIVERY", null as "NOTES", null as "ASSET_MANAGEMENT", null as "BUDGET", null as "CHARGE_REPOSITORY", null as "CWIP_ACCOUNTING", null as "DEPR_STUDIES", 
null as "LEASE", null as "PROPERTY_TAX", null as "POWERTAX_PROVISION", null as "POWERTAX", null as "SYSTEM", null as "UNITIZATION", null as "WORK_ORDER_MANAGEMENT", null as 
"CLIENT", null as "WHERE_CLAUSE" from dual union all select 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:14', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 's' as "PP_TABLE_TYPE_ID", 'LGEKU Project Upload' as "DESCRIPTION", 'work' as "SUBSYSTEM_SCREEN", null as 
"SELECT_WINDOW", null as "SUBSYSTEM_DISPLAY", null as "SUBSYSTEM", null as "DELIVERY", null as "NOTES", null as "ASSET_MANAGEMENT", null as "BUDGET", null as "CHARGE_REPOSITORY", 
null as "CWIP_ACCOUNTING", null as "DEPR_STUDIES", null as "LEASE", null as "PROPERTY_TAX", null as "POWERTAX_PROVISION", null as "POWERTAX", null as "SYSTEM", null as 
"UNITIZATION", null as "WORK_ORDER_MANAGEMENT", null as "CLIENT", null as "WHERE_CLAUSE" from dual union all select 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", 
to_date('2019-07-30 10:21:28', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 's' as "PP_TABLE_TYPE_ID", 'LGEKU Project Upload Arc' as "DESCRIPTION", 'work' as 
"SUBSYSTEM_SCREEN", null as "SELECT_WINDOW", null as "SUBSYSTEM_DISPLAY", null as "SUBSYSTEM", null as "DELIVERY", null as "NOTES", null as "ASSET_MANAGEMENT", null as "BUDGET", 
null as "CHARGE_REPOSITORY", null as "CWIP_ACCOUNTING", null as "DEPR_STUDIES", null as "LEASE", null as "PROPERTY_TAX", null as "POWERTAX_PROVISION", null as "POWERTAX", null as 
"SYSTEM", null as "UNITIZATION", null as "WORK_ORDER_MANAGEMENT", null as "CLIENT", null as "WHERE_CLAUSE" from dual union all select 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", 
to_date('2019-07-30 10:21:55', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 's' as "PP_TABLE_TYPE_ID", 'LGEKU Task Upload' as "DESCRIPTION", 'work' as 
"SUBSYSTEM_SCREEN", null as "SELECT_WINDOW", null as "SUBSYSTEM_DISPLAY", null as "SUBSYSTEM", null as "DELIVERY", null as "NOTES", null as "ASSET_MANAGEMENT", null as "BUDGET", 
null as "CHARGE_REPOSITORY", null as "CWIP_ACCOUNTING", null as "DEPR_STUDIES", null as "LEASE", null as "PROPERTY_TAX", null as "POWERTAX_PROVISION", null as "POWERTAX", null as 
"SYSTEM", null as "UNITIZATION", null as "WORK_ORDER_MANAGEMENT", null as "CLIENT", null as "WHERE_CLAUSE" from dual union all select 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", 
to_date('2019-07-30 10:22:08', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 's' as "PP_TABLE_TYPE_ID", 'LGEKU Task Upload Arc' as "DESCRIPTION", 'work' as 
"SUBSYSTEM_SCREEN", null as "SELECT_WINDOW", null as "SUBSYSTEM_DISPLAY", null as "SUBSYSTEM", null as "DELIVERY", null as "NOTES", null as "ASSET_MANAGEMENT", null as "BUDGET", 
null as "CHARGE_REPOSITORY", null as "CWIP_ACCOUNTING", null as "DEPR_STUDIES", null as "LEASE", null as "PROPERTY_TAX", null as "POWERTAX_PROVISION", null as "POWERTAX", null as 
"SYSTEM", null as "UNITIZATION", null as "WORK_ORDER_MANAGEMENT", null as "CLIENT", null as "WHERE_CLAUSE" from dual union all select 'LGEKU_TASK_ADMIN_UPLOAD_ARC' as "TABLE_NAME",
 to_date('2019-07-30 10:22:24', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 's' as "PP_TABLE_TYPE_ID", 'LGEKU Task Admin Upload Arc' as "DESCRIPTION", 
'work' as "SUBSYSTEM_SCREEN", null as "SELECT_WINDOW", null as "SUBSYSTEM_DISPLAY", null as "SUBSYSTEM", null as "DELIVERY", null as "NOTES", null as "ASSET_MANAGEMENT", null as 
"BUDGET", null as "CHARGE_REPOSITORY", null as "CWIP_ACCOUNTING", null as "DEPR_STUDIES", null as "LEASE", null as "PROPERTY_TAX", null as "POWERTAX_PROVISION", null as "POWERTAX",
 null as "SYSTEM", null as "UNITIZATION", null as "WORK_ORDER_MANAGEMENT", null as "CLIENT", null as "WHERE_CLAUSE" from dual union all select 'LGEKU_TASK_ADMIN_UPLOAD' as 
"TABLE_NAME", to_date('2019-07-30 10:22:38', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 's' as "PP_TABLE_TYPE_ID", 'LGEKU Task Admin Upload' as 
"DESCRIPTION", 'work' as "SUBSYSTEM_SCREEN", null as "SELECT_WINDOW", null as "SUBSYSTEM_DISPLAY", null as "SUBSYSTEM", null as "DELIVERY", null as "NOTES", null as 
"ASSET_MANAGEMENT", null as "BUDGET", null as "CHARGE_REPOSITORY", null as "CWIP_ACCOUNTING", null as "DEPR_STUDIES", null as "LEASE", null as "PROPERTY_TAX", null as 
"POWERTAX_PROVISION", null as "POWERTAX", null as "SYSTEM", null as "UNITIZATION", null as "WORK_ORDER_MANAGEMENT", null as "CLIENT", null as "WHERE_CLAUSE" from dual) B  ON 
(A."TABLE_NAME" = B."TABLE_NAME") WHEN NOT MATCHED THEN INSERT ("TABLE_NAME", "TIME_STAMP", "USER_ID", "PP_TABLE_TYPE_ID", "DESCRIPTION", "SUBSYSTEM_SCREEN", "SELECT_WINDOW", 
"SUBSYSTEM_DISPLAY", "SUBSYSTEM", "DELIVERY", "NOTES", "ASSET_MANAGEMENT", "BUDGET", "CHARGE_REPOSITORY", "CWIP_ACCOUNTING", "DEPR_STUDIES", "LEASE", "PROPERTY_TAX", 
"POWERTAX_PROVISION", "POWERTAX", "SYSTEM", "UNITIZATION", "WORK_ORDER_MANAGEMENT", "CLIENT", "WHERE_CLAUSE") values (B."TABLE_NAME", B."TIME_STAMP", B."USER_ID", 
B."PP_TABLE_TYPE_ID", B."DESCRIPTION", B."SUBSYSTEM_SCREEN", B."SELECT_WINDOW", B."SUBSYSTEM_DISPLAY", B."SUBSYSTEM", B."DELIVERY", B."NOTES", B."ASSET_MANAGEMENT", B."BUDGET", 
B."CHARGE_REPOSITORY", B."CWIP_ACCOUNTING", B."DEPR_STUDIES", B."LEASE", B."PROPERTY_TAX", B."POWERTAX_PROVISION", B."POWERTAX", B."SYSTEM", B."UNITIZATION", 
B."WORK_ORDER_MANAGEMENT", B."CLIENT", B."WHERE_CLAUSE") WHEN MATCHED THEN UPDATE SET A."TIME_STAMP" = B."TIME_STAMP", A."USER_ID" = B."USER_ID", A."PP_TABLE_TYPE_ID" = 
B."PP_TABLE_TYPE_ID", A."DESCRIPTION" = B."DESCRIPTION", A."SUBSYSTEM_SCREEN" = B."SUBSYSTEM_SCREEN", A."SELECT_WINDOW" = B."SELECT_WINDOW", A."SUBSYSTEM_DISPLAY" = 
B."SUBSYSTEM_DISPLAY", A."SUBSYSTEM" = B."SUBSYSTEM", A."DELIVERY" = B."DELIVERY", A."NOTES" = B."NOTES", A."ASSET_MANAGEMENT" = B."ASSET_MANAGEMENT", A."BUDGET" = B."BUDGET", 
A."CHARGE_REPOSITORY" = B."CHARGE_REPOSITORY", A."CWIP_ACCOUNTING" = B."CWIP_ACCOUNTING", A."DEPR_STUDIES" = B."DEPR_STUDIES", A."LEASE" = B."LEASE", A."PROPERTY_TAX" = 
B."PROPERTY_TAX", A."POWERTAX_PROVISION" = B."POWERTAX_PROVISION", A."POWERTAX" = B."POWERTAX", A."SYSTEM" = B."SYSTEM", A."UNITIZATION" = B."UNITIZATION", 
A."WORK_ORDER_MANAGEMENT" = B."WORK_ORDER_MANAGEMENT", A."CLIENT" = B."CLIENT", A."WHERE_CLAUSE" = B."WHERE_CLAUSE"; 

INSERT INTO powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank) VALUES('work_order_number', 'LGEKU_CONTACT_ADMIN_UPLOAD', 'e', 'Project Number', 1);
INSERT INTO powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank) VALUES('ext_company', 'LGEKU_CONTACT_ADMIN_UPLOAD', 'e', 'Company', 2);
INSERT INTO powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank) VALUES('ext_project_manager', 'LGEKU_CONTACT_ADMIN_UPLOAD', 'e', 'Project Manager', 3);
INSERT INTO powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank) VALUES('ext_plant_accountant', 'LGEKU_CONTACT_ADMIN_UPLOAD', 'e', 'Property Accounting', 4);
INSERT INTO powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank) VALUES('ext_contract_admin', 'LGEKU_CONTACT_ADMIN_UPLOAD', 'e', 'AIP Coordinator', 5);
INSERT INTO powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank) VALUES('ext_engineer', 'LGEKU_CONTACT_ADMIN_UPLOAD', 'e', 'Cost Scheduler', 6);
INSERT INTO powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank) VALUES('ext_other_contact', 'LGEKU_CONTACT_ADMIN_UPLOAD', 'e', 'Budget Coordinator', 7);
INSERT INTO powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank) VALUES('time_stamp', 'LGEKU_CONTACT_ADMIN_UPLOAD', 'e', 'time stamp', 100);
INSERT INTO powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank) VALUES('user_id', 'LGEKU_CONTACT_ADMIN_UPLOAD', 'e', 'user id', 101);

MERGE INTO "POWERPLANT_COLUMNS" A USING (select 
'action' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'action' as "DESCRIPTION", 1 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'afudc_start_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null 
as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'afudc start date' as "DESCRIPTION", 2 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'afudc_stop_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null 
as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'afudc stop date' as "DESCRIPTION", 3 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'agreement_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'agreement id' as "DESCRIPTION", 4 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null 
as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'alternatives' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'alternatives' as "DESCRIPTION", 5 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null 
as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'asset_location_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'asset location id' as "DESCRIPTION", 6 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'auto_approved' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'auto approved' as "DESCRIPTION", 7 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'background' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'background' as "DESCRIPTION", 8 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'base_year' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'base year' as "DESCRIPTION", 9 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'batch_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'batch id' as "DESCRIPTION", 10 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'budget_company_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'budget company id' as "DESCRIPTION", 11 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'budget_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'budget id' as "DESCRIPTION", 12 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'budget_number' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'budget number' as "DESCRIPTION", 13 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null 
as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'budget_version_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'budget version id' as "DESCRIPTION", 14 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'bus_segment_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'bus segment id' as "DESCRIPTION", 15 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_code1' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code1' as "DESCRIPTION", 16 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code10' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code10' as "DESCRIPTION", 17 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code11' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code11' as "DESCRIPTION", 18 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code12' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code12' as "DESCRIPTION", 19 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code13' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code13' as "DESCRIPTION", 20 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code14' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code14' as "DESCRIPTION", 21 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code15' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code15' as "DESCRIPTION", 22 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code16' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code16' as "DESCRIPTION", 23 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code17' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code17' as "DESCRIPTION", 24 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code18' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code18' as "DESCRIPTION", 25 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code19' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code19' as "DESCRIPTION", 26 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code2' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID",
 null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code2' as "DESCRIPTION", 27 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code20' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code20' as "DESCRIPTION", 28 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code21' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code21' as "DESCRIPTION", 29 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code22' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code22' as "DESCRIPTION", 30 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code23' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code23' as "DESCRIPTION", 31 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code24' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code24' as "DESCRIPTION", 32 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code25' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code25' as "DESCRIPTION", 33 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code26' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code26' as "DESCRIPTION", 34 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code27' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code27' as "DESCRIPTION", 35 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code28' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code28' as "DESCRIPTION", 36 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code29' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code29' as "DESCRIPTION", 37 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code3' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID",
 null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code3' as "DESCRIPTION", 38 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code30' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code30' as "DESCRIPTION", 39 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code4' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID",
 null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code4' as "DESCRIPTION", 40 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code5' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID",
 null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code5' as "DESCRIPTION", 41 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code6' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID",
 null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code6' as "DESCRIPTION", 42 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code7' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID",
 null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code7' as "DESCRIPTION", 43 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code8' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID",
 null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code8' as "DESCRIPTION", 44 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_code9' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID",
 null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code9' as "DESCRIPTION", 45 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_value1' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value1' as "DESCRIPTION", 46 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_value10' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value10' as "DESCRIPTION", 47 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value11' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value11' as "DESCRIPTION", 48 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value12' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value12' as "DESCRIPTION", 49 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value13' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value13' as "DESCRIPTION", 50 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value14' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value14' as "DESCRIPTION", 51 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value15' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value15' as "DESCRIPTION", 52 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value16' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value16' as "DESCRIPTION", 53 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value17' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value17' as "DESCRIPTION", 54 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value18' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value18' as "DESCRIPTION", 55 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value19' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value19' as "DESCRIPTION", 56 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value2' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value2' as "DESCRIPTION", 57 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_value20' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value20' as "DESCRIPTION", 58 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value21' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value21' as "DESCRIPTION", 59 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value22' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value22' as "DESCRIPTION", 60 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value23' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value23' as "DESCRIPTION", 61 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value24' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value24' as "DESCRIPTION", 62 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value25' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value25' as "DESCRIPTION", 63 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value26' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value26' as "DESCRIPTION", 64 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value27' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value27' as "DESCRIPTION", 65 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value28' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value28' as "DESCRIPTION", 66 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value29' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value29' as "DESCRIPTION", 67 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value3' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value3' as "DESCRIPTION", 68 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_value30' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value30' as "DESCRIPTION", 69 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'class_value4' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value4' as "DESCRIPTION", 70 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_value5' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value5' as "DESCRIPTION", 71 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_value6' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value6' as "DESCRIPTION", 72 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_value7' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value7' as "DESCRIPTION", 73 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_value8' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value8' as "DESCRIPTION", 74 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'class_value9' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value9' as "DESCRIPTION", 75 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'close_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'close date' as "DESCRIPTION", 76 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'company_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'company id' as "DESCRIPTION", 77 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'completion_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null 
as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'completion date' as "DESCRIPTION", 78 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'contract_admin' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null 
as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'contract admin' as "DESCRIPTION", 79 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'department_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'department id' as "DESCRIPTION", 80 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null 
as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'description' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'description' as "DESCRIPTION", 0 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'engineer' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'engineer' as "DESCRIPTION", 82 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'error_message' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'error message' as "DESCRIPTION", 83 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null 
as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'est_annual_rev' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null 
as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'est annual rev' as "DESCRIPTION", 84 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'est_complete_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'est complete date' as "DESCRIPTION", 85 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'est_in_service_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'est in service date' as "DESCRIPTION", 86 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", 
null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from 
dual union all select 'est_start_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 
'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'est start date' as "DESCRIPTION", 87 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'ext_agreement' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext agreement' as "DESCRIPTION", 88 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'ext_asset_location' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext asset location' as "DESCRIPTION", 
89 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as 
"RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'ext_company' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", 
to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'ext company' as "DESCRIPTION", 90 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as 
"RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'ext_department' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as 
"TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'ext department' as "DESCRIPTION", 91 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as 
"DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'ext_major_location' as "COLUMN_NAME",
 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext major location' as "DESCRIPTION", 92 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'ext_reason_cd' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext reason cd' as "DESCRIPTION", 93 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null 
as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'ext_repair_location' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext repair location' as "DESCRIPTION", 94 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'ext_wo_approval_group' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext wo approval group' as "DESCRIPTION", 95 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION",
 null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from 
dual union all select 'ext_work_order_group' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 
'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext work order group' as "DESCRIPTION", 96 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'ext_work_order_status' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext work order status' as 
"DESCRIPTION", 97 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as 
"RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'ext_work_order_type' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", 
to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'ext work order type' as "DESCRIPTION", 98 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null 
as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'external_wo_number' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' 
as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'external wo number' as "DESCRIPTION", 99 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as 
"DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'financial_analysis' as "COLUMN_NAME",
 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'financial analysis' as "DESCRIPTION", 100 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'funding_proj_number' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'funding proj number' as "DESCRIPTION", 101 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'funding_wo_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'funding wo id' as "DESCRIPTION", 102 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'funding_wo_indicator' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 
'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'funding wo indicator' as "DESCRIPTION", 103 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'future_projects' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') 
as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'future projects' as "DESCRIPTION", 104 as "COLUMN_RANK", null 
as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", 
null as "DEFAULT_VALUE" from dual union all select 'in_service_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'in service date' as "DESCRIPTION", 105 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'initiation_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'initiation date' as "DESCRIPTION", 106 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'initiator' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'initiator' as "DESCRIPTION", 107 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'long_description' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'long description' as "DESCRIPTION", 
108 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as 
"RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'major_location_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", 
to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'major location id' as "DESCRIPTION", 109 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as 
"RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'new_budget' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as 
"TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'new budget' as "DESCRIPTION", 110 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", 
null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'notes' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as 
"TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'notes' as "DESCRIPTION", 111 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null 
as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'old_wo_status_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as 
"TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'old wo status id' as "DESCRIPTION", 112 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as 
"DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'other_contact' as "COLUMN_NAME", 
'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'other contact' as "DESCRIPTION", 113 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", 
null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'out_of_service_date' as 
"COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 
'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'out of service date' as "DESCRIPTION", 114 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'plant_accountant' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null 
as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'plant accountant' as "DESCRIPTION", 115 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'project_manager' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'project manager' as "DESCRIPTION", 116 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", 
null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from 
dual union all select 'reason_cd_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' 
as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'reason cd id' as "DESCRIPTION", 117 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", 
null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from 
dual union all select 'reason_for_work' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 
'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'reason for work' as "DESCRIPTION", 118 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'reimbursable_type_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'reimbursable type id' as "DESCRIPTION",
 119 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as 
"RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'repair_location_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", 
to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'repair location id' as "DESCRIPTION", 120 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null 
as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'row_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as 
"TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'row id' as "DESCRIPTION", 121 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null 
as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'seq_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as 
"TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'seq id' as "DESCRIPTION", 122 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null 
as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'status' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as 
"TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'status' as "DESCRIPTION", 123 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null 
as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'suspended_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as 
"TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'suspended date' as "DESCRIPTION", 124 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as 
"DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'time_stamp' as "COLUMN_NAME", 
'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'time stamp' as "DESCRIPTION", 100 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", 
null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'user_id' as "COLUMN_NAME", 
'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'user id' as "DESCRIPTION", 101 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null 
as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'wo_approval_group_id' as 
"COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 
'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'wo approval group id' as "DESCRIPTION", 127 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'wo_status_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'wo status id' as "DESCRIPTION", 128 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null 
as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'work_order_grp_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'work order grp id' as "DESCRIPTION", 129 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'work_order_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'work order id' as "DESCRIPTION", 130 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'work_order_number' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' 
as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'work order number' as "DESCRIPTION", 131 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION",
 null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from 
dual union all select 'work_order_type_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:15', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 
'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'work order type id' as "DESCRIPTION", 132 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'action' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'action' as "DESCRIPTION", 1 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'afudc_start_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'afudc start date' as "DESCRIPTION", 2 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'afudc_stop_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", 
to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'afudc stop date' as "DESCRIPTION", 3 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as 
"RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'agreement_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as 
"TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'agreement id' as "DESCRIPTION", 4 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", 
null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'alternatives' as "COLUMN_NAME", 
'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'alternatives' as "DESCRIPTION", 5 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", 
null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'asset_location_id' as 
"COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'asset location id' as "DESCRIPTION", 6 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'auto_approved' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'auto approved' as "DESCRIPTION", 7 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'background' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'background' as "DESCRIPTION", 8 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'base_year' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'base year' as "DESCRIPTION", 9 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'batch_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'batch id' as "DESCRIPTION", 10 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'budget_company_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'budget company id' as "DESCRIPTION", 11 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", 
null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from 
dual union all select 'budget_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' 
as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'budget id' as "DESCRIPTION", 12 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'budget_number' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'budget number' as "DESCRIPTION", 13 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'budget_version_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 
'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'budget version id' as "DESCRIPTION", 14 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'bus_segment_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'bus segment id' as "DESCRIPTION", 15 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code1' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code1' as "DESCRIPTION", 16 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code10' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code10' as "DESCRIPTION", 17 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code11' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code11' as "DESCRIPTION", 18 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code12' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code12' as "DESCRIPTION", 19 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code13' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code13' as "DESCRIPTION", 20 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code14' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code14' as "DESCRIPTION", 21 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code15' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code15' as "DESCRIPTION", 22 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code16' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code16' as "DESCRIPTION", 23 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code17' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code17' as "DESCRIPTION", 24 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code18' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code18' as "DESCRIPTION", 25 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code19' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code19' as "DESCRIPTION", 26 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code2' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code2' as "DESCRIPTION", 27 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code20' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code20' as "DESCRIPTION", 28 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code21' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code21' as "DESCRIPTION", 29 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code22' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code22' as "DESCRIPTION", 30 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code23' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code23' as "DESCRIPTION", 31 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code24' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code24' as "DESCRIPTION", 32 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code25' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code25' as "DESCRIPTION", 33 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code26' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code26' as "DESCRIPTION", 34 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code27' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code27' as "DESCRIPTION", 35 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code28' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code28' as "DESCRIPTION", 36 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code29' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code29' as "DESCRIPTION", 37 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code3' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code3' as "DESCRIPTION", 38 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code30' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code30' as "DESCRIPTION", 39 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code4' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code4' as "DESCRIPTION", 40 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code5' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code5' as "DESCRIPTION", 41 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code6' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code6' as "DESCRIPTION", 42 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code7' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code7' as "DESCRIPTION", 43 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code8' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code8' as "DESCRIPTION", 44 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_code9' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code9' as "DESCRIPTION", 45 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value1' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value1' as "DESCRIPTION", 46 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value10' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value10' as "DESCRIPTION", 47 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value11' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value11' as "DESCRIPTION", 48 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value12' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value12' as "DESCRIPTION", 49 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value13' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value13' as "DESCRIPTION", 50 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value14' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value14' as "DESCRIPTION", 51 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value15' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value15' as "DESCRIPTION", 52 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value16' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value16' as "DESCRIPTION", 53 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value17' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value17' as "DESCRIPTION", 54 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value18' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value18' as "DESCRIPTION", 55 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value19' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value19' as "DESCRIPTION", 56 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value2' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value2' as "DESCRIPTION", 57 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value20' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value20' as "DESCRIPTION", 58 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value21' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value21' as "DESCRIPTION", 59 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value22' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value22' as "DESCRIPTION", 60 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value23' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value23' as "DESCRIPTION", 61 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value24' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value24' as "DESCRIPTION", 62 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value25' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value25' as "DESCRIPTION", 63 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value26' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value26' as "DESCRIPTION", 64 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value27' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value27' as "DESCRIPTION", 65 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value28' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value28' as "DESCRIPTION", 66 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value29' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value29' as "DESCRIPTION", 67 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value3' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value3' as "DESCRIPTION", 68 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value30' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value30' as "DESCRIPTION", 69 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value4' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value4' as "DESCRIPTION", 70 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value5' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value5' as "DESCRIPTION", 71 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value6' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value6' as "DESCRIPTION", 72 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value7' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value7' as "DESCRIPTION", 73 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value8' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value8' as "DESCRIPTION", 74 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'class_value9' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value9' as "DESCRIPTION", 75 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'close_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'close date' as "DESCRIPTION", 76 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'company_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'company id' as "DESCRIPTION", 77 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'completion_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", 
to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'completion date' as "DESCRIPTION", 78 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as 
"RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'contract_admin' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as 
"TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'contract admin' as "DESCRIPTION", 79 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as 
"DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'department_id' as "COLUMN_NAME", 
'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'department id' as "DESCRIPTION", 80 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", 
null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'description' as 
"COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'description' as "DESCRIPTION", 0 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'engineer' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'engineer' as "DESCRIPTION", 82 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'error_message' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'error message' as "DESCRIPTION", 83 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'est_annual_rev' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'est annual rev' as "DESCRIPTION", 84 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'est_complete_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 
'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'est complete date' as "DESCRIPTION", 85 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'est_in_service_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'est in service date' as "DESCRIPTION", 
86 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as 
"RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'est_start_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", 
to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'est start date' as "DESCRIPTION", 87 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as 
"RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'ext_agreement' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as 
"TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'ext agreement' as "DESCRIPTION", 88 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT",
 null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'ext_asset_location' as "COLUMN_NAME", 
'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext asset location' as "DESCRIPTION", 89 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'ext_company' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null 
as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext company' as "DESCRIPTION", 90 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'ext_department' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext department' as "DESCRIPTION", 91 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'ext_major_location' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' 
as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext major location' as "DESCRIPTION", 92 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION",
 null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from 
dual union all select 'ext_reason_cd' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 
'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext reason cd' as "DESCRIPTION", 93 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'ext_repair_location' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext repair location' as "DESCRIPTION", 
94 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as 
"RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'ext_wo_approval_group' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", 
to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'ext wo approval group' as "DESCRIPTION", 95 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null 
as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'ext_work_order_group' as "COLUMN_NAME", 
'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext work order group' as "DESCRIPTION", 96 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'ext_work_order_status' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext work order status' as "DESCRIPTION", 97 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION",
 null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from 
dual union all select 'ext_work_order_type' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 
'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext work order type' as "DESCRIPTION", 98 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'external_wo_number' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'external wo number' as "DESCRIPTION", 
99 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as 
"RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'financial_analysis' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", 
to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'financial analysis' as "DESCRIPTION", 100 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null 
as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'funding_proj_number' as "COLUMN_NAME", 
'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'funding proj number' as "DESCRIPTION", 101 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'funding_wo_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'funding wo id' as "DESCRIPTION", 102 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'funding_wo_indicator' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' 
as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'funding wo indicator' as "DESCRIPTION", 103 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'future_projects' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'future projects' as "DESCRIPTION", 104 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'in_service_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", 
to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'in service date' as "DESCRIPTION", 105 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as 
"RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'initiation_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as 
"TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'initiation date' as "DESCRIPTION", 106 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as 
"DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'initiator' as "COLUMN_NAME", 
'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'initiator' as "DESCRIPTION", 107 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", 
null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'long_description' as 
"COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'long description' as "DESCRIPTION", 108 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'major_location_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'major location id' as "DESCRIPTION", 109 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'new_budget' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'new budget' as "DESCRIPTION", 110 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'notes' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'notes' as "DESCRIPTION", 111 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'old_wo_status_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'old wo status id' as "DESCRIPTION", 112 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'other_contact' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'other contact' as "DESCRIPTION", 113 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'out_of_service_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 
'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'out of service date' as "DESCRIPTION", 114 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'plant_accountant' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'plant accountant' as "DESCRIPTION", 
115 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as 
"RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'project_manager' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", 
to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'project manager' as "DESCRIPTION", 116 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as 
"RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'reason_cd_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as 
"TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'reason cd id' as "DESCRIPTION", 117 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT",
 null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'reason_for_work' as "COLUMN_NAME", 
'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'reason for work' as "DESCRIPTION", 118 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'reimbursable_type_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'reimbursable type id' as "DESCRIPTION", 119 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION",
 null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from 
dual union all select 'repair_location_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 
'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'repair location id' as "DESCRIPTION", 120 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'row_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'row id' as "DESCRIPTION", 121 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'seq_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'seq id' as "DESCRIPTION", 122 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'status' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'status' as "DESCRIPTION", 123 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'suspended_date' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'suspended date' as "DESCRIPTION", 124 
as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'time_stamp' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'time stamp' as "DESCRIPTION", 100 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'user_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'user id' as "DESCRIPTION", 101 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'wo_approval_group_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", 
to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'wo approval group id' as "DESCRIPTION", 127 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null 
as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'wo_status_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as 
"TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'wo status id' as "DESCRIPTION", 128 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT",
 null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'work_order_grp_id' as "COLUMN_NAME", 
'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'work order grp id' as "DESCRIPTION", 129 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'work_order_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'work order id' as "DESCRIPTION", 130 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'work_order_number' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'work order number' as "DESCRIPTION", 131 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", 
null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from 
dual union all select 'work_order_type_id' as "COLUMN_NAME", 'LGEKU_PROJECT_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:21:30', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 
'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'work order type id' as "DESCRIPTION", 132 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'action' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'action' as "DESCRIPTION", 1 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'alternatives' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'alternatives' as "DESCRIPTION", 2 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'background' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'background' as "DESCRIPTION", 3 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'batch_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'batch id' as "DESCRIPTION", 4 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'budget_analyst' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'budget analyst' as "DESCRIPTION", 5 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'chargeable' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'chargeable' as "DESCRIPTION", 6 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code1' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code1' as "DESCRIPTION", 7 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code10' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code10' as "DESCRIPTION", 8 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code11' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code11' as "DESCRIPTION", 9 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code12' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code12' as "DESCRIPTION", 10 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code13' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code13' as "DESCRIPTION", 11 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code14' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code14' as "DESCRIPTION", 12 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code15' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code15' as "DESCRIPTION", 13 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code16' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code16' as "DESCRIPTION", 14 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code17' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code17' as "DESCRIPTION", 15 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code18' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code18' as "DESCRIPTION", 16 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code19' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code19' as "DESCRIPTION", 17 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code2' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code2' as "DESCRIPTION", 18 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code20' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code20' as "DESCRIPTION", 19 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code3' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code3' as "DESCRIPTION", 20 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code4' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code4' as "DESCRIPTION", 21 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code5' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code5' as "DESCRIPTION", 22 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code6' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code6' as "DESCRIPTION", 23 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code7' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code7' as "DESCRIPTION", 24 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code8' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code8' as "DESCRIPTION", 25 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code9' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code9' as "DESCRIPTION", 26 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value1' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value1' as "DESCRIPTION", 27 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value10' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value10' as "DESCRIPTION", 28 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value11' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value11' as "DESCRIPTION", 29 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value12' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value12' as "DESCRIPTION", 30 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value13' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value13' as "DESCRIPTION", 31 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value14' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value14' as "DESCRIPTION", 32 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value15' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value15' as "DESCRIPTION", 33 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value16' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value16' as "DESCRIPTION", 34 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value17' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value17' as "DESCRIPTION", 35 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value18' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value18' as "DESCRIPTION", 36 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value19' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value19' as "DESCRIPTION", 37 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value2' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value2' as "DESCRIPTION", 38 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value20' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value20' as "DESCRIPTION", 39 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value3' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value3' as "DESCRIPTION", 40 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value4' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value4' as "DESCRIPTION", 41 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value5' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value5' as "DESCRIPTION", 42 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value6' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value6' as "DESCRIPTION", 43 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value7' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value7' as "DESCRIPTION", 44 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value8' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value8' as "DESCRIPTION", 45 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value9' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value9' as "DESCRIPTION", 46 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'company_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'company id' as "DESCRIPTION", 47 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'completion_date' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'completion date' as "DESCRIPTION", 48 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'description' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'description' as "DESCRIPTION", 0 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'engineer' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'engineer' as "DESCRIPTION", 50 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'error_message' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'error message' as "DESCRIPTION", 51 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'est_complete_date' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'est complete date' as "DESCRIPTION", 52 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'est_start_date' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'est start date' as "DESCRIPTION", 53 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'estimate' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'estimate' as "DESCRIPTION", 54 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'ext_company' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'ext company' as "DESCRIPTION", 55 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'external_job_task' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'external job task' as "DESCRIPTION", 56 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'field1' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'field1' as "DESCRIPTION", 57 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'field2' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'field2' as "DESCRIPTION", 58 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'field3' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'field3' as "DESCRIPTION", 59 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'field4' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'field4' as "DESCRIPTION", 60 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'field5' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'field5' as "DESCRIPTION", 61 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'financial_analysis' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') 
as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'financial analysis' as "DESCRIPTION", 62 as "COLUMN_RANK", null 
as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", 
null as "DEFAULT_VALUE" from dual union all select 'forecast' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'forecast' as "DESCRIPTION", 63 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'funding_task_indicator' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'funding task indicator' as 
"DESCRIPTION", 64 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as 
"RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'future_projects' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", 
to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'future projects' as "DESCRIPTION", 65 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as 
"RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'group_descr' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as 
"TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'group descr' as "DESCRIPTION", 66 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", 
null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'group_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as 
"TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'group id' as "DESCRIPTION", 67 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", 
null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'initiator' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as 
"TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'initiator' as "DESCRIPTION", 68 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", 
null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'job_task_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as 
"TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'job task id' as "DESCRIPTION", 69 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", 
null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'job_task_status_descr' as "COLUMN_NAME", 
'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'job task status descr' as "DESCRIPTION", 70 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'job_task_status_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null 
as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'job task status id' as "DESCRIPTION", 71 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'level_number' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'level number' as "DESCRIPTION", 72 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'long_description' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'long description' as "DESCRIPTION", 73 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", 
null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from 
dual union all select 'notes' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'notes' as "DESCRIPTION", 74 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'other_contact' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'other contact' as "DESCRIPTION", 75 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'parent_job_task' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'parent job task' as "DESCRIPTION", 76 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'percent_complete' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'percent complete' as "DESCRIPTION", 77 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", 
null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from 
dual union all select 'priority_code_descr' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 
'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'priority code descr' as "DESCRIPTION", 78 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'priority_code_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'priority code id' as "DESCRIPTION", 79 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'project_manager' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'project manager' as "DESCRIPTION", 80 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'reason_for_work' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'reason for work' as "DESCRIPTION", 81 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'row_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'row id' as "DESCRIPTION", 82 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'seq_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'seq id' as "DESCRIPTION", 83 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'setup_template' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'setup template' as "DESCRIPTION", 84 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'setup_template_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'setup template id' as "DESCRIPTION", 85 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'status' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'status' as "DESCRIPTION", 86 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'status_code_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'status code id' as "DESCRIPTION", 87 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'system_descr' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'system descr' as "DESCRIPTION", 88 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'system_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'system id' as "DESCRIPTION", 89 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'task_owner' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'task owner' as "DESCRIPTION", 90 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'time_stamp' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'time stamp' as "DESCRIPTION", 100 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'user_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'user id' as "DESCRIPTION", 101 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'work_effort' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'work effort' as "DESCRIPTION", 93 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'work_order_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'work order id' as "DESCRIPTION", 94 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'work_order_number' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:21:56', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'work order number' as "DESCRIPTION", 95 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'action' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'action' as "DESCRIPTION", 1 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'alternatives' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'alternatives' as "DESCRIPTION", 2 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'background' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'background' as "DESCRIPTION", 3 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'batch_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'batch id' as "DESCRIPTION", 4 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'budget_analyst' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') 
as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'budget analyst' as "DESCRIPTION", 5 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'chargeable' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'chargeable' as "DESCRIPTION", 6 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code1' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code1' as "DESCRIPTION", 7 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code10' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code10' as "DESCRIPTION", 8 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code11' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code11' as "DESCRIPTION", 9 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code12' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code12' as "DESCRIPTION", 10 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code13' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code13' as "DESCRIPTION", 11 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code14' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code14' as "DESCRIPTION", 12 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code15' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code15' as "DESCRIPTION", 13 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code16' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code16' as "DESCRIPTION", 14 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code17' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code17' as "DESCRIPTION", 15 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code18' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code18' as "DESCRIPTION", 16 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code19' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code19' as "DESCRIPTION", 17 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code2' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code2' as "DESCRIPTION", 18 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code20' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code20' as "DESCRIPTION", 19 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code3' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code3' as "DESCRIPTION", 20 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code4' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code4' as "DESCRIPTION", 21 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code5' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code5' as "DESCRIPTION", 22 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code6' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code6' as "DESCRIPTION", 23 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code7' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code7' as "DESCRIPTION", 24 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code8' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code8' as "DESCRIPTION", 25 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_code9' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class code9' as "DESCRIPTION", 26 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value1' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value1' as "DESCRIPTION", 27 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value10' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value10' as "DESCRIPTION", 28 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value11' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value11' as "DESCRIPTION", 29 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value12' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value12' as "DESCRIPTION", 30 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value13' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value13' as "DESCRIPTION", 31 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value14' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value14' as "DESCRIPTION", 32 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value15' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value15' as "DESCRIPTION", 33 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value16' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value16' as "DESCRIPTION", 34 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value17' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value17' as "DESCRIPTION", 35 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value18' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value18' as "DESCRIPTION", 36 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value19' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value19' as "DESCRIPTION", 37 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value2' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value2' as "DESCRIPTION", 38 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value20' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value20' as "DESCRIPTION", 39 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value3' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value3' as "DESCRIPTION", 40 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value4' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value4' as "DESCRIPTION", 41 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value5' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value5' as "DESCRIPTION", 42 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value6' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value6' as "DESCRIPTION", 43 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value7' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value7' as "DESCRIPTION", 44 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value8' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value8' as "DESCRIPTION", 45 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'class_value9' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'class value9' as "DESCRIPTION", 46 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'company_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'company id' as "DESCRIPTION", 47 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'completion_date' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') 
as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'completion date' as "DESCRIPTION", 48 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'description' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'description' as "DESCRIPTION", 0 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'engineer' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'engineer' as "DESCRIPTION", 50 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'error_message' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'error message' as "DESCRIPTION", 51 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'est_complete_date' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'est complete date' as "DESCRIPTION", 
52 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as 
"RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'est_start_date' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", 
to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'est start date' as "DESCRIPTION", 53 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as 
"RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'estimate' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as 
"TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'estimate' as "DESCRIPTION", 54 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", 
null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'ext_company' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as 
"TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'ext company' as "DESCRIPTION", 55 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", 
null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'external_job_task' as "COLUMN_NAME", 
'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'external job task' as "DESCRIPTION", 56 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'field1' 
as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'field1' as "DESCRIPTION", 57 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'field2' 
as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'field2' as "DESCRIPTION", 58 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'field3' 
as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'field3' as "DESCRIPTION", 59 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'field4' 
as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'field4' as "DESCRIPTION", 60 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'field5' 
as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'field5' as "DESCRIPTION", 61 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'financial_analysis' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'financial analysis' as "DESCRIPTION", 62 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'forecast' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'forecast' as "DESCRIPTION", 63 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'funding_task_indicator' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'funding task indicator' as "DESCRIPTION", 64 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'future_projects' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') 
as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'future projects' as "DESCRIPTION", 65 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'group_descr' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'group descr' as "DESCRIPTION", 66 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'group_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'group id' as "DESCRIPTION", 67 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'initiator' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'initiator' as "DESCRIPTION", 68 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'job_task_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'job task id' as "DESCRIPTION", 69 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'job_task_status_descr' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'job task status descr' as 
"DESCRIPTION", 70 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as 
"RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'job_task_status_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", 
to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'job task status id' as "DESCRIPTION", 71 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as 
"RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'level_number' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as 
"TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'level number' as "DESCRIPTION", 72 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", 
null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'long_description' as "COLUMN_NAME", 
'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'long description' as "DESCRIPTION", 73 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'notes' 
as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'notes' as "DESCRIPTION", 74 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'other_contact' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null 
as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'other contact' as "DESCRIPTION", 75 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'parent_job_task' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null 
as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'parent job task' as "DESCRIPTION", 76 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'percent_complete' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'percent complete' as "DESCRIPTION", 77 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'priority_code_descr' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'priority code descr' as "DESCRIPTION", 78 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", 
null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from 
dual union all select 'priority_code_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 
'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'priority code id' as "DESCRIPTION", 79 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'project_manager' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') 
as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'project manager' as "DESCRIPTION", 80 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'reason_for_work' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') 
as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'reason for work' as "DESCRIPTION", 81 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'row_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'row id' as "DESCRIPTION", 82 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'seq_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as 
"TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'seq id' as "DESCRIPTION", 83 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'setup_template' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') 
as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'setup template' as "DESCRIPTION", 84 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'setup_template_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'setup template id' as "DESCRIPTION", 
85 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as 
"RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'status' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", 
to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'status' 
as "DESCRIPTION", 86 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null 
as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'status_code_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", 
to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'status code id' as "DESCRIPTION", 87 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as 
"RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'system_descr' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as 
"TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'system descr' as "DESCRIPTION", 88 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", 
null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'system_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as 
"TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'system id' as "DESCRIPTION", 89 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", 
null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'task_owner' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as 
"TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'task owner' as "DESCRIPTION", 90 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", 
null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'time_stamp' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as 
"TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'time stamp' as "DESCRIPTION", 100 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", 
null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'user_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as 
"TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'user id' as "DESCRIPTION", 101 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", 
null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'work_effort' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' as 
"TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'work effort' as "DESCRIPTION", 93 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", 
null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'work_order_id' as "COLUMN_NAME", 'LGEKU_TASK_UPLOAD_ARC' 
as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as 
"HELP_INDEX", 'work order id' as "DESCRIPTION", 94 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT",
 null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'work_order_number' as "COLUMN_NAME", 
'LGEKU_TASK_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:09', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'work order number' as "DESCRIPTION", 95 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'chargeable' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:25', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'chargeable' as "DESCRIPTION", 1 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'completion_date' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:25', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID",
 null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'completion date' as "DESCRIPTION", 2 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'engineer' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:25', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'engineer' as "DESCRIPTION", 3 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'est_start_date' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:25', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'est start date' as "DESCRIPTION", 4 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'job_task_id' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:25', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' 
as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'job task id' as "DESCRIPTION", 5 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'other_contact' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:25', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 
'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'other contact' as "DESCRIPTION", 6 as "COLUMN_RANK", null as 
"SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null 
as "DEFAULT_VALUE" from dual union all select 'status_code_id' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:25', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'status code id' as "DESCRIPTION", 7 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'time_stamp' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:25',
 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'time stamp' as "DESCRIPTION", 100 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'user_id' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:25', 
'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'user id' as "DESCRIPTION", 101 as 
"COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", 
null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'work_order_id' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD_ARC' as "TABLE_NAME", 
to_date('2019-07-30 10:22:25', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 
'work order id' as "DESCRIPTION", 10 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as 
"RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 'work_order_number' as "COLUMN_NAME", 
'LGEKU_TASK_ADMIN_UPLOAD_ARC' as "TABLE_NAME", to_date('2019-07-30 10:22:25', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as "DROPDOWN_NAME", 'e' as 
"PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'work order number' as "DESCRIPTION", 11 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'chargeable' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:22:39', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null as 
"DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'chargeable' as "DESCRIPTION", 1 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", null as 
"DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'completion_date' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:22:39', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'completion date' as "DESCRIPTION", 2 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'engineer' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:22:39', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID",
 null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'engineer' as "DESCRIPTION", 3 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'est_start_date' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:22:39', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'est start date' as "DESCRIPTION", 4 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'job_task_id' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:22:39', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'job task id' as "DESCRIPTION", 5 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'other_contact' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:22:39', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'other contact' as "DESCRIPTION", 6 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'status_code_id' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:22:39', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'status code id' as "DESCRIPTION", 7 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null 
as "EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual 
union all select 'time_stamp' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:22:39', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as 
"USER_ID", null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'time stamp' as "DESCRIPTION", 100 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union 
all select 'user_id' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:22:39', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'user id' as "DESCRIPTION", 101 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'work_order_id' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:22:39', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", null 
as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'work order id' as "DESCRIPTION", 10 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as "EDIT_MASK", 
null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual union all select 
'work_order_number' as "COLUMN_NAME", 'LGEKU_TASK_ADMIN_UPLOAD' as "TABLE_NAME", to_date('2019-07-30 10:22:39', 'yyyy-mm-dd hh24:mi:ss') as "TIME_STAMP", 'PWRPLANT' as "USER_ID", 
null as "DROPDOWN_NAME", 'e' as "PP_EDIT_TYPE_ID", null as "HELP_INDEX", 'work order number' as "DESCRIPTION", 11 as "COLUMN_RANK", null as "SHORT_LONG_DESCRIPTION", null as 
"EDIT_MASK", null as "DROPDOWN_PERCENT", null as "DROPDOWN_RESTRICT", null as "RELATED_TYPE", null as "RELATED_TABLE", null as "READ_ONLY", null as "DEFAULT_VALUE" from dual) B 
 ON (A."COLUMN_NAME" = B."COLUMN_NAME" AND A."TABLE_NAME" = B."TABLE_NAME") WHEN NOT MATCHED THEN INSERT ("COLUMN_NAME", "TABLE_NAME", "TIME_STAMP", "USER_ID", "DROPDOWN_NAME", 
"PP_EDIT_TYPE_ID", "HELP_INDEX", "DESCRIPTION", "COLUMN_RANK", "SHORT_LONG_DESCRIPTION", "EDIT_MASK", "DROPDOWN_PERCENT", "DROPDOWN_RESTRICT", "RELATED_TYPE", "RELATED_TABLE", 
"READ_ONLY", "DEFAULT_VALUE") values (B."COLUMN_NAME", B."TABLE_NAME", B."TIME_STAMP", B."USER_ID", B."DROPDOWN_NAME", B."PP_EDIT_TYPE_ID", B."HELP_INDEX", B."DESCRIPTION", 
B."COLUMN_RANK", B."SHORT_LONG_DESCRIPTION", B."EDIT_MASK", B."DROPDOWN_PERCENT", B."DROPDOWN_RESTRICT", B."RELATED_TYPE", B."RELATED_TABLE", B."READ_ONLY", B."DEFAULT_VALUE") 
WHEN MATCHED THEN UPDATE SET A."TIME_STAMP" = B."TIME_STAMP", A."USER_ID" = B."USER_ID", A."DROPDOWN_NAME" = B."DROPDOWN_NAME", A."PP_EDIT_TYPE_ID" = B."PP_EDIT_TYPE_ID", 
A."HELP_INDEX" = B."HELP_INDEX", A."DESCRIPTION" = B."DESCRIPTION", A."COLUMN_RANK" = B."COLUMN_RANK", A."SHORT_LONG_DESCRIPTION" = B."SHORT_LONG_DESCRIPTION", A."EDIT_MASK" = 
B."EDIT_MASK", A."DROPDOWN_PERCENT" = B."DROPDOWN_PERCENT", A."DROPDOWN_RESTRICT" = B."DROPDOWN_RESTRICT", A."RELATED_TYPE" = B."RELATED_TYPE", A."RELATED_TABLE" = 
B."RELATED_TABLE", A."READ_ONLY" = B."READ_ONLY", A."DEFAULT_VALUE" = B."DEFAULT_VALUE"; 

/*
|| Log the run of the script
*/
insert into PP_UPDATE_FLEX
                (COL_ID,
                 TYPE_NAME,
                 FLEXVCHAR1)
select NVL(MAX(COL_ID), 0) + 1,
                 '2018_Upgrade_LKE_scripts',
                '0522_LKE_B50_add_powerplant_tables_DML.sql' --this should be your script name
from PP_UPDATE_FLEX;
commit;