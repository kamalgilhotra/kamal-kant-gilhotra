SELECT DECODE (COUNT (*), 0, 0, 1)
  FROM WORK_ORDER_APPROVAL A
 WHERE EXISTS (SELECT 1
                 FROM WORKFLOW W
                WHERE W.ID_FIELD1 = A.WORK_ORDER_ID
                  AND W.SUBSYSTEM = 'fp_approval'
                  AND W.APPROVAL_STATUS_ID = 6
                  AND W.ID_FIELD1 = <arg1>)
   AND A.APPROVAL_STATUS_ID = 1
   AND A.APPROVAL_TYPE_ID = 99

--Final Query 1
UPDATE work_order_approval A
   SET A.approval_status_id = 6
 WHERE EXISTS (SELECT 1
                 FROM workflow w
                WHERE w.id_field1 = A.work_order_id
                  AND w.subsystem = 'fp_approval'
                  AND w.approval_status_id = 6
                  AND w.id_field1 = A.work_order_id)
   AND A.approval_status_id = 1
   AND A.approval_type_id = 99
   AND A.work_order_id IN(SELECT woc.work_order_id
                            FROM work_order_control woc,
                                 lgeku_project_upload U
                           WHERE woc.funding_wo_indicator = 1
                             AND U.interface_batch IS NOT NULL
                             AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
                             AND NVL(U.status, 0) <> -1
                             AND woc.work_order_number = U.work_order_number
                             AND woc.company_id = U.company_id);

--Final Query 2
UPDATE work_order_control c
   SET c.wo_status_id = 2
 WHERE EXISTS (SELECT 1
                 FROM workflow w
                WHERE w.id_field1 = c.work_order_id
                  AND w.subsystem = 'fp_approval'
                  AND w.approval_status_id = 6
                  AND w.id_field1 = C.work_order_id)
   AND EXISTS (SELECT 1
                 FROM work_order_approval A
                WHERE A.work_order_id = c.work_order_id
                  AND A.approval_type_id = 99
                  AND A.approval_status_id = 6)
   AND c.funding_wo_indicator = 1
   AND c.wo_status_id = 1
   AND (c.work_order_number, c.company_id) IN(SELECT U.work_order_number, U.company_id
                                                FROM lgeku_project_upload U
                                               WHERE U.interface_batch IS NOT NULL
                                                 AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
                                                 AND NVL(U.status, 0) <> -1)