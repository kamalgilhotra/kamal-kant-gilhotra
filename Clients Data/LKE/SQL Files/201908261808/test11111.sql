SELECT work_order_number, company_id, class_code_id,
       DECODE(seq_no, 1, (SELECT class_code_value
                            FROM work_order_class_code lgeku_project_upload_cc_values
                           WHERE lgeku_project_upload_cc_values.work_order_number = x.work_order_number
                             AND lgeku_project_upload_cc_values.company_id = x.company_id
                             AND lgeku_project_upload_cc_values.class_code_id = x.class_code_id),
                      2, (SELECT class_code_value
                            FROM work_order_class_code lgeku_project_upload_cc_values
                           WHERE lgeku_project_upload_cc_values.work_order_number = x.work_order_number
                             AND lgeku_project_upload_cc_values.company_id = x.company_id
                             AND lgeku_project_upload_cc_values.class_code_id = x.class_code_id)) class_code_value
FROM 
(SELECT work_order_number, company_id, class_code_id, MIN(seq_no) seq_no FROM
(SELECT class_code_id,
               1 seq_no,
               (SELECT work_order_number FROM work_order_control WHERE work_order_id = lgeku_project_upload_cc_values.work_order_id) work_order_number,
               (SELECT company_id FROM work_order_control WHERE work_order_id = lgeku_project_upload_cc_values.work_order_id) company_id
          FROM work_order_class_code lgeku_project_upload_cc_values
         WHERE class_code_id IN(52, 53)
       UNION ALL
       SELECT class_code_id,
              2 seq_no,
              (SELECT work_order_number FROM work_order_control WHERE work_order_id = lgeku_project_upload_cc_values.work_order_id) work_order_number,
              (SELECT company_id FROM work_order_control WHERE work_order_id = lgeku_project_upload_cc_values.work_order_id) company_id
         FROM work_order_class_code lgeku_project_upload_cc_values
        WHERE class_code_id IN(52, 53))
WHERE (work_order_number, company_id) IN(SELECT work_order_number, company_id FROM work_order_control WHERE work_order_id IN(18379688, 251656091))
GROUP BY work_order_number, company_id, class_code_id) x

SELECT * FROM work_order_class_code
WHERE class_code_id IN(52, 53)
AND work_order_id IN(18379688, 251656091)


SELECT work_order_number, company_id,
       (SELECT 1 FROM work_order_class_code l WHERE l.work_order_id = (SELECT work_order_id FROM work_order_control c WHERE c.work_order_number = U.work_order_number AND c.company_id =  U.company_id AND class_code_id = 52)) lcc52,
       (SELECT 1 FROM work_order_class_code l WHERE l.work_order_id = (SELECT work_order_id FROM work_order_control c WHERE c.work_order_number = U.work_order_number AND c.company_id =  U.company_id AND class_code_id = 53)) lcc53,
       (SELECT 2 FROM work_order_class_code w WHERE w.work_order_id = (SELECT work_order_id FROM work_order_control c WHERE c.work_order_number = U.work_order_number AND c.company_id =  U.company_id AND class_code_id = 52)) wocc52,
       (SELECT 2 FROM work_order_class_code w WHERE w.work_order_id = (SELECT work_order_id FROM work_order_control c WHERE c.work_order_number = U.work_order_number AND c.company_id =  U.company_id AND class_code_id = 53)) wocc53
FROM work_order_control U WHERE work_order_id IN(18379688, 251656091)