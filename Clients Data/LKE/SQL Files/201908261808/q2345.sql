update lgeku_project_upload
set error_message = error_message || 'The Funding Project Department is not a valid Project Owning Organization (in the HR All Organization Units table in Oracle Projects). Please select  another Department or contact the Budgeting Group.; ', status = -1
where ext_department in(
SELECT description from department where description in(select ext_department from lgeku_project_upload where nvl(status, 0) <> -1)
minus
select dp.description from department dp, division dv, hr_all_organization_units hr, hr.hr_organization_information@orafin hi,
pa_segment_value_lookup_sets pvls, pa_segment_value_lookups pvl
where hi.organization_id = hr.organization_id
and   hr.attribute3 = pvl.segment_value
and   pvls.segment_value_lookup_set_id = pvl.segment_value_lookup_set_id
and   pvls.segment_value_lookup_set_name = 'Proj Org to GL Responsible Ctr'
and dp.external_department_code = hr.attribute3
and dp.division_id = dv.division_id
and hr.type not in('CORP', 'INV', 'SBG')
and hi.org_information1 = 'PA_PROJECT_ORG'
and hi.org_information2 = 'Y'
and hr.date_to is null
and dv.company_id = decode(to_number(hr.attribute1), 700, 100, 702, 800, 703, 301, 704, 4, 710, 110, 720, 20, to_number(hr.attribute1))
and dp.department_id IN(SELECT department_id from department where description in(select ext_department from lgeku_project_upload where nvl(status, 0) <> -1)))

select decode(count(*), 0, 1, 0) does_not_exist
from department dp, division dv,
     hr_all_organization_units hr,
     hr.hr_organization_information@orafin hi,
     pa_segment_value_lookup_sets pvls,
     pa_segment_value_lookups pvl
where hi.organization_id = hr.organization_id
and hr.attribute3 = pvl.segment_value
and pvls.segment_value_lookup_set_id = pvl.segment_value_lookup_set_id
and pvls.segment_value_lookup_set_name = 'Proj Org to GL Responsible Ctr'
and dp.external_department_code = hr.attribute3
and dv.division_id = dp.division_id
and hr.type not in ('CORP', 'INV', 'SBG')
and hi.org_information1 = 'PA_PROJECT_ORG'
and hi.org_information2 = 'Y'
and hr.date_to IS NULL
and dv.company_id = decode(to_number(hr.attribute1), 700, 100, 702, 800, 703, 301, 704, 4, 710, 110, 720, 20, to_number(hr.attribute1))
and dp.department_id = 1297


select d.description, d.department_id, status, dbms_lob.substr(error_message, 500, 1) err_msg, u.*
from lgeku_project_upload u, department d where u.ext_department = d.description

update lgeku_project_upload set status = null, error_message = null

commit

rollback

select * from lgeku_project_upload_arc where error_message like '%wning%'

select * from department where department_id = 1297

update lgeku_project_upload u
set error_message = error_message || 'The Funding Project Department is not a valid Project Owning Organization (in the HR All Organization Units table in Oracle Projects). Please select  another Department or contact the Budgeting Group.; ', status = -1
where not exists (
select dp.description from department dp, division dv, hr_all_organization_units hr, hr.hr_organization_information@orafin hi,
pa_segment_value_lookup_sets pvls, pa_segment_value_lookups pvl
where hi.organization_id = hr.organization_id
and   hr.attribute3 = pvl.segment_value
and   pvls.segment_value_lookup_set_id = pvl.segment_value_lookup_set_id
and   pvls.segment_value_lookup_set_name = 'Proj Org to GL Responsible Ctr'
and dp.external_department_code = hr.attribute3
and dp.division_id = dv.division_id
and hr.type not in('CORP', 'INV', 'SBG')
and hi.org_information1 = 'PA_PROJECT_ORG'
and hi.org_information2 = 'Y'
and hr.date_to is null
and dv.company_id = decode(to_number(hr.attribute1), 700, 100, 702, 800, 703, 301, 704, 4, 710, 110, 720, 20, to_number(hr.attribute1))
and dp.description = U.EXT_DEPARTMENT)
and u.interface_batch is not null