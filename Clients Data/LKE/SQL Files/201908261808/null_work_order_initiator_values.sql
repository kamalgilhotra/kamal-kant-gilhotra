UPDATE work_order_initiator woi
   SET engineer = DECODE (engineer, 'null', NULL, engineer),
       plant_accountant = DECODE (plant_accountant, 'null', NULL, plant_accountant),
       proj_mgr = DECODE (proj_mgr, 'null', NULL, proj_mgr),
       contract_adm = DECODE (contract_adm, 'null', NULL, contract_adm),
       other = DECODE (other, 'null', NULL, other)
 WHERE EXISTS
          (SELECT 1
             FROM lgeku_project_upload U, work_order_control woc
            WHERE     U.work_order_number = woc.work_order_number
                  AND U.company_id = woc.company_id
                  AND woc.work_order_id = woi.work_order_id
                  AND U.interface_batch IS NOT NULL
                  AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload))