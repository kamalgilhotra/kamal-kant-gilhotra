SELECT DECODE(COUNT(*), 0, 0, 1)
FROM
(
SELECT woc.work_order_id
FROM work_order_control woc, work_order_class_code wocc
WHERE woc.work_order_id = wocc.work_order_id AND woc.funding_wo_indicator = 1
AND woc.work_order_id = <arg1>
AND class_code_id IN (70,77)
AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 70) IS NOT NULL
AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 77) IS NOT NULL
)

'No Target Numbers are allowed when there is an Allocation Rule defined.'

SELECT interface_batch, funding_wo_indicator, budget_number, EXT_DEPARTMENT, action, status, dbms_lob.substr(error_message, 1000, 1) err_msg, U.*
FROM lgeku_project_upload U

INSERT INTO lgeku_project_upload (company_id, work_order_number, ext_work_order_type, ext_major_location, description, ext_department, est_start_date,
est_complete_date, est_in_service_date, interface_batch)
SELECT company_id, work_order_number, (SELECT description FROM work_order_type x WHERE work_order_type_id = z.work_order_type_id) ext_work_order_type,
       (SELECT description FROM major_location x WHERE major_location_id = z.major_location_id) ext_major_location,
       SUBSTR(TO_CHAR(SYSDATE, 'yyyymmddhhmmss - ') || description, 1, LENGTH(description) - 17) description,
       (SELECT description FROM department x WHERE department_id = z.department_id) ext_department,
       SYSDATE est_start_date, SYSDATE est_complete_date, SYSDATE est_in_service_date, 37 interface_batch
FROM  work_order_control z WHERE work_order_id = 90550994

SELECT * FROM work_order_control WHERE work_order_number = '151893'

COMMIT

ROLLBACK

UPDATE lgeku_project_upload U
SET error_message = error_message || 'No Target Numbers are allowed when there is an Allocation Rule defined.; ', status = -1
WHERE EXISTS(
SELECT 1
FROM work_order_control woc, work_order_class_code wocc
WHERE woc.work_order_id = wocc.work_order_id AND woc.funding_wo_indicator = 1
AND woc.work_order_number = U.work_order_number
AND woc.company_id = U.company_id
AND class_code_id IN (70,77)
AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 70) IS NOT NULL
AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 77) IS NOT NULL
)
AND U.interface_batch IS NOT NULL

-- Final Query
UPDATE lgeku_project_upload U
SET    error_message = error_message || 'No Target Numbers are allowed when there is an Allocation Rule defined.; ', status = -1
WHERE  interface_batch IS NOT NULL
   AND (
        (   (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) IS NOT NULL
           AND (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 77) IS NOT NULL)
        OR
        (   (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) IS NOT NULL
           AND NOT EXISTS(SELECT 1
                        FROM work_order_class_code
                       WHERE work_order_id IN(SELECT work_order_id FROM work_order_control c WHERE c.work_order_number = U.work_order_number AND c.company_id = U.company_id AND c.funding_wo_indicator = 1)
                         AND class_code_id = 70)
           AND EXISTS(SELECT 1
                        FROM work_order_class_code
                       WHERE work_order_id IN(SELECT work_order_id FROM work_order_control c WHERE c.work_order_number = U.work_order_number AND c.company_id = U.company_id AND c.funding_wo_indicator = 1)
                         AND class_code_id = 77
                         AND VALUE IS NOT NULL))
        OR
        (   (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 77) IS NOT NULL
           AND NOT EXISTS(SELECT 1
                        FROM work_order_class_code
                       WHERE work_order_id IN(SELECT work_order_id FROM work_order_control c WHERE c.work_order_number = U.work_order_number AND c.company_id = U.company_id AND c.funding_wo_indicator = 1)
                         AND class_code_id = 77)
           AND EXISTS(SELECT 1
                        FROM work_order_class_code
                       WHERE work_order_id IN(SELECT work_order_id FROM work_order_control c WHERE c.work_order_number = U.work_order_number AND c.company_id = U.company_id AND c.funding_wo_indicator = 1)
                         AND class_code_id = 70
                         AND VALUE IS NOT NULL))
       )