SELECT COUNT (*) AS COUNT
  FROM WORK_ORDER_CONTROL FP, WORK_ORDER_ACCOUNT WA
 WHERE     WA.WORK_ORDER_ID = FP.WORK_ORDER_ID
       AND FP.FUNDING_WO_INDICATOR = 1
       AND WA.WORK_ORDER_GRP_ID = 1
       AND FP.WORK_ORDER_ID = <arg1>
       AND NOT EXISTS
                  (SELECT 1
                     FROM WORK_ORDER_CLASS_CODE WCC
                    WHERE     WCC.CLASS_CODE_ID = 57
                          AND WCC.WORK_ORDER_ID = FP.WORK_ORDER_ID)

-- DV44 (Final Query)
INSERT INTO work_order_class_code (class_code_id, work_order_id, VALUE)
   SELECT 57, woc.work_order_id, 'NA'
     FROM work_order_control woc,
          lgeku_project_upload U
    WHERE U.interface_batch IS NOT NULL
      AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
      AND U.WORK_ORDER_GRP_ID = 1
      AND U.company_id = woc.company_id
      AND U.work_order_number = woc.work_order_number
      AND NVL(U.status, 0) <> -1
      AND woc.funding_wo_indicator = 1
      AND NOT EXISTS
                  (SELECT 1
                     FROM work_order_class_code wcc
                    WHERE     wcc.class_code_id = 57
                          AND wcc.work_order_id = woc.work_order_id)