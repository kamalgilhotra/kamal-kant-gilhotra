SELECT DECODE (COUNT (*), 0, 0, 1)
  FROM WORK_ORDER_CONTROL WOC, WORK_ORDER_ACCOUNT WOA
 WHERE     WOC.FUNDING_WO_INDICATOR = 1
       AND WOC.WORK_ORDER_ID = <arg1>
       AND WOC.WORK_ORDER_ID = WOA.WORK_ORDER_ID
       AND WOA.WORK_ORDER_GRP_ID = 1
       AND NOT EXISTS
                  (SELECT 1
                     FROM WORK_ORDER_CLASS_CODE WOCC
                    WHERE     WOC.WORK_ORDER_ID = WOCC.WORK_ORDER_ID
                          AND WOCC.CLASS_CODE_ID = 46)

-- Final Query
UPDATE lgeku_project_upload U
   SET error_message = error_message || ' A Plant Account must be selected for a capital funding project.  Please select a plant account from the class code tab.; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND NOT EXISTS (SELECT 1
                     FROM lgeku_project_upload_cc_values cc
                    WHERE cc.class_code_id = 46
                      AND cc.work_order_number = U.work_order_number
                      AND cc.company_id = U.company_id)
   AND (action = 'I'
        OR
        (
             action = 'U'
         AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 46 AND wcc.work_order_id = U.work_order_id)
        )
       )