CREATE OR REPLACE FUNCTION PWRPLANT.lge_ku_chk_class_codes_mu (
   A_WO               WORK_ORDER_CONTROL.WORK_ORDER_NUMBER%TYPE,
   A_COMPANY_ID       WORK_ORDER_CONTROL.COMPANY_ID%TYPE,
   A_CLASS_CODE_ID    WORK_ORDER_CLASS_CODE.CLASS_CODE_ID%TYPE,
   A_QUERY_TYPE       VARCHAR2)
   RETURN VARCHAR2
AS
   THE_VALUE   WORK_ORDER_CLASS_CODE.VALUE%TYPE;
BEGIN
   WITH X
        AS (SELECT 1 SQ_NO,
                   CLASS_VALUE1 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE1 = (SELECT DESCRIPTION
                                    FROM CLASS_CODE
                                   WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 2 SQ_NO,
                   CLASS_VALUE2 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE2 = (SELECT DESCRIPTION
                                    FROM CLASS_CODE
                                   WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 3 SQ_NO,
                   CLASS_VALUE3 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE3 = (SELECT DESCRIPTION
                                    FROM CLASS_CODE
                                   WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 4 SQ_NO,
                   CLASS_VALUE4 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE4 = (SELECT DESCRIPTION
                                    FROM CLASS_CODE
                                   WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 5 SQ_NO,
                   CLASS_VALUE5 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE5 = (SELECT DESCRIPTION
                                    FROM CLASS_CODE
                                   WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 6 SQ_NO,
                   CLASS_VALUE6 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE6 = (SELECT DESCRIPTION
                                    FROM CLASS_CODE
                                   WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 7 SQ_NO,
                   CLASS_VALUE7 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE7 = (SELECT DESCRIPTION
                                    FROM CLASS_CODE
                                   WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 8 SQ_NO,
                   CLASS_VALUE8 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE8 = (SELECT DESCRIPTION
                                    FROM CLASS_CODE
                                   WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 9 SQ_NO,
                   CLASS_VALUE9 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE9 = (SELECT DESCRIPTION
                                    FROM CLASS_CODE
                                   WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 10 SQ_NO,
                   CLASS_VALUE10 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE10 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 11 SQ_NO,
                   CLASS_VALUE11 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE11 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 12 SQ_NO,
                   CLASS_VALUE12 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE12 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 13 SQ_NO,
                   CLASS_VALUE13 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE13 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 14 SQ_NO,
                   CLASS_VALUE14 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE14 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 15 SQ_NO,
                   CLASS_VALUE15 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE15 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 16 SQ_NO,
                   CLASS_VALUE16 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE16 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 17 SQ_NO,
                   CLASS_VALUE17 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE17 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 18 SQ_NO,
                   CLASS_VALUE18 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE18 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 19 SQ_NO,
                   CLASS_VALUE19 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE19 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 20 SQ_NO,
                   CLASS_VALUE20 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE20 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 21 SQ_NO,
                   CLASS_VALUE21 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE21 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 22 SQ_NO,
                   CLASS_VALUE22 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE22 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 23 SQ_NO,
                   CLASS_VALUE23 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE23 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 24 SQ_NO,
                   CLASS_VALUE24 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE24 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 25 SQ_NO,
                   CLASS_VALUE25 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE25 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 26 SQ_NO,
                   CLASS_VALUE26 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE26 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 27 SQ_NO,
                   CLASS_VALUE27 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE27 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 28 SQ_NO,
                   CLASS_VALUE28 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE28 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 29 SQ_NO,
                   CLASS_VALUE29 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE29 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID)
            UNION
            SELECT 30 SQ_NO,
                   CLASS_VALUE30 CLASS_CODE,
                   WORK_ORDER_NUMBER,
                   COMPANY_ID
              FROM LGEKU_PROJECT_UPLOAD
             WHERE CLASS_CODE30 = (SELECT DESCRIPTION
                                     FROM CLASS_CODE
                                    WHERE CLASS_CODE_ID = A_CLASS_CODE_ID))
   SELECT CLASS_CODE
     INTO THE_VALUE
     FROM X
    WHERE A_QUERY_TYPE = 'NOT_NULL_CC_VALUE'
          AND WORK_ORDER_NUMBER = A_WO
          AND COMPANY_ID = A_COMPANY_ID
          AND SQ_NO = (SELECT MIN (SQ_NO) FROM X WHERE WORK_ORDER_NUMBER = A_WO AND COMPANY_ID = A_COMPANY_ID);

   IF A_QUERY_TYPE = 'NOT_NULL_CC_VALUE' THEN
      IF THE_VALUE IS NULL THEN
         THE_VALUE := 'NULL';
      ELSE
         THE_VALUE := 'NOT NULL';
      END IF;
   END IF;
   
   RETURN THE_VALUE;
EXCEPTION
   WHEN NO_DATA_FOUND THEN
      RETURN 'DOES NOT EXIST';
   WHEN OTHERS THEN
      RETURN 'NULL';
END;
/



-- drop function lge_ku_chk_class_codes_mu



/* DV 18
SELECT * FROM work_order_group

SELECT * FROM work_order_type

SELECT * FROM work_order_account

SELECT work_order_type_id, w.* FROM work_order_control w

SELECT * FROM cols WHERE column_name LIKE '%WORK_ORDER_GRP_ID%'
ORDER BY 1

SELECT * FROM WO_GRP_WO_TYPE WHERE WORK_ORDER_TYPE_ID = 1029
*/