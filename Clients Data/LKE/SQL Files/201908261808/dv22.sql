SELECT DECODE (COUNT (*), 0, 0, 1)
  FROM WORK_ORDER_CONTROL WOC
 WHERE     WOC.FUNDING_WO_INDICATOR = 1
       AND WOC.WORK_ORDER_ID = <arg1>
       AND NOT EXISTS
                  (SELECT 1
                     FROM WORK_ORDER_CLASS_CODE WOCC
                    WHERE     WOC.WORK_ORDER_ID = WOCC.WORK_ORDER_ID
                          AND WOCC.CLASS_CODE_ID = 52);

-- Final Query
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'The Category Class Code must be populated in order for a Funding Project to be sent to Oracle; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND NOT EXISTS (SELECT 1
                     FROM lgeku_project_upload_cc_values cc
                    WHERE cc.class_code_id = 52
                      AND cc.work_order_number = U.work_order_number
                      AND cc.company_id = U.company_id)
   AND (action = 'I'
        OR
        (
             action = 'U'
         AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 52 AND wcc.work_order_id = U.work_order_id)
        )
       )