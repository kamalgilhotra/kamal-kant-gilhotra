SELECT DECODE(COUNT(*), 1,1,0)
FROM
(
SELECT wocc.work_order_id
FROM work_order_control woc, work_order_class_code wocc
WHERE wocc.work_order_id = woc.work_order_id AND woc.funding_wo_indicator = 1
AND woc.work_order_id = <arg1> AND wocc.class_code_id = 70
AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 70) IS NOT NULL
AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 71) IS NULL
)

'There is an Allocation Rule defined, but no Target Projects have been entered.'

SELECT wocc.work_order_id
FROM work_order_control woc, work_order_class_code wocc
WHERE wocc.work_order_id = woc.work_order_id AND woc.funding_wo_indicator = 1
AND woc.work_order_number = U.work_order_number
AND woc.company_id = U.company_id
AND wocc.class_code_id = 70
AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 70) IS NOT NULL
AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 71) IS NULL

SELECT 1
FROM work_order_control woc, work_order_account woa
WHERE woc.funding_wo_indicator = 1
AND woc.work_order_id = <arg1>
AND woc.worK_order_id = woa.work_order_id
AND woa.work_order_grp_id = 1
AND NOT EXISTS (SELECT 1
FROM work_order_class_code wocc
WHERE woc.work_order_id = wocc.work_order_id
AND wocc.class_code_id = 46)

SELECT asset_location_id, w.* FROM work_order_control w WHERE work_order_number = '124561'

SELECT work_order_grp_id FROM work_order_account WHERE work_order_id = 11942310

SELECT asset_location_id. w.* FROM work_order_control w WHERE work_order_id = 11978483

SELECT interface_batch, funding_wo_indicator, budget_number, EXT_DEPARTMENT, action, status, dbms_lob.substr(error_message, 500, 1) err_msg, U.*
FROM lgeku_project_upload U WHERE work_order_number = '124301'

SELECT interface_batch, funding_wo_indicator, budget_number, EXT_DEPARTMENT, action, status, dbms_lob.substr(error_message, 1000, 1) err_msg, U.*
FROM lgeku_project_upload U

INSERT INTO lgeku_project_upload (company_id, work_order_number, ext_work_order_type, ext_major_location, description, ext_department, est_start_date,
est_complete_date, est_in_service_date, interface_batch)
SELECT company_id, work_order_number, (SELECT description FROM work_order_type x WHERE work_order_type_id = z.work_order_type_id) ext_work_order_type,
       (SELECT description FROM major_location x WHERE major_location_id = z.major_location_id) ext_major_location,
       'testing update' description,
       (SELECT description FROM department x WHERE department_id = z.department_id) ext_department,
       SYSDATE est_start_date, SYSDATE est_complete_date, SYSDATE est_in_service_date, 37 interface_batch
FROM  work_order_control z WHERE work_order_id = 182401249

COMMIT

ROLLBACK

UPDATE lgeku_project_upload U
SET error_message = error_message || 'There is an Allocation Rule defined, but no Target Projects have been entered.; ', status = -1
WHERE U.interface_batch IS NOT NULL
AND (
--        (EXISTS(SELECT 1
--        FROM work_order_control woc, work_order_class_code wocc
--        WHERE wocc.work_order_id = woc.work_order_id AND woc.funding_wo_indicator = 1
--        AND woc.work_order_number = U.work_order_number
--        AND woc.company_id = U.company_id
--        AND wocc.class_code_id = 70 AND wocc.VALUE IS NOT NULL
--        AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 71) IS NULL)
--        AND lge_ku_chk_class_codes_mu(U.work_order_number, U.company_id, 70, 'NOT_NULL_CC_VALUE') IS NULL
--        AND lge_ku_chk_class_codes_mu(U.work_order_number, U.company_id, 71, 'NOT_NULL_CC_VALUE') IS NULL)
--     OR
--        (EXISTS(SELECT 1
--        FROM work_order_control woc, work_order_class_code wocc
--        WHERE wocc.work_order_id = woc.work_order_id AND woc.funding_wo_indicator = 1
--        AND woc.work_order_number = U.work_order_number
--        AND woc.company_id = U.company_id
--        AND wocc.class_code_id = 70 AND wocc.VALUE IS NOT NULL
--        AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 71) IS NULL)
--        AND lge_ku_chk_class_codes_mu(U.work_order_number, U.company_id, 70, 'NOT_NULL_CC_VALUE') IS NOT NULL
--        AND lge_ku_chk_class_codes_mu(U.work_order_number, U.company_id, 71, 'NOT_NULL_CC_VALUE') IS NULL)
--     OR
        (lge_ku_chk_class_codes_mu(U.work_order_number, U.company_id, 70, 'NOT_NULL_CC_VALUE') = 'NOT NULL'
        AND lge_ku_chk_class_codes_mu(U.work_order_number, U.company_id, 71, 'NOT_NULL_CC_VALUE') IN('DOES NOT EXIST', 'NULL'))
    )
    
    
SELECT COUNT(*)
FROM lgeku_project_upload U
WHERE work_order_number = 'SWITCH340'
AND lge_ku_chk_class_codes_mu(U.work_order_number, U.company_id, 70, 'NOT_NULL_CC_VALUE') = 'NOT NULL'
AND lge_ku_chk_class_codes_mu(U.work_order_number, U.company_id, 71, 'NOT_NULL_CC_VALUE') IN('DOES NOT EXIST', 'NULL')

edit lgeku_project_upload
WHERE work_order_number = 'SWITCH340' -- RETENLGE|RETENKU

COMMIT

UPDATE lgeku_project_upload U
SET error_message = error_message || 'There is an Allocation Rule defined, but no Target Projects have been entered.; ', status = -1
WHERE work_order_number = 'SWITCH340'
AND lge_ku_chk_class_codes_mu(U.work_order_number, U.company_id, 70, 'NOT_NULL_CC_VALUE') = 'NOT NULL'
AND lge_ku_chk_class_codes_mu(U.work_order_number, U.company_id, 71, 'NOT_NULL_CC_VALUE') IN('DOES NOT EXIST', 'NULL')

UPDATE lgeku_project_upload z
set error_message = error_message || 'There is an Allocation Rule defined, but no Target Projects have been entered.; ', status = -1
WHERE EXISTS
(
AND U.work_order_number = z.work_order_number
AND U.company_id = z.company_id)


UPDATE lgeku_project_upload U
set error_message = error_message || 'There is an Allocation Rule defined, but no Target Projects have been entered.; ', status = -1
WHERE U.interface_batch IS NOT NULL
AND lge_ku_chk_class_codes_mu(U.work_order_number, U.company_id, 70, 'NOT_NULL_CC_VALUE') = 'NOT NULL'
AND lge_ku_chk_class_codes_mu(U.work_order_number, U.company_id, 71, 'NOT_NULL_CC_VALUE') IN('DOES NOT EXIST', 'NULL')

UPDATE lgeku_project_upload U
SET error_message = error_message || 'There is an Allocation Rule defined, but no Target Projects have been entered.; ', status = -1
WHERE U.interface_batch IS NOT NULL
AND (
     EXISTS(
         SELECT 1 FROM lgeku_project_upload_cc_values cc70, lgeku_project_upload_cc_values cc71
         WHERE  U.work_order_number = cc70.work_order_number
           AND  U.work_order_number = cc71.work_order_number
           AND  U.company_id = cc70.company_id
           AND  U.company_id = cc71.company_id
           AND  cc70.class_code_id = 70
           AND  cc70.class_code_value IS NOT NULL
           AND  cc71.class_code_id = 71
           AND  cc71.class_code_value IS NULL)
    )
    
edit lgeku_project_upload WHERE work_order_number = 'SWITCH340' --LKE Allocation Target

SELECT class_code_value cc70_value,
       (SELECT class_code_value cc71_value FROM lgeku_project_upload_cc_values cc71 WHERE U.work_order_number = cc71.work_order_number AND U.company_id = cc71.company_id AND class_code_id = 71) cc71_value,
       cc70.company_id, cc70.work_order_number
FROM   lgeku_project_upload_cc_values cc70, lgeku_project_upload U
WHERE  U.work_order_number = cc70.work_order_number AND U.company_id = cc70.company_id



-- Final Query
UPDATE lgeku_project_upload U
SET    error_message = error_message || 'There is an Allocation Rule defined, but no Target Projects have been entered.; ', status = -1
WHERE  U.interface_batch IS NOT NULL
  AND  U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
  AND  (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) IS NOT NULL
  AND  (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id AND wocc.class_code_id = 71) IS NULL