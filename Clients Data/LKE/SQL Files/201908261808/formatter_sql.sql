UPDATE LGEKU_PROJECT_UPLOAD U
   SET ERROR_MESSAGE = ERROR_MESSAGE || 'Please ensure that Product, Description, and Detailed Description fields are all populated before updating a capital project; ', STATUS = -1
 WHERE     U.INTERFACE_BATCH IS NOT NULL
       AND (   EXISTS
                  (SELECT 1
                     FROM WORK_ORDER_CONTROL WOC, WORK_ORDER_ACCOUNT WOA
                    WHERE     WOC.WORK_ORDER_NUMBER = U.WORK_ORDER_NUMBER
                          AND WOC.COMPANY_ID = U.COMPANY_ID
                          AND WOC.WORK_ORDER_ID = WOA.WORK_ORDER_ID
                          AND WOA.WORK_ORDER_GRP_ID = 1
                          AND WOC.FUNDING_WO_INDICATOR = 1
                          AND (   (    WOC.LONG_DESCRIPTION IS NULL
                                   AND U.LONG_DESCRIPTION IS NULL)
                               OR (    WOC.REASON_CD_ID IS NULL
                                   AND U.REASON_CD_ID IS NULL)
                               OR (WOC.NOTES IS NULL AND U.NOTES IS NULL)))
            OR (    NOT EXISTS
                           (SELECT 1
                              FROM WORK_ORDER_CONTROL
                             WHERE     WORK_ORDER_NUMBER =
                                          U.WORK_ORDER_NUMBER
                                   AND COMPANY_ID = U.COMPANY_ID)
                AND (   U.LONG_DESCRIPTION IS NULL
                     OR U.REASON_CD_ID IS NULL
                     OR U.NOTES IS NULL))
           )