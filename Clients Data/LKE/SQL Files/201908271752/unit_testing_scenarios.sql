SELECT notes, reason_cd_id, long_description, woc.*, woa.* FROM work_order_control woc, work_order_account woa
WHERE woc.work_order_id = woa.work_order_id
AND   woc.funding_wo_indicator = 1
AND woa.work_order_grp_id = 1
AND (notes IS NULL OR reason_cd_id IS NULL OR long_description IS NULL)

11978480 -- None null
414529909 -- All null
407115103 -- long description null
414523048 -- notes null
18386202 -- reason_cd_id null
13637900 -- notes and reason_cd_id null
11884833 -- notes and long desription null
18129385 -- reason_cd_id and long desription null

UPDATE work_order_control set long_description = NULL WHERE work_order_id = 11884833 --Points of Interest Earlington
UPDATE work_order_control set long_description = NULL WHERE work_order_id = 18129385 --LGE BACKYARD MACHINE


INSERT INTO lgeku_project_upload (company_id, work_order_number, ext_work_order_type, ext_major_location, description, ext_department, est_start_date,
est_complete_date, est_in_service_date, notes, reason_cd_id, long_description)
SELECT company_id, work_order_number, (SELECT description FROM work_order_type x WHERE work_order_type_id = z.work_order_type_id) ext_work_order_type,
       (SELECT description FROM major_location x WHERE major_location_id = z.major_location_id) ext_major_location,
       SUBSTR(TO_CHAR(SYSDATE, 'yyyymmddhhmmss - ') || description, 1, LENGTH(description) - 17) description,
       (SELECT description FROM department x WHERE department_id = z.department_id) ext_department,
       SYSDATE est_start_date, SYSDATE est_complete_date, SYSDATE est_in_service_date, notes, reason_cd_id, long_description
FROM  work_order_control z WHERE work_order_id IN(11978480, 414529909, 407115103, 414523048, 18386202, 13637900, 11884833, 18129385)
UNION
SELECT company_id, SUBSTR('K' || work_order_number, 1, 9) work_order_number,
       (SELECT description FROM work_order_type x WHERE work_order_type_id = z.work_order_type_id) ext_work_order_type,
       (SELECT description FROM major_location x WHERE major_location_id = z.major_location_id) ext_major_location,
       SUBSTR(TO_CHAR(SYSDATE, 'yyyymmddhhmmss - ') || description, 1, LENGTH(description) - 17) description,
       (SELECT description FROM department x WHERE department_id = z.department_id) ext_department,
       SYSDATE est_start_date, SYSDATE est_complete_date, SYSDATE est_in_service_date, notes, reason_cd_id, long_description
FROM  work_order_control z WHERE work_order_id IN(11978480, 414529909, 407115103, 414523048, 18386202, 13637900, 11884833, 18129385)

COMMIT