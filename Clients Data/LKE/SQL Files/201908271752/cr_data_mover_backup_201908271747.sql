SET DEFINE OFF;
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Post Processing', 100, 1, 'UPDATE work_order_initiator woi
   SET engineer = DECODE (engineer, ''null'', NULL, engineer),
       plant_accountant = DECODE (plant_accountant, ''null'', NULL, plant_accountant),
       proj_mgr = DECODE (proj_mgr, ''null'', NULL, proj_mgr),
       contract_adm = DECODE (contract_adm, ''null'', NULL, contract_adm),
       other = DECODE (other, ''null'', NULL, other)
 WHERE EXISTS
          (SELECT 1
             FROM lgeku_project_upload U, work_order_control woc
            WHERE     U.work_order_number = woc.work_order_number
                  AND U.company_id = woc.company_id
                  AND woc.work_order_id = woi.work_order_id
                  AND U.interface_batch IS NOT NULL
                  AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload))', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 16:46:24', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Null Work Order Initiator Values');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Post Processing', 110, 1, 'delete from work_order_class_code wocc  where exists (select 1 from lgeku_project_upload p, work_order_control woc, class_code c   where woc.work_Order_number = p.work_order_number and woc.work_order_id= wocc.work_order_id    and c.class_code_id = wocc.class_code_id   and (   (p.class_value1 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code1)))   or  (p.class_value2 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code2)))   or  (p.class_value3 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code3)))    or  (p.class_value4 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code4)))     or  (p.class_value5 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code5)))      or  (p.class_value6 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code6)))       or  (p.class_value7 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code7)))        or  (p.class_value8 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code8)))         or  (p.class_value9 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code9)))          or  (p.class_value10 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code10)))           or  (p.class_value11 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code11)))            or  (p.class_value12 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code12)))             or  (p.class_value13 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code13)))              or  (p.class_value14 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code14)))               or  (p.class_value15 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code15)))                or  (p.class_value16 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code16)))                 or  (p.class_value17= ''null''and lower(trim(c.description)) = lower(trim(p.class_code17)))                  or  (p.class_value18 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code18)))                   or  (p.class_value19 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code19)))                    or  (p.class_value20 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code20)))                     or  (p.class_value21 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code21)))                      or  (p.class_value22 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code22)))                       or  (p.class_value23 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code23)))                        or  (p.class_value24 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code24)))                         or  (p.class_value25 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code25)))                          or  (p.class_value26 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code26)))                           or  (p.class_value27 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code27)))                            or  (p.class_value28 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code28)))                             or  (p.class_value29 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code29)))                              or  (p.class_value30 = ''null''and lower(trim(c.description)) = lower(trim(p.class_code30)))   ))', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 09:29:25', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Null Class Code Values');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Post Processing', 120, 1, 'INSERT INTO work_order_class_code (class_code_id, work_order_id, VALUE)
   SELECT 57, woc.work_order_id, ''NA''
     FROM work_order_control woc,
          lgeku_project_upload U
    WHERE U.interface_batch IS NOT NULL
      AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
      AND U.WORK_ORDER_GRP_ID = 1
      AND U.company_id = woc.company_id
      AND U.work_order_number = woc.work_order_number
      AND NVL(U.status, 0) <> -1
      AND woc.funding_wo_indicator = 1
      AND NOT EXISTS
                  (SELECT 1
                     FROM work_order_class_code wcc
                    WHERE     wcc.class_code_id = 57
                          AND wcc.work_order_id = woc.work_order_id)', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 09:29:25', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV44 FP Inverst Appr Dflt-WO Update [34]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Post Processing', 130, 1, 'INSERT INTO work_order_class_code (class_code_id, work_order_id, VALUE)
   SELECT 56, woc.work_order_id, ''NA''
     FROM work_order_control woc,
          lgeku_project_upload U
    WHERE U.interface_batch IS NOT NULL
      AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
      AND U.WORK_ORDER_GRP_ID = 1
      AND U.company_id = woc.company_id
      AND U.work_order_number = woc.work_order_number
      AND NVL(U.status, 0) <> -1
      AND woc.funding_wo_indicator = 1
      AND NOT EXISTS
                  (SELECT 1
                     FROM work_order_class_code wcc
                    WHERE     wcc.class_code_id = 56
                          AND wcc.work_order_id = woc.work_order_id)', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 16:46:24', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV45 FP RAC Appr Dflt-WO Update [33]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Post Processing', 140, 1, 'UPDATE work_order_control woc
   SET woc.long_description = woc.work_order_number || ''-'' || SUBSTR (woc.long_description, 1, 1990)
 WHERE SUBSTR (woc.long_description, 1, LENGTH(woc.work_order_number)) <> woc.work_order_number
   AND woc.funding_wo_indicator = 1
   AND (woc.work_order_number, woc.company_id) IN(SELECT work_order_number, company_id
                                                    FROM lgeku_project_upload U
                                                   WHERE U.interface_batch IS NOT NULL
                                                     AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
                                                     AND NVL(U.status, 0) <> -1)', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 16:46:01', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV53 FP Description-WO Update [41]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Post Processing', 150, 1, 'UPDATE work_order_account
   SET wo_est_hierarchy_id = 1
 WHERE NVL (wo_est_hierarchy_id, 0) NOT IN (SELECT wo_est_hierarchy_id FROM wo_est_hierarchy)
   AND 1 = (SELECT COUNT (*) FROM wo_est_hierarchy)
   AND work_order_id IN(SELECT woc.work_order_id
                          FROM work_order_control woc,
                               lgeku_project_upload U
                         WHERE woc.funding_wo_indicator = 1
                           AND U.interface_batch IS NOT NULL
                           AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
                           AND NVL(U.status, 0) <> -1
                           AND woc.work_order_number = U.work_order_number
                           AND woc.company_id = U.company_id)', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 16:45:49', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV54 FP/WO Est Hierarchy Update [48]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Post Processing', 160, 1, 'UPDATE work_order_approval A
   SET A.approval_status_id = 6
 WHERE EXISTS (SELECT 1
                 FROM workflow w
                WHERE w.id_field1 = A.work_order_id
                  AND w.subsystem = ''fp_approval''
                  AND w.approval_status_id = 6
                  AND w.id_field1 = A.work_order_id)
   AND A.approval_status_id = 1
   AND A.approval_type_id = 99
   AND A.work_order_id IN(SELECT woc.work_order_id
                            FROM work_order_control woc,
                                 lgeku_project_upload U
                           WHERE woc.funding_wo_indicator = 1
                             AND U.interface_batch IS NOT NULL
                             AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
                             AND NVL(U.status, 0) <> -1
                             AND woc.work_order_number = U.work_order_number
                             AND woc.company_id = U.company_id)', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 16:45:49', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV55 Auto approve indirect projects - part1 [110]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Post Processing', 170, 1, 'UPDATE work_order_control c
   SET c.wo_status_id = 2
 WHERE EXISTS (SELECT 1
                 FROM workflow w
                WHERE w.id_field1 = c.work_order_id
                  AND w.subsystem = ''fp_approval''
                  AND w.approval_status_id = 6
                  AND w.id_field1 = C.work_order_id)
   AND EXISTS (SELECT 1
                 FROM work_order_approval A
                WHERE A.work_order_id = c.work_order_id
                  AND A.approval_type_id = 99
                  AND A.approval_status_id = 6)
   AND c.funding_wo_indicator = 1
   AND c.wo_status_id = 1
   AND (c.work_order_number, c.company_id) IN(SELECT U.work_order_number, U.company_id
                                                FROM lgeku_project_upload U
                                               WHERE U.interface_batch IS NOT NULL
                                                 AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
                                                 AND NVL(U.status, 0) <> -1)', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 16:45:49', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV55 Auto approve indirect projects - part2 [110]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Post Processing', 200, 1, 'insert into LGEKU_PROJECT_UPLOAD_ARC (  WORK_ORDER_ID,COMPANY_ID,BUS_SEGMENT_ID,BUDGET_ID,BUDGET_VERSION_ID,WORK_ORDER_NUMBER,REASON_CD_ID,  WORK_ORDER_TYPE_ID,FUNDING_WO_ID,FUNDING_PROJ_NUMBER,MAJOR_LOCATION_ID,ASSET_LOCATION_ID,DESCRIPTION,LONG_DESCRIPTION,  FUNDING_WO_INDICATOR,EST_START_DATE,EST_COMPLETE_DATE,NOTES,DEPARTMENT_ID,EST_IN_SERVICE_DATE,WORK_ORDER_GRP_ID,  STATUS,ACTION,NEW_BUDGET,IN_SERVICE_DATE,COMPLETION_DATE,INITIATION_DATE,ROW_ID,CLOSE_DATE,FUTURE_PROJECTS,ALTERNATIVES,  FINANCIAL_ANALYSIS,REASON_FOR_WORK,BACKGROUND,ENGINEER,PLANT_ACCOUNTANT,PROJECT_MANAGER,CONTRACT_ADMIN,OTHER_CONTACT,  BASE_YEAR,AFUDC_STOP_DATE,AFUDC_START_DATE,EST_ANNUAL_REV,EXTERNAL_WO_NUMBER,OUT_OF_SERVICE_DATE,SUSPENDED_DATE,WO_APPROVAL_GROUP_ID,  INITIATOR,WO_STATUS_ID,AGREEMENT_ID,CLASS_CODE1,CLASS_VALUE1,CLASS_CODE2,CLASS_VALUE2,CLASS_CODE3,CLASS_VALUE3,CLASS_CODE4,  CLASS_VALUE4,CLASS_CODE5,CLASS_VALUE5,CLASS_CODE6,CLASS_VALUE6,CLASS_CODE7,CLASS_VALUE7,CLASS_CODE8,CLASS_VALUE8,CLASS_CODE9,  CLASS_VALUE9,CLASS_CODE10,CLASS_VALUE10,CLASS_CODE11,CLASS_VALUE11,CLASS_CODE12,CLASS_VALUE12,CLASS_CODE13,CLASS_VALUE13,  CLASS_CODE14,CLASS_VALUE14,CLASS_CODE15,CLASS_VALUE15,CLASS_CODE16,CLASS_VALUE16,CLASS_CODE17,CLASS_VALUE17,CLASS_CODE18,  CLASS_VALUE18,CLASS_CODE19,CLASS_VALUE19,CLASS_CODE20,CLASS_VALUE20,CLASS_CODE21,CLASS_VALUE21,CLASS_CODE22,CLASS_VALUE22,  CLASS_CODE23,CLASS_VALUE23,CLASS_CODE24,CLASS_VALUE24,CLASS_CODE25,CLASS_VALUE25,CLASS_CODE26,CLASS_VALUE26,CLASS_CODE27,  CLASS_VALUE27,CLASS_CODE28,CLASS_VALUE28,CLASS_CODE29,CLASS_VALUE29,CLASS_CODE30,CLASS_VALUE30,REIMBURSABLE_TYPE_ID,  BATCH_ID,EXT_COMPANY,EXT_WORK_ORDER_TYPE,EXT_MAJOR_LOCATION,BUDGET_NUMBER,BUDGET_COMPANY_ID,EXT_DEPARTMENT,EXT_WORK_ORDER_GROUP,  EXT_ASSET_LOCATION,SEQ_ID,USER_ID,TIME_STAMP,OLD_WO_STATUS_ID,EXT_WORK_ORDER_STATUS,EXT_REASON_CD,EXT_WO_APPROVAL_GROUP,  EXT_AGREEMENT,AUTO_APPROVED,REPAIR_LOCATION_ID,EXT_REPAIR_LOCATION,CO_TENANT_PARENT,INTERFACE_BATCH,RUN_TIMESTAMP,ARCHIVE_TIMESTAMP)  (select WORK_ORDER_ID,COMPANY_ID,BUS_SEGMENT_ID,BUDGET_ID,BUDGET_VERSION_ID,WORK_ORDER_NUMBER,REASON_CD_ID,  WORK_ORDER_TYPE_ID,FUNDING_WO_ID,FUNDING_PROJ_NUMBER,MAJOR_LOCATION_ID,ASSET_LOCATION_ID,DESCRIPTION,LONG_DESCRIPTION,  FUNDING_WO_INDICATOR,EST_START_DATE,EST_COMPLETE_DATE,NOTES,DEPARTMENT_ID,EST_IN_SERVICE_DATE,WORK_ORDER_GRP_ID,  STATUS,ACTION,NEW_BUDGET,IN_SERVICE_DATE,COMPLETION_DATE,INITIATION_DATE,ROW_ID,CLOSE_DATE,FUTURE_PROJECTS,ALTERNATIVES,  FINANCIAL_ANALYSIS,REASON_FOR_WORK,BACKGROUND,ENGINEER,PLANT_ACCOUNTANT,PROJECT_MANAGER,CONTRACT_ADMIN,OTHER_CONTACT,  BASE_YEAR,AFUDC_STOP_DATE,AFUDC_START_DATE,EST_ANNUAL_REV,EXTERNAL_WO_NUMBER,OUT_OF_SERVICE_DATE,SUSPENDED_DATE,WO_APPROVAL_GROUP_ID,  INITIATOR,WO_STATUS_ID,AGREEMENT_ID,CLASS_CODE1,CLASS_VALUE1,CLASS_CODE2,CLASS_VALUE2,CLASS_CODE3,CLASS_VALUE3,CLASS_CODE4,  CLASS_VALUE4,CLASS_CODE5,CLASS_VALUE5,CLASS_CODE6,CLASS_VALUE6,CLASS_CODE7,CLASS_VALUE7,CLASS_CODE8,CLASS_VALUE8,CLASS_CODE9,  CLASS_VALUE9,CLASS_CODE10,CLASS_VALUE10,CLASS_CODE11,CLASS_VALUE11,CLASS_CODE12,CLASS_VALUE12,CLASS_CODE13,CLASS_VALUE13,  CLASS_CODE14,CLASS_VALUE14,CLASS_CODE15,CLASS_VALUE15,CLASS_CODE16,CLASS_VALUE16,CLASS_CODE17,CLASS_VALUE17,CLASS_CODE18,  CLASS_VALUE18,CLASS_CODE19,CLASS_VALUE19,CLASS_CODE20,CLASS_VALUE20,CLASS_CODE21,CLASS_VALUE21,CLASS_CODE22,CLASS_VALUE22,  CLASS_CODE23,CLASS_VALUE23,CLASS_CODE24,CLASS_VALUE24,CLASS_CODE25,CLASS_VALUE25,CLASS_CODE26,CLASS_VALUE26,CLASS_CODE27,  CLASS_VALUE27,CLASS_CODE28,CLASS_VALUE28,CLASS_CODE29,CLASS_VALUE29,CLASS_CODE30,CLASS_VALUE30,REIMBURSABLE_TYPE_ID,  BATCH_ID,EXT_COMPANY,EXT_WORK_ORDER_TYPE,EXT_MAJOR_LOCATION,BUDGET_NUMBER,BUDGET_COMPANY_ID,EXT_DEPARTMENT,EXT_WORK_ORDER_GROUP,  EXT_ASSET_LOCATION,SEQ_ID,USER_ID,TIME_STAMP,OLD_WO_STATUS_ID,EXT_WORK_ORDER_STATUS,EXT_REASON_CD,EXT_WO_APPROVAL_GROUP,  EXT_AGREEMENT,AUTO_APPROVED,REPAIR_LOCATION_ID,EXT_REPAIR_LOCATION,CO_TENANT_PARENT,INTERFACE_BATCH,RUN_TIMESTAMP,sysdate  from lgeku_project_upload) ', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 09:29:25', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Archive Data');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Post Processing', 210, 1, 'delete from lgeku_project_upload p where exists (select 1 from lgeku_project_upload_arc a where a.row_id = p.row_id)
and nvl(status,0) <> -1', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 09:29:26', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Delete Data');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 110, 1, 'update LGEKU_PROJECT_UPLOAD set interface_batch = (select nvl(max(interface_batch), 0)+1 from LGEKU_PROJECT_UPLOAD_ARC),  run_timestamp = sysdate
where interface_batch is null', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:59', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Update Interface Processing Columns');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 120, 1, 'UPDATE lgeku_project_upload U
   SET action = DECODE ((SELECT COUNT (*)
                           FROM work_order_control w
                          WHERE w.funding_wo_indicator = 1
                            AND w.company_id = U.company_id
                            AND w.work_order_number = U.work_order_number), 0, ''I'', ''U''),
       work_order_id = (SELECT work_order_id
                           FROM work_order_control w
                          WHERE w.funding_wo_indicator = 1
                            AND w.company_id = U.company_id
                            AND w.work_order_number = U.work_order_number)
 WHERE interface_batch IS NOT NULL', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:58', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Update Action Code and Work Order ID');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 130, 1, 'UPDATE lgeku_project_upload U
   SET (work_order_type_id, department_id, work_order_grp_id) = 
                        (SELECT work_order_type_id, department_id, work_order_grp_id
                           FROM work_order_control c, work_order_account A
                          WHERE c.funding_wo_indicator = 1
                            AND c.work_order_id = U.work_order_id
                            AND c.work_order_id = A.work_order_id)
 WHERE interface_batch IS NOT NULL
   AND interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND action = ''U''', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:56', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Update existing Work Order fields');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 140, 1, 'UPDATE lgeku_project_upload U
   SET work_order_type_id =
          (SELECT work_order_type_id
             FROM (SELECT work_order_type_id
                     FROM (SELECT 1 seq_no, work_order_type_id
                             FROM work_order_type
                            WHERE external_work_order_type = U.ext_work_order_type
                           UNION ALL
                           SELECT 2, work_order_type_id
                             FROM work_order_type
                            WHERE description = U.ext_work_order_type
                           UNION ALL
                           SELECT 3, work_order_type_id
                             FROM work_order_type
                            WHERE TO_CHAR (work_order_type_id) = U.ext_work_order_type)
                   ORDER BY seq_no)
            WHERE ROWNUM = 1)
 WHERE U.ext_work_order_type IS NOT NULL AND interface_batch IS NOT NULL', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:54', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Update Work Order Type ID field');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 150, 1, 'UPDATE lgeku_project_upload U
   SET department_id =
          (SELECT department_id
             FROM (SELECT department_id
                     FROM (SELECT 1 seq_no, department_id
                             FROM department
                            WHERE external_department_code = U.ext_department
                           UNION ALL
                           SELECT 2, department_id
                             FROM department
                            WHERE description = U.ext_department
                           UNION ALL
                           SELECT 3, department_id
                             FROM department
                            WHERE TO_CHAR (department_id) = U.ext_department)
                   ORDER BY seq_no)
            WHERE ROWNUM = 1)
 WHERE U.ext_department IS NOT NULL AND interface_batch IS NOT NULL', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:52', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Update Department ID field');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 160, 1, 'UPDATE lgeku_project_upload U
   SET work_order_grp_id =
          (SELECT work_order_grp_id
             FROM (SELECT work_order_grp_id
                     FROM (SELECT 1 seq_no, work_order_grp_id
                             FROM work_order_group
                            WHERE description = U.ext_work_order_group
                           UNION ALL
                           SELECT 2, work_order_grp_id
                             FROM work_order_group
                            WHERE TO_CHAR (work_order_grp_id) = U.ext_work_order_group)
                   ORDER BY seq_no)
            WHERE ROWNUM = 1)
 WHERE U.ext_work_order_group IS NOT NULL AND U.interface_batch IS NOT NULL AND interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:51', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Update Work Order Group ID field for all');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 170, 1, 'UPDATE LGEKU_PROJECT_UPLOAD U
   SET WORK_ORDER_GRP_ID = (SELECT WORK_ORDER_GRP_ID
                              FROM WO_GRP_WO_TYPE
                             WHERE WORK_ORDER_TYPE_ID = U.WORK_ORDER_TYPE_ID)
 WHERE     ACTION = ''I''
       AND U.EXT_WORK_ORDER_GROUP IS NULL
       AND INTERFACE_BATCH IS NOT NULL
       AND INTERFACE_BATCH = (SELECT MAX (INTERFACE_BATCH) FROM LGEKU_PROJECT_UPLOAD)', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:40', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Update Work Order Group ID field for Inserts Only');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 180, 1, 'update LGEKU_PROJECT_UPLOAD set funding_wo_indicator = 1 where interface_batch is not null', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:38', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Backfill FUNDING_WO_INDICATOR');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 190, 1, 'DELETE FROM LGEKU_PROJECT_UPLOAD_CC_VALUES', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:37', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Clear previous class codes');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 200, 1, 'INSERT INTO LGEKU_PROJECT_UPLOAD_CC_VALUES (SEQ_NO, CLASS_CODE_ID, CLASS_CODE_VALUE, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH)
SELECT * FROM( 
SELECT 1 SEQ_NO, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE1) CC_ID, CLASS_VALUE1 cc_value, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE1 IS NOT NULL
UNION
SELECT 2, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE2) CC_ID, CLASS_VALUE2, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE2 IS NOT NULL
UNION
SELECT 3, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE3) CC_ID, CLASS_VALUE3, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE3 IS NOT NULL
UNION
SELECT 4, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE4) CC_ID, CLASS_VALUE4, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE4 IS NOT NULL
UNION
SELECT 5, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE5) CC_ID, CLASS_VALUE5, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE5 IS NOT NULL
UNION
SELECT 6, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE6) CC_ID, CLASS_VALUE6, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE6 IS NOT NULL
UNION
SELECT 7, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE7) CC_ID, CLASS_VALUE7, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE7 IS NOT NULL
UNION
SELECT 8, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE8) CC_ID, CLASS_VALUE8, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE8 IS NOT NULL
UNION
SELECT 9, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE9) CC_ID, CLASS_VALUE9, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE9 IS NOT NULL
UNION
SELECT 10, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE10) CC_ID, CLASS_VALUE10, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE10 IS NOT NULL
UNION
SELECT 11, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE11) CC_ID, CLASS_VALUE11, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE11 IS NOT NULL
UNION
SELECT 12, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE12) CC_ID, CLASS_VALUE12, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE12 IS NOT NULL
UNION
SELECT 13, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE13) CC_ID, CLASS_VALUE13, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE13 IS NOT NULL
UNION
SELECT 14, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE14) CC_ID, CLASS_VALUE14, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE14 IS NOT NULL
UNION
SELECT 15, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE15) CC_ID, CLASS_VALUE15, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE15 IS NOT NULL)
WHERE interface_batch IS NOT NULL', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:33', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Load class codes 1 to 15');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 210, 1, 'INSERT INTO LGEKU_PROJECT_UPLOAD_CC_VALUES (SEQ_NO, CLASS_CODE_ID, CLASS_CODE_VALUE, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH)
SELECT * FROM( 
SELECT 16 SEQ_NO, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE16) CC_ID, CLASS_VALUE16, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE16 IS NOT NULL
UNION
SELECT 17, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE17) CC_ID, CLASS_VALUE17, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE17 IS NOT NULL
UNION
SELECT 18, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE18) CC_ID, CLASS_VALUE18, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE18 IS NOT NULL
UNION
SELECT 19, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE19) CC_ID, CLASS_VALUE19, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE19 IS NOT NULL
UNION
SELECT 20, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE20) CC_ID, CLASS_VALUE20, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE20 IS NOT NULL
UNION
SELECT 21, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE21) CC_ID, CLASS_VALUE21, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE21 IS NOT NULL
UNION
SELECT 22, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE22) CC_ID, CLASS_VALUE22, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE22 IS NOT NULL
UNION
SELECT 23, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE23) CC_ID, CLASS_VALUE23, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE23 IS NOT NULL
UNION
SELECT 24, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE24) CC_ID, CLASS_VALUE24, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE24 IS NOT NULL
UNION
SELECT 25, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE25) CC_ID, CLASS_VALUE25, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE25 IS NOT NULL
UNION
SELECT 26, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE26) CC_ID, CLASS_VALUE26, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE26 IS NOT NULL
UNION
SELECT 27, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE27) CC_ID, CLASS_VALUE27, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE27 IS NOT NULL
UNION
SELECT 28, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE28) CC_ID, CLASS_VALUE28, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE28 IS NOT NULL
UNION
SELECT 29, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE29) CC_ID, CLASS_VALUE29, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE29 IS NOT NULL
UNION
SELECT 30, (SELECT CLASS_CODE_ID FROM CLASS_CODE WHERE DESCRIPTION = CLASS_CODE30) CC_ID, CLASS_VALUE30, WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH FROM LGEKU_PROJECT_UPLOAD WHERE CLASS_CODE30 IS NOT NULL)
WHERE interface_batch IS NOT NULL', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:35', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Load class codes 16 to 30');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 220, 1, 'DELETE FROM LGEKU_PROJECT_UPLOAD_cc_values
WHERE (WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH, CLASS_CODE_ID, seq_no) NOT IN(
SELECT WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH, CLASS_CODE_ID, MIN(seq_no) FROM LGEKU_PROJECT_UPLOAD_cc_values GROUP BY WORK_ORDER_NUMBER, COMPANY_ID, INTERFACE_BATCH, CLASS_CODE_ID)', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:24', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Discard duplicate class code entries');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 230, 1, 'update lgeku_project_upload p  set budget_number = (select woc.budget_id from work_order_control woc, budget b where woc.work_order_number = p.work_order_number   and woc.company_id = p.company_id and woc.budget_id = b.budget_id and woc.company_id = b.company_id and funding_wo_indicator = 1)
where interface_batch is not null', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:23', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Update Budget Number');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 240, 1, 'update lgeku_project_upload
set error_message = error_message || ''Enter project estimate dates; '', status = -1
where (est_start_date is null or est_in_service_date is null or est_complete_date is null)
and interface_batch is not null', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:22', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Validate Estimate Dates');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 250, 1, 'update lgeku_project_upload
set est_complete_date = 180+est_start_date,
est_in_service_date = 180+est_start_date
where est_start_date = est_complete_date
and interface_batch is not null', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:21', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Default Estimate Dates ');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 260, 1, 'update lgeku_project_upload
set error_message = error_message || ''FP description greater than 30 characters; '', status = -1
where length(description) > 30
and interface_batch is not null', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:20', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Validate Description Length');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 270, 1, 'update lgeku_project_upload
set error_message = error_message || ''FP name greater than 9 characters; '', status = -1
where length(work_order_number) > 9
and interface_batch is not null', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:19', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Validate Project Length');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 280, 1, 'update lgeku_project_upload
set error_message = error_message || ''FP name not uppercase; '', status = -1
where upper(work_order_number) <> work_Order_number
and interface_batch is not null', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:18', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Validate Project Case');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 290, 1, 'UPDATE lgeku_project_upload U
   SET error_message = error_message || ''The Funding Project Department is not a valid Project Owning Organization (in the HR All Organization Units table in Oracle Projects). Please select  another Department or contact the Budgeting Group.; '', status = -1
 WHERE NOT EXISTS
           (SELECT dp.description
              FROM department dp,
                   division dv,
                   hr_all_organization_units hr,
                   hr.hr_organization_information@orafin hi,
                   pa_segment_value_lookup_sets pvls,
                   pa_segment_value_lookups pvl
             WHERE hi.organization_id = hr.organization_id
               AND hr.attribute3 = pvl.segment_value
               AND pvls.segment_value_lookup_set_id = pvl.segment_value_lookup_set_id
               AND pvls.segment_value_lookup_set_name = ''Proj Org to GL Responsible Ctr''
               AND dp.external_department_code = hr.attribute3
               AND dp.division_id = dv.division_id
               AND hr.TYPE NOT IN (''CORP'', ''INV'', ''SBG'')
               AND hi.org_information1 = ''PA_PROJECT_ORG''
               AND hi.org_information2 = ''Y''
               AND hr.date_to IS NULL
               AND dv.company_id = DECODE (TO_NUMBER (hr.attribute1), 700, 100, 702, 800, 703, 301, 704, 4, 710, 110, 720, 20, TO_NUMBER (hr.attribute1))
               AND dp.description = U.ext_department)
   AND ((action = ''U'' AND U.ext_department IS NOT NULL) OR action = ''I'')
   AND U.interface_batch IS NOT NULL
   and U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:17', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV60 Validate Own Department [38]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 300, 1, 'UPDATE lgeku_project_upload U
   SET error_message = error_message || ''The Organization for this Funding Project is not valid for the Company on the designated Work Order Type.  Please select another Organization.; '', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND EXISTS (SELECT 1
                 FROM work_order_type T, department d, division dv
                WHERE dv.division_id = d.division_id
                  AND work_order_type_id = U.work_order_type_id
                  AND department_id = U.department_id
                  AND dv.company_id <> T.company_id)', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:03', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV62 Validate Work Order Type Company [44]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 310, 1, 'UPDATE LGEKU_PROJECT_UPLOAD U
   SET ERROR_MESSAGE = ERROR_MESSAGE || ''Please ensure that Product, Description, and Detailed Description fields are all populated before updating a capital project; '', STATUS = -1
 WHERE     U.INTERFACE_BATCH IS NOT NULL
       AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
       AND U.WORK_ORDER_GRP_ID = 1
       AND (
            (    U.action = ''U''
             AND EXISTS (SELECT 1
                           FROM WORK_ORDER_CONTROL WOC
                          WHERE WOC.WORK_ORDER_ID = U.WORK_ORDER_ID
                            AND (   (WOC.LONG_DESCRIPTION IS NULL AND U.LONG_DESCRIPTION IS NULL)
                                 OR (WOC.REASON_CD_ID IS NULL AND U.REASON_CD_ID IS NULL)
                                 OR (WOC.NOTES IS NULL AND U.NOTES IS NULL)))
            )
            OR
            (    U.action =''I''
             AND (U.LONG_DESCRIPTION IS NULL OR U.REASON_CD_ID IS NULL OR U.NOTES IS NULL)
            )
           )', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:02', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV6 - Enforce Prod/Long Desc/Notes [86]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 320, 1, 'UPDATE lgeku_project_upload U
   SET error_message = error_message || ''A Plant Account must be selected for a capital funding project.  Please select a plant account from the class code tab.; '', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND NOT EXISTS (SELECT 1
                     FROM lgeku_project_upload_cc_values cc
                    WHERE cc.class_code_id = 46
                      AND cc.work_order_number = U.work_order_number
                      AND cc.company_id = U.company_id)
   AND (action = ''I''
        OR
        (
             action = ''U''
         AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 46 AND wcc.work_order_id = U.work_order_id)
        )
       )', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:01', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV11 - Enforce Plant Account for CAP FP [98]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 330, 1, 'UPDATE lgeku_project_upload U
   SET error_message = error_message || ''Please populate an asset location for this capital funding project; '', status = -1
 WHERE     U.interface_batch IS NOT NULL
       AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
       AND U.work_order_grp_id = 1
       AND U.ext_asset_location IS NULL
       AND (
            U.action = ''I''
            OR
            (
                 U.action = ''U''
             AND EXISTS
                   (SELECT 1
                      FROM work_order_control woc
                     WHERE woc.work_order_id = U.work_order_id
                       AND woc.asset_location_id IS NULL)
            )
           )', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:45:00', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV12 - Enforce Asset Location for Cap FP [87]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 340, 1, 'UPDATE lgeku_project_upload U
SET    error_message = error_message || ''There is an Allocation Rule defined, but no Target Projects have been entered.; '', status = -1
WHERE  U.interface_batch IS NOT NULL
  AND  U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
  AND  (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) IS NOT NULL
  AND  (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id AND wocc.class_code_id = 71) IS NULL', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:58', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV17 - Target Project Req w Rule [58]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 350, 1, 'UPDATE lgeku_project_upload U
   SET error_message = error_message || ''''''CAP'''' Allocation Rules are reserved for Capital Source Projects; '', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 2
   AND EXISTS (SELECT 1
                 FROM lgeku_project_upload_cc_values wocc
                WHERE wocc.class_code_id = 70
                  AND wocc.work_order_number = U.work_order_number
                  AND wocc.company_id = U.company_id
                  AND wocc.class_code_value LIKE ''CAP%'')', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:56', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV18 - IND Alloc Rule for IND Proj [57]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 360, 1, 'select 1 from dual', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:54', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV19 - CAP Alloc Rule for IND Proj [55]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 370, 1, 'UPDATE lgeku_project_upload U
   SET error_message = error_message || ''Allocation Rules on Capital Source Projects have to start with ''''CAP''''.; '', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND EXISTS (SELECT 1
                 FROM lgeku_project_upload_cc_values wocc
                WHERE wocc.class_code_id = 70
                  AND wocc.work_order_number = U.work_order_number
                  AND wocc.company_id = U.company_id
                  AND wocc.class_code_value IS NOT NULL)
   AND EXISTS (SELECT 1
                 FROM lgeku_project_upload_cc_values wocc
                WHERE wocc.class_code_id = 70
                  AND wocc.work_order_number = U.work_order_number
                  AND wocc.company_id = U.company_id
                  AND wocc.class_code_value NOT LIKE ''CAP%'')', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:50', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV20 - CAP Alloc Rule for CAP Proj [54]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 380, 1, 'select 1 from dual', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:48', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV21 - Validate Target Projects [52]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 400, 1, 'UPDATE lgeku_project_upload U
   SET error_message = error_message || ''Warning: A budget coordinator must be selected for a capital funding project.  Please select a budget coordinator from the accounts tab.; '', status = -1
 WHERE U.interface_batch IS NOT NULL AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND (
        (    U.action = ''U''
         AND U.other_contact IS NULL
         AND U.work_order_grp_id = 1
         AND EXISTS (SELECT 1 FROM work_order_initiator i WHERE i.work_order_id = U.work_order_id AND i.other IS NULL)
        )
        OR
        (    action = ''I''
         AND U.other_contact IS NULL
         AND U.work_order_grp_id = 1
        )
       )', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:46', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV27 - Enforce Budget Coord for Cap FP [97]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 410, 1, 'UPDATE lgeku_project_upload U
SET    error_message = error_message || ''There is a Target Project entered, but no valid Allocation Rule has been entered.; '', status = -1
WHERE  U.interface_batch IS NOT NULL AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id AND wocc.class_code_id = 71) IS NOT NULL
   AND (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) IS NULL', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:45', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV28 - Alloc Target no Alloc Rule [59]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 420, 1, 'UPDATE lgeku_project_upload U
SET    error_message = error_message || ''No Target Numbers are allowed when there is an Allocation Rule defined.; '', status = -1
WHERE  U.interface_batch IS NOT NULL AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND (
        (   (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) IS NOT NULL
           AND (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 77) IS NOT NULL)
        OR
        (   (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) IS NOT NULL
           AND U.action = ''U''
           AND EXISTS(SELECT 1
                        FROM work_order_class_code
                       WHERE work_order_id = U.work_order_id
                         AND class_code_id = 77
                         AND VALUE IS NOT NULL))
        OR
        (   (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 77) IS NOT NULL
           AND U.action = ''U''
           AND EXISTS(SELECT 1
                        FROM work_order_class_code
                       WHERE work_order_id = U.work_order_id
                         AND class_code_id = 70
                         AND VALUE IS NOT NULL))
       )', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:44', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV29 - Target Number on Source Proj [60]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 430, 1, 'select ''may not use it'' from dual', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:27', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV32 - Allocation Exception on CAP [63]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 440, 1, 'select ''may not use it'' from dual', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:26', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV33 - No Alloc Excep on Non Source Proj [64]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 470, 1, 'UPDATE lgeku_project_upload U
   SET error_message = error_message || ''The RAC Category value is required for Funding Projects.; '', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND NOT EXISTS (SELECT 1
                     FROM lgeku_project_upload_cc_values cc
                    WHERE cc.class_code_id = 54
                      AND cc.work_order_number = U.work_order_number
                      AND cc.company_id = U.company_id)
   AND (action = ''I''
        OR
        (
             action = ''U''
         AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 54 AND wcc.work_order_id = U.work_order_id)
        )
       )', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:22', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV41 - FP RAC Cat Required-WO Update [14]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 480, 1, 'UPDATE lgeku_project_upload U
   SET error_message = error_message || ''The FP Reporting Group value is required for all Funding Projects.; '', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND NOT EXISTS (SELECT 1
                     FROM lgeku_project_upload_cc_values cc
                    WHERE cc.class_code_id = 55
                      AND cc.work_order_number = U.work_order_number
                      AND cc.company_id = U.company_id)
   AND (action = ''I''
        OR
        (
             action = ''U''
         AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 55 AND wcc.work_order_id = U.work_order_id)
        )
       )', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:21', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV42 - FP RAC Priority Req-WO Update [16]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 490, 1, 'UPDATE lgeku_project_upload U
   SET error_message = error_message || ''A Code of "ECR Approved Plan" is designated for this funding project. Please designate an ECR Plan Number class code value for this funding project.; '', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id
           AND wocc.class_code_id = 53) = ''ECR APPROVED PLAN''
   AND (
        (    action = ''U''
         AND NOT EXISTS(SELECT 1
                          FROM lgeku_project_upload_cc_values wocc
                         WHERE wocc.work_order_number = U.work_order_number
                           AND wocc.company_id = U.company_id
                           AND wocc.class_code_id = 58)
         AND NOT EXISTS(SELECT 1
                          FROM work_order_class_code wocc
                         WHERE wocc.work_order_id = U.work_order_id
                           AND wocc.class_code_id = 58)
        )
        OR
        (    action = ''I''
         AND NOT EXISTS(SELECT 1
                          FROM lgeku_project_upload_cc_values wocc
                         WHERE wocc.work_order_number = U.work_order_number
                           AND wocc.company_id = U.company_id
                           AND wocc.class_code_id = 58)
        )
       )', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:20', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV43 - FP ECR Plan Required-WO Update [18]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 500, 1, 'select ''may not use'' from dual', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:18', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV46 - WO Start Date - WO Update [31]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 520, 1, 'update lgeku_project_upload 
set error_message = ''Warning: Indirect funding project cannot receive an updated department once they are estimated.'', status = -1
where work_Order_number in (
select  distinct woc.work_Order_number
from cr_budget_data_entry c, cr_budget_version b, work_order_control woc, work_order_account woa, lgeku_project_upload l
where b.cr_budget_version_id = c.cr_budget_version_id
and b.current_version = 1
and c.budget_item = (SELECT budget_number FROM budget WHERE budget_id = l.budget_number)
and project_number = l.work_order_number
and c.total <> 0
AND woc.work_order_number = project_number
AND woc.funding_wo_indicator = 1
AND woc.work_order_id = woa.work_order_id
AND woa.work_order_grp_id = 2
and nvl(l.department_id,0) <> (SELECT DISTINCT nvl(department_id,0)
FROM cr_budget_data_entry z, cr_budget_version y, department d
WHERE z.cr_budget_version_id = y.cr_budget_version_id
and d.external_department_code = z.organization
AND y.current_version = 1
AND z.budget_item = (SELECT budget_number FROM budget WHERE budget_id = l.budget_number)
AND project_number = l.work_order_number
AND z.total <> 0))', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:11', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV63 - Lock Department for Indirect FP [88]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 530, 1, 'UPDATE lgeku_project_upload lke_lpu
   SET error_message = error_message || ''The selected ''''Code'''' value is not valid based on the ''''Category'''' selection.  This funding project is not valid to be sent to Oracle, please update the appropriate selection.; '', status = -1
 WHERE lke_lpu.interface_batch IS NOT NULL
   AND lke_lpu.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND (lke_lpu.work_order_number, lke_lpu.company_id)
        IN(
            WITH x AS
            (SELECT work_order_number, company_id,
                   (DECODE ((SELECT COUNT (*)
                               FROM lgeku_project_upload_cc_values lcc
                              WHERE lcc.work_order_number = lpu.work_order_number
                                AND lcc.company_id = lpu.company_id
                                AND lcc.class_code_id = 52),
                            0, (SELECT VALUE
                                  FROM work_order_class_code wocc
                                 WHERE wocc.class_code_id = 52
                                   AND wocc.work_order_id = (SELECT work_order_id
                                                               FROM work_order_control woc
                                                              WHERE lpu.work_order_number = woc.work_order_number
                                                                AND lpu.company_id = woc.company_id)),
                               (SELECT class_code_value
                               FROM lgeku_project_upload_cc_values lcc
                              WHERE lcc.work_order_number = lpu.work_order_number
                                AND lcc.company_id = lpu.company_id
                                AND lcc.class_code_id = 52))) class_value52,
                   (DECODE ((SELECT COUNT (*)
                               FROM lgeku_project_upload_cc_values lcc
                              WHERE lcc.work_order_number = lpu.work_order_number
                                AND lcc.company_id = lpu.company_id
                                AND lcc.class_code_id = 53),
                            0, (SELECT VALUE
                                  FROM work_order_class_code wocc
                                 WHERE wocc.class_code_id = 53
                                   AND wocc.work_order_id = (SELECT work_order_id
                                                               FROM work_order_control woc
                                                              WHERE lpu.work_order_number = woc.work_order_number
                                                                AND lpu.company_id = woc.company_id)),
                               (SELECT class_code_value
                               FROM lgeku_project_upload_cc_values lcc
                              WHERE lcc.work_order_number = lpu.work_order_number
                                AND lcc.company_id = lpu.company_id
                                AND lcc.class_code_id = 53))) class_value53
              FROM lgeku_project_upload lpu
             WHERE lpu.interface_batch IS NOT NULL
               AND lpu.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload))
            SELECT U.work_order_number, U.company_id
              FROM lgeku_project_upload U, x
             WHERE U.interface_batch IS NOT NULL
               AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
               AND U.work_order_number = x.work_order_number
               AND U.company_id = x.company_id
               AND NVL (x.class_value53, 0) NOT IN(SELECT NVL (E.class_code, 0)
                                                     FROM eon_fp_type_class_cat_code E
                                                    WHERE E.class_category = x.class_value52))', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:09', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV9 - Enforce valid Code vs. Category [95]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 540, 1, 'UPDATE lgeku_project_upload U
   SET error_message = error_message || ''The Category Class Code must be populated in order for a Funding Project to be sent to Oracle; '', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND NOT EXISTS (SELECT 1
                     FROM lgeku_project_upload_cc_values cc
                    WHERE cc.class_code_id = 52
                      AND cc.work_order_number = U.work_order_number
                      AND cc.company_id = U.company_id)
   AND (action = ''I''
        OR
        (
             action = ''U''
         AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 52 AND wcc.work_order_id = U.work_order_id)
        )
       )', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:06', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV22 - Enforce Category CC [93]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 550, 1, 'UPDATE lgeku_project_upload U
   SET error_message = error_message || ''The Code Class Code must be populated in order for a Funding Project to be sent to Oracle; '', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND NOT EXISTS (SELECT 1
                     FROM lgeku_project_upload_cc_values cc
                    WHERE cc.class_code_id = 53
                      AND cc.work_order_number = U.work_order_number
                      AND cc.company_id = U.company_id)
   AND (action = ''I''
        OR
        (
             action = ''U''
         AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 53 AND wcc.work_order_id = U.work_order_id)
        )
       )', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:44:05', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'DV24 - Enforce ''Code'' CC [94]');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 600, 1, 'delete from wo_interface_staging w where exists (select 1 from LGEKU_PROJECT_UPLOAD l where l.company_id = w.company_id and l.work_order_number = w.work_order_number and interface_batch is not null)', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:43:34', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Delete Previously Staged Records');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 610, 1, 'insert into WO_INTERFACE_STAGING (  WORK_ORDER_ID,COMPANY_ID,BUS_SEGMENT_ID,BUDGET_ID,BUDGET_VERSION_ID,WORK_ORDER_NUMBER,REASON_CD_ID,WORK_ORDER_TYPE_ID,FUNDING_WO_ID,  FUNDING_PROJ_NUMBER,MAJOR_LOCATION_ID,ASSET_LOCATION_ID,DESCRIPTION,LONG_DESCRIPTION,FUNDING_WO_INDICATOR,EST_START_DATE,EST_COMPLETE_DATE,NOTES,  DEPARTMENT_ID,EST_IN_SERVICE_DATE,WORK_ORDER_GRP_ID,STATUS,ACTION,NEW_BUDGET,IN_SERVICE_DATE,COMPLETION_DATE,INITIATION_DATE,ROW_ID,CLOSE_DATE,FUTURE_PROJECTS,  ALTERNATIVES,FINANCIAL_ANALYSIS,REASON_FOR_WORK,BACKGROUND,ENGINEER,PLANT_ACCOUNTANT,PROJECT_MANAGER,CONTRACT_ADMIN,OTHER_CONTACT,BASE_YEAR,AFUDC_STOP_DATE,  AFUDC_START_DATE,EST_ANNUAL_REV,EXTERNAL_WO_NUMBER,OUT_OF_SERVICE_DATE,SUSPENDED_DATE,WO_APPROVAL_GROUP_ID,INITIATOR,WO_STATUS_ID,AGREEMENT_ID,  CLASS_CODE1,CLASS_VALUE1,CLASS_CODE2,CLASS_VALUE2,CLASS_CODE3,CLASS_VALUE3,CLASS_CODE4,CLASS_VALUE4,CLASS_CODE5,CLASS_VALUE5,CLASS_CODE6,CLASS_VALUE6,  CLASS_CODE7,CLASS_VALUE7,CLASS_CODE8,CLASS_VALUE8,CLASS_CODE9,CLASS_VALUE9,CLASS_CODE10,CLASS_VALUE10,CLASS_CODE11,CLASS_VALUE11,CLASS_CODE12,  CLASS_VALUE12,CLASS_CODE13,CLASS_VALUE13,CLASS_CODE14,CLASS_VALUE14,CLASS_CODE15,CLASS_VALUE15,CLASS_CODE16,CLASS_VALUE16,CLASS_CODE17,CLASS_VALUE17,  CLASS_CODE18,CLASS_VALUE18,CLASS_CODE19,CLASS_VALUE19,CLASS_CODE20,CLASS_VALUE20,CLASS_CODE21,CLASS_VALUE21,CLASS_CODE22,CLASS_VALUE22,CLASS_CODE23,  CLASS_VALUE23,CLASS_CODE24,CLASS_VALUE24,CLASS_CODE25,CLASS_VALUE25,CLASS_CODE26,CLASS_VALUE26,CLASS_CODE27,CLASS_VALUE27,CLASS_CODE28,CLASS_VALUE28,CLASS_CODE29,  CLASS_VALUE29,CLASS_CODE30,CLASS_VALUE30,REIMBURSABLE_TYPE_ID,BATCH_ID,EXT_COMPANY,EXT_WORK_ORDER_TYPE,EXT_MAJOR_LOCATION,BUDGET_NUMBER,BUDGET_COMPANY_ID,  EXT_DEPARTMENT,EXT_WORK_ORDER_GROUP,EXT_ASSET_LOCATION,SEQ_ID,USER_ID,TIME_STAMP,OLD_WO_STATUS_ID,EXT_WORK_ORDER_STATUS,EXT_REASON_CD,EXT_WO_APPROVAL_GROUP,  EXT_AGREEMENT,AUTO_APPROVED,REPAIR_LOCATION_ID,EXT_REPAIR_LOCATION,CO_TENANT_PARENT)  (select WORK_ORDER_ID,COMPANY_ID,BUS_SEGMENT_ID,BUDGET_ID,BUDGET_VERSION_ID,WORK_ORDER_NUMBER,REASON_CD_ID,WORK_ORDER_TYPE_ID,FUNDING_WO_ID,  FUNDING_PROJ_NUMBER,MAJOR_LOCATION_ID,ASSET_LOCATION_ID,DESCRIPTION,LONG_DESCRIPTION,FUNDING_WO_INDICATOR,EST_START_DATE,EST_COMPLETE_DATE,NOTES,  DEPARTMENT_ID,EST_IN_SERVICE_DATE,WORK_ORDER_GRP_ID,STATUS,ACTION,NEW_BUDGET,IN_SERVICE_DATE,COMPLETION_DATE,INITIATION_DATE,ROW_ID,CLOSE_DATE,FUTURE_PROJECTS,  ALTERNATIVES,FINANCIAL_ANALYSIS,REASON_FOR_WORK,BACKGROUND,ENGINEER,PLANT_ACCOUNTANT,PROJECT_MANAGER,CONTRACT_ADMIN,OTHER_CONTACT,BASE_YEAR,AFUDC_STOP_DATE,  AFUDC_START_DATE,EST_ANNUAL_REV,EXTERNAL_WO_NUMBER,OUT_OF_SERVICE_DATE,SUSPENDED_DATE,WO_APPROVAL_GROUP_ID,INITIATOR,WO_STATUS_ID,AGREEMENT_ID,  CLASS_CODE1, CLASS_VALUE1,CLASS_CODE2, CLASS_VALUE2,CLASS_CODE3,CLASS_VALUE3,CLASS_CODE4,CLASS_VALUE4,CLASS_CODE5,CLASS_VALUE5,CLASS_CODE6,CLASS_VALUE6,  CLASS_CODE7,CLASS_VALUE7,CLASS_CODE8,CLASS_VALUE8,CLASS_CODE9,CLASS_VALUE9,CLASS_CODE10,CLASS_VALUE10,CLASS_CODE11,CLASS_VALUE11,CLASS_CODE12,  CLASS_VALUE12,CLASS_CODE13,CLASS_VALUE13,CLASS_CODE14,CLASS_VALUE14,CLASS_CODE15,CLASS_VALUE15,CLASS_CODE16,CLASS_VALUE16,CLASS_CODE17,CLASS_VALUE17,  CLASS_CODE18,CLASS_VALUE18,CLASS_CODE19,CLASS_VALUE19,CLASS_CODE20,CLASS_VALUE20,CLASS_CODE21,CLASS_VALUE21,CLASS_CODE22,CLASS_VALUE22,CLASS_CODE23,  CLASS_VALUE23,CLASS_CODE24,CLASS_VALUE24,CLASS_CODE25,CLASS_VALUE25,CLASS_CODE26,CLASS_VALUE26,CLASS_CODE27,CLASS_VALUE27,CLASS_CODE28,CLASS_VALUE28,CLASS_CODE29,  CLASS_VALUE29,CLASS_CODE30,CLASS_VALUE30,REIMBURSABLE_TYPE_ID,BATCH_ID,EXT_COMPANY,EXT_WORK_ORDER_TYPE,EXT_MAJOR_LOCATION,BUDGET_NUMBER,BUDGET_COMPANY_ID,  EXT_DEPARTMENT,EXT_WORK_ORDER_GROUP,EXT_ASSET_LOCATION,SEQ_ID,USER_ID,TIME_STAMP,OLD_WO_STATUS_ID,EXT_WORK_ORDER_STATUS,EXT_REASON_CD,EXT_WO_APPROVAL_GROUP,  EXT_AGREEMENT,AUTO_APPROVED,REPAIR_LOCATION_ID,EXT_REPAIR_LOCATION,CO_TENANT_PARENT  from LGEKU_PROJECT_UPLOAD where nvl(status,0) <> -1 and interface_batch is not null)', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:43:32', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Transfer to WO_INTERFACE_STAGING');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 620, 1, 'update wo_interface_staging set batch_id = ''Create FP''', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:43:37', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Update Batch ID');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 630, 1, 'update wo_interface_staging w  set bus_segment_id = (select bus_segment_id from work_order_type wot  where w.work_order_type_id = wot.work_order_type_id)', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:43:39', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Update Business Segment');
Insert into PWRPLANT.CR_DATA_MOVER
   (BATCH, PRIORITY, DATABASE_ID, SQL, ACTION, 
    COMMIT_COUNTER, MAX_ERRORS, STATUS, DIRECTION, USER_ID, 
    TIME_STAMP, DELETE_WHERE_CLAUSE, DESCRIPTION)
 Values
   ('PROJ: Stage Data', 640, 1, 'update wo_interface_staging w set  CLASS_VALUE1 = decode(CLASS_VALUE1, ''null'', null, CLASS_VALUE1 ),  CLASS_VALUE2 = decode(CLASS_VALUE2, ''null'', null, CLASS_VALUE2 ),  CLASS_VALUE3 = decode(CLASS_VALUE3, ''null'', null, CLASS_VALUE3 ),  CLASS_VALUE4 = decode(CLASS_VALUE4, ''null'', null, CLASS_VALUE4 ),  CLASS_VALUE5 = decode(CLASS_VALUE5, ''null'', null, CLASS_VALUE5 ),  CLASS_VALUE6 = decode(CLASS_VALUE6, ''null'', null, CLASS_VALUE6 ),  CLASS_VALUE7 = decode(CLASS_VALUE7, ''null'', null, CLASS_VALUE7 ),  CLASS_VALUE8 = decode(CLASS_VALUE8, ''null'', null, CLASS_VALUE8 ),  CLASS_VALUE9 = decode(CLASS_VALUE9, ''null'', null, CLASS_VALUE9 ),  CLASS_VALUE10 = decode(CLASS_VALUE10, ''null'', null, CLASS_VALUE10 ),  CLASS_VALUE11 = decode(CLASS_VALUE11, ''null'', null, CLASS_VALUE11 ),  CLASS_VALUE12 = decode(CLASS_VALUE12, ''null'', null, CLASS_VALUE12 ),  CLASS_VALUE13 = decode(CLASS_VALUE13, ''null'', null, CLASS_VALUE13 ),  CLASS_VALUE14 = decode(CLASS_VALUE14, ''null'', null, CLASS_VALUE14 ),  CLASS_VALUE15 = decode(CLASS_VALUE15, ''null'', null, CLASS_VALUE15 ),  CLASS_VALUE16 = decode(CLASS_VALUE16, ''null'', null, CLASS_VALUE16 ),  CLASS_VALUE17 = decode(CLASS_VALUE17, ''null'', null, CLASS_VALUE17 ),  CLASS_VALUE18 = decode(CLASS_VALUE18, ''null'', null, CLASS_VALUE18 ),  CLASS_VALUE19 = decode(CLASS_VALUE19, ''null'', null, CLASS_VALUE19 ),  CLASS_VALUE20 = decode(CLASS_VALUE20, ''null'', null, CLASS_VALUE20 ),  CLASS_VALUE21 = decode(CLASS_VALUE21, ''null'', null, CLASS_VALUE21 ),  CLASS_VALUE22 = decode(CLASS_VALUE22, ''null'', null, CLASS_VALUE22 ),  CLASS_VALUE23 = decode(CLASS_VALUE23, ''null'', null, CLASS_VALUE23 ),  CLASS_VALUE24 = decode(CLASS_VALUE24, ''null'', null, CLASS_VALUE24 ),  CLASS_VALUE25 = decode(CLASS_VALUE25, ''null'', null, CLASS_VALUE25 ),  CLASS_VALUE26 = decode(CLASS_VALUE26, ''null'', null, CLASS_VALUE26 ),  CLASS_VALUE27 = decode(CLASS_VALUE27, ''null'', null, CLASS_VALUE27 ),  CLASS_VALUE28 = decode(CLASS_VALUE28, ''null'', null, CLASS_VALUE28 ),  CLASS_VALUE29 = decode(CLASS_VALUE29, ''null'', null, CLASS_VALUE29 ),  CLASS_VALUE30 = decode(CLASS_VALUE30, ''null'', null, CLASS_VALUE30 )  where exists (select 1 from lgeku_project_upload l, work_order_control woc where l.work_order_number = woc.work_order_number and l.company_id = woc.company_id and woc.work_order_number = w.work_order_number)', 'direct SQL', 
    0, 1, 1, 'Pull', 'PWRPLANT', 
    TO_DATE('08/27/2019 17:43:41', 'MM/DD/YYYY HH24:MI:SS'), ' ', 'Clean Up Class Code Values');
COMMIT;
