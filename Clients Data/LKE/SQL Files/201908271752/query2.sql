select dv.company_id, t.company_id, t.*, d.*, dv.* from work_order_type t, department d, division dv
where dv.division_id = d.division_id
and work_order_type_id = 18370240
and department_id = 1297
and dv.company_id <> t.company_id

select work_order_type_id, u.* from lgeku_project_upload u

select work_order_type_id, w.* from work_order_control w where work_order_number in(
select distinct work_order_number from lgeku_project_upload_arc)

update lgeku_project_upload
set error_message = error_message || 'The Funding Project Department is not a valid Project Owning Organization (in the HR All Organization Units table in Oracle Projects). Please select  another Department or contact the Budgeting Group.; ', status = -1
where ext_department in(
SELECT description from department where description in(select ext_department from lgeku_project_upload where nvl(status, 0) <> -1)
minus
select dp.description from department dp, division dv, hr_all_organization_units hr, hr.hr_organization_information@orafin hi,
pa_segment_value_lookup_sets pvls, pa_segment_value_lookups pvl
where hi.organization_id = hr.organization_id
and   hr.attribute3 = pvl.segment_value
and   pvls.segment_value_lookup_set_id = pvl.segment_value_lookup_set_id
and   pvls.segment_value_lookup_set_name = 'Proj Org to GL Responsible Ctr'
and dp.external_department_code = hr.attribute3
and dp.division_id = dv.division_id
and hr.type not in('CORP', 'INV', 'SBG')
and hi.org_information1 = 'PA_PROJECT_ORG'
and hi.org_information2 = 'Y'
and hr.date_to is null
and dv.company_id = decode(to_number(hr.attribute1), 700, 100, 702, 800, 703, 301, 704, 4, 710, 110, 720, 20, to_number(hr.attribute1))
and dp.department_id IN(SELECT department_id from department where description in(select ext_department from lgeku_project_upload where nvl(status, 0) <> -1)))

select * from (
with x as (select ext_work_order_type, ext_department from lgeku_project_upload where nvl(status, 0) <> -1)
select (SELECT department_id from department where description = x.ext_department) department_id,
       (select work_order_type_id from work_order_type where description = x.ext_work_order_type) work_order_type_id
from dual, x)

select * from work_order_type t, department d, division dv
where dv.division_id = d.division_id
and work_order_type_id = 18370240
and department_id = 1297
and dv.company_id <> t.company_id

select * from work_order_type

select * from department

select * from division order by 2, 1

update lgeku_project_upload u
set error_message = error_message || 'The Organization for this Funding Project is not valid for the Company on the designated Work Order Type.  Please select another Organization.; ', status = -1
where exists (select 1 from work_order_type t, department d, division dv
where dv.division_id = d.division_id
and work_order_type_id = (select work_order_type_id from work_order_type where description = u.ext_work_order_type)
and department_id = (select department_id from department where description = u.ext_department)
and dv.company_id <> t.company_id)

rollback

select ext_work_order_type, ext_department, u.* from lgeku_project_upload u
where exists (select 1 from work_order_type t, department d, division dv
where dv.division_id = d.division_id
and work_order_type_id = (select work_order_type_id from work_order_type where description = u.ext_work_order_type)
and department_id = (select department_id from department where description = u.ext_department)
and dv.company_id <> t.company_id)

select ext_work_order_type, ext_department, u.* from lgeku_project_upload u

select * from work_order_type where work_order_type_id = 18370240

select * from division where division_id = (select division_id from department where department_id = 1297)

select ext_work_order_type, ext_department, status, dbms_lob.substr(error_message, 500, 1) err_msg, u.* from lgeku_project_upload u