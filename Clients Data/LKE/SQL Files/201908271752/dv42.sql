SELECT COUNT(*) AS COUNT
FROM work_order_control fp, work_order_account wa
WHERE wa.work_order_id = fp.work_order_id
AND fp.funding_wo_indicator = 1
AND fp.work_order_id = <arg1>
AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 55 AND wcc.work_order_id = fp.work_order_id)

'The FP Reporting Group value is required for all Funding Projects.'

SELECT interface_batch, funding_wo_indicator, budget_number, EXT_DEPARTMENT, action, status, dbms_lob.substr(error_message, 1000, 1) err_msg, U.*
FROM lgeku_project_upload U

INSERT INTO lgeku_project_upload (company_id, work_order_number, ext_work_order_type, ext_major_location, description, ext_department, est_start_date,
est_complete_date, est_in_service_date, interface_batch)
SELECT company_id, work_order_number, (SELECT description FROM work_order_type x WHERE work_order_type_id = z.work_order_type_id) ext_work_order_type,
       (SELECT description FROM major_location x WHERE major_location_id = z.major_location_id) ext_major_location,
       SUBSTR(TO_CHAR(SYSDATE, 'yyyymmddhhmmss - ') || description, 1, LENGTH(description) - 17) description,
       (SELECT description FROM department x WHERE department_id = z.department_id) ext_department,
       SYSDATE est_start_date, SYSDATE est_complete_date, SYSDATE est_in_service_date, 37 interface_batch
FROM  work_order_control z WHERE work_order_id = 90550994

SELECT * FROM work_order_control WHERE work_order_number = '151893'

COMMIT

ROLLBACK

UPDATE lgeku_project_upload U
SET error_message = error_message || 'The FP Reporting Group value is required for all Funding Projects.; ', status = -1
WHERE EXISTS(
SELECT 1
FROM work_order_control fp, work_order_account wa
WHERE wa.work_order_id = fp.work_order_id
AND fp.funding_wo_indicator = 1
AND fp.work_order_number = U.work_order_number
AND fp.company_id = U.company_id
AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 55 AND wcc.work_order_id = fp.work_order_id)
)
AND U.interface_batch IS NOT NULL


--Final Query
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'The FP Reporting Group value is required for all Funding Projects.; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND NOT EXISTS (SELECT 1
                     FROM lgeku_project_upload_cc_values cc
                    WHERE cc.class_code_id = 55
                      AND cc.work_order_number = U.work_order_number
                      AND cc.company_id = U.company_id)
   AND (action = 'I'
        OR
        (
             action = 'U'
         AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 55 AND wcc.work_order_id = U.work_order_id)
        )
       )