SELECT work_order_number,
        (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) lgeku_70_cc,
        (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 77) lgeku_77_cc,
        (SELECT wocc.VALUE
                        FROM work_order_class_code wocc
                       WHERE work_order_id IN(SELECT work_order_id FROM work_order_control c WHERE c.work_order_number = U.work_order_number AND c.company_id = U.company_id AND funding_wo_indicator = 1)
                         AND class_code_id = 70) wocc_70_cc,
        (SELECT wocc.VALUE
                        FROM work_order_class_code wocc
                       WHERE work_order_id IN(SELECT work_order_id FROM work_order_control c WHERE c.work_order_number = U.work_order_number AND c.company_id = U.company_id AND funding_wo_indicator = 1)
                         AND class_code_id = 77) wocc_77_cc
FROM lgeku_project_upload U
