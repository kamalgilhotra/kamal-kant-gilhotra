SELECT DECODE (COUNT (*), 0, 0, 1)
  FROM work_order_account
 WHERE     work_order_id = <arg1>
       AND NVL (wo_est_hierarchy_id, 0) NOT IN (SELECT wo_est_hierarchy_id FROM wo_est_hierarchy)

-- Final Query
UPDATE work_order_account
   SET wo_est_hierarchy_id = 1
 WHERE NVL (wo_est_hierarchy_id, 0) NOT IN (SELECT wo_est_hierarchy_id FROM wo_est_hierarchy)
   AND 1 = (SELECT COUNT (*) FROM wo_est_hierarchy)
   AND work_order_id IN(SELECT woc.work_order_id
                          FROM work_order_control woc,
                               lgeku_project_upload U
                         WHERE woc.funding_wo_indicator = 1
                           AND U.interface_batch IS NOT NULL
                           AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
                           AND NVL(U.status, 0) <> -1
                           AND woc.work_order_number = U.work_order_number
                           AND woc.company_id = U.company_id)