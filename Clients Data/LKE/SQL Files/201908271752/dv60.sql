UPDATE lgeku_project_upload U
   SET error_message = error_message || 'The Funding Project Department is not a valid Project Owning Organization (in the HR All Organization Units table in Oracle Projects). Please select  another Department or contact the Budgeting Group.; ', status = -1
 WHERE NOT EXISTS
           (SELECT dp.description
              FROM department dp,
                   division dv,
                   hr_all_organization_units hr,
                   hr.hr_organization_information@orafin hi,
                   pa_segment_value_lookup_sets pvls,
                   pa_segment_value_lookups pvl
             WHERE hi.organization_id = hr.organization_id
               AND hr.attribute3 = pvl.segment_value
               AND pvls.segment_value_lookup_set_id = pvl.segment_value_lookup_set_id
               AND pvls.segment_value_lookup_set_name = 'Proj Org to GL Responsible Ctr'
               AND dp.external_department_code = hr.attribute3
               AND dp.division_id = dv.division_id
               AND hr.TYPE NOT IN ('CORP', 'INV', 'SBG')
               AND hi.org_information1 = 'PA_PROJECT_ORG'
               AND hi.org_information2 = 'Y'
               AND hr.date_to IS NULL
               AND dv.company_id = DECODE (TO_NUMBER (hr.attribute1), 700, 100, 702, 800, 703, 301, 704, 4, 710, 110, 720, 20, TO_NUMBER (hr.attribute1))
               AND dp.description = U.ext_department)
   AND ((action = 'U' AND U.ext_department IS NOT NULL) OR action = 'I')
   AND U.interface_batch IS NOT NULL
   and U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)