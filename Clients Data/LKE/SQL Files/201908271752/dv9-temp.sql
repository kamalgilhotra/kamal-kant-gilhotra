SELECT DECODE (COUNT (*), 0, 0, 1)
  FROM WORK_ORDER_CLASS_CODE WOCC53, WORK_ORDER_CONTROL WOC2
 WHERE WOC2.FUNDING_WO_INDICATOR = 1
   AND WOC2.WORK_ORDER_ID = <arg1>
   AND WOC2.WORK_ORDER_ID = WOCC53.WORK_ORDER_ID
   AND WOCC53.CLASS_CODE_ID = 53
   AND NVL (WOCC53.VALUE, 0) NOT IN
          (SELECT NVL (E.CLASS_CODE, 0)
             FROM EON_FP_TYPE_CLASS_CAT_CODE E, WORK_ORDER_CONTROL WOC1
            WHERE E.CLASS_CATEGORY =
                     (SELECT WOCC.VALUE
                        FROM WORK_ORDER_CONTROL WOC,
                             WORK_ORDER_CLASS_CODE WOCC
                       WHERE WOC.FUNDING_WO_INDICATOR = 1
                         AND WOC.WORK_ORDER_ID = <arg1>
                         AND WOC.WORK_ORDER_ID = WOCC.WORK_ORDER_ID
                         AND WOCC.CLASS_CODE_ID = 52)
              AND WOC1.FUNDING_WO_INDICATOR = 1
              AND WOC1.WORK_ORDER_ID = <arg1>
              AND WOC1.WORK_ORDER_TYPE_ID = E.WORK_ORDER_TYPE_ID)

WITH cc52 AS
(SELECT *
  FROM (SELECT class_code_id,
               VALUE class_code_value,
               1 seq_no,
               (SELECT work_order_number FROM work_order_control WHERE work_order_id = lgeku_project_upload_cc_values.work_order_id) work_order_number,
               (SELECT company_id FROM work_order_control WHERE work_order_id = lgeku_project_upload_cc_values.work_order_id) company_id
          FROM work_order_class_code lgeku_project_upload_cc_values
         WHERE class_code_id IN(52, 53) AND work_order_id = 251656091
       UNION ALL
       SELECT class_code_id,
              VALUE class_code_value,
              2 seq_no,
              (SELECT work_order_number FROM work_order_control WHERE work_order_id = lgeku_project_upload_cc_values.work_order_id) work_order_number,
              (SELECT company_id FROM work_order_control WHERE work_order_id = lgeku_project_upload_cc_values.work_order_id) company_id
         FROM work_order_class_code lgeku_project_upload_cc_values
        WHERE class_code_id IN(52, 53) AND work_order_id = 251656091
       ORDER BY 1, 2, 3)
 WHERE ROWNUM = 1)
SELECT cc52.class_code_id, cc52.class_code_value FROM work_order_control woc, cc52
WHERE woc.work_order_id = 251656091


SELECT E.CLASS_CODE FROM eon_fp_type_class_cat_code E, work_order_control c
WHERE c.work_order_id = 251656091
AND c.work_order_type_id = E.work_order_type_id
AND E.class_category = (SELECT VALUE FROM work_order_class_code WHERE work_order_id = 251656091 AND class_code_id = 52)
AND c.funding_wo_indicator = 1

SELECT * FROM work_order_class_code WHERE work_order_id = 251656091 AND class_code_id IN(52, 53)

SELECT * FROM 