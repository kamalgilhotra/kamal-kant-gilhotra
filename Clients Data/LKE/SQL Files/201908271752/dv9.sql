UPDATE lgeku_project_upload lke_lpu
   SET error_message = error_message || 'The selected ''Code'' value is not valid based on the ''Category'' selection.  This funding project is not valid to be sent to Oracle, please update the appropriate selection.; ', status = -1
 WHERE lke_lpu.interface_batch IS NOT NULL
   AND lke_lpu.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND (lke_lpu.work_order_number, lke_lpu.company_id)
        IN(
            WITH x AS
            (SELECT work_order_number, company_id,
                   (DECODE ((SELECT COUNT (*)
                               FROM lgeku_project_upload_cc_values lcc
                              WHERE lcc.work_order_number = lpu.work_order_number
                                AND lcc.company_id = lpu.company_id
                                AND lcc.class_code_id = 52),
                            0, (SELECT VALUE
                                  FROM work_order_class_code wocc
                                 WHERE wocc.class_code_id = 52
                                   AND wocc.work_order_id = (SELECT work_order_id
                                                               FROM work_order_control woc
                                                              WHERE lpu.work_order_number = woc.work_order_number
                                                                AND lpu.company_id = woc.company_id)),
                               (SELECT class_code_value
                               FROM lgeku_project_upload_cc_values lcc
                              WHERE lcc.work_order_number = lpu.work_order_number
                                AND lcc.company_id = lpu.company_id
                                AND lcc.class_code_id = 52))) class_value52,
                   (DECODE ((SELECT COUNT (*)
                               FROM lgeku_project_upload_cc_values lcc
                              WHERE lcc.work_order_number = lpu.work_order_number
                                AND lcc.company_id = lpu.company_id
                                AND lcc.class_code_id = 53),
                            0, (SELECT VALUE
                                  FROM work_order_class_code wocc
                                 WHERE wocc.class_code_id = 53
                                   AND wocc.work_order_id = (SELECT work_order_id
                                                               FROM work_order_control woc
                                                              WHERE lpu.work_order_number = woc.work_order_number
                                                                AND lpu.company_id = woc.company_id)),
                               (SELECT class_code_value
                               FROM lgeku_project_upload_cc_values lcc
                              WHERE lcc.work_order_number = lpu.work_order_number
                                AND lcc.company_id = lpu.company_id
                                AND lcc.class_code_id = 53))) class_value53
              FROM lgeku_project_upload lpu
             WHERE lpu.interface_batch IS NOT NULL
               AND lpu.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload))
            SELECT U.work_order_number, U.company_id
              FROM lgeku_project_upload U, x
             WHERE U.interface_batch IS NOT NULL
               AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
               AND U.work_order_number = x.work_order_number
               AND U.company_id = x.company_id
               AND NVL (x.class_value53, 0) NOT IN(SELECT NVL (E.class_code, 0)
                                                     FROM eon_fp_type_class_cat_code E
                                                    WHERE E.class_category = x.class_value52))