UPDATE lgeku_project_upload U
   SET action = DECODE ((SELECT COUNT (*)
                           FROM work_order_control w
                          WHERE w.funding_wo_indicator = 1
                            AND w.company_id = U.company_id
                            AND w.work_order_number = U.work_order_number), 0, 'I', 'U'),
       work_order_id = (SELECT work_order_id
                           FROM work_order_control w
                          WHERE w.funding_wo_indicator = 1
                            AND w.company_id = U.company_id
                            AND w.work_order_number = U.work_order_number)
 WHERE interface_batch IS NOT NULL