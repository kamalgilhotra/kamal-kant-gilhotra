SELECT Decode(Count(*), 0, 0, 1)
FROM work_order_class_code wocc53,
work_order_control woc2
WHERE woc2.funding_wo_indicator = 1
AND woc2.work_order_id = <arg1>
AND woc2.work_order_id = wocc53.work_order_id
AND wocc53.class_code_id = 53
AND Nvl(wocc53.Value, 0) NOT IN (
SELECT Nvl(e.class_code, 0)
FROM eon_fp_type_class_cat_code e,
work_order_control woc1
WHERE e.class_category = (
SELECT wocc.value
FROM work_order_control woc, work_order_class_code wocc
WHERE woc.funding_wo_indicator = 1
AND woc.work_order_id = <arg1>
AND woc.work_order_id = wocc.work_order_id
AND wocc.class_code_id = 52)
AND woc1.funding_wo_indicator = 1
AND woc1.work_order_id = <arg1>
AND woc1.work_order_type_id = e.work_order_type_id)