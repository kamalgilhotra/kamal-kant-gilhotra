SELECT company_id, work_order_number, (SELECT description FROM work_order_type x WHERE work_order_type_id = z.work_order_type_id) ext_work_order_type,
       (SELECT description FROM major_location x WHERE major_location_id = z.major_location_id) ext_major_location,
       SUBSTR(TO_CHAR(SYSDATE, 'yymmddhh24miss - ') || TRIM(description), 1, 30) description,
       (SELECT description FROM department x WHERE department_id = z.department_id) ext_department,
       SYSDATE est_start_date, SYSDATE est_complete_date, SYSDATE est_in_service_date
FROM  work_order_control z
WHERE work_order_id IN(SELECT work_order_id FROM WORK_ORDER_INITIATOR WHERE initiation_date >= TRUNC(SYSDATE - 101) AND initiation_date <= TRUNC(SYSDATE - 90))
AND funding_wo_indicator = 1
AND major_location_id IN(SELECT TO_NUMBER(SUBSTR(pkml, 1, LENGTH(pkml) - 1)) pkml FROM(
SELECT TRIM(SUBSTR(pkml, 19, LENGTH(pkml))) pkml FROM
(SELECT DISTINCT primary_key pkml FROM pp_audits_tables_trail
WHERE table_name = 'MAJOR_LOCATION'
AND action = 'I' AND month_number < 201905))) 
ORDER BY work_order_id