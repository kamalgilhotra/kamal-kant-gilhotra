SELECT DECODE (COUNT (*), 0, 0, 1)
  FROM WORK_ORDER_CONTROL FP
 WHERE     FP.FUNDING_WO_INDICATOR = 1
       AND SUBSTR (FP.LONG_DESCRIPTION, 1, LENGTH (FP.WORK_ORDER_NUMBER)) <> FP.WORK_ORDER_NUMBER
       AND FP.WORK_ORDER_ID = <arg1>

-- Final Query
UPDATE work_order_control woc
   SET woc.long_description = woc.work_order_number || '-' || SUBSTR (woc.long_description, 1, 1990)
 WHERE SUBSTR (woc.long_description, 1, LENGTH(woc.work_order_number)) <> woc.work_order_number
   AND woc.funding_wo_indicator = 1
   AND (woc.work_order_number, woc.company_id) IN(SELECT work_order_number, company_id
                                                    FROM lgeku_project_upload U
                                                   WHERE U.interface_batch IS NOT NULL
                                                     AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
                                                     AND NVL(U.status, 0) <> -1)