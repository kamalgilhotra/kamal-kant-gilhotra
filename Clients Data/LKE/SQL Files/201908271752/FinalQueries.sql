-- DV6 (Final 20190815)
UPDATE LGEKU_PROJECT_UPLOAD U
   SET ERROR_MESSAGE = ERROR_MESSAGE || 'Please ensure that Product, Description, and Detailed Description fields are all populated before updating a capital project; ', STATUS = -1
 WHERE     U.INTERFACE_BATCH IS NOT NULL
       AND (   EXISTS
                  (SELECT 1
                     FROM WORK_ORDER_CONTROL WOC, WORK_ORDER_ACCOUNT WOA
                    WHERE     WOC.WORK_ORDER_NUMBER = U.WORK_ORDER_NUMBER
                          AND WOC.COMPANY_ID = U.COMPANY_ID
                          AND WOC.WORK_ORDER_ID = WOA.WORK_ORDER_ID
                          AND WOA.WORK_ORDER_GRP_ID = 1
                          AND WOC.FUNDING_WO_INDICATOR = 1
                          AND (   (    WOC.LONG_DESCRIPTION IS NULL
                                   AND U.LONG_DESCRIPTION IS NULL)
                               OR (    WOC.REASON_CD_ID IS NULL
                                   AND U.REASON_CD_ID IS NULL)
                               OR (WOC.NOTES IS NULL AND U.NOTES IS NULL)))
            OR (    NOT EXISTS
                           (SELECT 1
                              FROM WORK_ORDER_CONTROL
                             WHERE     WORK_ORDER_NUMBER =
                                          U.WORK_ORDER_NUMBER
                                   AND COMPANY_ID = U.COMPANY_ID)
                AND (   U.LONG_DESCRIPTION IS NULL
                     OR U.REASON_CD_ID IS NULL
                     OR U.NOTES IS NULL))
           )

-- DV12 (Final 20190819)
UPDATE LGEKU_PROJECT_UPLOAD U
   SET ERROR_MESSAGE = ERROR_MESSAGE || 'Please populate an asset location for this capital funding project; ', STATUS = -1
 WHERE     U.INTERFACE_BATCH IS NOT NULL
       AND (EXISTS
              (SELECT 1
                 FROM WORK_ORDER_CONTROL WOC, WORK_ORDER_ACCOUNT WOA
                WHERE     WOC.WORK_ORDER_NUMBER = U.WORK_ORDER_NUMBER
                  AND WOC.WORK_ORDER_ID = WOA.WORK_ORDER_ID
                  AND WOA.WORK_ORDER_GRP_ID = 1
                  AND WOC.FUNDING_WO_INDICATOR = 1
                  AND WOC.ASSET_LOCATION_ID IS NULL)
        OR (NOT EXISTS
              (SELECT 1
                 FROM WORK_ORDER_CONTROL
                WHERE WORK_ORDER_NUMBER = U.WORK_ORDER_NUMBER
                  AND COMPANY_ID = U.COMPANY_ID)
                  AND U.ASSET_LOCATION_ID IS NULL))


-- 41 - Validate Own Department
UPDATE lgeku_project_upload U
set error_message = error_message || 'The Funding Project Department is not a valid Project Owning Organization (in the HR All Organization Units table in Oracle Projects). Please select  another Department or contact the Budgeting Group.; ', status = -1
WHERE NOT EXISTS (
SELECT dp.description FROM department dp, division dv, hr_all_organization_units hr, hr.hr_organization_information@orafin hi,
pa_segment_value_lookup_sets pvls, pa_segment_value_lookups pvl
WHERE hi.organization_id = hr.organization_id
AND   hr.attribute3 = pvl.segment_value
AND   pvls.segment_value_lookup_set_id = pvl.segment_value_lookup_set_id
AND   pvls.segment_value_lookup_set_name = 'Proj Org to GL Responsible Ctr'
AND dp.external_department_code = hr.attribute3
AND dp.division_id = dv.division_id
AND hr.TYPE NOT IN('CORP', 'INV', 'SBG')
AND hi.org_information1 = 'PA_PROJECT_ORG'
AND hi.org_information2 = 'Y'
AND hr.date_to IS NULL
AND dv.company_id = DECODE(TO_NUMBER(hr.attribute1), 700, 100, 702, 800, 703, 301, 704, 4, 710, 110, 720, 20, TO_NUMBER(hr.attribute1))
AND dp.description = U.EXT_DEPARTMENT)
AND U.interface_batch IS NOT NULL

-- 42 - Validate Work Order Type Company
UPDATE lgeku_project_upload U
set error_message = error_message || 'The Organization for this Funding Project is not valid for the Company on the designated Work Order Type.  Please select another Organization.; ', status = -1
WHERE EXISTS (SELECT 1 FROM work_order_type T, department d, division dv
WHERE dv.division_id = d.division_id
AND work_order_type_id = (SELECT work_order_type_id FROM work_order_type WHERE description = U.ext_work_order_type)
AND department_id = (SELECT department_id FROM department WHERE description = U.ext_department)
AND dv.company_id <> T.company_id)
AND U.interface_batch IS NOT NULL

-- DV57 - Skip, as this is just for FUNDING_WO_INDICATOR = 0
SELECT * FROM lgeku_project_upload U
WHERE EXISTS(
SELECT 1
FROM work_order_control woc
WHERE funding_wo_indicator = 0
AND work_order_id = (
SELECT work_order_id
FROM work_order_control
WHERE work_order_number = U.work_order_number
AND company_id = U.company_id
AND funding_wo_indicator = 0)
AND EXISTS(SELECT 1
FROM cwip_charge cc
WHERE cc.work_order_id = woc.work_order_id
AND cc.amount <> 0)
AND NVL(woc.department_id, 0) <> (SELECT department_id FROM department WHERE description = U.ext_department))

-- DV6 - Enforce availability of Product,Long Description and Notes fields for Capital FP's   
UPDATE lgeku_project_upload U
SET error_message = error_message || 'Please ensure that Product, Description, and Detailed Description fields are all populated before updating a capital project; ', status = -1
WHERE EXISTS(
SELECT 1
FROM work_order_control woc,
work_order_account woa
WHERE woc.work_order_number = U.work_order_number
AND woc.company_id = U.company_id
AND woc.work_order_id = woa.work_order_id
AND woa.work_order_grp_id = 1
AND woc.funding_wo_indicator = 1
AND (woc.long_description IS NULL OR woc.reason_cd_id IS NULL OR woc.notes IS NULL))
AND U.interface_batch IS NOT NULL

-- DV11 - Enforce Plant Account for CAP FP
UPDATE lgeku_project_upload U
SET error_message = error_message || 'Warning: A Plant Account must be selected for a capital funding project.  Please select a plant account from the class code tab.; ', status = -1
WHERE EXISTS(SELECT 1
FROM work_order_control woc, work_order_account woa
WHERE woc.funding_wo_indicator = 1
AND woc.work_order_number = U.work_order_number
AND woc.company_id = U.company_id
AND woc.work_order_id = woa.work_order_id
AND woa.work_order_grp_id = 1
AND NOT EXISTS (SELECT 1
FROM work_order_class_code wocc
WHERE woc.work_order_id = wocc.work_order_id
AND wocc.class_code_id = 46))
AND U.interface_batch IS NOT NULL

-- DV12 - Enforce Asset Location for Cap FP
UPDATE lgeku_project_upload U
SET error_message = error_message || 'Please populate an asset location for this capital funding project; ', status = -1
WHERE EXISTS(SELECT 1
FROM work_order_control woc,
work_order_account woa
WHERE woc.work_order_number = U.work_order_number
AND woc.company_id = U.company_id
AND woc.funding_wo_indicator = 1
AND woc.work_order_id = woa.work_order_id
AND woa.work_order_grp_id = 1
AND woc.asset_location_id IS NULL)
AND U.interface_batch IS NOT NULL

-- DV17 - Target Project Req w Rule
UPDATE lgeku_project_upload U
SET error_message = error_message || 'There is an Allocation Rule defined, but no Target Projects have been entered.; ', status = -1
WHERE EXISTS(SELECT 1
FROM work_order_control woc, work_order_class_code wocc
WHERE wocc.work_order_id = woc.work_order_id
AND woc.funding_wo_indicator = 1
AND woc.work_order_number = U.work_order_number
AND woc.company_id = U.company_id
AND wocc.class_code_id = 70
AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 70) IS NOT NULL
AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 71) IS NULL)
AND U.interface_batch IS NOT NULL

-- DV18 - IND Alloc Rule for IND Proj
UPDATE lgeku_project_upload U
SET error_message = error_message || '''CAP'' Allocation Rules are reserved for Capital Source Projects; ', status = -1
WHERE EXISTS(SELECT 1
FROM work_order_control woc
INNER JOIN work_order_account woa ON woa.work_order_id = woc.work_order_id
AND woa.work_order_grp_id = 2
AND woc.funding_wo_indicator = 1
WHERE woc.work_order_number = U.work_order_number
AND woc.company_id = U.company_id
AND SUBSTR((SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.class_code_id = 70
AND wocc.work_order_id = woc.work_order_id), 1, 3) = 'CAP'
AND EXISTS
(SELECT wocc.work_order_id, wocc.VALUE
FROM work_order_class_code wocc
WHERE wocc.work_order_id = woc.work_order_id
AND wocc.class_code_id = 70
AND wocc.VALUE IS NOT NULL)
)
AND U.interface_batch IS NOT NULL

-- DV20 - CAP Alloc Rule for CAP Proj [54]
UPDATE lgeku_project_upload U
SET error_message = error_message || 'Allocation Rules on Capital Source Projects have to start with ''CAP''.; ', status = -1
WHERE EXISTS(SELECT 1
FROM work_order_control woc
INNER JOIN work_order_account woa ON woa.work_order_id = woc.work_order_id
WHERE woa.work_order_grp_id = 1 AND woc.funding_wo_indicator = 1
AND woc.work_order_number = U.work_order_number
AND woc.company_id = U.company_id
AND SUBSTR((SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.class_code_id = 70
AND wocc.work_order_id = woc.work_order_id), 1, 3) <> 'CAP'
AND EXISTS (SELECT wocc.work_order_id, wocc.VALUE
FROM work_order_class_code wocc
WHERE wocc.work_order_id = woc.work_order_id
AND wocc.class_code_id = 70
AND wocc.VALUE IS NOT NULL)
)
AND U.interface_batch IS NOT NULL

-- DV21 - Validate Target Projects [52]
UPDATE lgeku_project_upload U
SET error_message = error_message || 'Target Projects does not exist or is a Source Project.; ', status = -1
WHERE EXISTS(
SELECT 1 FROM
(SELECT work_order_number, work_order_id, wo_status_id
FROM work_order_control woc
WHERE woc.work_order_number IN (
SELECT REGEXP_SUBSTR((SELECT wocc.VALUE
FROM work_order_class_code wocc, work_order_control woc
WHERE wocc.work_order_id = woc.work_order_id
AND woc.work_order_number = U.work_order_number
AND woc.COMPANY_ID = U.company_id 
AND class_code_id = 71), '|[^|]+|', 1, ROWNUM) sp_wo_num
FROM dual
connect BY LEVEL <= LENGTH(REGEXP_REPLACE((SELECT wocc.VALUE
FROM work_order_class_code wocc, work_order_control woc
WHERE wocc.work_order_id = woc.work_order_id
AND woc.work_order_number = U.work_order_number
AND woc.COMPANY_ID = U.company_id 
AND class_code_id = 71), '[^|]+')) +1)
MINUS
SELECT work_order_number, work_order_id, wo_status_id
FROM work_order_control woc
WHERE woc.work_order_number IN (
SELECT REGEXP_SUBSTR((SELECT wocc.VALUE
FROM work_order_class_code wocc, work_order_control woc
WHERE wocc.work_order_id = woc.work_order_id
AND woc.work_order_number = U.work_order_number
AND woc.COMPANY_ID = U.company_id 
AND class_code_id = 71), '|[^|]+|', 1, ROWNUM) sp_wo_num
FROM dual
connect BY LEVEL <= LENGTH(REGEXP_REPLACE((SELECT wocc.VALUE
FROM work_order_class_code wocc, work_order_control woc
WHERE wocc.work_order_id = woc.work_order_id
AND woc.work_order_number = U.work_order_number
AND woc.COMPANY_ID = U.company_id 
AND class_code_id = 71), '[^|]+')) +1)
AND woc.work_order_number NOT IN
(SELECT work_order_number
FROM (SELECT woc.work_order_number
FROM work_order_control woc, work_order_class_code wocc
WHERE woc.work_order_id = wocc.work_order_id AND wocc.class_code_id = 70
AND woc.work_order_number IN (SELECT REGEXP_SUBSTR((SELECT wocc.VALUE
FROM work_order_class_code wocc, work_order_control woc
WHERE wocc.work_order_id = woc.work_order_id
AND woc.work_order_number = U.work_order_number
AND woc.COMPANY_ID = U.company_id 
AND class_code_id = 71), '|[^|]+|', 1, ROWNUM) sp_wo_num
FROM dual
connect BY LEVEL <= LENGTH(REGEXP_REPLACE((SELECT wocc.VALUE
FROM work_order_class_code wocc, work_order_control woc
WHERE wocc.work_order_id = woc.work_order_id
AND woc.work_order_number = U.work_order_number
AND woc.COMPANY_ID = U.company_id 
AND class_code_id = 71), '[^|]+')) +1))))
)
AND U.interface_batch IS NOT NULL

-- DV27 - Enforce Budget Coord for Cap FP [97]
UPDATE lgeku_project_upload U
SET error_message = error_message || 'Warning: A budget coordinator must be selected for a capital funding project.  Please select a budget coordinator from the accounts tab.; ', status = -1
WHERE EXISTS(
SELECT 1
FROM work_order_control woc, work_order_account woa, work_order_initiator woi
WHERE funding_wo_indicator = 1
AND woc.work_order_number = U.work_order_number
AND woc.company_id = U.company_id
AND woc.work_order_id = woa.work_order_id
AND woa.work_order_grp_id = 1
AND woc.work_order_id = woi.work_order_id
AND woi.other IS NULL
)
AND U.interface_batch IS NOT NULL

-- DV28 - Alloc Target no Alloc Rule [59]
UPDATE lgeku_project_upload U
SET error_message = error_message || 'There is a Target Project entered, but no valid Allocation Rule has been entered.; ', status = -1
WHERE EXISTS(
SELECT 1
FROM work_order_control woc, work_order_class_code wocc
WHERE woc.work_order_id = wocc.work_order_id
AND woc.work_order_number = U.work_order_number
AND woc.company_id = U.company_id
AND wocc.class_code_id = 71
AND woc.funding_wo_indicator = 1
AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 70) IS NULL
AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 71) IS NOT NULL
)
AND U.interface_batch IS NOT NULL

-- DV29 - Target Number on Source Proj [60]
UPDATE lgeku_project_upload U
SET error_message = error_message || 'No Target Numbers are allowed when there is an Allocation Rule defined.; ', status = -1
WHERE EXISTS(
SELECT 1
FROM work_order_control woc, work_order_class_code wocc
WHERE woc.work_order_id = wocc.work_order_id AND woc.funding_wo_indicator = 1
AND woc.work_order_number = U.work_order_number
AND woc.company_id = U.company_id
AND class_code_id IN (70,77)
AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 70) IS NOT NULL
AND (SELECT wocc.VALUE FROM work_order_class_code wocc WHERE wocc.work_order_id = woc.work_order_id AND wocc.class_code_id = 77) IS NOT NULL
)
AND U.interface_batch IS NOT NULL

-- DV41 - FP RAC Cat Required-WO Update [14]
UPDATE lgeku_project_upload U
SET error_message = error_message || 'The RAC Category value is required for Funding Projects.; ', status = -1
WHERE EXISTS(
SELECT 1
FROM work_order_control fp, work_order_account wa
WHERE wa.work_order_id = fp.work_order_id
AND fp.funding_wo_indicator = 1
AND wa.work_order_grp_id = 1
AND fp.work_order_number = U.work_order_number
AND fp.company_id = U.company_id
AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 54 AND wcc.work_order_id = fp.work_order_id)
)
AND U.interface_batch IS NOT NULL

-- DV42 - FP RAC Priority Req-WO Update [16]
UPDATE lgeku_project_upload U
SET error_message = error_message || 'The FP Reporting Group value is required for all Funding Projects.; ', status = -1
WHERE EXISTS(
SELECT 1
FROM work_order_control fp, work_order_account wa
WHERE wa.work_order_id = fp.work_order_id
AND fp.funding_wo_indicator = 1
AND fp.work_order_number = U.work_order_number
AND fp.company_id = U.company_id
AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 55 AND wcc.work_order_id = fp.work_order_id)
)
AND U.interface_batch IS NOT NULL

-- DV43 - FP ECR Plan Required-WO Update [18]
UPDATE lgeku_project_upload U
SET error_message = error_message || 'A Code of "ECR Approved Plan" is designated for this funding project. Please designate an ECR Plan Number class code value for this funding project.; ', status = -1
WHERE EXISTS(
SELECT 1
FROM work_order_class_code wcc, work_order_control fp, work_order_account wa
WHERE wcc.work_order_id = fp.work_order_id
AND wa.work_order_id = fp.work_order_id
AND fp.funding_wo_indicator = 1
AND wa.work_order_grp_id = 1
AND wcc.class_code_id = 53
AND fp.work_order_number = U.work_order_number
AND fp.company_id = U.company_id
AND UPPER(wcc.VALUE) = 'ECR APPROVED PLAN'
AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc58 WHERE wcc58.class_code_id = 58 AND wcc58.work_order_id = fp.work_order_id)
)
AND U.interface_batch IS NOT NULL

