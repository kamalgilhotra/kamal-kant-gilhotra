SELECT DECODE(COUNT(*), 0, 0, 1)
FROM work_order_control woc, work_order_account woa, work_order_initiator woi
WHERE funding_wo_indicator = 1
AND woc.work_order_id = <arg1>
AND woc.work_order_id = woa.work_order_id
AND woa.work_order_grp_id = 1
AND woc.work_order_id = woi.work_order_id
AND woi.other IS NULL

'Warning: A budget coordinator must be selected for a capital funding project.  Please select a budget coordinator from the accounts tab.'

SELECT interface_batch, funding_wo_indicator, budget_number, EXT_DEPARTMENT, action, status, dbms_lob.substr(error_message, 1000, 1) err_msg, U.*
FROM lgeku_project_upload U

INSERT INTO lgeku_project_upload (company_id, work_order_number, ext_work_order_type, ext_major_location, description, ext_department, est_start_date,
est_complete_date, est_in_service_date, interface_batch)
SELECT company_id, work_order_number, (SELECT description FROM work_order_type x WHERE work_order_type_id = z.work_order_type_id) ext_work_order_type,
       (SELECT description FROM major_location x WHERE major_location_id = z.major_location_id) ext_major_location,
       SUBSTR(TO_CHAR(SYSDATE, 'yyyymmddhhmmss - ') || description, 1, LENGTH(description) - 17) description,
       (SELECT description FROM department x WHERE department_id = z.department_id) ext_department,
       SYSDATE est_start_date, SYSDATE est_complete_date, SYSDATE est_in_service_date, 37 interface_batch
FROM  work_order_control z WHERE work_order_id = 90550994

SELECT * FROM work_order_control WHERE work_order_number = '151893'

COMMIT

ROLLBACK

--Final Query
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'Warning: A budget coordinator must be selected for a capital funding project.  Please select a budget coordinator from the accounts tab.; ', status = -1
 WHERE U.interface_batch IS NOT NULL AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND (
        (    U.action = 'U'
         AND U.other_contact IS NULL
         AND U.work_order_grp_id = 1
         AND EXISTS (SELECT 1 FROM work_order_initiator i WHERE i.work_order_id = U.work_order_id AND i.other IS NULL)
        )
        OR
        (    action = 'I'
         AND U.other_contact IS NULL
         AND U.work_order_grp_id = 1
        )
       )