SELECT work_order_number
  FROM work_order_control woc, work_order_account woa
 WHERE     woc.work_order_id = 11942311
       AND woc.work_order_id = woa.work_order_id
       AND woa.work_order_grp_id = 1
       AND woc.funding_wo_indicator = 1
       AND (   woc.long_description IS NULL
            OR woc.reason_cd_id IS NULL
            OR woc.notes IS NULL)

'Please ensure that Product, Description, and Detailed Description fields are all populated before updating a capital project'

SELECT * FROM work_order_control woc, work_order_account woa
WHERE (woc.long_description IS NULL OR woc.reason_cd_id IS NULL OR woc.notes IS NULL)
  AND funding_wo_indicator = 1
  AND woc.work_order_id = woa.work_order_id
  AND woa.work_order_grp_id = 1
  AND work_order_number = '124561'

SELECT * FROM work_order_approval
WHERE work_order_grp_id = 1

SELECT DECODE(COUNT(*), 0, 0, 1)
FROM work_order_control woc,
work_order_account woa
WHERE woc.work_order_id = 12921145
AND woc.work_order_id = woa.work_order_id
AND woa.work_order_grp_id = 1
AND woc.funding_wo_indicator = 1
AND (woc.long_description IS NULL OR woc.reason_cd_id IS NULL OR woc.notes IS NULL)

SELECT * FROM work_order_control WHERE work_order_number = 'BTM216-1227264'

SELECT work_order_grp_id FROM work_order_account WHERE work_order_id = 11942310

SELECT * FROM work_order_control WHERE work_order_id = 12921145

SELECT interface_batch, funding_wo_indicator, budget_number, EXT_DEPARTMENT, action, status, dbms_lob.substr(error_message, 500, 1) err_msg, U.*
FROM lgeku_project_upload U WHERE work_order_number = '124561'

UPDATE lgeku_project_upload
set interface_batch = 37
WHERE work_order_number = '124561'

COMMIT

UPDATE lgeku_project_upload U
set error_message = error_message || 'Please ensure that Product, Description, and Detailed Description fields are all populated before updating a capital project; ', status = -1
WHERE EXISTS(
SELECT 1
FROM work_order_control woc,
work_order_account woa
WHERE woc.work_order_number = U.work_order_number
AND woc.work_order_id = woa.work_order_id
AND woa.work_order_grp_id = 1
AND woc.funding_wo_indicator = 1
AND (woc.long_description IS NULL OR woc.reason_cd_id IS NULL OR woc.notes IS NULL))
AND U.interface_batch IS NOT NULL



-- Final 20190821
UPDATE LGEKU_PROJECT_UPLOAD U
   SET ERROR_MESSAGE = ERROR_MESSAGE || 'Please ensure that Product, Description, and Detailed Description fields are all populated before updating a capital project; ', STATUS = -1
 WHERE     U.INTERFACE_BATCH IS NOT NULL
       AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
       AND U.WORK_ORDER_GRP_ID = 1
       AND (
            (    U.action = 'U'
             AND EXISTS (SELECT 1
                           FROM WORK_ORDER_CONTROL WOC
                          WHERE WOC.WORK_ORDER_ID = U.WORK_ORDER_ID
                            AND (   (WOC.LONG_DESCRIPTION IS NULL AND U.LONG_DESCRIPTION IS NULL)
                                 OR (WOC.REASON_CD_ID IS NULL AND U.REASON_CD_ID IS NULL)
                                 OR (WOC.NOTES IS NULL AND U.NOTES IS NULL)))
            )
            OR
            (    U.action ='I'
             AND (U.LONG_DESCRIPTION IS NULL OR U.REASON_CD_ID IS NULL OR U.NOTES IS NULL)
            )
           )