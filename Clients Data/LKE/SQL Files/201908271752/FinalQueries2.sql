-- DV6 (Final 20190815)
UPDATE LGEKU_PROJECT_UPLOAD U
   SET ERROR_MESSAGE = ERROR_MESSAGE || 'Please ensure that Product, Description, and Detailed Description fields are all populated before updating a capital project; ', STATUS = -1
 WHERE     U.INTERFACE_BATCH IS NOT NULL
       AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
       AND U.WORK_ORDER_GRP_ID = 1
       AND (
            (    U.action = 'U'
             AND EXISTS (SELECT 1
                           FROM WORK_ORDER_CONTROL WOC
                          WHERE WOC.WORK_ORDER_ID = U.WORK_ORDER_ID
                            AND (   (WOC.LONG_DESCRIPTION IS NULL AND U.LONG_DESCRIPTION IS NULL)
                                 OR (WOC.REASON_CD_ID IS NULL AND U.REASON_CD_ID IS NULL)
                                 OR (WOC.NOTES IS NULL AND U.NOTES IS NULL)))
            )
            OR
            (    U.action ='I'
             AND (U.LONG_DESCRIPTION IS NULL OR U.REASON_CD_ID IS NULL OR U.NOTES IS NULL)
            )
           )

-- DV12 (Final 20190819)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'Please populate an asset location for this capital funding project; ', status = -1
 WHERE     U.interface_batch IS NOT NULL
       AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
       AND U.work_order_grp_id = 1
       AND U.ext_asset_location IS NULL
       AND (
            U.action = 'I'
            OR
            (
                 U.action = 'U'
             AND EXISTS
                   (SELECT 1
                      FROM work_order_control woc
                     WHERE woc.work_order_id = U.work_order_id
                       AND woc.asset_location_id IS NULL)
            )
           )


-- DV17 (Final 20190819)
UPDATE lgeku_project_upload U
SET    error_message = error_message || 'There is an Allocation Rule defined, but no Target Projects have been entered.; ', status = -1
WHERE  U.interface_batch IS NOT NULL
  AND  U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
  AND  (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) IS NOT NULL
  AND  (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id AND wocc.class_code_id = 71) IS NULL
           
-- DV18 (Final 20190819)
UPDATE lgeku_project_upload U
   SET error_message = error_message || '''CAP'' Allocation Rules are reserved for Capital Source Projects; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 2
   AND EXISTS (SELECT 1
                 FROM lgeku_project_upload_cc_values wocc
                WHERE wocc.class_code_id = 70
                  AND wocc.work_order_number = U.work_order_number
                  AND wocc.company_id = U.company_id
                  AND wocc.class_code_value LIKE 'CAP%')

--DV20 (Final 20190819)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'Allocation Rules on Capital Source Projects have to start with ''CAP''.; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND EXISTS (SELECT 1
                 FROM lgeku_project_upload_cc_values wocc
                WHERE wocc.class_code_id = 70
                  AND wocc.work_order_number = U.work_order_number
                  AND wocc.company_id = U.company_id
                  AND wocc.class_code_value IS NOT NULL)
   AND EXISTS (SELECT 1
                 FROM lgeku_project_upload_cc_values wocc
                WHERE wocc.class_code_id = 70
                  AND wocc.work_order_number = U.work_order_number
                  AND wocc.company_id = U.company_id
                  AND wocc.class_code_value NOT LIKE 'CAP%')
       
--DV27 (Final 20190820)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'Warning: A budget coordinator must be selected for a capital funding project.  Please select a budget coordinator from the accounts tab.; ', status = -1
 WHERE U.interface_batch IS NOT NULL AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND (
        (    U.action = 'U'
         AND U.other_contact IS NULL
         AND U.work_order_grp_id = 1
         AND EXISTS (SELECT 1 FROM work_order_initiator i WHERE i.work_order_id = U.work_order_id AND i.other IS NULL)
        )
        OR
        (    action = 'I'
         AND U.other_contact IS NULL
         AND U.work_order_grp_id = 1
        )
       )

--DV28 (Final Query 20190820)
UPDATE lgeku_project_upload U
SET    error_message = error_message || 'There is a Target Project entered, but no valid Allocation Rule has been entered.; ', status = -1
WHERE  U.interface_batch IS NOT NULL AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id AND wocc.class_code_id = 71) IS NOT NULL
   AND (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) IS NULL
           
-- DV29 (Final Query 20190820)
UPDATE lgeku_project_upload U
SET    error_message = error_message || 'No Target Numbers are allowed when there is an Allocation Rule defined.; ', status = -1
WHERE  U.interface_batch IS NOT NULL AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND (
        (   (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) IS NOT NULL
           AND (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 77) IS NOT NULL)
        OR
        (   (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) IS NOT NULL
           AND U.action = 'U'
           AND EXISTS(SELECT 1
                        FROM work_order_class_code
                       WHERE work_order_id = U.work_order_id
                         AND class_code_id = 77
                         AND VALUE IS NOT NULL))
        OR
        (   (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 77) IS NOT NULL
           AND U.action = 'U'
           AND EXISTS(SELECT 1
                        FROM work_order_class_code
                       WHERE work_order_id = U.work_order_id
                         AND class_code_id = 70
                         AND VALUE IS NOT NULL))
       )
       
--DV41 (Final Query 20190820)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'The RAC Category value is required for Funding Projects.; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND NOT EXISTS (SELECT 1
                     FROM lgeku_project_upload_cc_values cc
                    WHERE cc.class_code_id = 54
                      AND cc.work_order_number = U.work_order_number
                      AND cc.company_id = U.company_id)
   AND (action = 'I'
        OR
        (
             action = 'U'
         AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 54 AND wcc.work_order_id = U.work_order_id)
        )
       )
    
--DV42 (Final Query 20190820)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'The FP Reporting Group value is required for all Funding Projects.; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND NOT EXISTS (SELECT 1
                     FROM lgeku_project_upload_cc_values cc
                    WHERE cc.class_code_id = 55
                      AND cc.work_order_number = U.work_order_number
                      AND cc.company_id = U.company_id)
   AND (action = 'I'
        OR
        (
             action = 'U'
         AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 55 AND wcc.work_order_id = U.work_order_id)
        )
       )
    
--DV43 (Final Query 20190821)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'A Code of "ECR Approved Plan" is designated for this funding project. Please designate an ECR Plan Number class code value for this funding project.; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id
           AND wocc.class_code_id = 53) = 'ECR APPROVED PLAN'
   AND (
        (    action = 'U'
         AND NOT EXISTS(SELECT 1
                          FROM lgeku_project_upload_cc_values wocc
                         WHERE wocc.work_order_number = U.work_order_number
                           AND wocc.company_id = U.company_id
                           AND wocc.class_code_id = 58)
         AND NOT EXISTS(SELECT 1
                          FROM work_order_class_code wocc
                         WHERE wocc.work_order_id = U.work_order_id
                           AND wocc.class_code_id = 58)
        )
        OR
        (    action = 'I'
         AND NOT EXISTS(SELECT 1
                          FROM lgeku_project_upload_cc_values wocc
                         WHERE wocc.work_order_number = U.work_order_number
                           AND wocc.company_id = U.company_id
                           AND wocc.class_code_id = 58)
        )
       )
                          
--DV60 (Final Query 20190821)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'The Funding Project Department is not a valid Project Owning Organization (in the HR All Organization Units table in Oracle Projects). Please select  another Department or contact the Budgeting Group.; ', status = -1
 WHERE NOT EXISTS
           (SELECT dp.description
              FROM department dp,
                   division dv,
                   hr_all_organization_units hr,
                   hr.hr_organization_information@orafin hi,
                   pa_segment_value_lookup_sets pvls,
                   pa_segment_value_lookups pvl
             WHERE hi.organization_id = hr.organization_id
               AND hr.attribute3 = pvl.segment_value
               AND pvls.segment_value_lookup_set_id = pvl.segment_value_lookup_set_id
               AND pvls.segment_value_lookup_set_name = 'Proj Org to GL Responsible Ctr'
               AND dp.external_department_code = hr.attribute3
               AND dp.division_id = dv.division_id
               AND hr.TYPE NOT IN ('CORP', 'INV', 'SBG')
               AND hi.org_information1 = 'PA_PROJECT_ORG'
               AND hi.org_information2 = 'Y'
               AND hr.date_to IS NULL
               AND dv.company_id = DECODE (TO_NUMBER (hr.attribute1), 700, 100, 702, 800, 703, 301, 704, 4, 710, 110, 720, 20, TO_NUMBER (hr.attribute1))
               AND dp.description = U.ext_department)
   AND ((action = 'U' AND U.ext_department IS NOT NULL) OR action = 'I')
   AND U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)

--DV62 (Final Query 20190821)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'The Organization for this Funding Project is not valid for the Company on the designated Work Order Type.  Please select another Organization.; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND EXISTS (SELECT 1
                 FROM work_order_type T, department d, division dv
                WHERE dv.division_id = d.division_id
                  AND work_order_type_id = U.work_order_type_id
                  AND department_id = U.department_id
                  AND dv.company_id <> T.company_id)

--DV44 (Final Query 20190822)
INSERT INTO work_order_class_code (class_code_id, work_order_id, VALUE)
   SELECT 57, woc.work_order_id, 'NA'
     FROM work_order_control woc,
          lgeku_project_upload U
    WHERE U.interface_batch IS NOT NULL
      AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
      AND U.WORK_ORDER_GRP_ID = 1
      AND U.company_id = woc.company_id
      AND U.work_order_number = woc.work_order_number
      AND NVL(U.status, 0) <> -1
      AND woc.funding_wo_indicator = 1
      AND NOT EXISTS
                  (SELECT 1
                     FROM work_order_class_code wcc
                    WHERE     wcc.class_code_id = 57
                          AND wcc.work_order_id = woc.work_order_id)

--DV45 (Final Query 20190822)
INSERT INTO work_order_class_code (class_code_id, work_order_id, VALUE)
   SELECT 56, woc.work_order_id, 'NA'
     FROM work_order_control woc,
          lgeku_project_upload U
    WHERE U.interface_batch IS NOT NULL
      AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
      AND U.WORK_ORDER_GRP_ID = 1
      AND U.company_id = woc.company_id
      AND U.work_order_number = woc.work_order_number
      AND NVL(U.status, 0) <> -1
      AND woc.funding_wo_indicator = 1
      AND NOT EXISTS
                  (SELECT 1
                     FROM work_order_class_code wcc
                    WHERE     wcc.class_code_id = 56
                          AND wcc.work_order_id = woc.work_order_id)

--DV53 (Final Query 20190822)
UPDATE work_order_control woc
   SET woc.long_description = woc.work_order_number || '-' || SUBSTR (woc.long_description, 1, 1990)
 WHERE SUBSTR (woc.long_description, 1, LENGTH(woc.work_order_number)) <> woc.work_order_number
   AND woc.funding_wo_indicator = 1
   AND (woc.work_order_number, woc.company_id) IN(SELECT work_order_number, company_id
                                                    FROM lgeku_project_upload U
                                                   WHERE U.interface_batch IS NOT NULL
                                                     AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
                                                     AND NVL(U.status, 0) <> -1)

--DV54 (Final Query 20190822)
UPDATE work_order_account
   SET wo_est_hierarchy_id = 1
 WHERE NVL (wo_est_hierarchy_id, 0) NOT IN (SELECT wo_est_hierarchy_id FROM wo_est_hierarchy)
   AND 1 = (SELECT COUNT (*) FROM wo_est_hierarchy)
   AND work_order_id IN(SELECT woc.work_order_id
                          FROM work_order_control woc,
                               lgeku_project_upload U
                         WHERE woc.funding_wo_indicator = 1
                           AND U.interface_batch IS NOT NULL
                           AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
                           AND NVL(U.status, 0) <> -1
                           AND woc.work_order_number = U.work_order_number
                           AND woc.company_id = U.company_id)

--DV55 (Final Query 1 20190822)
UPDATE work_order_approval A
   SET A.approval_status_id = 6
 WHERE EXISTS (SELECT 1
                 FROM workflow w
                WHERE w.id_field1 = A.work_order_id
                  AND w.subsystem = 'fp_approval'
                  AND w.approval_status_id = 6
                  AND w.id_field1 = A.work_order_id)
   AND A.approval_status_id = 1
   AND A.approval_type_id = 99
   AND A.work_order_id IN(SELECT woc.work_order_id
                            FROM work_order_control woc,
                                 lgeku_project_upload U
                           WHERE woc.funding_wo_indicator = 1
                             AND U.interface_batch IS NOT NULL
                             AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
                             AND NVL(U.status, 0) <> -1
                             AND woc.work_order_number = U.work_order_number
                             AND woc.company_id = U.company_id)

--DV55 (Final Query 2 20190826)
UPDATE work_order_control c
   SET c.wo_status_id = 2
 WHERE EXISTS (SELECT 1
                 FROM workflow w
                WHERE w.id_field1 = c.work_order_id
                  AND w.subsystem = 'fp_approval'
                  AND w.approval_status_id = 6
                  AND w.id_field1 = C.work_order_id)
   AND EXISTS (SELECT 1
                 FROM work_order_approval A
                WHERE A.work_order_id = c.work_order_id
                  AND A.approval_type_id = 99
                  AND A.approval_status_id = 6)
   AND c.funding_wo_indicator = 1
   AND c.wo_status_id = 1
   AND (c.work_order_number, c.company_id) IN(SELECT U.work_order_number, U.company_id
                                                FROM lgeku_project_upload U
                                               WHERE U.interface_batch IS NOT NULL
                                                 AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
                                                 AND NVL(U.status, 0) <> -1)

--DV24 (Final Query 20190826)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'The Code Class Code must be populated in order for a Funding Project to be sent to Oracle; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND NOT EXISTS (SELECT 1
                     FROM lgeku_project_upload_cc_values cc
                    WHERE cc.class_code_id = 53
                      AND cc.work_order_number = U.work_order_number
                      AND cc.company_id = U.company_id)
   AND (action = 'I'
        OR
        (
             action = 'U'
         AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 53 AND wcc.work_order_id = U.work_order_id)
        )
       )

--DV22 (Final Query 20190826)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'The Category Class Code must be populated in order for a Funding Project to be sent to Oracle; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND NOT EXISTS (SELECT 1
                     FROM lgeku_project_upload_cc_values cc
                    WHERE cc.class_code_id = 52
                      AND cc.work_order_number = U.work_order_number
                      AND cc.company_id = U.company_id)
   AND (action = 'I'
        OR
        (
             action = 'U'
         AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 52 AND wcc.work_order_id = U.work_order_id)
        )
       )

--DV11 (Final Query 20190826)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'A Plant Account must be selected for a capital funding project.  Please select a plant account from the class code tab.; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND NOT EXISTS (SELECT 1
                     FROM lgeku_project_upload_cc_values cc
                    WHERE cc.class_code_id = 46
                      AND cc.work_order_number = U.work_order_number
                      AND cc.company_id = U.company_id)
   AND (action = 'I'
        OR
        (
             action = 'U'
         AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 46 AND wcc.work_order_id = U.work_order_id)
        )
       )

--DV9 (Final Query 20190827)
UPDATE lgeku_project_upload lke_lpu
   SET error_message = error_message || 'The selected ''Code'' value is not valid based on the ''Category'' selection.  This funding project is not valid to be sent to Oracle, please update the appropriate selection.; ', status = -1
 WHERE lke_lpu.interface_batch IS NOT NULL
   AND lke_lpu.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND (lke_lpu.work_order_number, lke_lpu.company_id)
        IN(
            WITH x AS
            (SELECT work_order_number, company_id,
                   (DECODE ((SELECT COUNT (*)
                               FROM lgeku_project_upload_cc_values lcc
                              WHERE lcc.work_order_number = lpu.work_order_number
                                AND lcc.company_id = lpu.company_id
                                AND lcc.class_code_id = 52),
                            0, (SELECT VALUE
                                  FROM work_order_class_code wocc
                                 WHERE wocc.class_code_id = 52
                                   AND wocc.work_order_id = (SELECT work_order_id
                                                               FROM work_order_control woc
                                                              WHERE lpu.work_order_number = woc.work_order_number
                                                                AND lpu.company_id = woc.company_id)),
                               (SELECT class_code_value
                               FROM lgeku_project_upload_cc_values lcc
                              WHERE lcc.work_order_number = lpu.work_order_number
                                AND lcc.company_id = lpu.company_id
                                AND lcc.class_code_id = 52))) class_value52,
                   (DECODE ((SELECT COUNT (*)
                               FROM lgeku_project_upload_cc_values lcc
                              WHERE lcc.work_order_number = lpu.work_order_number
                                AND lcc.company_id = lpu.company_id
                                AND lcc.class_code_id = 53),
                            0, (SELECT VALUE
                                  FROM work_order_class_code wocc
                                 WHERE wocc.class_code_id = 53
                                   AND wocc.work_order_id = (SELECT work_order_id
                                                               FROM work_order_control woc
                                                              WHERE lpu.work_order_number = woc.work_order_number
                                                                AND lpu.company_id = woc.company_id)),
                               (SELECT class_code_value
                               FROM lgeku_project_upload_cc_values lcc
                              WHERE lcc.work_order_number = lpu.work_order_number
                                AND lcc.company_id = lpu.company_id
                                AND lcc.class_code_id = 53))) class_value53
              FROM lgeku_project_upload lpu
             WHERE lpu.interface_batch IS NOT NULL
               AND lpu.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload))
            SELECT U.work_order_number, U.company_id
              FROM lgeku_project_upload U, x
             WHERE U.interface_batch IS NOT NULL
               AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
               AND U.work_order_number = x.work_order_number
               AND U.company_id = x.company_id
               AND NVL (x.class_value53, 0) NOT IN(SELECT NVL (E.class_code, 0)
                                                     FROM eon_fp_type_class_cat_code E
                                                    WHERE E.class_category = x.class_value52))