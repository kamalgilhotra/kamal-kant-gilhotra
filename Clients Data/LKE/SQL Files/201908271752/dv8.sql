--------------------------------------------
-------------- TO BE DONE YET --------------
--------------------------------------------

SELECT DECODE (COUNT (*), 0, 0, 1)
  FROM work_order_account woa,
       work_order_class_code wocc,
       work_order_control woc,
       co_tenancy_agreement co_ten
 WHERE woa.agreemnt_id IS NOT NULL
   AND woa.work_order_id = wocc.work_order_id(+)
   AND woc.work_order_id = woa.work_order_id
   AND woc.funding_wo_indicator = 1
   AND wocc.class_code_id = 70
   AND woa.agreemnt_id = co_ten.agreemnt_id
   AND LOWER (co_ten.description) <> LOWER (wocc.VALUE)
   AND wocc.VALUE IS NOT NULL
   AND woa.work_order_id = 222;

UPDATE work_order_account woa
   SET agreemnt_id =
          (SELECT agreemnt_id
             FROM co_tenancy_agreement co_ten, work_order_class_code wocc
            WHERE     wocc.class_code_id = 70
                  AND wocc.work_order_id = woa.work_order_id
                  AND wocc.VALUE IS NOT NULL
                  AND LOWER (wocc.VALUE) = LOWER (co_ten.description))
 WHERE     EXISTS
              (SELECT 1
                 FROM co_tenancy_agreement co_ten, work_order_class_code wocc
                WHERE     wocc.class_code_id = 70
                      AND wocc.work_order_id = woa.work_order_id
                      AND wocc.VALUE IS NOT NULL
                      AND LOWER (wocc.VALUE) = LOWER (co_ten.description))
       AND woa.work_order_id = 222;

UPDATE budget bud
   SET agreemnt_id =
          (SELECT agreemnt_id
             FROM co_tenancy_agreement co_ten,
                  work_order_class_code wocc,
                  work_order_control woc
            WHERE     wocc.class_code_id = 70
                  AND wocc.work_order_id = woc.work_order_id
                  AND woc.budget_id = bud.budget_id
                  AND wocc.VALUE IS NOT NULL
                  AND LOWER (wocc.VALUE) = LOWER (co_ten.description)
                  AND wocc.work_order_id = 222)
 WHERE EXISTS
          (SELECT 1
             FROM co_tenancy_agreement co_ten,
                  work_order_class_code wocc,
                  work_order_control woc
            WHERE     wocc.class_code_id = 70
                  AND wocc.work_order_id = woc.work_order_id
                  AND woc.budget_id = bud.budget_id
                  AND wocc.VALUE IS NOT NULL
                  AND LOWER (wocc.VALUE) = LOWER (co_ten.description)
                  AND wocc.work_order_id = 222);