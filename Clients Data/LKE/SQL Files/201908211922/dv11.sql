SELECT DECODE(COUNT(*), 0, 0, 1)
FROM work_order_control woc, work_order_account woa
WHERE woc.funding_wo_indicator = 1
AND woc.work_order_id = 11978483
AND woc.worK_order_id = woa.work_order_id
AND woa.work_order_grp_id = 1
AND NOT EXISTS (SELECT 1
FROM work_order_class_code wocc
WHERE woc.work_order_id = wocc.work_order_id
AND wocc.class_code_id = 46)

'Warning: A Plant Account must be selected for a capital funding project.  Please select a plant account from the class code tab.'

SELECT * FROM work_order_control woc
WHERE funding_wo_indicator = 1
AND NOT EXISTS (SELECT 1
FROM work_order_class_code wocc
WHERE woc.work_order_id = wocc.work_order_id
AND wocc.class_code_id = 46)

SELECT woc.*
FROM work_order_control woc, work_order_account woa
WHERE woc.funding_wo_indicator = 1
AND woc.work_order_id = <arg1>
AND woc.worK_order_id = woa.work_order_id
AND woa.work_order_grp_id = 1
AND NOT EXISTS (SELECT 1
FROM work_order_class_code wocc
WHERE woc.work_order_id = wocc.work_order_id
AND wocc.class_code_id = 46)

SELECT * FROM work_order_control WHERE work_order_number = 'BTM216-1227264'

SELECT work_order_grp_id FROM work_order_account WHERE work_order_id = 11942310

SELECT * FROM work_order_control WHERE work_order_id = 12921145

SELECT interface_batch, funding_wo_indicator, budget_number, EXT_DEPARTMENT, action, status, dbms_lob.substr(error_message, 500, 1) err_msg, U.*
FROM lgeku_project_upload U WHERE work_order_number = '124561'

UPDATE lgeku_project_upload
set interface_batch = 37
WHERE work_order_number = '124561'

COMMIT

UPDATE LGEKU_PROJECT_UPLOAD U
   SET ERROR_MESSAGE = ERROR_MESSAGE || 'Please populate an asset location for this capital funding project; ', STATUS = -1
 WHERE     U.INTERFACE_BATCH IS NOT NULL
       AND (EXISTS
              (SELECT 1
                 FROM WORK_ORDER_CONTROL WOC, WORK_ORDER_ACCOUNT WOA
                WHERE     WOC.WORK_ORDER_NUMBER = U.WORK_ORDER_NUMBER
                  AND WOC.WORK_ORDER_ID = WOA.WORK_ORDER_ID
                  AND WOA.WORK_ORDER_GRP_ID = 1
                  AND WOC.FUNDING_WO_INDICATOR = 1
                  AND WOC.ASSET_LOCATION_ID IS NULL)
        OR (NOT EXISTS
              (SELECT 1
                 FROM WORK_ORDER_CONTROL
                WHERE WORK_ORDER_NUMBER = U.WORK_ORDER_NUMBER
                  AND COMPANY_ID = U.COMPANY_ID)
                  AND U.ASSET_LOCATION_ID IS NULL))