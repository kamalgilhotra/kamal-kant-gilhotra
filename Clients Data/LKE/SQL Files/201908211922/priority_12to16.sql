UPDATE lgeku_project_upload U
   SET (work_order_type_id, department_id, work_order_grp_id) = 
                        (SELECT work_order_type_id, department_id, work_order_grp_id
                           FROM work_order_control c, work_order_account A
                          WHERE c.funding_wo_indicator = 1
                            AND c.work_order_id = U.work_order_id
                            AND c.work_order_id = A.work_order_id)
 WHERE interface_batch IS NOT NULL
   AND interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND action = 'U'

UPDATE lgeku_project_upload U
   SET work_order_type_id =
          (SELECT work_order_type_id
             FROM (SELECT work_order_type_id
                     FROM (SELECT 1 seq_no, work_order_type_id
                             FROM work_order_type
                            WHERE external_work_order_type = U.ext_work_order_type
                           UNION ALL
                           SELECT 2, work_order_type_id
                             FROM work_order_type
                            WHERE description = U.ext_work_order_type
                           UNION ALL
                           SELECT 3, work_order_type_id
                             FROM work_order_type
                            WHERE TO_CHAR (work_order_type_id) = U.ext_work_order_type)
                   ORDER BY seq_no)
            WHERE ROWNUM = 1)
 WHERE U.ext_work_order_type IS NOT NULL AND interface_batch IS NOT NULL

UPDATE lgeku_project_upload U
   SET department_id =
          (SELECT department_id
             FROM (SELECT department_id
                     FROM (SELECT 1 seq_no, department_id
                             FROM department
                            WHERE external_department_code = U.ext_department
                           UNION ALL
                           SELECT 2, department_id
                             FROM department
                            WHERE description = U.ext_department
                           UNION ALL
                           SELECT 3, department_id
                             FROM department
                            WHERE TO_CHAR (department_id) = U.ext_department)
                   ORDER BY seq_no)
            WHERE ROWNUM = 1)
 WHERE U.ext_department IS NOT NULL AND interface_batch IS NOT NULL
 

UPDATE lgeku_project_upload U
   SET work_order_grp_id =
          (SELECT work_order_grp_id
             FROM (SELECT work_order_grp_id
                     FROM (SELECT 1 seq_no, work_order_grp_id
                             FROM work_order_group
                            WHERE description = U.ext_work_order_group
                           UNION ALL
                           SELECT 2, work_order_grp_id
                             FROM work_order_group
                            WHERE TO_CHAR (work_order_grp_id) = U.ext_work_order_group)
                   ORDER BY seq_no)
            WHERE ROWNUM = 1)
 WHERE U.ext_work_order_group IS NOT NULL AND u.interface_batch IS NOT NULL AND interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)

UPDATE lgeku_project_upload U
   SET work_order_grp_id = (SELECT work_order_grp_id FROM wo_grp_wo_type WHERE work_order_type_id = U.work_order_type_id)
 WHERE action = 'I' AND U.ext_work_order_group IS NULL AND interface_batch IS NOT NULL
   AND interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)


 
SELECT * FROM wo_grp_wo_type

SELECT ext_work_order_group, U.* FROM lgeku_project_upload U

SELECT ext_work_order_group, U.* FROM lgeku_project_upload_arc U ORDER BY archive_timestamp desc

SELECT ext_work_order_group, work_order_grp_id, A.* FROM wo_interface_staging_arc A WHERE action = 'I' AND row_id > 210241244 ORDER BY run_date desc