-- APPEARS TO BE SAME AS DV18 
select decode(count(*), 0,0,1)
from
(select woc.work_order_id, woc.work_order_number, woa.work_order_grp_id
from work_order_control woc
inner join work_order_account woa on woa.work_order_id = woc.work_order_id
where woa.work_order_grp_id = 2
and woc.funding_wo_indicator = 1
and woc.work_order_id = <arg1>
and substr((select wocc.value from work_order_class_code wocc where wocc.class_code_id = 70
and wocc.work_order_id = woc.work_order_id), 1, 3) = 'CAP'
and exists
(select wocc.work_order_id, wocc.value
from work_order_class_code wocc
where wocc.work_order_id = woc.work_order_id
and wocc.class_code_id = 70
and wocc.value is not null))

'''CAP'' Allocation Rules are reserved for Capital Source projects.'

SELECT woc.work_order_id, woc.work_order_number, woa.work_order_grp_id
FROM work_order_control woc
INNER JOIN work_order_account woa on woa.work_order_id = woc.work_order_id
AND woa.work_order_grp_id = 2
AND woc.funding_wo_indicator = 1
WHERE woc.work_order_id = 90550994
--AND woc.company_id = u.company_id
AND SubStr((SELECT wocc.value FROM work_order_class_code wocc WHERE wocc.class_code_id = 70 AND wocc.work_order_id = woc.work_order_id), 1, 3) = 'CAP'
AND EXISTS(SELECT wocc.work_order_id, wocc.value
             FROM work_order_class_code wocc
            WHERE wocc.work_order_id = woc.work_order_id
              AND wocc.class_code_id = 70
              AND wocc.value IS NOT NULL)

select * from work_order_control woc
INNER JOIN work_order_account woa on woa.work_order_id = woc.work_order_id
AND woa.work_order_grp_id = 2
AND woc.funding_wo_indicator = 1
and woc.work_order_id in(select wocc.work_order_id
             FROM work_order_class_code wocc
            WHERE wocc.work_order_id = woc.work_order_id
              AND wocc.class_code_id = 70
              AND wocc.value like 'CAP%')

select * from work_order_group

select * from work_order_control where work_order_id in(
select work_order_id from work_order_account where work_order_grp_id = 2)
and funding_wo_indicator = 1

and work_order_id in(select work_order_id from work_order_class_code where work_order_id = 11943850 and class_code_id = 70)

select * from work_order_class_code where work_order_id = 11943850 and class_code_id = 70

select * from class_code where class_code_id = 70

select interface_batch, funding_wo_indicator, budget_number, EXT_DEPARTMENT, action, status, dbms_lob.substr(error_message, 1000, 1) err_msg, u.*
from lgeku_project_upload u

insert into lgeku_project_upload (company_id, work_order_number, ext_work_order_type, ext_major_location, description, ext_department, est_start_date,
est_complete_date, est_in_service_date, interface_batch)
select company_id, work_order_number, (select description from work_order_type x where work_order_type_id = z.work_order_type_id) ext_work_order_type,
       (select description from major_location x where major_location_id = z.major_location_id) ext_major_location,
       substr(to_char(sysdate, 'yyyymmddhhmmss - ') || description, 1, length(description) - 17) description,
       (select description from department x where department_id = z.department_id) ext_department,
       sysdate est_start_date, sysdate est_complete_date, sysdate est_in_service_date, 37 interface_batch
from  work_order_control z where work_order_id = 90550994

select * from work_order_control where work_order_number = '151893'

commit

rollback

UPDATE lgeku_project_upload u
SET error_message = error_message || '''CAP'' Allocation Rules are reserved for Capital Source projects.; ', status = -1
WHERE EXISTS(select 1
FROM work_order_control woc
INNER JOIN work_order_account woa ON woa.work_order_id = woc.work_order_id
AND woa.work_order_grp_id = 2
AND woc.funding_wo_indicator = 1
WHERE woc.work_order_number = u.work_order_number
AND woc.company_id = u.company_id
AND SubStr((SELECT wocc.value FROM work_order_class_code wocc WHERE wocc.class_code_id = 70
AND wocc.work_order_id = woc.work_order_id), 1, 3) = 'CAP'
AND EXISTS
(SELECT wocc.work_order_id, wocc.value
FROM work_order_class_code wocc
WHERE wocc.work_order_id = woc.work_order_id
AND wocc.class_code_id = 70
AND wocc.value is not null)
)
AND u.interface_batch IS NOT NULL