UPDATE budget bud
   SET budget_summary_id =
          (SELECT budget_summary_id
             FROM work_order_type_budget_summary wot_bs,
                  work_order_type wot,
                  work_order_control woc
            WHERE     wot_bs.work_order_type_id = wot.work_order_type_id
                  AND woc.work_order_type_id = wot.work_order_type_id
                  AND woc.work_order_id = 11883800
                  AND woc.funding_wo_indicator = 1)
 WHERE EXISTS
          (SELECT budget_summary_id
             FROM work_order_type_budget_summary wot_bs,
                  work_order_type wot,
                  work_order_control woc
            WHERE     wot_bs.work_order_type_id = wot.work_order_type_id
                  AND woc.work_order_type_id = wot.work_order_type_id
                  AND woc.work_order_id = 11883800
                  AND woc.funding_wo_indicator = 1)

SELECT DECODE (COUNT (*), 0, 0, 1)
  FROM work_order_control woc
 WHERE     funding_wo_indicator = 1
       AND work_order_id = 11883800
       AND EXISTS
              (SELECT 1
                 FROM budget bud,
                      work_order_type wot,
                      work_order_type_budget_summary wot_bs
                WHERE bud.budget_id = woc.budget_id
                  AND bud.budget_number = woc.work_order_number
                  AND woc.work_order_Type_id = wot.work_order_type_id
                  AND wot.work_order_type_id = wot_bs.work_order_type_id
                  AND NVL (wot_bs.budget_summary_id, -1) <> NVL (bud.budget_summary_id, -1))