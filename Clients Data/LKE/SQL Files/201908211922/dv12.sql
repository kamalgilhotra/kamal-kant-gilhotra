SELECT DECODE(COUNT(*), 0, 0, 1)
FROM work_order_control woc,
work_order_account woa
WHERE woc.work_order_id = <arg1>
AND woc.funding_wo_indicator = 1
AND woc.work_order_id = woa.work_order_id
AND woa.work_order_grp_id = 1
AND (woc.asset_location_id IS NULL)

'Please populate an asset location for this capital funding project'

SELECT * FROM work_order_control woc
WHERE funding_wo_indicator = 1
AND NOT EXISTS (SELECT 1
FROM work_order_class_code wocc
WHERE woc.work_order_id = wocc.work_order_id
AND wocc.class_code_id = 46)

SELECT 1
FROM work_order_control woc, work_order_account woa
WHERE woc.funding_wo_indicator = 1
AND woc.work_order_id = <arg1>
AND woc.worK_order_id = woa.work_order_id
AND woa.work_order_grp_id = 1
AND NOT EXISTS (SELECT 1
FROM work_order_class_code wocc
WHERE woc.work_order_id = wocc.work_order_id
AND wocc.class_code_id = 46)

SELECT asset_location_id, w.* FROM work_order_control w WHERE work_order_number = '124561'

SELECT work_order_grp_id FROM work_order_account WHERE work_order_id = 11942310

SELECT asset_location_id. w.* FROM work_order_control w WHERE work_order_id = 11978483

SELECT interface_batch, funding_wo_indicator, budget_number, EXT_DEPARTMENT, action, status, dbms_lob.substr(error_message, 500, 1) err_msg, U.*
FROM lgeku_project_upload U WHERE work_order_number = '124301'

SELECT interface_batch, funding_wo_indicator, budget_number, EXT_DEPARTMENT, action, status, dbms_lob.substr(error_message, 1000, 1) err_msg, U.*
FROM lgeku_project_upload U

UPDATE lgeku_project_upload
set interface_batch = 37
WHERE work_order_number = '124301'

COMMIT

ROLLBACK

-- Final Query
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'Please populate an asset location for this capital funding project; ', status = -1
 WHERE     U.interface_batch IS NOT NULL
       AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
       AND U.work_order_grp_id = 1
       AND U.ext_asset_location IS NULL
       AND (
            U.action = 'I'
            OR
            (
                 U.action = 'U'
             AND EXISTS
                   (SELECT 1
                      FROM work_order_control woc
                     WHERE woc.work_order_id = U.work_order_id
                       AND woc.asset_location_id IS NULL)
            )
           )