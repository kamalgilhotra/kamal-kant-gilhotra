UPDATE lgeku_project_upload U
   SET work_order_type_id =
          (SELECT work_order_type_id
             FROM (  SELECT work_order_type_id
                       FROM (SELECT 1 seq_no, work_order_type_id
                               FROM work_order_type
                              WHERE external_work_order_type = U.ext_work_order_type
                             UNION ALL
                             SELECT 2, work_order_type_id
                               FROM work_order_type
                              WHERE description = U.ext_work_order_type
                             UNION ALL
                             SELECT 3, work_order_type_id
                               FROM work_order_type
                              WHERE TO_CHAR (work_order_type_id) = U.ext_work_order_type)
                   ORDER BY seq_no)
            WHERE ROWNUM = 1)
 WHERE U.ext_work_order_type IS NOT NULL AND interface_batch IS NOT NULL