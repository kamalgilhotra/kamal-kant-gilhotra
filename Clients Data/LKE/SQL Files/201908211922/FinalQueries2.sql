-- DV6 (Final 20190815)
UPDATE LGEKU_PROJECT_UPLOAD U
   SET ERROR_MESSAGE = ERROR_MESSAGE || 'Please ensure that Product, Description, and Detailed Description fields are all populated before updating a capital project; ', STATUS = -1
 WHERE     U.INTERFACE_BATCH IS NOT NULL
       AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
       AND U.WORK_ORDER_GRP_ID = 1
       AND (
            (    U.action = 'U'
             AND EXISTS (SELECT 1
                           FROM WORK_ORDER_CONTROL WOC
                          WHERE WOC.WORK_ORDER_ID = U.WORK_ORDER_ID
                            AND (   (WOC.LONG_DESCRIPTION IS NULL AND U.LONG_DESCRIPTION IS NULL)
                                 OR (WOC.REASON_CD_ID IS NULL AND U.REASON_CD_ID IS NULL)
                                 OR (WOC.NOTES IS NULL AND U.NOTES IS NULL)))
            )
            OR
            (    U.action ='I'
             AND (U.LONG_DESCRIPTION IS NULL OR U.REASON_CD_ID IS NULL OR U.NOTES IS NULL)
            )
           )

-- DV12 (Final 20190819)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'Please populate an asset location for this capital funding project; ', status = -1
 WHERE     U.interface_batch IS NOT NULL
       AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
       AND U.work_order_grp_id = 1
       AND U.ext_asset_location IS NULL
       AND (
            U.action = 'I'
            OR
            (
                 U.action = 'U'
             AND EXISTS
                   (SELECT 1
                      FROM work_order_control woc
                     WHERE woc.work_order_id = U.work_order_id
                       AND woc.asset_location_id IS NULL)
            )
           )


-- DV17 (Final 20190819)
UPDATE lgeku_project_upload U
SET    error_message = error_message || 'There is an Allocation Rule defined, but no Target Projects have been entered.; ', status = -1
WHERE  U.interface_batch IS NOT NULL
  AND  U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
  AND  (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) IS NOT NULL
  AND  (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id AND wocc.class_code_id = 71) IS NULL
           
-- DV18 (Final 20190819)
UPDATE lgeku_project_upload U
   SET error_message = error_message || '''CAP'' Allocation Rules are reserved for Capital Source Projects; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 2
   AND EXISTS (SELECT 1
                 FROM lgeku_project_upload_cc_values wocc
                WHERE wocc.class_code_id = 70
                  AND wocc.work_order_number = U.work_order_number
                  AND wocc.company_id = U.company_id
                  AND wocc.class_code_value LIKE 'CAP%')

--DV20 (Final 20190819)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'Allocation Rules on Capital Source Projects have to start with ''CAP''.; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND EXISTS (SELECT 1
                 FROM lgeku_project_upload_cc_values wocc
                WHERE wocc.class_code_id = 70
                  AND wocc.work_order_number = U.work_order_number
                  AND wocc.company_id = U.company_id
                  AND wocc.class_code_value IS NOT NULL)
   AND EXISTS (SELECT 1
                 FROM lgeku_project_upload_cc_values wocc
                WHERE wocc.class_code_id = 70
                  AND wocc.work_order_number = U.work_order_number
                  AND wocc.company_id = U.company_id
                  AND wocc.class_code_value NOT LIKE 'CAP%')
       
--DV27 (Final 20190820)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'Warning: A budget coordinator must be selected for a capital funding project.  Please select a budget coordinator from the accounts tab.; ', status = -1
 WHERE U.interface_batch IS NOT NULL AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND (
        (    U.action = 'U'
         AND U.other_contact IS NULL
         AND U.work_order_grp_id = 1
         AND EXISTS (SELECT 1 FROM work_order_initiator i WHERE i.work_order_id = U.work_order_id AND i.other IS NULL)
        )
        OR
        (    action = 'I'
         AND U.other_contact IS NULL
         AND U.work_order_grp_id = 1
        )
       )

--DV28 (Final Query 20190820)
UPDATE lgeku_project_upload U
SET    error_message = error_message || 'There is a Target Project entered, but no valid Allocation Rule has been entered.; ', status = -1
WHERE  U.interface_batch IS NOT NULL AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id AND wocc.class_code_id = 71) IS NOT NULL
   AND (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) IS NULL
           
-- DV29 (Final Query 20190820)
UPDATE lgeku_project_upload U
SET    error_message = error_message || 'No Target Numbers are allowed when there is an Allocation Rule defined.; ', status = -1
WHERE  U.interface_batch IS NOT NULL AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND (
        (   (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) IS NOT NULL
           AND (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 77) IS NOT NULL)
        OR
        (   (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 70) IS NOT NULL
           AND U.action = 'U'
           AND EXISTS(SELECT 1
                        FROM work_order_class_code
                       WHERE work_order_id = U.work_order_id
                         AND class_code_id = 77
                         AND VALUE IS NOT NULL))
        OR
        (   (SELECT wocc.class_code_value
                  FROM lgeku_project_upload_cc_values wocc
                 WHERE wocc.work_order_number = U.work_order_number
                   AND wocc.company_id = U.company_id AND wocc.class_code_id = 77) IS NOT NULL
           AND U.action = 'U'
           AND EXISTS(SELECT 1
                        FROM work_order_class_code
                       WHERE work_order_id = U.work_order_id
                         AND class_code_id = 70
                         AND VALUE IS NOT NULL))
       )
       
--DV41 (Final Query 20190820)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'The RAC Category value is required for Funding Projects.; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND NOT EXISTS (SELECT 1
                     FROM lgeku_project_upload_cc_values cc
                    WHERE cc.class_code_id = 54
                      AND cc.work_order_number = U.work_order_number
                      AND cc.company_id = U.company_id)
   AND (action = 'I'
        OR
        (
             action = 'U'
         AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 54 AND wcc.work_order_id = U.work_order_id)
        )
       )
    
--DV42 (Final Query 20190820)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'The FP Reporting Group value is required for all Funding Projects.; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND NOT EXISTS (SELECT 1
                     FROM lgeku_project_upload_cc_values cc
                    WHERE cc.class_code_id = 55
                      AND cc.work_order_number = U.work_order_number
                      AND cc.company_id = U.company_id)
   AND (action = 'I'
        OR
        (
             action = 'U'
         AND NOT EXISTS (SELECT 1 FROM work_order_class_code wcc WHERE wcc.class_code_id = 55 AND wcc.work_order_id = U.work_order_id)
        )
       )
    
--DV43 (Final Query 20190821)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'A Code of "ECR Approved Plan" is designated for this funding project. Please designate an ECR Plan Number class code value for this funding project.; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND U.work_order_grp_id = 1
   AND (SELECT wocc.class_code_value
          FROM lgeku_project_upload_cc_values wocc
         WHERE wocc.work_order_number = U.work_order_number
           AND wocc.company_id = U.company_id
           AND wocc.class_code_id = 53) = 'ECR APPROVED PLAN'
   AND (
        (    action = 'U'
         AND NOT EXISTS(SELECT 1
                          FROM lgeku_project_upload_cc_values wocc
                         WHERE wocc.work_order_number = U.work_order_number
                           AND wocc.company_id = U.company_id
                           AND wocc.class_code_id = 58)
         AND NOT EXISTS(SELECT 1
                          FROM work_order_class_code wocc
                         WHERE wocc.work_order_id = U.work_order_id
                           AND wocc.class_code_id = 58)
        )
        OR
        (    action = 'I'
         AND NOT EXISTS(SELECT 1
                          FROM lgeku_project_upload_cc_values wocc
                         WHERE wocc.work_order_number = U.work_order_number
                           AND wocc.company_id = U.company_id
                           AND wocc.class_code_id = 58)
        )
       )
                          
--DV60 (Final Query 20190821)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'The Funding Project Department is not a valid Project Owning Organization (in the HR All Organization Units table in Oracle Projects). Please select  another Department or contact the Budgeting Group.; ', status = -1
 WHERE NOT EXISTS
           (SELECT dp.description
              FROM department dp,
                   division dv,
                   hr_all_organization_units hr,
                   hr.hr_organization_information@orafin hi,
                   pa_segment_value_lookup_sets pvls,
                   pa_segment_value_lookups pvl
             WHERE hi.organization_id = hr.organization_id
               AND hr.attribute3 = pvl.segment_value
               AND pvls.segment_value_lookup_set_id = pvl.segment_value_lookup_set_id
               AND pvls.segment_value_lookup_set_name = 'Proj Org to GL Responsible Ctr'
               AND dp.external_department_code = hr.attribute3
               AND dp.division_id = dv.division_id
               AND hr.TYPE NOT IN ('CORP', 'INV', 'SBG')
               AND hi.org_information1 = 'PA_PROJECT_ORG'
               AND hi.org_information2 = 'Y'
               AND hr.date_to IS NULL
               AND dv.company_id = DECODE (TO_NUMBER (hr.attribute1), 700, 100, 702, 800, 703, 301, 704, 4, 710, 110, 720, 20, TO_NUMBER (hr.attribute1))
               AND dp.description = U.ext_department)
   AND ((action = 'U' AND U.ext_department IS NOT NULL) OR action = 'I')
   AND U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)

--DV62 (Final Query 20190821)
UPDATE lgeku_project_upload U
   SET error_message = error_message || 'The Organization for this Funding Project is not valid for the Company on the designated Work Order Type.  Please select another Organization.; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND EXISTS (SELECT 1
                 FROM work_order_type T, department d, division dv
                WHERE dv.division_id = d.division_id
                  AND work_order_type_id = U.work_order_type_id
                  AND department_id = U.department_id
                  AND dv.company_id <> T.company_id)