SELECT notes, reason_cd_id, long_description, woc.*, woa.* FROM work_order_control woc, work_order_account woa
WHERE woc.work_order_id = woa.work_order_id
AND   woc.funding_wo_indicator = 1
AND woa.work_order_grp_id = 1
AND woc.wo_status_id = 1
AND NOT (notes IS NULL OR reason_cd_id IS NULL OR long_description IS NULL)
AND woc.work_order_id NOT IN(414529909, 407115103, 414523048, 18386202, 13637900, 11884833, 18129385, 11978480, 12921144,186756596,414529979,11939167,18380025,11978484,414529983,414529977)

--R1
11978480 -- None null (ignored as WO STATUS ID was 7.
414529909 -- All null
407115103 -- long description null
414523048 -- notes null
18386202 -- reason_cd_id null
13637900 -- notes and reason_cd_id null
11884833 -- notes and long desription null
18129385 -- reason_cd_id and long desription null
-- R2
12921144 -- None null (ignored as WO STATUS ID was 7. done
186756596 -- All null done
414529979 -- long description null done
11939167 -- notes null done
18380025 -- reason_cd_id null done
11978484 -- notes and reason_cd_id null done
414529983 -- notes and long desription null done
414529977 -- reason_cd_id and long desription null done

--R3
12917573 -- None null
414523243 -- All null
414530101 -- long description null
414530098 -- notes null
414530031 -- reason_cd_id null
11980386 -- notes and reason_cd_id null
186810343 -- notes and long desription null
18380022 -- reason_cd_id and long desription null


UPDATE work_order_control set long_description = NULL WHERE work_order_id = 11884833 --Points of Interest Earlington
UPDATE work_order_control set long_description = NULL WHERE work_order_id = 18129385 --LGE BACKYARD MACHINE
UPDATE work_order_control set long_description = NULL WHERE work_order_id = 186810343 -- 144869-PRESTON CITY GATE STATION
UPDATE work_order_control set long_description = NULL WHERE work_order_id = 18380022 --LTPPR13LG-PR13 C Inspection

INSERT INTO lgeku_project_upload (company_id, work_order_number, ext_work_order_type, ext_major_location, description, ext_department, est_start_date,
est_complete_date, est_in_service_date, notes, reason_cd_id, long_description)
SELECT company_id, work_order_number, (SELECT description FROM work_order_type x WHERE work_order_type_id = z.work_order_type_id) ext_work_order_type,
       (SELECT description FROM major_location x WHERE major_location_id = z.major_location_id) ext_major_location,
       SUBSTR(TO_CHAR(SYSDATE, 'yyyymmddhhmmss - ') || description, 1, 30) description,
       (SELECT description FROM department x WHERE department_id = z.department_id) ext_department,
       SYSDATE est_start_date, SYSDATE est_complete_date, SYSDATE est_in_service_date, notes, reason_cd_id, long_description
FROM  work_order_control z WHERE work_order_id IN(12917573,414523243,414530101,414530098,414530031,11980386,186810343,18380022)
--FROM  work_order_control z WHERE work_order_id IN(12921144,186756596,414529979,11939167,18380025,11978484,414529983,414529977)
--FROM  work_order_control z WHERE work_order_id IN(414529909, 407115103, 414523048, 18386202, 13637900, 11884833, 18129385) -- 11978480
UNION
SELECT company_id, SUBSTR('K' || work_order_number, 1, 9) work_order_number,
       (SELECT description FROM work_order_type x WHERE work_order_type_id = z.work_order_type_id) ext_work_order_type,
       (SELECT description FROM major_location x WHERE major_location_id = z.major_location_id) ext_major_location,
       SUBSTR(TO_CHAR(SYSDATE, 'yyyymmddhhmmss - ') || description, 1, 30) description,
       (SELECT description FROM department x WHERE department_id = z.department_id) ext_department,
       SYSDATE est_start_date, SYSDATE est_complete_date, SYSDATE est_in_service_date, notes, reason_cd_id, long_description
FROM  work_order_control z WHERE work_order_id IN(12917573,414523243,414530101,414530098,414530031,11980386,186810343,18380022)
--FROM  work_order_control z WHERE work_order_id IN(12921144,186756596,414529979,11939167,18380025,11978484,414529983,414529977)
--FROM  work_order_control z WHERE work_order_id IN(414529909, 407115103, 414523048, 18386202, 13637900, 11884833, 18129385) -- 11978480

COMMIT

SELECT work_order_id, wo_status_id
FROM  work_order_control z WHERE work_order_id IN(12921144,186756596,414529979,11939167,18380025,11978484,414529983,414529977)

DELETE lgeku_project_upload

DELETE wo_interface_staging

INSERT INTO lgeku_project_upload (company_id, work_order_number, ext_work_order_type, ext_major_location, description, ext_department, est_start_date,
est_complete_date, est_in_service_date, notes, reason_cd_id, long_description,
ext_asset_location, other_contact)
SELECT company_id, work_order_number, NULL ext_work_order_type,
       NULL ext_major_location,
       SUBSTR(TO_CHAR(SYSDATE, 'yyyymmddhhmmss - ') || description, 1, 30) description,
       NULL ext_department,
       SYSDATE est_start_date, SYSDATE est_complete_date, SYSDATE est_in_service_date, notes, reason_cd_id, long_description,
       NULL ext_asset_location, NULL other_contact
FROM  work_order_control z WHERE wo_status_id = 1 AND work_order_id = 144355883
UNION
SELECT company_id, SUBSTR('B' || work_order_number, 1, 9) work_order_number,
       (SELECT description FROM work_order_type x WHERE work_order_type_id = z.work_order_type_id)  ext_work_order_type,
       (SELECT description FROM major_location x WHERE major_location_id = z.major_location_id) ext_major_location,
       SUBSTR(TO_CHAR(SYSDATE, 'yyyymmddhhmmss - ') || description, 1, 30) description,
       (SELECT description FROM department x WHERE department_id = z.department_id) ext_department,
       SYSDATE est_start_date, SYSDATE est_complete_date, SYSDATE est_in_service_date, notes, reason_cd_id, long_description,
       (SELECT long_description FROM asset_location WHERE asset_location_id = z.asset_location_id) ext_asset_location,
       (SELECT other FROM work_order_initiator WHERE work_order_id = z.work_order_id)
FROM  work_order_control z WHERE wo_status_id = 1 AND work_order_id = 144355883

UPDATE lgeku_project_upload
set class_code2 = 'RAC Category', class_value2 = '2 Not Required but high priority in next 0-3 years', class_code1 = 'FP Reporting Group', class_value1 = 'Capital-IT'
WHERE WORK_ORDER_NUMBER LIKE 'F%'

SELECT * FROM class_code WHERE class_code_id IN(70, 71)

UPDATE lgeku_project_upload
set class_code2 = 'Code', class_value2 = 'ECR APPROVED PLAN', class_code1 = 'ECR Plan Number', class_value1 = 'LGE30 16PLAN'
WHERE WORK_ORDER_NUMBER = '140SER16'

UPDATE lgeku_project_upload
set ext_department = '026492-SER IT CHARGES'
WHERE WORK_ORDER_NUMBER = '140SER16'

edit work_order_class_code
WHERE work_order_id IN(SELECT work_order_id FROM work_order_control WHERE work_order_number LIKE '%140SER16%' OR work_order_number LIKE '%KS140SER%' OR work_order_number LIKE '%??140SER%')
AND class_code_id = 58

UPDATE lgeku_project_upload
set ext_department = '026492-SER IT CHARGES'
WHERE WORK_ORDER_NUMBER = 'U140SER16'

SELECT * FROM work_order_control WHERE work_order_number LIKE '%140SER16%' OR work_order_number LIKE '%KS140SER%' OR work_order_number LIKE '%??140SER%'

SELECT class_code1, class_value1, class_code2, class_value2, U.* FROM lgeku_project_upload U WHERE work_order_number IN('K124344', '133681LGE') --LIKE '%681LG%'

SELECT * FROM work_order_control WHERE work_order_number IN('K124344', '133681LGE') AND funding_wo_indicator = 1

SELECT * FROM work_order_control WHERE work_order_id = 144355883

SELECT * FROM work_order_class_code WHERE work_order_id IN(144355883) AND class_code_id IN(70, 71, 77)

SELECT * FROM lgeku_project_upload_arc ORDER BY archive_timestamp desc

SELECT * FROM asset_location

SELECT (SELECT long_description FROM asset_location WHERE asset_location_id = z.asset_location_id) asset_location_id, z.* FROM  work_order_control z WHERE work_order_id = 144355883