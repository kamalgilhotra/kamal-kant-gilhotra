UPDATE lgeku_project_upload U
   SET error_message = error_message || 'The Organization for this Funding Project is not valid for the Company on the designated Work Order Type.  Please select another Organization.; ', status = -1
 WHERE U.interface_batch IS NOT NULL
   AND U.interface_batch = (SELECT MAX(interface_batch) FROM lgeku_project_upload)
   AND EXISTS (SELECT 1
                 FROM work_order_type T, department d, division dv
                WHERE dv.division_id = d.division_id
                  AND work_order_type_id = U.work_order_type_id
                  AND department_id = U.department_id
                  AND dv.company_id <> T.company_id)