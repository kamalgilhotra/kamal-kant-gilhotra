select * from department dp, division dv, hr_all_organization_units hr, hr.hr_organization_information@orafin hi,
pa_segment_value_lookup_sets pvls, pa_segment_value_lookups pvl
where hi.organization_id = hr.organization_id
and   hr.attribute3 = pvl.segment_value
and   pvls.segment_value_lookup_set_id = pvl.segment_value_lookup_set_id
and   pvls.segment_value_lookup_set_name = 'Proj Org to GL Responsible Ctr'
and dp.external_department_code = hr.attribute3
and dp.division_id = dv.division_id
and hr.type not in('CORP', 'INV', 'SBG')
and hi.org_information1 = 'PA_PROJECT_ORG'
and hi.org_information2 = 'Y'
and hr.date_to is null
and dv.company_id = decode(to_number(hr.attribute1), 700, 100, 702, 800, 703, 301, 704, 4, 710, 110, 720, 20, to_number(hr.attribute1))
and dp.department_id = 1756

insert into lgeku_project_upload (company_id, work_order_number, ext_work_order_type, ext_major_location, description, ext_department, est_start_date,
est_complete_date, est_in_service_date)
values(110, 'KAMAL8', 'INDIRECT KU Operating Services', 'Common General Plant-Office Complex', 'test2', '008523-TRIMBLE COUNTY CHARGES FROM ', sysdate,
sysdate, sysdate)

select interface_batch, funding_wo_indicator, budget_number, EXT_DEPARTMENT, action, status, dbms_lob.substr(error_message, 500, 1) err_msg, u.* from lgeku_project_upload u

delete from lgeku_project_upload

select funding_wo_indicator, budget_number, EXT_DEPARTMENT, action, status, dbms_lob.substr(error_message, 500, 1) err_msg, s.* from wo_interface_staging s

delete from wo_interface_staging

select interface_batch, funding_wo_indicator, budget_number, EXT_DEPARTMENT, action, status, dbms_lob.substr(error_message, 500, 1) err_msg, a.* from lgeku_project_upload_arc a
where interface_batch > 25 order by archive_timestamp desc

select department_id, w.* from work_order_control w where work_order_id > 414523242 order by work_order_id desc

select * from department where description = '008523-TRIMBLE COUNTY CHARGES FROM '

select cast (substr(error_message, 1, 500) as varchar2(500)) as err_msg from lgeku_project_upload

select dbms_lob.substr(error_message, 500, 1) err_msg from lgeku_project_upload

commit

select * from pp_processes_messages
where occurrence_id = 353399
order by 2 desc, 6 desc

select * from pp_processes_messages
where process_id = 286 and time_stamp > trunc(sysdate - 1) and occurrence_id > 353390
and msg like '%Finished%Company I%'
order by 2 desc, 6 desc

select * from company where description like '1%' order by company_id

select department_id, w.* from work_order_control w where funding_wo_indicator = 1 and work_order_id = 414446261 order by work_order_id desc

select * from work_order_type where work_order_type_id = 18370240

select * from major_location where description like 'Common Gene%'

select * from department where description like '018890%'

select * from work_order_control where work_order_id = 414523345

/*
The Funding Project Department is not a valid Project Owning Organization (in the HR All Organization Units table in Oracle Projects). Please select  another Department or contact the Budgeting Group.
*/

commit

delete from lgeku_project_upload where work_order_number = 'RDDD366OH'

insert into lgeku_project_upload (company_id, work_order_number, ext_work_order_type, ext_major_location, description, ext_department, est_start_date,
est_complete_date, est_in_service_date, interface_batch)
values(20, '151893', 'KU Steam Task Level', 'Generation - Steam -  Brown', 'testing update', '016220-E W BROWN - SUPT AND ADMIN',
sysdate, sysdate, sysdate, 37)


select description from work_order_type x where work_order_type_id = 1077

select description from major_location x where major_location_id = 9225499

select description, x.* from department x where department_id = 45976114

select work_order_type_id, major_location_id, department_id, description, company_id,
       est_start_date, est_complete_date, est_in_service_date, trunc(sysdate - 3511), w.*
from work_order_control w where work_order_id = 182401249
-- TC CT KU PURCH LUBOIL VARN

insert into lgeku_project_upload (company_id, work_order_number, ext_work_order_type, ext_major_location, description, ext_department, est_start_date,
est_complete_date, est_in_service_date, interface_batch)
select company_id, work_order_number, (select description from work_order_type x where work_order_type_id = z.work_order_type_id) ext_work_order_type,
       (select description from major_location x where major_location_id = z.major_location_id) ext_major_location,
       'testing update' description,
       (select description from department x where department_id = z.department_id) ext_department,
       sysdate est_start_date, sysdate est_complete_date, sysdate est_in_service_date, 37 interface_batch
from  work_order_control z where work_order_id = 182401249