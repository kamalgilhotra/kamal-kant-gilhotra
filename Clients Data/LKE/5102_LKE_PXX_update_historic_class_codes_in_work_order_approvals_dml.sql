/*
||==============================================================================================================================
|| Application: PowerPlant
|| File Name:   5102_LKE_PXX_update_historic_class_codes_in_work_order_approvals_dml.sql
||==============================================================================================================================
|| Copyright (C) 2019 by PowerPlan Consultants, Inc. All Rights Reserved.
||==============================================================================================================================
|| Version    Date       Revised By     		Reason for Change
|| ---------- ---------- -------------------	--------------------------------------------------------------------------------
|| 2018.2.1.0 07/30/2019 Kamal Kant Gilhotra	Updating the historic values for class codes in Work Order Approval table
||==============================================================================================================================
*/
UPDATE work_order_approval woa
SET    (woa.user_text13, woa.user_text14, woa.user_text15) = 
       (SELECT cc_data.user_text13, cc_data.user_text14, cc_data.user_text15
        FROM   (SELECT work_order_id, mx_revision, user_text13, user_text14, user_text15
                FROM  (SELECT work_order_id,
                              (SELECT MAX(revision) FROM budget_version_fund_proj bvfp WHERE budget_version_id IN (SELECT budget_version_id FROM budget_version WHERE current_version = 1) AND work_order_id = woc.work_order_id) mx_revision,
                              (SELECT wcc.value FROM class_code cc, work_order_class_code wcc WHERE cc.class_code_id = wcc.class_code_id (+) AND wcc.work_order_id = woc.work_order_id AND description = 'Category') user_text13,
                              (SELECT wcc.value FROM class_code cc, work_order_class_code wcc WHERE cc.class_code_id = wcc.class_code_id (+) AND wcc.work_order_id = woc.work_order_id AND description = 'Code') user_text14,
                              (SELECT wcc.value FROM class_code cc, work_order_class_code wcc WHERE cc.class_code_id = wcc.class_code_id (+) AND wcc.work_order_id = woc.work_order_id AND description = 'RAC Category') user_text15
                       FROM   work_order_control woc
                       WHERE  funding_wo_indicator = 1)
                WHERE mx_revision IS NOT NULL) cc_data
        WHERE woa.work_order_id = cc_data.work_order_id AND woa.revision = cc_data.mx_revision)
WHERE  (woa.work_order_id, woa.revision) IN(SELECT work_order_id, mx_revision
                                            FROM (SELECT * FROM
                                                       (SELECT work_order_id,
                                                              (SELECT MAX(revision) FROM budget_version_fund_proj bvfp WHERE budget_version_id IN (SELECT budget_version_id FROM budget_version WHERE current_version = 1) AND work_order_id = woc.work_order_id) mx_revision,
                                                              (SELECT wcc.value FROM class_code cc, work_order_class_code wcc WHERE cc.class_code_id = wcc.class_code_id (+) AND wcc.work_order_id = woc.work_order_id AND description = 'Category') user_text13,
                                                              (SELECT wcc.value FROM class_code cc, work_order_class_code wcc WHERE cc.class_code_id = wcc.class_code_id (+) AND wcc.work_order_id = woc.work_order_id AND description = 'Code') user_text14,
                                                              (SELECT wcc.value FROM class_code cc, work_order_class_code wcc WHERE cc.class_code_id = wcc.class_code_id (+) AND wcc.work_order_id = woc.work_order_id AND description = 'RAC Category') user_text15
                                                       FROM   work_order_control woc
                                                       WHERE  funding_wo_indicator = 1)
                                                  WHERE user_text13 IS NOT NULL OR user_text14 IS NOT NULL OR user_text15 IS NOT NULL)
                                            WHERE mx_revision IS NOT NULL)
;

COMMIT;


/*
|| Log the run of the script
*/
insert into PP_UPDATE_FLEX
                (COL_ID,
                 TYPE_NAME,
                 FLEXVCHAR1)
select NVL(MAX(COL_ID), 0) + 1,
                 'CustomScript',
                '5102_LKE_PXX_update_historic_class_codes_in_work_order_approvals_dml.sql' --this should be your script name
from PP_UPDATE_FLEX;
commit;
