set main_dir=%cd%
@echo off
FOR /f "tokens=2 delims==" %%G in ('wmic os get localdatetime /value') do set datetime=%%G
 set bkp_folder=%datetime:~0,14%
@echo on
IF EXIST Deliverable REN %main_dir%\Deliverable Deliverable-%bkp_folder%
IF %errorlevel% NEQ 0 echo ******ERROR CAME****** & goto error_det
IF EXIST SupportingFiles REN %main_dir%\SupportingFiles SupportingFiles-%bkp_folder%
IF %errorlevel% NEQ 0 echo ******ERROR CAME****** & goto error_det

mkdir %main_dir%\Deliverable\bin
mkdir %main_dir%\Deliverable\src
mkdir %main_dir%\SupportingFiles\src
mkdir %main_dir%\SupportingFiles\logs
echo test

REM FOR /F %%i IN (%main_dir%\base_pbls.txt) DO move /Y %main_dir%\Application\src\%%i %main_dir%\SupportingFiles\src & IF NOT EXIST %main_dir%\SupportingFiles\src\%%i @echo ******ERROR CAME****** & goto error_det

REM MOVE /Y %main_dir%\Application\src\*.pbl %main_dir%\Deliverable\src || IF %errorlevel% NEQ 0 @echo ******ERROR CAME****** & goto error_det

REM MOVE /Y %main_dir%\Application\bin\*.* %main_dir%\Deliverable\bin || IF %errorlevel% NEQ 0 @echo ******ERROR CAME****** & goto error_det

robocopy %main_dir%\Application\logs %main_dir%\SupportingFiles\logs /move /e || IF %errorlevel% NEQ 0 @echo ******ERROR CAME****** & goto error_det

REM MAY NOT USE IT -------------- MOVE /Y %main_dir%\Application\logs %main_dir%\SupportingFiles || IF %errorlevel% NEQ 0 @echo ******ERROR CAME****** & goto error_det

GOTO all_is_well

:all_is_well
GOTO end

:error_det
echo The Process has failed, please look for the word ******ERROR CAME****** to see more details where it failed

:end
echo Completed