@echo off
FOR /f "tokens=2 delims==" %%G in ('wmic os get localdatetime /value') do set datetime=%%G
 set bkp_folder=%datetime:~0,14%
@echo on
IF EXIST Deliverable REN %cd%\Deliverable Deliverable-%bkp_folder%
IF %errorlevel% NEQ 0 echo ******ERROR CAME****** & goto error_det
IF EXIST SupportingFiles REN %cd%\SupportingFiles SupportingFiles-%bkp_folder%
IF %errorlevel% NEQ 0 echo ******ERROR CAME****** & goto error_det

goto end

:error_det
echo The Process has failed, please look for the word ******ERROR CAME****** to see more details where it failed

:end
pause