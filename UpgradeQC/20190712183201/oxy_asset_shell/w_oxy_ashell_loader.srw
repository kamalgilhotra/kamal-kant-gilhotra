forward
global type w_oxy_ashell_loader from w_top
end type
type dw_company from uo_dw_grid_multi_no_cntl_top within w_oxy_ashell_loader
end type
type pb_close from picturebutton within w_oxy_ashell_loader
end type
type st_noload_message from statictext within w_oxy_ashell_loader
end type
type pb_load from picturebutton within w_oxy_ashell_loader
end type
type pb_companies from picturebutton within w_oxy_ashell_loader
end type
type pb_select_all from picturebutton within w_oxy_ashell_loader
end type
type pb_stage_files from picturebutton within w_oxy_ashell_loader
end type
type pb_clear from picturebutton within w_oxy_ashell_loader
end type
type pb_update_assets from picturebutton within w_oxy_ashell_loader
end type
type pb_errors from picturebutton within w_oxy_ashell_loader
end type
type pb_close_asset_locs from picturebutton within w_oxy_ashell_loader
end type
type sle_extract_date from singlelineedit within w_oxy_ashell_loader
end type
type st_1 from statictext within w_oxy_ashell_loader
end type
type st_2 from statictext within w_oxy_ashell_loader
end type
end forward

global type w_oxy_ashell_loader from w_top
integer width = 3351
integer height = 2016
string title = "asset shell : chevron : loading interface"
12632256
dw_company dw_company
pb_close pb_close
st_noload_message st_noload_message
pb_load pb_load
pb_companies pb_companies
pb_select_all pb_select_all
pb_stage_files pb_stage_files
pb_clear pb_clear
pb_update_assets pb_update_assets
pb_errors pb_errors
pb_close_asset_locs pb_close_asset_locs
sle_extract_date sle_extract_date
st_1 st_1
st_2 st_2
end type
global w_oxy_ashell_loader w_oxy_ashell_loader

type variables
////
////	used to track whether we go to the company table or not for refreshing the datawindow.
////
	boolean	i_opened_company_table = false
	

end variables
forward prototypes
public subroutine of_retrievecompanies ()
end prototypes

public subroutine of_retrievecompanies ();////
////	retrieve the eligible companies datawindow.
////
			dw_company.retrieve()

////
////	change what's visible/enabled based on if we have any company's to load or not.
////
			if dw_company.rowcount() = 0 then
				st_noload_message.visible = true
				pb_load.enabled = false
			else
				st_noload_message.visible = false
			end if
end subroutine

on w_oxy_ashell_loader.create
int icurrent
call super::create
this.dw_company=create dw_company
this.pb_close=create pb_close
this.st_noload_message=create st_noload_message
this.pb_load=create pb_load
this.pb_companies=create pb_companies
this.pb_select_all=create pb_select_all
this.pb_stage_files=create pb_stage_files
this.pb_clear=create pb_clear
this.pb_update_assets=create pb_update_assets
this.pb_errors=create pb_errors
this.pb_close_asset_locs=create pb_close_asset_locs
this.sle_extract_date=create sle_extract_date
this.st_1=create st_1
this.st_2=create st_2
icurrent=upperbound(this.control)
this.control[icurrent+1]=this.dw_company
this.control[icurrent+2]=this.pb_close
this.control[icurrent+3]=this.st_noload_message
this.control[icurrent+4]=this.pb_load
this.control[icurrent+5]=this.pb_companies
this.control[icurrent+6]=this.pb_select_all
this.control[icurrent+7]=this.pb_stage_files
this.control[icurrent+8]=this.pb_clear
this.control[icurrent+9]=this.pb_update_assets
this.control[icurrent+10]=this.pb_errors
this.control[icurrent+11]=this.pb_close_asset_locs
this.control[icurrent+12]=this.sle_extract_date
this.control[icurrent+13]=this.st_1
this.control[icurrent+14]=this.st_2
end on

on w_oxy_ashell_loader.destroy
call super::destroy
destroy(this.dw_company)
destroy(this.pb_close)
destroy(this.st_noload_message)
destroy(this.pb_load)
destroy(this.pb_companies)
destroy(this.pb_select_all)
destroy(this.pb_stage_files)
destroy(this.pb_clear)
destroy(this.pb_update_assets)
destroy(this.pb_errors)
destroy(this.pb_close_asset_locs)
destroy(this.sle_extract_date)
destroy(this.st_1)
destroy(this.st_2)
end on

event open;call super::open;////
////	before we do anything, start with the nothing to load message invisible.
////
			st_noload_message.visible = false

////
////	set up the eligible companies datawindow.  then, retrieve it.
////
			dw_company.settransobject( sqlca )
			this.of_retrievecompanies()
end event

event activate;call super::activate;////
////	if we didn't go to the company table before we activated this, get on out of here.
////
			if i_opened_company_table = false then return

////
////	if we've made it here then we went to the companies table where things could have been edited/added so re-retrieve the eligible companies datawindow.
////
			this.of_retrievecompanies()
			i_opened_company_table = false
end event

type dw_all_cons_columns from w_top`dw_all_cons_columns within w_oxy_ashell_loader
integer taborder = 0
end type

type dw_table_help from w_top`dw_table_help within w_oxy_ashell_loader
integer taborder = 0
end type

type dw_help from w_top`dw_help within w_oxy_ashell_loader
integer taborder = 0
end type

type dw_topkey from w_top`dw_topkey within w_oxy_ashell_loader
integer taborder = 0
end type

type dw_company from uo_dw_grid_multi_no_cntl_top within w_oxy_ashell_loader
integer x = 50
integer y = 44
integer width = 2171
integer height = 1824
integer taborder = 0
boolean bringtotop = true
string dataobject = "dw_oxy_ashell_companies_in_staging"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event constructor;call super::constructor;////
////	we don't want that stupid mouse selection stuff.
////
			this.modify( "datawindow.selected.mouse=no" )
end event

type pb_close from picturebutton within w_oxy_ashell_loader
integer x = 2437
integer y = 1220
integer width = 777
integer height = 100
integer taborder = 20
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
string text = "close"
boolean originalsize = true
vtextalign vtextalign = vcenter!
12632256
end type

event clicked;////
////	close this here window.
////
			close( parent )
end event

type st_noload_message from statictext within w_oxy_ashell_loader
integer x = 187
integer y = 500
integer width = 1888
integer height = 76
boolean bringtotop = true
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
12632256
string text = "there are currently no assets from sap to load."
alignment alignment = center!
boolean focusrectangle = false
end type

type pb_load from picturebutton within w_oxy_ashell_loader
integer x = 2437
integer y = 704
integer width = 777
integer height = 100
integer taborder = 10
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
string text = "3. load assets to cpr"
boolean originalsize = true
vtextalign vtextalign = vcenter!
12632256
end type

event clicked;/*
 *	processing.
 */
	long	i, maxi, j
	string	glco[], run_type

/*
 *	misc objects.
 */
	uo_oxy_ashell_loader uo_loader

	run_type = 'asset shell - load assets'
////
////	grab all of the selected companies.
////
			maxi = dw_company.rowcount()
			j = 1
			
			for i = 1 to maxi
				if dw_company.isselected( i ) then
					glco[j] = dw_company.getitemstring( i, "company" )
					j++
				end if
			next
			
			if upperbound( glco[] ) = 0 then
				messagebox( "no companies selected", "please select one or more companies for which i should load assets.  no changes made." )
				return
			end if

////  
////	create the loader.
////
			uo_loader = create uo_oxy_ashell_loader

////
////	call the function to load the assets and let it go to work.
////
			uo_loader.of_run( glco[], run_type )

////
////	we're done with the loader so kill it.
////
			destroy uo_loader

////
////		do the auto insert to cpr control so the activity can be pulled.   this should be done automatically for the user
////
			insert into cpr_control
			(company_id, accounting_month)
			select company_id, maxdate
			from company_setup,
				  (select to_date(max(extract_date),'mm/dd/yyyy') maxdate
				  from oxy_stage_assets
				  )
			where not exists
					(select 1
					from cpr_control
					where company_setup.company_id = cpr_control.company_id
					and accounting_month = maxdate
					) ;

////
////	re-retrieve the eligible companies datawindow.
////
			of_retrievecompanies()
end event

type pb_companies from picturebutton within w_oxy_ashell_loader
integer x = 2437
integer y = 1764
integer width = 777
integer height = 100
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
string text = "companies ..."
boolean originalsize = true
vtextalign vtextalign = vcenter!
12632256
end type

event clicked;/*
 * misc objects.
 */
	s_pp_table_parms table_info
	w_pp_table_grid lw_pp_table_grid

////
////	set the table name and description for the company table that we're going to open.
////
			table_info.table_name = "company_setup"
			
			select		description
			into		:table_info.table_short_desc
			from		powerplant_tables
			where	lower( table_name ) = :table_info.table_name;

////
////	open up the standard table maintenance window for the company table.  (also, note that we're about to open the company table for editing.)
////
			i_opened_company_table = true
			opensheetwithparm( lw_pp_table_grid, table_info, "w_pp_table_grid", w_top_frame, 7, original! )
end event

type pb_select_all from picturebutton within w_oxy_ashell_loader
string tag = "select all"
integer x = 2245
integer y = 380
integer width = 146
integer height = 128
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
boolean originalsize = true
string picturename = "ptc_button_selectall.gif"
vtextalign vtextalign = vcenter!
12632256
end type

event clicked;long i
if this.tag = 'select all' then
	for i = 1 to dw_company.rowcount()
		dw_company.selectrow(i, true)
	next
	this.tag = 'unselect all'
else
	for i = 1 to dw_company.rowcount()
		dw_company.selectrow(i, false)
	next
	this.tag = 'select all'
end if
end event

type pb_stage_files from picturebutton within w_oxy_ashell_loader
integer x = 2437
integer y = 352
integer width = 777
integer height = 100
integer taborder = 10
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
string text = "1. load sap files"
boolean originalsize = true
vtextalign vtextalign = vcenter!
12632256
end type

event clicked;/*
 *	processing.
 */
	long	i, maxi, j, rows, rtn
	string	glco[], err_msg
	string extract_date
/*
 *	misc objects.
 */
	uo_stage_sap_files uo_loader

////
////	validate the input month number (of format mmddyyyy)
////
		extract_date = sle_extract_date.text
		if extract_date = 'mmddyyyy' then
			err_msg = "please input a valid file month number to load of format mmddyyyy (month + day +  year).  it is currently actually "+&
				"'mmyyyy'.~n~nno processing occurred."
			messagebox( "invalid date",err_msg )
			return -1
		elseif isnumber(extract_date) = false then
			err_msg = "please input a valid file month number to load of format mmddyyyy (month + day + year). it is currently not a "+&
				"number.~n~nno processing occurred."
			messagebox( "invalid date",err_msg )
			return -1
		elseif len(extract_date) <> 8 then
			err_msg = "please input a valid file month number to load of format mmddyyyy (month + day + year). it is currently not 8 characters. "+&
				"~n~nno processing occurred."
			messagebox( "invalid date",err_msg )
			return -1
		end if 

////
////	create the loader.
////
			uo_loader = create uo_stage_sap_files

			select process_id
			into :g_process_id
			from pp_processes
			where description = 'asset shell import'
			;

////
			rows = dw_company.rowcount()
			
			if rows > 0 then
					rtn = messagebox( "unprocessed assets", "there are unprocessed assets.   are you sure you want to re-load a new set of sap files?  (if you click 'yes' all of the assets will be reloaded.)", stopsign!, yesno!, 1 )
					if rtn = 2 then return
			end if

////
////	call the function to load the assets and let it go to work.
////
			uo_loader.uf_read( extract_date )

////
////	we're done with the loader so kill it.
////
			destroy uo_loader

////
////	re-retrieve the eligible companies datawindow.
////
			of_retrievecompanies()

end event

type pb_clear from picturebutton within w_oxy_ashell_loader
integer x = 2437
integer y = 1632
integer width = 777
integer height = 100
integer taborder = 20
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
string text = "clear session ids"
boolean originalsize = true
vtextalign vtextalign = vcenter!
12632256
end type

event clicked;/*
 *	processing.
 */
	long	rtn
	 

					rtn = messagebox("check point","the running session id ensures that this process is not executed while other users "+&
						"are also running.   are you sure that you want to clear this?",question!, yesno!)
					if rtn = 2 then 
						return
					else
						update pp_processes
						set running_session_id = null
						where trim(description) in ('asset shell - load assets', 'asset shell import')
						;
						
						if sqlca.sqlcode <> 0 then
							messagebox("error","running session id could not be cleared")
						else
							commit;
						end if
					end if
end event

type pb_update_assets from picturebutton within w_oxy_ashell_loader
integer x = 2437
integer y = 528
integer width = 777
integer height = 100
integer taborder = 20
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
string text = "2.  update sap records"
boolean originalsize = true
vtextalign vtextalign = vcenter!
12632256
end type

event clicked;//
//	open the details window.
//
			open( w_oxy_sap_assets_override)
end event

type pb_errors from picturebutton within w_oxy_ashell_loader
integer x = 2437
integer y = 880
integer width = 777
integer height = 100
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
string text = "4. view errors"
boolean originalsize = true
vtextalign vtextalign = vcenter!
12632256
end type

event clicked;//
//	open the details window.
//
			open( w_oxy_stg_asst_rev_ov)
end event

type pb_close_asset_locs from picturebutton within w_oxy_ashell_loader
integer x = 2437
integer y = 1048
integer width = 777
integer height = 100
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
string text = "5. close old asset locations"
boolean originalsize = true
vtextalign vtextalign = vcenter!
12632256
end type

event clicked;long rtn

rtn = messagebox( "close old asset locations", "this process should only be run after company 0061 assets have been processed.  if you have not yet "+&
				"run company 0061, please exit this window and complete that process. ~r~n~r~n"+&
				"the following window will map old abandoned asset locations to parcels with the name "+&
				"xx-nr abandoned, where xx is the 2 digit state on the parcel.   this works in the following way: "+"~r~n~r~n"+&
				"1.  the maximum valuation date is taken for the tax types in each state. ~r~n"+&
				"2.  the most recently completed year in sap is used to determine the cutoff year.   (for an oct 2010 sap file, the year will be 2009, for dec. 31 2010, the year will be 2010). ~r~n"+&
				"3.  the valuation date and year are combined to yield the cutoff date (e.g.  10/01/2009 for alabama, 01/01/2009 for most states, etc.) ~r~n"+&
				"4.  we check the asset location against the oxy facilities staging table (via the minor location).  ~r~n" + &
				"5.  if the status code is 05 on the facility and the status start date is older than the cutoff date, then action proceeds. ~r~n"+&
				"6.  the existing location mappings for the asset location are deleted.   ~r~n"+&
				"7.  mappings are added so that each abandoned asset location is mapped to a nr abandoned parcel (see above) if one exists. ~r~n ~r~n"+&
				"8.  there can only be one nr abandoned parcel in each state. ~r~n ~r~n"+&
				"are you sure you want to do this?  ", stopsign!, yesno!, 1 )
if rtn = 2 then return

/* mark an asset location as abandoned if it was abandoned more than 1 year from the lien date.
because users may want to rerun a state, we are going to be very careful to only select
things that are definitely at least one year old.   this means that some records will
still need to be corrected manually.   

we could use pt_control if oxy locks tax years, but they don't.   we can't use property
tax year (ptv_year_view), because they may not have created the tax year yet.   so we'll
take the most recent sap year and append the valuation date to it to build our (safe) year.*/

delete from pt_parcel_location
where asset_location_id in 
   (select a.asset_location_id   /* pool of abandoned asset locations */
      from asset_location a,
           minor_location b,
           pt_parcel_location c,
           (select to_number(facility_no) facility_number, status_start_date
              from oxy_stage_facility facility
             where status_code = '05'
           ) facility_view,
           (select type_view.state_id,
                   to_date(type_view.valuation_date||'/'||asset_view.valuation_year,'mm/dd/yyyy') max_lien_date
              from (select state_id, max(to_char(valuation_date,'mm/dd')) valuation_date
                    from property_tax_type_data
                    group by state_id
                   ) type_view,
                   (select distinct to_char(to_date(extract_date,'mm/dd/yyyy')-360,'yyyy') valuation_year
                      from oxy_stage_assets   /* pick the most recent completed calendar year */
                     where rownum = 1
                   ) asset_view
             ) date_view
      where a.state_id = date_view.state_id
      and   a.minor_location_id = b.minor_location_id
      and facility_view.facility_number = b.minor_location_id
      and date_view.max_lien_date > facility_view.status_start_date
      and a.asset_location_id = c.asset_location_id
      )
;


if sqlca.sqlcode <> 0 then
	messagebox( "error", "error occurred when deleting asset location to parcel mappings.  no changes made. ~r~n " + sqlca.sqlerrtext)
	rollback;
	return 
end if

insert into pt_parcel_location
(asset_location_id, prop_tax_company_id, parcel_type_id, allocated_yn, parcel_id)
select distinct asset_view.asset_location_id, asset_view.prop_tax_company_id, 0 /* all */, 
       0, prcl_view.parcel_id
  from
       (select distinct asset_location_id, prop_tax_company_id
       from cpr_ledger,
            company_setup
       where cpr_ledger.company_id = company_setup.company_id
       ) asset_view,
      (select distinct a.asset_location_id, a.state_id   /* pool of abandoned asset locations */
       from asset_location a,
           minor_location b,
           (select to_number(facility_no) facility_number, status_start_date
              from oxy_stage_facility facility
             where status_code = '05'
           ) facility_view,
           (select type_view.state_id,
                   to_date(type_view.valuation_date||'/'||asset_view.valuation_year,'mm/dd/yyyy') max_lien_date
              from (select state_id, max(to_char(valuation_date,'mm/dd')) valuation_date
                    from property_tax_type_data
                    group by state_id
                   ) type_view,
                   (select distinct to_char(to_date(extract_date,'mm/dd/yyyy')-360,'yyyy') valuation_year
                      from oxy_stage_assets   /* pick the most recent completed calendar year */
                     where rownum = 1
                   ) asset_view
             ) date_view
      where a.state_id = date_view.state_id
      and   a.minor_location_id = b.minor_location_id
      and facility_view.facility_number = b.minor_location_id
      and date_view.max_lien_date > facility_view.status_start_date
      order by 1, 2
      ) abandoned_loc_view,
      (select parcel_id, state_id
         from pt_parcel
        where upper(substr(parcel_number,4)) = 'nr abandoned'
       ) prcl_view 
where asset_view.asset_location_id = abandoned_loc_view.asset_location_id
and   abandoned_loc_view.state_id = prcl_view.state_id
;

if sqlca.sqlcode <> 0 then
	messagebox( "error", "error occurred when insert asset location mappings to nr abandoned parcels.  no changes made. ~r~n" + sqlca.sqlerrtext)
	rollback;
	return 
end if

commit;
messagebox( "process completed", "the asset location mappings were updated.")
end event

type sle_extract_date from singlelineedit within w_oxy_ashell_loader
integer x = 2542
integer y = 68
integer width = 567
integer height = 100
integer taborder = 10
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
12632256
string text = "mmddyyyy"
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_oxy_ashell_loader
integer x = 2478
integer y = 180
integer width = 699
integer height = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
33554432
67108864
string text = "extraction date"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_2 from statictext within w_oxy_ashell_loader
integer x = 2478
integer y = 240
integer width = 699
integer height = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
33554432
67108864
string text = "ex: 12/31/2014 = 12312014"
alignment alignment = center!
boolean focusrectangle = false
end type

