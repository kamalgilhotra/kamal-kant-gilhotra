forward
global type w_oxy_sap_assets_override from w_ppbase_top
end type
type dw_activity from uo_dw_grid_single_top_edit within w_oxy_sap_assets_override
end type
type pb_update from uo_ppbase_button_picture within w_oxy_sap_assets_override
end type
type pb_cancel from uo_ppbase_button_picture within w_oxy_sap_assets_override
end type
type pb_search from uo_ppbase_button_picture within w_oxy_sap_assets_override
end type
type st_state_label from statictext within w_oxy_sap_assets_override
end type
type st_company from statictext within w_oxy_sap_assets_override
end type
type dw_state from datawindow within w_oxy_sap_assets_override
end type
type dw_company from datawindow within w_oxy_sap_assets_override
end type
type st_cost_center from statictext within w_oxy_sap_assets_override
end type
type sle_cost_center from uo_ppbase_singlelineedit within w_oxy_sap_assets_override
end type
type st_partial_search from statictext within w_oxy_sap_assets_override
end type
type gb_filters from groupbox within w_oxy_sap_assets_override
end type
type gb_assets from groupbox within w_oxy_sap_assets_override
end type
end forward

global type w_oxy_sap_assets_override from w_ppbase_top
integer width = 4265
integer height = 2416
string title = "property tax - update sap assets window"
dw_activity dw_activity
pb_update pb_update
pb_cancel pb_cancel
pb_search pb_search
st_state_label st_state_label
st_company st_company
dw_state dw_state
dw_company dw_company
st_cost_center st_cost_center
sle_cost_center sle_cost_center
st_partial_search st_partial_search
gb_filters gb_filters
gb_assets gb_assets
end type
global w_oxy_sap_assets_override w_oxy_sap_assets_override

type variables
////
////	the currently selected information.
////
		long		i_year
		long		i_company_id
		string		i_state_id
		string		i_county_id
		string    i_cost_center
		string		i_asset_class, i_eval_group5, i_facility_number, i_inventory_num, i_asset_group
		
////
////	the original sql of the rates datawindow
////
		string	i_orig_sql
end variables

on w_oxy_sap_assets_override.create
int icurrent
call super::create
this.dw_activity=create dw_activity
this.pb_update=create pb_update
this.pb_cancel=create pb_cancel
this.pb_search=create pb_search
this.st_state_label=create st_state_label
this.st_company=create st_company
this.dw_state=create dw_state
this.dw_company=create dw_company
this.st_cost_center=create st_cost_center
this.sle_cost_center=create sle_cost_center
this.st_partial_search=create st_partial_search
this.gb_filters=create gb_filters
this.gb_assets=create gb_assets
icurrent=upperbound(this.control)
this.control[icurrent+1]=this.dw_activity
this.control[icurrent+2]=this.pb_update
this.control[icurrent+3]=this.pb_cancel
this.control[icurrent+4]=this.pb_search
this.control[icurrent+5]=this.st_state_label
this.control[icurrent+6]=this.st_company
this.control[icurrent+7]=this.dw_state
this.control[icurrent+8]=this.dw_company
this.control[icurrent+9]=this.st_cost_center
this.control[icurrent+10]=this.sle_cost_center
this.control[icurrent+11]=this.st_partial_search
this.control[icurrent+12]=this.gb_filters
this.control[icurrent+13]=this.gb_assets
end on

on w_oxy_sap_assets_override.destroy
call super::destroy
destroy(this.dw_activity)
destroy(this.pb_update)
destroy(this.pb_cancel)
destroy(this.pb_search)
destroy(this.st_state_label)
destroy(this.st_company)
destroy(this.dw_state)
destroy(this.dw_company)
destroy(this.st_cost_center)
destroy(this.sle_cost_center)
destroy(this.st_partial_search)
destroy(this.gb_filters)
destroy(this.gb_assets)
end on

event closequery;call super::closequery;long	rtn, update_rtn

////
////	check to see if they want to save their changes
////
		
		dw_activity.accepttext()
		if dw_activity.modifiedcount() > 0 then
			rtn = messagebox( 'save changes', 'do you want to save your changes?', question!, &
										yesnocancel!, 1 )
			if rtn = 1 then
				update_rtn = pb_update.event clicked()
				if update_rtn <> 1 then 
					messagebox('error', 'an error occurred while attempting to save your changes.', &
									stopsign!, ok!)
					return 1
				end if
			elseif rtn = 2 then
				// do nothing
			else
				return 1
			end if
		end if
			
end event

event open;call super::open;
////
////	retrieve the dropdown datawindows and set up the rates datawindow.
////
		dw_state.settransobject( sqlca )
		dw_state.insertrow( 0 )
		f_add_dddw_null_retrieve(dw_state,"state_id","state_id","state_id")
		
		dw_activity.settransobject( sqlca )
		dw_activity.insertrow( 0 )
		
		dw_company.settransobject( sqlca )
		dw_company.insertrow( 0 )
		f_add_dddw_null_retrieve(dw_company,"company_id","company_id","company_id")

		
////	null out the instance variables.
////
		setnull( i_year )
		setnull( i_state_id )
		setnull( i_company_id )
		setnull( i_cost_center )

		
////
////	save off the original sql.
////
		i_orig_sql = dw_activity.object.datawindow.table.select
end event

type dw_all_cons_columns from w_ppbase_top`dw_all_cons_columns within w_oxy_sap_assets_override
end type

type dw_table_help from w_ppbase_top`dw_table_help within w_oxy_sap_assets_override
end type

type dw_help from w_ppbase_top`dw_help within w_oxy_sap_assets_override
end type

type dw_topkey from w_ppbase_top`dw_topkey within w_oxy_sap_assets_override
end type

type dw_activity from uo_dw_grid_single_top_edit within w_oxy_sap_assets_override
integer x = 82
integer y = 784
integer width = 3986
integer height = 1452
integer taborder = 10
boolean bringtotop = true
string dataobject = "dw_oxy_sap_asset_listing"
boolean maxbox = true
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event clicked;//override - we dont' want to highlight rows
end event

event doubleclicked;//override - we dont' want to highlight rows
end event

type pb_update from uo_ppbase_button_picture within w_oxy_sap_assets_override
string tag = "save"
integer x = 3625
integer y = 592
integer height = 124
integer taborder = 120
boolean bringtotop = true
string picturename = "ptc_button_save.gif"
string powertiptext = "save"
decimal i_resize_x_multiplier = 1
end type

event clicked;call super::clicked;long i, maxi, rows, v_count
decimal activity_cost, ref_cost
boolean update_row
long trans_id, asset_id, fiscal_year, asset_no, cost_center
string company, asset_main,asset_sub,extract_date
string ref_cost_center, ref_asset_class, ref_eval_group5, ref_state, ref_county, ref_zip_code, ref_geo,ref_asset_group
string ref_inventory_num, ref_license_num, ref_environ_invest, ref_cap_on_date, ref_evidence, ref_facility_number


////	accept the text.
////
		dw_activity.accepttext()

////
////	loop through the datawindow, search for changes and update the table.
////
		maxi = dw_activity.rowcount()
		
		for i = 1 to maxi
			if dw_activity.getitemstatus(i,0,primary!) <> notmodified! then
				company = dw_activity.getitemstring(i,"company")
				extract_date = dw_activity.getitemstring(i,"extract_date")
				cost_center = dw_activity.getitemnumber(i,"cost_center")
				asset_no = dw_activity.getitemnumber(i,"asset_no")
				ref_cost = dw_activity.getitemnumber(i,"cost")
				ref_cap_on_date = dw_activity.getitemstring(i,"cap_on_date")
				ref_evidence = dw_activity.getitemstring(i,"override_comments")
		////
		////	get the info for this row.
		////
	
				update oxy_stage_assets
				set 		loaded = 0
				where company = :company
				and	 asset_no = :asset_no
				and	 cost_center = :cost_center
				and	 extract_date = :extract_date
				;

				if sqlca.sqlcode <> 0 then
					messagebox( "error", "an error occurred while updating " + &
									"to the oxy_stage_assets table for reload.  no changes made." + &
									"~n~nerror code: " + string( sqlca.sqldbcode ) + &
									"~nerror text: " + sqlca.sqlerrtext  +&
									company+"-"+string(asset_no)+"-"+string(cost_center))
					rollback;
					return -1
				end if
				
				select count(*) 
				into :v_count
				from oxy_stage_assets_ref_override
				where company = :company
				and	 asset_no = :asset_no
				and	 cost_center = :cost_center
				and	 extract_date = :extract_date
				;
				
				if v_count = 0 then
					insert into oxy_stage_assets_ref_override
					(company, asset_no, cost_center, extract_date,
					ref_cap_on_date,
					ref_state,
					ref_evidence,
					ref_cost,
					orig_cap_on_date,
					orig_state,
					orig_cost,
					loaded, errored, error_message)
					select :company, :asset_no, :cost_center, :extract_date,
							:ref_cap_on_date,
							:ref_state,
							:ref_evidence,
							round(:ref_cost,2),
							cap_on_date,
							state,
							round(cost,2),
							loaded, nvl(errored,100), error_message
							from oxy_stage_assets
							where company = :company
							and	 asset_no = :asset_no
							and	 cost_center = :cost_center
							;				
				else
					update oxy_stage_assets_ref_override
					set ref_cost = round(:ref_cost,2),
						ref_state = :ref_state,
						ref_cap_on_date = :ref_cap_on_date,
						ref_evidence = :ref_evidence,
						(loaded, errored, error_message) = 
							(select loaded, nvl(errored,100), error_message
							from   oxy_stage_assets
							where oxy_stage_assets_ref_override.company = oxy_stage_assets.company
							and	 oxy_stage_assets_ref_override.asset_no = oxy_stage_assets.asset_no
							and	 oxy_stage_assets_ref_override.cost_center = oxy_stage_assets.cost_center
							and	 oxy_stage_assets_ref_override.extract_date = oxy_stage_assets.extract_date
							)
					where company = :company
					and	 asset_no = :asset_no
					and	 cost_center = :cost_center
					;
				end if				
	 
				if sqlca.sqlcode <> 0 then
					messagebox( "error", "an error occurred while inserting or updating " + &
									"to the oxy_stage_assets_ref_override table.  no changes made." + &
									"~n~nerror code: " + string( sqlca.sqldbcode ) + &
									"~nerror text: " + sqlca.sqlerrtext  +&
									company+"-"+string(asset_no)+"-"+string(cost_center))
					rollback;
					return -1
				end if
								
				dw_activity.setitemstatus( i, 0, primary!, notmodified! )
			end if 
		next
		
////
////	save our changes.
////
		commit;
		messagebox('success', 'asset override data updated successfully.')
		return 1
end event

type pb_cancel from uo_ppbase_button_picture within w_oxy_sap_assets_override
string tag = "cancel"
integer x = 3854
integer y = 592
integer height = 124
integer taborder = 20
boolean bringtotop = true
string picturename = "ptc_button_close.gif"
string powertiptext = "save"
decimal i_resize_x_multiplier = 1
end type

event clicked;call super::clicked;close(parent)
end event

type pb_search from uo_ppbase_button_picture within w_oxy_sap_assets_override
string tag = "save"
integer x = 1733
integer y = 52
integer height = 124
integer taborder = 20
boolean bringtotop = true
string picturename = "ptc_button_search.gif"
string powertiptext = "save"
decimal i_resize_x_multiplier = 1
end type

event clicked;call super::clicked;//////
//////	retrieve the rates datawindow, adding any filter criteria the user selected.
//////

string	where_state, where_county, where_cost_center, where_company, where_eval_group5, where_asset_class
string	where_asset_group, where_inventory_num, where_facility_number, where_blank_facility 
string	new_sql, gl_company_no
long		i, maxi, rtn, update_rtn
			

////
////	check to see if they want to save their changes before retrieving.
////
		dw_activity.accepttext()
		if dw_activity.modifiedcount() > 0 then
			rtn = messagebox( 'save changes', 'do you want to save your changes?', question!, &
										yesnocancel!, 1 )
			if rtn = 1 then
				update_rtn = pb_update.event clicked()
				if update_rtn <> 1 then return -1
			elseif rtn = 2 then
				// do nothing
			else
				return -1
			end if
		end if


////
////	grab the single line edits
////		

		setnull(i_cost_center)

		
		if trim(sle_cost_center.text) <> "" then i_cost_center = trim(sle_cost_center.text)
		
		new_sql = i_orig_sql

////
////	set up the filter criteria.
////
	
		if not isnull( i_company_id ) then
			select company_id
			into :gl_company_no
			from company_setup
			where company_id= :i_company_id
			;		
			
			where_company = " and trim( leading 0 from oxy_stage_assets.company) = trim(leading 0 from '" + gl_company_no + "')"
			new_sql = new_sql + where_company
		end if
		if not isnull( i_state_id ) then
			where_state = " and state.state_id = '" + trim(i_state_id) + "'"
			new_sql = new_sql + where_state
		end if
		
		if not isnull( i_cost_center ) then
			where_cost_center = " and oxy_stage_assets.cost_center like '%" + trim(i_cost_center) + "%'"
			new_sql = new_sql + where_cost_center
		end if
		
		
		dw_activity.object.datawindow.table.select = new_sql
		dw_activity.resetupdate()
		dw_activity.retrieve(  )
		

end event

type st_state_label from statictext within w_oxy_sap_assets_override
integer x = 155
integer y = 112
integer width = 155
integer height = 52
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
8388608
16777215
string text = "state"
boolean focusrectangle = false
end type

type st_company from statictext within w_oxy_sap_assets_override
integer x = 155
integer y = 208
integer width = 247
integer height = 52
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
8388608
16777215
string text = "company"
boolean focusrectangle = false
end type

type dw_state from datawindow within w_oxy_sap_assets_override
integer x = 443
integer y = 100
integer width = 1006
integer height = 92
integer taborder = 30
boolean bringtotop = true
string dataobject = "dddw_pp_state"
boolean border = false
boolean livescroll = true
end type

event itemchanged;datawindowchild	dw_kid
string				null_str

////
////	save off the new state if its changed.
////
		if i_state_id = data then return
		i_state_id = data
	
////
////	fill the county menu with the state's counties.
////
//		dw_county.getchild( "state_id_county_id", dw_kid )
//		dw_kid.setfilter( "state_id = '" + trim( i_state_id ) + "' or " + &
//								"isnull( county_id ) " )
//		dw_kid.filter()

		
////
////	fill the county menu with the state's counties.
////
//		dw_cost_center.getchild( "major_location_id", dw_kid )
//		dw_kid.setfilter( "state_id = '" + trim( i_state_id )  )
//		dw_kid.filter()
	

////
////	clear out the county and cost center selection.
////
		setnull( null_str )
		
		
end event

type dw_company from datawindow within w_oxy_sap_assets_override
integer x = 443
integer y = 200
integer width = 1006
integer height = 92
integer taborder = 20
boolean bringtotop = true
string dataobject = "dddw_pp_company"
boolean border = false
boolean livescroll = true
end type

event itemchanged;datawindowchild	dw_kid
string				null_str

////
////	save off the new state if its changed.
////
		if i_company_id = long(data) then return
		i_company_id = long(data)

end event

type st_cost_center from statictext within w_oxy_sap_assets_override
integer x = 155
integer y = 416
integer width = 306
integer height = 52
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
8388608
16777215
string text = "cost center**"
boolean focusrectangle = false
end type

type sle_cost_center from uo_ppbase_singlelineedit within w_oxy_sap_assets_override
integer x = 457
integer y = 412
integer width = 969
integer height = 64
integer taborder = 130
boolean bringtotop = true
end type

type st_partial_search from statictext within w_oxy_sap_assets_override
integer x = 101
integer y = 516
integer width = 562
integer height = 52
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
8388608
16777215
string text = "** partial search allowed"
boolean focusrectangle = false
end type

type gb_filters from groupbox within w_oxy_sap_assets_override
integer x = 46
integer y = 12
integer width = 1568
integer height = 576
integer taborder = 10
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
8388608
16777215
string text = "filter criteria"
end type

type gb_assets from groupbox within w_oxy_sap_assets_override
integer x = 50
integer y = 704
integer width = 4050
integer height = 1576
integer taborder = 10
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
8388608
16777215
string text = "asset data"
end type

