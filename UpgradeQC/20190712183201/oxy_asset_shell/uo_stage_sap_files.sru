forward
global type uo_stage_sap_files from nonvisualobject
end type
end forward

global type uo_stage_sap_files from nonvisualobject
end type
global uo_stage_sap_files uo_stage_sap_files

type variables
string i_local_file_directory, i_co_file_array[], i_fi_file_array[]
boolean i_conv = false
string i_processed_file_directory, i_received_file_directory, i_file_to_put, i_exe_name

////
////	the logs connection object.
////
	uo_sqlca_logs	i_sqlsa

////
////	all our logs variables.
////
	long	i_logs_proc_id
	long	i_logs_occur_id
	string	i_logs_proc_desc = 'asset shell import'
	string	i_logs_proc_desc = 'process for importing asset file from sap into the asset shell'
	boolean i_bad_record = false
	string extract_date
end variables

forward prototypes
public function string uf_check_sql (string a_message)
public function long uf_asset_shell_import (string file_name, string file_path, string original_file_name)
public function integer uf_cost_center_import (string file_name, string file_path, string original_file_name)
public function long uf_find_files ()
public function integer uf_import_01_asset_files ()
public function long uf_cost_center_load ()
public function long uf_load_ml ()
public subroutine of_write_log (string a_message)
public function long uf_read (string a_extract_date)
end prototypes

public function string uf_check_sql (string a_message);string rtn_str
rtn_str = 'ok'
if sqlca.sqlcode <> 0 then
	rtn_str = a_message + " ~r~n  ora error : " + sqlca.sqlerrtext
end if
return rtn_str
end function

public function long uf_asset_shell_import (string file_name, string file_path, string original_file_name);boolean success = true
string fixed_string, sqls,sysdate
long file_number, rtn, i, numrows, max_dk, invalid_cost_records, invalid_format_records
double sum_cost

string company,cost_center,asset_main,asset_sub,cap_on_date,state,zip_code,geo,county,plant,location, &
		room,asset_group,asset_class,eval_group5,quantity,unit,cost,eval_group1,eval_group2,eval_group3,reason,inventory_num, &
		license_num,asset_main_txt,asset_name1,asset_name2,environ_invest,ref_state,ref_zip_code,ref_geo,ref_county, &
		life,asset_no,description,depr,deprsign,costsign,glclass,glexp,bldg,ctgy,class,plant_name,cost_ctr_name,category_name,class_name
uo_ds_top ds_temp
ds_temp = create uo_ds_top

sqls = "delete from oxy_stage_assets_nopk"
execute immediate :sqls;

file_number = fileopen(file_name)

sum_cost = 0
invalid_cost_records = 0
invalid_format_records = 0

if file_number = -1 then
	this.of_write_log('-- file not found! : ' + file_name)
	success = false
	goto theend
elseif isnull(file_number) then
	this.of_write_log('-- null value passed! : ' + file_name)
	success = false
	goto theend
end if

//f_status_box('import file', string(now()) + '... reading file ... ')
do while true
	rtn = fileread(file_number, fixed_string)
	if rtn < 0 and i_bad_record = true then 
		exit 
	elseif rtn < 0 then
		i_bad_record = true
		goto end_of_loop
	end if
	
	i++
	
	//extract_date = a_extract_date
	company = trim(mid(fixed_string,1,4))
	plant = trim(mid(fixed_string,5,4))
	cost_center = trim(mid(fixed_string,9,10))
	//asset_main = trim(mid(fixed_string,25,12))
	//asset_sub = trim(mid(fixed_string,37,4))
	cap_on_date = trim(mid(fixed_string,19,8))
	state = trim(mid(fixed_string,27,2))
	//zip_code = trim(mid(fixed_string,53,5))
	//geo = trim(mid(fixed_string,58,2))
	//county = trim(mid(fixed_string,60,3))
	//plant = trim(mid(fixed_string,66,4))
	life = trim(mid(fixed_string,29,6))
	asset_no = trim(mid(fixed_string,35,16))
	description = trim(mid(fixed_string,51,50))
	quantity = trim(mid(fixed_string,101,10))
	depr = trim(mid(fixed_string,111,13))
	deprsign = trim(mid(fixed_string,124,1))
	cost = trim(mid(fixed_string,125,13))
	costsign = trim(mid(fixed_string,138,1))
	glclass = trim(mid(fixed_string,139,3))
	glexp = trim(mid(fixed_string,142,1))
	bldg = trim(mid(fixed_string,143,8))
	ctgy = trim(mid(fixed_string,151,4))
	class = trim(mid(fixed_string,155,8))
	plant_name = trim(mid(fixed_string,222,30))
	cost_ctr_name = trim(mid(fixed_string,252,20))
	category_name = trim(mid(fixed_string,272,30))
	class_name = trim(mid(fixed_string,302,20))
	

/* sometimes the records from sap are formatted in an invalid manner due to carriage returns in the description.  in this case, the loader
should not load the record, but continue with the load.   in this way, the users can ignore the records if desired */
	if isnumber(company) then
				
				insert into oxy_stage_assets_nopk
				(
				extract_date,company,cost_center,cap_on_date,state,plant,location,quantity,cost,life,asset_no,description,depr,deprsign,
				costsign,glclass,glexp,bldg,ctgy,class,plant_name,cost_ctr_name,category_name,class_name
				) values
				(
				:extract_date,:company,:cost_center,:cap_on_date,:state,:plant,:location,:quantity,:cost,:life,:asset_no,:description,:depr,:deprsign,
				:costsign,:glclass,:glexp,:bldg,:ctgy,:class,:plant_name,:cost_ctr_name,:category_name,:class_name
				);
				
				if sqlca.sqlcode <> 0 then
					this.of_write_log('--insert into staging no-pk table failed')
					this.of_write_log("  ora error : " + sqlca.sqlerrtext)
					success = false
					goto theend
				else
					if mod(i,1000) = 0 then
						commit;
					end if
					if mod(i,5000) = 0 then
						this.of_write_log('--loaded ... ' + string(i) + ' ... rows ... commit')
					end if
					
					if isnumber(cost) then
						sum_cost = sum_cost + long(cost)
					else
						invalid_cost_records++
					end if
				end if
		else
				this.of_write_log('error record discovered on record number: ' + string(i) )
				this.of_write_log('data found: ' + fixed_string )
				invalid_format_records++
		end if
		i_bad_record = false
		end_of_loop:

loop

commit;

if rtn <> -100 and rtn <> 0 then
	this.of_write_log('error reading file.   return code of: ' + string(rtn)+' found on record: ' + string(i)+' please contact powerplant support with the file and this error message.' )
	this.of_write_log('once the error is corrected, you will need to restart the sap asset load from the beginning.' )
	success = false
	goto theend
end if 

this.of_write_log('total rows loaded into pre-staging table: ' + string(i) )
this.of_write_log('total cost loaded into pre-staging table from '+string(file_name)+': '+string(sum_cost))
this.of_write_log('number of records loaded with invalid cost amounts: '+string(invalid_cost_records))
this.of_write_log('number of records not loaded due to invalid format: '+string(invalid_format_records))
			
/* make sure that there are no duplicate rows by primary key of oxy_stage_assets */
select count(*) into :rtn
from (
	select cost_center, asset_no, count(1) 
	from oxy_stage_assets_nopk
	group by cost_center, asset_no
	having count(1) > 1
);

if rtn > 0 then
	this.of_write_log('--**invalid file: file contains duplicate rows. this file will not be loaded into staging table')
	sqls = + &
	" select company, asset_no, cost_center, count(1) " + &
	" from oxy_stage_assets_nopk " + &
	" group by company, asset_no, cost_center " + &
	" having count(1) > 1 "
	sqls = f_create_dynamic_ds(ds_temp,'grid', sqls,sqlca,true)
	if sqls = 'ok' then
		this.of_write_log('--       details:')
		for i = 1 to ds_temp.rowcount()
			company = ds_temp.getitemstring(i,1)
			asset_no = ds_temp.getitemstring(i,2)
			cost_center = ds_temp.getitemstring(i,3)
			this.of_write_log('--       company: ' + company + ' asset number: ' + asset_no + ' cost center: ' + cost_center)
		next
	end if
	success = false 
	goto theend
end if 

/*convert cost and depr fields in select to add decimal place 2 spaces from right*/
insert into oxy_stage_assets
(extract_date,company,cost_center,cap_on_date,state,plant,location,quantity,cost,life,asset_no,description,depr,deprsign,
costsign,glclass,glexp,bldg,ctgy,class,plant_name,cost_ctr_name,category_name,class_name)
select extract_date,company,cost_center,cap_on_date,state,plant,location,quantity,
substr(cost,2,10) || '.' || substr(cost,12),
life,asset_no,description,
substr(depr,2,10) || '.' || substr(depr,12)
,deprsign,
costsign,glclass,glexp,bldg,ctgy,class,plant_name,cost_ctr_name,category_name,class_name
from oxy_stage_assets_nopk;

if sqlca.sqlcode <> 0 then
	this.of_write_log('--insert into staging failed:')
	this.of_write_log("  ora error : " + sqlca.sqlerrtext)
	success = false
	goto theend
end if
theend:
fileclose(file_number)
destroy ds_temp
if success then 
	commit;
	this.of_write_log('--load successfully')
	this.of_write_log('--renaming file to: '+file_path+sysdate+'_'+original_file_name)
	sysdate	= string(today(),"yyyymmdd")+"_"+string(now(),"hhmm")
	//f_file_rename(file_name,file_path+sysdate+'_'+original_file_name)
	return 1
else
	rollback;
	this.of_write_log('--load failed')
	return -1
end if
	
end function

public function integer uf_cost_center_import (string file_name, string file_path, string original_file_name);boolean success = true
string fixed_string, sqls,sysdate
long file_number, rtn, i, numrows, max_dk

string company,cost_center,state,zip_code,geo,county,ref_county,name_1,name_2,address,city,pc_code

file_number = fileopen(file_name)
i=0

if file_number = -1 then
	this.of_write_log('-- file not found! : ' + file_name)
	return 1
elseif isnull(file_number) then
	this.of_write_log('-- null value passed! : ' + file_name)
	return 1
end if

//f_status_box('import file', string(now()) + '... reading file ... ')
do while true
	rtn = fileread(file_number, fixed_string)
	if rtn <= 0 then exit
	i++
	company = trim(mid(fixed_string,1,4))
	cost_center = trim(mid(fixed_string,5,10))
	state = trim(mid(fixed_string,15,2))
	zip_code = trim(mid(fixed_string,17,5))
	geo = trim(mid(fixed_string,22,2))
	county = trim(mid(fixed_string,24,3))
	ref_county = trim(mid(fixed_string,24,3))
	name_1 = trim(mid(fixed_string,30,35))
	name_2 = trim(mid(fixed_string,65,35))
	address = trim(mid(fixed_string,100,35))
	city = trim(mid(fixed_string,135,35))
	pc_code = trim(mid(fixed_string,170,10))

	
	insert into oxy_stage_cost_center
	("company","cost_center","state","zip_code","geo","county",
	"ref_county","name_1","name_2","address","city","pc_code") values 
	(:company,:cost_center,:state,:zip_code,:geo,:county,
	:ref_county,:name_1,:name_2,:address,:city,:pc_code)
	;
	
	if sqlca.sqlcode <> 0 then
		this.of_write_log('--insert into staging failed')
		this.of_write_log("  ora error : " + sqlca.sqlerrtext)
		success = false
		goto theend
	else
		if mod(i,1000) = 0 then
			commit;
		end if
		if mod(i,5000) = 0 then
			this.of_write_log('--loaded ... ' + string(i) + ' ... rows ... commit')
		end if
	end if
loop

theend:
fileclose(file_number)
if success then 
	commit;
	this.of_write_log('--load successfully')
	this.of_write_log('--loaded ... ' + string(i) + ' ... rows ... commit')
	this.of_write_log('--renaming file to: '+file_path+sysdate+'_'+original_file_name)
	sysdate	= string(today(),"yyyymmdd")+"_"+string(now(),"hhmm")
	//f_file_rename(file_name,file_path+sysdate+'_'+original_file_name)

	return 1
else
	rollback;
	this.of_write_log('--load failed')
	return -1
end if
	
end function

public function long uf_find_files ();boolean success = true
string sqls, filepath, filename
long rtn

this.of_write_log("search for interface file options ... both must be present for process to continue.")
////get the file details from the system options
filename = f_pp_system_control("oxy - asset shell - file name")
filepath = f_pp_system_control("oxy - asset shell - file path")

////
////	make sure we've gotten a file path.
////
	if filepath = "" or isnull( filepath ) then
		this.of_write_log("error occurred while retrieving the path of the folder in which the file should " + &
					"exist.  check the 'ptc system options' table to make sure that a control exists " + &
					"called 'oxy - asset shell - file path' and that the control value is populated.")
		return -1
	end if

////
////	make sure we've gotten a file name.
////
	if filename = "" or isnull( filename ) then
		this.of_write_log("error occurred while retrieving the name of the file that should " + &
					"exist.  check the 'ptc system options' table to make sure that a control exists " + &
					"called 'oxy - asset shell - file name' and that the control value is populated.")
		return -1
	end if
return 1

	
end function

public function integer uf_import_01_asset_files ();boolean success = true
string sqls, description, filepath, filename
long i, rtn

uo_ds_top ds_temp
ds_temp = create uo_ds_top

////get the file details from the system options
filename = f_pp_system_control("oxy - asset shell - file name")
filepath = f_pp_system_control("oxy - asset shell - file path")

/* archive off whatever is in  oxy_stage_assets*/
this.of_write_log('archiving existing records in oxy_stage_assets ...')
insert into oxy_stage_assets_archive
(extract_date,company,cost_center,cap_on_date,state,plant,location,quantity,cost,life,asset_no,description,depr,deprsign,
costsign,glclass,glexp,bldg,ctgy,class,plant_name,cost_ctr_name,category_name,class_name,ldg_asset_id,ldg_books_schema_id,ldg_company_id,
ldg_gl_account_id,ldg_bus_segment_id,ldg_utility_account_id,ldg_sub_account_id,
ldg_retirement_unit_id,ldg_property_group_id,ldg_func_class_id,ldg_depr_group_id,
ldg_asset_location_id,ldg_in_service_year,ldg_eng_in_service_year,ldg_ledger_status,
ldg_subledger_indicator,ldg_work_order_number,ldg_description,ldg_description,
ldg_serial_number,act_asset_id,act_asset_activity_id,act_gl_posting_mo_yr,
act_out_of_service_mo_yr,act_cpr_posting_mo_yr,act_work_order_number,act_gl_je_code,
act_description,act_description,act_disposition_code,act_activity_code,
act_activity_status,act_activity_quantity,act_activity_cost,act_ferc_activity_code,
act_month_number,trans_from_asset_id,trans_to_asset_id,trans_asset_activity_id,
trans_gl_posting_mo_yr,trans_out_of_service_mo_yr,trans_cpr_posting_mo_yr,
trans_work_order_number,trans_gl_je_code,trans_description,trans_description,
trans_disposition_code,trans_activity_code,trans_activity_status,trans_activity_quantity,
trans_activity_cost,trans_ferc_activity_code,trans_month_number,loaded,
errored,error_message, act_depr_cost,
arch_batch_id,archive_date)
select 
extract_date,company,cost_center,cap_on_date,state,plant,location,quantity,cost,life,asset_no,description,depr,deprsign,
costsign,glclass,glexp,bldg,ctgy,class,plant_name,cost_ctr_name,category_name,class_name,ldg_asset_id,ldg_books_schema_id,ldg_company_id,
ldg_gl_account_id,ldg_bus_segment_id,ldg_utility_account_id,ldg_sub_account_id,
ldg_retirement_unit_id,ldg_property_group_id,ldg_func_class_id,ldg_depr_group_id,
ldg_asset_location_id,ldg_in_service_year,ldg_eng_in_service_year,ldg_ledger_status,
ldg_subledger_indicator,ldg_work_order_number,ldg_description,ldg_description,
ldg_serial_number,act_asset_id,act_asset_activity_id,act_gl_posting_mo_yr,
act_out_of_service_mo_yr,act_cpr_posting_mo_yr,act_work_order_number,act_gl_je_code,
act_description,act_description,act_disposition_code,act_activity_code,
act_activity_status,act_activity_quantity,act_activity_cost,act_ferc_activity_code,
act_month_number,trans_from_asset_id,trans_to_asset_id,trans_asset_activity_id,
trans_gl_posting_mo_yr,trans_out_of_service_mo_yr,trans_cpr_posting_mo_yr,
trans_work_order_number,trans_gl_je_code,trans_description,trans_description,
trans_disposition_code,trans_activity_code,trans_activity_status,trans_activity_quantity,
trans_activity_cost,trans_ferc_activity_code,trans_month_number,loaded,
errored,error_message, act_depr_cost,
arch_b_id, sysdate
from oxy_stage_assets,
	(select (nvl(max(arch_batch_id),0) + 1) arch_b_id from oxy_stage_assets_archive)
;
if sqlca.sqlcode <> 0 then
	this.of_write_log('--insert into archiving failed:')
	this.of_write_log("  ora error : " + sqlca.sqlerrtext)
	success = false
	goto theend
end if

/*truncate  oxy_stage_assets*/
execute immediate "delete from oxy_stage_assets";
if sqlca.sqlcode <> 0 then
	this.of_write_log('--stage truncating failed:')
	this.of_write_log("  ora error : " + sqlca.sqlerrtext)
	success = false
	goto theend
end if

commit;


/*loader*/
//sqls = + &
//" select description, file_directory, file_name " + &
//" from oxy_ashell_file_directory " + &
//" where process_id =  " + string(g_process_id) + " " + &
//" and lower(description) like '" + 'sap 01 asset file' + "%'" +&
//" and upper(trim(include_in_process_run)) = 'y' "

//sqls = f_create_dynamic_ds(ds_temp,'grid', sqls,sqlca,true)
//if sqls <> 'ok' then
//	this.of_write_log('--**error: creating dynamic ds failed: ')
//	this.of_write_log('--'+sqls)
//	success = false
//	goto theend
//end if
//for i = 1 to ds_temp.rowcount()
//	description = ds_temp.getitemstring(i,1)
//	filepath = ds_temp.getitemstring(i,2)
//	filename = ds_temp.getitemstring(i,3)
	this.of_write_log('--loading : ' + description)
	rtn = uf_asset_shell_import(filepath+filename, filepath,filename)
	if rtn <> 1 then
		this.of_write_log("   *****  error: asset 01 import failed!!!!! *****")
		success = false
		goto theend
	end if
//next

theend:
destroy ds_temp
if success then
	return 1
else
	return -1
end if
	
end function

public function long uf_cost_center_load ();boolean success = true
string sqls, description, filepath, filename, rtn_str
long i, rtn
uo_ds_top ds_temp


ds_temp = create uo_ds_top
sqls = + &
" select description, file_directory, file_name " + &
" from oxy_ashell_file_directory " + &
" where process_id =  " + string(g_process_id) + " " + &
" and lower(description) like '" + 'sap cost center file' + "%'" +&
" and upper(trim(include_in_process_run)) = 'y' order by dir_id"

sqls = f_create_dynamic_ds(ds_temp,'grid', sqls,sqlca,true)
if sqls <> 'ok' then
	this.of_write_log('--**error: creating dynamic ds failed: ')
	this.of_write_log('--'+sqls)
	success = false
	goto theend
end if

if ds_temp.rowcount() = 0 or isnull(ds_temp.rowcount())then
	this.of_write_log("   *****  no files set to load! check include in process run field, description. continuing...*****")
	success = true
	goto theend
end if 

delete from oxy_stage_cost_center;

rtn_str = uf_check_sql('--deleting old cost centers: ')
if rtn_str <> 'ok' then
	this.of_write_log("   " + rtn_str)
	success = false
	goto theend
end if

for i = 1 to ds_temp.rowcount()
	description = ds_temp.getitemstring(i,1)
	filepath = ds_temp.getitemstring(i,2)
	filename = ds_temp.getitemstring(i,3)
	this.of_write_log('--loading : ' + description+" filename: "+filename)
	rtn = uf_cost_center_import(filepath+filename, filepath,filename)
	if rtn <> 1 then
		this.of_write_log("   *****  error: cost center import failed!!!!! *****")
		success = false
		goto theend
	end if
next

rtn = uf_load_ml()
if rtn <> 1 then
	this.of_write_log("   *****  error: major location update failed!!!!! *****")
	success = false
	goto theend
end if


theend:
destroy ds_temp
if success then
	return 1
else
	return -1
end if
	
end function

public function long uf_load_ml ();boolean success = true
string rtn_str
long i, rtn

this.of_write_log("inserting new cost centers into major location table....")

insert into major_location ( 
major_location_id, division_id, state_id, 
municipality_id, location_type_id, external_location_id, 
grid_coordinate, description, description, 
address, zip_code )
select		mview.max_id + rownum major_location_id,
			1 division,
			nvl(st.state_id,'unknown') state_id,
			null municipality_id,
			1 location_type_id,
			trim( costcenter.cost_center ) external_location_id,
			null grid_coordinate,
			substr(trim( costcenter.cost_center ) || decode(nvl(trim( name_1 ),' '), ' ','',  ' - ' || trim( nvl(name_1 ,' '))),1,35) description,
			substr(trim( costcenter.cost_center ) || decode(nvl(trim( name_1 ),' '), ' ','',  ' - ' || trim( nvl(name_1 ,' ') ))||
                                            decode(nvl(trim( name_2 ),' '), ' ','',  ' - ' || trim( nvl(name_2 ,' ') )),1,254)  description,
      		decode(nvl(trim( costcenter.address ),' '), ' ','',  trim( costcenter.address )||' - ')|| nvl( trim( costcenter.city ) , '' )  address,
//			trim( costcenter.zip_code ) zip_code
			trim( 
				decode(costcenter.zip_code,'r2r0t',null,costcenter.zip_code)
			 ) zip_code 
from		(select cost_center, max(company) company, max(state) state, max(zip_code) zip_code, 
			max(geo) geo, max(county) county, max(ref_county) ref_county, max(name_1) name_1, max(name_2) name_2, 
			max(address) address, max(city) city, max(pc_code) pc_code
			from oxy_stage_cost_center
			group by cost_center) costcenter,
			state st,
			(	select		nvl( max( ml.major_location_id ), 0 ) + 1 max_id
				from		major_location ml
			) mview
where	trim( costcenter.state ) = st.state_code (+)
and 	not exists (
	select 1 from major_location where external_location_id = costcenter.cost_center
);

rtn_str = uf_check_sql('--creating new major location: ')
if rtn_str <> 'ok' then
	this.of_write_log("   " + rtn_str)
	success = false
	goto theend
end if

this.of_write_log("updating cost center information in major location table....")

update major_location z
set (description, description) = 
(
	select description, description from (
		select major_location_id, 
		substr(trim( costcenter.cost_center ) || decode(nvl(trim( name_1 ),' '), ' ','', ' - ' || trim( name_1 )),1,35) description,
		substr(trim( costcenter.cost_center ) || decode(nvl(trim( name_1 ),' '), ' ','',  ' - ' || trim( nvl(name_1 ,' ') ))||
                                            decode(nvl(trim( name_2 ),' '), ' ','',  ' - ' || trim( nvl(name_2 ,' ') )),1,254)  description
      		from major_location, 
		(
		select cost_center,
					max(company) company,
					max(state) state,
					max(zip_code) zip_code,
					max(geo) geo,
					max(county) county,
					max(ref_county) ref_county,
					max(name_1) name_1,
					max(name_2) name_2,
					max(address) address,
					max(city) city,
					max(pc_code) pc_code 
		from oxy_stage_cost_center 
				 group by cost_center
		) costcenter
		where major_location.external_location_id =  trim( costcenter.cost_center )
		and   
		(
		substr(trim( costcenter.cost_center ) || decode(nvl(trim( name_1 ),' '), ' ','', ' - ' || trim( name_1 )),1,35) <> major_location.description or 
		substr(trim( costcenter.cost_center ) || decode(nvl(trim( name_1 ),' '), ' ','',  ' - ' || trim( nvl(name_1 ,' ') ))||
                                            decode(nvl(trim( name_2 ),' '), ' ','',  ' - ' || trim( nvl(name_2 ,' ') )),1,254)    <> major_location.description)
	) y
	where y.major_location_id = z.major_location_id
)
where major_location_id in (
	select major_location_id
	from major_location, 
	(
	select cost_center,
				max(company) company,
				max(state) state,
				max(zip_code) zip_code,
				max(geo) geo,
				max(county) county,
				max(ref_county) ref_county,
				max(name_1) name_1,
				max(name_2) name_2,
				max(address) address,
				max(city) city,
				max(pc_code) pc_code 
	from oxy_stage_cost_center 
			 group by cost_center
	) costcenter
	where major_location.external_location_id =  trim( costcenter.cost_center )
	and   
	(
	substr(trim( costcenter.cost_center ) || decode(nvl(trim( name_1 ),' '), ' ','', ' - ' || trim( name_1 )),1,35) <> major_location.description or 
	substr(trim( costcenter.cost_center ) || decode(nvl(trim( name_1 ),' '), ' ','',  ' - ' || trim( nvl(name_1 ,' ') ))||
                                            decode(nvl(trim( name_2 ),' '), ' ','',  ' - ' || trim( nvl(name_2 ,' ') )),1,254)  <> major_location.description)
)
;
rtn_str = uf_check_sql('--update major location description : ')
if rtn_str <> 'ok' then
	this.of_write_log("   " + rtn_str)
	success = false
	goto theend
end if


update major_location z
set address = 
(
	select address from (
		select major_location_id, 
		 decode(nvl(trim( costcenter.address ),' '), ' ','', trim( costcenter.address )||' - ')|| nvl( trim( costcenter.city ) , '' ) address
		from major_location, 
		(
		select cost_center,
					max(company) company,
					max(state) state,
					max(zip_code) zip_code,
					max(geo) geo,
					max(county) county,
					max(ref_county) ref_county,
					max(name_1) name_1,
					max(name_2) name_2,
					max(address) address,
					max(city) city,
					max(pc_code) pc_code 
		from oxy_stage_cost_center 
				 group by cost_center
		) costcenter
		where major_location.external_location_id =  trim( costcenter.cost_center )
		and   decode(nvl(trim( costcenter.address ),' '), ' ','', trim( costcenter.address )||' - ')|| nvl( trim( costcenter.city ) , '' ) <> major_location.address
	) y
	where y.major_location_id = z.major_location_id
)
where major_location_id in (
	select major_location_id
	from major_location, 
	(
	select cost_center,
				max(company) company,
				max(state) state,
				max(zip_code) zip_code,
				max(geo) geo,
				max(county) county,
				max(ref_county) ref_county,
				max(name_1) name_1,
				max(name_2) name_2,
				max(address) address,
				max(city) city,
				max(pc_code) pc_code 
	from oxy_stage_cost_center 
			 group by cost_center
	) costcenter
	where major_location.external_location_id =  trim( costcenter.cost_center )
	and   decode(nvl(trim( costcenter.address ),' '), ' ','', trim( costcenter.address )||' - ')|| nvl( trim( costcenter.city ) , '' ) <> major_location.address
)
;

rtn_str = uf_check_sql('--update major location address : ')
if rtn_str <> 'ok' then
	this.of_write_log("   " + rtn_str)
	success = false
	goto theend
end if

update major_location z
set zip_code = 
(
	select zip_code from (
		select major_location_id, 
		  trim( costcenter.zip_code ) zip_code 
		from major_location, 
		(
		select cost_center,
					max(company) company,
					max(state) state,
					max(zip_code) zip_code,
					max(geo) geo,
					max(county) county,
					max(ref_county) ref_county,
					max(name_1) name_1,
					max(name_2) name_2,
					max(address) address,
					max(city) city,
					max(pc_code) pc_code 
		from oxy_stage_cost_center 
				 group by cost_center
		) costcenter
		where major_location.external_location_id =  trim( costcenter.cost_center )
		and    trim( costcenter.zip_code ) <> major_location.zip_code 
	) y
	where y.major_location_id = z.major_location_id
)
where major_location_id in (
	select major_location_id
	from major_location, 
	(
	select cost_center,
				max(company) company,
				max(state) state,
				max(zip_code) zip_code,
				max(geo) geo,
				max(county) county,
				max(ref_county) ref_county,
				max(name_1) name_1,
				max(name_2) name_2,
				max(address) address,
				max(city) city,
				max(pc_code) pc_code 
	from oxy_stage_cost_center 
			 group by cost_center
	) costcenter
	where major_location.external_location_id =  trim( costcenter.cost_center )
	and   trim( costcenter.zip_code ) <> major_location.zip_code 
)
;

rtn_str = uf_check_sql('--update major location zip code : ')
if rtn_str <> 'ok' then
	this.of_write_log("   " + rtn_str)
	success = false
	goto theend
end if

update major_location z
set state_id = 
(
	select state_id from (
		select major_location_id, 
				 trim( st.state_id ) state_id
		from major_location, 
		(
		select cost_center,
					max(company) company,
					max(state) state,
					max(zip_code) zip_code,
					max(geo) geo,
					max(county) county,
					max(ref_county) ref_county,
					max(name_1) name_1,
					max(name_2) name_2,
					max(address) address,
					max(city) city,
					max(pc_code) pc_code 
		from oxy_stage_cost_center 
				 group by cost_center
		) costcenter, state st
		where major_location.external_location_id =  trim( costcenter.cost_center )
		and 	trim( costcenter.state ) = trim(st.state_code)
		and    trim( st.state_id ) <> trim(major_location.state_id)
		
	) y
	where y.major_location_id = z.major_location_id
)
where major_location_id in (
	select major_location_id
	from major_location, 
	(
	select cost_center,
				max(company) company,
				max(state) state,
				max(zip_code) zip_code,
				max(geo) geo,
				max(county) county,
				max(ref_county) ref_county,
				max(name_1) name_1,
				max(name_2) name_2,
				max(address) address,
				max(city) city,
				max(pc_code) pc_code 
	from oxy_stage_cost_center 
			 group by cost_center
	) costcenter, state st
	where major_location.external_location_id =  trim( costcenter.cost_center )
	and 	trim( costcenter.state ) = trim(st.state_code)
	and    trim( st.state_id ) <> trim(major_location.state_id)
)
;

rtn_str = uf_check_sql('--update major location state id : ')
if rtn_str <> 'ok' then
	this.of_write_log("   " + rtn_str)
	success = false
	goto theend
end if

goto theend
theend:
if success then
	return 1
else
	return -1
end if
	


end function

public subroutine of_write_log (string a_message);/*
 *	processing.
 */
	string	display_msg

	

////
////	append time if we need to.
////
//			if a_append_time then
				display_msg = a_message + ":  " + string( today(), "m/d/yyyy 'at' h:mm:ss am/pm" )
//			else
//				display_msg = a_message
//			end if

////
////	write the message to the status box.
////
			f_status_box( "", display_msg )

////
////	put the message in the logs (if they're running).
////
			if not isnull( i_logs_proc_id ) and not isnull( i_logs_occur_id ) then
				insert into pp_processes_messages ( process_id, occurrence_id, msg_order, msg )
					select		:i_logs_proc_id process_id,
								:i_logs_occur_id occurrence_id,
								orderview.new_msg_order msg_order,
								:display_msg msg
					from		(	select		nvl( max( msg_order ), 0 ) + 1 new_msg_order
									from		pp_processes_messages
									where	process_id = :i_logs_proc_id
										and	occurrence_id = :i_logs_occur_id
								) orderview
					using		i_sqlsa;
				
				if i_sqlsa.sqlcode <> 0 then
					f_status_box( "", "logging error: " + i_sqlsa.sqlerrtext )
					rollback using i_sqlsa;
				end if
				
				commit using i_sqlsa;
			end if
end subroutine

public function long uf_read (string a_extract_date);long source_id, rtn, com, counter
string file_name, file_path
boolean success = true

/*
 *	processing.
 */
	long	i, maxi
	long	rows 
	string	err_msg

////
////
////
long		running_session_id, session_id
string 	str_rtn

////
////	set extract_date
////
			extract_date = a_extract_date
////
////	log in to set up the logs.
////
			i_sqlsa = create uo_sqlca_logs
			if i_sqlsa.uf_connect() <> 0 then
				messagebox( "logging set-up error", "error creating a connection to the database (" + i_sqlsa.servername + ") in order to log the running of the load assets interface.  " + &
									"no changes made.~n~nerror: " + i_sqlsa.sqlerrtext )
				i_sqlsa.uf_disconnect()
				destroy i_sqlsa
				return -1
			end if
			
			if i_sqlsa.uf_set_role( "pwrplant_role_dev" ) <> 0 then
				messagebox( "logging set-up error", "error granting the proper database roles during set-up of the load assets interface logging process.  no changes made.~n~nerror: " + &
									i_sqlsa.sqlerrtext )
				i_sqlsa.uf_disconnect()
				destroy i_sqlsa
				return -1
			end if

////
////	get the process and occurrence information for the logs.
////
			setnull( i_logs_proc_id )
			select		process_id
			into		:i_logs_proc_id
			from		pp_processes
			where	trim( upper( description ) ) = trim( upper( :i_logs_proc_desc ) )
			using		i_sqlsa;
			
			if isnull( i_logs_proc_id ) then
				select		nvl( max( process_id ), 0 ) + 1
				into		:i_logs_proc_id
				from		pp_processes
				using		i_sqlsa;
				
				insert into pp_processes ( process_id, description, description )
					values ( :i_logs_proc_id, :i_logs_proc_desc, :i_logs_proc_desc )
					using i_sqlsa;
				
				if i_sqlsa.sqlcode <> 0 then
					messagebox( "logging set-up error", "unable to find and/or create the logging process under which the progress of the load assets interface program will be saved.  " + &
										"no changes made.~n~nerror: " + i_sqlsa.sqlerrtext )
					rollback using i_sqlsa;
					i_sqlsa.uf_disconnect()
					destroy i_sqlsa
					return -1
				end if
			end if
			
			setnull( i_logs_occur_id )
			select		nvl( max( occurrence_id ), 0 ) + 1
			into		:i_logs_occur_id
			from		pp_processes_occurrences
			where	process_id = :i_logs_proc_id
			using		i_sqlsa;
			
			if isnull( i_logs_occur_id ) or i_logs_occur_id = 0 then
				messagebox( "logging set-up error", "unable to obtain a new logging occurrence number under which the progress of the load assets interface program can be saved.  " + &
									"no changes made." )
				i_sqlsa.uf_disconnect()
				destroy i_sqlsa
				return -1
			end if
			
			insert into pp_processes_occurrences ( process_id, occurrence_id, start_time )
				values ( :i_logs_proc_id, :i_logs_occur_id, sysdate )
				using i_sqlsa;
			
			if i_sqlsa.sqlcode <> 0 then
				messagebox( "logging set-up error", "unable to create a new logging occurrence in the database under which the progress of the load assets interface program can be " + &
				 					"saved.  no changes made.~n~nerror: " + i_sqlsa.sqlerrtext )
				rollback using i_sqlsa;
				i_sqlsa.uf_disconnect()
				destroy i_sqlsa
				return -1
			end if
			
			commit using i_sqlsa;

////
////	tell the user what we're doing.
////
			this.of_write_log( "starting")
			
			
//*****************************************************************************************
//
//  checkrunning session id to see if the interface is already running
//
//*****************************************************************************************
	running_session_id = 0
	select running_session_id into :running_session_id from pp_processes
	 where process_id = :i_logs_proc_id;
	
	if isnull(running_session_id) or running_session_id = 0 then
		//  not currently running:  update the record with this session_id.
		select userenv('sessionid') into :session_id from dual;
		
		update pp_processes 
		set running_session_id = :session_id
		where process_id = :i_logs_proc_id;
		
		if sqlca.sqlcode = 0 then
			commit;
			
		else
			this.of_write_log('  error: updating pp_processes.running_session_id: ' + &
				sqlca.sqlerrtext)		
			rollback;		
			
			destroy i_sqlsa
			return -1
			
		end if
			
	else
		
		this.of_write_log(' ')
		this.of_write_log(' ')
		this.of_write_log('another session is currently running this process.')
		this.of_write_log('  session_id = ' + string(running_session_id))
		this.of_write_log(' ')
		this.of_write_log(' ')
					
		// exit via the close() event
		success = false
		goto theend
		
	end if






/*********************************************************************************/

this.of_write_log("-----------------------------------------------------------------------------")
this.of_write_log("   *****  begin: find files " + string(now()) + "  *****")
this.of_write_log("  ")
rtn = uf_find_files()
if rtn <> 1 then
	success = false
	goto theend
end if
this.of_write_log("  ")
this.of_write_log("   *****  end: find files " + string(now()) + "  *****")
this.of_write_log("-----------------------------------------------------------------------------")

/*********************************************************************************/

//this.of_write_log("-----------------------------------------------------------------------------")
//this.of_write_log("   *****  begin: loading cost center files " + string(now()) + "  *****")
//this.of_write_log("  ")
//rtn = uf_cost_center_load()
//if rtn <> 1 then
//	success = false
//	goto theend
//end if
//this.of_write_log("  ")
//this.of_write_log("   *****  end: loading cost center files " + string(now()) + "  *****")
//this.of_write_log("-----------------------------------------------------------------------------")
//
/*********************************************************************************/
this.of_write_log("-----------------------------------------------------------------------------")
this.of_write_log("   *****  begin: loading asset files " + string(now()) + "  *****")
this.of_write_log("  ")
rtn = uf_import_01_asset_files()
if rtn <> 1 then
	success = false
	goto theend
end if
this.of_write_log("  ")
this.of_write_log("   *****  end: loading asset files " + string(now()) + "  *****")
this.of_write_log("-----------------------------------------------------------------------------")


///*********************************************************************************/
//this.of_write_log("-----------------------------------------------------------------------------")
//this.of_write_log("   *****  begin: removing asset groups for non-jobbers  " + string(now()) + "  *****")
//this.of_write_log("  ")
//
//
///* we remove the asset groups here so they do not cause confusion when users are looking at asset groups
//    through the update window.   asset group can only be used if it is in fact a true jobber.   in this way, if it really
//	 is a jobber, the users will be able to enter that in the asset group field and override what is in sap */
//update oxy_stage_assets
//set asset_group = null
//where company = '0061'
//and cost_center like 'dcm%'
//and substr(cost_center,4,1) not between '1' and '9'
//;
//
//if sqlca.sqlcode = 0 then
//	commit;
//else
//	this.of_write_log("error: nulling asset group for non-jobbers: " + &
//		sqlca.sqlerrtext)
//	this.of_write_log(" ")
//	this.of_write_log("the process did run.")
//	this.of_write_log("the the pp_processes table must be updated by hand.")
//	this.of_write_log(" ")
//	rollback;
//end if
//
//this.of_write_log("  ")
//this.of_write_log("   *****  end: removing asset groups for non-jobbers " + string(now()) + "  *****")
//this.of_write_log("-----------------------------------------------------------------------------")
///*********************************************************************************/

/*********************************************************************************/
//this.of_write_log("-----------------------------------------------------------------------------")
//this.of_write_log("   *****  begin: use cc state, county and zip code for specified cost centers  " + string(now()) + "  *****")
//this.of_write_log("  ")
//
///* we update the state, county and zip code based on the overrides provided - they will get overridden again if they 
//are facility related, pipeline related or manually overridden.  this way, the data can be validated downstream and updated if needed  */
//update oxy_stage_assets a
//set (state, county, geo, zip_code) = 
//	(select state, county, geo, zip_code
//	from oxy_stage_cost_center b,
//		    oxy_ashell_use_costctr_addr c
//	where b.cost_center = c.cost_center
//	and	 a.cost_center = c.cost_center
//	)
//where exists
//	(select 1
//	from oxy_stage_cost_center b,
//		    oxy_ashell_use_costctr_addr c
//	where b.cost_center = c.cost_center
//	and	 a.cost_center = c.cost_center
//	);
//
//if sqlca.sqlcode = 0 then
//	commit;
//else
//	this.of_write_log("error: updating tax juris data from the cost centernulling asset group for non-jobbers: " + &
//		sqlca.sqlerrtext)
//	this.of_write_log(" ")
//	this.of_write_log("the process did run.")
//	this.of_write_log("the the pp_processes table must be updated by hand.")
//	this.of_write_log(" ")
//	rollback;
//end if
//
//this.of_write_log("  ")
//this.of_write_log("   *****  end: use cc state, county and zip code for specified cost centers " + string(now()) + "  *****")
//this.of_write_log("-----------------------------------------------------------------------------")
///*********************************************************************************/
//
//
///*********************************************************************************/
//this.of_write_log("-----------------------------------------------------------------------------")
//this.of_write_log("   *****  begin: update facility number as needed  " + string(now()) + "  *****")
//this.of_write_log("  ")
//
//update oxy_stage_assets
//set facility_number = asset_group
//where company = '0061'
//and cost_center like 'dcm%'
//and substr(cost_center,4,1) between '1' and '9'
//;
//
//if sqlca.sqlcode = 0 then
//	commit;
//else
//	this.of_write_log("error: update facility number for jobbers: " + &
//		sqlca.sqlerrtext)
//	this.of_write_log(" ")
//	this.of_write_log("the process did run.")
//	this.of_write_log("the the pp_processes table must be updated by hand.")
//	this.of_write_log(" ")
//	rollback;
//end if
//
//update oxy_stage_assets
//set facility_number = to_char(to_number(trim(substr(cost_center,5))))
//where company = '0061'
//and cost_center like 'dcm0%'
//and facility_number is null
//;
//
//if sqlca.sqlcode = 0 then
//	commit;
//else
//	this.of_write_log("error: update facility number for non-jobbers: " + &
//		sqlca.sqlerrtext)
//	this.of_write_log(" ")
//	this.of_write_log("the process did run.")
//	this.of_write_log("the the pp_processes table must be updated by hand.")
//	this.of_write_log(" ")
//	rollback;
//end if
//this.of_write_log("  ")
//this.of_write_log("   *****  end: update facility number as needed " + string(now()) + "  *****")
//this.of_write_log("-----------------------------------------------------------------------------")
/*********************************************************************************/

/*********************************************************************************/
//this.of_write_log("-----------------------------------------------------------------------------")
//this.of_write_log("   *****  begin: translate county code  " + string(now()) + "  *****")
//this.of_write_log("  ")
//
///* update now so users can edit through the windows as needed */
//update	oxy_stage_assets
//set			county =
//			(select to_county_code
//			from	 oxy_ashell_taxware_cnty_codes
//			where trim(oxy_stage_assets.state) = trim(oxy_ashell_taxware_cnty_codes.state_code)
//			and	 trim(oxy_stage_assets.county) = trim(oxy_ashell_taxware_cnty_codes.from_county_code)
//			)
//where	nvl( loaded, 0 ) = 0
//	and 	exists
//			(select 1
//			from	 oxy_ashell_taxware_cnty_codes
//			where trim(oxy_stage_assets.state) = trim(oxy_ashell_taxware_cnty_codes.state_code)
//			and	 trim(oxy_stage_assets.county) = trim(oxy_ashell_taxware_cnty_codes.from_county_code)
//			);
//
//if sqlca.sqlcode = 0 then
//	commit;
//else
//	this.of_write_log("error: translate county code from oxy_ashell_taxware_cnty_codes " + &
//		sqlca.sqlerrtext)
//	this.of_write_log(" ")
//	this.of_write_log("the process did run.")
//	this.of_write_log("the the pp_processes table must be updated by hand.")
//	this.of_write_log(" ")
//	rollback;
//end if
//this.of_write_log("  ")
//this.of_write_log("   *****  end: translate county code " + string(now()) + "  *****")
//this.of_write_log("-----------------------------------------------------------------------------")
/*********************************************************************************/

/*stolen from close event of stand alone interface:*/

theend:

long os_return_value
string   s_date
datetime finished_at
date	ddate
time	ttime

s_date = string(today(), "yyyy-mm-dd hh:mm:ss")
ddate  = date(left(s_date, 10))
ttime  = time(right(s_date, 8))

//  update pp_processes to show this session is finished
//  make sure to only update pp_processes if this process is the current running process
update pp_processes set running_session_id = null
where process_id = :i_logs_proc_id
and running_session_id = userenv('sessionid')
;

if sqlca.sqlcode = 0 then
	commit;
else
	this.of_write_log("error: updating pp_processes.running_session_id: " + &
		sqlca.sqlerrtext)
	this.of_write_log(" ")
	this.of_write_log("the process did run.")
	this.of_write_log("the the pp_processes table must be updated by hand.")
	this.of_write_log(" ")
	rollback;
end if


finished_at   = datetime(ddate, ttime)
g_finished_at = finished_at

update pp_processes_occurrences 
   set end_time = :g_finished_at
 where process_id = :i_logs_proc_id and occurrence_id = :i_logs_occur_id
 using i_sqlsa;
	
if i_sqlsa.sqlcode = 0 then
	commit 	using i_sqlsa;
else
	rollback 	using i_sqlsa;
end if


//*****************************************************************************************
//
//	disconnect the logs connection
//
//*****************************************************************************************

i_sqlsa.uf_disconnect()

if success then
	return 1
else
	return -1
end if
end function

on uo_stage_sap_files.create
call super::create
triggerevent( this, "constructor" )
end on

on uo_stage_sap_files.destroy
triggerevent( this, "destructor" )
call super::destroy
end on

event constructor;i_exe_name = 'oxy_asset_shell_import.exe'
end event

