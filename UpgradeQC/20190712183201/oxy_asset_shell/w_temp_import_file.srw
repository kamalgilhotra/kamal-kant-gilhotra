forward
global type w_temp_import_file from window
end type
type lb_1 from listbox within w_temp_import_file
end type
type st_2 from statictext within w_temp_import_file
end type
type st_1 from statictext within w_temp_import_file
end type
type sle_1 from singlelineedit within w_temp_import_file
end type
type cb_1 from commandbutton within w_temp_import_file
end type
end forward

global type w_temp_import_file from window
integer width = 2199
integer height = 1004
boolean titlebar = true
string title = "book 80 class code asset tag"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
67108864
string icon = "appicon!"
boolean center = true
lb_1 lb_1
st_2 st_2
st_1 st_1
sle_1 sle_1
cb_1 cb_1
end type
global w_temp_import_file w_temp_import_file

type variables
uo_sqlca_logs	i_sqlsa

////
////	all our logs variables.
////
	long	i_logs_proc_id
	long	i_logs_occur_id
	string	i_logs_proc_desc = 'asset shell - load assets'
	string	i_logs_proc_desc = 'process for loading assets from sap/jde into the asset shell'
end variables

forward prototypes
public function long wf_asset_shell_import (string file_name, string file_path, string original_file_name)
public subroutine wf_load_em ()
end prototypes

public function long wf_asset_shell_import (string file_name, string file_path, string original_file_name);boolean success = true
string fixed_string, sqls,sysdate
long file_number, rtn, i, numrows, max_dk

string extract_date,company,cost_center,asset_main,asset_sub,cap_on_date,state,zip_code,geo,county,plant,location, &
		room,asset_group,asset_class,eval_group5,quantity,unit,cost,eval_group1,eval_group2,eval_group3,reason,inventory_num, &
		license_num,asset_main_txt,asset_name1,asset_name2,environ_invest,ref_state,ref_zip_code,ref_geo,ref_county
uo_ds_top ds_temp
ds_temp = create uo_ds_top

sqls = "truncate table oxy_stage_assets_nopk"
execute immediate :sqls;

file_number = fileopen(file_name)
if file_number = -1 then
	f_status_box('','-- file not found! : ' + file_name)
	success = false
	goto theend
elseif isnull(file_number) then
	f_status_box('','-- null value passed! : ' + file_name)
	success = false
	goto theend
end if

//f_status_box('import file', string(now()) + '... reading file ... ')
do while true
	rtn = fileread(file_number, fixed_string)
	if rtn <= 0 then exit
	i++
	extract_date = trim(mid(fixed_string,1,10))
	company = trim(mid(fixed_string,11,4))
	cost_center = trim(mid(fixed_string,15,10))
	asset_main = trim(mid(fixed_string,25,12))
	asset_sub = trim(mid(fixed_string,37,4))
	cap_on_date = trim(mid(fixed_string,41,10))
	state = trim(mid(fixed_string,51,2))
	zip_code = trim(mid(fixed_string,53,5))
	geo = trim(mid(fixed_string,58,2))
	county = trim(mid(fixed_string,60,3))
	plant = trim(mid(fixed_string,66,4))
	location = trim(mid(fixed_string,70,10))
	room = trim(mid(fixed_string,80,8))
	asset_group = trim(mid(fixed_string,88,12))
	asset_class = trim(mid(fixed_string,100,8))
	eval_group5 = trim(mid(fixed_string,108,8))
	quantity = trim(mid(fixed_string,116,13))
	unit = trim(mid(fixed_string,129,3))
	cost = trim(mid(fixed_string,132,13))
	eval_group1 = trim(mid(fixed_string,145,4))
	eval_group2 = trim(mid(fixed_string,149,4))
	eval_group3 = trim(mid(fixed_string,153,4))
	reason = trim(mid(fixed_string,157,3))
	inventory_num = trim(mid(fixed_string,160,25))
	license_num = trim(mid(fixed_string,185,15))
	asset_main_txt = trim(mid(fixed_string,200,50))
	asset_name1 = trim(mid(fixed_string,250,50))
	asset_name2 = trim(mid(fixed_string,300,50))
	environ_invest = trim(mid(fixed_string,350,5))
	ref_state = trim(mid(fixed_string,355,2))
	ref_zip_code = trim(mid(fixed_string,357,5))
	ref_geo = trim(mid(fixed_string,362,2))
	ref_county = trim(mid(fixed_string,364,3))
	
	insert into oxy_stage_assets_nopk
	(
	extract_date,company,cost_center,asset_main,asset_sub,cap_on_date,state,zip_code,geo,county,plant,location,room,
	asset_group,asset_class,eval_group5,quantity,unit,cost,eval_group1,eval_group2,eval_group3,reason,inventory_num,
	license_num,asset_main_txt,asset_name1,asset_name2,environ_invest,ref_state,ref_zip_code,ref_geo,ref_county
	) values
	(
	:extract_date,:company,:cost_center,:asset_main,:asset_sub,:cap_on_date,:state,:zip_code,:geo,:county,:plant,:location,:room,
	:asset_group,:asset_class,:eval_group5,:quantity,:unit,:cost,:eval_group1,:eval_group2,:eval_group3,:reason,:inventory_num,
	:license_num,:asset_main_txt,:asset_name1,:asset_name2,:environ_invest,:ref_state,:ref_zip_code,:ref_geo,:ref_county
	);
	
	if sqlca.sqlcode <> 0 then
		f_status_box('','--insert into staging no-pk failed')
		f_status_box('',"  ora error : " + sqlca.sqlerrtext)
		success = false
		goto theend
	else
		if mod(i,1000) = 0 then
			commit;
		end if
		if mod(i,10000) = 0 then
			f_status_box('','--loaded ... ' + string(i) + ' ... rows ... commit')
		end if
	end if
loop

commit;
f_status_box('','--loaded ... ' + string(i) + ' ... rows ... commit')

delete from oxy_stage_assets_nopk where company in ('eira','quei');
if sqlca.sqlcode <> 0 then
	f_status_box('','--deleteing garbage company records failed:')
	f_status_box('',"  ora error : " + sqlca.sqlerrtext)
	success = false
	goto theend
end if
			
/* make sure that there are no duplicate rows by primary key of oxy_stage_assets */
select count(*) into :rtn
from (
	select company, asset_main, asset_sub, count(1) 
	from oxy_stage_assets_nopk
	group by company, asset_main, asset_sub
	having count(1) > 1
);

if rtn > 0 then
	f_status_box('','--**invalid file: file contains duplicate rows. this file will not be loaded into staging table')
	sqls = + &
	" select company, asset_main, asset_sub, count(1) " + &
	" from oxy_stage_assets_nopk " + &
	" group by company, asset_main, asset_sub " + &
	" having count(1) > 1 "
	sqls = f_create_dynamic_ds(ds_temp,'grid', sqls,sqlca,true)
	if sqls = 'ok' then
		f_status_box('','--       details:')
		for i = 1 to ds_temp.rowcount()
			company = ds_temp.getitemstring(i,1)
			asset_main = ds_temp.getitemstring(i,2)
			asset_sub = ds_temp.getitemstring(i,3)
			f_status_box('','--       company: ' + company + ' asset main: ' + asset_main + ' asset sub: ' + asset_sub)
		next
	end if
	success = false 
	goto theend
end if 

insert into oxy_stage_assets
(extract_date,company,cost_center,asset_main,asset_sub,cap_on_date,state,zip_code,geo,county,plant,location,room,
asset_group,asset_class,eval_group5,quantity,unit,cost,eval_group1,eval_group2,eval_group3,reason,inventory_num,
license_num,asset_main_txt,asset_name1,asset_name2,environ_invest,ref_state,ref_zip_code,ref_geo,ref_county)
select extract_date,company,cost_center,asset_main,asset_sub,cap_on_date,state,zip_code,geo,county,plant,location,room,
asset_group,asset_class,eval_group5,quantity,unit,cost,eval_group1,eval_group2,eval_group3,reason,inventory_num,
license_num,asset_main_txt,asset_name1,asset_name2,environ_invest,ref_state,ref_zip_code,ref_geo,ref_county 
from oxy_stage_assets_nopk;

if sqlca.sqlcode <> 0 then
	f_status_box('','--insert into staging failed:')
	f_status_box('',"  ora error : " + sqlca.sqlerrtext)
	success = false
	goto theend
end if
theend:
fileclose(file_number)
destroy ds_temp
if success then 
	commit;
	f_status_box('','--load successfully')
	f_status_box('','--renaming file to: '+file_path+sysdate+'_'+original_file_name)
	sysdate	= string(today(),"yyyymmdd")+"_"+string(now(),"hhmm")
	//f_file_rename(file_name,file_path+sysdate+'_'+original_file_name)
	return 1
else
	rollback;
	f_status_box('','--load failed')
	return -1
end if
	
end function

public subroutine wf_load_em ();string sqls 
update	oxy_stage_assets stage
set			stage.ldg_serial_number = trim( stage.company ) || '-' || trim( stage.asset_main ) || '-' || trim( stage.asset_sub )
where	nvl( stage.loaded, 0 ) = 0
	and	lengthb( trim( stage.company ) || '-' || trim( stage.asset_main ) || '-' || trim( stage.asset_sub ) ) <= 35;
if sqlca.sqlcode <> 0 then
	f_status_box('','--update serial number failed:')
	f_status_box('',"  ora error : " + sqlca.sqlerrtext)
	rollback;
	return
end if

update oxy_stage_assets 
set errored = -1, error_message = 'cannot find asset in cpr ledger!!'
where ldg_serial_number in (
select ldg_serial_number from oxy_stage_assets 
minus
select serial_number from cpr_ledger)
;
if sqlca.sqlcode <> 0 then
	f_status_box('','--update error message failed:')
	f_status_box('',"  ora error : " + sqlca.sqlerrtext)
	rollback;
	return
end if

sqls = 'truncate table oxy_temp_table_al'
execute immediate :sqls;

insert into oxy_temp_table_al (company, asset_main, asset_sub, asset_location_id)
	select company, asset_main, asset_sub, a.asset_id 
	from cpr_ledger a, oxy_stage_assets b
	where a.serial_number = b.ldg_serial_number
	and abs(to_number(trim(cost))) > 0 
;

update oxy_stage_assets z
set ldg_asset_id = 
(
	select asset_location_id from oxy_temp_table_al y
	where y.company = z.company
	and y.asset_main = z.asset_main
	and y.asset_sub = z.asset_sub
)
where (company, asset_main, asset_sub) in 
(
	select company, asset_main, asset_sub  from oxy_temp_table_al y
);

if sqlca.sqlcode <> 0 then
	f_status_box('','--update asset id failed:')
	f_status_box('',"  ora error : " + sqlca.sqlerrtext)
	rollback;
	return
end if

delete from class_code_cpr_ledger where class_code_id = 3;
if sqlca.sqlcode <> 0 then
	f_status_box('','--delete from class code cpr ledger failed:')
	f_status_box('',"  ora error : " + sqlca.sqlerrtext)
	rollback;
	return
end if

insert into class_code_cpr_ledger
(class_code_id, asset_id, value) 
select 3, ldg_asset_id, 'no' from oxy_stage_assets
where ldg_asset_id is not null
;
if sqlca.sqlcode <> 0 then
	f_status_box('','--insert into class code cpr ledger (no) failed:')
	f_status_box('',"  ora error : " + sqlca.sqlerrtext)
	rollback;
	return
end if

insert into class_code_cpr_ledger 
(class_code_id, asset_id, value) 
select 3, asset_id, 'yes' from 
(
	select asset_id from cpr_ledger 
	minus
	select asset_id from class_code_cpr_ledger where class_code_id = 3
);
if sqlca.sqlcode <> 0 then
	f_status_box('','--insert into class code cpr ledger (no) failed:')
	f_status_box('',"  ora error : " + sqlca.sqlerrtext)
	rollback;
	return
end if

commit;
end subroutine

on w_temp_import_file.create
this.lb_1=create lb_1
this.st_2=create st_2
this.st_1=create st_1
this.sle_1=create sle_1
this.cb_1=create cb_1
this.control[]={this.lb_1,&
this.st_2,&
this.st_1,&
this.sle_1,&
this.cb_1}
end on

on w_temp_import_file.destroy
destroy(this.lb_1)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.sle_1)
destroy(this.cb_1)
end on

type lb_1 from listbox within w_temp_import_file
integer x = 110
integer y = 492
integer width = 1925
integer height = 324
integer taborder = 20
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "ms sans serif"
33554432
borderstyle borderstyle = stylelowered!
end type

type st_2 from statictext within w_temp_import_file
integer x = 105
integer y = 428
integer width = 1925
integer height = 52
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "ms sans serif"
33554432
67108864
string text = "file name"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_1 from statictext within w_temp_import_file
integer x = 110
integer y = 256
integer width = 1925
integer height = 52
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "ms sans serif"
33554432
67108864
string text = "path"
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_1 from singlelineedit within w_temp_import_file
integer x = 110
integer y = 320
integer width = 1925
integer height = 92
integer taborder = 10
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "ms sans serif"
33554432
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_temp_import_file
integer x = 64
integer y = 108
integer width = 731
integer height = 92
integer taborder = 10
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "ms sans serif"
string text = "book 80 load ..."
end type

event clicked;long rtn, i
string docpath, docname[]
string file_path

rtn = getfileopenname("select file", &
  docpath, docname[], "txt", &
   + "text files (*.txt),*.txt," &
   + "all files (*.*), *.*", &
   "h:\ptax\powerplant\data\asset shell\book_80_inbound", 18)
choose case rtn
	case 1
		sle_1.text= docpath
		for i = 1 to upperbound(docname[])
			lb_1.additem(docname[i])
		next
	case 0
		return
	case -1
		messagebox('error', 'get file failed')
		return
	case else
		messagebox('error', "one or more argument's value is null")
		return
end choose

select substr(:docpath, 1, decode(sign(instr(:docpath, :docname[1]) - 2), 1,instr(:docpath, :docname[1]) - 2,length(:docpath))) into :file_path from dual;

/* archive off whatever is in  oxy_stage_assets*/
f_pp_msgs('archiving existing records in oxy_stage_assets ...')
insert into oxy_stage_assets_archive
(extract_date,company,cost_center,asset_main,asset_sub,cap_on_date,
state,zip_code,geo,county,plant,location,room,asset_group,asset_class,
eval_group5,quantity,unit,cost,eval_group1,eval_group2,eval_group3,reason,
inventory_num,license_num,asset_main_txt,asset_name1,asset_name2,environ_invest,
ref_state,ref_zip_code,ref_geo,ref_county,ldg_asset_id,ldg_books_schema_id,ldg_company_id,
ldg_gl_account_id,ldg_bus_segment_id,ldg_utility_account_id,ldg_sub_account_id,
ldg_retirement_unit_id,ldg_property_group_id,ldg_func_class_id,ldg_depr_group_id,
ldg_asset_location_id,ldg_in_service_year,ldg_eng_in_service_year,ldg_ledger_status,
ldg_subledger_indicator,ldg_work_order_number,ldg_description,ldg_description,
ldg_serial_number,act_asset_id,act_asset_activity_id,act_gl_posting_mo_yr,
act_out_of_service_mo_yr,act_cpr_posting_mo_yr,act_work_order_number,act_gl_je_code,
act_description,act_description,act_disposition_code,act_activity_code,
act_activity_status,act_activity_quantity,act_activity_cost,act_ferc_activity_code,
act_month_number,trans_from_asset_id,trans_to_asset_id,trans_asset_activity_id,
trans_gl_posting_mo_yr,trans_out_of_service_mo_yr,trans_cpr_posting_mo_yr,
trans_work_order_number,trans_gl_je_code,trans_description,trans_description,
trans_disposition_code,trans_activity_code,trans_activity_status,trans_activity_quantity,
trans_activity_cost,trans_ferc_activity_code,trans_month_number,loaded,
errored,error_message,
arch_batch_id,archive_date)
select 
extract_date,company,cost_center,asset_main,asset_sub,cap_on_date,
state,zip_code,geo,county,plant,location,room,asset_group,asset_class,
eval_group5,quantity,unit,cost,eval_group1,eval_group2,eval_group3,reason,
inventory_num,license_num,asset_main_txt,asset_name1,asset_name2,environ_invest,
ref_state,ref_zip_code,ref_geo,ref_county,ldg_asset_id,ldg_books_schema_id,ldg_company_id,
ldg_gl_account_id,ldg_bus_segment_id,ldg_utility_account_id,ldg_sub_account_id,
ldg_retirement_unit_id,ldg_property_group_id,ldg_func_class_id,ldg_depr_group_id,
ldg_asset_location_id,ldg_in_service_year,ldg_eng_in_service_year,ldg_ledger_status,
ldg_subledger_indicator,ldg_work_order_number,ldg_description,ldg_description,
ldg_serial_number,act_asset_id,act_asset_activity_id,act_gl_posting_mo_yr,
act_out_of_service_mo_yr,act_cpr_posting_mo_yr,act_work_order_number,act_gl_je_code,
act_description,act_description,act_disposition_code,act_activity_code,
act_activity_status,act_activity_quantity,act_activity_cost,act_ferc_activity_code,
act_month_number,trans_from_asset_id,trans_to_asset_id,trans_asset_activity_id,
trans_gl_posting_mo_yr,trans_out_of_service_mo_yr,trans_cpr_posting_mo_yr,
trans_work_order_number,trans_gl_je_code,trans_description,trans_description,
trans_disposition_code,trans_activity_code,trans_activity_status,trans_activity_quantity,
trans_activity_cost,trans_ferc_activity_code,trans_month_number,loaded,
errored,error_message, 
arch_b_id, sysdate
from oxy_stage_assets,
	(select (max(arch_batch_id) + 1) arch_b_id from oxy_stage_assets_archive)
;
if sqlca.sqlcode <> 0 then
	messagebox('error', 'archive insert failed: ora error : ' + sqlca.sqlerrtext)
	return
//	f_pp_msgs('--insert into archiving failed:')
//	f_pp_msgs("  ora error : " + sqlca.sqlerrtext)
//	success = false
//	goto theend
end if

/*truncate  oxy_stage_assets*/
execute immediate " truncate table oxy_stage_assets ";
if sqlca.sqlcode <> 0 then
	messagebox('error', 'stage table truncate failed: ora error : ' + sqlca.sqlerrtext)
	return
//	f_pp_msgs('--stage truncating failed:')
//	f_pp_msgs("  ora error : " + sqlca.sqlerrtext)
//	success = false
//	goto theend

end if



for i = 1 to upperbound(docname[])
	rtn = wf_asset_shell_import(file_path+'\'+docname[i],file_path+'\', docname[i])
	if rtn <> 1 then
		return
	end if 
next

wf_load_em()
end event

