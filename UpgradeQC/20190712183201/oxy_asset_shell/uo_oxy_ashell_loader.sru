forward
global type uo_oxy_ashell_loader from nonvisualobject
end type
end forward

global type uo_oxy_ashell_loader from nonvisualobject
end type
global uo_oxy_ashell_loader uo_oxy_ashell_loader

type variables
////
////	the logs connection object.
////
	uo_sqlca_logs	i_sqlsa

////
////	all our logs variables.
////
	long	i_logs_proc_id
	long	i_logs_occur_id
	string	i_logs_proc_desc = 'asset shell - load assets'
	string	i_logs_proc_desc = 'process for loading assets from sap/jde into the asset shell'
end variables

forward prototypes
public subroutine of_writelog (string a_message, boolean a_append_time)
public function long of_translatefields (string a_gl_company, ref string a_msg)
public function long of_initializeload (string a_gl_company, ref string a_msg)
public function long of_computeactivity (string a_gl_company, ref string a_msg)
public function long of_populatecontroltables (string a_gl_company, ref string a_msg)
public function long of_postactivity (string a_gl_company, ref string a_msg)
public function long of_importfacilities (ref string a_msg)
public function long uf_minor_location ()
public function long uf_translate_asset_loc (string a_gl_company)
public function long uf_load_asset_location (string a_gl_company)
public function long uf_computetransfer (string a_gl_company, ref string a_msg)
public function long uf_posttransfer (string a_gl_company, ref string a_msg)
public function long uf_computeretire_add (string a_gl_company, ref string a_msg)
public function long uf_postretire_add (string a_gl_company, ref string a_msg)
public function long of_ref_override (string a_gl_company, ref string a_msg)
public function long of_populate_ref_ov_tbl (string a_gl_company, ref string a_msg, long a_fails_found)
public function long uf_classcode_add (string a_gl_company, ref string a_msg)
public function long of_run (string a_gl_company[], string a_run_type)
public function long uf_final_asset_location_updates ()
public function integer of_translatecountycode (string a_gl_company, ref string a_msg)
public function long uf_final_company_updates ()
end prototypes

public subroutine of_writelog (string a_message, boolean a_append_time);/*
 *	processing.
 */
	string	display_msg

////
////	append time if we need to.
////
			if a_append_time then
				display_msg = a_message + ":  " + string( today(), "m/d/yyyy 'at' h:mm:ss am/pm" )
			else
				display_msg = a_message
			end if

////
////	write the message to the status box.
////
			f_status_box( "", display_msg )

////
////	put the message in the logs (if they're running).
////
			if not isnull( i_logs_proc_id ) and not isnull( i_logs_occur_id ) then
				insert into pp_processes_messages ( process_id, occurrence_id, msg_order, msg )
					select		:i_logs_proc_id process_id,
								:i_logs_occur_id occurrence_id,
								orderview.new_msg_order msg_order,
								:display_msg msg
					from		(	select		nvl( max( msg_order ), 0 ) + 1 new_msg_order
									from		pp_processes_messages
									where	process_id = :i_logs_proc_id
										and	occurrence_id = :i_logs_occur_id
								) orderview
					using		i_sqlsa;
				
				if i_sqlsa.sqlcode <> 0 then
					f_status_box( "", "logging error: " + i_sqlsa.sqlerrtext )
					rollback using i_sqlsa;
				end if
				
				commit using i_sqlsa;
			end if
end subroutine

public function long of_translatefields (string a_gl_company, ref string a_msg);////
////	tell the user what we're doing.
////

////			
////	books_schema_id, func_class_id, ledger_status, property_group_id, subledger_indicator, work_order_number
////
			this.of_writelog( "      - translating fixed values", true )
			
			update	oxy_stage_assets stage
			set			stage.ldg_books_schema_id = 1,
						stage.ldg_func_class_id = 1,
						stage.ldg_ledger_status = 1,
						stage.ldg_property_group_id = 100,
						stage.ldg_subledger_indicator = 0,
						stage.ldg_work_order_number = null
			where	    stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during translation of fixed values ('books schema', 'func class', 'ledger status', 'property group', 'subledger indicator', and 'work " + &
					"order number') for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

//////
//////	fail records that have a non-numeric quanity or cost
//////
			this.of_writelog( "      - marking invalid cost, quantity", true )
			
			update	oxy_stage_assets stage
			set			stage.errored = 1,
						stage.error_message = nvl(stage.error_message,'')|| ', non-numeric quantity or cost field field encountered! '
			where stage.company = :a_gl_company
			and nvl(loaded,0) = 0
			and (is_number(cost) <> 1
				or
				is_number(quantity) <> 1 ) ;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error failing non-numeric cost/quantity for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if


////
////	serial_number
////
			this.of_writelog( "      - translating serial number", true )
			
			update	oxy_stage_assets stage
			set			stage.ldg_serial_number = trim( stage.company ) || '-' || trim( stage.asset_no )
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0
				and	lengthb( trim( stage.company ) || '-' || trim( stage.asset_no ) ) <= 35;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during translation of 'serial number' for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			update	oxy_stage_assets stage
			set			stage.errored = 1,
						stage.error_message = nvl(stage.error_message,'')|| ', serial number translation failed.  check the data coming from ' ||
							'sap to ensure that the company, asset main, and asset sub fields are populated correctly.'
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0
				and	stage.ldg_serial_number is null;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error identifying failed translations of 'serial number' for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	company_id
////
////	changed from linking to gl company no since that isnt unique here

			this.of_writelog( "      - translating company", true )
			
			update	oxy_stage_assets stage
			set			stage.ldg_company_id =
							(	select		co.company_id
								from		company co
								where	to_number(stage.company) = co.company_id
							)
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during translation of 'company' for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			update	oxy_stage_assets stage
			set			stage.errored = 1,
						stage.error_message = nvl(stage.error_message,'')|| ', company translation failed.  check the company field coming ' ||
							'from sap to ensure that it is a 4-character valid company code.'
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0
				and	stage.ldg_company_id is null;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error identifying failed translations of 'company' for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	gl_account_id, bus_segment_id, utility_account_id, sub_account_id
////
			this.of_writelog( "      - translating accounting", true )

//			update oxy_stage_assets z
//			set (z.ldg_bus_segment_id, z.ldg_utility_account_id ) = 
//			(
//				select 1, utility_account_id from 
//				(
//					select stage.company, stage.asset_no, stage.cost_center, ua.utility_account_id
//					from company_setup b, oxy_stage_assets stage, utility_account ua
//					where b.gl_company_no = to_number(stage.company)
//					and 	stage.company = :a_gl_company
//					and	nvl( stage.loaded, 0 ) = 0
//					and 	trim( stage.class ) = trim(external_account_code)
//				) y
//				where y.company = z.company
//				and y.asset_no = z.asset_no
//				and y.cost_center = z.cost_center
//			)
//			where (company, asset_no, cost_center) in 
//			(
//				select stage.company, stage.asset_no, stage.cost_center
//					from company_setup b, oxy_stage_assets stage, utility_account ua
//					where b.gl_company_no = to_number(stage.company)
//					and 	stage.company = :a_gl_company
//					and	nvl( stage.loaded, 0 ) = 0
//					and 	trim( stage.class ) = trim(external_account_code)
//			); 

			update	oxy_stage_assets stage
			set			stage.ldg_bus_segment_id = 1
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during translation of accounting ('bus segment' for company '" + a_gl_company + "'.  no " + &
					"changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

			update	oxy_stage_assets stage
			set			stage.ldg_utility_account_id =
							(	select		ua.utility_account_id
								from		utility_account ua
								where	trim( stage.class ) = trim(external_account_code)
							)
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during translation of utility account for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			update	oxy_stage_assets stage
			set			stage.ldg_sub_account_id = 1
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during translation of accounting ('sub account') for company '" + a_gl_company + "'.  no " + &
					"changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			update	oxy_stage_assets stage
			set			stage.errored = 1,
						stage.error_message = nvl(stage.error_message,'')|| ', accounting (bus segment, utility ' ||
							'account, and sub account) translation failed.  check the asset class field coming from sap to ensure that it is populated with a valid value.'
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0
				and	(	stage.ldg_bus_segment_id is null or
							stage.ldg_utility_account_id is null or
							stage.ldg_sub_account_id is null
						);
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error identifying failed translations of 'bus segment', 'utility account', and 'sub account' for company '" + a_gl_company + "'.  no " + &
					"changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	gl account
////
////	change to hardcoded gl account since gl class in the sap file doesn't mean anything to them
////	no other fields made sense to use here

			update	oxy_stage_assets stage
			set			stage.ldg_gl_account_id = 1001
//							(	select		gl.gl_account_id
//								from		gl_account gl
//								where	trim( nvl( stage.glclass, 'no gl class' ) ) = trim( gl.external_account_code )
//							)

			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during translation of gl account for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			update	oxy_stage_assets stage
			set			stage.errored = 1,
						stage.error_message = nvl(stage.error_message,'')|| ', gl account translation failed.  check the gl class ' ||
							'field coming from sap to ensure that it is populated with a valid value.  (the field may be blank - that is allowed.)'
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0
				and	stage.ldg_gl_account_id is null;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error identifying failed translations of 'gl account for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
////
////	retirement_unit_id
////

			this.of_writelog( "      - setting cip retirement unit", true )
			
			update	oxy_stage_assets stage
			set			stage.ctgy = 'cip'
			where	stage.ctgy is null
			and		stage.class = 'cip';
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during translation of cip retirement unit for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if			
			
			this.of_writelog( "      - translating retirement unit", true )
			
			update	oxy_stage_assets stage
			set			stage.ldg_retirement_unit_id =
							(	select		ru.retirement_unit_id
								from		retirement_unit ru
								where	trim( nvl(stage.ctgy,'0000') ) = trim( ru.external_retire_unit )
							)
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0;
			
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during translation of retirement unit for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			update	oxy_stage_assets stage
			set			stage.errored = 1,
						stage.error_message = nvl(stage.error_message,'')|| ', retirement unit translation failed.  check the ctgy ' ||
							'field coming from sap to ensure that it is populated with a valid value.'
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0
				and	stage.ldg_retirement_unit_id is null;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error identifying failed translations of 'retirement unit' for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

			
////
////	depr_group_id
////
			this.of_writelog( "      - translating depreciation group", true )
			
			update	oxy_stage_assets stage
			set			stage.ldg_depr_group_id = 1
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during translation of depr group for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			update	oxy_stage_assets stage
			set			stage.errored = 1,
						stage.error_message = nvl(stage.error_message,'')|| ', depr group translation failed.  check the asset class field ' ||
							'coming from sap to ensure that it is populated with a valid value.'
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0
				and	stage.ldg_depr_group_id is null;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error identifying failed translations of 'depr group' for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	asset_location_id
////
			this.of_writelog( "      - loading asset location", true )
			if this.uf_load_asset_location( a_gl_company) <> 1 then
				a_msg = "error during loading of asset location for company '" + a_gl_company + "'. "
				return -1
			end if
			
			
			this.of_writelog( "      - translating asset location", true )
			if this.uf_translate_asset_loc( a_gl_company) <> 1 then
				a_msg = "error during translation of asset location for company '" + a_gl_company + "'. "
				return -1
			end if


////
////	in_service_year, eng_in_service_year
////
			this.of_writelog( "      - translating vintage", true )
			
			update	oxy_stage_assets stage
			set			stage.ldg_in_service_year = to_date( cap_on_date, 'yyyymmdd' ),
						stage.ldg_eng_in_service_year = to_date( cap_on_date, 'yyyymmdd' )
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0
				and	is_date( stage.cap_on_date, 'yyyymmdd' ) = 1;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during translation of vintage ('in service year' and 'eng in service year') for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + &
					sqlca.sqlerrtext
				return -1
			end if
			
			update	oxy_stage_assets stage
			set			stage.errored = 1,
						stage.error_message = nvl(stage.error_message,'')|| ', in service year and eng in service year translations failed.  ' ||
							'check the cap on date field to ensure that it is a proper date in the yyyymmdd format.'
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0
				and	(	stage.ldg_in_service_year is null or
							stage.ldg_eng_in_service_year is null
						);
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error identifying failed translations of 'in service year' and 'eng in service year' for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + &
					sqlca.sqlerrtext
				return -1
			end if

////
////	description
////	
////
			this.of_writelog( "      - translating description", true )
			

			update oxy_stage_assets stage
			set stage.ldg_description =substr(stage.description,0,35)
			where	stage.company = :a_gl_company
			and		nvl( stage.loaded, 0 ) = 0;

			if sqlca.sqlcode <> 0 then
				a_msg = "error during translation of 'description' for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			update	oxy_stage_assets stage
			set			ldg_description = '<none>'
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0
				and	trim(stage.ldg_description) is null;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error marking failed translations of 'description' for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	description
////
			this.of_writelog( "      - translating ", true )
			
			update	oxy_stage_assets stage
			set			stage.ldg_description = substr(stage.description,0,254)
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during translation of  description' for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			update	oxy_stage_assets stage
			set			stage.errored = 1,
						stage.error_message = nvl(stage.error_message,'')|| ',  translation failed.  check the data coming from ' ||
							'sap to ensure that the description field is populated correctly.'
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0
				and	trim(stage.ldg_description) is null
				and	(	trim( stage.description ) is not null );
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error identifying failed translations of  description' for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	asset_id
////	
////	must be after serial number.  we're going to get the active asset for this row if there are multiple assets in the cpr with this same serial number (due to transfers).  we're not
////	going to be loading any zero dollar assets from sap so we should never run into a problem where a legit asset that we're trying to match to falls out because its accum cost is 0
////	(unless a previously retired asset now has dollars in it).
////
			this.of_writelog( "      - translating asset id", true )
			
			update	oxy_stage_assets stage
			set			stage.ldg_asset_id =
							(	select		cpr.asset_id
								from		cpr_ledger cpr
								where	trim( stage.ldg_serial_number ) = trim( cpr.serial_number )
									and	cpr.accum_cost <> 0
								group by	cpr.asset_id
							)
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0
				and	trim( stage.ldg_serial_number ) is not null
				and 	exists (
					select 1 from cpr_ledger
					where trim(serial_number) = trim(stage.ldg_serial_number)
				);
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during translation of 'asset id' for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	commit.
////
			this.of_writelog( "      - committing", true )
			commit;

////
////	we were successful so clear out the message and return 1.
////
			a_msg = ""
			return 1
end function

public function long of_initializeload (string a_gl_company, ref string a_msg);////
////	tell the user what we're doing.
////
			this.of_writelog( "    - initializing staging table", true )

////
////	clear out all of the translations of the unprocessed rows for this company.  also, clear out any error messages or error indicators.
////
			this.of_writelog( "      - nulling all translation and error columns", true )
			
			update	oxy_stage_assets
			set			ldg_asset_id = null, ldg_books_schema_id = null, ldg_company_id = null, ldg_gl_account_id = null, ldg_bus_segment_id = null, ldg_utility_account_id = null,
						ldg_sub_account_id = null, ldg_retirement_unit_id = null, ldg_property_group_id = null, ldg_func_class_id = null, ldg_depr_group_id = null,
						ldg_asset_location_id = null, ldg_in_service_year = null, ldg_eng_in_service_year = null, ldg_ledger_status = null, ldg_subledger_indicator = null,
						ldg_work_order_number = null, ldg_description = null, ldg_description = null, ldg_serial_number = null, act_asset_id = null, act_asset_activity_id = null,
						act_gl_posting_mo_yr = null, act_out_of_service_mo_yr = null, act_cpr_posting_mo_yr = null, act_work_order_number = null, act_gl_je_code = null,
						act_description = null, act_description = null, act_disposition_code = null, act_activity_code = null, act_activity_status = null, act_activity_quantity = null,
						act_activity_cost = null, act_ferc_activity_code = null, act_month_number = null, trans_from_asset_id = null, trans_to_asset_id = null,
						trans_asset_activity_id = null, trans_gl_posting_mo_yr = null, trans_out_of_service_mo_yr = null, trans_cpr_posting_mo_yr = null, trans_work_order_number = null,
						trans_gl_je_code = null, trans_description = null, trans_description = null, trans_disposition_code = null, trans_activity_code = null,
						trans_activity_status = null, trans_activity_quantity = null, trans_activity_cost = null, trans_ferc_activity_code = null, trans_month_number = null,
						loaded = 0, errored = 0, error_message = null
			where	company = :a_gl_company
				and	nvl( loaded, 0 ) = 0;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error while nulling all translation and error columns for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	commit.
////
			this.of_writelog( "      - committing", true )
			commit;

////
////	we were successful so clear out the message and return 1.
////
			a_msg = ""
			return 1
end function

public function long of_computeactivity (string a_gl_company, ref string a_msg);////
////	tell the user what we're doing.
////
			this.of_writelog( "    - computing activity", true )
////
////	find any assets which description changed.  change description and the later part these will be marked as loaded.
////
		update cpr_ledger z
		set (description, description) = 
		(
			select ldg_description,ldg_description from 
			(
				select a.asset_id, b.ldg_description,b.ldg_description
				from cpr_ledger a, oxy_stage_assets b
				where a.asset_id = b.ldg_asset_id
				and b.company = :a_gl_company
				and (a.description <> b.ldg_description or a.description <> b.ldg_description)
				and nvl(b.errored, 0 ) = 0
				and nvl(b.loaded, 0 ) = 0
			) y
			where y.asset_id = z.asset_id
		)
		where asset_id in (
			select a.asset_id
			from cpr_ledger a, oxy_stage_assets b
			where a.asset_id = b.ldg_asset_id
			and b.company = :a_gl_company
			and (a.description <> b.ldg_description or a.description <> b.ldg_description)
			and nvl(b.errored, 0 ) = 0
			and nvl(b.loaded, 0 ) = 0
		)
		;
		if sqlca.sqlcode <> 0 then
			a_msg = "error updating assets description for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
			return -1
		end if
////
////	find any assets which haven't changed.  mark these as loaded so that we don't have to worry about them again.
////
			this.of_writelog( "      - finding unchanged assets", true )
			update oxy_stage_assets z
			set loaded = 1
			where (company, asset_no, cost_center) in 
			(
				select company, asset_no, cost_center
				from cpr_ledger a, oxy_stage_assets b
				where a.asset_id = b.ldg_asset_id
				and b.company = :a_gl_company
				and a.company_id = b.ldg_company_id
				and a.utility_account_id = b.ldg_utility_account_id
				and a.bus_segment_id = b.ldg_bus_segment_id
				and a.retirement_unit_id = b.ldg_retirement_unit_id
				and a.asset_location_id = b.ldg_asset_location_id
				and a.in_service_year = b.ldg_in_service_year
				and a.eng_in_service_year = b.ldg_eng_in_service_year
				and a.description = b.ldg_description
				and a.description = b.ldg_description
				and a.accum_quantity = nvl(b.quantity,'0')
				and a.accum_cost = nvl(b.cost,'0')
				and nvl(b.loaded, 0 ) = 0
				and nvl(b.errored, 0 ) = 0
			)
			;
			
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error finding unchanged assets for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	additions (new).  these will be assets from sap that do not have a ldg_asset_id populated.  the ldg_asset_id is populated in of_translatefields so we can be sure
////         that only truly new additions are here.
////
			this.of_writelog( "      - finding additions", true )
			
			update	oxy_stage_assets stage
			set			stage.act_asset_id = pwrplant1.nextval,
						stage.act_asset_activity_id = 1,
						stage.act_gl_posting_mo_yr = to_date( stage.extract_date, 'mm/dd/yyyy' ),
						stage.act_out_of_service_mo_yr = null,
						stage.act_cpr_posting_mo_yr = to_date( stage.extract_date, 'mm/dd/yyyy' ),
						stage.act_work_order_number = 'new_addition_ldr',
						stage.act_gl_je_code = 'new_addition_ldr',
						stage.act_description = stage.ldg_description,
						stage.act_description = stage.ldg_description,
						stage.act_disposition_code = null,
						stage.act_activity_code = 'uadd',
						stage.act_activity_status = 1,
						stage.act_activity_quantity = nvl( to_number( stage.quantity ), 0 ),
						stage.act_activity_cost = to_number(nvl(stage.costsign,'') || stage.cost ),
						stage.act_depr_cost = to_number(nvl(stage.deprsign,'') || stage.depr ),
						stage.act_ferc_activity_code = 1, /* addition */
						stage.act_month_number = to_number( substr( trim( stage.extract_date ), -4 ) || substr( trim( stage.extract_date ), 1, 2 ) )
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0
//				and	nvl( to_number( trim(stage.cost) ), 0 ) <> 0 /*can't find dumb non-numeric error.  arg.*/
				and	nvl(trim(stage.cost),'0') not in ('0','0.0','0.00','0.000','00','00.0','00.00','00.000','000','000.0','000.00','000.000')
				and 	nvl(stage.errored, 0 ) = 0
				and	stage.ldg_asset_id is null;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during population of addition fields for new assets for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	
////
			

////
////	commit.
////
			this.of_writelog( "      - committing", true )
			commit;

////
////	we were successful so clear out the message and return 1.
////
			a_msg = ""
			return 1
end function

public function long of_populatecontroltables (string a_gl_company, ref string a_msg);////
////	tell the user what we're doing.
////
			this.of_writelog( "    - populating powerplant control tables", true )

////
////	utility account
////
			this.of_writelog( "      - populating utility accounts", true )
			
			insert into utility_account ( utility_account_id, bus_segment_id, func_class_id, description, description, status_code_id, external_account_code )
				select		mview.max_id + rownum utility_account_id,
							1 bus_segment_id,
							1 func_class_id,
							dview.class_name description,
							dview.class_name description,
							1 status_code_id,
							dview.class external_account_code
							
				from		(	
				
								select distinct class, class_name
								from 
								(
									select distinct trim( stage.class ) class, trim( stage.class_name ) class_name
									from oxy_stage_assets stage
									where class is not null
									and	nvl( stage.loaded, 0 ) = 0
								)
								
								minus
								
								select		trim( ua.external_account_code ), trim( ua.description )
								from		utility_account ua
								where	trim( ua.external_account_code ) is not null
							) dview,
							
							(	select		nvl(max( ua.utility_account_id ),0) max_id
								from		utility_account ua
							) mview;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during population of new utility accounts for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	sub account
////
			this.of_writelog( "      - populating sub accounts", true )
			
			insert into sub_account ( utility_account_id, bus_segment_id, sub_account_id, description, description, status_code_id )
				select		ua.utility_account_id utility_account_id,
							ua.bus_segment_id bus_segment_id,
							1 sub_account_id,
							'none' description,
							'none' description,
							1 status_code_id
				from		utility_account ua
				where	not exists
								(	select		'x'
									from		sub_account sa
									where	ua.bus_segment_id = sa.bus_segment_id
										and	ua.utility_account_id = sa.utility_account_id
										and	sa.sub_account_id = 1
								);
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during population of new sub accounts for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	gl account
////
////	removed gl class and just hardcoded to a default gl
////	its an old field that no  means anything to oxy
////
//			this.of_writelog( "      - populating gl accounts", true )
//			
//			insert into gl_account
//			(gl_account_id, account_type_id, description, description, status_code_id, external_account_code, ferc_sys_of_accts_id)
//			select 
//				max_id + rownum,
//				2,
//				oxy.glclass,
//				oxy.glclass,
//				1,
//				oxy.glclass,
//				1010
//			from (
//					select nvl(glclass,'no gl class') glclass
//					from oxy_stage_assets
//					group by glclass
//				) oxy,
//				(select nvl(max(gl_account_id),0) max_id from gl_account )
//			where not exists (
//				select 1 from gl_account pp
//				where pp.external_account_code = oxy.glclass
//			);			
//			if sqlca.sqlcode <> 0 then
//				a_msg = "error during population of new gl accounts for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
//				return -1
//			end if


////
////	depr group
////
			this.of_writelog( "      - populating depreciation groups", true )
			
			insert into depr_group ( depr_group_id, subledger_type_id, description, mid_period_conv, mid_period_method, status_id, external_depr_code, reserve_acct_id, expense_acct_id, gain_acct_id, loss_acct_id, cor_reserve_acct_id, cor_expense_acct_id, salvage_reserve_acct_id, salvage_expense_acct_id )
				select		mview.max_id + rownum depr_group_id,
							0 subledger_type_id,
							trim( ua.external_account_code ),
							0 mid_period_conv,
							'monthly' mid_period_method,
							1 status_id,
							trim( ua.external_account_code ),
							1001 reserve_acct_id,
							1001 expense_acct_id,
							1001 gain_acct_id,
							1001 loss_acct_id,
							1001 cor_reserve_acct_id,
							1001 cor_expense_acct_id,
							1001 salvage_reserve_acct_id,
							1001 salvage_expense_acct_id
							
				from		(select distinct trim(external_account_code) external_account_code
								from utility_account 
							) ua,
							
							(	select		nvl(max( dg.depr_group_id ),0) max_id
								from		depr_group dg
							) mview
				
				where	trim( ua.external_account_code ) is not null
					and	not exists
								(	select		'x'
									from		depr_group dg
									where	trim( ua.external_account_code ) = trim( dg.external_depr_code )
								);
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during population of new depreciation groups for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if


////
////	retirement unit
////
////	added external_retire_unit into desc to accomodate oxy having multiple ru's named 'inactive do not use'.  this blows up pk when writing to class code values.

			this.of_writelog( "      - populating retirement units", true )
			
			insert into retirement_unit
				( retirement_unit_id, property_unit_id, external_retire_unit, description, description, status_code_id, expected_life, environmental_percent, curve_minimum_retire )
				select		mview.max_id + rownum retirement_unit_id,
							1 property_unit_id,
							dview.ctgy external_retire_unit,
              				decode(dview.category_name,'inactive - do not use',dview.category_name || '-' || dview.ctgy,substr(dview.category_name,0,35)) description,
              				decode(dview.category_name,'inactive - do not use',dview.category_name || '-' || dview.ctgy,dview.category_name) description,
							1 status_code_id,
							0 expected_life,
							0 environmental_percent,
							0 curve_minimum_retire
				
				from		(	select		distinct trim( nvl(stage.ctgy,'0000') ) ctgy,
											                trim(nvl(decode(stage.category_name,
														  'inactive - do not use',
														  stage.category_name || '-' || stage.ctgy,
														  stage.category_name),
												 			'no category')) category_name											
								from		oxy_stage_assets stage
								where	stage.company = :a_gl_company
									and	trim( stage.ctgy ) is not null
									and	nvl( stage.loaded, 0 ) = 0
									and	stage.class <> 'cip'						/*already have cip as a defualt ru and we're backfilling using class later so ignore for now*/
								
								minus
								
								select		trim( ru.external_retire_unit ), trim ( ru.description )
								from		retirement_unit ru
								where	trim( ru.external_retire_unit ) is not null
							) dview,
							
							(	select		nvl(max( ru.retirement_unit_id ),0) max_id
								from		retirement_unit ru
							) mview
							;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during population of new retirement units for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	prop group prop unit
////
			this.of_writelog( "      - populating prop group - prop unit combinations", true )
			
			insert into prop_group_prop_unit ( property_group_id, property_unit_id )
				select		100 property_group_id,
							pu.property_unit_id
				from		property_unit pu
				where	pu.property_unit_id not in ( -1, 1 ) /* reserved values */
					and	trim( pu.external_prop_unit ) is not null
					and	not exists
								(	select		'x'
									from		prop_group_prop_unit pgpu
									where	pu.property_unit_id = pgpu.property_unit_id
										and	pgpu.property_group_id = 100
								);
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during population of new property group - property unit combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	commit.
////
			this.of_writelog( "      - committing", true )
			commit;

////
////	we were successful so clear out the message and return 1.
////
			a_msg = ""
			return 1
end function

public function long of_postactivity (string a_gl_company, ref string a_msg);////
////	tell the user what we're doing.
////
			this.of_writelog( "    - computing activity", true )

////
////	
////
			
			// temporary
			insert into cpr_ledger
				(	asset_id, property_group_id, depr_group_id, books_schema_id, retirement_unit_id, bus_segment_id, company_id, func_class_id, utility_account_id, gl_account_id,
					asset_location_id, sub_account_id, work_order_number, ledger_status, in_service_year, accum_quantity, accum_cost, subledger_indicator, description,
					description, eng_in_service_year, serial_number
				)
				select		stage.act_asset_id asset_id,
							stage.ldg_property_group_id property_group_id,
							stage.ldg_depr_group_id depr_group_id,
							stage.ldg_books_schema_id books_schema_id,
							stage.ldg_retirement_unit_id retirement_unit_id,
							stage.ldg_bus_segment_id bus_segment_id,
							stage.ldg_company_id company_id,
							stage.ldg_func_class_id func_class_id,
							stage.ldg_utility_account_id utility_account_id,
							stage.ldg_gl_account_id gl_account_id,
							stage.ldg_asset_location_id asset_location_id,
							stage.ldg_sub_account_id sub_account_id,
							stage.ldg_work_order_number work_order_number,
							stage.ldg_ledger_status ledger_status,
							stage.ldg_in_service_year in_service_year,
							stage.act_activity_quantity accum_quantity,
							stage.act_activity_cost accum_cost,
							stage.ldg_subledger_indicator subledger_indicator,
							stage.ldg_description description,
							stage.ldg_description description,
							stage.ldg_eng_in_service_year eng_in_service_year,
							stage.ldg_serial_number serial_number
				
				from		oxy_stage_assets stage
				
				where	stage.company = :a_gl_company
					and	nvl( stage.loaded, 0 ) = 0
					and	nvl( stage.errored, 0 ) = 0
					and	stage.act_asset_id is not null;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error new assets into cpr ledger for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			
			// temporary
			insert into cpr_act_depr_group ( asset_id, gl_posting_mo_yr, depr_group_id )
				select		stage.act_asset_id asset_id,
							stage.act_gl_posting_mo_yr gl_posting_mo_yr,
							stage.ldg_depr_group_id depr_group_id
				
				from		oxy_stage_assets stage
				
				where	stage.company = :a_gl_company
					and	nvl( stage.loaded, 0 ) = 0
					and	nvl( stage.errored, 0 ) = 0
					and	stage.act_asset_id is not null;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error new assets' depreciation groups into cpr act depr group for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			
			// temporary
			insert into cpr_ldg_basis ( asset_id, basis_1 )
				select		stage.act_asset_id asset_id,
							stage.act_activity_cost basis_1
				
				from		oxy_stage_assets stage
				
				where	stage.company = :a_gl_company
					and	nvl( stage.loaded, 0 ) = 0
					and	nvl( stage.errored, 0 ) = 0
					and	stage.act_asset_id is not null;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error new assets' accumulated sap cost into cpr ldg basis for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			
			// temporary
			insert into cpr_activity
				(	asset_id, asset_activity_id, gl_posting_mo_yr, out_of_service_mo_yr, cpr_posting_mo_yr, work_order_number, gl_je_code, description, description,
					disposition_code, activity_code, activity_status, activity_quantity, activity_cost, ferc_activity_code, month_number
				)
				select		stage.act_asset_id asset_id,
							stage.act_asset_activity_id asset_activity_id,
							stage.act_gl_posting_mo_yr gl_posting_mo_yr,
							stage.act_out_of_service_mo_yr out_of_service_mo_yr,
							stage.act_cpr_posting_mo_yr cpr_posting_mo_yr,
							stage.act_work_order_number work_order_number,
							stage.act_gl_je_code gl_je_code,
							stage.act_description description,
							stage.act_description description,
							stage.act_disposition_code disposition_code,
							stage.act_activity_code activity_code,
							stage.act_activity_status activity_status,
							stage.act_activity_quantity activity_quantity,
							stage.act_activity_cost activity_cost,
							stage.act_ferc_activity_code ferc_activity_code,
							stage.act_month_number month_number
				
				from		oxy_stage_assets stage
				
				where	stage.company = :a_gl_company
					and	nvl( stage.loaded, 0 ) = 0
					and	nvl( stage.errored, 0 ) = 0
					and	stage.act_asset_id is not null;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error new asset addition activity into cpr activity for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			
			// temporary
			insert into cpr_act_basis ( asset_id, asset_activity_id, basis_1 )
				select		stage.act_asset_id asset_id,
							stage.act_asset_activity_id asset_activity_id,
							stage.act_activity_cost basis_1
				
				from		oxy_stage_assets stage
				
				where	stage.company = :a_gl_company
					and	nvl( stage.loaded, 0 ) = 0
					and	nvl( stage.errored, 0 ) = 0
					and	stage.act_asset_id is not null;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error new asset addition sap cost into cpr act basis for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			////inserting into cpr_depr so they can run a report showing nbv from sap
			////none of the related tables are being populated, because well depr is a mess in pp
			////report will be created to join on asset_id and gl_posting_mo_year so thats all we need to worry abou here
			
			insert into cpr_depr (asset_id, set_of_books_id, gl_posting_mo_yr, asset_dollars)
				select		stage.act_asset_id asset_id,
							1 set_of_books_id,
							stage.act_gl_posting_mo_yr gl_posting_mo_yr,
							stage.act_depr_cost asset_dollars
				
				from		oxy_stage_assets stage
				
				where 	stage.company = :a_gl_company
					and	nvl( stage.loaded, 0 ) = 0
					and	nvl( stage.errored, 0 ) = 0
					and	stage.act_asset_id is not null;

			if sqlca.sqlcode <> 0 then
				a_msg = "error new asset addition sap cost into cpr depr for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
////
////	commit.
////
			this.of_writelog( "      - committing", true )
			commit;

////
////	load all of the valid combintations into some of the cpr control tables.
////
////	company bus segment control
////
			this.of_writelog( "      - updating valid company - bus segment combinations", true )
			
			insert into company_bus_segment_control ( company_id, bus_segment_id )
				select		distinct cpr.company_id company_id,
							cpr.bus_segment_id bus_segment_id
				from		cpr_ledger cpr
				
				minus
				
				select		cbsc.company_id company_id,
							cbsc.bus_segment_id bus_segment_id
				from		company_bus_segment_control cbsc;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during update of valid company - business segment combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	company gl account
////
			this.of_writelog( "      - updating valid company - gl account combinations", true )
			
			insert into company_gl_account ( company_id, gl_account_id )
				select		distinct cpr.company_id company_id,
							cpr.gl_account_id gl_account_id
				from		cpr_ledger cpr
				
				minus
				
				select		cgla.company_id company_id,
							cgla.gl_account_id gl_account_id
				from		company_gl_account cgla;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during update of valid company - gl account combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	company major location
////
			this.of_writelog( "      - updating valid company - major location combinations", true )
			
			insert into company_major_location (company_id, major_location_id )
				select		distinct cpr.company_id company_id,
							al.major_location_id major_location_id
				from		cpr_ledger cpr,
							asset_location al
				where	cpr.asset_location_id = al.asset_location_id
				
				minus
				
				select		cml.company_id company_id,
							cml.major_location_id major_location_id
				from		company_major_location cml;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during update of valid company - major location combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	company property unit
////
			this.of_writelog( "      - updating valid company - property unit combinations", true )
			
			insert into company_property_unit ( company_id, property_unit_id )
				select		distinct cpr.company_id company_id,
							ru.property_unit_id property_unit_id
				from		cpr_ledger cpr,
							retirement_unit ru
				where	cpr.retirement_unit_id = ru.retirement_unit_id
				
				minus
				
				select		cpu.company_id company_id,
							cpu.property_unit_id property_unit_id
				from		company_property_unit cpu;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during update of valid company - property unit combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	util acct prop unit
////
			this.of_writelog( "      - updating valid utility account - property unit combinations", true )
			
			insert into util_acct_prop_unit ( bus_segment_id, utility_account_id, property_unit_id )
				select		distinct cpr.bus_segment_id bus_segment_id,
							cpr.utility_account_id utility_account_id,
							ru.property_unit_id property_unit_id
				from		cpr_ledger cpr,
							retirement_unit ru
				where	cpr.retirement_unit_id = ru.retirement_unit_id
				
				minus
				
				select		uapu.bus_segment_id bus_segment_id,
							uapu.utility_account_id utility_account_id,
							uapu.property_unit_id property_unit_id
				from		util_acct_prop_unit uapu;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during update of valid utility account - property unit combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	commit.
////
			this.of_writelog( "      - committing", true )
			commit;

////
////	
////
			// temporary
			update	oxy_stage_assets stage
			set			loaded = 1
			where	stage.company = :a_gl_company
				and	nvl( stage.loaded, 0 ) = 0
				and	nvl( stage.errored, 0 ) = 0
				and	stage.act_asset_id is not null
				and	exists
							(	select		'x'
								from		cpr_ledger cpr
								where	stage.act_asset_id = cpr.asset_id
							);
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error updating the loaded field for new assets loaded for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	we were successful so clear out the message and return 1.
////
			a_msg = ""
			return 1
end function

public function long of_importfacilities (ref string a_msg);uo_ds_top ds_missing_county
long maxj, j
string sqls, rtn_str
////
////	tell the user what we're doing.
////
			this.of_writelog( "    - importing facilities", true )

////
////	clear out the facilities staging table.
////
			this.of_writelog( "      - clearing facilities staging table", true )
			
			delete from oxy_stage_facility;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error while deleting old facilities from the facilities staging table.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	insert facilities from the idr database in the staging table.
////
			this.of_writelog( "      - populating facilities staging table", true )
			
			insert into oxy_stage_facility
				(	extract_date, facility_no, name_1, name_2, house_num, address_1, address_2, city, state, zip_code, county_code, juris_code, location_type_code,
					location_type_desc, location_value_code, location_value_desc, status_code, status_desc, status_start_date, cost_center
				)
				select		sysdate extract_date,
							ltrim( idr_fac.location_id, '0' ) facility_no,
							nvl( cust.name1, idr_fac.loc_name1 ) name_1,
							nvl( cust.name2, idr_fac.loc_name2 ) name_2,
							idr_fac.house_num house_num,
							idr_fac.street address_1,
							idr_fac.street_suppl address_2,
							idr_fac.city1 city,
							upper( substr( idr_fac.tax_juri_code, 1, 2 ) ) state,
							substr( idr_fac.tax_juri_code, 3, 5 ) zip_code,
							decode(trim(county_xlate.to_county_code), 
											null, upper( substr( idr_fac.tax_juri_code, 10, 3 ) ), 
											trim(county_xlate.to_county_code) 
									  ) county_code,
							idr_fac.tax_juri_code juris_code,
							decode( trim( idr_fac.location_typ ), '', null, trim( idr_fac.location_typ ) ) location_type_code,
							upper( trim( idr_fac.location_typ_desc ) ) location_type_desc,
							decode( trim( idr_fac.location_val ), '', null, trim( idr_fac.location_val ) ) location_value_code,
							upper( trim( idr_fac.location_val_desc ) ) location_value_desc,
							decode( trim( idr_status_view.status_code ), '', null, trim( idr_status_view.status_code ) ) status_code,
							upper( trim( idr_status_view.status_desc ) ) status_desc,
							idr_status_view.status_start_date,
							upper( idr_fac.cost_center ) cost_center
				
				from		business_location@idrp idr_fac,
							business_partner_role@idrp idr_role,
							customer@idrp cust,
							oxy_ashell_taxware_cnty_codes county_xlate,
							
							(	select		idr_bus.location_id location_id,
											idr_bus.status status_code,
											idr_bus.status_desc status_desc,
											idr_bus.start_time_stmp status_start_date
								from		location_status@idrp idr_bus
								where	idr_bus.start_time_stmp <= sysdate
									and	idr_bus.end_time_stmp >= sysdate
									and	decode( trim( idr_bus.business_type ), '', null, idr_bus.business_type ) is null
							) idr_status_view
				
				where	upper( trim( idr_fac.country ) ) = 'us'
					and	idr_fac.location_id = idr_role.location_id (+)
					and	idr_role.partner_role (+) = 'zlop'
					and	idr_role.start_time_stmpbp (+) <= sysdate
					and	idr_role.end_time_stmpbp (+) >= sysdate
					and	idr_role.customer_num = cust.customer_num (+)
					and	idr_fac.location_id = idr_status_view.location_id (+)
					and	upper(nvl(substr(idr_fac.tax_juri_code, 1, 2),'  ')) = trim(county_xlate.state_code (+))
         			and	upper(nvl(substr(idr_fac.tax_juri_code, 10, 3), '   ')) = trim(county_xlate.from_county_code (+))
					;
         
			if sqlca.sqlcode <> 0 then
				a_msg = "error while inserting current facilities from the facilities staging table.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////  check to make sure all county combinations from facilities system are on the county table
////
			sqls =		"	select		distinct state, " + &
						"						county_code " + &
						"	from		oxy_stage_facility facility" + &
						"	where     	(state, county_code) not in " + &
						"				(select state.state_code, county.county_code " + & 
						"				from state, " + &
						"					   county " + &
						"			     where state.state_id = county.state_id " + &
						"				 )  " + &
						"    order by 1,2 "
						
						
			ds_missing_county = create uo_ds_top
			rtn_str = f_create_dynamic_ds( ds_missing_county, "grid", sqls, sqlca, true )
			
			if rtn_str <> "ok" then
				a_msg = "error creating datastore to check facility county codes"
				destroy ds_missing_county
				return -1
			end if
			
			maxj = ds_missing_county.rowcount()
			
			if maxj > 0 then 
				this.of_writelog( "***********************************************************", true )
				this.of_writelog( "", true )
				this.of_writelog( "county/state codes on facility table are missing from the county table", true )
				this.of_writelog( "", true )
				this.of_writelog( "add the following state code/county code combinations to the county table", true)
				this.of_writelog( " or update the oxy_ashell_taxware_cnty_codes table to translate this state code/county code combination to", true)
				this.of_writelog( " a valid state code/county code on the county table.", true)
				this.of_writelog( " then re-execute this process: ", true)
				for j = 1 to maxj
					this.of_writelog( "state: " + ds_missing_county.getitemstring( j, 1 ) + " county code: " + string( ds_missing_county.getitemstring( j, 2 ) ), true )
				next
				this.of_writelog( "", true )
				this.of_writelog( "***********************************************************", true )
				
				destroy ds_missing_county
			
				return -1
			else
				destroy ds_missing_county
			end if


////
////	commit and analyze.
////
			this.of_writelog( "      - committing and analyzing", true )
			commit;
			execute immediate "analyze table oxy_stage_facility compute statistics";
////
////	populate minor location.
////
		if uf_minor_location() = -1 then
			a_msg = "error while inserting current facilities from the facilities staging table.  no changes made."
			return -1
		else
			commit;
		end if
////
////	we were successful so clear out the message and return 1.
////
			a_msg = ""
			return 1
end function

public function long uf_minor_location ();insert into minor_location(minor_location_id, description, description)
select to_number(facility_no), substr(trim(facility_no)||'-'||trim(name_1)||' '||trim(name_2),1,35), 
substr(trim(house_num)||' '||trim(address_1)||' '||trim(address_2)||' '||trim(city)||','||trim(state)||' '||trim(zip_code),1,254)
from oxy_stage_facility 
where is_number(facility_no) = 1
and not exists
(
	select 1 from minor_location where minor_location_id = to_number(oxy_stage_facility.facility_no)
)
;

if sqlca.sqlcode <> 0 then
	of_writelog("error: insert into minor location failed.~n~nerror: " + sqlca.sqlerrtext, true)
	return -1
end if

update minor_location z
set description = 
(
	select substr(trim(facility_no)||'-'||trim(name_1),1,35) description
	from oxy_stage_facility y
	where is_number(facility_no) = 1
	and to_number(y.facility_no) = z.minor_location_id
	and substr(trim(facility_no)||'-'||trim(name_1),1,35) <> description
)
where minor_location_id in (
	select to_number(y.facility_no)
	from oxy_stage_facility y
	where is_number(facility_no) = 1
	and substr(trim(facility_no)||'-'||trim(name_1),1,35) <> description
);

if sqlca.sqlcode <> 0 then
	of_writelog("error: update minor location description failed.~n~nerror: " + sqlca.sqlerrtext, true)
	return -1
end if

update minor_location z
set description = 
(
	select substr(trim(name_2)||'-'||trim(house_num)||' '||trim(address_1)||' '||trim(address_2)||' '||trim(city)||','||trim(state)||' '||trim(zip_code),1,254) description
	from oxy_stage_facility y
	where is_number(facility_no) = 1
	and to_number(y.facility_no) = z.minor_location_id
	and substr(trim(name_2)||'-'||trim(house_num)||' '||trim(address_1)||' '||trim(address_2)||' '||trim(city)||','||trim(state)||' '||trim(zip_code),1,254) <> z.description
)
where minor_location_id in (
	select to_number(y.facility_no)
	from oxy_stage_facility y
	where is_number(facility_no) = 1
	and substr(trim(name_2)||'-'||trim(house_num)||' '||trim(address_1)||' '||trim(address_2)||' '||trim(city)||','||trim(state)||' '||trim(zip_code),1,254) <> z.description
)
;

if sqlca.sqlcode <> 0 then
	of_writelog("error: update minor location  failed.~n~nerror: " + sqlca.sqlerrtext, true)
	return -1
end if

return 1
end function

public function long uf_translate_asset_loc (string a_gl_company);boolean success = true
string sqls, description, filepath, filename, a_msg
long i, rtn, asset_location_id, county_id, counter

commit;

update oxy_stage_assets z
set ldg_asset_location_id = 
(
	select asset_location_id from asset_location y
	where y.ext_asset_location = trim(z.plant) || '-' || trim(z.cost_center)
)
;

if sqlca.sqlcode <> 0 then
	of_writelog("error: update asset location failed.~n~nerror: " + sqlca.sqlerrtext, true)
	success = false
	goto theend
else
	commit;
end if

commit;


update	oxy_stage_assets stage
set			stage.errored = 1,
			stage.error_message = nvl(stage.error_message,'')|| ', asset location translation failed.  check the cost center  and plant fields ' ||
				' to ensure that it is populated with a valid value.  '
where	stage.company = :a_gl_company
	and	nvl( stage.loaded, 0 ) = 0
	and	stage.ldg_asset_location_id is null;

if sqlca.sqlcode <> 0 then
	a_msg = "error identifying failed translations of 'asset location' for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	of_writelog(a_msg, true)
	success = false
	goto theend
end if

theend:
if success then
	return 1
else
	return -1
end if
	
end function

public function long uf_load_asset_location (string a_gl_company);boolean success = true
string sqls, description, filepath, filename
long i, rtn, asset_location_id, county_id



//
// insert asset locations
//
insert into asset_location (
	asset_location_id, 
	major_location_id, 
	ext_asset_location, 
	description, 
	status_code_id, 
	state_id
)
select
  (select nvl(max(asset_location_id),0) from asset_location) + rownum as asset_location_id,
  major_location_id, 
  ext_asset_location, 
  description, 
  status_code_id, 
  state_id
from 
(
  select distinct
    major_location_id as major_location_id,
    trim(a.plant) || '-' || trim(a.cost_center) as ext_asset_location,
    trim(a.plant_name) || '-' || trim(a.cost_ctr_name) as description,
    1 status_code_id,
    (select state_id from state where trim(state_code) = trim(a.state)) as state_id
  from oxy_stage_assets a, major_location e
  where e.external_location_id = trim(a.plant)
  and company  = :a_gl_company
  and not exists
  (
    select 1 from asset_location
    where ext_asset_location = trim(a.plant) || '-' || trim(a.cost_center)
  )
)
;
if sqlca.sqlcode <> 0 then
	of_writelog("error: insert into asset location.~n~nerror: " + sqlca.sqlerrtext, true)
	success = false
	goto theend
end if

execute immediate "analyze table asset_location compute statistics";



theend:
if success then
	return 1
else
	return -1
end if
	
end function

public function long uf_computetransfer (string a_gl_company, ref string a_msg);////
////	tell the user what we're doing.
////
this.of_writelog( "    - computing transfer", true )

////
////	transfer (new).  these will be assets from sap that the serial number matches powerplant but attribute changes
////
this.of_writelog( "      - finding transfer", true )
/*
	find any asset with the same serial number but attributes does not match 
		company_id
		utility_account_id
		bus_segment_id
		retirement_unit_id
		asset_location_id
		in_service_year
		eng_in_service_year
	populate the transfer from and transfer to (pwrplant1.nextval) asset_id 
	
	populate the transfer from cost and quantity from the transfer from assets. 
	populate the trasfer to cost and quantity from the transfer from assets.
	if there is a balance, the cost/quantity will be add as an add.
	
	this way we do not need to keep the serial number for the transfer from asset.
	we can mark them so that the transfer from asset will no  get picked up.
	
	essentially as follows:
	
	from sap: 
	asset	serial number	cost	company
	1234	123-123123-1	4000	2
	
	in ppc: 
	asset	serial number	cost	company
	0001	123-123123-1	1000	1
	
	we will be creating the following activities
	asset	cost	activity 
	0001	1000	uadd	*original
	0001	-1000	utrf
	1234	3000	uadd
	1234	1000	utrt
*/

update oxy_stage_assets z
set (trans_from_asset_id, trans_activity_quantity,trans_activity_cost) = 
(
	select trans_from_asset_id, accum_quantity, accum_cost from
	(
		select company, asset_no, cost_center, 
				ldg_asset_id trans_from_asset_id, a.accum_quantity, a.accum_cost
		from cpr_ledger a, oxy_stage_assets b
		where a.asset_id = b.ldg_asset_id
		and b.company =  :a_gl_company
		and (a.company_id <> b.ldg_company_id
			or a.utility_account_id <> b.ldg_utility_account_id
			or a.bus_segment_id <> b.ldg_bus_segment_id
			or a.retirement_unit_id <> b.ldg_retirement_unit_id
			or a.asset_location_id <> b.ldg_asset_location_id
			or a.in_service_year <> b.ldg_in_service_year
			or a.eng_in_service_year <> b.ldg_eng_in_service_year)
		and nvl(b.loaded, 0 ) = 0
		and nvl(b.errored, 0 ) = 0
	) y
	where y.company=z.company
	and y.asset_no=z.asset_no
	and y.cost_center=z.cost_center
	
)
where exists
(
	select 1
	from cpr_ledger a, oxy_stage_assets b
	where a.asset_id = b.ldg_asset_id
	and (a.company_id <> b.ldg_company_id
			or a.utility_account_id <> b.ldg_utility_account_id
			or a.bus_segment_id <> b.ldg_bus_segment_id
			or a.retirement_unit_id <> b.ldg_retirement_unit_id
			or a.asset_location_id <> b.ldg_asset_location_id
			or a.in_service_year <> b.ldg_in_service_year
			or a.eng_in_service_year <> b.ldg_eng_in_service_year)
	and b.company=z.company
	and b.asset_no=z.asset_no
	and b.cost_center=z.cost_center
	and nvl(b.loaded, 0 ) = 0
	and nvl(b.errored, 0 ) = 0
)
;
if sqlca.sqlcode <> 0 then
	a_msg = "error during population of transfer fields (transfer from) for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

update oxy_stage_assets
set trans_to_asset_id = pwrplant1.nextval
where company =  :a_gl_company
and trans_from_asset_id is not null
and nvl(loaded, 0 ) = 0
and nvl(errored, 0 ) = 0
;
if sqlca.sqlcode <> 0 then
	a_msg = "error during population of transfer fields (transfer to) for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if
/*
find the next asset activity id for the trans from asset id
trans to asset activity id is always going to be 1
*/

update oxy_stage_assets z
set (trans_asset_activity_id) = 
(
	select trans_from_asset_id from
	(
		select company, asset_no, cost_center, max(asset_activity_id) + 1 trans_asset_activity_id
		from cpr_activity a, oxy_stage_assets b
		where a.asset_id = b.trans_from_asset_id
		and b.company =  :a_gl_company
		and nvl(loaded, 0 ) = 0
		and nvl(b.errored, 0 ) = 0
		group by company, asset_no, cost_center
	) y
	where y.company=z.company
	and y.asset_no=z.asset_no
	and y.cost_center=z.cost_center
)
where (company, asset_no, cost_center) in (
	select company, asset_no, cost_center
	from cpr_activity a, oxy_stage_assets b
	where a.asset_id = b.trans_from_asset_id
	and nvl(loaded, 0 ) = 0
	and nvl(b.errored, 0 ) = 0
	and b.company =  :a_gl_company
)
;
if sqlca.sqlcode <> 0 then
	a_msg = "error during population of transfer fields (transfer from next activity) for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

update oxy_stage_assets
set 
	act_activity_quantity = nvl( to_number( quantity ), 0 ), /*this is for the transfer to amount*/
	act_activity_cost = to_number( cost ), /*this is for the transfer to amount*/
	trans_gl_posting_mo_yr = to_date( extract_date, 'mm/dd/yyyy' ),
	trans_out_of_service_mo_yr = null,
	trans_cpr_posting_mo_yr = to_date( extract_date, 'mm/dd/yyyy' ),
	trans_work_order_number = 'transfer_ldr',
	trans_gl_je_code = 'transfer_ldr',
	trans_description = ldg_description,
	trans_description = ldg_description,
	trans_disposition_code = null,
	trans_activity_code = 'utrt',
	trans_activity_status = trans_from_asset_id,  /* keep track of where the activity is transfer from */
	trans_ferc_activity_code = 4, /* transfer */
	trans_month_number = to_number( substr( trim( extract_date ), -4 ) || substr( trim( extract_date ), 1, 2 ) )
where company =  :a_gl_company
and trans_from_asset_id is not null
and nvl(loaded, 0 ) = 0
and nvl(errored, 0 ) = 0;

if sqlca.sqlcode <> 0 then
	a_msg = "error during population of transfer fields (transfer other attributes) for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

////
////	commit.
////
			this.of_writelog( "      - committing", true )
			commit;

////
////	we were successful so clear out the message and return 1.
////
			a_msg = ""
			return 1
end function

public function long uf_posttransfer (string a_gl_company, ref string a_msg);////
////	tell the user what we're doing.
////
			this.of_writelog( "    - posting transfer", true )


/*create the transfer to asset*/
insert into cpr_ledger
(	asset_id, property_group_id, depr_group_id, books_schema_id, retirement_unit_id, bus_segment_id, company_id, func_class_id, utility_account_id, gl_account_id,
	asset_location_id, sub_account_id, work_order_number, ledger_status, in_service_year, accum_quantity, accum_cost, subledger_indicator, description,
	description, eng_in_service_year, serial_number
)
select		stage.trans_to_asset_id asset_id,
			stage.ldg_property_group_id property_group_id,
			stage.ldg_depr_group_id depr_group_id,
			stage.ldg_books_schema_id books_schema_id,
			stage.ldg_retirement_unit_id retirement_unit_id,
			stage.ldg_bus_segment_id bus_segment_id,
			stage.ldg_company_id company_id,
			stage.ldg_func_class_id func_class_id,
			stage.ldg_utility_account_id utility_account_id,
			stage.ldg_gl_account_id gl_account_id,
			stage.ldg_asset_location_id asset_location_id,
			stage.ldg_sub_account_id sub_account_id,
			stage.ldg_work_order_number work_order_number,
			stage.ldg_ledger_status ledger_status,
			stage.ldg_in_service_year in_service_year,
			stage.act_activity_quantity accum_quantity,
			stage.act_activity_cost accum_cost,
			stage.ldg_subledger_indicator subledger_indicator,
			stage.ldg_description description,
			stage.ldg_description description,
			stage.ldg_eng_in_service_year eng_in_service_year,
			stage.ldg_serial_number serial_number
from		oxy_stage_assets stage
where company =  :a_gl_company
	and trans_from_asset_id is not null
	and nvl(loaded, 0 ) = 0
	and nvl( stage.errored, 0 ) = 0
;
if sqlca.sqlcode <> 0 then
	a_msg = "error transfer to assets into cpr ledger for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

insert into cpr_act_depr_group ( asset_id, gl_posting_mo_yr, depr_group_id )
select		stage.trans_to_asset_id asset_id,
			stage.trans_gl_posting_mo_yr gl_posting_mo_yr,
			stage.ldg_depr_group_id depr_group_id
from		oxy_stage_assets stage
where company =  :a_gl_company
	and trans_from_asset_id is not null
	and nvl(loaded, 0 ) = 0
	and nvl( stage.errored, 0 ) = 0
minus
select asset_id, gl_posting_mo_yr, depr_group_id from cpr_act_depr_group;
if sqlca.sqlcode <> 0 then
	a_msg = "error transfer to assets' depreciation groups into cpr act depr group for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

insert into cpr_ldg_basis ( asset_id, basis_1 )
select		stage.trans_to_asset_id asset_id,
			stage.act_activity_cost basis_1
from		oxy_stage_assets stage
where company =  :a_gl_company
	and trans_from_asset_id is not null
	and nvl(loaded, 0 ) = 0
	and nvl( stage.errored, 0 ) = 0
;
if sqlca.sqlcode <> 0 then
	a_msg = "error transfer to assets' accumulated sap cost into cpr ldg basis for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

/*this is the transfer activity*/
insert into cpr_activity
(	asset_id, asset_activity_id, gl_posting_mo_yr, out_of_service_mo_yr, cpr_posting_mo_yr, work_order_number, gl_je_code, description, description,
	disposition_code, activity_code, activity_status, activity_quantity, activity_cost, ferc_activity_code, month_number
)
select	stage.trans_to_asset_id asset_id,
			1 asset_activity_id,
			stage.trans_gl_posting_mo_yr gl_posting_mo_yr,
			stage.trans_out_of_service_mo_yr out_of_service_mo_yr,
			stage.trans_cpr_posting_mo_yr cpr_posting_mo_yr,
			stage.trans_work_order_number work_order_number,
			stage.trans_gl_je_code gl_je_code,
			stage.trans_description description,
			stage.trans_description description,
			stage.trans_disposition_code disposition_code,
			stage.trans_activity_code activity_code,
			stage.trans_activity_status activity_status,
			stage.trans_activity_quantity activity_quantity,
			stage.trans_activity_cost activity_cost,
			stage.trans_ferc_activity_code ferc_activity_code,
			stage.trans_month_number month_number
from		oxy_stage_assets stage
where company =  :a_gl_company
	and trans_from_asset_id is not null
	and nvl(loaded, 0 ) = 0
	and nvl( stage.errored, 0 ) = 0
;


if sqlca.sqlcode <> 0 then
	a_msg = "error transfer to asset transfer activity into cpr activity for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

insert into cpr_activity
(	asset_id, asset_activity_id, gl_posting_mo_yr, out_of_service_mo_yr, cpr_posting_mo_yr, work_order_number, gl_je_code, description, description,
	disposition_code, activity_code, activity_status, activity_quantity, activity_cost, ferc_activity_code, month_number
)
select	stage.trans_to_asset_id asset_id,
			2 asset_activity_id,
			stage.trans_gl_posting_mo_yr gl_posting_mo_yr,
			stage.trans_out_of_service_mo_yr out_of_service_mo_yr,
			stage.trans_cpr_posting_mo_yr cpr_posting_mo_yr,
			stage.trans_work_order_number work_order_number,
			stage.trans_gl_je_code gl_je_code,
			stage.trans_description description,
			stage.trans_description description,
			stage.trans_disposition_code disposition_code,
			'uadd' activity_code,
			stage.trans_activity_status activity_status,
			stage.act_activity_quantity - stage.trans_activity_quantity activity_quantity,
			stage.act_activity_cost - stage.trans_activity_cost activity_cost,
			1 ferc_activity_code,
			stage.trans_month_number month_number
from		oxy_stage_assets stage
where company =  :a_gl_company
	and trans_from_asset_id is not null
	and nvl(loaded, 0 ) = 0
	and nvl( stage.errored, 0 ) = 0
	and (stage.act_activity_quantity - stage.trans_activity_quantity <> 0 or stage.act_activity_cost - stage.trans_activity_cost <> 0)
;
if sqlca.sqlcode <> 0 then
	a_msg = "error transfer to asset addition activity into cpr activity for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

insert into cpr_act_basis ( asset_id, asset_activity_id, basis_1 )
select		stage.trans_to_asset_id asset_id,
			1 asset_activity_id,
			stage.act_activity_cost basis_1
from		oxy_stage_assets stage
where company =  :a_gl_company
	and trans_from_asset_id is not null
	and nvl(loaded, 0 ) = 0
	and nvl( stage.errored, 0 ) = 0
;
if sqlca.sqlcode <> 0 then
	a_msg = "error transfer to asset addition sap cost into cpr act basis for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

/* add activity lines to the transfer from asset*/
insert into cpr_activity
(	asset_id, asset_activity_id, gl_posting_mo_yr, out_of_service_mo_yr, cpr_posting_mo_yr, work_order_number, gl_je_code, description, description,
	disposition_code, activity_code, activity_status, activity_quantity, activity_cost, ferc_activity_code, month_number
)
select	stage.trans_from_asset_id asset_id,
			stage.trans_asset_activity_id asset_activity_id,
			stage.trans_gl_posting_mo_yr gl_posting_mo_yr,
			stage.trans_out_of_service_mo_yr out_of_service_mo_yr,
			stage.trans_cpr_posting_mo_yr cpr_posting_mo_yr,
			stage.trans_work_order_number work_order_number,
			stage.trans_gl_je_code gl_je_code,
			stage.trans_description description,
			stage.trans_description description,
			stage.trans_disposition_code disposition_code,
			'utrf' activity_code,
			stage.trans_to_asset_id activity_status,
			-1 * stage.trans_activity_quantity activity_quantity,
			-1 * stage.trans_activity_cost activity_cost,
			stage.trans_ferc_activity_code ferc_activity_code,
			stage.trans_month_number month_number
from		oxy_stage_assets stage
where company =  :a_gl_company
	and trans_from_asset_id is not null
	and nvl(loaded, 0 ) = 0
	and nvl( stage.errored, 0 ) = 0
;
if sqlca.sqlcode <> 0 then
	a_msg = "error transfer from asset addition activity into cpr activity for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

insert into cpr_act_basis ( asset_id, asset_activity_id, basis_1 )
select		stage.trans_from_asset_id asset_id,
			stage.trans_asset_activity_id asset_activity_id,
			-1 * stage.trans_activity_cost basis_1
from		oxy_stage_assets stage
where company =  :a_gl_company
	and trans_from_asset_id is not null
	and nvl(loaded, 0 ) = 0
	and nvl( stage.errored, 0 ) = 0
;
if sqlca.sqlcode <> 0 then
	a_msg = "error transfer from asset addition sap cost into cpr act basis for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

update cpr_ledger z
set (accum_cost, accum_quantity) =
(
	select activity_cost, activity_quantity
	from 
	(
		select a.asset_id, a.accum_cost, a.accum_quantity, sum(b.activity_cost) activity_cost, sum(b.activity_quantity) activity_quantity
		from cpr_ledger a, cpr_activity b
		where a.asset_id = b.asset_id
		group by a.asset_id, a.accum_cost, a.accum_quantity
		having (sum(b.activity_cost) <> a.accum_cost or sum(b.activity_quantity) <> a.accum_quantity)
	) y
	where y.asset_id = z.asset_id
)
where asset_id in (
	select asset_id
	from 
	(
		select a.asset_id, a.accum_cost, a.accum_quantity, sum(b.activity_cost) activity_cost, sum(b.activity_quantity) activity_quantity
		from cpr_ledger a, cpr_activity b
		where a.asset_id = b.asset_id
		group by a.asset_id, a.accum_cost, a.accum_quantity
		having (sum(b.activity_cost) <> a.accum_cost or sum(b.activity_quantity) <> a.accum_quantity)
	) y
)
;
if sqlca.sqlcode <> 0 then
	a_msg = "error transfer from assets update into cpr ledger for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

update cpr_ldg_basis z
set basis_1 =
(
	select activity_cost
	from 
	(
		select a.asset_id, a.basis_1, sum(b.activity_cost) activity_cost
		from cpr_ldg_basis a, cpr_activity b
		where a.asset_id = b.asset_id
		group by a.asset_id, a.basis_1
		having sum(b.activity_cost) <> a.basis_1
	) y
	where y.asset_id = z.asset_id
)
where asset_id in (
	select asset_id
	from 
	(
		select a.asset_id, a.basis_1, sum(b.activity_cost) activity_cost
		from cpr_ldg_basis a, cpr_activity b
		where a.asset_id = b.asset_id
		group by a.asset_id, a.basis_1
		having sum(b.activity_cost) <> a.basis_1
	) y
)
;			
if sqlca.sqlcode <> 0 then
	a_msg = "error transfer from assets' accumulated sap cost into cpr ldg basis for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if


////
////	updating the serial number of the cpr_ledger of the transfer from asset so it will not get pick up again.
////

update cpr_ledger 
set serial_number = 'ppc:'||serial_number
where asset_id in (
select		stage.trans_from_asset_id asset_id
from		oxy_stage_assets stage
where company = :a_gl_company
	and trans_from_asset_id is not null
	and nvl(loaded, 0 ) = 0
	and nvl( stage.errored, 0 ) = 0
);
if sqlca.sqlcode <> 0 then
	a_msg = "error updating transfer from assets' on cpr_ledger for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

////
////	commit.
////
			this.of_writelog( "      - committing", true )
			commit;

////
////	load all of the valid combintations into some of the cpr control tables.
////
////	company bus segment control
////
			this.of_writelog( "      - updating valid company - bus segment combinations", true )
			
			insert into company_bus_segment_control ( company_id, bus_segment_id )
				select		distinct cpr.company_id company_id,
							cpr.bus_segment_id bus_segment_id
				from		cpr_ledger cpr
				
				minus
				
				select		cbsc.company_id company_id,
							cbsc.bus_segment_id bus_segment_id
				from		company_bus_segment_control cbsc;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during update of valid company - business segment combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	company gl account
////
			this.of_writelog( "      - updating valid company - gl account combinations", true )
			
			insert into company_gl_account ( company_id, gl_account_id )
				select		distinct cpr.company_id company_id,
							cpr.gl_account_id gl_account_id
				from		cpr_ledger cpr
				
				minus
				
				select		cgla.company_id company_id,
							cgla.gl_account_id gl_account_id
				from		company_gl_account cgla;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during update of valid company - gl account combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	company major location
////
			this.of_writelog( "      - updating valid company - major location combinations", true )
			
			insert into company_major_location (company_id, major_location_id )
				select		distinct cpr.company_id company_id,
							al.major_location_id major_location_id
				from		cpr_ledger cpr,
							asset_location al
				where	cpr.retirement_unit_id = al.asset_location_id
				
				minus
				
				select		cml.company_id company_id,
							cml.major_location_id major_location_id
				from		company_major_location cml;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during update of valid company - major location combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	company property unit
////
			this.of_writelog( "      - updating valid company - property unit combinations", true )
			
			insert into company_property_unit ( company_id, property_unit_id )
				select		distinct cpr.company_id company_id,
							ru.property_unit_id property_unit_id
				from		cpr_ledger cpr,
							retirement_unit ru
				where	cpr.retirement_unit_id = ru.retirement_unit_id
				
				minus
				
				select		cpu.company_id company_id,
							cpu.property_unit_id property_unit_id
				from		company_property_unit cpu;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during update of valid company - property unit combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	util acct prop unit
////
			this.of_writelog( "      - updating valid utility account - property unit combinations", true )
			
			insert into util_acct_prop_unit ( bus_segment_id, utility_account_id, property_unit_id )
				select		distinct cpr.bus_segment_id bus_segment_id,
							cpr.utility_account_id utility_account_id,
							ru.property_unit_id property_unit_id
				from		cpr_ledger cpr,
							retirement_unit ru
				where	cpr.retirement_unit_id = ru.retirement_unit_id
				
				minus
				
				select		uapu.bus_segment_id bus_segment_id,
							uapu.utility_account_id utility_account_id,
							uapu.property_unit_id property_unit_id
				from		util_acct_prop_unit uapu;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during update of valid utility account - property unit combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	commit.
////
			this.of_writelog( "      - committing", true )
			commit;



////
////	
////
			update	oxy_stage_assets stage
			set			loaded = 1
			where company =  :a_gl_company
			and trans_from_asset_id is not null
			and nvl(loaded, 0 ) = 0
			and nvl( stage.errored, 0 ) = 0;
			
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error updating the loaded field for new assets loaded for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
////
////	we were successful so clear out the message and return 1.
////
			a_msg = ""
			return 1
end function

public function long uf_computeretire_add (string a_gl_company, ref string a_msg);////
////	tell the user what we're doing.
////
this.of_writelog( "    - computing retirements/minor adds", true )

////
////	transfer (new).  these will be assets from sap that the serial number matches powerplant but attribute changes
////
this.of_writelog( "      - finding retirements/minor adds", true )
/*
	find any asset with the same serial number but cost/quantity does not match 
*/

update oxy_stage_assets z
set (act_asset_id, act_activity_quantity,act_activity_cost, act_activity_code, act_ferc_activity_code) = 
(
	select ldg_asset_id, activity_quantity, activity_cost, activity_code, ferc_activity_code from
	(
		select company, asset_no, cost_center, ldg_asset_id, 
				nvl(to_number(quantity), 0) - a.accum_quantity activity_quantity, 
				to_number(cost) - a.accum_cost activity_cost,
				decode(sign(to_number( cost ) - a.accum_cost), 1, 'uadd', 'uret') activity_code,
				decode(sign(to_number( cost ) - a.accum_cost), 1, 1, 2) ferc_activity_code
		from cpr_ledger a, oxy_stage_assets b
		where a.asset_id = b.ldg_asset_id
		and b.company =  :a_gl_company
		and (a.accum_cost <> to_number( cost ) or a.accum_quantity <> nvl( to_number( quantity ), 0 ))
		and nvl(b.loaded, 0 ) = 0
		and nvl(b.errored, 0 ) = 0
	) y
	where y.company=z.company
	and y.asset_no=z.asset_no
	and y.cost_center=z.cost_center
	
)
where (company, asset_no, cost_center) in 
(
	select company, asset_no, cost_center
	from cpr_ledger a, oxy_stage_assets b
	where a.asset_id = b.ldg_asset_id
	and b.company =  :a_gl_company
	and (a.accum_cost <> to_number( cost ) or a.accum_quantity <> nvl( to_number( quantity ), 0 ))
	and nvl(b.loaded, 0 ) = 0
	and nvl(b.errored, 0 ) = 0
)
;
if sqlca.sqlcode <> 0 then
	a_msg = "error during population of retirements/minor adds fields for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if
/*
find the next asset activity id for the trans from asset id
trans to asset activity id is always going to be 1
*/
update oxy_stage_assets z
set act_asset_activity_id = 
(
	select asset_activity_id from
	(
		select company, asset_no, cost_center, nvl(max(asset_activity_id),0) + 1 asset_activity_id
		from cpr_activity a, oxy_stage_assets b
		where a.asset_id = b.act_asset_id
		and b.company =  :a_gl_company
		and nvl(b.loaded, 0 ) = 0
		and nvl(b.errored, 0 ) = 0
		and b.act_asset_id is not null
		group by company, asset_no, cost_center
	) y
	where y.company=z.company
	and y.asset_no=z.asset_no
	and y.cost_center=z.cost_center
)
where (company, asset_no, cost_center) in  
(
	select company, asset_no, cost_center
	from cpr_activity a, oxy_stage_assets b
	where a.asset_id = b.act_asset_id
	and b.company =  :a_gl_company
	and nvl(b.loaded, 0 ) = 0
	and nvl(b.errored, 0 ) = 0
	and b.act_asset_id is not null
)
;
if sqlca.sqlcode <> 0 then
	a_msg = "error during population of retirements/minor adds fields (asset_activity_id to) for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if


update	oxy_stage_assets stage
set			stage.act_gl_posting_mo_yr = to_date( stage.extract_date, 'mm/dd/yyyy' ),
			stage.act_out_of_service_mo_yr = null,
			stage.act_cpr_posting_mo_yr = to_date( stage.extract_date, 'mm/dd/yyyy' ),
			stage.act_work_order_number = decode(stage.act_ferc_activity_code, 1, 'minor_add_ldr', 'retirement_ldr'),
			stage.act_gl_je_code = decode(stage.act_ferc_activity_code, 1, 'minor_add_ldr', 'retirement_ldr'),
			stage.act_description = stage.ldg_description,
			stage.act_description = stage.ldg_description,
			stage.act_disposition_code = null,
			stage.act_activity_status = act_ferc_activity_code,
			stage.act_month_number = to_number( substr( trim( stage.extract_date ), -4 ) || substr( trim( stage.extract_date ), 1, 2 ) )
where	stage.company =  :a_gl_company
	and 	nvl(stage.loaded, 0 ) = 0
	and 	nvl(stage.errored, 0 ) = 0
	and 	stage.act_asset_id is not null;
if sqlca.sqlcode <> 0 then
	a_msg = "error during population of retirements/minor fields (activity other attributes) for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if



////
////	commit.
////
			this.of_writelog( "      - committing", true )
			commit;

////
////	we were successful so clear out the message and return 1.
////
			a_msg = ""
			return 1
end function

public function long uf_postretire_add (string a_gl_company, ref string a_msg);////
////	tell the user what we're doing.
////
			this.of_writelog( "    - posting retirements/minor adds", true )


/* add activity lines to the retirements/minor adds asset*/
insert into cpr_activity
(	asset_id, asset_activity_id, gl_posting_mo_yr, out_of_service_mo_yr, cpr_posting_mo_yr, work_order_number, gl_je_code, description, description,
	disposition_code, activity_code, activity_status, activity_quantity, activity_cost, ferc_activity_code, month_number
)
select		stage.act_asset_id asset_id,
			stage.act_asset_activity_id asset_activity_id,
			stage.act_gl_posting_mo_yr gl_posting_mo_yr,
			stage.act_out_of_service_mo_yr out_of_service_mo_yr,
			stage.act_cpr_posting_mo_yr cpr_posting_mo_yr,
			stage.act_work_order_number work_order_number,
			stage.act_gl_je_code gl_je_code,
			stage.act_description description,
			stage.act_description description,
			stage.act_disposition_code disposition_code,
			stage.act_activity_code activity_code,
			stage.act_activity_status activity_status,
			stage.act_activity_quantity activity_quantity,
			stage.act_activity_cost activity_cost,
			stage.act_ferc_activity_code ferc_activity_code,
			stage.act_month_number month_number
from		oxy_stage_assets stage
where stage.company =  :a_gl_company
	and 	nvl(stage.loaded, 0 ) = 0
	and 	nvl(stage.errored, 0 ) = 0
	and 	stage.act_asset_id is not null
;
if sqlca.sqlcode <> 0 then
	a_msg = "error adding retirements/minor adds asset activity into cpr activity for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

insert into cpr_act_basis ( asset_id, asset_activity_id, basis_1 )
select		stage.act_asset_id asset_id,
			stage.act_asset_activity_id asset_activity_id,
			stage.act_activity_cost basis_1
from		oxy_stage_assets stage
where stage.company =  :a_gl_company
	and 	nvl(stage.loaded, 0 ) = 0
	and 	nvl(stage.errored, 0 ) = 0
	and 	stage.act_asset_id is not null
;

if sqlca.sqlcode <> 0 then
	a_msg = "error adding retirements/minor adds into cpr act basis for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

/* now we will insert retirement activity rows for assets that are not on the sap asset file */
insert into cpr_activity
(asset_id, asset_activity_id, gl_posting_mo_yr, cpr_posting_mo_yr,
 work_order_number,gl_je_code, description, description,
 disposition_code, activity_code, activity_status, activity_quantity,
 activity_cost, ferc_activity_code, month_number
)
(select cpr_ledger.asset_id, 
       max(cpr_activity.asset_activity_id)+10000000 asset_activity_id,
       posting_date_view.gl_posting_mo_yr gl_posting_mo_yr,
       posting_date_view.cpr_posting_mo_yr cpr_posting_mo_yr,
       'retirement_ldr' work_order_number,
       'retirement_ldr' gl_je_code,
       cpr_ledger.description description,
       cpr_ledger.description description,
       15 disposition_code,  /* normal retirement */
       'uret' activity_code, 
       1 activity_status,
       sum(cpr_activity.activity_quantity) * -1 activity_quantity,
       sum(cpr_activity.activity_cost) *-1 activity_cost,
       2 ferc_activity_code,
       posting_date_view.month_number month_number
from  cpr_ledger,
      	cpr_activity,
			(select max(act_gl_posting_mo_yr) gl_posting_mo_yr, 
					  max(act_cpr_posting_mo_yr) cpr_posting_mo_yr,
					  max(act_month_number) month_number
				from oxy_stage_assets
			 ) posting_date_view
where cpr_ledger.asset_id = cpr_activity.asset_id
and 	ledger_status = 1
and 	company_id in 
    (select company_id
     from  company_setup
     where company_setup.gl_company_no = :a_gl_company
    )
and not exists
    (select 1 
     from 		oxy_stage_assets
     where 	company = :a_gl_company
     and     	cpr_ledger.serial_number = oxy_stage_assets.ldg_serial_number
     )
group by
       cpr_ledger.asset_id, 
       posting_date_view.gl_posting_mo_yr, 
       posting_date_view.cpr_posting_mo_yr  ,
       'retirement_ldr'  ,
       'retirement_ldr'  ,
       cpr_ledger.description ,
       cpr_ledger.description ,
       15 ,
       'uret' , 
       1 ,
       2 ,
       posting_date_view.month_number 
having sum(cpr_activity.activity_cost) <> 0 or sum(cpr_activity.activity_quantity) <> 0
);  

insert into cpr_act_basis ( asset_id, asset_activity_id, basis_1 )
select		asset_id,
        		asset_activity_id,
        		activity_cost  
from		cpr_activity
where   	activity_code = 'uret'
  and   	disposition_code = 15
  and  not exists
        (select 1
         from 		cpr_act_basis
         where 	cpr_activity.asset_id = cpr_act_basis.asset_id
         and 		cpr_activity.asset_activity_id = cpr_act_basis.asset_activity_id
         )
;

/* now update the cpr ledger tables with the correct balances from the retirement activities we created */
update cpr_ledger z
set (accum_cost, accum_quantity) =
(
	select activity_cost, activity_quantity
	from 
	(
		select a.asset_id, a.accum_cost, a.accum_quantity, sum(b.activity_cost) activity_cost, sum(b.activity_quantity) activity_quantity
		from cpr_ledger a, cpr_activity b
		where a.asset_id = b.asset_id
		group by a.asset_id, a.accum_cost, a.accum_quantity
		having (sum(b.activity_cost) <> a.accum_cost or sum(b.activity_quantity) <> a.accum_quantity)
	) y
	where y.asset_id = z.asset_id
)
where asset_id in (
	select asset_id
	from 
	(
		select a.asset_id, a.accum_cost, a.accum_quantity, sum(b.activity_cost) activity_cost, sum(b.activity_quantity) activity_quantity
		from cpr_ledger a, cpr_activity b
		where a.asset_id = b.asset_id
		group by a.asset_id, a.accum_cost, a.accum_quantity
		having (sum(b.activity_cost) <> a.accum_cost or sum(b.activity_quantity) <> a.accum_quantity)
	) y
)
;
if sqlca.sqlcode <> 0 then
	a_msg = "error updating retirements/minor adds into cpr ledger for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if

update cpr_ldg_basis z
set basis_1 =
(
	select activity_cost
	from 
	(
		select a.asset_id, a.basis_1, sum(b.activity_cost) activity_cost
		from cpr_ldg_basis a, cpr_activity b
		where a.asset_id = b.asset_id
		group by a.asset_id, a.basis_1
		having sum(b.activity_cost) <> a.basis_1
	) y
	where y.asset_id = z.asset_id
)
where asset_id in (
	select asset_id
	from 
	(
		select a.asset_id, a.basis_1, sum(b.activity_cost) activity_cost
		from cpr_ldg_basis a, cpr_activity b
		where a.asset_id = b.asset_id
		group by a.asset_id, a.basis_1
		having sum(b.activity_cost) <> a.basis_1
	) y
)
;			
if sqlca.sqlcode <> 0 then
	a_msg = "error updating adding retirements/minor adds into cpr ldg basis for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
	return -1
end if


////
////	commit.
////
			this.of_writelog( "      - committing", true )
			commit;

////
////	load all of the valid combintations into some of the cpr control tables.
////
////	company bus segment control
////
			this.of_writelog( "      - updating valid company - bus segment combinations", true )
			
			insert into company_bus_segment_control ( company_id, bus_segment_id )
				select		distinct cpr.company_id company_id,
							cpr.bus_segment_id bus_segment_id
				from		cpr_ledger cpr
				
				minus
				
				select		cbsc.company_id company_id,
							cbsc.bus_segment_id bus_segment_id
				from		company_bus_segment_control cbsc;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during update of valid company - business segment combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	company gl account
////
			this.of_writelog( "      - updating valid company - gl account combinations", true )
			
			insert into company_gl_account ( company_id, gl_account_id )
				select		distinct cpr.company_id company_id,
							cpr.gl_account_id gl_account_id
				from		cpr_ledger cpr
				
				minus
				
				select		cgla.company_id company_id,
							cgla.gl_account_id gl_account_id
				from		company_gl_account cgla;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during update of valid company - gl account combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	company major location
////
			this.of_writelog( "      - updating valid company - major location combinations", true )
			
			insert into company_major_location (company_id, major_location_id )
				select		distinct cpr.company_id company_id,
							al.major_location_id major_location_id
				from		cpr_ledger cpr,
							asset_location al
				where	cpr.retirement_unit_id = al.asset_location_id
				
				minus
				
				select		cml.company_id company_id,
							cml.major_location_id major_location_id
				from		company_major_location cml;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during update of valid company - major location combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	company property unit
////
			this.of_writelog( "      - updating valid company - property unit combinations", true )
			
			insert into company_property_unit ( company_id, property_unit_id )
				select		distinct cpr.company_id company_id,
							ru.property_unit_id property_unit_id
				from		cpr_ledger cpr,
							retirement_unit ru
				where	cpr.retirement_unit_id = ru.retirement_unit_id
				
				minus
				
				select		cpu.company_id company_id,
							cpu.property_unit_id property_unit_id
				from		company_property_unit cpu;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during update of valid company - property unit combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	util acct prop unit
////
			this.of_writelog( "      - updating valid utility account - property unit combinations", true )
			
			insert into util_acct_prop_unit ( bus_segment_id, utility_account_id, property_unit_id )
				select		distinct cpr.bus_segment_id bus_segment_id,
							cpr.utility_account_id utility_account_id,
							ru.property_unit_id property_unit_id
				from		cpr_ledger cpr,
							retirement_unit ru
				where	cpr.retirement_unit_id = ru.retirement_unit_id
				
				minus
				
				select		uapu.bus_segment_id bus_segment_id,
							uapu.utility_account_id utility_account_id,
							uapu.property_unit_id property_unit_id
				from		util_acct_prop_unit uapu;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error during update of valid utility account - property unit combinations for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////
////	commit.
////
			this.of_writelog( "      - committing", true )
			commit;



////
////	
////
			update	oxy_stage_assets stage
			set			loaded = 1
			where stage.company =  :a_gl_company
			and 	nvl(stage.loaded, 0 ) = 0
			and 	nvl(stage.errored, 0 ) = 0
			and 	stage.act_asset_id is not null;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error updating the loaded field for new assets loaded for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
////
////	we were successful so clear out the message and return 1.
////
			a_msg = ""
			return 1
end function

public function long of_ref_override (string a_gl_company, ref string a_msg);////
////	tell the user what we're doing.
////
			this.of_writelog( "    - updating override/ref data to staging table", true )

////
////	clear out all of the translations of the unprocessed rows for this company.  also, clear out any error messages or error indicators.
////
//			this.of_writelog( "      - nulling all translation and error columns", true )
			
			
			update oxy_stage_assets a
			set a.cap_on_date = (
				select trim(b.ref_cap_on_date)
				from oxy_stage_assets_ref_override b
				where a.company = b.company
				and a.asset_no = b.asset_no
				and a.cost_center = b.cost_center
				and a.extract_date = b.extract_date
				and trim(b.ref_cap_on_date) is not null
			)
			where exists (
				select 1 from oxy_stage_assets_ref_override b
				where a.company = b.company
				and a.asset_no = b.asset_no
				and a.cost_center = b.cost_center
				and a.extract_date = b.extract_date
				and trim(b.ref_cap_on_date) is not null
			)
			and company = :a_gl_company
			and	nvl( loaded, 0 ) = 0;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error while assigning ref/override cap_on_date for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			
			update oxy_stage_assets a
			set a.state = (
				select trim(b.ref_state)
				from oxy_stage_assets_ref_override b
				where a.company = b.company
				and a.asset_no = b.asset_no
				and a.cost_center = b.cost_center
				and a.extract_date = b.extract_date
				and trim(b.ref_state) is not null
			)
			where exists (
				select 1 from oxy_stage_assets_ref_override b
				where a.company = b.company
				and a.asset_no = b.asset_no
				and a.cost_center = b.cost_center
				and a.extract_date = b.extract_date
				and trim(b.ref_state) is not null
			)
			and company = :a_gl_company
			and	nvl( loaded, 0 ) = 0;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error while assigning ref/override state for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			
			update oxy_stage_assets a
			set a.cost = (
				select trim(b.ref_cost)
				from oxy_stage_assets_ref_override b
				where a.company = b.company
				and a.asset_no = b.asset_no
				and a.cost_center = b.cost_center
				and a.extract_date = b.extract_date
				and trim(b.ref_cost) is not null
			)
			where exists (
				select 1 from oxy_stage_assets_ref_override b
				where a.company = b.company
				and a.asset_no = b.asset_no
				and a.cost_center = b.cost_center
				and a.extract_date = b.extract_date
				and trim(b.ref_cost) is not null
			)
			and company = :a_gl_company
			and	nvl( loaded, 0 ) = 0;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error while assigning ref/override cost for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			

////
////	commit.
////
			this.of_writelog( "      - committing", true )
			commit;

////
////	we were successful so clear out the message and return 1.
////
			a_msg = ""
			return 1
end function

public function long of_populate_ref_ov_tbl (string a_gl_company, ref string a_msg, long a_fails_found);////
////	tell the user what we're doing.
////
			this.of_writelog( "    - populate ref/override data", true )
			
			if a_fails_found = 1 then 
	
	////
	////	insert into oxy_stage_assets_ref_override.  new records that have not been inserted before...
	////		populating ref_facility_number and ref_asset_group because these can be nulled out
	////
				this.of_writelog( "      - inserting failed records into ref/override table", true )
	
				insert into oxy_stage_assets_ref_override
				(company,
				asset_no,
				cost_center,
				extract_date,
				orig_state,
				orig_cost,
				loaded,
				errored,
				error_message)
				select 
				company,
				asset_no,
				cost_center,
				extract_date,
				state,
				cost,
				loaded,
				errored,
				error_message
				from oxy_stage_assets a
				where company = :a_gl_company
				and	nvl( errored, 0 ) = 1
				and not exists (
					select 1 from oxy_stage_assets_ref_override b
					where a.company = b.company
					and a.asset_no = b.asset_no
					and a.cost_center = b.cost_center
					and a.extract_date = b.extract_date
				);
				
				if sqlca.sqlcode <> 0 then
					a_msg = "error during insertion of new records into ref/override data for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
					return -1
				end if
	
				/*update any records in the override table to have an errored flag of "1". 
					the ref/override datawindow will filter to those records with an errored flag of "1".  */
				update oxy_stage_assets_ref_override a
				set (a.errored, a.error_message) = (
					select b.errored, b.error_message
					from oxy_stage_assets b
					where a.company = b.company
					and a.asset_no = b.asset_no
					and a.cost_center = b.cost_center
					and a.extract_date = b.extract_date
					and company = :a_gl_company
					and nvl( errored, 0 ) = 1
				)
				where exists (
					select 1 from oxy_stage_assets b
					where a.company = b.company
					and a.asset_no = b.asset_no
					and a.cost_center = b.cost_center
					and a.extract_date = b.extract_date
					and company = :a_gl_company
					and nvl( errored, 0 ) = 1
				)
				;
					
				if sqlca.sqlcode <> 0 then
					a_msg = "error during insertion of new records into ref/override data for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
					return -1
				end if
	
			end if
		
			/*update the records in the override to have an errored of "100" if they existed in the table, but the fixes
				applied allowed the data to be loaded.  don't want to delete these, as the original data should be saved off.
			*/
			update oxy_stage_assets_ref_override a
			set loaded = 1, errored = 100
			where exists (
				select 1 from oxy_stage_assets b
				where a.company = b.company
				and a.asset_no = b.asset_no
				and a.cost_center = b.cost_center
				and a.extract_date = b.extract_date
				and company = :a_gl_company
				and nvl( errored, 0 ) = 0 /*no errors on the stage table.  so set the errored flag to "100".  */
			)
			;
				
			if sqlca.sqlcode <> 0 then
				a_msg = "error during insertion of new records into ref/override data for '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
////
////	commit.
////
			this.of_writelog( "      - committing", true )
			commit;
			

////
////	we were successful so clear out the message and return 1.
////
//			a_msg = ""
			return 1
end function

public function long uf_classcode_add (string a_gl_company, ref string a_msg);long ret_unit_class_code_id, serial_no_class_code_id, company_id

////
////	tell the user what we're doing.
////
			this.of_writelog( "    - inserting to class code tables", true )


////			
////	retirement unit class codes
////	

			select company_id
			into 	:company_id
			from 	company_setup
			where company_id = to_number(:a_gl_company)
			;
		
			if sqlca.sqlcode <> 0 then
				a_msg = "error retrieving the company id number for class code processing " + &
					"for company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
////			
////	retirement unit class codes
////	

			select class_code_id
			into 	:ret_unit_class_code_id
			from 	class_code
			where upper(description) = 'retirement unit';
		
			if sqlca.sqlcode <> 0 then
				a_msg = "error retrieving the class code id for retirement unit " + &
					"in company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			// insert class code values

			this.of_writelog( "      - inserting class codes values for retirement unit", true )
			
			insert into class_code_values
			(class_code_id, value, ext_class_code_value)
			select :ret_unit_class_code_id, description, retirement_unit_id
			from 		retirement_unit
			where 	not exists
					(select 1
					from 		class_code_values ccv
					where 	ccv.class_code_id 	= :ret_unit_class_code_id
					and   		ccv.value 			= retirement_unit.description
					)
			;
		
			if sqlca.sqlcode <> 0 then
				a_msg = "error inserting into the class code values table for retirement unit " + &
					"in company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

			// insert into class code cpr ledger

			this.of_writelog( "      - inserting into class code cpr ledger for retirement unit", true )
			
			insert into class_code_cpr_ledger
			(class_code_id, asset_id, value)
			select :ret_unit_class_code_id, cpr_ledger.asset_id, retirement_unit.description
			  from cpr_ledger ,
					 retirement_unit
			 where cpr_ledger.retirement_unit_id 	= retirement_unit.retirement_unit_id
				and cpr_ledger.company_id 			= :company_id
				and not exists
					 (select 1
					 	from class_code_cpr_ledger cccl
					 	where 	cccl.class_code_id 	= :ret_unit_class_code_id
						and 		cccl.asset_id 			= cpr_ledger.asset_id
					  )
			;

		
			if sqlca.sqlcode <> 0 then
				a_msg = "error inserting into the class code cpr ledger table for retirement unit " + &
					"in company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

////			
////	serial number class codes
////	
			select class_code_id
			into :serial_no_class_code_id
			from class_code
			where upper(description) = 'serial number';
		
			if sqlca.sqlcode <> 0 then
				a_msg = "error retrieving the class code id for serial number " + &
					"in company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if
			
			// insert class code values

			this.of_writelog( "      - inserting class codes values for serial number", true )

			insert into class_code_values
			(class_code_id, value, ext_class_code_value)
			select :serial_no_class_code_id, serial_number, null  
			from 		cpr_ledger
			where 	company_id = :company_id
			and not exists
					(select 1
					from 		class_code_values ccv
					where 	ccv.class_code_id 	= :serial_no_class_code_id
					and   		ccv.value 			= cpr_ledger.serial_number
					)
			;
		
			if sqlca.sqlcode <> 0 then
				a_msg = "error inserting into the class code values table for serial number " + &
					"in company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if

			// insert into class code cpr ledger

			this.of_writelog( "      - inserting into class code cpr ledger for serial number", true )
			
			insert into class_code_cpr_ledger
			(class_code_id, asset_id, value)
			select  :serial_no_class_code_id, cpr_ledger.asset_id, cpr_ledger.serial_number
			  from  cpr_ledger  
			 where cpr_ledger.company_id = :company_id
				and not exists
					 (select 1
					 from 		class_code_cpr_ledger cccl
					 where 	cccl.class_code_id 	= :serial_no_class_code_id
						and 	cccl.asset_id 			= cpr_ledger.asset_id
					  )
			;		
		
			if sqlca.sqlcode <> 0 then
				a_msg = "error inserting into the class code cpr ledger table for serial number " + &
					"in company '" + a_gl_company + "'.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return -1
			end if			
			return 1
end function

public function long of_run (string a_gl_company[], string a_run_type);/*
 *	processing.
 */
	long	i, maxi
	long	rows 
	string	err_msg

////
////	log in to set up the logs.
////
		
		i_sqlsa = create uo_sqlca_logs
		if i_sqlsa.uf_connect() <> 0 then
			messagebox( "logging set-up error", "error creating a connection to the database (" + i_sqlsa.servername + ") in order to log the running of the load assets interface.  " + &
								"no changes made.~n~nerror: " + i_sqlsa.sqlerrtext )
			i_sqlsa.uf_disconnect()
			destroy i_sqlsa
			return -1
		end if
		
		if i_sqlsa.uf_set_role( "pwrplant_role_dev" ) <> 0 then
			messagebox( "logging set-up error", "error granting the proper database roles during set-up of the load assets interface logging process.  no changes made.~n~nerror: " + &
								i_sqlsa.sqlerrtext )
			i_sqlsa.uf_disconnect()
			destroy i_sqlsa
			return -1
		end if

////
////	get the process and occurrence information for the logs.
////
		setnull( i_logs_proc_id )
		select		process_id
		into		:i_logs_proc_id
		from		pp_processes
		where	trim( upper( description ) ) = trim( upper( :i_logs_proc_desc ) )
		using		i_sqlsa;
		
		if isnull( i_logs_proc_id ) then
			select		nvl( max( process_id ), 0 ) + 1
			into		:i_logs_proc_id
			from		pp_processes
			using		i_sqlsa;
			
			insert into pp_processes ( process_id, description, description )
				values ( :i_logs_proc_id, :i_logs_proc_desc, :i_logs_proc_desc )
				using i_sqlsa;
			
			if i_sqlsa.sqlcode <> 0 then
				messagebox( "logging set-up error", "unable to find and/or create the logging process under which the progress of the load assets interface program will be saved.  " + &
									"no changes made.~n~nerror: " + i_sqlsa.sqlerrtext )
				rollback using i_sqlsa;
				i_sqlsa.uf_disconnect()
				destroy i_sqlsa
				return -1
			end if
		end if
		
		setnull( i_logs_occur_id )
		select		nvl( max( occurrence_id ), 0 ) + 1
		into		:i_logs_occur_id
		from		pp_processes_occurrences
		where	process_id = :i_logs_proc_id
		using		i_sqlsa;
		
		if isnull( i_logs_occur_id ) or i_logs_occur_id = 0 then
			messagebox( "logging set-up error", "unable to obtain a new logging occurrence number under which the progress of the load assets interface program can be saved.  " + &
								"no changes made." )
			i_sqlsa.uf_disconnect()
			destroy i_sqlsa
			return -1
		end if
		
		insert into pp_processes_occurrences ( process_id, occurrence_id, start_time )
			values ( :i_logs_proc_id, :i_logs_occur_id, sysdate )
			using i_sqlsa;
		
		if i_sqlsa.sqlcode <> 0 then
			messagebox( "logging set-up error", "unable to create a new logging occurrence in the database under which the progress of the load assets interface program can be " + &
								"saved.  no changes made.~n~nerror: " + i_sqlsa.sqlerrtext )
			rollback using i_sqlsa;
			i_sqlsa.uf_disconnect()
			destroy i_sqlsa
			return -1
		end if
		
		commit using i_sqlsa;

////
////	tell the user what we're doing.
////
		this.of_writelog( "starting", true )


/// perform the load assets processing
			maxi = upperbound( a_gl_company[] )			

////
////	 loop over the companies and process each one at a time.
////
	
			for i = 1 to maxi
				////
				////	write to the log.
				////
					this.of_writelog( "  - loading company '" + a_gl_company[i] + "' (" + string( i ) + " of " + string( maxi ) + ")", true )
				
				////
				////	initialize the load.  this means clearing out any translations, messages, etc. for all unprocessed assets.
				////
					if this.of_initializeload( a_gl_company[i], err_msg ) <> 1 then
						this.of_writelog( "***** error ***** error ***** error *****", false )
						this.of_writelog( " ", false )
						this.of_writelog( err_msg, false )
						this.of_writelog( " ", false )
						this.of_writelog( "***** error ***** error ***** error *****", false )
						
						messagebox( "initialization error", err_msg )
						i_sqlsa.uf_disconnect()
						destroy i_sqlsa
						
						rollback;
						
						return -1
					end if
				
	 			
				////
				////	re-assign various fields based on what the user input in the ref/override window.
				////		this table is populated based on errors that occur, so the user can manually enter in
				////		appropriate data
				////
					if this.of_ref_override( a_gl_company[i], err_msg ) <> 1 then
						this.of_writelog( "***** error ***** error ***** error *****", false )
						this.of_writelog( " ", false )
						this.of_writelog( err_msg, false )
						this.of_writelog( " ", false )
						this.of_writelog( "***** error ***** error ***** error *****", false )
						
						messagebox( "ref/override error", err_msg )
						i_sqlsa.uf_disconnect()
						destroy i_sqlsa
						
						rollback;
						
						return -1
					end if

					
				////
				////	automatically populate the powerplant control tables with any new values that come over from sap in this company's asset feed.
				////
					if this.of_populatecontroltables( a_gl_company[i], err_msg ) <> 1 then
						this.of_writelog( "***** error ***** error ***** error *****", false )
						this.of_writelog( " ", false )
						this.of_writelog( err_msg, false )
						this.of_writelog( " ", false )
						this.of_writelog( "***** error ***** error ***** error *****", false )
						
						messagebox( "control table population error", err_msg )
						i_sqlsa.uf_disconnect()
						destroy i_sqlsa
						
						rollback;
						
						return -1
					end if
				
				////
				////	translate all of the fields on the sap asset to the powerplant control tables.
				////
					if this.of_translatefields( a_gl_company[i], err_msg ) <> 1 then
						this.of_writelog( "***** error ***** error ***** error *****", false )
						this.of_writelog( " ", false )
						this.of_writelog( err_msg, false )
						this.of_writelog( " ", false )
						this.of_writelog( "***** error ***** error ***** error *****", false )
						
						messagebox( "translation error", err_msg )
						i_sqlsa.uf_disconnect()
						destroy i_sqlsa
						
						rollback;
						
						return -1
					end if
				
				////
				////	figure out all of the activity that we'll need to create for the sap assets and populate that activity information on the staging table.
				////
					if this.of_computeactivity( a_gl_company[i], err_msg ) <> 1 then
						this.of_writelog( "***** error ***** error ***** error *****", false )
						this.of_writelog( " ", false )
						this.of_writelog( err_msg, false )
						this.of_writelog( " ", false )
						this.of_writelog( "***** error ***** error ***** error *****", false )
						
						messagebox( "activity computation error", err_msg )
						i_sqlsa.uf_disconnect()
						destroy i_sqlsa
						
						rollback;
						
						return -1
					end if
				
				////
				////	check for mapping errors.  if any exist, write so in the log and continue
				////		onto the next company.
				////
					this.of_writelog( "    - checking for errors", true )
					
					setnull( rows )
					select		count('x')
					into		:rows
					from		oxy_stage_assets stage
					where	stage.company = :a_gl_company[i]
						and	nvl( stage.errored, 0 ) = 1;
					
					if rows > 0 then
						if rows = 1 then
							err_msg = string( rows ) + " error was "
						else
							err_msg = string( rows ) + " errors were "
						end if
						
//						err_msg = err_msg + "encountered when trying to load the asset for '" + a_gl_company[i] + "'.  please check the sap assets staging table to see the actual errors " + &
//							"for each row from sap that triggered an error.~n~nno changes made."

						err_msg = err_msg + "encountered when trying to load the asset(s) for '" + a_gl_company[i] + "'.  please go to the sap ref/override window to correct input/staged data "+&
							"appropriately.~n~nno changes made."

						
						this.of_writelog( "***** error ***** error ***** error *****", false )
						this.of_writelog( " ", false )
						this.of_writelog( err_msg, false )
						this.of_writelog( " ", false )
						this.of_writelog( "***** error ***** error ***** error *****", false )
						
						////
						////	of_populate_ref_ov_tbl.  once failures have been found, populate the ref/override table
						////		so users can manipulate data so the data passes to plant appropriately.
						////
						if this.of_populate_ref_ov_tbl( a_gl_company[i], err_msg, 1) <> 1 then
							this.of_writelog( "***** error ***** error ***** error *****", false )
							this.of_writelog( " ", false )
							this.of_writelog( err_msg, false )
							this.of_writelog( " ", false )
							this.of_writelog( "***** error ***** error ***** error *****", false )
							
							messagebox( "populate ref/override error", err_msg )
							i_sqlsa.uf_disconnect()
							destroy i_sqlsa
							
							rollback;
							
							return -1
						end if	
						
					else
						////
						////	of_populate_ref_ov_tbl.  no failures found, but go ahead and update the error
						////		field on the ref/override table to 100 for existing records (the input data fixed the 
						////		issue)
						if this.of_populate_ref_ov_tbl( a_gl_company[i], err_msg, 0) <> 1 then
							this.of_writelog( "***** error ***** error ***** error *****", false )
							this.of_writelog( " ", false )
							this.of_writelog( err_msg, false )
							this.of_writelog( " ", false )
							this.of_writelog( "***** error ***** error ***** error *****", false )
							
							messagebox( "populate ref/override error", err_msg )
							i_sqlsa.uf_disconnect()
							destroy i_sqlsa
							
							rollback;
							
							return -1
						end if	
						
					end if
					
				////
				////	create our assets and activity.  that is, actually load the cpr tables, now.
				////
					if this.of_postactivity( a_gl_company[i], err_msg ) <> 1 then
						this.of_writelog( "***** error ***** error ***** error *****", false )
						this.of_writelog( " ", false )
						this.of_writelog( err_msg, false )
						this.of_writelog( " ", false )
						this.of_writelog( "***** error ***** error ***** error *****", false )
						
						messagebox( "posting activity error", err_msg )
						i_sqlsa.uf_disconnect()
						destroy i_sqlsa
						
						rollback;
						
						return -1
					end if

				////
				////	figure out all of the activity that we'll need to create for the sap assets and populate that activity information on the staging table.
				////
					if this.uf_computetransfer( a_gl_company[i], err_msg ) <> 1 then
						this.of_writelog( "***** error ***** error ***** error *****", false )
						this.of_writelog( " ", false )
						this.of_writelog( err_msg, false )
						this.of_writelog( " ", false )
						this.of_writelog( "***** error ***** error ***** error *****", false )
						
						messagebox( "transfer activity computation error", err_msg )
						i_sqlsa.uf_disconnect()
						destroy i_sqlsa
						
						rollback;
						
						return -1
					end if

				////
				////	create our transfer and activity.  that is, actually load the cpr tables, now.
				////
					if this.uf_posttransfer( a_gl_company[i], err_msg ) <> 1 then
						this.of_writelog( "***** error ***** error ***** error *****", false )
						this.of_writelog( " ", false )
						this.of_writelog( err_msg, false )
						this.of_writelog( " ", false )
						this.of_writelog( "***** error ***** error ***** error *****", false )
						
						messagebox( "posting transfer activity error", err_msg )
						i_sqlsa.uf_disconnect()
						destroy i_sqlsa
						
						rollback;
						
						return -1
					end if

				////
				////	figure out all of the activity that we'll need to create for the sap assets and populate that activity information on the staging table.
				////
					if this.uf_computeretire_add( a_gl_company[i], err_msg ) <> 1 then
						this.of_writelog( "***** error ***** error ***** error *****", false )
						this.of_writelog( " ", false )
						this.of_writelog( err_msg, false )
						this.of_writelog( " ", false )
						this.of_writelog( "***** error ***** error ***** error *****", false )
						
						messagebox( "retirement activity computation error", err_msg )
						i_sqlsa.uf_disconnect()
						destroy i_sqlsa
						
						rollback;
						
						return -1
					end if

				////
				////	create our transfer and activity.  that is, actually load the cpr tables, now.
				////
					if this.uf_postretire_add( a_gl_company[i], err_msg ) <> 1 then
						this.of_writelog( "***** error ***** error ***** error *****", false )
						this.of_writelog( " ", false )
						this.of_writelog( err_msg, false )
						this.of_writelog( " ", false )
						this.of_writelog( "***** error ***** error ***** error *****", false )
						
						messagebox( "posting retirement activity error", err_msg )
						i_sqlsa.uf_disconnect()
						destroy i_sqlsa
						
						rollback;
						
						return -1
					end if

				////
				////	finally, we are going to load the class code tables based on the activity we have generated.  this will facilitate extra fields going into the prop tax ledger
				////
					if this.uf_classcode_add( a_gl_company[i], err_msg ) <> 1 then
						this.of_writelog( "***** error ***** error ***** error *****", false )
						this.of_writelog( " ", false )
						this.of_writelog( err_msg, false )
						this.of_writelog( " ", false )
						this.of_writelog( "***** error ***** error ***** error *****", false )
						
						messagebox( "posting class codes error", err_msg )
						i_sqlsa.uf_disconnect()
						destroy i_sqlsa
						
						rollback;
						
						return -1
					end if				
				////
				////	commit.
				////
					commit;
					this.of_writelog( "  - completed company '" + a_gl_company[i] + "' (" + string( i ) + " of " + string( maxi ) + ")", true )
			next

////
//// perform final asset location updates - these need to be done once only, so we keep them outside the loop
////
			if this.uf_final_asset_location_updates () <> 1 then
						this.of_writelog( "***** error ***** error ***** error *****", false )
						this.of_writelog( " ", false )
						
						messagebox( "error updating asset locations", err_msg )
						i_sqlsa.uf_disconnect()
						destroy i_sqlsa
						
						rollback;
						
						return -1
			end if		


////
//// perform final company updates - these need to be done once only, so we keep them outside the loop
////
			if this.uf_final_company_updates () <> 1 then
						this.of_writelog( "***** error ***** error ***** error *****", false )
						this.of_writelog( " ", false )
						
						messagebox( "error updating company information for newly added companies", err_msg )
						i_sqlsa.uf_disconnect()
						destroy i_sqlsa
						
						rollback;
						
						return -1
			end if	
////
////	be done.
////
			this.of_writelog( "completed", true )

////
////	finish off all our logging stuff.
////
			update	pp_processes_occurrences
			set			end_time = sysdate
			where	process_id = :i_logs_proc_id
				and	occurrence_id = :i_logs_occur_id
			using		i_sqlsa;
			
			if i_sqlsa.sqlcode <> 0 then
				messagebox( "logging completion error", "unable to update the ending time for the load assets logging process for this run of the interface.  this does not affect the " + &
									"loading of the assets.  the assets loading process will still continue as normal.~n~nerror: " + i_sqlsa.sqlerrtext )
				rollback using i_sqlsa;
			end if
			
			commit using i_sqlsa;
			setnull( i_logs_proc_id )
			setnull( i_logs_occur_id )
			
			i_sqlsa.uf_disconnect()
			destroy i_sqlsa

////
////	we finished load assets successfully.
////
			messagebox( "complete", "the load assets interface completed successfully for the selected companies." )
		

////
////	we finished whatever we did successfully.
////		
		return 1
		
		
		
end function

public function long uf_final_asset_location_updates ();boolean success

success = true
///
/// assign the tax district of cpr search - this way, oxy can use the state to filter assets with asset location (we don;t have the actual
////  county values yet - that will come with mapping to parcels
///
update asset_location al
set al.tax_district_id = (
	select td.tax_district_id
	from prop_tax_district td
	where td.tax_district_code = 'cpr search'
	and td.state_id = al.state_id
	and nvl(al.tax_district_id,-999) <> nvl(td.tax_district_id,-999)
)
where al.state_id is not null
and exists (
	select 1
	from prop_tax_district td
	where td.tax_district_code = 'cpr search'
	and td.state_id = al.state_id
	and nvl(al.tax_district_id,-999) <> nvl(td.tax_district_id,-999)
);  
if sqlca.sqlcode <> 0 then
	of_writelog("error: insert updating tax district id on asset location (part 1).~n~nerror: " + sqlca.sqlerrtext, true)
	success = false
	goto theend
end if

update asset_location al
set al.tax_district_id = (
	select td.tax_district_id
	from prop_tax_district td
	where td.tax_district_code = 'cpr search'
	and lower(td.state_id) like '%unknown%'
)
where trim(al.state_id) is null
and trim(al.tax_district_id) not in (
	select td.tax_district_id
	from prop_tax_district td
	where td.tax_district_code = 'cpr search'
	and lower(td.state_id) like '%unknown%'
);  

if sqlca.sqlcode <> 0 then
	of_writelog("error: insert updating tax district id on asset location (part 2).~n~nerror: " + sqlca.sqlerrtext, true)
	success = false
	goto theend
end if

///
/// assign the minor location for company-owned service stations.   these are situs codes
/// that start with dcm0, yet the cost center maps to the facility code on the minor location table
/// as a result, we want to assign the appropriate minor location to the asset location if one does not exist already
///

update asset_location al
set minor_location_id = 
    (select minor_location_id
    from minor_location ml
     where to_number(substr(al.ext_asset_location,5,6)) = ml.minor_location_id
     and   is_number(substr(al.ext_asset_location,5,6)) = 1
     and   is_number(substr(al.ext_asset_location,1,3)) <> 1
     )
where ext_asset_location like 'dcm0%'
and   minor_location_id is null
and exists
(select 1
    from minor_location ml
     where to_number(substr(al.ext_asset_location,5,6)) = ml.minor_location_id
     and   is_number(substr(al.ext_asset_location,5,6)) = 1
     and   is_number(substr(al.ext_asset_location,1,3)) <> 1
    )
;

if sqlca.sqlcode <> 0 then
	of_writelog("error: error updating null minor location id on asset location for company owned service stations (situs dcm0xxxxxx).~n~nerror: " + sqlca.sqlerrtext, true)
	success = false
	goto theend
end if

///
/// assign the minor location for asset locations where the minor location is missing.
///
/// the minor location gets created when the facilities are imported.   the asset location is created by the assets, but
/// there are situations when an asset comes over from sap and the facility number is missing.
/// in those cases, the asset location is not populated with a minor location.   later the facility number is assigned to the asset record,
/// but no process goes back to re-attach the minor location to the asset location.
///
/// as a result, we want to assign the appropriate minor location to the asset location if one does not exist already
///

update asset_location al
set minor_location_id = 
    (select minor_location_id
     from minor_location ml
     where to_number(trim(substr(al.ext_asset_location,1,7))) = ml.minor_location_id
     and   is_number(trim(substr(al.ext_asset_location,1,7))) = 1
     )
where ext_asset_location like '%-dcm%'
and   minor_location_id is null
and exists
	(select 1
   	 from minor_location ml
     where to_number(trim(substr(al.ext_asset_location,1,7))) = ml.minor_location_id
     and   is_number(trim(substr(al.ext_asset_location,1,7))) = 1
     )
;

if sqlca.sqlcode <> 0 then
	of_writelog("error: error updating null minor location ids on asset location for numeric (non-dcm) situses.~n~nerror: " + sqlca.sqlerrtext, true)
	success = false
	goto theend
end if
theend:
if success then
	return 1
else
	return -1
end if

end function

public function integer of_translatecountycode (string a_gl_company, ref string a_msg);//////
//////	tell the user what we're doing.
//////

    		return 1
end function

public function long uf_final_company_updates ();boolean success

success = true
///
///   add necessary rows to pt_control for the newly added companies. 
///
insert into pt_control
(company_id, state_id, tax_year, prior_tax_year)
select company_id, state_id, tax_year, prior_tax_year
from 	company_setup,
     	state,
     	property_tax_year
where property_tax_year.valuation_year > 2008
and (state.state_code) in 
    (select distinct state
    from oxy_stage_assets
    )
and not exists
      (select 1
      from pt_control
      where pt_control.company_id = company_setup.company_id
      and   pt_control.state_id = state.state_id
      and   pt_control.tax_year = property_tax_year.tax_year
      )
and exists
	  (select 1
	  from pt_control
	  where pt_control.tax_year = property_tax_year.tax_year
	  )
;

if sqlca.sqlcode <> 0 then
	of_writelog("error: insert to pt_control for newly added companies within existing tax years.~n~nerror: " + sqlca.sqlerrtext, true)
	success = false
	goto theend
end if

//
//property tax year lock
//

insert into property_tax_year_lock
(company_id, state_id, tax_year, locked)
select company_id, state_id, tax_year, 0
from company_setup,
     state,
     property_tax_year
where property_tax_year.valuation_year > 2008
and (state.state_code) in 
    (select distinct state
    from oxy_stage_assets
    )
and not exists
      (select 1
      from property_tax_year_lock
      where property_tax_year_lock.company_id = company_setup.company_id
      and   property_tax_year_lock.state_id = state.state_id
      and   property_tax_year_lock.tax_year = property_tax_year.tax_year
      )
and exists
    (select 1
    from property_tax_year_lock
    where property_tax_year_lock.tax_year = property_tax_year.tax_year
    )
; 

if sqlca.sqlcode <> 0 then
	of_writelog("error: insert to property_tax_year_lock for newly added companies within existing tax years.~n~nerror: " + sqlca.sqlerrtext, true)
	success = false
	goto theend
end if

//
//cpr_control
//

insert into cpr_control
(company_id, accounting_month)
select distinct company_setup.company_id, extract_date
from company_setup,
     (select max(to_date(extract_date,'mm/dd/yyyy')) extract_date
        from oxy_stage_assets
     ) month_view
where (company_setup.company_id, month_view.extract_date) not in
    (select company_id, accounting_month
       from cpr_control
    )
;

if sqlca.sqlcode <> 0 then
	of_writelog("error: insert to cpr_control for newly added companies and/or extract dates.~n~nerror: " + sqlca.sqlerrtext, true)
	success = false
	goto theend
end if

theend:
if success then
	return 1
else
	return -1
end if

end function

on uo_oxy_ashell_loader.create
call super::create
triggerevent( this, "constructor" )
end on

on uo_oxy_ashell_loader.destroy
triggerevent( this, "destructor" )
call super::destroy
end on

