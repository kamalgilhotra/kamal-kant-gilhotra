forward
global type w_oxy_stg_asst_rev_ov from w_top
end type
type dw_company from uo_dw_grid_multi_no_cntl_top within w_oxy_stg_asst_rev_ov
end type
type pb_close from picturebutton within w_oxy_stg_asst_rev_ov
end type
type st_noload_message from statictext within w_oxy_stg_asst_rev_ov
end type
type pb_load from picturebutton within w_oxy_stg_asst_rev_ov
end type
type pb_companies from picturebutton within w_oxy_stg_asst_rev_ov
end type
end forward

global type w_oxy_stg_asst_rev_ov from w_top
integer width = 3657
integer height = 2016
string title = "asset shell : chevron : correct sap errors"
16777215
dw_company dw_company
pb_close pb_close
st_noload_message st_noload_message
pb_load pb_load
pb_companies pb_companies
end type
global w_oxy_stg_asst_rev_ov w_oxy_stg_asst_rev_ov

type variables
////
////	used to track whether we go to the company table or not for refreshing the datawindow.
////
	boolean	i_opened_company_table = false
end variables

forward prototypes
public subroutine of_retrievecompanies ()
end prototypes

public subroutine of_retrievecompanies ();////
////	retrieve the eligible companies datawindow.
////
			dw_company.retrieve()

////
////	change what's visible/enabled based on if we have any company's to load or not.
////
			if dw_company.rowcount() = 0 then
				st_noload_message.visible = true
				pb_load.enabled = false
			else
				st_noload_message.visible = false
			end if
end subroutine

on w_oxy_stg_asst_rev_ov.create
int icurrent
call super::create
this.dw_company=create dw_company
this.pb_close=create pb_close
this.st_noload_message=create st_noload_message
this.pb_load=create pb_load
this.pb_companies=create pb_companies
icurrent=upperbound(this.control)
this.control[icurrent+1]=this.dw_company
this.control[icurrent+2]=this.pb_close
this.control[icurrent+3]=this.st_noload_message
this.control[icurrent+4]=this.pb_load
this.control[icurrent+5]=this.pb_companies
end on

on w_oxy_stg_asst_rev_ov.destroy
call super::destroy
destroy(this.dw_company)
destroy(this.pb_close)
destroy(this.st_noload_message)
destroy(this.pb_load)
destroy(this.pb_companies)
end on

event open;call super::open;////
////	before we do anything, start with the nothing to load message invisible.
////
			st_noload_message.visible = false

////
////	set up the eligible companies datawindow.  then, retrieve it.
////
			dw_company.settransobject( sqlca )
			this.of_retrievecompanies()
end event

event activate;call super::activate;////
////	if we didn't go to the company table before we activated this, get on out of here.
////
			if i_opened_company_table = false then return

////
////	if we've made it here then we went to the companies table where things could have been edited/added so re-retrieve the eligible companies datawindow.
////
			this.of_retrievecompanies()
			i_opened_company_table = false
end event

type dw_all_cons_columns from w_top`dw_all_cons_columns within w_oxy_stg_asst_rev_ov
integer taborder = 0
end type

type dw_table_help from w_top`dw_table_help within w_oxy_stg_asst_rev_ov
integer taborder = 0
end type

type dw_help from w_top`dw_help within w_oxy_stg_asst_rev_ov
integer taborder = 0
end type

type dw_topkey from w_top`dw_topkey within w_oxy_stg_asst_rev_ov
integer taborder = 0
end type

type dw_company from uo_dw_grid_multi_no_cntl_top within w_oxy_stg_asst_rev_ov
integer x = 46
integer y = 40
integer width = 3049
integer height = 1824
integer taborder = 0
boolean bringtotop = true
string dataobject = "dw_oxy_stg_assets_ref_ov"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event constructor;call super::constructor;////
////	we don't want that stupid mouse selection stuff.
////
			this.modify( "datawindow.selected.mouse=no" )
end event

type pb_close from picturebutton within w_oxy_stg_asst_rev_ov
integer x = 3141
integer y = 168
integer width = 439
integer height = 100
integer taborder = 20
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
string text = "close"
boolean originalsize = true
vtextalign vtextalign = vcenter!
8388608
16777215
end type

event clicked;////
////	close this here window.
////
			close( parent )
end event

type st_noload_message from statictext within w_oxy_stg_asst_rev_ov
integer x = 187
integer y = 500
integer width = 1888
integer height = 72
boolean bringtotop = true
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
12632256
string text = "there are currently no assets from sap that failed to load."
alignment alignment = center!
boolean focusrectangle = false
end type

type pb_load from picturebutton within w_oxy_stg_asst_rev_ov
integer x = 3141
integer y = 40
integer width = 439
integer height = 100
integer taborder = 10
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
string text = "update"
boolean originalsize = true
vtextalign vtextalign = vcenter!
8388608
16777215
end type

event clicked;/*
 *	processing.
 */
	long	i, maxi, j
	string	glco[]

/*
 *	misc objects.
 */

////
////	grab all of the selected companies.
////
	
			dw_company.update()
	
	
////
////	re-retrieve the eligible companies datawindow.
////
			of_retrievecompanies()
end event

type pb_companies from picturebutton within w_oxy_stg_asst_rev_ov
integer x = 3141
integer y = 1764
integer width = 439
integer height = 100
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "arial"
string text = "companies ..."
boolean originalsize = true
vtextalign vtextalign = vcenter!
8388608
16777215
end type

event clicked;/*
 * misc objects.
 */
	s_pp_table_parms table_info
	w_pp_table_grid lw_pp_table_grid

////
////	set the table name and description for the company table that we're going to open.
////
			table_info.table_name = "company_setup"
			
			select		description
			into		:table_info.table_short_desc
			from		powerplant_tables
			where	lower( table_name ) = :table_info.table_name;

////
////	open up the standard table maintenance window for the company table.  (also, note that we're about to open the company table for editing.)
////
			i_opened_company_table = true
			opensheetwithparm( lw_pp_table_grid, table_info, "w_pp_table_grid", w_top_frame, 7, original! )
end event

