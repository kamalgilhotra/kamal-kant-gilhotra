forward
global type nvo_ptc_logic_parcel from nonvisualobject
end type
end forward

global type nvo_ptc_logic_parcel from nonvisualobject
end type
global nvo_ptc_logic_parcel nvo_ptc_logic_parcel

type variables
////
////	constants that identify fixed value field for assessment calc values function.
////
	constant  i_fixed_field_assessment = 1
	constant  i_fixed_field_equalized_value = 2
	constant  i_fixed_field_taxable_value = 3

////
////	constants for each of the flex field usage options.
////
	constant  i_flex_usage_not_used = 1
	constant  i_flex_usage_required = 2
	constant  i_flex_usage_optional = 3
	
////
////	constants for the equalization factor types.
////
	constant  i_eq_factor_none = 0
	constant  i_eq_factor_county = 1
	constant  i_eq_factor_district = 2

////
////	reserved parcel types.
////
	 i_reserved_ptype_allocated = -100

////
////	variables to control how many parcels until we use the temp parcel table or parcel-assesment group combos before we use the temp parcel ag table.  this will come from a system value that can
////	be set at clients.  there's really no need to change the default system option unless the client experiences  times in copying a large number of assessments from one year to the next.  (this
////	may also be dependant on the client's oracle version as older oracle version may need to have a much lower value for this variable.
////
		i_temp_prcl_start
		i_temp_prcl_ag_start

////
////	the case logic object.
////
	nvo_ptc_logic_case i_uo_case

////
////	the system options functions.
////
	nvo_ppbase_sysoption_functions i_uo_systemoptions
	
////
////	import lookups used for deriving parcel assessment values.
////
		ilook_derive_asmt_from_eqval = 112
		ilook_derive_asmt_from_taxval = 113
		ilook_derive_eqval_from_asmt = 114
		ilook_derive_eqval_from_taxval = 115
		ilook_derive_taxval_from_asmt = 116
		ilook_derive_taxval_from_eqval = 117
end variables

forward prototypes
public function boolean of_assessmentdelete ( a_prcl_id[],  a_ag_id[],  a_case_id, ref string a_msg)
public function boolean of_assessmentedit (s_pt_parcel_assessment as_asmt[],  a_case_id, boolean a_update_date, boolean a_update_assessor, ref string a_msg)
public function boolean of_assessmentcopy ( a_prcl_id[], s_ptc_assessments_copy_trending_options as_ag[],  a_from_case_id,  a_to_case_id, boolean a_overwrite_yn, ref string a_msg)
public function boolean of_assessmentcalcvalues ( a_prcl_id[],  a_ag_id[],  a_case_id,  a_fixed_field, ref string a_msg)
public function boolean of_parcelvalidate (s_pt_parcel as_prcl, ref string a_msg)
public function boolean of_parcelvalidate (s_pt_parcel as_prcl, s_pt_district as_dist, ref string a_msg)
public function boolean of_parceladd (ref s_pt_parcel as_prcl, ref string a_msg)
public function boolean of_parceladd (ref s_pt_parcel as_prcl, ref s_pt_district as_dist, ref string a_msg)
public function boolean of_parceledit (s_pt_parcel as_prcl, s_pt_district as_dist, string a_mod_fields[], ref string a_msg)
public function boolean of_parcelautoadjustedit (s_pt_parcel_auto_adjust as_adj[], ref string a_msg)
public function boolean of_parcelgeographyedit (s_pt_parcel_geography as_geo[], ref string a_msg)
public function boolean of_assessmentdelete ( a_prcl_id,  a_ag_id,  a_case_id, ref string a_msg)
public function boolean of_assessmentedit (s_pt_parcel_assessment as_asmt,  a_case_id, boolean a_update_date, boolean a_update_assessor, ref string a_msg)
public function boolean of_parcelhistoryedit (ref s_pt_parcel_history as_hist[], ref string a_msg)
public function boolean of_parcelhistorydelete ( a_parcel_id,  a_event_id, ref string a_msg)
public function boolean of_parcelappraisaldelete ( a_parcel_id,  a_appraisal_id,  a_assessment_group_id, ref string a_msg)
public function boolean of_parcelappraisaledit (ref s_pt_parcel_appraisal as_appr[], ref string a_msg)
public function boolean of_assessmentcalcvalues ( a_case_id, string a_where_clause,  a_fixed_field, boolean a_recalc_cc, ref string a_msg)
public function boolean of_assessmentspread ( a_case_id, string a_where_clause, string a_ratio_sql, string a_round_col_sql[], string a_round_val_sql, ref string a_msg)
public function boolean of_parcelassetadd ( a_parcel_id,  a_asset_id, ref string a_msg)
public function boolean of_parcelassetadd ( a_parcel_id,  a_asset_id[], ref string a_msg)
public function boolean of_parcelassetdelete ( a_parcel_id,  a_asset_id, ref string a_msg)
public function boolean of_parcelassetdelete ( a_parcel_id,  a_asset_id[], ref string a_msg)
public function boolean of_assessmentspread ( a_case_id, string a_where_clause, string a_ratio_sql, string a_round_col_sql[], string a_round_val_sql,  a_ptco_id, string a_st_id, ref string a_msg)
public function boolean of_assessmentcopy (string a_where_clause,  a_from_case_id,  a_to_case_id, decimal a_trend_percent, boolean a_overwrite_yn, ref string a_msg)
public function boolean of_parceldelete ( a_parcel_id, ref string a_msg)
public function boolean of_parceldelete ( a_parcel_id[], ref string a_msg)
public function boolean of_assessmentupdateeligibility ( a_case_id, boolean a_is_delete, ref string a_msg)
public function boolean of_assessmentupdateeligibility ( a_case_id,  a_prcl_id[],  a_ag_id[], boolean a_is_delete, ref string a_msg)
public function boolean of_assessmentupdateeligibility ( a_case_id,  a_parcel_id,  a_ag_id, boolean a_is_delete, ref string a_msg)
public function boolean of_assessmentdeleteforcase ( a_case_id, ref string a_msg)
public function boolean of_assessmentcalcvalues ( a_prcl_id,  a_ag_id,  a_case_id,  a_fixed_field, ref string a_msg)
public function boolean of_parcelresponsibilitydelete ( a_parcel_id,  a_resp_entity_id,  a_resp_type_id,  a_tax_year, ref string a_msg)
public function boolean of_parcelresponsibilityedit (s_pt_parcel_responsibility as_resp[], ref string a_msg)
public function boolean of_parcelresponsibleentityedit (ref s_pt_parcel_responsible_entity as_resp_ent, ref string a_msg)
public function boolean of_parcelgeographyfactorcopy ( a_tax_year_from,  a_tax_year_to,  a_geography_type_id, ref string a_msg)
public function boolean of_parcelgeographyfactordelete ( a_tax_year,  a_geography_type_id,  a_age, ref string a_msg)
public function boolean of_parcelgeographyfactoredit (s_pt_parcel_geo_type_factors as_factors[], ref string a_msg)
public function boolean of_parcelgeographyeditfromimport ( a_run_id,  a_template_id, ref string a_msg)
public function boolean of_parcelgeographyvalidatefromimport ( a_run_id,  a_template_id, ref string a_msg)
public function boolean of_parcelresponsibilityeditfromimport ( a_run_id,  a_template_id, ref string a_msg)
public function boolean of_parcelresponsibilityvalidatefromimp ( a_run_id,  a_template_id, ref string a_msg)
public function boolean of_parcelresponsibleentityeditfromimport ( a_run_id,  a_template_id, ref string a_msg)
public function boolean of_parcelresponsibleentityvalidfromimp ( a_run_id,  a_template_id, ref string a_msg)
public function boolean of_parcelgeographyfactorvalidatefromimp ( a_run_id, ref string a_msg)
public function boolean of_parcelgeographyfactorupdatefromimport ( a_run_id, ref string a_msg)
public function boolean of_parceladdfromimport ( a_run_id,  a_template_id, ref string a_msg)
public function boolean of_parcelvalidateaddfromimport ( a_run_id,  a_template_id, ref string a_msg)
public function boolean of_parcelvalidateeditfromimport ( a_run_id,  a_template_id, ref string a_msg)
public function boolean of_parceleditfromimport ( a_run_id,  a_template_id, ref string a_msg)
public function boolean of_assessmentvalidatefromimport ( a_run_id, ref string a_msg)
public function boolean of_assessmenteditfromimport ( a_run_id,  a_template_id, ref string a_msg)
public function boolean of_parcelappealeventedit (ref s_pt_parcel_appeal_event as_event[], ref string a_msg)
public function boolean of_parcelappealeventdelete (s_pt_parcel_appeal_event as_event, ref string a_msg)
public function boolean of_parcelappealdelete (s_pt_parcel_appeal as_appeal, ref string a_msg)
public function boolean of_parcelappealedit (ref s_pt_parcel_appeal as_appeal[], ref string a_msg)
end prototypes

public function boolean of_assessmentdelete ( a_prcl_id[],  a_ag_id[],  a_case_id, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_assessmentdelete()
 **	
 **	deletes existing assessments.  multiple parcel / assessment group combinations can be deleted at once by passing in multiple values in the parcel and assessment group arrays.  the number
 **	of nodes in the parcel and assessment group arrays must match as the values in each node correspond.  that is, a_prcl_id[1] goes with a_ag_id[1], a_prcl_id[2] goes with a_ag_id[2], etc.
 **	the function will also update the case calc and case calc authority tables in real time.
 **	
 **	this function can only handle deleting from one case at a time - if you want to delete assessments for multiple cases you will need to call this function for each case from which you're
 **	deleting assessments.
 **	
 **	parameters	:		:	(a_prcl_id[]) combined with a_ag_id[] this defines the assessment to delete.
 **							:	(a_ag_id[]) combined with a_prcl_id[] this defines the assessment to delete.
 **							:	(a_case_id) the case for which the assessments should be deleted.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the assessments were deleted successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi
		ptco_id
	string	st_id
	string	rtn_str, sqls, pasmt_where, cc_where
	string	st_sqls, ptco_sqls
	boolean	use_temp_prcl_ag

/*
 *	misc objects.
 */
	uo_ds_top ds_st
	uo_ds_top ds_ptco

////
////	make sure the parameters are ok.
////
			maxi = upperbound( a_prcl_id[] )
			
			if upperbound( a_ag_id[] ) <> maxi then
				a_msg = "an internal error occurred while attempting to delete assessments.  there is a mismatch between the parcels for which i'm to delete assessments and the assessment " + &
					"groups for which i'm to delete assessments.  if you continue to receive this error please contact your internal powerplant support or the powerplan property tax support " + &
					"desk."
				return false
			end if
			
			if maxi = 0 then return true
			
			if isnull( a_case_id ) then
				a_msg = "an internal error occurred while attempting to delete assessments.  no case was specified as the case from which to delete assessments.  if you continue to receive this " + &
					"error please contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if

////
////	validate update eligibility.  do this before messing with temp parcels ag as the function we're calling uses (and clears out) temp parcels ag itself.
////
			if not this.of_assessmentupdateeligibility( a_case_id, a_prcl_id[], a_ag_id[], true, a_msg ) then return false

////
////	see if we should use temp parcels ag.  if so, populate it and build the where clauses to work off it.
////
			use_temp_prcl_ag = false
			
			if upperbound( a_prcl_id[] ) > i_temp_prcl_ag_start then
				use_temp_prcl_ag = true
				
				delete from pt_temp_parcels_asmt_gp;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "error occurred during the delete process while deleting the temp parcel-assessment groups stored off from a prior run.  no changes made.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
				
				for i = 1 to maxi
					insert into pt_temp_parcels_asmt_gp ( parcel_id, assessment_group_id ) values ( :a_prcl_id[i], :a_ag_id[i] );
					
					if sqlca.sqlcode <> 0 then
						a_msg = "error occurred while inserting the temp parcel-assessment groups for which to delete values.  no changes made.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
				next
				
				pasmt_where =	"( pasmt.parcel_id, pasmt.assessment_group_id ) in " + "~r~n" + &
										"					(	select		parcel_id, assessment_group_id " + "~r~n" + &
										"						from		pt_temp_parcels_asmt_gp " + "~r~n" + &
										"					) "
				cc_where =	"			temp_pag.parcel_id = ptl.parcel_id " + "~r~n" + &
								"	and	temp_pag.assessment_group_id = agt.assessment_group_id "
			else
				pasmt_where = "( "
				
				for i = 1 to maxi
					pasmt_where = pasmt_where + "					( pasmt.parcel_id = " + string( a_prcl_id[i] ) + " and pasmt.assessment_group_id = " + string( a_ag_id[i] ) + " ) "
					if i < maxi then pasmt_where = pasmt_where + "or "
					pasmt_where = pasmt_where + "~r~n"
				next
				
				pasmt_where = pasmt_where + "				) "
				cc_where = pasmt_where
			end if

////
////	figure out if we're working on one or more prop tax companies and/or states.
////
			setnull( st_id )
			setnull( ptco_id )
			
			if use_temp_prcl_ag then
				st_sqls =		"	select		distinct prcl.state_id " + "~r~n" + &
								"	from		pt_parcel prcl, " + "~r~n" + &
								"				pt_temp_parcels_asmt_gp temp_pag " + "~r~n" + &
								"	where	prcl.parcel_id = temp_pag.parcel_id "
				
				ptco_sqls =	"	select		distinct prcl.prop_tax_company_id " + "~r~n" + &
								"	from		pt_parcel prcl, " + "~r~n" + &
								"				pt_temp_parcels_asmt_gp temp_pag " + "~r~n" + &
								"	where	prcl.parcel_id = temp_pag.parcel_id "
			else
				st_sqls =		"	select		distinct prcl.state_id " + "~r~n" + &
								"	from		pt_parcel prcl " + "~r~n" + &
								"	where	" + f_parsearrayby254( a_prcl_id[], "n", "prcl.parcel_id" ) + " "
				
				ptco_sqls =	"	select		distinct prcl.prop_tax_company_id " + "~r~n" + &
								"	from		pt_parcel prcl " + "~r~n" + &
								"	where	" + f_parsearrayby254( a_prcl_id[], "n", "prcl.parcel_id" ) + " "
			end if
			
			// state
			ds_st = create uo_ds_top
			rtn_str = f_create_dynamic_ds( ds_st, "grid", st_sqls, sqlca, true )
			
			if rtn_str <> "ok" then
				a_msg = "an internal error occurred while checking to see if the parcel assessments being deleted were all for one state.  if you continue to receive this error please contact your " + &
					"internal powerplant support or the powerplan property tax support desk.  the specific error returned was:~n~n" + rtn_str
				destroy ds_st
				return false
			end if
			
			if ds_st.rowcount() = 1 then st_id = ds_st.getitemstring( 1, 1 )
			destroy ds_st
			
			// prop tax company
			ds_ptco = create uo_ds_top
			rtn_str = f_create_dynamic_ds( ds_ptco, "grid", ptco_sqls, sqlca, true )
			
			if rtn_str <> "ok" then
				a_msg = "an internal error occurred while checking to see if the parcel assessments being deleted were all for one prop tax company.  if you continue to receive this error please " + &
					"contact your internal powerplant support or the powerplan property tax support desk.  the specific error returned was:~n~n" + rtn_str
				destroy ds_ptco
				return false
			end if
			
			if ds_ptco.rowcount() = 1 then ptco_id = ds_ptco.getitemnumber( 1, 1 )
			destroy ds_ptco

////
////	before we delete from parcel assessment we have to delete from case calc and case calc authority (as the deletes for these guys depend on parcel assessment).
////
			if not i_uo_case.of_deletecasecalc( a_case_id, cc_where, ptco_id, st_id, a_msg ) then
				a_msg = "an error occurred while deleting the assessment to asset allocation.  if you continue to receive this error please contact your internal powerplant support or the powerplan " + &
					"property tax support desk.  the specific error returned is below:~n~n" + a_msg
				return false
			end if

////
////	do the delete.
////
			sqls =		"delete from pt_parcel_assessment pasmt " + "~r~n" + &
						"	where	pasmt.case_id = " + string( a_case_id ) + " " + "~r~n" + &
						"		and	" + pasmt_where
			
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "an error occurred while deleting the selected assessments.  if you continue to receive this error please contact your internal powerplant support or the powerplan property " + &
					"tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if

////
////	delete our temp parcels - assessment groups so we don't cause any errors in other functions that might use it later on before returning.
////
			if use_temp_prcl_ag then
				delete from pt_temp_parcels_asmt_gp;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "error occurred while deleting the temp parcels stored off when deleting parcel assessments.  no changes made.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	we were successful.  clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_assessmentedit (s_pt_parcel_assessment as_asmt[],  a_case_id, boolean a_update_date, boolean a_update_assessor, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_assessmentedit()
 **	
 **	edits existing assessments.  if a passed in assessment already exists the assessment will be updated; if an assessment does not already exist it will be inserted.  this function will also
 **	update the case calc and case calc authority tables in real time.  passing multiple assessments at once is beneficial as the delete/insert into case calc and case calc authority is only
 **	performed once per call of this function.  therefore, more than one assessment to update at a given time for a given case, pass all of those assessments to this function at once for
 **	better performance.
 **	
 **	this function can only handle editing one case at a time - if you want to delete assessments for multiple cases you will need to call this function for each case you're editing.
 **	
 **	parameters	:	s_pt_parcel_assessment		:	(as_asmt[]) the assessment values for the update.  the case_id field in this structure is ignored (to ensure all assessments are for the
 **																same case).
 **							:	(a_case_id) the case for which the assessments should be edited.
 **						boolean							:	(a_update_date) whether the assessment date should be updated at the same time from the value in the as_asmt[] array.  if this parm
 **																is false any data in the as_asmt[].assessment_date field is ignored and pt_parcel_assessment.assessment_date is left untouched.
 **						boolean							:	(a_update_assessor) whether the assessor id should be updated at the same time from the value in the as_asmt[] array.  if this parm is
 **																false any data in the as_asmt[].assessor_id field is ignored and pt_parcel_assessment.assessor_id is left untouched.
 **						ref string							:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the assessments were updated successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi, rows
		assr_id
		prcl_id[], ag_id[]
	string	where_sqls
	boolean	use_temp_prcl_ag
	datetime	asmt_dt

////
////	make sure the parameters are ok.
////
			maxi = upperbound( as_asmt[] )
			
			if maxi = 0 then return true
			
			if isnull( a_case_id ) then
				a_msg = "an internal error occurred while attempting to edit assessments.  no case was specified as the case from which to update assessments.  if you continue to receive this " + &
					"error please contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if

////
////	validate update eligibility.  do this before messing with temp parcels below since the function we're calling itself uses (and clears out) temp parcels ag.
////
			maxi = upperbound( as_asmt[] )
			
			for i = 1 to maxi
				prcl_id[i] = as_asmt[i].parcel_id
				ag_id[i] = as_asmt[i].assessment_group_id
			next
			
			if not this.of_assessmentupdateeligibility( a_case_id, prcl_id[], ag_id[], false, a_msg ) then return false

////
////	see if we should use temp parcels ag.  if so, go ahead and clear it out.
////
			use_temp_prcl_ag = false
			
			if upperbound( as_asmt[] ) > i_temp_prcl_ag_start then
				use_temp_prcl_ag = true
				
				delete from pt_temp_parcels_asmt_gp;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "error occurred during the update process while deleting the temp parcel-assessment groups stored off from a prior run.  no changes made.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	loop over the assessments we're adding/updating and insert or update based on whether the row already exists or not.
////
			if not use_temp_prcl_ag then where_sqls = "( "
			
			for i = 1 to maxi
				////
				////	if the tiered rates indicator is null, make it zero.
				////
					if isnull( as_asmt[i].use_tiered_rates_yn ) then as_asmt[i].use_tiered_rates_yn = 0
					
				////
				////	do the insert or update.
				////
					setnull( rows )
					select		count('x')
					into		:rows
					from		pt_parcel_assessment pasmt
					where	pasmt.parcel_id = :as_asmt[i].parcel_id
						and	pasmt.assessment_group_id = :as_asmt[i].assessment_group_id
						and	pasmt.case_id = :a_case_id;
					
					if isnull( rows ) or rows = 0 then
						setnull( asmt_dt )
						setnull( assr_id )
						if a_update_date then asmt_dt = as_asmt[i].assessment_date
						if a_update_assessor then assr_id = as_asmt[i].assessor_id
						
						insert into pt_parcel_assessment ( parcel_id, assessment_group_id, case_id, assessment, equalized_value, taxable_value, assessment_date, assessor_id, use_tiered_rates_yn )
							values ( :as_asmt[i].parcel_id, :as_asmt[i].assessment_group_id, :a_case_id, :as_asmt[i].assessment, :as_asmt[i].equalized_value, :as_asmt[i].taxable_value, :asmt_dt, :assr_id, :as_asmt[i].use_tiered_rates_yn );
						
						if sqlca.sqlcode <> 0 then
							a_msg = "an error occurred while inserting one of the assessments.  if you continue to receive this error please contact your internal powerplant support or the powerplan " + &
								"property tax support desk.~n~nerror: " + sqlca.sqlerrtext
							return false
						end if
					else
						if a_update_date and a_update_assessor then
							update	pt_parcel_assessment pasmt
							set		pasmt.assessment = :as_asmt[i].assessment,
										pasmt.equalized_value = :as_asmt[i].equalized_value,
										pasmt.taxable_value = :as_asmt[i].taxable_value,
										pasmt.use_tiered_rates_yn = :as_asmt[i].use_tiered_rates_yn,
										pasmt.assessment_date = :as_asmt[i].assessment_date,
										pasmt.assessor_id = :as_asmt[i].assessor_id
							where	pasmt.parcel_id = :as_asmt[i].parcel_id
								and	pasmt.assessment_group_id = :as_asmt[i].assessment_group_id
								and	pasmt.case_id = :a_case_id;
						elseif a_update_date then
							update	pt_parcel_assessment pasmt
							set		pasmt.assessment = :as_asmt[i].assessment,
										pasmt.equalized_value = :as_asmt[i].equalized_value,
										pasmt.taxable_value = :as_asmt[i].taxable_value,
										pasmt.use_tiered_rates_yn = :as_asmt[i].use_tiered_rates_yn,
										pasmt.assessment_date = :as_asmt[i].assessment_date
							where	pasmt.parcel_id = :as_asmt[i].parcel_id
								and	pasmt.assessment_group_id = :as_asmt[i].assessment_group_id
								and	pasmt.case_id = :a_case_id;
						elseif a_update_assessor then
							update	pt_parcel_assessment pasmt
							set		pasmt.assessment = :as_asmt[i].assessment,
										pasmt.equalized_value = :as_asmt[i].equalized_value,
										pasmt.taxable_value = :as_asmt[i].taxable_value,
										pasmt.use_tiered_rates_yn = :as_asmt[i].use_tiered_rates_yn,
										pasmt.assessor_id = :as_asmt[i].assessor_id
							where	pasmt.parcel_id = :as_asmt[i].parcel_id
								and	pasmt.assessment_group_id = :as_asmt[i].assessment_group_id
								and	pasmt.case_id = :a_case_id;
						else
							update	pt_parcel_assessment pasmt
							set		pasmt.assessment = :as_asmt[i].assessment,
										pasmt.equalized_value = :as_asmt[i].equalized_value,
										pasmt.taxable_value = :as_asmt[i].taxable_value,
										pasmt.use_tiered_rates_yn = :as_asmt[i].use_tiered_rates_yn
							where	pasmt.parcel_id = :as_asmt[i].parcel_id
								and	pasmt.assessment_group_id = :as_asmt[i].assessment_group_id
								and	pasmt.case_id = :a_case_id;
						end if
						
						if sqlca.sqlcode <> 0 then
							a_msg = "an error occurred while updating one of the assessments.  if you continue to receive this error please contact your internal powerplant support or the powerplan " + &
								"property tax support desk.~n~nerror: " + sqlca.sqlerrtext
							return false
						end if
					end if
				
				////
				////	build the where clause for updating the assessment allocation if we're not using the temp parcel ag table.  if we are using the table, insert.
				////
					if use_temp_prcl_ag then
						insert into pt_temp_parcels_asmt_gp ( parcel_id, assessment_group_id ) values ( :as_asmt[i].parcel_id, :as_asmt[i].assessment_group_id );
						
						if sqlca.sqlcode <> 0 then
							a_msg = "error occurred while inserting the temp parcel-assessment groups for which to update values.  no changes made.~n~nerror: " + sqlca.sqlerrtext
							return false
						end if
					else
						where_sqls = where_sqls + "					( pasmt.parcel_id = " + string( as_asmt[i].parcel_id ) + " and pasmt.assessment_group_id = " + string( as_asmt[i].assessment_group_id ) + " ) "
						if i < maxi then where_sqls = where_sqls + "or "
						where_sqls = where_sqls + "~r~n"
					end if
			next
			
			if use_temp_prcl_ag then
				where_sqls =	"			temp_pag.parcel_id = ptl.parcel_id " + "~r~n" + &
									"	and	temp_pag.assessment_group_id = agt.assessment_group_id "
			else
				where_sqls = where_sqls + "				) "
			end if

////
////	update case calc and case calc authority for the changed assessments.
////
			if not i_uo_case.of_populatecasecalc( a_case_id, where_sqls, a_msg ) then
				a_msg = "an error occurred while generating the assessment to asset allocation.  if you continue to receive this error please contact your internal powerplant support or the " + &
					"powerplan property tax support desk.  the specific error returned is below:~n~n" + a_msg
				return false
			end if

////
////	delete our temp parcels - assessment groups so we don't cause any errors in other functions that might use it later on before returning.
////
			if use_temp_prcl_ag then
				delete from pt_temp_parcels_asmt_gp;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "error occurred while deleting the temp parcels stored off when updating parcel assessments.  no changes made.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	we were successful.  clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_assessmentcopy ( a_prcl_id[], s_ptc_assessments_copy_trending_options as_ag[],  a_from_case_id,  a_to_case_id, boolean a_overwrite_yn, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_assessmentcopy()
 **	
 **	copies assessments for given parcels and assessment groups from one case to another.  when copying, assessments can be "trended" (i.e., increased or decreased by a given percent).
 **	if no trend percent is specified, assessments will be copied as is.  this function copies the assessments column and then calculates and populated the equalized value and taxable value
 **	columns based on the data setup for this case.
 **	
 **	parameters	:		:	(a_prcl_id[]) the parcels for which we're copying assessments.
 **						s_ptc_assessments_copy_trending_options		:	(as_ag[]) a structure holding the assessment groups we're copying  with the amount to inflate/deflate the values
 **																						when copying.  (for the trend percent field in the structure this should be the percentage in decimal terms of the amount
 **																						to increase/decrease the factor when copying.  for instance a 30% increase would be noted by passing .3 whereas a 0%
 **																						increase - i.e., straight copy - would be noted by passing 0.)
 **							:	(a_from_case_id) the case we're copying the assessment group setup from.
 **							:	(a_to_case_id) the case we're copying the assessment group setup to.
 **						boolean													:	(a_overwrite_yn) whether any existing assessments in the to case should be overwritten (true) or not (false).
 **						ref string													:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the assessments were copied successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi, j, maxj, k
		ag_id[], del_prcl_id[], del_ag_id[]
	string	sqls, asmt_decode
	boolean	use_temp_prcl

////
////	don't error.
////
			if upperbound( a_prcl_id[] ) = 0 then
				a_msg = "no parcels specified to copy assessments for.  if you continue to receive this error contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if
			
			if isnull( a_from_case_id ) then
				a_msg = "missing case to copy from.  if you continue to receive this error contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if
			
			if isnull( a_to_case_id ) then
				a_msg = "missing case to copy to.  if you continue to receive this error contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if
			
			if upperbound( as_ag[] ) = 0 then
				a_msg = "no assessment groups specified to copy assessments for.  if you continue to receive this error contact your internal powerplant support or the powerplan property tax " + &
					"support desk."
				return false
			end if

////
////	build the decode statement for trending the assessment.  and, build an array of assessment groups for limiting by 254 in the sql below.
////
			maxi = upperbound( as_ag[] )
			asmt_decode = "decode( pasmt.assessment_group_id, "
			
			for i = 1 to maxi
				if i > 1 then asmt_decode = asmt_decode + ", "
				if isnull( as_ag[i].trend_percent ) then as_ag[i].trend_percent = 0
				asmt_decode = asmt_decode + string( as_ag[i].assessment_group_id ) + ", " + string( 1 + as_ag[i].trend_percent )
				ag_id[i] = as_ag[i].assessment_group_id
			next
			
			asmt_decode = asmt_decode + " )"

////
////	validate update eligibility.  populate temp parcels ag before doing so as the function uses temp parcels ag to know what we plan on updating.
////
			delete from pt_temp_parcels_asmt_gp;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while deleting prior parcel / assessment group combinations stored off prior to validation (during the copy).  if you continue to receive this " + &
					"error contact your internal powerplant support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if
			
			sqls =		"	insert into pt_temp_parcels_asmt_gp ( parcel_id, assessment_group_id ) " + "~r~n" + &
						"		select		pasmt.parcel_id, pasmt.assessment_group_id "+  "~r~n" + &
						"		from		pt_parcel_assessment pasmt " + "~r~n" + &
						"		where	pasmt.case_id = " + string( a_from_case_id ) + " " + "~r~n" + &
						"			and	" + f_parsearrayby254( a_prcl_id[], "n", "pasmt.parcel_id" ) + " " + "~r~n" + &
						"			and	" + f_parsearrayby254( ag_id[], "n", "pasmt.assessment_group_id" ) + " " + "~r~n"
			
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while storing off parcel / assessment group combinations for validation (during the copy).  if you continue to receive this error contact your " + &
					"internal powerplant support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				
				delete from pt_temp_parcels_asmt_gp;
				
				return false
			end if
			
			if not this.of_assessmentupdateeligibility( a_to_case_id, true, a_msg ) then return false
			
			delete from pt_temp_parcels_asmt_gp;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while deleting parcel / assessment group combinations stored for validation (during the copy).  if you continue to receive this error contact " + &
					"your internal powerplant support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if

////
////	if we're overwriting then we need to delete the existing parcel / assessment group combinations for everything that we're copying.  build the arrays for deleting here, then call the delete
////	function in this user object to do the delete for us.
////
			if a_overwrite_yn then
				maxi = upperbound( a_prcl_id[] )
				maxj = upperbound( ag_id[] )
				k = 1
				
				for i = 1 to maxi
					for j = 1 to maxj
						del_prcl_id[k] = a_prcl_id[i]
						del_ag_id[k] = ag_id[j]
						k++
					next
				next
				
				if not this.of_assessmentdelete( del_prcl_id[], del_ag_id[], a_to_case_id, a_msg ) then return false
			end if

////
////	see if we need to use temp parcel or not to store off the parcels we're copying.
////
			maxi = upperbound( a_prcl_id[] )
			use_temp_prcl = false
			
			if maxi > i_temp_prcl_start then
				use_temp_prcl = true
				
				delete from pt_temp_parcels;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "error occurred during the copy process while deleting the temp parcels stored off from a prior run.  no changes made.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
				
				for i = 1 to maxi
					insert into pt_temp_parcels ( parcel_id ) values ( :a_prcl_id[i] );
					
					if sqlca.sqlcode <> 0 then
						a_msg = "error occurred while inserting the temp parcels to copy.  no changes made.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
				next
			end if

////
////	build the sql to do the copy and do it.
////
			sqls =					"	insert into pt_parcel_assessment ( parcel_id, assessment_group_id, case_id, assessment, use_tiered_rates_yn ) " + "~r~n" + &
									"		select		pasmt.parcel_id parcel_id, " + "~r~n" + &
									"					pasmt.assessment_group_id assessment_group_id, " + "~r~n" + &
									"					" + string( a_to_case_id ) + " case_id, " + "~r~n" + &
									"					pasmt.assessment * " + asmt_decode + ", " + "~r~n" + &
									"					pasmt.use_tiered_rates_yn use_tiered_rates_yn " + "~r~n" + &
									"		 " + "~r~n"
			
			if use_temp_prcl then
				sqls = sqls +	"		from		pt_parcel_assessment pasmt, " + "~r~n" + &
									"					pt_temp_parcels temp_prcl " + "~r~n"
			else
				sqls = sqls +	"		from		pt_parcel_assessment pasmt " + "~r~n"
			end if
			
			sqls = sqls +		"		 " + "~r~n" + &
									"		where	pasmt.case_id = " + string( a_from_case_id ) + " " + "~r~n"
			
			if use_temp_prcl then
				sqls = sqls +	"			and	pasmt.parcel_id = temp_prcl.parcel_id " + "~r~n"
			else
				sqls = sqls +	"			and	" + f_parsearrayby254( a_prcl_id[], "n", "pasmt.parcel_id" ) + " " + "~r~n"
			end if
			
			sqls = sqls +		"			and	" + f_parsearrayby254( ag_id[], "n", "pasmt.assessment_group_id" ) + " " + "~r~n" + &
									"			and	not exists " + "~r~n" + &
									"						(	select		'x' " + "~r~n" + &
									"							from		pt_parcel_assessment exists_pasmt " + "~r~n" + &
									"							where	pasmt.parcel_id = exists_pasmt.parcel_id " + "~r~n" + &
									"								and	pasmt.assessment_group_id = exists_pasmt.assessment_group_id " + "~r~n" + &
									"								and	exists_pasmt.case_id = " + string( a_to_case_id ) + " " + "~r~n" + &
									"						) "
			
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while copying the assessments for the selected parcels and assessment groups from one case to another.  if you continue to receive this " + &
					"error contact your internal powerplant support or the powerplan property tax support desk.  the specified error returned was: " + sqlca.sqlerrtext
				return false
			end if

////
////	if we used temp parcel, clean it up.
////
			if use_temp_prcl then
				delete from pt_temp_parcels;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "error occurred while deleting the temp parcels stored off when copying.  no changes made.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	now we need to update the equalized value and taxable value for these guys.  call our function in this user object to do that work for us.  this function will also take care of populating the
////	case calc and case calc authority tables for us so we don't have to do it here.
////
			if not this.of_assessmentcalcvalues( a_prcl_id[], ag_id[], a_to_case_id, this.i_fixed_field_assessment, a_msg ) then return false

////
////	we were successful.  clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_assessmentcalcvalues ( a_prcl_id[],  a_ag_id[],  a_case_id,  a_fixed_field, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_assessmentcalcvalues()
 **	
 **	calculates two of the three "value" fields on the assessment table (i.e., assessment, equalized value, taxable_value) for given parcels and assessment groups.  one of the "value" fields is
 **	identified as the "fixed" value and the other two values are calculated from it.  at the end, this function also updates the case calc and case calc authority tables for the changes.
 **	
 **	parameters	:		:	(a_prcl_id[]) the parcels for which we're calculating assessment values.
 **							:	(a_ag_id[]) the assessment groups for which we're calculating assessment values.
 **							:	(a_case_id) the case for which we're calculating assessment values.
 **							:	(a_fixed_field) a constant identifying the field that is know - i.e., the field that all other fields will be calculated from.  the constants can be found as
 **											instance variables for this object starting with i_fixed_field_...
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the assessment values were calculated successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi
	string	sqls
	boolean	use_temp_prcl

////
////	build the string to limit the parcels / assessment groups we're recalcing.  if we're above the number of parcels in the array, use the temp parcel table.  then, call the function that takes in a
////	where clause to do the re-calc.
////
			maxi = upperbound( a_prcl_id[] )
			use_temp_prcl = false
			
			if maxi > i_temp_prcl_start then
				use_temp_prcl = true
				
				delete from pt_temp_parcels;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "error occurred during the recalc process while deleting the temp parcels stored off from a prior run.  no changes made.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
				
				for i = 1 to maxi
					insert into pt_temp_parcels ( parcel_id ) values ( :a_prcl_id[i] );
					
					if sqlca.sqlcode <> 0 then
						a_msg = "error occurred while inserting the temp parcels for which to calculate values.  no changes made.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
				next
				
				sqls =		"			pasmt.parcel_id = temp_prcl.parcel_id " + "~r~n" + &
							"	and	" + f_parsearrayby254( a_ag_id[], "n", "pasmt.assessment_group_id" )
			else
				sqls =		"			" + f_parsearrayby254( a_prcl_id[], "n", "pasmt.parcel_id" ) + " " + "~r~n" + &
							"	and	" + f_parsearrayby254( a_ag_id[], "n", "pasmt.assessment_group_id" )
			end if
			
			if not this.of_assessmentcalcvalues( a_case_id, sqls, a_fixed_field, true, a_msg ) then return false

////
////	delete our temp parcels so we don't cause any errors in other functions that might use it later on before returning.
////
			if use_temp_prcl then
				delete from pt_temp_parcels;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "error occurred while deleting the temp parcels stored off when calculating the values.  no changes made.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
			end if
			
			a_msg = ""
			return true
end function

public function boolean of_parcelvalidate (s_pt_parcel as_prcl, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelvalidate()
 **	
 **	validates all of the parcel fields for a parcel being added/updated.  call this version of the function if you're parcel is not one-to-one with tax districts.  if your parcel is one-to-one then you
 **	need to call this version of the function with the district parameter.  if validation fails an error message will be returned that can be displayed to the user and describes the specific validation
 **	problem.
 **	
 **	parameters	:	s_pt_parcel		:	(as_prcl) the parcel details for the parcel to validate.
 **						ref string			:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel passed validation.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
	s_pt_district s_dist

////
////	set the district structure to indicate that this parcel is not one-to-one with tax districts and call the more detailed version of this function to do the validation.
////
			s_dist.is_parcel_one_to_one = 0
			return this.of_parcelvalidate( as_prcl, s_dist, a_msg )
end function

public function boolean of_parcelvalidate (s_pt_parcel as_prcl, s_pt_district as_dist, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelvalidate()
 **	
 **	validates all of the parcel fields for a parcel being added/updated.  call this version of the function if you're parcel is one-to-one with tax districts.  if your parcel is not one-to-one then you 
 **	can call either this version of the function or the one with less parameters.  this function will also validate tax district details if the parcel is one-to-one with tax districts.  if validation fails
 **	an error message will be returned that can be displayed to the user and describes the specific validation problem.
 **	
 **	parameters	:	s_pt_parcel		:	(as_prcl) the parcel details for the parcel to validate.
 **						s_pt_district		:	(as_dist) the district details for the parcel to validate.  at a minimum the is_parcel_one_to_one field must be populated.  if that field is set to 0 then the
 **												rest of this structure is ignored.  if the field is set to 1, this structure (i.e., tax district) is validated as well.
 **						ref string			:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel passed validation.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the of_parcelvalidatefromimport() function as both functions are used to validate parcels being added.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi, rows
		ptype_id, ff_id, use_id, validate_yn
	string	sysval, sqls, rtn_str
	string	st_id, cnty_id, ff_label
	boolean	req_error, isnull_ffval

/*
 *	misc objects.
 */
	uo_ds_top ds_ff

////
////	check that all of the required information is filled in.
////
			////
			////	description
			////
				if isnull( as_prcl.description ) or trim( as_prcl.description ) = "" then
					a_msg = "'description' is a required field.  please enter a description."
					return false
				end if
			
			////
			////	parcel number
			////
				if isnull( as_prcl.parcel_number ) or trim( as_prcl.parcel_number ) = "" then
					a_msg = "'parcel number' is a required field.  please enter a parcel number."
					return false
				end if
			
			////
			////	parcel type id
			////
				if isnull( as_prcl.parcel_type_id ) then
					a_msg = "'parcel type' is a required field.  please select a parcel type."
					return false
				end if
			
			////
			////	prop tax company id
			////
				if isnull( as_prcl.prop_tax_company_id ) then
					a_msg = "'prop tax company' is a required field.  please select a property tax company."
					return false
				end if
			
			////
			////	state id
			////
				if isnull( as_prcl.state_id ) or trim( as_prcl.state_id ) = "" then
					a_msg = "'state' is a required field.  please select a state."
					return false
				end if
			
			////
			////	tax district id
			////
				if as_dist.is_parcel_one_to_one = 1 and isnull( as_prcl.parcel_id ) then
					// don't check it because one will automatically be added
				else
					if isnull( as_prcl.tax_district_id ) then
						a_msg = "'tax district' is a required field.  please select a property tax district."
						return false
					end if
				end if

////
////	make sure this isn't a duplicate parcel number.  first, we'll need to find from the system controls what creates a unique parcel.  (if the parcel number is not null then we'll assume that the
////	parcel has already been added - if the parcel number is null then we'll assume that this is a new parcel that hasn't been added, yet.)
////
			sysval = upper( trim( i_uo_systemoptions.of_getsystemoption( "parcels - parcel uniqueness columns" ) ) )
			setnull( rows )
			
			choose case sysval
				case	"parcel number"
					if isnull( as_prcl.parcel_id ) then
						select		count('x')
						into		:rows
						from		pt_parcel prcl
						where	upper( trim( prcl.parcel_number ) ) = upper( trim( :as_prcl.parcel_number ) );
					else
						select		count('x')
						into		:rows
						from		pt_parcel prcl
						where	upper( trim( prcl.parcel_number ) ) = upper( trim( :as_prcl.parcel_number ) )
							and	prcl.parcel_id <> :as_prcl.parcel_id;
					end if
					
					if not isnull( rows ) and rows > 0 then
						a_msg = "this parcel number already exists in the database.  please enter a unique parcel number or update the existing parcel in the database with this parcel number.~n~n" + &
							"(there is a ptc system option which is currently set to force parcel numbers to be unique.  if appropriate you can change the selected value for this system option to " + &
							"accomodate your data.)"
						return false
					end if
				case	"parcel number, state"
					if isnull( as_prcl.parcel_id ) then
						select		count('x')
						into		:rows
						from		pt_parcel prcl
						where	upper( trim( prcl.parcel_number ) ) = upper( trim( :as_prcl.parcel_number ) )
							and	prcl.state_id = :as_prcl.state_id;
					else
						select		count('x')
						into		:rows
						from		pt_parcel prcl
						where	upper( trim( prcl.parcel_number ) ) = upper( trim( :as_prcl.parcel_number ) )
							and	prcl.state_id = :as_prcl.state_id
							and	prcl.parcel_id <> :as_prcl.parcel_id;
					end if
					
					if not isnull( rows ) and rows > 0 then
						a_msg = "this parcel number already exists in the database for this state.  please enter a unique parcel number for this state or update the existing parcel in the database with " + &
							"this parcel number for the selected state.~n~n(there is a ptc system option which is currently set to force parcel numbers to be unique within a state.  if appropriate you " + &
							"can change the selected value for this system option to accomodate your data.)"
						return false
					end if
				case	"parcel number, state, county"
					if as_dist.is_parcel_one_to_one = 1 then
						cnty_id = as_dist.county_id
					else
						select		td.county_id
						into		:cnty_id
						from		prop_tax_district td
						where	td.tax_district_id = :as_prcl.tax_district_id;
					end if
					
					if isnull( as_prcl.parcel_id ) then
						select		count('x')
						into		:rows
						from		pt_parcel prcl,
									prop_tax_district td
						where	upper( trim( prcl.parcel_number ) ) = upper( trim( :as_prcl.parcel_number ) )
							and	prcl.state_id = :as_prcl.state_id
							and	prcl.tax_district_id = td.tax_district_id
							and	td.county_id = :cnty_id;
					else
						select		count('x')
						into		:rows
						from		pt_parcel prcl,
									prop_tax_district td
						where	upper( trim( prcl.parcel_number ) ) = upper( trim( :as_prcl.parcel_number ) )
							and	prcl.state_id = :as_prcl.state_id
							and	prcl.tax_district_id = td.tax_district_id
							and	td.county_id = :cnty_id
							and	prcl.parcel_id <> :as_prcl.parcel_id;
					end if
					
					if not isnull( rows ) and rows > 0 then
						a_msg = "this parcel number already exists in the database for this state and county.  please enter a unique parcel number for this state and county or update the existing " + &
							"parcel in the database with this parcel number for the selected state and county.~n~n(there is a ptc system option which is currently set to force parcel numbers to be " + &
							"unique within a state and county.  if appropriate you can change the selected value for this system option to accomodate your data.)"
						return false
					end if
				case	"parcel number, state, prop tax company"
					if isnull( as_prcl.parcel_id ) then
						select		count('x')
						into		:rows
						from		pt_parcel prcl
						where	upper( trim( prcl.parcel_number ) ) = upper( trim( :as_prcl.parcel_number ) )
							and	prcl.state_id = :as_prcl.state_id
							and	prcl.prop_tax_company_id = :as_prcl.prop_tax_company_id;
					else
						select		count('x')
						into		:rows
						from		pt_parcel prcl
						where	upper( trim( prcl.parcel_number ) ) = upper( trim( :as_prcl.parcel_number ) )
							and	prcl.state_id = :as_prcl.state_id
							and	prcl.prop_tax_company_id = :as_prcl.prop_tax_company_id
							and	prcl.parcel_id <> :as_prcl.parcel_id;
					end if
					
					if not isnull( rows ) and rows > 0 then
						a_msg = "this parcel number already exists in the database for this state and property tax company.  please enter a unique parcel number for this state and property tax " + &
							"company or update the existing parcel in the database with this parcel number for the selected state and property tax company.~n~n(there is a ptc system option which " + &
							"is currently set to force parcel numbers to be unique for a state and property tax company.  if appropriate you can change the selected value for this system option to " + &
							"accomodate your data.)"
						return false
					end if
				case	"parcel number, state, county, prop tax company"
					if as_dist.is_parcel_one_to_one = 1 then
						cnty_id = as_dist.county_id
					else
						select		td.county_id
						into		:cnty_id
						from		prop_tax_district td
						where	td.tax_district_id = :as_prcl.tax_district_id;
					end if
					
					if isnull( as_prcl.parcel_id ) then
						select		count('x')
						into		:rows
						from		pt_parcel prcl,
									prop_tax_district td
						where	upper( trim( prcl.parcel_number ) ) = upper( trim( :as_prcl.parcel_number ) )
							and	prcl.state_id = :as_prcl.state_id
							and	prcl.prop_tax_company_id = :as_prcl.prop_tax_company_id
							and	prcl.tax_district_id = td.tax_district_id
							and	td.county_id = :cnty_id;
					else
						select		count('x')
						into		:rows
						from		pt_parcel prcl,
									prop_tax_district td
						where	upper( trim( prcl.parcel_number ) ) = upper( trim( :as_prcl.parcel_number ) )
							and	prcl.state_id = :as_prcl.state_id
							and	prcl.prop_tax_company_id = :as_prcl.prop_tax_company_id
							and	prcl.tax_district_id = td.tax_district_id
							and	td.county_id = :cnty_id
							and	prcl.parcel_id <> :as_prcl.parcel_id;
					end if
					
					if not isnull( rows ) and rows > 0 then
						a_msg = "this parcel number already exists in the database for this state, county and property tax company.  please enter a unique parcel number for this state, county and " + &
							"property tax company or update the existing parcel in the database with this parcel number for the selected state, county and property tax company.~n~n(there is a ptc " + &
							"system option which is currently set to force parcel numbers to be unique for a state, county and property tax company.  if appropriate you can change the selected value " + &
							"for this system option to accomodate your data.)"
						return false
					end if
				case	else
					a_msg = "unable to determine whether this is a unique parcel.  please check the system options for the property tax center (admin center, system options) to ensure that a " + &
						"system control named 'parcels - parcel uniqueness columns' is available and that values are set for that option.  if the system control is not present or there is another " + &
						"problem with the system option contact the powerplan property tax support desk for assistance."
					return false
			end choose

////
////	make sure this parcel isn't a -100 parcel type (either in the database or in the structure) as those are special and can only be changed by special processing.
////
			if not isnull( as_prcl.parcel_id ) then
				select		parcel_type_id
				into		:ptype_id
				from		pt_parcel
				where	parcel_id = :as_prcl.parcel_id;
				
				if ptype_id = i_reserved_ptype_allocated then
					a_msg = "the parcel being changed is a special system-only parcel used to track 'allocated' locations and cannot be modified."
					return false
				end if
			end if
			
			if as_prcl.parcel_type_id = i_reserved_ptype_allocated then
				a_msg = "the 'allocated property' parcel type is reserved for system-only special processing.  no user-created parcels may be assigned to this parcel type.  please select a different " + &
					"parcel type for the parcel."
				return false
			end if

////
////	make sure the legal description isn't bigger than 16000 characters.
////
			if len( as_prcl.legal_description ) > 16000 then
				a_msg = "the legal description has a maximum limit of 16,000 characters.  this legal description exceeds that limit.  please decrease the number of characters to 16,000 or less."
				return false
			end if

////
////	validate the flex fields for this parcel (based on the flex field set up for this parcel type).  get all of the flex fields used for this parcel type and loop over them to validate.
////
			sqls =		"	select		ffc.flex_field_id flex_field_id, " + "~r~n" + &
						"				ffc.flex_field_label flex_field_label, " + "~r~n" + &
						"				ffc.flex_usage_id flex_usage_id, " + "~r~n" + &
						"				ffc.validate_value validate_value " + "~r~n" + &
						"	from		pt_parcel_flex_field_control ffc " + "~r~n" + &
						"	where	ffc.parcel_type_id = " + string( as_prcl.parcel_type_id ) + " " + "~r~n" + &
						"		and	ffc.flex_usage_id in ( 2, 3 ) "
			
			ds_ff = create uo_ds_top
			rtn_str = f_create_dynamic_ds( ds_ff, "grid", sqls, sqlca, true )
			
			if rtn_str <> "ok" then
				a_msg = "an error occurred while retrieving the data used to validate the parcel's flex fields.  if you continue to receive this error contact your internal powerplant support or the " + &
					"powerplan property tax support desk."
					destroy ds_ff
				return false
			end if
			
			maxi = ds_ff.rowcount()
			req_error = false
			
			for i = 1 to maxi
				////
				////	get the information for this flex field.
				////
					ff_id = ds_ff.getitemnumber( i, 1 )
					use_id = ds_ff.getitemnumber( i, 3 )
					validate_yn = ds_ff.getitemnumber( i, 4 )
					if isnull( validate_yn ) then validate_yn = 0
					isnull_ffval = false
				
				////
				////	check for anything that is required but is not populated.  also store off if this guy is null.
				////
					choose case ff_id
						case	1
							if isnull( as_prcl.flex_1 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_1 ) then req_error = true
						case	2
							if isnull( as_prcl.flex_2 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_2 ) then req_error = true
						case	3
							if isnull( as_prcl.flex_3 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_3 ) then req_error = true
						case	4
							if isnull( as_prcl.flex_4 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_4 ) then req_error = true
						case	5
							if isnull( as_prcl.flex_5 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_5 ) then req_error = true
						case	6
							if isnull( as_prcl.flex_6 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_6 ) then req_error = true
						case	7
							if isnull( as_prcl.flex_7 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_7 ) then req_error = true
						case	8
							if isnull( as_prcl.flex_8 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_8 ) then req_error = true
						case	9
							if isnull( as_prcl.flex_9 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_9 ) then req_error = true
						case	10
							if isnull( as_prcl.flex_10 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_10 ) then req_error = true
						case	11
							if isnull( as_prcl.flex_11 ) or trim( as_prcl.flex_11 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_11 ) or trim( as_prcl.flex_11 ) = "" ) then req_error = true
						case	12
							if isnull( as_prcl.flex_12 ) or trim( as_prcl.flex_12 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_12 ) or trim( as_prcl.flex_12 ) = "" ) then req_error = true
						case	13
							if isnull( as_prcl.flex_13 ) or trim( as_prcl.flex_13 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_13 ) or trim( as_prcl.flex_13 ) = "" ) then req_error = true
						case	14
							if isnull( as_prcl.flex_14 ) or trim( as_prcl.flex_14 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_14 ) or trim( as_prcl.flex_14 ) = "" ) then req_error = true
						case	15
							if isnull( as_prcl.flex_15 ) or trim( as_prcl.flex_15 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_15 ) or trim( as_prcl.flex_15 ) = "" ) then req_error = true
						case	16
							if isnull( as_prcl.flex_16 ) or trim( as_prcl.flex_16 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_16 ) or trim( as_prcl.flex_16 ) = "" ) then req_error = true
						case	17
							if isnull( as_prcl.flex_17 ) or trim( as_prcl.flex_17 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_17 ) or trim( as_prcl.flex_17 ) = "" ) then req_error = true
						case	18
							if isnull( as_prcl.flex_18 ) or trim( as_prcl.flex_18 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_18 ) or trim( as_prcl.flex_18 ) = "" ) then req_error = true
						case	19
							if isnull( as_prcl.flex_19 ) or trim( as_prcl.flex_19 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_19 ) or trim( as_prcl.flex_19 ) = "" ) then req_error = true
						case	20
							if isnull( as_prcl.flex_20 ) or trim( as_prcl.flex_20 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_20 ) or trim( as_prcl.flex_20 ) = "" ) then req_error = true
						case	21
							if isnull( as_prcl.flex_21 ) or trim( as_prcl.flex_21 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_21 ) or trim( as_prcl.flex_21 ) = "" ) then req_error = true
						case	22
							if isnull( as_prcl.flex_22 ) or trim( as_prcl.flex_22 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_22 ) or trim( as_prcl.flex_22 ) = "" ) then req_error = true
						case	23
							if isnull( as_prcl.flex_23 ) or trim( as_prcl.flex_23 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_23 ) or trim( as_prcl.flex_23 ) = "" ) then req_error = true
						case	24
							if isnull( as_prcl.flex_24 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_24 ) then req_error = true
						case	25
							if isnull( as_prcl.flex_25 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_25 ) then req_error = true
						case	26
							if isnull( as_prcl.flex_26 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_26 ) then req_error = true
						case	27
							if isnull( as_prcl.flex_27 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_27 ) then req_error = true
						case	28
							if isnull( as_prcl.flex_28 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_28 ) then req_error = true
						case	29
							if isnull( as_prcl.flex_29 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_29 ) then req_error = true
						case	30
							if isnull( as_prcl.flex_30 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_30 ) then req_error = true
						case	31
							if isnull( as_prcl.flex_31 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_31 ) then req_error = true
						case	32
							if isnull( as_prcl.flex_32 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_32 ) then req_error = true
						case	33
							if isnull( as_prcl.flex_33 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_33 ) then req_error = true
						case	34
							if isnull( as_prcl.flex_34 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_34 ) then req_error = true
						case	35
							if isnull( as_prcl.flex_35 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_35 ) then req_error = true
						case	36
							if isnull( as_prcl.flex_36 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_36 ) then req_error = true
						case	37
							if isnull( as_prcl.flex_37 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_37 ) then req_error = true
						case	38
							if isnull( as_prcl.flex_38 ) then isnull_ffval = true
							if use_id = i_flex_usage_required and isnull( as_prcl.flex_38 ) then req_error = true
						case	39
							if isnull( as_prcl.flex_39 ) or trim( as_prcl.flex_39 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_39 ) or trim( as_prcl.flex_39 ) = "" ) then req_error = true
						case	40
							if isnull( as_prcl.flex_40 ) or trim( as_prcl.flex_40 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_40 ) or trim( as_prcl.flex_40 ) = "" ) then req_error = true
						case	41
							if isnull( as_prcl.flex_41 ) or trim( as_prcl.flex_41 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_41 ) or trim( as_prcl.flex_41 ) = "" ) then req_error = true
						case	42
							if isnull( as_prcl.flex_42 ) or trim( as_prcl.flex_42 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_42 ) or trim( as_prcl.flex_42 ) = "" ) then req_error = true
						case	43
							if isnull( as_prcl.flex_43 ) or trim( as_prcl.flex_43 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_43 ) or trim( as_prcl.flex_43 ) = "" ) then req_error = true
						case	44
							if isnull( as_prcl.flex_44 ) or trim( as_prcl.flex_44 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_44 ) or trim( as_prcl.flex_44 ) = "" ) then req_error = true
						case	45
							if isnull( as_prcl.flex_45 ) or trim( as_prcl.flex_45 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_45 ) or trim( as_prcl.flex_45 ) = "" ) then req_error = true
						case	46
							if isnull( as_prcl.flex_46 ) or trim( as_prcl.flex_46 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_46 ) or trim( as_prcl.flex_46 ) = "" ) then req_error = true
						case	47
							if isnull( as_prcl.flex_47 ) or trim( as_prcl.flex_47 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_47 ) or trim( as_prcl.flex_47 ) = "" ) then req_error = true
						case	48
							if isnull( as_prcl.flex_48 ) or trim( as_prcl.flex_48 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_48 ) or trim( as_prcl.flex_48 ) = "" ) then req_error = true
						case	49
							if isnull( as_prcl.flex_49 ) or trim( as_prcl.flex_49 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_49 ) or trim( as_prcl.flex_49 ) = "" ) then req_error = true
						case	50
							if isnull( as_prcl.flex_50 ) or trim( as_prcl.flex_50 ) = "" then isnull_ffval = true
							if use_id = i_flex_usage_required and ( isnull( as_prcl.flex_50 ) or trim( as_prcl.flex_50 ) = "" ) then req_error = true
					end choose
					
					if req_error then
						ff_label = ds_ff.getitemstring( i, 2 )
						if isnull( ff_label ) or trim( ff_label ) = "" then ff_label = "flex " + string( ff_id )
						a_msg = "'" + ff_label + "' is a required field.  please enter a value for this field."
						destroy ds_ff
						return false
					end if
				
				////
				////	validate values if we need to.
				////
					if validate_yn = 1 and not isnull_ffval then
						select		count('x')
						into		:rows
						from		pt_parcel_flex_field_values ffv
						where	ffv.flex_field_id = :ff_id
							and	ffv.parcel_type_id = :as_prcl.parcel_type_id
							and	decode( ffv.flex_field_id,
												1, to_char( :as_prcl.flex_1 ),
												2, to_char( :as_prcl.flex_2 ),
												3, to_char( :as_prcl.flex_3 ),
												4, to_char( :as_prcl.flex_4 ),
												5, to_char( :as_prcl.flex_5 ),
												6, to_char( :as_prcl.flex_6, 'mm/dd/yyyy' ),
												7, to_char( :as_prcl.flex_7, 'mm/dd/yyyy' ),
												8, to_char( :as_prcl.flex_8, 'mm/dd/yyyy' ),
												9, to_char( :as_prcl.flex_9, 'mm/dd/yyyy' ),
												10, to_char( :as_prcl.flex_10, 'mm/dd/yyyy' ),
												11, trim( to_char( :as_prcl.flex_11 ) ),
												12, trim( to_char( :as_prcl.flex_12 ) ),
												13, trim( to_char( :as_prcl.flex_13 ) ),
												14, trim( to_char( :as_prcl.flex_14 ) ),
												15, trim( to_char( :as_prcl.flex_15 ) ),
												16, trim( to_char( :as_prcl.flex_16 ) ),
												17, trim( to_char( :as_prcl.flex_17 ) ),
												18, trim( to_char( :as_prcl.flex_18 ) ),
												19, trim( to_char( :as_prcl.flex_19 ) ),
												20, trim( to_char( :as_prcl.flex_20 ) ),
												21, trim( to_char( :as_prcl.flex_21 ) ),
												22, trim( to_char( :as_prcl.flex_22 ) ),
												23, trim( to_char( :as_prcl.flex_23 ) ),
												24, to_char( :as_prcl.flex_24 ),
												25, to_char( :as_prcl.flex_25 ),
												26, to_char( :as_prcl.flex_26 ),
												27, to_char( :as_prcl.flex_27 ),
												28, to_char( :as_prcl.flex_28 ),
												29, to_char( :as_prcl.flex_29 ),
												30, to_char( :as_prcl.flex_30 ),
												31, to_char( :as_prcl.flex_31 ),
												32, to_char( :as_prcl.flex_32 ),
												33, to_char( :as_prcl.flex_33 ),
												34, to_char( :as_prcl.flex_34, 'mm/dd/yyyy' ),
												35, to_char( :as_prcl.flex_35, 'mm/dd/yyyy' ),
												36, to_char( :as_prcl.flex_36, 'mm/dd/yyyy' ),
												37, to_char( :as_prcl.flex_37, 'mm/dd/yyyy' ),
												38, to_char( :as_prcl.flex_38, 'mm/dd/yyyy' ),
												39, trim( to_char( :as_prcl.flex_39 ) ),
												40, trim( to_char( :as_prcl.flex_40 ) ),
												41, trim( to_char( :as_prcl.flex_41 ) ),
												42, trim( to_char( :as_prcl.flex_42 ) ),
												43, trim( to_char( :as_prcl.flex_43 ) ),
												44, trim( to_char( :as_prcl.flex_44 ) ),
												45, trim( to_char( :as_prcl.flex_45 ) ),
												46, trim( to_char( :as_prcl.flex_46 ) ),
												47, trim( to_char( :as_prcl.flex_47 ) ),
												48, trim( to_char( :as_prcl.flex_48 ) ),
												49, trim( to_char( :as_prcl.flex_49 ) ),
												50, trim( to_char( :as_prcl.flex_50 ) )
											) = trim( ffv.value );
						
						if rows = 0 then
							ff_label = ds_ff.getitemstring( i, 2 )
							if isnull( ff_label ) or trim( ff_label ) = "" then ff_label = "flex " + string( ff_id )
							a_msg = "the value in '" + ff_label + "' must be contained in the list of valid values for this flex field.  either enter a valid value for this field or update the list of valid values " + &
								"for this flex field."
							destroy ds_ff
							return false
						end if
					end if
			next
			
			destroy ds_ff

////
////	if districts and parcels are one-to-one, validate the tax district information.  if not, make sure the state on the parcel and state on the tax district match.
////
			if as_dist.is_parcel_one_to_one = 1 then
				////
				////	state id
				////
					if isnull( as_dist.state_id ) or trim( as_dist.state_id ) = "" then
						a_msg = "'state' is a required field (for districts).  please select a state."
						return false
					end if
					
					if as_dist.state_id <> as_prcl.state_id then
						a_msg = "there is a mismatch between the 'state' on the parcel and 'state' on the tax district.  the two must be the same."
						return false
					end if
				
				////
				////	county id
				////
					if isnull( as_dist.county_id ) or trim( as_dist.county_id ) = "" then
						a_msg = "'county' is a required field.  please select a county."
						return false
					end if
				
				////
				////	use composite authority yn
				////
					if isnull( as_dist.use_composite_authority_yn ) then
						a_msg = "'use composite authorities' is a required field.  please select an option for this field."
						return false
					end if
			else
				select		td.state_id
				into		:st_id
				from		prop_tax_district td
				where	td.tax_district_id = :as_prcl.tax_district_id;
				
				if as_prcl.state_id <> st_id then
					a_msg = "there is a mismatch between the 'state' on the parcel and 'state' on the tax district.  the two must be the same.  either update the state on the parcel to match the " + &
						"state of the selected tax district or select a tax district with a state matching the state for this parcel."
					return false
				end if
			end if

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parceladd (ref s_pt_parcel as_prcl, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parceladd()
 **	
 **	adds a new parcel to the database.  call this version of the function if you're parcel is not one-to-one with tax districts.  if your parcel is one-to-one then you should call the version of the
 **	function with the district parameter   before doing the add, the function will validate all of the fields.  the new parcel id will be returned via the as_prcl.parcel_id field.
 **	
 **	parameters	:	ref s_pt_parcel		:	(as_prcl) the parcel details for the parcel to add.  after adding, the new parcel id will be returned via the parcel_id field.
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel was added successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
	s_pt_district s_dist

////
////	set the district structure to indicate that this parcel is not one-to-one with tax districts and call the more detailed version of this function to do the add.
////
			s_dist.is_parcel_one_to_one = 0
			
			return this.of_parceladd( as_prcl, s_dist, a_msg )
end function

public function boolean of_parceladd (ref s_pt_parcel as_prcl, ref s_pt_district as_dist, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parceladd()
 **	
 **	adds a new parcel to the database.  call this version of the function if you're parcel is one-to-one with tax districts.  if your parcel is not one-to-one then you can call either this version of
 **	the function or the one with less parameters.  this function will insert a new tax district if the parcel is one-to-one with tax districts (based on the information in the districts structure).
 **	before doing the add, the function will validate all of the fields.  the new parcel id will be returned via the as_prcl.parcel_id field (as will the new tax district id if this is a one-to-one parcel).
 **	
 **	parameters	:	ref s_pt_parcel		:	(as_prcl) the parcel details for the parcel to add.  after adding, the new parcel id will be returned via the parcel_id field.  (if a district is create the id
 **													for the new tax district is returned via the tax_district_id field.)
 **						ref s_pt_district	:	(as_dist) the district details for the parcel to add.  at a minimum the is_parcel_one_to_one field must be populated.  if that field is set to 0 then the
 **													rest of this structure is ignored.  if the field is set to 1, this structure used to add the related tax district.  after adding, the new tax district id will be
 **													returned via the tax_district_id field (as will the new description and ).
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel was added successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the other of_parceladd() functions as all functions are used to add parcels.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		prcl_id, td_id
	string	lgla, lglb, lglc, lgld
	string	td_desc, td_desc

////
////	make sure the parcel id is null (since we're adding).
////
			setnull( as_prcl.parcel_id )

////
////	validate.
////
			if not this.of_parcelvalidate( as_prcl, as_dist, a_msg ) then return false

////
////	get the next parcel id to use.  store this in a variable so that we can pass it back if successful.
////
			setnull( prcl_id )
			select		pt_parcel_seq.nextval
			into		:prcl_id
			from		dual;
			
			if isnull( prcl_id ) then
				a_msg = "a database error occurred while obtaining the internal id for the parcel being added.  if you continue to receive this error contact your internal powerplant support or the " + &
					"powerplan property tax support desk."
				return false
			end if

////
////	split up the legal description into 4 4000 character fields for storage in the database.
////	
////	just an fyi - we may run into crazy errors where the result of the substringing above still results in a description that is  than 4000 bytes causing an error on the insert.  this happens if
////	the user has entered any multi-byte characters such as � or � or �.  i could probably write some function that does the same thing as the mid function taking these characters into account,
////	but that's way too much work for too little gain.  the solution, then, is to tell users not to use these characters.  (and if they are getting an error because they are trying to use these characters
////	then they should change them into normal notation like � becomes 1/2, � becomes 1/4, and � becomes deg, etc.).
////
			lgla = mid( as_prcl.legal_description, 1, 4000 )
			lglb = mid( as_prcl.legal_description, 4001, 4000 )
			lglc = mid( as_prcl.legal_description, 8001, 4000 )
			lgld = mid( as_prcl.legal_description, 12001, 4000 )

////
////	if we're adding the parcel one-to-one with a tax district create the tax district.  protect against single quotes.
////
			if as_dist.is_parcel_one_to_one = 1 then
				select		nvl( max( td.tax_district_id ), 0 ) + 1
				into		:td_id
				from		prop_tax_district td;
				
				td_desc = "parcel_id = " + string( prcl_id )
				td_desc = td_desc
				
				insert into prop_tax_district
					(	tax_district_id, description, description, state_id, county_id, type_code_id, tax_district_code, grid_coordinate, assessor_id, use_composite_authority_yn,
						is_parcel_one_to_one, assignment_indicator
					)
					values
						(	:td_id, :td_desc, :td_desc, :as_dist.state_id, :as_dist.county_id, :as_dist.type_code_id, :as_dist.tax_district_code, :as_dist.grid_coordinate, :as_dist.assessor_id,
							:as_dist.use_composite_authority_yn, :as_dist.is_parcel_one_to_one, :as_dist.assignment_indicator
						);
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while creating a new tax district for this parcel.  if you continue to receive this error contact your internal powerplant support or the " + &
						"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			else
				td_id = as_prcl.tax_district_id
			end if

////
////	make our insert into the parcel table.  protect against single qotes.
////
			insert into pt_parcel
				(	parcel_id, parcel_number, description, description, parcel_type_id, prop_tax_company_id, state_id, tax_district_id, land_acreage, improve_sq_ft, mineral_acreage,
					legal_description, legal_description_b, legal_description_c, legal_description_d, grantor, address_1, address_2, city, zip_code, company_grid_number, state_grid_number,
					x_coordinate, y_coordinate, retired_date, notes
				)
				values
					(	:prcl_id, :as_prcl.parcel_number, :as_prcl.description, :as_prcl.description, :as_prcl.parcel_type_id, :as_prcl.prop_tax_company_id, :as_prcl.state_id, :td_id,
						:as_prcl.land_acreage, :as_prcl.improve_sq_ft, :as_prcl.mineral_acreage, :lgla, :lglb, :lglc, :lgld, :as_prcl.grantor, :as_prcl.address_1, :as_prcl.address_2, :as_prcl.city,
						:as_prcl.zip_code, :as_prcl.company_grid_number, :as_prcl.state_grid_number, :as_prcl.x_coordinate, :as_prcl.y_coordinate, :as_prcl.retired_date, :as_prcl.notes
					);
			
			if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while inserting the new parcel.  if you continue to receive this error contact your internal powerplant support or the powerplan property tax " + &
						"support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if

////
////	make our insert into the flex field table.  protect against single quotes.
////
			insert into pt_parcel_flex_fields
				(	parcel_id, flex_1, flex_2, flex_3, flex_4, flex_5, flex_6, flex_7, flex_8, flex_9, flex_10, flex_11, flex_12, flex_13, flex_14, flex_15, flex_16, flex_17, flex_18, flex_19, flex_20,
					flex_21, flex_22, flex_23, flex_24, flex_25, flex_26, flex_27, flex_28, flex_29, flex_30, flex_31, flex_32, flex_33, flex_34, flex_35, flex_36, flex_37, flex_38, flex_39, flex_40,
					flex_41, flex_42, flex_43, flex_44, flex_45, flex_46, flex_47, flex_48, flex_49, flex_50
				)
				values
					(	:prcl_id, :as_prcl.flex_1, :as_prcl.flex_2, :as_prcl.flex_3, :as_prcl.flex_4, :as_prcl.flex_5, :as_prcl.flex_6, :as_prcl.flex_7, :as_prcl.flex_8, :as_prcl.flex_9, :as_prcl.flex_10,
						:as_prcl.flex_11, :as_prcl.flex_12, :as_prcl.flex_13, :as_prcl.flex_14, :as_prcl.flex_15, :as_prcl.flex_16, :as_prcl.flex_17, :as_prcl.flex_18, :as_prcl.flex_19, :as_prcl.flex_20,
						:as_prcl.flex_21, :as_prcl.flex_22, :as_prcl.flex_23, :as_prcl.flex_24, :as_prcl.flex_25, :as_prcl.flex_26, :as_prcl.flex_27, :as_prcl.flex_28, :as_prcl.flex_29, :as_prcl.flex_30,
						:as_prcl.flex_31, :as_prcl.flex_32, :as_prcl.flex_33, :as_prcl.flex_34, :as_prcl.flex_35, :as_prcl.flex_36, :as_prcl.flex_37, :as_prcl.flex_38, :as_prcl.flex_39, :as_prcl.flex_40,
						:as_prcl.flex_41, :as_prcl.flex_42, :as_prcl.flex_43, :as_prcl.flex_44, :as_prcl.flex_45, :as_prcl.flex_46, :as_prcl.flex_47, :as_prcl.flex_48, :as_prcl.flex_49, :as_prcl.flex_50
					);
			
			if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while inserting the flex fields for this parcel.  if you continue to receive this error contact your internal powerplant support or the " + &
						"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if

////
////	call the customizable function for parcel adds.
////
			if not f_ptc_client_parcel_add( prcl_id, a_msg ) then return false

////
////	update the parcel structure (and the district structure, if necessary) to contain the ids for the new parcel/district we added.  we wait to do this at the end so that it only gets done if everything
////	was successful (so we don't pass back invalid data).
////
			as_prcl.parcel_id = prcl_id
			
			if as_dist.is_parcel_one_to_one = 1 then
				as_prcl.tax_district_id = td_id
				as_dist.tax_district_id = td_id
				as_dist.description = td_desc
				as_dist.description = td_desc
			end if

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parceledit (s_pt_parcel as_prcl, s_pt_district as_dist, string a_mod_fields[], ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parceledit()
 **	
 **	edits a parcel.  this function can update tax district fields if the parcel is one-to-one with a tax district (based on the current database value since this cannot be changed after a parcel
 **	has been added).  if this parcel is not one-to-one any data in the districts structure will be ignored.  pass an array of the fields to update in the a_mod_fields[] array so that only those fields
 **	that have changed will be updated.  (thus, it is only necessary to populate as_prcl.parcel_id and the fields that are changing as listed in the a_mod_fields[] array).  before doing the update,
 **	the function will validate all of the fields.
 **	
 **	parameters	:	s_pt_parcel		:	(as_prcl) the parcel details for the parcel to edit.  the only required fields in here are parcel_id as well as any field in the a_mod_fields[] array.
 **						s_pt_district		:	(as_dist) the district details for the parcel to edit.  if the parcel is not one-to-one with a tax district (based on the is_parcel_one_to_one field in this array if
 **												populated or on the current value in the database) this structure is ignored and no prop_tax_district fields are updated.  only some of the fields in here are
 **												updateable for 1-to-1 parcels.  those that are updateable include: county_id, type_code_id, assessor_id, tax_district_code, grid_coordinate,
 **												use_composite_authority_yn, assignment_indicator.  (non-updatable ones include description, legal_description, is_parcel_one_to_one.)
 **						string				:	(a_mod_fields[]) an array holding each of the columns that should be edited/updated.
 **						ref string			:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel was edited successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi
		rows
		td_id, is_1to1_yn, ptco_id
	string	lgla, lglb, lglc, lgld, st_id, cty_id
	string	prcl_sqls, td_sqls, ff_sqls
	boolean	first_prcl_added, first_td_added, first_ff_added
	boolean	td_changed, check_loc, check_bills, check_ldg
	boolean	st_changed, ptco_changed, cty_changed

/*
 *	misc objects.
 */
	s_pt_parcel s_valid_prcl, s_orig_prcl
	s_pt_district s_valid_dist, s_orig_dist

////
////	don't error.
////
			if upperbound( a_mod_fields[] ) = 0 then
				a_msg = ""
				return true
			end if

////
////	make sure we have a parcel id.
////
			if isnull( as_prcl.parcel_id ) then
				a_msg = "no internal parcel_id specified for the parcel to update.  if you continue to receive this error contact your internal powerplant support or the powerplan property tax " + &
					"support desk."
				return false
			end if

////
////	make sure the tax district id hasn't changed if this is a one to one parcel.
////
			select		prcl.tax_district_id tax_district_id,
						td.is_parcel_one_to_one is_parcel_one_to_one
			into		:td_id,
						:is_1to1_yn
			from		pt_parcel prcl,
						prop_tax_district td
			where	prcl.parcel_id = :as_prcl.parcel_id
				and	prcl.tax_district_id = td.tax_district_id;
			
			maxi = upperbound( a_mod_fields[] )
			td_changed = false
			
			for i = 1 to maxi
				if a_mod_fields[i] = "tax_district_id" then
					td_changed = true
					exit
				end if
			next
			
			if td_changed and is_1to1_yn = 1 then
				if as_prcl.tax_district_id <> td_id then
					a_msg = "the tax district cannot be changed for any parcel whose 'is parcel one to one' flag is set to 'yes'."
					return false
				elseif as_dist.tax_district_id <> td_id then
					a_msg = "the tax district cannot be changed for any parcel whose 'is parcel one to one' flag is set to 'yes'.  a mismatch occurred between the parcel and tax district " + &
						"information being updated.  if you continue to receive this error contact your internal powerplant support or the powerplan property tax support desk."
					return false
				end if
			end if

////
////	there are some restrictions on what can change when - check those restrictions here.
////	also, translating to parcel id may require the prop tax company/state/county to be included in the mod fields, even if they're not being modified.
////	so check if they've actually changed.
////
			////
			////	retrieve the current values.
			////
				select	prcl.state_id,
							prcl.prop_tax_company_id,
							td.county_id
				into		:st_id,
							:ptco_id,
							:cty_id
				from		pt_parcel prcl,
							prop_tax_district td
				where	prcl.parcel_id = :as_prcl.parcel_id
					and	prcl.tax_district_id = td.tax_district_id;
				
			////
			////	figure out what we need to check.
			////
				maxi = upperbound( a_mod_fields[] )
				check_loc = false
				check_bills = false
				check_ldg = false
				st_changed = false
				ptco_changed = false
				cty_changed = false
				
				for i = 1 to maxi
					choose case lower( trim( a_mod_fields[i] ) )
						case	"state_id"
							if as_prcl.state_id <> st_id then
								st_changed = true
								check_bills = true
								check_ldg = true
							end if
						case	"prop_tax_company_id"
							if as_prcl.prop_tax_company_id <> ptco_id then
								ptco_changed = true
								check_loc = true
								check_bills = true
								check_ldg = true
							end if
						case "county_id"
							if as_dist.county_id <> cty_id then
								cty_changed = true
							end if
						case	"parcel_type_id"
							check_loc = true
					end choose
				next
			
			////
			////	cannot change parcel type or prop tax company if this guy is assigned to any locations.
			////
				if check_loc then
					setnull( rows )
					
					select		count('x')
					into		:rows
					from		pt_parcel_location ploc
					where	ploc.parcel_id = :as_prcl.parcel_id;
					
					if not isnull( rows ) and rows > 0 then
						maxi = upperbound( a_mod_fields[] )
						
						for i = 1 to maxi
							choose case lower( trim( a_mod_fields[i] ) )
								case	"parcel_type_id"
									a_msg = "this parcel is currently assigned to one or more locations and, thus, it's 'parcel type' cannot be changed.  if you would like to change the 'parcel type' for " + &
										"this parcel you will first need to delete all parcel - location assignments for this parcel.  or, you could create a new parcel for the correct parcel type and " + &
										"invaildate or retire this parcel."
									return false
								case	"prop_tax_company_id"
									if ptco_changed then
										a_msg = "this parcel is currently assigned to one or more locations and, thus, it's 'prop tax company' cannot be changed.  if you would like to change the 'prop tax " + &
											"company' for this parcel you will first need to delete all parcel - location assignments for this parcel.  or, you could create a new parcel for the correct prop tax " + &
											"company and invaildate or retire this parcel."
										return false
									end if
							end choose
						next
					end if
				end if
			
			////
			////	cannot change prop tax company or state if this guy is assigned to any bills.
			////
				if check_bills then
					setnull( rows )
					
					select		count('x')
					into		:rows
					from		pt_statement_line sl
					where	sl.parcel_id = :as_prcl.parcel_id;
					
					if not isnull( rows ) and rows > 0 then
						maxi = upperbound( a_mod_fields[] )
						
						for i = 1 to maxi
							choose case lower( trim( a_mod_fields[i] ) )
								case	"state_id"
									if st_changed then
										a_msg = "this parcel is currently assigned to one or more bills and, thus, it's 'state' cannot be changed.  if you would like to change the 'state' for this parcel you will " + &
											"first need to delete all billing lines for this parcel.  or, you could create a new parcel for the correct state and invaildate or retire this parcel."
										return false
									end if
								case	"prop_tax_company_id"
									if ptco_changed then
										a_msg = "this parcel is currently assigned to one or more bills and, thus, it's 'prop tax company' cannot be changed.  if you would like to change the 'prop tax " + &
											"company' for this parcel you will first need to delete all billing lines for this parcel.  or, you could create a new parcel for the correct prop tax company and " + &
											"invaildate or retire this parcel."
										return false
									end if
							end choose
						next
					end if
				end if
			
			////
			////	cannot change prop tax company or state if this guy is assigned to any ledger items.  before we do this check get rid of any orphan headers out there.
			////
				if check_ldg then
					delete from pt_ledger ptl
						where	ptl.parcel_id = :as_prcl.parcel_id
							and	not exists
										(	select		'x'
											from		pt_ledger_tax_year ptlty
											where	ptl.property_tax_ledger_id = ptlty.property_tax_ledger_id
										);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while deleting any orphan ledger header records for this parcel.  if you continue to receive this error contact your internal " + &
							"powerplant support or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				
					setnull( rows )
					
					select		count('x')
					into		:rows
					from		pt_ledger ptl
					where	ptl.parcel_id = :as_prcl.parcel_id;
					
					if not isnull( rows ) and rows > 0 then
						maxi = upperbound( a_mod_fields[] )
						
						for i = 1 to maxi
							choose case lower( trim( a_mod_fields[i] ) )
								case	"state_id"
									if st_changed then
										a_msg = "this parcel is currently assigned to one or more ledger items and, thus, it's 'state' cannot be changed.  if you would like to change the 'state' for this parcel " + &
											"you will first need to delete all ledger items for this parcel.  or, you could create a new parcel for the correct state and invaildate or retire this parcel."
										return false
									end if
								case	"prop_tax_company_id"
									if ptco_changed then
										a_msg = "this parcel is currently assigned to one or more ledger items and, thus, it's 'prop tax company' cannot be changed.  if you would like to change the 'prop tax " + &
											"company' for this parcel you will first need to delete all ledger items for this parcel.  or, you could create a new parcel for the correct prop tax company and " + &
											"invaildate or retire this parcel."
										return false
									end if
							end choose
						next
					end if
				end if

////
////	create a parcel structure that will hold a combination of fields being updated and current fields for validation.  start by populating it with what currently exists in the database.
////
			s_valid_prcl.parcel_id = as_prcl.parcel_id
			
			select		prcl.parcel_number, prcl.description, prcl.description, prcl.parcel_type_id, prcl.prop_tax_company_id, prcl.state_id, prcl.tax_district_id, prcl.land_acreage, prcl.improve_sq_ft,
						prcl.mineral_acreage, prcl.legal_description, prcl.legal_description_b, prcl.legal_description_c, prcl.legal_description_d, prcl.grantor, prcl.address_1, prcl.address_2, prcl.city,
						prcl.zip_code, prcl.company_grid_number, prcl.state_grid_number, prcl.x_coordinate, prcl.y_coordinate, prcl.retired_date, prcl.notes, ff.flex_1, ff.flex_2, ff.flex_3, ff.flex_4,
						ff.flex_5, ff.flex_6, ff.flex_7, ff.flex_8, ff.flex_9, ff.flex_10, ff.flex_11, ff.flex_12, ff.flex_13, ff.flex_14, ff.flex_15, ff.flex_16, ff.flex_17, ff.flex_18, ff.flex_19, ff.flex_20,
						ff.flex_21, ff.flex_22, ff.flex_23, ff.flex_24, ff.flex_25, ff.flex_26, ff.flex_27, ff.flex_28, ff.flex_29, ff.flex_30,
						ff.flex_31, ff.flex_32, ff.flex_33, ff.flex_34, ff.flex_35, ff.flex_36, ff.flex_37, ff.flex_38, ff.flex_39, ff.flex_40,
						ff.flex_41, ff.flex_42, ff.flex_43, ff.flex_44, ff.flex_45, ff.flex_46, ff.flex_47, ff.flex_48, ff.flex_49, ff.flex_50
			into		:s_valid_prcl.parcel_number, :s_valid_prcl.description, :s_valid_prcl.description, :s_valid_prcl.parcel_type_id, :s_valid_prcl.prop_tax_company_id, :s_valid_prcl.state_id,
						:s_valid_prcl.tax_district_id, :s_valid_prcl.land_acreage, :s_valid_prcl.improve_sq_ft, :s_valid_prcl.mineral_acreage, :lgla, :lglb, :lglc, :lgld, :s_valid_prcl.grantor,
						:s_valid_prcl.address_1, :s_valid_prcl.address_2, :s_valid_prcl.city, :s_valid_prcl.zip_code, :s_valid_prcl.company_grid_number, :s_valid_prcl.state_grid_number,
						:s_valid_prcl.x_coordinate, :s_valid_prcl.y_coordinate, :s_valid_prcl.retired_date, :s_valid_prcl.notes, :s_valid_prcl.flex_1, :s_valid_prcl.flex_2, :s_valid_prcl.flex_3,
						:s_valid_prcl.flex_4, :s_valid_prcl.flex_5, :s_valid_prcl.flex_6, :s_valid_prcl.flex_7, :s_valid_prcl.flex_8, :s_valid_prcl.flex_9, :s_valid_prcl.flex_10, :s_valid_prcl.flex_11,
						:s_valid_prcl.flex_12, :s_valid_prcl.flex_13, :s_valid_prcl.flex_14, :s_valid_prcl.flex_15, :s_valid_prcl.flex_16, :s_valid_prcl.flex_17, :s_valid_prcl.flex_18, :s_valid_prcl.flex_19,
						:s_valid_prcl.flex_20, :s_valid_prcl.flex_21, :s_valid_prcl.flex_22, :s_valid_prcl.flex_23, :s_valid_prcl.flex_24, :s_valid_prcl.flex_25, :s_valid_prcl.flex_26, :s_valid_prcl.flex_27, :s_valid_prcl.flex_28, :s_valid_prcl.flex_29,
						:s_valid_prcl.flex_30, :s_valid_prcl.flex_31, :s_valid_prcl.flex_32, :s_valid_prcl.flex_33, :s_valid_prcl.flex_34, :s_valid_prcl.flex_35, :s_valid_prcl.flex_36, :s_valid_prcl.flex_37, :s_valid_prcl.flex_38, :s_valid_prcl.flex_39,
						:s_valid_prcl.flex_40, :s_valid_prcl.flex_41, :s_valid_prcl.flex_42, :s_valid_prcl.flex_43, :s_valid_prcl.flex_44, :s_valid_prcl.flex_45, :s_valid_prcl.flex_46, :s_valid_prcl.flex_47, :s_valid_prcl.flex_48, :s_valid_prcl.flex_49,
						:s_valid_prcl.flex_50
			from		pt_parcel prcl,
						pt_parcel_flex_fields ff
			where	prcl.parcel_id = :as_prcl.parcel_id
				and	prcl.parcel_id = ff.parcel_id
				and	ff.parcel_id = :as_prcl.parcel_id;
			
			if isnull( lgla ) then lgla = ""
			if isnull( lglb ) then lglb = ""
			if isnull( lglc ) then lglc = ""
			if isnull( lgld ) then lgld = ""
			
			s_valid_prcl.legal_description = lgla + lglb + lglc + lgld
			
			s_orig_prcl = s_valid_prcl

////
////	if we need to, populate the district structure for validation with what's currently in the database.
////
			if is_1to1_yn = 1 then
				select		tax_district_id, description, description, state_id, county_id, type_code_id, assessor_id, tax_district_code, grid_coordinate, use_composite_authority_yn,
							is_parcel_one_to_one, assignment_indicator
				into		:s_valid_dist.tax_district_id, :s_valid_dist.description, :s_valid_dist.description, :s_valid_dist.state_id, :s_valid_dist.county_id, :s_valid_dist.type_code_id,
							:s_valid_dist.assessor_id, :s_valid_dist.tax_district_code, :s_valid_dist.grid_coordinate, :s_valid_dist.use_composite_authority_yn, :s_valid_dist.is_parcel_one_to_one,
							:s_valid_dist.assignment_indicator
				from		prop_tax_district td
				where	td.tax_district_id = :s_valid_prcl.tax_district_id;
			else
				s_valid_dist.is_parcel_one_to_one = 0
			end if
			
			s_orig_dist = s_valid_dist

////
////	update the parcel structure for validation with the new values to update.
////
			maxi = upperbound( a_mod_fields[] )
			
			for i = 1 to maxi
				choose case lower( trim( a_mod_fields[i] ) )
					case	"parcel_number"
						s_valid_prcl.parcel_number = as_prcl.parcel_number
					case	"description"
						s_valid_prcl.description = as_prcl.description
					case	"description"
						s_valid_prcl.description = as_prcl.description
					case	"parcel_type_id"
						s_valid_prcl.parcel_type_id = as_prcl.parcel_type_id
					case	"prop_tax_company_id"
						if ptco_changed then
							s_valid_prcl.prop_tax_company_id = as_prcl.prop_tax_company_id
						end if
					case	"state_id"
						if st_changed then
							s_valid_prcl.state_id = as_prcl.state_id
							if is_1to1_yn = 1 then s_valid_dist.state_id = as_prcl.state_id
						end if
					case	"tax_district_id"
						if is_1to1_yn = 0 then s_valid_prcl.tax_district_id = as_prcl.tax_district_id
					case	"land_acreage"
						s_valid_prcl.land_acreage = as_prcl.land_acreage
					case	"improve_sq_ft"
						s_valid_prcl.improve_sq_ft = as_prcl.improve_sq_ft
					case	"mineral_acreage"
						s_valid_prcl.mineral_acreage = as_prcl.mineral_acreage
					case	"legal_description"
						s_valid_prcl.legal_description = as_prcl.legal_description
					case	"grantor"
						s_valid_prcl.grantor = as_prcl.grantor
					case	"address_1"
						s_valid_prcl.address_1 = as_prcl.address_1
					case	"address_2"
						s_valid_prcl.address_2 = as_prcl.address_2
					case	"city"
						s_valid_prcl.city = as_prcl.city
					case	"zip_code"
						s_valid_prcl.zip_code = as_prcl.zip_code
					case	"company_grid_number"
						s_valid_prcl.company_grid_number = as_prcl.company_grid_number
					case	"state_grid_number"
						s_valid_prcl.state_grid_number = as_prcl.state_grid_number
					case	"x_coordinate"
						s_valid_prcl.x_coordinate = as_prcl.x_coordinate
					case	"y_coordinate"
						s_valid_prcl.y_coordinate = as_prcl.y_coordinate
					case	"retired_date"
						s_valid_prcl.retired_date = as_prcl.retired_date
					case	"notes"
						s_valid_prcl.notes = as_prcl.notes
					case	"county_id"
						if cty_changed then
							if is_1to1_yn = 1 then s_valid_dist.county_id = as_dist.county_id
						end if
					case	"type_code_id"
						if is_1to1_yn = 1 then s_valid_dist.type_code_id = as_dist.type_code_id
					case	"assessor_id"
						if is_1to1_yn = 1 then s_valid_dist.assessor_id = as_dist.assessor_id
					case	"tax_district_code"
						if is_1to1_yn = 1 then s_valid_dist.tax_district_code = as_dist.tax_district_code
					case	"grid_coordinate"
						if is_1to1_yn = 1 then s_valid_dist.grid_coordinate = as_dist.grid_coordinate
					case	"use_composite_authority_yn"
						if is_1to1_yn = 1 then s_valid_dist.use_composite_authority_yn = as_dist.use_composite_authority_yn
					case	"assignment_indicator"
						if is_1to1_yn = 1 then s_valid_dist.assignment_indicator = as_dist.assignment_indicator
					case	"flex_1"
						s_valid_prcl.flex_1 = as_prcl.flex_1
					case	"flex_2"
						s_valid_prcl.flex_2 = as_prcl.flex_2
					case	"flex_3"
						s_valid_prcl.flex_3 = as_prcl.flex_3
					case	"flex_4"
						s_valid_prcl.flex_4 = as_prcl.flex_4
					case	"flex_5"
						s_valid_prcl.flex_5 = as_prcl.flex_5
					case	"flex_6"
						s_valid_prcl.flex_6 = as_prcl.flex_6
					case	"flex_7"
						s_valid_prcl.flex_7 = as_prcl.flex_7
					case	"flex_8"
						s_valid_prcl.flex_8 = as_prcl.flex_8
					case	"flex_9"
						s_valid_prcl.flex_9 = as_prcl.flex_9
					case	"flex_10"
						s_valid_prcl.flex_10 = as_prcl.flex_10
					case	"flex_11"
						s_valid_prcl.flex_11 = as_prcl.flex_11
					case	"flex_12"
						s_valid_prcl.flex_12 = as_prcl.flex_12
					case	"flex_13"
						s_valid_prcl.flex_13 = as_prcl.flex_13
					case	"flex_14"
						s_valid_prcl.flex_14 = as_prcl.flex_14
					case	"flex_15"
						s_valid_prcl.flex_15 = as_prcl.flex_15
					case	"flex_16"
						s_valid_prcl.flex_16 = as_prcl.flex_16
					case	"flex_17"
						s_valid_prcl.flex_17 = as_prcl.flex_17
					case	"flex_18"
						s_valid_prcl.flex_18 = as_prcl.flex_18
					case	"flex_19"
						s_valid_prcl.flex_19 = as_prcl.flex_19
					case	"flex_20"
						s_valid_prcl.flex_20 = as_prcl.flex_20
					case	"flex_21"
						s_valid_prcl.flex_21 = as_prcl.flex_21
					case	"flex_22"
						s_valid_prcl.flex_22 = as_prcl.flex_22
					case	"flex_23"
						s_valid_prcl.flex_23 = as_prcl.flex_23
					case	"flex_24"
						s_valid_prcl.flex_24 = as_prcl.flex_24
					case	"flex_25"
						s_valid_prcl.flex_25 = as_prcl.flex_25
					case	"flex_26"
						s_valid_prcl.flex_26 = as_prcl.flex_26
					case	"flex_27"
						s_valid_prcl.flex_27 = as_prcl.flex_27
					case	"flex_28"
						s_valid_prcl.flex_28 = as_prcl.flex_28
					case	"flex_29"
						s_valid_prcl.flex_29 = as_prcl.flex_29
					case	"flex_30"
						s_valid_prcl.flex_30 = as_prcl.flex_30
					case	"flex_31"
						s_valid_prcl.flex_31 = as_prcl.flex_31
					case	"flex_32"
						s_valid_prcl.flex_32 = as_prcl.flex_32
					case	"flex_33"
						s_valid_prcl.flex_33 = as_prcl.flex_33
					case	"flex_34"
						s_valid_prcl.flex_34 = as_prcl.flex_34
					case	"flex_35"
						s_valid_prcl.flex_35 = as_prcl.flex_35
					case	"flex_36"
						s_valid_prcl.flex_36 = as_prcl.flex_36
					case	"flex_37"
						s_valid_prcl.flex_37 = as_prcl.flex_37
					case	"flex_38"
						s_valid_prcl.flex_38 = as_prcl.flex_38
					case	"flex_39"
						s_valid_prcl.flex_39 = as_prcl.flex_39
					case	"flex_40"
						s_valid_prcl.flex_40 = as_prcl.flex_40
					case	"flex_41"
						s_valid_prcl.flex_41 = as_prcl.flex_41
					case	"flex_42"
						s_valid_prcl.flex_42 = as_prcl.flex_42
					case	"flex_43"
						s_valid_prcl.flex_43 = as_prcl.flex_43
					case	"flex_44"
						s_valid_prcl.flex_44 = as_prcl.flex_44
					case	"flex_45"
						s_valid_prcl.flex_45 = as_prcl.flex_45
					case	"flex_46"
						s_valid_prcl.flex_46 = as_prcl.flex_46
					case	"flex_47"
						s_valid_prcl.flex_47 = as_prcl.flex_47
					case	"flex_48"
						s_valid_prcl.flex_48 = as_prcl.flex_48
					case	"flex_49"
						s_valid_prcl.flex_49 = as_prcl.flex_49
					case	"flex_50"
						s_valid_prcl.flex_50 = as_prcl.flex_50
				end choose
			next

////
////	validate.
////
			if not this.of_parcelvalidate( s_valid_prcl, s_valid_dist, a_msg ) then return false

////
////	build the string for the update.  make sure to protect against single quotes (') in char fields.
////
			prcl_sqls =	"	update	pt_parcel prcl " + "~r~n" + &
							"	set		"
			td_sqls =	"	update	prop_tax_district td " + "~r~n" + &
							"	set		"
			ff_sqls =		"	update	pt_parcel_flex_fields ff " + "~r~n" + &
							"	set		"
			
			first_prcl_added = false
			first_td_added = false
			first_ff_added = false
			
			maxi = upperbound( a_mod_fields[] )
			
			for i = 1 to maxi
				choose case lower( trim( a_mod_fields[i] ) )
					case	"parcel_number"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.parcel_number ) then
							prcl_sqls = prcl_sqls + "prcl.parcel_number = null"
						else
							prcl_sqls = prcl_sqls + "prcl.parcel_number = '" + f_replace_string( trim( as_prcl.parcel_number ), "'", "''", "all" ) + "'"
						end if
						
						first_prcl_added = true
					case	"description"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.description ) then
							prcl_sqls = prcl_sqls + "prcl.description = null"
						else
							prcl_sqls = prcl_sqls + "prcl.description = '" + f_replace_string( trim( as_prcl.description ), "'", "''", "all" ) + "'"
						end if
						
						first_prcl_added = true
					case	"description"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.description ) then
							prcl_sqls = prcl_sqls + "prcl.description = null"
						else
							prcl_sqls = prcl_sqls + "prcl.description = '" + f_replace_string( trim( as_prcl.description ), "'", "''", "all" ) + "'"
						end if
						
						first_prcl_added = true
					case	"parcel_type_id"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.parcel_type_id ) then
							prcl_sqls = prcl_sqls + "prcl.parcel_type_id = null"
						else
							prcl_sqls = prcl_sqls + "prcl.parcel_type_id = " + string( as_prcl.parcel_type_id )
						end if
						
						first_prcl_added = true
					case	"prop_tax_company_id"
						if ptco_changed then
							if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
							
							if isnull( as_prcl.prop_tax_company_id ) then
								prcl_sqls = prcl_sqls + "prcl.prop_tax_company_id = null"
							else
								prcl_sqls = prcl_sqls + "prcl.prop_tax_company_id = " + string( as_prcl.prop_tax_company_id )
							end if
							
							first_prcl_added = true
						end if
					case	"state_id"
						if st_changed then
							if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
							
							if isnull( as_prcl.state_id ) then
								prcl_sqls = prcl_sqls + "prcl.state_id = null"
								if is_1to1_yn = 1 then
									if first_td_added then td_sqls = td_sqls + ", " + "~r~n" + "				"
									td_sqls = td_sqls + "td.state_id = null"
									first_td_added = true
								end if
							else
								prcl_sqls = prcl_sqls + "prcl.state_id = '" + as_prcl.state_id + "'"
								if is_1to1_yn = 1 then
									if first_td_added then td_sqls = td_sqls + ", " + "~r~n" + "				"
									td_sqls = td_sqls + "td.state_id = '" + as_prcl.state_id + "'"
									first_td_added = true
								end if
							end if
							
							first_prcl_added = true
						end if
					case	"tax_district_id"
						if is_1to1_yn = 0 then
							if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
							if isnull( as_prcl.tax_district_id ) then
								prcl_sqls = prcl_sqls + "prcl.tax_district_id = null"
							else
								prcl_sqls = prcl_sqls + "prcl.tax_district_id = " + string( as_prcl.tax_district_id )
							end if
							
							first_prcl_added = true
						end if
					case	"land_acreage"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.land_acreage ) then
							prcl_sqls = prcl_sqls + "prcl.land_acreage = null"
						else
							prcl_sqls = prcl_sqls + "prcl.land_acreage = " + string( as_prcl.land_acreage )
						end if
						
						first_prcl_added = true
					case	"improve_sq_ft"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.improve_sq_ft ) then
							prcl_sqls = prcl_sqls + "prcl.improve_sq_ft = null"
						else
							prcl_sqls = prcl_sqls + "prcl.improve_sq_ft = " + string( as_prcl.improve_sq_ft )
						end if
						
						first_prcl_added = true
					case	"mineral_acreage"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.mineral_acreage ) then
							prcl_sqls = prcl_sqls + "prcl.mineral_acreage = null"
						else
							prcl_sqls = prcl_sqls + "prcl.mineral_acreage = " + string( as_prcl.mineral_acreage )
						end if
						
						first_prcl_added = true
					case	"legal_description"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						as_prcl.legal_description = trim( as_prcl.legal_description )
						
						lgla = mid( as_prcl.legal_description, 1, 4000 )
						lglb = mid( as_prcl.legal_description, 4001, 4000 )
						lglc = mid( as_prcl.legal_description, 8001, 4000 )
						lgld = mid( as_prcl.legal_description, 12001, 4000 )
						
						if lgla = "" then setnull( lgla )
						if lglb = "" then setnull( lglb )
						if lglc = "" then setnull( lglc )
						if lgld = "" then setnull( lgld )
						
						if isnull( lgla ) then
							prcl_sqls = prcl_sqls + "prcl.legal_description = null"
						else
							prcl_sqls = prcl_sqls + "prcl.legal_description = '" + f_replace_string( lgla, "'", "''", "all" ) + "'"
						end if
						
						prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( lglb ) then
							prcl_sqls = prcl_sqls + "prcl.legal_description_b = null"
						else
							prcl_sqls = prcl_sqls + "prcl.legal_description_b = '" + f_replace_string( lglb, "'", "''", "all" ) + "'"
						end if
						
						prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( lglc ) then
							prcl_sqls = prcl_sqls + "prcl.legal_description_c = null"
						else
							prcl_sqls = prcl_sqls + "prcl.legal_description_c = '" + f_replace_string( lglc, "'", "''", "all" ) + "'"
						end if
						
						prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( lgld ) then
							prcl_sqls = prcl_sqls + "prcl.legal_description_d = null"
						else
							prcl_sqls = prcl_sqls + "prcl.legal_description_d = '" + f_replace_string( lgld, "'", "''", "all" ) + "'"
						end if
						
						first_prcl_added = true
					case	"grantor"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.grantor ) then
							prcl_sqls = prcl_sqls + "prcl.grantor = null"
						else
							prcl_sqls = prcl_sqls + "prcl.grantor = '" + f_replace_string( trim( as_prcl.grantor ), "'", "''", "all" ) + "'"
						end if
						
						first_prcl_added = true
					case	"address_1"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.address_1 ) then
							prcl_sqls = prcl_sqls + "prcl.address_1 = null"
						else
							prcl_sqls = prcl_sqls + "prcl.address_1 = '" + f_replace_string( trim( as_prcl.address_1 ), "'", "''", "all" ) + "'"
						end if
						
						first_prcl_added = true
					case	"address_2"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.address_2 ) then
							prcl_sqls = prcl_sqls + "prcl.address_2 = null"
						else
							prcl_sqls = prcl_sqls + "prcl.address_2 = '" + f_replace_string( trim( as_prcl.address_2 ), "'", "''", "all" ) + "'"
						end if
						
						first_prcl_added = true
					case	"city"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.city ) then
							prcl_sqls = prcl_sqls + "prcl.city = null"
						else
							prcl_sqls = prcl_sqls + "prcl.city = '" + f_replace_string( trim( as_prcl.city ), "'", "''", "all" ) + "'"
						end if
						
						first_prcl_added = true
					case	"zip_code"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.zip_code ) then
							prcl_sqls = prcl_sqls + "prcl.zip_code = null"
						else
							prcl_sqls = prcl_sqls + "prcl.zip_code = '" + f_replace_string( trim( as_prcl.zip_code ), "'", "''", "all" ) + "'"
						end if
						
						first_prcl_added = true
					case	"company_grid_number"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.company_grid_number ) then
							prcl_sqls = prcl_sqls + "prcl.company_grid_number = null"
						else
							prcl_sqls = prcl_sqls + "prcl.company_grid_number = '" + f_replace_string( trim( as_prcl.company_grid_number ), "'", "''", "all" ) + "'"
						end if
						
						first_prcl_added = true
					case	"state_grid_number"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.state_grid_number ) then
							prcl_sqls = prcl_sqls + "prcl.state_grid_number = null"
						else
							prcl_sqls = prcl_sqls + "prcl.state_grid_number = '" + f_replace_string( trim( as_prcl.state_grid_number ), "'", "''", "all" ) + "'"
						end if
						
						first_prcl_added = true
					case	"x_coordinate"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.x_coordinate ) then
							prcl_sqls = prcl_sqls + "prcl.x_coordinate = null"
						else
							prcl_sqls = prcl_sqls + "prcl.x_coordinate = '" + f_replace_string( trim( as_prcl.x_coordinate ), "'", "''", "all" ) + "'"
						end if
						
						first_prcl_added = true
					case	"y_coordinate"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.y_coordinate ) then
							prcl_sqls = prcl_sqls + "prcl.y_coordinate = null"
						else
							prcl_sqls = prcl_sqls + "prcl.y_coordinate = '" + f_replace_string( trim( as_prcl.y_coordinate ), "'", "''", "all" ) + "'"
						end if
						
						first_prcl_added = true
					case	"retired_date"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.retired_date ) then
							prcl_sqls = prcl_sqls + "prcl.retired_date = null"
						else
							prcl_sqls = prcl_sqls + "prcl.retired_date = to_date( '" + string( as_prcl.retired_date, "mm/dd/yyyy" ) + "', 'mm/dd/yyyy' )"
						end if
						
						first_prcl_added = true
					case	"notes"
						if first_prcl_added then prcl_sqls = prcl_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.notes ) then
							prcl_sqls = prcl_sqls + "prcl.notes = null"
						else
							prcl_sqls = prcl_sqls + "prcl.notes = '" + f_replace_string( trim( as_prcl.notes ), "'", "''", "all" ) + "'"
						end if
						
						first_prcl_added = true
					case	"county_id"
						if cty_changed then
							if is_1to1_yn = 1 then
								if first_td_added then td_sqls = td_sqls + ", " + "~r~n" + "				"
								
								if isnull( as_dist.county_id ) then
									td_sqls = td_sqls + "td.county_id = null"
								else
									td_sqls = td_sqls + "td.county_id = '" + f_replace_string( as_dist.county_id, "'", "''", "all" ) + "'"
								end if
								
								first_td_added = true
							end if
						end if
					case	"type_code_id"
						if is_1to1_yn = 1 then
							if first_td_added then td_sqls = td_sqls + ", " + "~r~n" + "				"
							
							if isnull( as_dist.type_code_id ) then
								td_sqls = td_sqls + "td.type_code_id = null"
							else
								td_sqls = td_sqls + "td.type_code_id = " + string( as_dist.type_code_id )
							end if
							
							first_td_added = true
						end if
					case	"assessor_id"
						if is_1to1_yn = 1 then
							if first_td_added then td_sqls = td_sqls + ", " + "~r~n" + "				"
							
							if isnull( as_dist.assessor_id ) then
								td_sqls = td_sqls + "td.assessor_id = null"
							else
								td_sqls = td_sqls + "td.assessor_id = " + string( as_dist.assessor_id )
							end if
							
							first_td_added = true
						end if
					case	"tax_district_code"
						if is_1to1_yn = 1 then
							if first_td_added then td_sqls = td_sqls + ", " + "~r~n" + "				"
							
							if isnull( as_dist.tax_district_code ) then
								td_sqls = td_sqls + "td.tax_district_code = null"
							else
								td_sqls = td_sqls + "td.tax_district_code = '" + f_replace_string( trim( as_dist.tax_district_code ), "'", "''", "all" ) + "'"
							end if
							
							first_td_added = true
						end if
					case	"grid_coordinate"
						if is_1to1_yn = 1 then
							if first_td_added then td_sqls = td_sqls + ", " + "~r~n" + "				"
							
							if isnull( as_dist.grid_coordinate ) then
								td_sqls = td_sqls + "td.grid_coordinate = null"
							else
								td_sqls = td_sqls + "td.grid_coordinate = '" + f_replace_string( trim( as_dist.grid_coordinate ), "'", "''", "all" ) + "'"
							end if
							
							first_td_added = true
						end if
					case	"use_composite_authority_yn"
						if is_1to1_yn = 1 then
							if first_td_added then td_sqls = td_sqls + ", " + "~r~n" + "				"
							
							if isnull( as_dist.use_composite_authority_yn ) then
								td_sqls = td_sqls + "td.use_composite_authority_yn = null"
							else
								td_sqls = td_sqls + "td.use_composite_authority_yn = " + string( as_dist.use_composite_authority_yn )
							end if
							
							first_td_added = true
						end if
					case	"assignment_indicator"
						if is_1to1_yn = 1 then
							if first_td_added then td_sqls = td_sqls + ", " + "~r~n" + "				"
							
							if isnull( as_dist.assignment_indicator ) then
								td_sqls = td_sqls + "td.assignment_indicator = null"
							else
								td_sqls = td_sqls + "td.assignment_indicator = " + string( as_dist.assignment_indicator )
							end if
							
							first_td_added = true
						end if
					case	"flex_1"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_1 ) then
							ff_sqls = ff_sqls + "ff.flex_1 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_1 = " + string( as_prcl.flex_1 )
						end if
						
						first_ff_added = true
					case	"flex_2"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_2 ) then
							ff_sqls = ff_sqls + "ff.flex_2 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_2 = " + string( as_prcl.flex_2 )
						end if
						
						first_ff_added = true
					case	"flex_3"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_3 ) then
							ff_sqls = ff_sqls + "ff.flex_3 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_3 = " + string( as_prcl.flex_3 )
						end if
						
						first_ff_added = true
					case	"flex_4"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_4 ) then
							ff_sqls = ff_sqls + "ff.flex_4 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_4 = " + string( as_prcl.flex_4 )
						end if
						
						first_ff_added = true
					case	"flex_5"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_5 ) then
							ff_sqls = ff_sqls + "ff.flex_5 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_5 = " + string( as_prcl.flex_5 )
						end if
						
						first_ff_added = true
					case	"flex_6"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_6 ) then
							ff_sqls = ff_sqls + "ff.flex_6 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_6 = to_date( '" + string( as_prcl.flex_6, "mm/dd/yyyy" ) + "', 'mm/dd/yyyy' )"
						end if
						
						first_ff_added = true
					case	"flex_7"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_7 ) then
							ff_sqls = ff_sqls + "ff.flex_7 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_7 = to_date( '" + string( as_prcl.flex_7, "mm/dd/yyyy" ) + "', 'mm/dd/yyyy' )"
						end if
						
						first_ff_added = true
					case	"flex_8"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_8 ) then
							ff_sqls = ff_sqls + "ff.flex_8 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_8 = to_date( '" + string( as_prcl.flex_8, "mm/dd/yyyy" ) + "', 'mm/dd/yyyy' )"
						end if
						
						first_ff_added = true
					case	"flex_9"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_9 ) then
							ff_sqls = ff_sqls + "ff.flex_9 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_9 = to_date( '" + string( as_prcl.flex_9, "mm/dd/yyyy" ) + "', 'mm/dd/yyyy' )"
						end if
						
						first_ff_added = true
					case	"flex_10"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_10 ) then
							ff_sqls = ff_sqls + "ff.flex_10 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_10 = to_date( '" + string( as_prcl.flex_10, "mm/dd/yyyy" ) + "', 'mm/dd/yyyy' )"
						end if
						
						first_ff_added = true
					case	"flex_11"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_11 ) or trim( as_prcl.flex_11 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_11 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_11 = '" + f_replace_string( trim( as_prcl.flex_11 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_12"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_12 ) or trim( as_prcl.flex_12 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_12 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_12 = '" + f_replace_string( trim( as_prcl.flex_12 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_13"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_13 ) or trim( as_prcl.flex_13 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_13 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_13 = '" + f_replace_string( trim( as_prcl.flex_13 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_14"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_14 ) or trim( as_prcl.flex_14 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_14 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_14 = '" + f_replace_string( trim( as_prcl.flex_14 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_15"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_15 ) or trim( as_prcl.flex_15 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_15 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_15 = '" + f_replace_string( trim( as_prcl.flex_15 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_16"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_16 ) or trim( as_prcl.flex_16 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_16 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_16 = '" + f_replace_string( trim( as_prcl.flex_16 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_17"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_17 ) or trim( as_prcl.flex_17 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_17 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_17 = '" + f_replace_string( trim( as_prcl.flex_17 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_18"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_18 ) or trim( as_prcl.flex_18 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_18 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_18 = '" + f_replace_string( trim( as_prcl.flex_18 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_19"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_19 ) or trim( as_prcl.flex_19 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_19 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_19 = '" + f_replace_string( trim( as_prcl.flex_19 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_20"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_20 ) or trim( as_prcl.flex_20 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_20 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_20 = '" + f_replace_string( trim( as_prcl.flex_20 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_21"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_21 ) or trim( as_prcl.flex_21 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_21 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_21 = '" + f_replace_string( trim( as_prcl.flex_21 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_22"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_22 ) or trim( as_prcl.flex_22 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_22 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_22 = '" + f_replace_string( trim( as_prcl.flex_22 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_23"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_23 ) or trim( as_prcl.flex_23 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_23 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_23 = '" + f_replace_string( trim( as_prcl.flex_23 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_24"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_24 ) then
							ff_sqls = ff_sqls + "ff.flex_24 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_24 = " + string( as_prcl.flex_24 )
						end if
						
						first_ff_added = true
						
					case	"flex_25"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_25 ) then
							ff_sqls = ff_sqls + "ff.flex_25 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_25 = " + string( as_prcl.flex_25 )
						end if
						
						first_ff_added = true
					case	"flex_26"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_26 ) then
							ff_sqls = ff_sqls + "ff.flex_26 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_26 = " + string( as_prcl.flex_26 )
						end if
						
						first_ff_added = true
					case	"flex_27"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_27 ) then
							ff_sqls = ff_sqls + "ff.flex_27 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_27 = " + string( as_prcl.flex_27 )
						end if
						
						first_ff_added = true
					case	"flex_28"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_28 ) then
							ff_sqls = ff_sqls + "ff.flex_28 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_28 = " + string( as_prcl.flex_28 )
						end if
						
						first_ff_added = true
					case	"flex_29"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_29 ) then
							ff_sqls = ff_sqls + "ff.flex_29 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_29 = " + string( as_prcl.flex_29 )
						end if
						
						first_ff_added = true
					case	"flex_30"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_30 ) then
							ff_sqls = ff_sqls + "ff.flex_30 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_30 = " + string( as_prcl.flex_30 )
						end if
						
						first_ff_added = true
					case	"flex_31"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_31 ) then
							ff_sqls = ff_sqls + "ff.flex_31 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_31 = " + string( as_prcl.flex_31 )
						end if
						
						first_ff_added = true
					case	"flex_32"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_32 ) then
							ff_sqls = ff_sqls + "ff.flex_32 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_32 = " + string( as_prcl.flex_32 )
						end if
						
						first_ff_added = true
					case	"flex_33"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_33 ) then
							ff_sqls = ff_sqls + "ff.flex_33 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_33 = " + string( as_prcl.flex_33 )
						end if
						
						first_ff_added = true
					case	"flex_34"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_34 ) then
							ff_sqls = ff_sqls + "ff.flex_34 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_34 = to_date( '" + string( as_prcl.flex_34, "mm/dd/yyyy" ) + "', 'mm/dd/yyyy' )"
						end if
						
						first_ff_added = true
					case	"flex_35"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_35 ) then
							ff_sqls = ff_sqls + "ff.flex_35 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_35 = to_date( '" + string( as_prcl.flex_35, "mm/dd/yyyy" ) + "', 'mm/dd/yyyy' )"
						end if
						
						first_ff_added = true
					case	"flex_36"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_36 ) then
							ff_sqls = ff_sqls + "ff.flex_36 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_36 = to_date( '" + string( as_prcl.flex_36, "mm/dd/yyyy" ) + "', 'mm/dd/yyyy' )"
						end if
						
						first_ff_added = true
					case	"flex_37"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_37 ) then
							ff_sqls = ff_sqls + "ff.flex_37 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_37 = to_date( '" + string( as_prcl.flex_37, "mm/dd/yyyy" ) + "', 'mm/dd/yyyy' )"
						end if
						
						first_ff_added = true
					case	"flex_38"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_38 ) then
							ff_sqls = ff_sqls + "ff.flex_38 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_38 = to_date( '" + string( as_prcl.flex_38, "mm/dd/yyyy" ) + "', 'mm/dd/yyyy' )"
						end if
						
						first_ff_added = true
					case	"flex_39"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_39 ) or trim( as_prcl.flex_39 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_39 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_39 = '" + f_replace_string( trim( as_prcl.flex_39 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_40"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_40 ) or trim( as_prcl.flex_40 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_40 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_40 = '" + f_replace_string( trim( as_prcl.flex_40 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_41"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_41 ) or trim( as_prcl.flex_41 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_41 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_41 = '" + f_replace_string( trim( as_prcl.flex_41 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_42"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_42 ) or trim( as_prcl.flex_42 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_42 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_42 = '" + f_replace_string( trim( as_prcl.flex_42 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_43"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_43 ) or trim( as_prcl.flex_43 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_43 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_43 = '" + f_replace_string( trim( as_prcl.flex_43 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_44"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_44 ) or trim( as_prcl.flex_44 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_44 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_44 = '" + f_replace_string( trim( as_prcl.flex_44 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_45"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_45 ) or trim( as_prcl.flex_45 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_45 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_45 = '" + f_replace_string( trim( as_prcl.flex_45 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_46"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_46 ) or trim( as_prcl.flex_46 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_46 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_46 = '" + f_replace_string( trim( as_prcl.flex_46 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_47"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_47 ) or trim( as_prcl.flex_47 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_47 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_47 = '" + f_replace_string( trim( as_prcl.flex_47 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_48"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_48 ) or trim( as_prcl.flex_48 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_48 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_48 = '" + f_replace_string( trim( as_prcl.flex_48 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_49"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_49 ) or trim( as_prcl.flex_49 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_49 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_49 = '" + f_replace_string( trim( as_prcl.flex_49 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
					case	"flex_50"
						if first_ff_added then ff_sqls = ff_sqls + ", " + "~r~n" + "				"
						
						if isnull( as_prcl.flex_50 ) or trim( as_prcl.flex_50 ) = "" then
							ff_sqls = ff_sqls + "ff.flex_50 = null"
						else
							ff_sqls = ff_sqls + "ff.flex_50 = '" + f_replace_string( trim( as_prcl.flex_50 ), "'", "''", "all" ) + "'"
						end if
						
						first_ff_added = true
				end choose
			next
			
			prcl_sqls = prcl_sqls + "~r~n"
			td_sqls = td_sqls + "~r~n"
			
			prcl_sqls = prcl_sqls + "	where	prcl.parcel_id = " + string( as_prcl.parcel_id ) + " "
			td_sqls = td_sqls + "	where	td.tax_district_id = " + string( s_valid_prcl.tax_district_id ) + " "
			ff_sqls = ff_sqls + "	where	ff.parcel_id = " + string( as_prcl.parcel_id ) + " "

////
////	do our updates.  we're only going to run the tax district update if this guy is one to one.
////
			if first_prcl_added then
				execute immediate :prcl_sqls;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while updating the parcel.  if you continue to receive this error contact your internal powerplant support or the powerplan property tax " + &
						"support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if
			
			if first_td_added and is_1to1_yn = 1 then
				execute immediate :td_sqls;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while updating the district information for this parcel.  if you continue to receive this error contact your internal powerplant support or the " + &
						"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if
			
			if first_ff_added then
				execute immediate :ff_sqls;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while updating the parcel flex fields.  if you continue to receive this error contact your internal powerplant support or the powerplan property " + &
						"tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	call the customizable function for parcel updates.
////
			if not f_ptc_client_parcel_update( as_prcl, as_dist, s_orig_prcl, s_orig_dist, a_mod_fields[], a_msg ) then return false

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parcelautoadjustedit (s_pt_parcel_auto_adjust as_adj[], ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelautoadjustedit()
 **	
 **	adds, updates or deletes the parcel auto adjust table for the given rows.  this function will take care of doing the update or the insert correctly based on whether this is a new row or not.
 **	if the value for a node in the array is a 0 or is null that auto adjust entry will be deleted.
 **	
 **	parameters	:	s_pt_parcel_auto_adjust	:	(as_adj[]) the parcel auto adjust rows to insert/update/delete.
 **						ref string							:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel auto adjust was edited successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi
		rows

////
////	loop over the auto adjust rows and insert / update as necessary.
////
			maxi = upperbound( as_adj[] )
			
			for i = 1 to maxi
				////
				////	validate.
				////
					if isnull( as_adj[i].parcel_id ) then
						a_msg = "'parcel' is a required field.  one of the parcel / adjustment relationships to update is missing a parcel.  if you continue to receive this error contact your internal " + &
							"powerplant support or the powerplan property tax support desk."
						return false
					end if
					
					if isnull( as_adj[i].property_tax_adjust_id ) then
						a_msg = "'property tax adjustment' is a required field.  one of the parcel / adjustment relationships to update is missing an adjustment.  please check row " + string ( i ) + " to " + &
							"ensure a property tax adjustment is selected."
						return false
					end if
				
				////
				////	ensure this isn't a reserved adjustment.
				////
					choose case as_adj[i].property_tax_adjust_id
						case	-100, 1, 99, 100
							a_msg = "one or more of the parcel / adjustment relationships to update is using a reserved 'property tax adjustment'.  please check row " + string ( i ) + " to " + &
								"ensure that the following adjustments are not used: 'depreciation floor adjustment', 'manual transfer', 'preallo manual transfer', or 'spread by rules allocation'."
							return false
					end choose
				
				////
				////	are we deleting?  if so delete and skip the rest of the loop (which does the insert).
				////
					if isnull( as_adj[i].auto_adjust_percent ) or as_adj[i].auto_adjust_percent = 0 then
						delete from pt_parcel_auto_adjust padj
							where	padj.parcel_id = :as_adj[i].parcel_id
								and	padj.property_tax_adjust_id = :as_adj[i].property_tax_adjust_id;
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while deleting an existing parcel / adjustment relationship.  if you continue to receive this error contact your internal powerplant support " + &
								"or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
						
						continue
					end if
				
				////
				////	do we insert or update.
				////
					setnull( rows )
					select		count('x')
					into		:rows
					from		pt_parcel_auto_adjust padj
					where	padj.parcel_id = :as_adj[i].parcel_id
						and	padj.property_tax_adjust_id= :as_adj[i].property_tax_adjust_id;
					
					if isnull( rows ) or rows = 0 then
						insert into pt_parcel_auto_adjust ( parcel_id, property_tax_adjust_id, auto_adjust_percent )
							values ( :as_adj[i].parcel_id, :as_adj[i].property_tax_adjust_id, trim( :as_adj[i].auto_adjust_percent ) );
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while inserting a new parcel / adjustment relationship.  if you continue to receive this error contact your internal powerplant support or " + &
								"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					else
						update	pt_parcel_auto_adjust padj
						set		padj.auto_adjust_percent = trim( :as_adj[i].auto_adjust_percent )
						where	padj.parcel_id = :as_adj[i].parcel_id
							and	padj.property_tax_adjust_id = :as_adj[i].property_tax_adjust_id;
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while updating an existing parcel / adjustment relationship.  if you continue to receive this error contact your internal powerplant " + &
								"support or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					end if
			next

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parcelgeographyedit (s_pt_parcel_geography as_geo[], ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelgeographyedit()
 **	
 **	adds, updates or deletes the parcel geography table for the given rows.  this function will take care of doing the update or the insert correctly based on whether this is a new row or not.
 **	if the value for a node in the array is an empty string or is null that geography type / parcel will be deleted.
 **	
 **	parameters	:	s_pt_parcel_geography	:	(as_geo[]) the parcel geography rows to insert/update/delete.
 **						ref string						:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel geography was edited successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the of_parcelgeographyeditfromimport() function as both functions are used to update the
 **							pt_parcel_geography table.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi
		rows

////
////	loop over the geography rows and insert / update as necessary.
////
			maxi = upperbound( as_geo[] )
			
			for i = 1 to maxi
				////
				////	validate.
				////
					if isnull( as_geo[i].parcel_id ) then
						a_msg = "'parcel' is a required field.  one of the parcel / geography relationships to update is missing a parcel.  if you continue to receive this error contact your internal " + &
							"powerplant support or the powerplan property tax support desk."
						return false
					end if
					
					if isnull( as_geo[i].geography_type_id ) then
						a_msg = "'geography type' is a required field.  one of the parcel / geography relationships to update is missing a geography type.  please check row " + string ( i ) + " to " + &
							"ensure a geography type is selected."
						return false
					end if
				
				////
				////	are we deleting?  if so delete and skip the rest of the loop (which does the insert).
				////
					if isnull( as_geo[i].value ) or trim( as_geo[i].value ) = "" then
						delete from pt_parcel_geography geo
							where	geo.parcel_id = :as_geo[i].parcel_id
								and	geo.geography_type_id = :as_geo[i].geography_type_id;
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while deleting an existing parcel / geography relationship.  if you continue to receive this error contact your internal powerplant support " + &
								"or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
						
						continue
					end if
				
				////
				////	do we insert or update.
				////
					setnull( rows )
					select		count('x')
					into		:rows
					from		pt_parcel_geography geo
					where	geo.parcel_id = :as_geo[i].parcel_id
						and	geo.geography_type_id = :as_geo[i].geography_type_id;
					
					if isnull( rows ) or rows = 0 then
						insert into pt_parcel_geography ( parcel_id, geography_type_id, value )
							values ( :as_geo[i].parcel_id, :as_geo[i].geography_type_id, trim( :as_geo[i].value ) );
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while inserting a new parcel / geography relationship.  if you continue to receive this error contact your internal powerplant support or " + &
								"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					else
						update	pt_parcel_geography geo
						set		geo.value = trim( :as_geo[i].value )
						where	geo.parcel_id = :as_geo[i].parcel_id
							and	geo.geography_type_id = :as_geo[i].geography_type_id;
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while updating an existing parcel / geography relationship.  if you continue to receive this error contact your internal powerplant " + &
								"support or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					end if
			next

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_assessmentdelete ( a_prcl_id,  a_ag_id,  a_case_id, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_assessmentdelete()
 **	
 **	deletes one existing assessment.  the function will also update the case calc and case calc authority tables in real time.
 **	
 **	parameters	:		:	(a_prcl_id)  with a_ag_id the assessment to delete.
 **							:	(a_ag_id)  with a_prcl_id the assessment to delete.
 **							:	(a_case_id) the case for which the assessments should be deleted.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the assessments were deleted successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		prcl_id[], ag_id[]

////
////	call the delete function taking in arrays to do the delete - this is just a case of 1 node in the arrays.
////
			prcl_id[1] = a_prcl_id
			ag_id[1] = a_ag_id
			
			return this.of_assessmentdelete( prcl_id[], ag_id[], a_case_id, a_msg )
end function

public function boolean of_assessmentedit (s_pt_parcel_assessment as_asmt,  a_case_id, boolean a_update_date, boolean a_update_assessor, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_assessmentedit()
 **	
 **	edits one existing assessment.  if a passed in assessment already exists the assessment will be updated; if an assessment does not already exist it will be inserted.  this function will also
 **	update the case calc and case calc authority tables in real time.  
 **	
 **	parameters	:	s_pt_parcel_assessment		:	(as_asmt) the assessment values for the update.  the case_id field in this structure is ignored.
 **							:	(a_case_id) the case for which the assessment should be edited.
 **						boolean							:	(a_update_date) whether the assessment date should be updated at the same time from the value in the as_asmt[] array.  if this parm
 **																is false any data in the as_asmt[].assessment_date field is ignored and pt_parcel_assessment.assessment_date is left untouched.
 **						boolean							:	(a_update_assessor) whether the assessor id should be updated at the same time from the value in the as_asmt[] array.  if this parm is
 **																false any data in the as_asmt[].assessor_id field is ignored and pt_parcel_assessment.assessor_id is left untouched.
 **						ref string							:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the assessment was updated successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	misc objects.
 */
	s_pt_parcel_assessment s_asmt[]

////
////	call the edit function taking in arrays to do the edit - this is just a case of 1 node in the arrays.
////
			s_asmt[1] = as_asmt
			
			return this.of_assessmentedit( s_asmt[], a_case_id, a_update_date, a_update_assessor, a_msg )
end function

public function boolean of_parcelhistoryedit (ref s_pt_parcel_history as_hist[], ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelhistoryedit()
 **	
 **	edits the parcel history table for the given rows.  editing means inserts or updates as necessary.  this function will take care of doing the update or the insert correctly based on whether
 **	the array node has an event_id defined or not.  an event_id means the row will be updated.  otherwise it will be an insert.  for inserts, the just inserted event_id will be passed back
 **	via the structure.
 **	
 **	parameters	:	ref s_pt_parcel_history	:	(as_hist[]) the parcel history rows to insert/update.  blank event_id's indicates an insert, a populated event_id indicates an update.  if it's an
 **															insert, the new event_id will be populated in the event_id column of the structure and passed back.
 **						ref string						:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel history was edited successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi
		evnt_id[]

////
////	loop over the history rows and insert / update as necessary.
////
			maxi = upperbound( as_hist[] )
			
			for i = 1 to maxi
				////
				////	validate.
				////
					if isnull( as_hist[i].parcel_id ) then
						a_msg = "'parcel' is a required field.  one of the parcel events to update is missing a parcel.  if you continue to receive this error contact your internal powerplant support or " + &
							"the powerplan property tax support desk."
						return false
					end if
					
					if isnull( as_hist[i].event_date ) then
						a_msg = "'event date' is a required field.  one of the parcel events to update is missing an event date.  please check row " + string ( i ) + " to ensure an event date is entered."
						return false
					end if
					
					if isnull( as_hist[i].notes ) or trim( as_hist[i].notes ) = "" then
						a_msg = "'notes' is a required field.  one of the parcel events to update is missing event notes.  please check row " + string ( i ) + " to ensure event notes are entered."
						return false
					end if
					
					if len( as_hist[i].notes ) > 4000 then
						a_msg = "the event 'notes' field is limited to 4000 characters.  please edit row " + string( i ) + " as the event notes on that row are greater than 4000 characters."
						return false
					end if
				
				////
				////	do we insert or update.  if we're inserting get the next in line event id and store it in our event id array.  we'll wait until the very end to update the event id on the structure in
				////	case we encounter an error midway through the process.  if we're updating, just set the event id to the event id current in the structure.
				////
					//if the passed-in event id is null, check to see if the given parcel/date/notes already exist in the database.  if so, we don't need to do anything since the record already exists.
					setnull( evnt_id[i] )
					
					if isnull( as_hist[i].event_id ) then
						select		event_id
						into		:evnt_id[i]
						from		pt_parcel_history
						where	parcel_id = :as_hist[i].parcel_id
						and		event_date = :as_hist[i].event_date
						and		upper( trim( notes ) ) = upper( trim( :as_hist[i].notes ) );
						
						if not isnull( evnt_id[i] ) then continue
					else
						evnt_id[i] = as_hist[i].event_id
					end if
						
					//insert or update
					if isnull( evnt_id[i] ) then
						select		nvl( max( event_id ), 0 ) + 1
						into		:evnt_id[i]
						from		pt_parcel_history
						where	parcel_id = :as_hist[i].parcel_id;
						
						insert into pt_parcel_history ( parcel_id, event_id, event_date, notes )
							values ( :as_hist[i].parcel_id, :evnt_id[i], :as_hist[i].event_date, trim( :as_hist[i].notes ) );
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while inserting a new parcel event.  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
								"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					else
						update	pt_parcel_history phist
						set			phist.event_date = :as_hist[i].event_date,
									phist.notes = trim( :as_hist[i].notes )
						where	phist.parcel_id = :as_hist[i].parcel_id
							and	phist.event_id = :as_hist[i].event_id;
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while updating an existing parcel event.  if you continue to receive this error contact your internal powerplant support or the " + &
								"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					end if
			next

////
////	we're in the clear for errors so populate the event id on the passed in structure.
////
			for i = 1 to maxi
				as_hist[i].event_id = evnt_id[i]
			next

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parcelhistorydelete ( a_parcel_id,  a_event_id, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelhistorydelete()
 **	
 **	deletes the passed in event for the passed in parcel from the parcel history table.  passing in a null event id means all events for the given parcel should be deleted.
 **	
 **	parameters	:		:	(a_parcel_id) the parcel for which the event should be deleted.
 **							:	(a_event_id) the event which should be deleted.  a null value means all events should be deleted.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel history was deleted successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

////
////	validate.
////
			if isnull( a_parcel_id ) then
				a_msg = "an internal error occurred while attempting to delete parcel history.  no parcel was specified as the parcel from which to delete events.  if you continue to receive this " + &
					"error please contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if

////
////	do the delete.  if the event id is null, delete all events for this parcel.  otherwise, just delete the specified event.
////
			if isnull( a_event_id ) then
				delete from pt_parcel_history phist
					where	phist.parcel_id = :a_parcel_id;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "an error occurred while deleting all events for the selected parcel.  if you continue to receive this error please contact your internal powerplant support or the " + &
						"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
			else
				delete from pt_parcel_history phist
					where	phist.parcel_id = :a_parcel_id
						and	phist.event_id = :a_event_id;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "an error occurred while deleting the specified event for the selected parcel.  if you continue to receive this error please contact your internal powerplant support or the " + &
						"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parcelappraisaldelete ( a_parcel_id,  a_appraisal_id,  a_assessment_group_id, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelappraisaldelete()
 **	
 **	deletes the passed in appraisal for the passed in parcel from the parcel appraisal table.
 **	
 **	parameters	:		:	(a_parcel_id) the parcel for which the appraisal should be deleted.
 **							:	(a_appraisal_id) the appraisal that should be deleted.
 **							:	(a_assessment_group_id) the assessment group appraisal that should be deleted.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel appraisal was deleted successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

////
////	validate.
////
			if isnull( a_parcel_id ) then
				a_msg = "an internal error occurred while attempting to delete parcel appraisals.  no parcel was specified as the parcel from which to delete an appraisal.  if you continue to receive " + &
					"this error please contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if
			
			if isnull( a_appraisal_id ) then
				a_msg = "an internal error occurred while attempting to delete parcel appraisals.  no appraisal identifier was specified as the appraisal to delete.  if you continue to receive this error " + &
					"please contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if
			
			if isnull( a_assessment_group_id ) then
				a_msg = "an internal error occurred while attempting to delete parcel appraisals.  no assessment group was specified for the appraisal to delete.  if you continue to receive this error " + &
					"please contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if

////
////	do the delete.  if the assessment group id is null, delete all appraisals for this parcel / appraisal.  otherwise, just delete the specified appraisal.
////
			delete from pt_parcel_appraisal pappr
				where	pappr.parcel_id = :a_parcel_id
					and	pappr.appraisal_id = :a_appraisal_id
					and	pappr.assessment_group_id = :a_assessment_group_id;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "an error occurred while deleting the specified value for the selected parcel / appraisal.  if you continue to receive this error please contact your internal powerplant " + &
					"support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parcelappraisaledit (ref s_pt_parcel_appraisal as_appr[], ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelappraisaledit()
 **	
 **	edits the parcel appraisal table for the given rows.  editing means inserts or updates as necessary.  this function will take care of doing the update or the insert correctly based on whether
 **	the array node has an appraisal_id defined or not.  an appraisal_id means the row will be updated.  otherwise it will be an insert.  for inserts, the just inserted appraisal_id will be passed
 **	back via the structure.
 **	
 **	parameters	:	ref s_pt_parcel_appraisal	:	(as_appr[]) the parcel appraisal rows to insert/update.  blank appraisal_id's indicates an insert, a populated appraisal_id indicates an update.
 **																if it's an insert, the new appraisal_id will be populated in the appraisal_id column of the structure and passed back.
 **						ref string							:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel appraisal was edited successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi
		appr_id[]

////
////	loop over the appraisal rows and insert / update as necessary.
////
			maxi = upperbound( as_appr[] )
			
			for i = 1 to maxi
				////
				////	validate.
				////
					if isnull( as_appr[i].parcel_id ) then
						a_msg = "'parcel' is a required field.  one of the parcel appraisals to update is missing a parcel.  if you continue to receive this error contact your internal powerplant support or " + &
							"the powerplan property tax support desk."
						return false
					end if
					
					if isnull( as_appr[i].assessment_group_id ) then
						a_msg = "'assessment group' is a required field.  one of the parcel appraisals to update is missing an assessment group.  if you continue to receive this error contact your " + &
							"internal powerplant support or the powerplan property tax support desk."
						return false
					end if
					
					if isnull( as_appr[i].appraisal_value ) then
						a_msg = "'appraisal value' is a required field.  one of the parcel appraisals to update is missing a value.  please ensure all appraisals have a corresponding value."
						return false
					end if
					
					if not isnull( as_appr[i].notes ) then
						if len( as_appr[i].notes ) > 4000 then
							a_msg = "the appraisal 'notes' field is limited to 4000 characters.  one of the appraisals to update has notes on that are greater than 4000 characters.  please correct any " + &
								"appraisal notes which are over 4000 characters."
							return false
						end if
					end if
				
				////
				////	do we insert or update.  if we're inserting get the next in line appraisal id and store it in our appraisal id array.  we'll wait until the very end to update the appraisal id on the
				////	structure in case we encounter an error midway through the process.  if we're updating, just set the appraisal id to the appraisal id current in the structure.
				////
					if isnull( as_appr[i].appraisal_id ) then
						select		nvl( max( appraisal_id ), 0 ) + 1
						into		:appr_id[i]
						from		pt_parcel_appraisal
						where	parcel_id = :as_appr[i].parcel_id;
						
						insert into pt_parcel_appraisal ( parcel_id, assessment_group_id, appraisal_id, appraisal_date, appraiser_id, appraisal_value, notes )
							values (	:as_appr[i].parcel_id, :as_appr[i].assessment_group_id, :appr_id[i], :as_appr[i].appraisal_date, :as_appr[i].appraiser_id, :as_appr[i].appraisal_value,
										trim( :as_appr[i].notes ) );
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while inserting a new parcel appraisal.  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
								"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					else
						appr_id[i] = as_appr[i].appraisal_id
						
						update	pt_parcel_appraisal pappr
						set		pappr.appraisal_date = :as_appr[i].appraisal_date,
									pappr.appraiser_id = :as_appr[i].appraiser_id,
									pappr.appraisal_value = :as_appr[i].appraisal_value,
									pappr.notes = trim( :as_appr[i].notes )
						where	pappr.parcel_id = :as_appr[i].parcel_id
							and	pappr.assessment_group_id = :as_appr[i].assessment_group_id
							and	pappr.appraisal_id = :as_appr[i].appraisal_id;
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while updating an existing parcel appraisal.  if you continue to receive this error contact your internal powerplant support or the " + &
								"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					end if
			next

////
////	we're in the clear for errors so populate the event id on the passed in structure.
////
			for i = 1 to maxi
				as_appr[i].appraisal_id = appr_id[i]
			next

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_assessmentcalcvalues ( a_case_id, string a_where_clause,  a_fixed_field, boolean a_recalc_cc, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_assessmentcalcvalues()
 **	
 **	calculates two of the three "value" fields on the assessment table (i.e., assessment, equalized value, taxable_value) for given parcels and assessment groups.  one of the "value" fields is
 **	identified as the "fixed" value and the other two values are calculated from it.  at the end, this function also updates the case calc and case calc authority tables for the changes.
 **	
 **	parameters	:		:	(a_case_id) the case for which we're calculating assessment values.
 **						string			:	(a_where_clause) an sql where clause that can be used to limit the parcels / assessment groups for this case that will have their values calculated.  the
 **											where clause will be appended to sql statement(s) directly after an "and" and, thus, should not start with a "where" or an "and".  the table in the
 **											"from" clause of the sql statement(s) to which this where clause will be appended is:
 **											
 **												*	pt_parcel_assessment pasmt
 **												*	pt_temp_parcels temp_prcl (which you must populate ahead of time and you must use a join to one of the tables above on parcel_id so the system
 **													knows to include this table in the from clause)
 **											
 **											important: be sure to use the alias listed above in your where clause (and not the actual table name)
 **							:	(a_fixed_field) a constant identifying the field that is known - i.e., the field that all other fields will be calculated from.  the constants can be found as
 **											instance variables for this object starting with i_fixed_field_...
 **						boolean		:	(a_recalc_cc) whether pt case calc and pt case calc authority should be recalcualted.  this should always be set to true unless you are calling this function
 **											from a process that has it's own call to recalculate these tables later.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the assessment values were calculated successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi
		unique_ptco_id, cc_ptco_id
	string	sqls, rtn_str, eqfctr_sqls
	string	unique_st_id, cc_st_id
	boolean	temp_prcl_used, is_unique_ptco, is_unique_st

/*
 *	misc objects.
 */
	uo_ds_top ds_grps

////
////	don't error.
////
			if a_fixed_field <> i_fixed_field_assessment and a_fixed_field <> i_fixed_field_equalized_value and a_fixed_field <> i_fixed_field_taxable_value then
				a_msg = "invalid 'fixed' value specified when calculating assessed values for the specified parcels.  if you continue to receive this error contact your internal powerplant support " + &
					"or the powerplan property tax support desk."
				return false
			end if

////
////	make sure we have a case.
////
			if isnull( a_case_id ) then
				a_msg = "no case specified for populating the 'pt case calc' table."
				return false
			end if

////
////	check to see if temp parcel is used in the where clause.
////
			temp_prcl_used = false
			if pos( a_where_clause, "temp_prcl." ) > 0 then temp_prcl_used = true

////
////	if the where clause is blank make it 5=5.
////
			if isnull( a_where_clause ) or trim( a_where_clause ) = "" then a_where_clause = "5=5"

////
////	validate update eligibility.  populate temp parcels ag before doing so as the function uses temp parcels ag to know what we plan on updating.
////
			delete from pt_temp_parcels_asmt_gp;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while deleting prior parcel / assessment group combinations stored off prior to validation (during the recalc).  if you continue to receive this " + &
					"error contact your internal powerplant support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if
			
			sqls =					"	insert into pt_temp_parcels_asmt_gp ( parcel_id, assessment_group_id ) " + "~r~n" + &
									"		select		pasmt.parcel_id, pasmt.assessment_group_id "+  "~r~n" + &
									"		from		pt_parcel_assessment pasmt"
			
			if temp_prcl_used then
				sqls = sqls + ", " + "~r~n"
				sqls = sqls +	"					pt_temp_parcels temp_prcl " + "~r~n"
			else
				sqls = sqls + " " + "~r~n"
			end if
			
			sqls =sqls +			"		where	pasmt.case_id = " + string( a_case_id ) + " " + "~r~n" + &
									"			and	" + a_where_clause + "~r~n"
			
			if temp_prcl_used then
				sqls = sqls +	"			and	pasmt.parcel_id = temp_prcl.parcel_id "
			end if
			
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while storing off parcel / assessment group combinations for validation (during the recalc).  if you continue to receive this error contact your " + &
					"internal powerplant support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				
				delete from pt_temp_parcels_asmt_gp;
				
				return false
			end if
			
			if not this.of_assessmentupdateeligibility( a_case_id, true, a_msg ) then return false
			
			delete from pt_temp_parcels_asmt_gp;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while deleting parcel / assessment group combinations stored for validation (during the recalc).  if you continue to receive this error contact " + &
					"your internal powerplant support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if
			
////
////	get the distinct prop tax company and state combinations for the parcels we're processing.
////
			sqls =					"	select		distinct prcl.prop_tax_company_id prop_tax_company_id, " + "~r~n" + &
									"				prcl.state_id state_id " + "~r~n" + &
									"	from		pt_parcel_assessment pasmt, " + "~r~n"
			
			if temp_prcl_used then
				sqls = sqls +	"				pt_temp_parcels temp_prcl, " + "~r~n"
			end if
			
			sqls = sqls +		"				pt_parcel prcl " + "~r~n" + &
									"	where	pasmt.case_id = " + string( a_case_id ) + " " + "~r~n" + &
									"		and	pasmt.parcel_id = prcl.parcel_id " + "~r~n"
			
			if temp_prcl_used then
				sqls =sqls +		"		and	pasmt.parcel_id = temp_prcl.parcel_id " + "~r~n" + &
									"		and	prcl.parcel_id = temp_prcl.parcel_id " + "~r~n"
			end if
			
			sqls = sqls +		"		and	5=5 "
			
			sqls = f_replace_string( sqls, "5=5", a_where_clause, "all" )
			
			ds_grps = create uo_ds_top
			rtn_str = f_create_dynamic_ds( ds_grps, "grid", sqls, sqlca, true )
			
			if rtn_str <> "ok" then
				a_msg = "an error occurred while determining the prop tax companies and states for the parcels being re-calced.  if you continue to receive this error contact your internal " + &
					"powerplant support or the powerplan property tax support desk.  the specific error returned was: " + rtn_str
				destroy ds_grps
				return false
			end if
			
			maxi = ds_grps.rowcount()
			
			is_unique_ptco = true
			is_unique_st = true
			
			for i = 1 to maxi
			////
			////	see if we're dealing with one unique prop tax company and/or one unique state.
			////
					if i = 1 then
						unique_ptco_id = ds_grps.getitemnumber( i, 1 )
						unique_st_id = ds_grps.getitemstring( i, 2 )
					else
						if ds_grps.getitemnumber( i, 1 ) <> unique_ptco_id then is_unique_ptco = false
						if ds_grps.getitemstring( i, 2 ) <> unique_st_id then is_unique_st = false
					end if
			next
			
			destroy ds_grps
			
			if not is_unique_ptco then setnull( unique_ptco_id )
			if not is_unique_st then setnull( unique_st_id )
			
////
////	based on the fixed field, calculate the other fields:
////		- assessment is usually our fixed field.  if it's not, reverse the calculations described for equalized value or taxable value.
////		- equalized value is the assessment times the equalization factor, which may be by district or county.  the method is stored on the assessment group.
////		- taxable value is the equalized value times the taxable value rate, which may be by county or uniform.  also, tiered taxable value rates may be used, in which a different rate
////				is applied to the piece of the assessment under a given threshold.  if tiered rates are used, the taxable value rates (regular and under threshold) are applied to the assessment;
////				then the equalization factor is applied.
////
			//regardless of the fixed field, we're going to store the equalization factor on the assessment table.  it will make our calculations easier.
			eqfctr_sqls =						"	update	pt_parcel_assessment pasmt " + "~r~n" + &
													"	set			pasmt.temp_eq_factor = nvl( " + "~r~n" + &
													"					(	select		case " + "~r~n" + &
													"										when nvl( agc.eq_factor_type_id, 0 ) = " + string( i_eq_factor_none ) + " then 1 " + "~r~n" + &
													"										when nvl( agc.eq_factor_type_id, 0 ) = " + string( i_eq_factor_county ) + " then nvl( ceview.equalization_factor, 1 ) " + "~r~n" + &
													"										when nvl( agc.eq_factor_type_id, 0 ) = " + string( i_eq_factor_district ) + " then nvl( tdeview.equalization_factor, 1 ) " + "~r~n" + &
													"										else 1 " + "~r~n" + &
													"									end " + "~r~n" + &
													"						from		pt_parcel prcl, " + "~r~n" + &
													"									prop_tax_district td, " + "~r~n" + &
													"									pt_assessment_group_case agc, " + "~r~n" + &
													"									(	select		state_id state_id, " + "~r~n" + &
													"													county_id county_id, " + "~r~n" + &
													"													equalization_factor equalization_factor " + "~r~n" + &
													"										from		pt_county_equalization " + "~r~n" + &
													"										where	case_id = " + string( a_case_id ) + " " + "~r~n" + &
													"									) ceview, " + "~r~n" + &
													"									(	select		tax_district_id tax_district_id, " + "~r~n" + &
													"													equalization_factor equalization_factor " + "~r~n" + &
													"										from		pt_district_equalization " + "~r~n" + &
													"										where	case_id = " + string( a_case_id ) + " " + "~r~n" + &
													"									) tdeview " + "~r~n" + &
													"						where	pasmt.parcel_id = prcl.parcel_id " + "~r~n" + &
													"							and	prcl.tax_district_id = td.tax_district_id " + "~r~n" + &
													"							and	pasmt.assessment_group_id = agc.assessment_group_id " + "~r~n" + &
													"							and	pasmt.case_id = agc.case_id " + "~r~n" + &
													"							and	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
													"							and	prcl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
													"							and	prcl.state_id = agc.state_id " + "~r~n" + &
													"							and	td.state_id = ceview.state_id (+) " + "~r~n" + &
													"							and	td.county_id = ceview.county_id (+) " + "~r~n" + &
													"							and	prcl.tax_district_id = tdeview.tax_district_id (+) " + "~r~n" + &
													"					), 1 ) " + "~r~n" + &
													"	where	pasmt.case_id = " + string( a_case_id ) + " " + "~r~n"
				
			if temp_prcl_used then
				eqfctr_sqls = eqfctr_sqls +	"		and	( pasmt.parcel_id, pasmt.assessment_group_id ) in " + "~r~n" + &
													"					(	select		in_pasmt.parcel_id, in_pasmt.assessment_group_id " + "~r~n" + &
													"						from		pt_parcel_assessment in_pasmt, " + "~r~n" + &
													"									pt_temp_parcels temp_prcl " + "~r~n" + &
													"						where	in_pasmt.case_id = " + string( a_case_id ) + " " + "~r~n" + &
													"							and	in_pasmt.parcel_id = temp_prcl.parcel_id " + "~r~n" + &
													"							and	5=5 " + "~r~n" + &
													"					) "
			else
				eqfctr_sqls = eqfctr_sqls +	"		and	5=5 "
			end if

			choose case a_fixed_field
				case	i_fixed_field_assessment
					//set the equalized value and taxable value
					sqls =						"	update	pt_parcel_assessment pasmt " + "~r~n" + &
												"	set			pasmt.equalized_value = round( pasmt.assessment * pasmt.temp_eq_factor, 2 ), " + "~r~n" + &
												"				pasmt.taxable_value = nvl( nvl( " + "~r~n" + &
												"								(	select		case " + "~r~n" + &
												"													when nvl( pasmt.use_tiered_rates_yn, 0 ) = 0 then " + "~r~n" + &
												"														round( round( pasmt.assessment * pasmt.temp_eq_factor, 2 ) * tvrc.taxable_value_rate, 2 ) " + "~r~n" + &
												"													when nvl( tvrc.threshold_amount, 0 ) = 0 then " + "~r~n" + &
												"														round( round( pasmt.assessment * pasmt.temp_eq_factor, 2 ) * tvrc.taxable_value_rate, 2 ) " + "~r~n" + &
												"													when pasmt.assessment > tvrc.threshold_amount then " + "~r~n" + &
												"														round( ( round( tvrc.threshold_amount * tvrc.rate_under_threshold, 2 ) + round( ( pasmt.assessment - tvrc.threshold_amount ) * tvrc.taxable_value_rate, 2 ) ) * pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"													else " + "~r~n" + &
												"														round( round( pasmt.assessment * tvrc.rate_under_threshold, 2 ) * pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"												end " + "~r~n" + &
												"									from		pt_parcel prcl, " + "~r~n" + &
												"												prop_tax_district td, " + "~r~n" + & 
												"												pt_assessment_group_case agc, " + "~r~n" + &
												"												pt_taxable_value_rate_county tvrc " + "~r~n" + &
												"									where	pasmt.parcel_id = prcl.parcel_id " + "~r~n" + &
												"										and	prcl.tax_district_id = td.tax_district_id " + "~r~n" + &
												"										and	pasmt.assessment_group_id = agc.assessment_group_id " + "~r~n" + &
												"										and	pasmt.case_id = agc.case_id " + "~r~n" + &
												"										and	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"										and	prcl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
												"										and	prcl.state_id = agc.state_id " + "~r~n" + &
												"										and	agc.taxable_value_rate_id = tvrc.pt_rate_id " + "~r~n" + &
												"										and	agc.case_id = tvrc.case_id " + "~r~n" + &
												"										and	tvrc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"										and	td.state_id = tvrc.state_id " + "~r~n" + &
												"										and	td.county_id = tvrc.county_id " + "~r~n" + &
												"								), " + "~r~n" + &
												"								(	select		case " + "~r~n" + &
												"													when nvl( pasmt.use_tiered_rates_yn, 0 ) = 0 then " + "~r~n" + &
												"														round( round( pasmt.assessment * pasmt.temp_eq_factor, 2 ) * tvr.taxable_value_rate, 2 ) " + "~r~n" + &
												"													when nvl( tvr.threshold_amount, 0 ) = 0 then " + "~r~n" + &
												"														round( round( pasmt.assessment * pasmt.temp_eq_factor, 2 ) * tvr.taxable_value_rate, 2 ) " + "~r~n" + &
												"													when pasmt.assessment > tvr.threshold_amount then " + "~r~n" + &
												"														round( ( round( tvr.threshold_amount * tvr.rate_under_threshold, 2 ) + round( ( pasmt.assessment - tvr.threshold_amount ) * tvr.taxable_value_rate, 2 ) ) * pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"													else " + "~r~n" + &
												"														round( round( pasmt.assessment * tvr.rate_under_threshold, 2 ) * pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"												end " + "~r~n" + &
												"									from		pt_parcel prcl, " + "~r~n" + &
												"												pt_assessment_group_case agc, " + "~r~n" + &
												"												pt_taxable_value_rate tvr " + "~r~n" + &
												"									where	pasmt.parcel_id = prcl.parcel_id " + "~r~n" + &
												"										and	pasmt.assessment_group_id = agc.assessment_group_id " + "~r~n" + &
												"										and	pasmt.case_id = agc.case_id " + "~r~n" + &
												"										and	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"										and	prcl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
												"										and	prcl.state_id = agc.state_id " + "~r~n" + &
												"										and	agc.taxable_value_rate_id = tvr.pt_rate_id (+) " + "~r~n" + &
												"										and	agc.case_id = tvr.case_id (+) " + "~r~n" + &
												"								) ), round( pasmt.assessment * pasmt.temp_eq_factor, 2 ) ) " + "~r~n" + &
												"	where	pasmt.case_id = " + string( a_case_id ) + " " + "~r~n"
						
						if temp_prcl_used then
							sqls = sqls +	"		and	( pasmt.parcel_id, pasmt.assessment_group_id ) in " + "~r~n" + &
												"					(	select		in_pasmt.parcel_id, in_pasmt.assessment_group_id " + "~r~n" + &
												"						from		pt_parcel_assessment in_pasmt, " + "~r~n" + &
												"									pt_temp_parcels temp_prcl " + "~r~n" + &
												"						where	in_pasmt.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"							and	in_pasmt.parcel_id = temp_prcl.parcel_id " + "~r~n" + &
												"							and	5=5 " + "~r~n" + &
												"					) "
						else
							sqls = sqls +	"		and	5=5 "
						end if
				case	i_fixed_field_equalized_value
					//set the assessment and taxable value
					sqls =						"	update	pt_parcel_assessment pasmt " + "~r~n" + &
												"	set			pasmt.assessment = round( pasmt.equalized_value / pasmt.temp_eq_factor, 2 ), " + "~r~n" + &
												"				pasmt.taxable_value = nvl( nvl( " + "~r~n" + &
												"								(	select		case " + "~r~n" + &
												"													when nvl( pasmt.use_tiered_rates_yn, 0 ) = 0 then " + "~r~n" + &
												"														round( pasmt.equalized_value * tvrc.taxable_value_rate, 2 ) " + "~r~n" + &
												"													when nvl( tvrc.threshold_amount, 0 ) = 0 then " + "~r~n" + &
												"														round( pasmt.equalized_value * tvrc.taxable_value_rate, 2 ) " + "~r~n" + &
												"													when round( pasmt.equalized_value / pasmt.temp_eq_factor, 2 ) > tvrc.threshold_amount then " + "~r~n" + &
												"														round( ( round( tvrc.threshold_amount * tvrc.rate_under_threshold, 2 ) + round( ( round( pasmt.equalized_value / pasmt.temp_eq_factor, 2 ) - tvrc.threshold_amount ) * tvrc.taxable_value_rate, 2 ) ) * pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"													else " + "~r~n" + &
												"														round( pasmt.equalized_value * tvrc.rate_under_threshold, 2 ) " + "~r~n" + &
												"												end " + "~r~n" + &
												"									from		pt_parcel prcl, " + "~r~n" + &
												"												prop_tax_district td, " + "~r~n" + & 
												"												pt_assessment_group_case agc, " + "~r~n" + &
												"												pt_taxable_value_rate_county tvrc " + "~r~n" + &
												"									where	pasmt.parcel_id = prcl.parcel_id " + "~r~n" + &
												"										and	prcl.tax_district_id = td.tax_district_id " + "~r~n" + &
												"										and	pasmt.assessment_group_id = agc.assessment_group_id " + "~r~n" + &
												"										and	pasmt.case_id = agc.case_id " + "~r~n" + &
												"										and	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"										and	prcl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
												"										and	prcl.state_id = agc.state_id " + "~r~n" + &
												"										and	agc.taxable_value_rate_id = tvrc.pt_rate_id " + "~r~n" + &
												"										and	agc.case_id = tvrc.case_id " + "~r~n" + &
												"										and	tvrc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"										and	td.state_id = tvrc.state_id " + "~r~n" + &
												"										and	td.county_id = tvrc.county_id " + "~r~n" + &
												"								), " + "~r~n" + &
												"								(	select		case " + "~r~n" + &
												"													when nvl( pasmt.use_tiered_rates_yn, 0 ) = 0 then " + "~r~n" + &
												"														round( pasmt.equalized_value * tvr.taxable_value_rate, 2 ) " + "~r~n" + &
												"													when nvl( tvr.threshold_amount, 0 ) = 0 then " + "~r~n" + &
												"														round( pasmt.equalized_value * tvr.taxable_value_rate, 2 ) " + "~r~n" + &
												"													when round( pasmt.equalized_value / pasmt.temp_eq_factor, 2 ) > tvr.threshold_amount then " + "~r~n" + &
												"														round( ( round( tvr.threshold_amount * tvr.rate_under_threshold, 2 ) + round( ( round( pasmt.equalized_value / pasmt.temp_eq_factor, 2 ) - tvr.threshold_amount ) * tvr.taxable_value_rate, 2 ) ) * pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"													else " + "~r~n" + &
												"														round( pasmt.equalized_value * tvr.rate_under_threshold, 2 ) " + "~r~n" + &
												"												end " + "~r~n" + &
												"									from		pt_parcel prcl, " + "~r~n" + &
												"												pt_assessment_group_case agc, " + "~r~n" + &
												"												pt_taxable_value_rate tvr " + "~r~n" + &
												"									where	pasmt.parcel_id = prcl.parcel_id " + "~r~n" + &
												"										and	pasmt.assessment_group_id = agc.assessment_group_id " + "~r~n" + &
												"										and	pasmt.case_id = agc.case_id " + "~r~n" + &
												"										and	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"										and	prcl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
												"										and	prcl.state_id = agc.state_id " + "~r~n" + &
												"										and	agc.taxable_value_rate_id = tvr.pt_rate_id (+) " + "~r~n" + &
												"										and	agc.case_id = tvr.case_id (+) " + "~r~n" + &
												"								) ), pasmt.equalized_value ) " + "~r~n" + &
												"	where	pasmt.case_id = " + string( a_case_id ) + " " + "~r~n"
						
						if temp_prcl_used then
							sqls = sqls +	"		and	( pasmt.parcel_id, pasmt.assessment_group_id ) in " + "~r~n" + &
												"					(	select		in_pasmt.parcel_id, in_pasmt.assessment_group_id " + "~r~n" + &
												"						from		pt_parcel_assessment in_pasmt, " + "~r~n" + &
												"									pt_temp_parcels temp_prcl " + "~r~n" + &
												"						where	in_pasmt.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"							and	in_pasmt.parcel_id = temp_prcl.parcel_id " + "~r~n" + &
												"							and	5=5 " + "~r~n" + &
												"					) "
						else
							sqls = sqls +	"		and	5=5 "
						end if
				case	i_fixed_field_taxable_value
					//set the assessment and equalized value
					sqls =						"	update	pt_parcel_assessment pasmt " + "~r~n" + &
												"	set			pasmt.assessment = nvl( nvl( " + "~r~n" + &
												"								(	select		case " + "~r~n" + &
												"													when nvl( pasmt.use_tiered_rates_yn, 0 ) = 0 then " + "~r~n" + &
												"														round( round( pasmt.taxable_value / tvrc.taxable_value_rate, 2 ) / pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"													when nvl( tvrc.threshold_amount, 0 ) = 0 then " + "~r~n" + &
												"														round( round( pasmt.taxable_value / tvrc.taxable_value_rate, 2 ) / pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"													when pasmt.taxable_value > round( round( tvrc.threshold_amount * tvrc.rate_under_threshold, 2 ) * pasmt.temp_eq_factor, 2 ) then " + "~r~n" + &
												"														tvrc.threshold_amount + round( ( round( pasmt.taxable_value / pasmt.temp_eq_factor, 2 ) - round( tvrc.threshold_amount * tvrc.rate_under_threshold, 2 ) ) / tvrc.taxable_value_rate, 2 ) " + "~r~n" + &
												"													else " + "~r~n" + &
												"														round( round( pasmt.taxable_value / tvrc.rate_under_threshold, 2 ) / pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"												end " + "~r~n" + &
												"									from		pt_parcel prcl, " + "~r~n" + &
												"												prop_tax_district td, " + "~r~n" + & 
												"												pt_assessment_group_case agc, " + "~r~n" + &
												"												pt_taxable_value_rate_county tvrc " + "~r~n" + &
												"									where	pasmt.parcel_id = prcl.parcel_id " + "~r~n" + &
												"										and	prcl.tax_district_id = td.tax_district_id " + "~r~n" + &
												"										and	pasmt.assessment_group_id = agc.assessment_group_id " + "~r~n" + &
												"										and	pasmt.case_id = agc.case_id " + "~r~n" + &
												"										and	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"										and	prcl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
												"										and	prcl.state_id = agc.state_id " + "~r~n" + &
												"										and	agc.taxable_value_rate_id = tvrc.pt_rate_id " + "~r~n" + &
												"										and	agc.case_id = tvrc.case_id " + "~r~n" + &
												"										and	tvrc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"										and	td.state_id = tvrc.state_id " + "~r~n" + &
												"										and	td.county_id = tvrc.county_id " + "~r~n" + &
												"								), " + "~r~n" + &
												"								(	select		case " + "~r~n" + &
												"													when nvl( pasmt.use_tiered_rates_yn, 0 ) = 0 then " + "~r~n" + &
												"														round( round( pasmt.taxable_value / tvr.taxable_value_rate, 2 ) / pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"													when nvl( tvr.threshold_amount, 0 ) = 0 then " + "~r~n" + &
												"														round( round( pasmt.taxable_value / tvr.taxable_value_rate, 2 ) / pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"													when pasmt.taxable_value > round( round( tvr.threshold_amount * tvr.rate_under_threshold, 2 ) * pasmt.temp_eq_factor, 2 ) then " + "~r~n" + &
												"														tvr.threshold_amount + round( ( round( pasmt.taxable_value / pasmt.temp_eq_factor, 2 ) - round( tvr.threshold_amount * tvr.rate_under_threshold, 2 ) ) / tvr.taxable_value_rate, 2 ) " + "~r~n" + &
												"													else " + "~r~n" + &
												"														round( round( pasmt.taxable_value / tvr.rate_under_threshold, 2 ) / pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"												end " + "~r~n" + &
												"									from		pt_parcel prcl, " + "~r~n" + &
												"												pt_assessment_group_case agc, " + "~r~n" + &
												"												pt_taxable_value_rate tvr " + "~r~n" + &
												"									where	pasmt.parcel_id = prcl.parcel_id " + "~r~n" + &
												"										and	pasmt.assessment_group_id = agc.assessment_group_id " + "~r~n" + &
												"										and	pasmt.case_id = agc.case_id " + "~r~n" + &
												"										and	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"										and	prcl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
												"										and	prcl.state_id = agc.state_id " + "~r~n" + &
												"										and	agc.taxable_value_rate_id = tvr.pt_rate_id (+) " + "~r~n" + &
												"										and	agc.case_id = tvr.case_id (+) " + "~r~n" + &
												"								) ), round( pasmt.taxable_value / pasmt.temp_eq_factor, 2 ) ), " + "~r~n" + &
												"				pasmt.equalized_value = nvl( nvl( " + "~r~n" + &
												"								(	select		case " + "~r~n" + &
												"													when nvl( pasmt.use_tiered_rates_yn, 0 ) = 0 then " + "~r~n" + &
												"														round( pasmt.taxable_value / tvrc.taxable_value_rate, 2 ) " + "~r~n" + &
												"													when nvl( tvrc.threshold_amount, 0 ) = 0 then " + "~r~n" + &
												"														round( pasmt.taxable_value / tvrc.taxable_value_rate, 2 ) " + "~r~n" + &
												"													when pasmt.taxable_value > round( round( tvrc.threshold_amount * tvrc.rate_under_threshold, 2 ) * pasmt.temp_eq_factor, 2 ) then " + "~r~n" + &
												"														round( ( tvrc.threshold_amount + round( ( round( pasmt.taxable_value / pasmt.temp_eq_factor, 2 ) - round( tvrc.threshold_amount * tvrc.rate_under_threshold, 2 ) ) / tvrc.taxable_value_rate, 2 ) ) * pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"													else " + "~r~n" + &
												"														round( round( round( pasmt.taxable_value / tvrc.rate_under_threshold, 2 ) / pasmt.temp_eq_factor, 2 ) * pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"												end " + "~r~n" + &
												"									from		pt_parcel prcl, " + "~r~n" + &
												"												prop_tax_district td, " + "~r~n" + & 
												"												pt_assessment_group_case agc, " + "~r~n" + &
												"												pt_taxable_value_rate_county tvrc " + "~r~n" + &
												"									where	pasmt.parcel_id = prcl.parcel_id " + "~r~n" + &
												"										and	prcl.tax_district_id = td.tax_district_id " + "~r~n" + &
												"										and	pasmt.assessment_group_id = agc.assessment_group_id " + "~r~n" + &
												"										and	pasmt.case_id = agc.case_id " + "~r~n" + &
												"										and	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"										and	prcl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
												"										and	prcl.state_id = agc.state_id " + "~r~n" + &
												"										and	agc.taxable_value_rate_id = tvrc.pt_rate_id " + "~r~n" + &
												"										and	agc.case_id = tvrc.case_id " + "~r~n" + &
												"										and	tvrc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"										and	td.state_id = tvrc.state_id " + "~r~n" + &
												"										and	td.county_id = tvrc.county_id " + "~r~n" + &
												"								), " + "~r~n" + &
												"								(	select		case " + "~r~n" + &
												"													when nvl( pasmt.use_tiered_rates_yn, 0 ) = 0 then " + "~r~n" + &
												"														round( pasmt.taxable_value / tvr.taxable_value_rate, 2 ) " + "~r~n" + &
												"													when nvl( tvr.threshold_amount, 0 ) = 0 then " + "~r~n" + &
												"														round( pasmt.taxable_value / tvr.taxable_value_rate, 2 ) " + "~r~n" + &
												"													when pasmt.taxable_value > round( round( tvr.threshold_amount * tvr.rate_under_threshold, 2 ) * pasmt.temp_eq_factor, 2 ) then " + "~r~n" + &
												"														round( ( tvr.threshold_amount + round( ( round( pasmt.taxable_value / pasmt.temp_eq_factor, 2 ) - round( tvr.threshold_amount * tvr.rate_under_threshold, 2 ) ) / tvr.taxable_value_rate, 2 ) ) * pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"													else " + "~r~n" + &
												"														round( round( round( pasmt.taxable_value / tvr.rate_under_threshold, 2 ) / pasmt.temp_eq_factor, 2 ) * pasmt.temp_eq_factor, 2 ) " + "~r~n" + &
												"												end " + "~r~n" + &
												"									from		pt_parcel prcl, " + "~r~n" + &
												"												pt_assessment_group_case agc, " + "~r~n" + &
												"												pt_taxable_value_rate tvr " + "~r~n" + &
												"									where	pasmt.parcel_id = prcl.parcel_id " + "~r~n" + &
												"										and	pasmt.assessment_group_id = agc.assessment_group_id " + "~r~n" + &
												"										and	pasmt.case_id = agc.case_id " + "~r~n" + &
												"										and	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"										and	prcl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
												"										and	prcl.state_id = agc.state_id " + "~r~n" + &
												"										and	agc.taxable_value_rate_id = tvr.pt_rate_id (+) " + "~r~n" + &
												"										and	agc.case_id = tvr.case_id (+) " + "~r~n" + &
												"								) ), round( pasmt.taxable_value / pasmt.temp_eq_factor, 2 ) ) " + "~r~n" + &
												"	where	pasmt.case_id = " + string( a_case_id ) + " " + "~r~n"
						
						if temp_prcl_used then
							sqls = sqls +	"		and	( pasmt.parcel_id, pasmt.assessment_group_id ) in " + "~r~n" + &
												"					(	select		in_pasmt.parcel_id, in_pasmt.assessment_group_id " + "~r~n" + &
												"						from		pt_parcel_assessment in_pasmt, " + "~r~n" + &
												"									pt_temp_parcels temp_prcl " + "~r~n" + &
												"						where	in_pasmt.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"							and	in_pasmt.parcel_id = temp_prcl.parcel_id " + "~r~n" + &
												"							and	5=5 " + "~r~n" + &
												"					) "
						else
							sqls = sqls +	"		and	5=5 "
						end if
			end choose
			
			eqfctr_sqls = f_replace_string( eqfctr_sqls, "5=5", a_where_clause, "all" )
			
			execute immediate :eqfctr_sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while storing the equalization factor.  if you continue to receive this error contact your " + &
					"internal powerplant support or the powerplan property tax support desk.  the specific error returned by the database was: " + sqlca.sqlerrtext
				return false
			end if
				
			sqls = f_replace_string( sqls, "5=5", a_where_clause, "all" )
			
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while calculating the assessed values.  if you continue to receive this error contact your " + &
					"internal powerplant support or the powerplan property tax support desk.  the specific error returned by the database was: " + sqlca.sqlerrtext
				return false
			end if

////
////	update case calc and case calc authority for the changed assessments if we need to.  (pass the state and prop tax company if they're unique to help with performance.)
////
			if a_recalc_cc then
				setnull( cc_ptco_id )
				setnull( cc_st_id )
				
				if is_unique_ptco then cc_ptco_id = unique_ptco_id
				if is_unique_st then cc_st_id = unique_st_id
				
				if not i_uo_case.of_populatecasecalc( a_case_id, a_where_clause, cc_ptco_id, cc_st_id, a_msg ) then
					a_msg = "an error occurred while updating the assessment to asset allocation.  if you continue to receive this error please contact your internal powerplant support or the " + &
						"powerplan property tax support desk.  the specific error returned is below:~n~n" + a_msg
					return false
				end if
			end if

////
////	we were successful.  clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_assessmentspread ( a_case_id, string a_where_clause, string a_ratio_sql, string a_round_col_sql[], string a_round_val_sql, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_assessmentspread()
 **	
 **	this function is used to calculate assessed, equalized, and taxable values for unit assessed property (i.e., state, county, tax district, value times rate) and populate pt_parcel_assessment
 **	with those values .  the function works for the passed in case against parcel / assessment group combinations which meet the passed in criteria specified by the a_where_clause parameter.
 **	the value is calculated by taking the appropriate value from the ledger (based on the allocate assessment value for the assessment group) and multiplying it by a ratio specified by the
 **	a_ratio_sql.  (the a_ratio_sql can be a value or it can be an sql statement that results in a single numeric value.)
 **	
 **	the function performs the following operations:
 **		
 **		1.	deletes existing rows meeting the criteria from pt case calc and pt case calc authority.
 **		2.	deletes existing rows meeting the criteria from pt parcel assessment.
 **		3.	inserts the calculated assessment into pt parcel assessment.
 **		4.	corrects any rounding differences (if necessary).
 **		5.	updates the other values (equalized, taxable) on pt parcel assessment.
 **		6.	updates pt case calc and pt case calc authority for the new inserts into pt parcel assessment.
 **	
 **	parameters	:		:	(a_case_id) the case for which parcel values will be updated.
 **						string			:	(a_where_clause) an sql where clause that can be used to limit the parcels / assessment groups for this case that will have their values calculated.  the
 **											where clause will be appended to sql statement(s) directly after an "and" and, thus, should not start with a "where" or an "and".  the tables in the
 **											"from" clause of the sql statement(s) to which this where clause will be appended are:
 **											
 **												*	pt_ledger ptl
 **												*	company co
 **												*	pt_assessment_group_types agt
 **												*	pt_assessment_group_case agc
 **											
 **											important: be sure to use the aliases listed above in your where clause (and not the actual table names)
 **											
 **											important: any where clause you pass in can only limit at most to a parcel/assessment group combination.  limiting at any lower
 **												level (i.e., by property tax type, vintage, utility account, business segment, company, etc.) may cause very, very, very, bad
 **												results during the allocation !!!!!
 **											
 **											generally, then, your where clause is going to be limited to filtering to just one or more parcels, one or more assessment groups, one or more prop tax
 **											companies, one or more states, one or more counties or one or more tax districts.
 **											
 **						string			:	(a_ratio_sql) an sql statement resolving to a single decimal value that will be multiplied against the correct ledger field to compute the assessed value for
 **											each parcel.  this statement can be a single value or can be a computed field, making reference to any of the tables (using aliases) specified below:
 **											
 **												*	pt_ledger ptl
 **												*	company co
 **												*	pt_assessment_group_types agt
 **												*	pt_assessment_group_case agc
 **												*	pt_parcel prcl
 **												*	prop_tax_district td
 **											
 **											important: be sure to use the aliases listed above in your ratio statement (and not the actual table names)
 **											
 **											if you would like an example on how to set up this sql, check the of_unitassessmentdistribute() function in the nvo_ptc_logic_case user object.
 **											
 **						string			:	(a_round_col_sql[]) a collection of sql statements, each resolving to a single value that reflects the level at which the values should be checked to correct
 **											for rounding.  for instance, if you wanted to check for rounding at a state level, you might set this to "ptl.state_id" so the system will check that the total
 **											value by state equates to the value specified by the a_round_val_sql.  and, if rounding has resulted in a difference, the difference will be plugged.  if this
 **											array is empty this function won't check for rounding differences.  the statements can be a single value, single column or can be a computed field, making
 **											reference to any of the tables (using aliases) specified below:
 **											
 **												*	pt_parcel prcl
 **												*	prop_tax_district td
 **											
 **											important: be sure to use the aliases listed above in your where clause (and not the actual table names).
 **											
 **											note that this is an array because you might need multiple columns (say, state_id and county_id or state_id and assessment_group_id) to define the level
 **											at which rounding should be checked and corrected.
 **											
 **											if you would like an example on how to set up this sql, check the of_unitassessmentdistribute() function in the nvo_ptc_logic_case user object.
 **											
 **						string			:	(a_round_val_sql) an sql statement resolving to a single value that will be used to check the distributed values versus a control to ensure rounding hasn't
 **											caused any differences.  and, if rounding has resulted in a difference, the difference will be plugged.  this statement can be a single value, single column, or
 **											can be a computed field, making reference to any of the tables (using aliases) specified below:
 **											
 **												*	pt_parcel_assessment pasmt
 **												*	pt_parcel prcl
 **												*	prop_tax_district td
 **											
 **											important: be sure to use the aliases listed above in your where clause (and not the actual table names).
 **											
 **											if you would like an example on how to set up this sql, check the of_unitassessmentdistribute() function in the nvo_ptc_logic_case user object.
 **											
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel values were updated successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		null_ptco_id
	string	null_st_id

////
////	call the function with more arguments, passing null prop tax company and null state.
////
			setnull( null_ptco_id )
			setnull( null_st_id )
			
			return this.of_assessmentspread( a_case_id, a_where_clause, a_ratio_sql, a_round_col_sql[], a_round_val_sql, null_ptco_id, null_st_id, a_msg )
end function

public function boolean of_parcelassetadd ( a_parcel_id,  a_asset_id, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelassetadd()
 **	
 **	associates the passed in asset with the parcel in the pt_parcel_asset table (used for overriding parcel - location assignments).
 **	
 **	parameters	:		:	(a_parcel_id) the parcel for which the asset should be added.
 **							:	(a_asset_id) the asset that should be associated with the parcel.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel asset association was made successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		rows
		rel_prcl_id
		co_id, ast_ptco_id, prcl_ptco_id
	string	ast_st_id, prcl_st_id

////
////	validate.
////
			if isnull( a_parcel_id ) then
				a_msg = "an internal error occurred while attempting to associate parcels and assets.  no parcel was specified as the parcel for association.  if you continue to receive this error " + &
					"please contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if
			
			if isnull( a_asset_id ) then
				a_msg = "an internal error occurred while attempting to associate parcels and assets.  no asset was specified as the asset for association.  if you continue to receive this error " + &
					"please contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if

////
////	check to see if this association already exists.  if so, don't worry about going any further.
////
			setnull( rows )
			
			select		count('x')
			into		:rows
			from		pt_parcel_asset past
			where	past.parcel_id = :a_parcel_id
				and	past.asset_id = :a_asset_id;
			
			if isnull( rows ) then rows = 0
			
			if rows > 0 then
				a_msg = ""
				return true
			end if

////
////	make sure this asset isn't associated with any other parcel.
////
			setnull( rows )
			
			select		count('x')
			into		:rows
			from		pt_parcel_asset past
			where	past.asset_id = :a_asset_id;
			
			if isnull( rows ) then rows = 0
			
			if rows > 0 then
				select		past.parcel_id
				into		:rel_prcl_id
				from		pt_parcel_asset past
				where	past.asset_id = :a_asset_id;
				
				a_msg = "this asset (asset_id = " + string( a_asset_id ) + ") is already related to a parcel (parcel_id = " + string( rel_prcl_id ) + ").  an asset can only be related to one parcel.  in " + &
					"order to relate the asset with the selected parcel, you will first need to delete it's existing parcel association."
				return false
			end if

////
////	make sure the asset's state and the parcel's state match up.
////
			setnull( ast_st_id )
			
			select		al.state_id
			into		:ast_st_id
			from		cpr_ledger cpr,
						asset_location al
			where	cpr.asset_id = :a_asset_id
				and	cpr.asset_location_id = al.asset_location_id;
			
			if isnull( ast_st_id ) then
				a_msg = "unable to determine the 'state' for the selected asset (asset_id = " + string( a_asset_id ) + ").  please check to make sure this asset id exists in the system and that it is " + &
					"assigned to an asset location with a state populated."
				return false
			end if
			
			setnull( prcl_st_id )
			
			select		prcl.state_id
			into		:prcl_st_id
			from		pt_parcel prcl
			where	prcl.parcel_id = :a_parcel_id;
			
			if isnull( prcl_st_id ) then
				a_msg = "unable to determine the 'state' for the selected parcel (parcel_id = " + string( a_parcel_id ) + ").  please check to make sure this parcel exists in the system and that is it " + &
					"currently assigned to a state."
				return false
			end if
			
			if ast_st_id <> prcl_st_id then
				a_msg = "the 'state' for this asset (asset_id = " + string( a_asset_id ) + ") does not match the 'state' for this parcel (parcel_id = " + string( a_parcel_id ) + ").  assets can only be " + &
					"assigned to parcels located in the same 'state' as the asset."
				return false
			end if

////
////	make sure the prop tax companies match up.
////
			setnull( co_id )
			
			select		cpr.company_id
			into		:co_id
			from		cpr_ledger cpr
			where	asset_id = :a_asset_id;
			
			if isnull( co_id ) then
				a_msg = "unable to determine the 'company' for the selected asset (asset_id = " + string( a_asset_id ) + ").  please check to make sure this asset id exists in the system."
				return false
			end if
			
			setnull( ast_ptco_id )
			
			select		ptcov.prop_tax_company_id
			into		:ast_ptco_id
			from		ptv_co_view ptcov
			where	ptcov.company_id = :co_id
				and	ptcov.state_id = :ast_st_id;
			
			if isnull( ast_ptco_id ) then
				a_msg = "unable to determine the 'prop tax company' for the selected asset (asset_id = " + string( a_asset_id ) + ").  if you continue to receive this error contact your internal " + &
					"powerplant support or the powerplan property tax support desk."
				return false
			end if
			
			setnull( prcl_ptco_id )
			
			select		prcl.prop_tax_company_id
			into		:prcl_ptco_id
			from		pt_parcel prcl
			where	prcl.parcel_id = :a_parcel_id;
			
			if isnull( prcl_ptco_id ) then
				a_msg = "unable to determine the 'prop tax company' for the selected parcel (parcel_id = " + string( a_parcel_id ) + ").  please check to make sure this parcel exists in the system " + &
					"and that is it currently assigned to a prop tax company."
				return false
			end if
			
			if ast_ptco_id <> prcl_ptco_id then
				a_msg = "the 'prop tax company' for this asset (asset_id = " + string( a_asset_id ) + ") does not match the 'prop tax company' for this parcel (parcel_id = " + string( a_parcel_id ) + &
					").  assets can only be assigned to parcels located in the same 'prop tax company' as the asset."
				return false
			end if

////
////	do the insert.
////
			insert into pt_parcel_asset ( asset_id, parcel_id )
				values ( :a_asset_id, :a_parcel_id );
			
			if sqlca.sqlcode <> 0 then
				a_msg = "an error occurred while inserting the parcel - asset relationship.  if you continue to receive this error please contact your internal powerplant support or the powerplan " + &
					"property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parcelassetadd ( a_parcel_id,  a_asset_id[], ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelassetadd()
 **	
 **	associates the passed in assets with the parcel in the pt_parcel_asset table (used for overriding parcel - location assignments).
 **	
 **	parameters	:		:	(a_parcel_id) the parcel for which the asset should be added.
 **							:	(a_asset_id[]) the assets that should be associated with the parcel.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel asset association was made successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi

////
////	loop over the assets and process each add.
////
			maxi = upperbound( a_asset_id[] )
			
			for i = 1 to maxi
				if not this.of_parcelassetadd( a_parcel_id, a_asset_id[i], a_msg ) then return false
			next

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parcelassetdelete ( a_parcel_id,  a_asset_id, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelassetdelete()
 **	
 **	deletes the asset - parcel association from the pt_parcel_asset table (used for overriding parcel - location assignments).
 **	
 **	parameters	:		:	(a_parcel_id) the parcel for which the asset association should be deleted.
 **							:	(a_asset_id) the asset that should be disassociated from the parcel.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel asset association was deleted successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

////
////	validate.
////
			if isnull( a_parcel_id ) then
				a_msg = "an internal error occurred while attempting to disassociate parcels and assets.  no parcel was specified.  if you continue to receive this error please contact your internal " + &
					"powerplant support or the powerplan property tax support desk."
				return false
			end if
			
			if isnull( a_asset_id ) then
				a_msg = "an internal error occurred while attempting to disassociate parcels and assets.  no asset was specified.  if you continue to receive this error please contact your internal " + &
					"powerplant support or the powerplan property tax support desk."
				return false
			end if

////
////	do the delete.
////
			delete from pt_parcel_asset past
				where	past.parcel_id = :a_parcel_id
					and	past.asset_id = :a_asset_id;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "an error occurred while deleting the parcel - asset relationship.  if you continue to receive this error please contact your internal powerplant support or the powerplan " + &
					"property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parcelassetdelete ( a_parcel_id,  a_asset_id[], ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelassetdelete()
 **	
 **	deletes the asset - parcel associations from the pt_parcel_asset table (used for overriding parcel - location assignments).
 **	
 **	parameters	:		:	(a_parcel_id) the parcel for which the asset associations should be deleted.
 **							:	(a_asset_id[]) the assets that should be disassociated from the parcel.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel asset association was deleted successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi

////
////	loop over the assets and process each delete.
////
			maxi = upperbound( a_asset_id[] )
			
			for i = 1 to maxi
				if not this.of_parcelassetdelete( a_parcel_id, a_asset_id[i], a_msg ) then return false
			next

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_assessmentspread ( a_case_id, string a_where_clause, string a_ratio_sql, string a_round_col_sql[], string a_round_val_sql,  a_ptco_id, string a_st_id, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_assessmentspread()
 **	
 **	this function is used to calculate assessed, equalized, and taxable values for unit assessed property (i.e., state, county, tax district, value times rate) and populate pt_parcel_assessment
 **	with those values .  the function works for the passed in case against parcel / assessment group combinations which meet the passed in criteria specified by the a_where_clause parameter.
 **	the value is calculated by taking the appropriate value from the ledger (based on the allocate assessment value for the assessment group) and multiplying it by a ratio specified by the
 **	a_ratio_sql.  (the a_ratio_sql can be a value or it can be an sql statement that results in a single numeric value.)
 **	
 **	the function performs the following operations:
 **		
 **		1.	deletes existing rows meeting the criteria from pt case calc and pt case calc authority.
 **		2.	deletes existing rows meeting the criteria from pt parcel assessment.
 **		3.	inserts the calculated assessment into pt parcel assessment.
 **		4.	corrects any rounding differences (if necessary).
 **		5.	updates the other values (equalized, taxable) on pt parcel assessment.
 **		6.	updates pt case calc and pt case calc authority for the new inserts into pt parcel assessment.
 **	
 **	parameters	:		:	(a_case_id) the case for which parcel values will be updated.
 **						string			:	(a_where_clause) an sql where clause that can be used to limit the parcels / assessment groups for this case that will have their values calculated.  the
 **											where clause will be appended to sql statement(s) directly after an "and" and, thus, should not start with a "where" or an "and".  the tables in the
 **											"from" clause of the sql statement(s) to which this where clause will be appended are:
 **											
 **												*	pt_ledger ptl
 **												*	company co
 **												*	pt_assessment_group_types agt
 **												*	pt_assessment_group_case agc
 **											
 **											important: be sure to use the aliases listed above in your where clause (and not the actual table names)
 **											
 **											important: any where clause you pass in can only limit at most to a parcel/assessment group combination.  limiting at any lower
 **												level (i.e., by property tax type, vintage, utility account, business segment, company, etc.) may cause very, very, very, bad
 **												results during the allocation !!!!!
 **											
 **											generally, then, your where clause is going to be limited to filtering to just one or more parcels, one or more assessment groups, one or more prop tax
 **											companies, one or more states, one or more counties or one or more tax districts.
 **											
 **						string			:	(a_ratio_sql) an sql statement resolving to a single decimal value that will be multiplied against the correct ledger field to compute the assessed value for
 **											each parcel.  this statement can be a single value or can be a computed field, making reference to any of the tables (using aliases) specified below:
 **											
 **												*	pt_ledger ptl
 **												*	company co
 **												*	pt_assessment_group_types agt
 **												*	pt_assessment_group_case agc
 **												*	pt_parcel prcl
 **												*	prop_tax_district td
 **											
 **											important: be sure to use the aliases listed above in your ratio statement (and not the actual table names)
 **											
 **											if you would like an example on how to set up this sql, check the of_unitassessmentdistribute() function in the nvo_ptc_logic_case user object.
 **											
 **						string			:	(a_round_col_sql[]) a collection of sql statements, each resolving to a single value that reflects the level at which the values should be checked to correct
 **											for rounding.  for instance, if you wanted to check for rounding at a state level, you might set this to "ptl.state_id" so the system will check that the total
 **											value by state equates to the value specified by the a_round_val_sql.  and, if rounding has resulted in a difference, the difference will be plugged.  if this
 **											array is empty this function won't check for rounding differences.  the statements can be a single value, single column or can be a computed field, making
 **											reference to any of the tables (using aliases) specified below:
 **											
 **												*	pt_parcel prcl
 **												*	prop_tax_district td
 **											
 **											important: be sure to use the aliases listed above in your where clause (and not the actual table names).
 **											
 **											note that this is an array because you might need multiple columns (say, state_id and county_id or state_id and assessment_group_id) to define the level
 **											at which rounding should be checked and corrected.
 **											
 **											if you would like an example on how to set up this sql, check the of_unitassessmentdistribute() function in the nvo_ptc_logic_case user object.
 **											
 **						string			:	(a_round_val_sql) an sql statement resolving to a single value that will be used to check the distributed values versus a control to ensure rounding hasn't
 **											caused any differences.  and, if rounding has resulted in a difference, the difference will be plugged.  this statement can be a single value, single column, or
 **											can be a computed field, making reference to any of the tables (using aliases) specified below:
 **											
 **												*	pt_parcel_assessment pasmt
 **												*	pt_parcel prcl
 **												*	prop_tax_district td
 **											
 **											important: be sure to use the aliases listed above in your where clause (and not the actual table names).
 **											
 **											if you would like an example on how to set up this sql, check the of_unitassessmentdistribute() function in the nvo_ptc_logic_case user object.
 **						
 **							:	(a_ptco_id) if this statement works on just one prop tax company, and you know that prop tax company, pass the prop tax company id here to help with
 **											performance.  if you don't know the prop tax company or if this spans multiple prop tax companies, pass a null value.
 **						string			:	(a_st_id) if this statement works on just one state, and you know that state, pass the state id here to help with performance.  if you don't know the state
 **											or if this spans multiple states, pass a null value.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel values were updated successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi, j, maxj
		pos_type
		ty
	string	sqls, rtn_str
	string	col_type
	string	round_col_sql_compare[], empty_string_array[]
	boolean	agc_used, prcl_used, td_used
	decimal{2}	max_asmt, asmt_diff

/*
 *	misc objects.
 */
	uo_ds_top ds_diffs

////
////	make sure we have a case.
////
			if isnull( a_case_id ) then
				a_msg = "no case specified for populating the 'pt case calc' table."
				return false
			end if

////
////	protect against some bad values for prop tax company or state.  (this means there can never be a prop tax company id = 0.)
////
			if a_ptco_id = 0 then setnull( a_ptco_id )
			if trim( a_st_id ) = "" then setnull( a_st_id )

////
////	if the where clause is blank make it 5=5.
////
			if isnull( a_where_clause ) or trim( a_where_clause ) = "" then a_where_clause = "5=5"

////
////	validate update eligibility.  populate temp parcels ag before doing so as the function uses temp parcels ag to know what we plan on updating.
////
			delete from pt_temp_parcels_asmt_gp;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while deleting prior parcel / assessment group combinations stored off prior to validation (during the spread).  if you continue to receive this " + &
					"error contact your internal powerplant support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if
			
			agc_used = false
			if pos( a_where_clause, "agc." ) > 0 then agc_used = true
			
			sqls =						"	insert into pt_temp_parcels_asmt_gp ( parcel_id, assessment_group_id ) " + "~r~n" + &
										"		select		distinct ptl.parcel_id, agt.assessment_group_id "+  "~r~n" + &
										"		from		pt_ledger ptl, " + "~r~n" + &
										"					company co, " + "~r~n"
			
			if agc_used then
				sqls = sqls +		"					pt_assessment_group_case agc, " + "~r~n"
			end if
			
			sqls = sqls +			"					pt_assessment_group_types agt " + "~r~n" + &
										"		where	ptl.company_id = co.company_id " + "~r~n" + &
										"			and	ptl.prop_tax_company_id = agt.prop_tax_company_id " + "~r~n" + &
										"			and	ptl.state_id = agt.state_id " + "~r~n" + &
										"			and	ptl.property_tax_type_id = agt.property_tax_type_id " + "~r~n" + &
										"			and	agt.case_id = " + string( a_case_id ) + " " + "~r~n"
			
			if agc_used then
				sqls = sqls +		"			and	agt.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
										"			and	agt.state_id = agc.state_id " + "~r~n" + &
										"			and	agt.assessment_group_id = agc.assessment_group_id " + "~r~n" + &
										"			and	agt.case_id = agc.case_id " + "~r~n" + &
										"			and	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
										"			and	ptl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
										"			and	ptl.state_id = agc.state_id " + "~r~n"
			end if
			
			if not isnull( a_ptco_id ) then
				sqls = sqls +		"			and	ptl.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n" + &
										"			and	agt.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n"
				
				if agc_used then
					sqls = sqls +	"			and	agc.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n"
				end if
			end if
			
			if not isnull( a_st_id ) then
				sqls = sqls +		"			and	ptl.state_id = '" + a_st_id + "' " + "~r~n" + &
										"			and	agt.state_id = '" + a_st_id + "' " + "~r~n"
				
				if agc_used then
					sqls = sqls +	"			and	agc.state_id = '" + a_st_id + "' " + "~r~n"
				end if
			end if
			
			sqls = sqls +			"			and	" + a_where_clause + " "
			
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while storing off parcel / assessment group combinations for validation (during the spread).  if you continue to receive this error contact your " + &
					"internal powerplant support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				
				delete from pt_temp_parcels_asmt_gp;
				
				return false
			end if
			
			if not this.of_assessmentupdateeligibility( a_case_id, true, a_msg ) then return false
			
			delete from pt_temp_parcels_asmt_gp;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while deleting parcel / assessment group combinations stored for validation (during the spread).  if you continue to receive this error contact " + &
					"your internal powerplant support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if

////
////	delete the existing rows in pt case calc authority and pt case calc that meet this criteria.
////
			if not i_uo_case.of_deletecasecalc( a_case_id, a_where_clause, a_ptco_id, a_st_id, a_msg ) then return false

////
////	delete from parcel assessment.
////
			agc_used = false
			if pos( a_where_clause, "agc." ) > 0 then agc_used = true
			
			sqls =						"	delete from pt_parcel_assessment pasmt " + "~r~n" + &
										"		where	pasmt.case_id = " + string( a_case_id ) + " " + "~r~n" + &
										"			and	( pasmt.parcel_id, pasmt.assessment_group_id, pasmt.case_id ) in " + "~r~n" + &
										"						(	select		ptl.parcel_id parcel_id, " + "~r~n" + &
										"										agt.assessment_group_id assessment_group_id, " + "~r~n" + &
										"										agt.case_id case_id " + "~r~n" + &
										"							from		pt_ledger ptl, " + "~r~n" + &
										"										company co, " + "~r~n"
			
			if agc_used then
				sqls = sqls +		"										pt_assessment_group_case agc, " + "~r~n"
			end if
			
			sqls = sqls +			"										pt_assessment_group_types agt " + "~r~n" + &
										"							where	ptl.company_id = co.company_id " + "~r~n" + &
										"								and	ptl.prop_tax_company_id = agt.prop_tax_company_id " + "~r~n" + &
										"								and	ptl.state_id = agt.state_id " + "~r~n" + &
										"								and	ptl.property_tax_type_id = agt.property_tax_type_id " + "~r~n" + &
										"								and	agt.case_id = " + string( a_case_id ) + " " + "~r~n"
			
			if agc_used then
				sqls = sqls +		"								and	agt.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
										"								and	agt.state_id = agc.state_id " + "~r~n" + &
										"								and	agt.assessment_group_id = agc.assessment_group_id " + "~r~n" + &
										"								and	agt.case_id = agc.case_id " + "~r~n" + &
										"								and	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
										"								and	ptl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
										"								and	ptl.state_id = agc.state_id " + "~r~n"
			end if
			
			if not isnull( a_ptco_id ) then
				sqls = sqls +		"								and	ptl.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n" + &
										"								and	agt.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n"
				
				if agc_used then
					sqls = sqls +	"								and	agc.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n"
				end if
			end if
			
			if not isnull( a_st_id ) then
				sqls = sqls +		"								and	ptl.state_id = '" + a_st_id + "' " + "~r~n" + &
										"								and	agt.state_id = '" + a_st_id + "' " + "~r~n"
				
				if agc_used then
					sqls = sqls +	"								and	agc.state_id = '" + a_st_id + "' " + "~r~n"
				end if
			end if
			
			sqls = sqls +			"								and	5=5 " + "~r~n" + &
										"						) "
			
			sqls = f_replace_string( sqls, "5=5", a_where_clause, "all" )
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while deleting the previously loaded parcel assessments.  if you continue to receive this error contact your internal powerplant support or the " + &
					"powerplan property tax support line.  the specific error was: " + sqlca.sqlerrtext
				return false
			end if

////
////	get the tax year for this case.
////
			setnull( ty )
			
			select		tax_year
			into		:ty
			from		pt_case
			where	case_id = :a_case_id;
			
			if isnull( ty ) then
				a_msg = "no tax year specified for this case.  please check the 'pt case' table to ensure a tax year has been associated with this case."
				return false
			end if

////
////	calculate the assessed values for all parcels that meet the where clause criteria and insert those values into pt parcel assessment.  build the sql and replace the 5=5 with the passed in
////	where clause.
////
			prcl_used = false
			td_used = false
			
			if pos( a_ratio_sql, "prcl." ) > 0 then prcl_used = true
			if pos( a_ratio_sql, "td." ) > 0 then td_used = true
			if td_used then prcl_used = true
			
			sqls =						"	insert into pt_parcel_assessment ( parcel_id, assessment_group_id, case_id, assessment, equalized_value, taxable_value, use_tiered_rates_yn ) " + "~r~n" + &
										"		select		ptl.parcel_id parcel_id, " + "~r~n" + &
										"					agc.assessment_group_id assessment_group_id, " + "~r~n" + &
										"					agc.case_id case_id, " + "~r~n" + &
										"					sum( round( decode( agc.assessed_value_allocation_id, 1, nvl( ptlty.prop_tax_balance, 0 ), 2, nvl( ptlty.market_value, 0 ), 3, nvl( ptlty.quantity_factored, 0 ) ) * " + a_ratio_sql + ", 2 ) ) assessment, " + "~r~n" + &
										"					to_number( null ) equalized_value, " + "~r~n" + &
										"					to_number( null ) taxable_value, " + "~r~n" + &
										"					0 use_tiered_rates_yn " + "~r~n" + &
										"		" + "~r~n" + &
										"		from		pt_ledger_tax_year ptlty, " + "~r~n" + &
										"					pt_ledger ptl, " + "~r~n" + &
										"					company co, " + "~r~n"
			
			if prcl_used then
				sqls = sqls +		"					pt_parcel prcl, " + "~r~n"
			end if
			
			if td_used then
				sqls = sqls +		"					prop_tax_district td, " + "~r~n"
			end if
			
			sqls = sqls +			"					pt_assessment_group_case agc, " + "~r~n" + &
										"					pt_assessment_group_types agt " + "~r~n" + &
										"		" + "~r~n" + &
										"		where	ptlty.tax_year = " + string( ty ) + " " + "~r~n" + &
										"			and	ptlty.property_tax_ledger_id = ptl.property_tax_ledger_id " + "~r~n" + &
										"			and	ptl.company_id = co.company_id " + "~r~n" + &
										"			and	ptl.prop_tax_company_id = agt.prop_tax_company_id " + "~r~n" + &
										"			and	ptl.state_id = agt.state_id " + "~r~n" + &
										"			and	ptl.property_tax_type_id = agt.property_tax_type_id " + "~r~n" + &
										"			and	agt.case_id = " + string( a_case_id ) + " " + "~r~n" + &
										"			and	agt.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
										"			and	agt.state_id = agc.state_id " + "~r~n" + &
										"			and	agt.assessment_group_id = agc.assessment_group_id " + "~r~n" + &
										"			and	agt.case_id = agc.case_id " + "~r~n" + &
										"			and	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
										"			and	ptl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
										"			and	ptl.state_id = agc.state_id " + "~r~n"
			
			if prcl_used then
				sqls = sqls +		"			and	ptl.parcel_id = prcl.parcel_id " + "~r~n" + &
										"			and	prcl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
										"			and	prcl.state_id = agc.state_id " + "~r~n" + &
										"			and	prcl.prop_tax_company_id = agt.prop_tax_company_id " + "~r~n" + &
										"			and	prcl.state_id = agt.state_id " + "~r~n"
				
				if td_used then
					sqls = sqls +	"			and	prcl.tax_district_id = td.tax_district_id " + "~r~n"
				end if
			end if
			
			if not isnull( a_ptco_id ) then
				sqls = sqls +		"			and	ptl.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n" + &
										"			and	agt.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n" + &
										"			and	agc.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n"
				
				if prcl_used then
					sqls = sqls +	"			and	prcl.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n"
				end if
			end if
			
			if not isnull( a_st_id ) then
				sqls = sqls +		"			and	ptl.state_id = '" + a_st_id + "' " + "~r~n" + &
										"			and	agt.state_id = '" + a_st_id + "' " + "~r~n" + &
										"			and	agc.state_id = '" + a_st_id + "' " + "~r~n"
				
				if prcl_used then
					sqls = sqls +	"			and	prcl.state_id = '" + a_st_id + "' " + "~r~n"
				end if
				
				if td_used then
					sqls = sqls +	"			and	td.state_id = '" + a_st_id + "' " + "~r~n"
				end if
			end if
			
			sqls = sqls +			"			and	5=5 " + "~r~n" + &
										"		" + "~r~n" + &
										"		group by	ptl.parcel_id, " + "~r~n" + &
										"					agc.assessment_group_id, " + "~r~n" + &
										"					agc.case_id "
			
			sqls = f_replace_string( sqls, "5=5", a_where_clause, "all" )
			
			execute immediate :sqls using sqlca;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error occurred while allocating the assessments to parcels on the 'pt parcel assessment' table.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if

////
////	now, correct for rounding if we need to.  the assessment(s) we just calculated should add back exactly to the passed in value(s) if there are any.  if this is not the case then we need to take
////	care of the differences.
////	
////	we're going to stick the differences on the parcel with the largest assessment.  in the case that there are multiple rows in pt parcel assessment with the largest assessment, take the minimum
////	parcel id to make sure that we only stick the difference on one row.
////
			if upperbound( a_round_col_sql[] ) > 0 then
				////	
				////	build the sql to get any differences.
				////
					prcl_used = false
					td_used = false
					
					if pos( a_round_val_sql, "prcl." ) > 0 then prcl_used = true
					if pos( a_round_val_sql, "td." ) > 0 then td_used = true
					
					maxi = upperbound( a_round_col_sql[] )
					
					if not prcl_used or not td_used then
						for i = 1 to maxi
							if pos( a_round_col_sql[i], "prcl." ) > 0 then prcl_used = true
							if pos( a_round_col_sql[i], "td." ) > 0 then td_used = true
						next
					end if
					
					if td_used then prcl_used = true
					
					agc_used = false
					if pos( a_where_clause, "agc." ) > 0 then agc_used = true
					
					sqls =						"	select		"
					
					for i = 1 to maxi
						sqls = sqls +		"				" + a_round_col_sql[i] + ", " + "~r~n"
					next
					
					sqls = sqls +			"				max( nvl( pasmt.assessment, 0 ) ) max_assessment, " + "~r~n" + &
												"				nvl( " + a_round_val_sql + ", 0 ) - sum( nvl( pasmt.assessment, 0 ) ) assessment_diff " + "~r~n" + &
												"	" + "~r~n" + &
												"	from		pt_parcel_assessment pasmt"
					
					if prcl_used then
						sqls = sqls + ", " + "~r~n"
						sqls = sqls +		"				pt_parcel prcl"
						if td_used then
							sqls = sqls + ", " + "~r~n"
							sqls = sqls +	"				prop_tax_district td " + "~r~n"
						else
							sqls = sqls + " " + "~r~n"
						end if
					else
						sqls = sqls + " " + "~r~n"
					end if
					
					sqls = sqls +			"	" + "~r~n" + &
												"	where	pasmt.case_id = " + string( a_case_id ) + " " + "~r~n"
					
					if prcl_used then
						sqls = sqls +		"		and	pasmt.parcel_id = prcl.parcel_id " + "~r~n"
						
						if td_used then
							sqls = sqls +	"		and	prcl.tax_district_id = td.tax_district_id " + "~r~n"
						end if
					end if
					
					sqls = sqls +			"		and	( pasmt.parcel_id, pasmt.assessment_group_id, pasmt.case_id ) in " + "~r~n" + &
												"					(	select		ptl.parcel_id parcel_id, " + "~r~n" + &
												"									agt.assessment_group_id assessment_group_id, " + "~r~n" + &
												"									agt.case_id case_id " + "~r~n" + &
												"						from		pt_ledger ptl, " + "~r~n" + &
												"									company co, " + "~r~n"
					
					if agc_used then
						sqls = sqls +		"									pt_assessment_group_case agc, " + "~r~n"
					end if
					
					sqls = sqls +			"									pt_assessment_group_types agt " + "~r~n" + &
												"						where	ptl.company_id = co.company_id " + "~r~n" + &
												"							and	ptl.prop_tax_company_id = agt.prop_tax_company_id " + "~r~n" + &
												"							and	ptl.state_id = agt.state_id " + "~r~n" + &
												"							and	ptl.property_tax_type_id = agt.property_tax_type_id " + "~r~n" + &
												"							and	agt.case_id = " + string( a_case_id ) + " " + "~r~n"
					
					if agc_used then
						sqls = sqls +		"							and	agt.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
												"							and	agt.state_id = agc.state_id " + "~r~n" + &
												"							and	agt.assessment_group_id = agc.assessment_group_id " + "~r~n" + &
												"							and	agt.case_id = agc.case_id " + "~r~n" + &
												"							and	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
												"							and	ptl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
												"							and	ptl.state_id = agc.state_id " + "~r~n"
					end if
					
					if not isnull( a_ptco_id ) then
						sqls = sqls +		"							and	ptl.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n" + &
												"							and	agt.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n"
						
						if agc_used then
							sqls = sqls +	"							and	agc.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n"
						end if
					end if
					
					if not isnull( a_st_id ) then
						sqls = sqls +		"							and	ptl.state_id = '" + a_st_id + "' " + "~r~n" + &
												"							and	agt.state_id = '" + a_st_id + "' " + "~r~n"
						
						if agc_used then
							sqls = sqls +	"							and	agc.state_id = '" + a_st_id + "' " + "~r~n"
						end if
					end if
					
					sqls = sqls +			"							and	" + a_where_clause + " " + "~r~n" + &
												"					) " + "~r~n" + &
												"	" + "~r~n" + &
												"	having	sum( nvl( pasmt.assessment, 0 ) ) <> nvl( " + a_round_val_sql + ", 0 ) " + "~r~n" + &
												"	" + "~r~n" + &
												"	group by	"
					
					for i = 1 to maxi
						sqls = sqls +		"				" + a_round_col_sql[i] + ", " + "~r~n"
					next
					
					sqls = sqls +			"				nvl( " + a_round_val_sql + ", 0 ) " + "~r~n"
				
				////
				////	retrieve the differences.
				////
					ds_diffs = create uo_ds_top
					rtn_str = f_create_dynamic_ds( ds_diffs, "grid", sqls, sqlca, true )
					if isnull( rtn_str ) then rtn_str = "null"
					
					if rtn_str <> "ok" then
						a_msg = "error occurred while retrieving the rounding differences resulting from the allocation of the assessments to parcels.~n~nerror: " + rtn_str
						destroy ds_diffs
						return false
					end if
				
				////
				////	loop over the rounding differences we have and update the correct row in pt parcel assessment.
				////
					maxi = ds_diffs.rowcount()
					maxj = upperbound( a_round_col_sql[] )
					
					for i = 1 to maxi
						round_col_sql_compare[] = empty_string_array[]
						
						for j = 1 to maxj
							col_type = lower( trim( ds_diffs.describe( "#" + string( j ) + ".coltype" ) ) )
							pos_type = pos( col_type, "(", 1 )
							if pos_type > 1 then col_type = left( col_type, pos_type - 1 )
							
							choose case col_type
								case	"char"
									round_col_sql_compare[j] = a_round_col_sql[j] + " = '" + f_replace_string( ds_diffs.getitemstring( i, j ), "'", "''", "all" ) + "'"
								case	"number", "decimal"
									round_col_sql_compare[j] = a_round_col_sql[j] + " = " + string( ds_diffs.getitemnumber( i, j ) )
								case	else
									a_msg = "an internal error occurred while finding the parcel to adjust for rounding.  the column for rounding is in an unexpected format.  if you continue to receive this " + &
										"error contact your internal powerplant support or the powerplan property tax support desk."
									destroy ds_diffs
									return false
							end choose
						next
						
						max_asmt = round( ds_diffs.getitemnumber( i, maxj + 1 ), 2 )
						asmt_diff = round( ds_diffs.getitemnumber( i, maxj + 2 ), 2 )
						
						if isnull( max_asmt ) then max_asmt = 0
						
						if isnull( asmt_diff ) then asmt_diff = 0
						if asmt_diff = 0 then continue
						
						sqls =							"	update	pt_parcel_assessment pasmt " + "~r~n" + &
														"	set		pasmt.assessment = nvl( pasmt.assessment, 0 ) + " + string( asmt_diff ) + " " + "~r~n" + &
														"	where	pasmt.case_id =  " + string( a_case_id ) + " " + "~r~n" + &
														"		and	nvl( pasmt.assessment, 0 ) = " + string( max_asmt ) + " " + "~r~n" + &
														"		and	rownum = 1 " + "~r~n" + &
														"		and	( pasmt.parcel_id, pasmt.assessment_group_id, pasmt.case_id ) in " + "~r~n" + &
														"					(	select		ptl.parcel_id parcel_id, " + "~r~n" + &
														"									agt.assessment_group_id assessment_group_id, " + "~r~n" + &
														"									agt.case_id case_id " + "~r~n" + &
														"						from		pt_ledger ptl, " + "~r~n" + &
														"									company co, " + "~r~n"
						
						if agc_used then
							sqls = sqls +			"									pt_assessment_group_case agc, " + "~r~n"
						end if
						
						if prcl_used then
							sqls = sqls +			"									pt_parcel prcl, " + "~r~n"
						end if
						
						if td_used then
							sqls = sqls +			"									prop_tax_district td, " + "~r~n"
						end if
						
						sqls = sqls +				"									pt_assessment_group_types agt " + "~r~n" + &
														"						where	ptl.company_id = co.company_id " + "~r~n" + &
														"							and	ptl.prop_tax_company_id = agt.prop_tax_company_id " + "~r~n" + &
														"							and	ptl.state_id = agt.state_id " + "~r~n" + &
														"							and	ptl.property_tax_type_id = agt.property_tax_type_id " + "~r~n" + &
														"							and	agt.case_id = " + string( a_case_id ) + " " + "~r~n"
						
						if agc_used then
							sqls = sqls +			"							and	agt.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
														"							and	agt.state_id = agc.state_id " + "~r~n" + &
														"							and	agt.assessment_group_id = agc.assessment_group_id " + "~r~n" + &
														"							and	agt.case_id = agc.case_id " + "~r~n" + &
														"							and	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
														"							and	ptl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
														"							and	ptl.state_id = agc.state_id " + "~r~n"
						end if
						
						if prcl_used then
							sqls = sqls +			"							and	ptl.parcel_id = prcl.parcel_id " + "~r~n" + &
														"							and	prcl.state_id = agt.state_id " + "~r~n" + &
														"							and	prcl.prop_tax_company_id = agt.prop_tax_company_id " + "~r~n"
														
							if td_used then
								sqls = sqls +		"							and	prcl.tax_district_id = td.tax_district_id " + "~r~n"
							end if
							
							if agc_used then
								sqls = sqls +		"							and	prcl.state_id = agc.state_id " + "~r~n" + &
														"							and	prcl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n"
							end if
						end if
				
						if not isnull( a_ptco_id ) then
							sqls = sqls +			"							and	ptl.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n" + &
														"							and	agt.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n"
							
							if agc_used then
								sqls = sqls +		"							and	agc.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n"
							end if
							
							if prcl_used then
								sqls = sqls +		"							and	prcl.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n"
							end if
						end if
						
						if not isnull( a_st_id ) then
							sqls = sqls +			"							and	ptl.state_id = '" + a_st_id + "' " + "~r~n" + &
														"							and	agt.state_id = '" + a_st_id + "' " + "~r~n"
							
							if agc_used then
								sqls = sqls +		"							and	agc.state_id = '" + a_st_id + "' " + "~r~n"
							end if
							
							if prcl_used then
								sqls = sqls +		"							and	prcl.state_id = '" + a_st_id + "' " + "~r~n"
							end if
							
							if td_used then
								sqls = sqls +		"							and	td.state_id = '" + a_st_id + "' " + "~r~n"
							end if
						end if
						
						for j = 1 to maxj
							sqls = sqls +			"							and	" + round_col_sql_compare[j] + " " + "~r~n"
						next
						
						sqls = sqls +				"							and	" + a_where_clause + " " + "~r~n" + &
														"					) "
						
						execute immediate :sqls;
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while correcting the rounding difference during the allocation of the assessment to parcels on the 'pt parcel assessment' table.  if you " + &
										"continue to receive this error contact your internal powerplant support or the powerplan property tax support desk.~n~nthe specific error was: " + sqlca.sqlerrtext
							destroy ds_diffs
							return false
						end if
					next
					
					destroy ds_diffs
			end if

////
////	update the values derived from assessment (i.e., equalized and taxable values) for the assessments we just entered.  first, build the sql to limit the parcel / assessment group combos that
////	will be updated to those we just inserted.  then, call our function in the parcel user object to do the recalc.   make sure to pass it a false for the a_recalc_cc parameter so the recalc function
////	won't call the populatecasecalc function because calling it with this sql we're building wouldn't work.  instead we'll call the populatecasecalc function ourselves after the recalc with better sql.
////
			agc_used = false
			if pos( a_where_clause, "agc." ) > 0 then agc_used = true
			
			sqls =						"			( pasmt.parcel_id, pasmt.assessment_group_id, pasmt.case_id ) in " + "~r~n" + &
										"				(	select		ptl.parcel_id parcel_id, " + "~r~n" + &
										"								agt.assessment_group_id assessment_group_id, " + "~r~n" + &
										"								agt.case_id case_id " + "~r~n" + &
										"					from		pt_ledger_tax_year ptlty, " + "~r~n" + &
										"								pt_ledger ptl, " + "~r~n" + &
										"								company co, " + "~r~n"
			
			if agc_used then
				sqls = sqls +		"								pt_assessment_group_case agc, " + "~r~n"
			end if
			
			sqls = sqls +			"								pt_assessment_group_types agt " + "~r~n" + &
										"					" + "~r~n" + &
										"					where	ptlty.tax_year = " + string( ty ) + " " + "~r~n" + &
										"						and	ptlty.property_tax_ledger_id = ptl.property_tax_ledger_id " + "~r~n" + &
										"						and	ptl.company_id = co.company_id " + "~r~n" + &
										"						and	ptl.prop_tax_company_id = agt.prop_tax_company_id " + "~r~n" + &
										"						and	ptl.state_id = agt.state_id " + "~r~n" + &
										"						and	ptl.property_tax_type_id = agt.property_tax_type_id " + "~r~n" + &
										"						and	agt.case_id = " + string( a_case_id ) + " " + "~r~n"
			
			if agc_used then
				sqls = sqls +		"						and	agt.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
										"						and	agt.state_id = agc.state_id " + "~r~n" + &
										"						and	agt.assessment_group_id = agc.assessment_group_id " + "~r~n" + &
										"						and	agt.case_id = agc.case_id " + "~r~n" + &
										"						and	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
										"						and	ptl.prop_tax_company_id = agc.prop_tax_company_id " + "~r~n" + &
										"						and	ptl.state_id = agc.state_id " + "~r~n"
			end if
			
			if not isnull( a_ptco_id ) then
				sqls = sqls +		"						and	ptl.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n" + &
										"						and	agt.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n"
				
				if agc_used then
					sqls = sqls +	"						and	agc.prop_tax_company_id = " + string( a_ptco_id ) + " " + "~r~n"
				end if
			end if
			
			if not isnull( a_st_id ) then
				sqls = sqls +		"						and	ptl.state_id = '" + a_st_id + "' " + "~r~n" + &
										"						and	agt.state_id = '" + a_st_id + "' " + "~r~n"
				
				if agc_used then
					sqls = sqls +	"						and	agc.state_id = '" + a_st_id + "' " + "~r~n"
				end if
			end if
			
			sqls = sqls +			"						and	" + a_where_clause + " " + "~r~n" + &
										"				) "
			
			if not this.of_assessmentcalcvalues( a_case_id, sqls, this.i_fixed_field_assessment, false, a_msg ) then return false

////
////	update the corresponding rows in pt case calc for the parcel/assessment group combinations that we processed above.
////
			if not i_uo_case.of_populatecasecalc( a_case_id, a_where_clause, a_ptco_id, a_st_id, a_msg ) then return false

////
////	everything was successful so clear out the error message and return 1.
////
			a_msg = ""
			return true
end function

public function boolean of_assessmentcopy (string a_where_clause,  a_from_case_id,  a_to_case_id, decimal a_trend_percent, boolean a_overwrite_yn, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_assessmentcopy()
 **	
 **	copies assessments for given parcels from one case to another.  when copying, assessments can be "trended" (i.e., increased or decreased by a given percent).
 **	if no trend percent is specified, assessments will be copied as is.  this function copies the assessments column and then calculates and populated the equalized value and taxable value
 **	columns based on the data setup for this case.
 **	
 **	parameters	:	string					:	(a_where_clause) an sql where clause that can be used to limit the parcel assessments that will be copied to the new case.  this where clause
 **														will be appended to sql statement(s) directly after an "and" and, thus, should not start with a "where" or an "and".  the table in the "from" clause of
 **														the sql statement(s) to which this where clause will be appended is:
 **											
 **															*	pt_parcel prcl
 **											
 **															important: be sure to use the alias listed above in your where clause (and not the actual table name)
 **
 **							:	(a_from_case_id) the case we're copying the assessment group setup from.
 **							:	(a_to_case_id) the case we're copying the assessment group setup to.
 **						decimal				:	(a_trend_percent) the amount to inflate/deflate the values
 **														when copying.  this should be the percentage in decimal terms of the amount
 **														to increase/decrease the factor when copying.  for instance a 30% increase would be noted by passing .3 whereas a 0%
 **														increase - i.e., straight copy - would be noted by passing 0.
 **						boolean				:	(a_overwrite_yn) whether any existing assessments in the to case should be overwritten (true) or not (false).
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the assessments were copied successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
	string	sqls, pass_where_clause
	string	null_string
		null
	decimal{8}	trend_fctr

////
////	don't error.
////
			if isnull( a_where_clause ) then a_where_clause = ""
			
			if isnull( a_from_case_id ) then
				a_msg = "missing case to copy from.  if you continue to receive this error contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if
			
			if isnull( a_to_case_id ) then
				a_msg = "missing case to copy to.  if you continue to receive this error contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if
			
////
////	if the where clause is blank, set it to "5=5" so we don't get an error.
////
			if trim( a_where_clause ) = "" then a_where_clause = "5=5"

////
////	validate update eligibility.  populate temp parcels ag before doing so as the function uses temp parcels ag to know what we plan on updating.
////
			delete from pt_temp_parcels_asmt_gp;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while deleting prior parcel / assessment group combinations stored off prior to validation (during the copy).  if you continue to receive this " + &
					"error contact your internal powerplant support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if
			
			sqls =		"	insert into pt_temp_parcels_asmt_gp ( parcel_id, assessment_group_id ) " + "~r~n" + &
						"		select		pasmt.parcel_id, pasmt.assessment_group_id "+  "~r~n" + &
						"		from		pt_parcel_assessment pasmt, " + "~r~n" + &
						"					pt_parcel prcl " + "~r~n" + &
						"		where	pasmt.case_id = " + string( a_from_case_id ) + " " + "~r~n" + &
						"			and	pasmt.parcel_id = prcl.parcel_id " + "~r~n" + &
						"			and	" + a_where_clause + " "
			
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while storing off parcel / assessment group combinations for validation (during the copy).  if you continue to receive this error contact your " + &
					"internal powerplant support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				
				delete from pt_temp_parcels_asmt_gp;
				
				return false
			end if
			
			if not this.of_assessmentupdateeligibility( a_to_case_id, true, a_msg ) then return false
			
			delete from pt_temp_parcels_asmt_gp;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while deleting parcel / assessment group combinations stored for validation (during the copy).  if you continue to receive this error contact " + &
					"your internal powerplant support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if
			
////
////	figure out our trending percent.
////
			if isnull( a_trend_percent ) then a_trend_percent = 0
			trend_fctr = 1 + a_trend_percent

////
////	if we're overwriting then we need to delete the existing parcel / assessment group combinations for everything that we're copying.
////	before we delete from parcel assessment we have to delete from case calc and case calc authority (as the deletes for these guys depend on parcel assessment).
////
			if a_overwrite_yn then
				setnull( null )
				setnull( null_string )
				
				if not i_uo_case.of_deletecasecalc( a_to_case_id, a_where_clause, null, null_string, a_msg ) then
					a_msg = "an error occurred while deleting the assessment to asset allocation.  if you continue to receive this error please contact your internal powerplant support or the powerplan " + &
						"property tax support desk.  the specific error returned is below:~n~n" + a_msg
					return false
				end if
				
				sqls =		"	delete from pt_parcel_assessment pasmt " + "~r~n" + &
							"		where	pasmt.case_id = " + string( a_to_case_id ) + " " + "~r~n" + &
							"			and	pasmt.parcel_id in " + "~r~n" + &
							"						(	select 	prcl.parcel_id " + "~r~n" + &
							"							from 		pt_parcel prcl" + "~r~n" +&
							"							where 	" + a_where_clause + " " + "~r~n" + &
							"						) "
				
				execute immediate :sqls;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "an error occurred while deleting the selected parcel assessments.  if you continue to receive this error please contact your internal powerplant support or the powerplan property " + &
						"tax support desk.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	build the sql to do the copy and do it.
////
			sqls =					"	insert into pt_parcel_assessment ( parcel_id, assessment_group_id, case_id, assessment, use_tiered_rates_yn ) " + "~r~n" + &
									"		select		pasmt.parcel_id parcel_id, " + "~r~n" + &
									"					pasmt.assessment_group_id assessment_group_id, " + "~r~n" + &
									"					" + string( a_to_case_id ) + " case_id, " + "~r~n" + &
									"					round( pasmt.assessment * " + string( trend_fctr ) + ", 2 ) assessment, " + "~r~n" + &
									"					pasmt.use_tiered_rates_yn use_tiered_rates_yn " + "~r~n" + &
									"		from		pt_parcel_assessment pasmt, " + "~r~n" + & 
									"					pt_parcel prcl " + "~r~n" + &
									"		where	pasmt.case_id = " + string( a_from_case_id ) + " " + "~r~n" + &
									"			and	pasmt.parcel_id = prcl.parcel_id " + "~r~n" + &
									"			and 	" + a_where_clause + " " + "~r~n" + &
									"			and	not exists " + "~r~n" + &
									"						(	select		'x' " + "~r~n" + &
									"							from		pt_parcel_assessment exists_pasmt " + "~r~n" + &
									"							where	pasmt.parcel_id = exists_pasmt.parcel_id " + "~r~n" + &
									"								and	pasmt.assessment_group_id = exists_pasmt.assessment_group_id " + "~r~n" + &
									"								and	exists_pasmt.case_id = " + string( a_to_case_id ) + " " + "~r~n" + &
									"						) "
			
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while copying the assessments for the selected parcels from one case to another.  if you continue to receive this " + &
					"error contact your internal powerplant support or the powerplan property tax support desk.  the specified error returned was: " + sqlca.sqlerrtext
				return false
			end if

////
////	now we need to update the equalized value and taxable value for these guys.  call our function in this user object to do that work for us.  this function will also take care of populating the
////	case calc and case calc authority tables for us so we don't have to do it here.  first we'll have to build a where clause that can be used in the function we're calling.
////
			pass_where_clause = a_where_clause
			
			if trim(pass_where_clause) <> "5=5" then
				pass_where_clause =	"pasmt.parcel_id in " + "~r~n" + &
												"	(	select 	prcl.parcel_id " + "~r~n" + &
												"		from 		pt_parcel prcl" + "~r~n" +&
												"		where 	" + a_where_clause + " " + "~r~n" + &
												"	) "
			end if
			
			if not this.of_assessmentcalcvalues( a_to_case_id, pass_where_clause, this.i_fixed_field_assessment, true, a_msg ) then return false

////
////	we were successful.  clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parceldelete ( a_parcel_id, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_parceldelete()
 **	
 **	deletes the parcel (according to system controls telling when a parcel can be deleted).
 **	
 **	parameters	:		:	(a_parcel_id) the parcel to delete.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel was deleted successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi, rows
		td_id, ptco_id, ty, trans_id
	string	sqls, rtn_str, sysval
	string	st_id
	boolean	del_attrib, del_user_input

/*
 *	misc objects.
 */
	uo_ds_top ds_lock
	uo_ds_top ds_trans
	uo_ds_top ds_pre_trans
	nvo_ptc_logic_ledger uo_ledger
	nvo_ptc_logic_taxyear uo_taxyear
	nvo_ptc_logic_preallo_ledger uo_preallo

////
////	validate.
////
			////
			////	make sure we have a parcel passed.
			////
				if isnull( a_parcel_id ) then
					a_msg = "an internal error occurred while attempting to delete a parcel.  no parcel was specified.  if you continue to receive this error please contact your internal powerplant " + &
						"support or the powerplan property tax support desk."
					return false
				end if
			
			////
			////	check the system option to see when we're allowed to delete.
			////
				del_attrib = false
				del_user_input = false
				
				sysval = upper( trim( i_uo_systemoptions.of_getsystemoption( "parcels - allow deletes" ) ) )
				
				choose case sysval
					case	"no"
						a_msg = "parcels are not allowed to be deleted (per the 'parcels - allow deletes' system option)."
						return false
					case	"unused parcels"
						// nothing needed
					case	"unused parcels w/ attributes"
						del_attrib = true
					case	"unused parcels w/ attributes, user input ledger items"
						del_attrib = true
						del_user_input = true
					case	else
						a_msg = "unable to determine whether permissions exist allowing parcels to be deleted.  the value for the 'parcels - allow deletes' system option cannot be determined.  if you " + &
							"continue to receive this error contact your internal powerplant support or the powerplan property tax support desk."
						return false
				end choose
			
			////
			////	if we're here, then we're allowed to delete.  first, make sure this is an unused parcel.  there are some parcels that will never be allowed to be deleted (unless underlying tables are
			////	deleted first).  check for those here.
			////
				// pt statement line
				setnull( rows )
				select		count('x')
				into		:rows
				from		pt_statement_line
				where	parcel_id = :a_parcel_id
					and	rownum = 1;
				
				if not isnull( rows ) and rows > 0 then
					a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is associated with one or more bills and cannot be deleted.  in order to delete this parcel, first " + &
						"delete this parcel from the bills with which it is associated.  (if those bills have been paid, the association cannot be deleted).  after doing so, re-attempt to delete the parcel."
					return false
				end if
				
				// pt parcel assessment
				setnull( rows )
				select		count('x')
				into		:rows
				from		pt_parcel_assessment
				where	parcel_id = :a_parcel_id
					and	rownum = 1;
				
				if not isnull( rows ) and rows > 0 then
					a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") has one or more assessments.  in order to delete this parcel, first delete the assessments from " + &
						"this parcel.  after doing so, re-attempt to delete the parcel."
					return false
				end if
				
				// pt parcel location
				setnull( rows )
				select		count('x')
				into		:rows
				from		pt_parcel_location
				where	parcel_id = :a_parcel_id
					and	rownum = 1;
				
				if not isnull( rows ) and rows > 0 then
					a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is assigned to one or more asset locations.  in order to delete this parcel, first delete the " + &
						"location assessments for this parcel.  after doing so, re-attempt to delete the parcel."
					return false
				end if
				
				// pt parcel asset
				setnull( rows )
				select		count('x')
				into		:rows
				from		pt_parcel_asset
				where	parcel_id = :a_parcel_id
					and	rownum = 1;
				
				if not isnull( rows ) and rows > 0 then
					a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is assigned to one or more assets.  in order to delete this parcel, first delete the asset " + &
						"assessments for this parcel.  after doing so, re-attempt to delete the parcel."
					return false
				end if
				
				// pt statistics full
				setnull( rows )
				select		count('x')
				into		:rows
				from		pt_statistics_full
				where	parcel_id = :a_parcel_id
					and	rownum = 1;
				
				if not isnull( rows ) and rows > 0 then
					a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is assigned to one or more (full) statistics.  in order to delete this parcel, first delete the " + &
						"statisics for this parcel.  after doing so, re-attempt to delete the parcel."
					return false
				end if
				
				// pt statistics incremental
				setnull( rows )
				select		count('x')
				into		:rows
				from		pt_statistics_incremental
				where	parcel_id = :a_parcel_id
					and	rownum = 1;
				
				if not isnull( rows ) and rows > 0 then
					a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is assigned to one or more (incremental) statistics.  in order to delete this parcel, first delete " + &
						"the statisics for this parcel.  after doing so, re-attempt to delete the parcel."
					return false
				end if
				
				// pt value vault
				setnull( rows )
				select		count('x')
				into		:rows
				from		pt_value_vault
				where	parcel_id = :a_parcel_id
					and	rownum = 1;
				
				if not isnull( rows ) and rows > 0 then
					a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is associated with one or more entries in the data center value vault.  in order to delete this parcel, first delete the value " + &
							"vault records relating to this parcel.  after doing so, re-attempt to delete the parcel."
					return false
				end if
			
			////
			////	if we can't delete parcels with attributes, check those here.
			////
				if not del_attrib then
					// pt parcel appeal
					setnull( rows )
					select		count('x')
					into		:rows
					from		pt_parcel_appeal
					where	parcel_id = :a_parcel_id
						and	rownum = 1;
					
					if not isnull( rows ) and rows > 0 then
						a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is associated with one or more appeals.  in order to delete this parcel, first delete the appeals " + &
							"for this parcel.  after doing so, re-attempt to delete the parcel."
						return false
					end if
					
					// pt parcel appraisal
					setnull( rows )
					select		count('x')
					into		:rows
					from		pt_parcel_appraisal
					where	parcel_id = :a_parcel_id
						and	rownum = 1;
					
					if not isnull( rows ) and rows > 0 then
						a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is associated with one or more appraisals.  in order to delete this parcel, first delete the " + &
							"appraisals for this parcel.  after doing so, re-attempt to delete the parcel."
						return false
					end if
					
					// pt parcel auto adjust
					setnull( rows )
					select		count('x')
					into		:rows
					from		pt_parcel_auto_adjust
					where	parcel_id = :a_parcel_id
						and	rownum = 1;
					
					if not isnull( rows ) and rows > 0 then
						a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is associated with one or more automatic adjustments.  in order to delete this parcel, first " + &
							"delete the automatic adjustments for this parcel.  after doing so, re-attempt to delete the parcel."
						return false
					end if
					
					// pt parcel geography
					setnull( rows )
					select		count('x')
					into		:rows
					from		pt_parcel_geography
					where	parcel_id = :a_parcel_id
						and	rownum = 1;
					
					if not isnull( rows ) and rows > 0 then
						a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is associated with one or more geography values.  in order to delete this parcel, first delete the " + &
							"geography values for this parcel.  after doing so, re-attempt to delete the parcel."
						return false
					end if
					
					// pt parcel history
					setnull( rows )
					select		count('x')
					into		:rows
					from		pt_parcel_history
					where	parcel_id = :a_parcel_id
						and	rownum = 1;
					
					if not isnull( rows ) and rows > 0 then
						a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is associated with one or more history events.  in order to delete this parcel, first delete the " + &
							"history events for this parcel.  after doing so, re-attempt to delete the parcel."
						return false
					end if
					
					// pt parcel responsibility
					setnull( rows )
					select		count('x')
					into		:rows
					from		pt_parcel_responsibility
					where	parcel_id = :a_parcel_id
						and	rownum = 1;
					
					if not isnull( rows ) and rows > 0 then
						a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is associated with one or more responsibilities.  in order to delete this parcel, first delete the " + &
							"responsibilities for this parcel.  after doing so, re-attempt to delete the parcel."
						return false
					end if
				end if
			
			////
			////	check on ledger use of this parcel, here, and act accordingly.
			////
				if not del_user_input then
					// pt ledger
					setnull( rows )
					select		count('x')
					into		:rows
					from		pt_ledger
					where	parcel_id = :a_parcel_id
						and	rownum = 1;
					
					if not isnull( rows ) and rows > 0 then
						a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is associated with one or more ledger items.  in order to delete this parcel, first delete the " + &
							"ledger items for this parcel (if they are user input).  after doing so, re-attempt to delete the parcel."
						return false
					end if
					
					// pt preallo ledger
					setnull( rows )
					select		count('x')
					into		:rows
					from		pt_preallo_ledger
					where	parcel_id = :a_parcel_id
						and	rownum = 1;
					
					if not isnull( rows ) and rows > 0 then
						a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is associated with one or more preallo ledger items.  in order to delete this parcel, first delete " + &
							"the preallo ledger items for this parcel (if they are user input).  after doing so, re-attempt to delete the parcel."
						return false
					end if
				else
					// pt ledger
					setnull( rows )
					select		count('x')
					into		:rows
					from		pt_ledger ptl
					where	ptl.parcel_id = :a_parcel_id
						and	(			ptl.user_input = 0
									or		exists
												(	select		'x'
													from		pt_accrual_charge ac
													where	ptl.property_tax_ledger_id = ac.property_tax_ledger_id
												)
									or		exists
												(	select		'x'
													from		pt_actuals act
													where	ptl.property_tax_ledger_id = act.property_tax_ledger_id
												)
								)
						and	rownum = 1;
					
					if not isnull( rows ) and rows > 0 then
						a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is associated with one or more system-generated ledger items and cannot be deleted.  (or, " + &
							"this parcel, while having only user input ledger items has been used in a monthly accrual and/or payment and cannot be deleted.)"
						return false
					end if
					
					// pt preallo ledger
					setnull( rows )
					select		count('x')
					into		:rows
					from		pt_preallo_ledger
					where	parcel_id = :a_parcel_id
						and	user_input = 0
						and	rownum = 1;
					
					if not isnull( rows ) and rows > 0 then
						a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") is associated with one or more system-generated preallo ledger items and cannot be deleted."
						return false
					end if
				end if
				
				// pt incremental base year - just in case there's some kind of data problem - if there are rows in here we should fail the ledger test above before getting here
				setnull( rows )
				select		count('x')
				into		:rows
				from		pt_incremental_base_year
				where	parcel_id = :a_parcel_id
					and	rownum = 1;
				
				if not isnull( rows ) and rows > 0 then
					a_msg = "the parcel selected to delete (parcel id = " + string( a_parcel_id ) + ") has been used in an incremental allocation and cannot be deleted."
					return false
				end if

////
////	see if this is a one-to-one parcel.  and, if so see if this guy has composites.
////
			setnull( td_id )
			
			select		td.tax_district_id
			into		:td_id
			from		pt_parcel prcl,
						prop_tax_district td
			where	prcl.parcel_id = :a_parcel_id
				and	prcl.tax_district_id = td.tax_district_id
				and	nvl( td.is_parcel_one_to_one, 0 ) = 1;

////
////	do the delete.
////
			////
			////	attributes
			////
				if del_attrib then
					// pt parcel appeal
					delete from pt_parcel_appeal_event
						where	parcel_id = :a_parcel_id;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "an error occurred while deleting the parcel (appeal event).  if you continue to receive this error please contact your internal powerplant support or the powerplan " + &
							"property tax support desk.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
					
					delete from pt_parcel_appeal
						where	parcel_id = :a_parcel_id;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "an error occurred while deleting the parcel (appeal).  if you continue to receive this error please contact your internal powerplant support or the powerplan property " + &
							"tax support desk.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
					
					// pt parcel appraisal
					delete from pt_parcel_appraisal
						where	parcel_id = :a_parcel_id;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "an error occurred while deleting the parcel (appraisal).  if you continue to receive this error please contact your internal powerplant support or the powerplan " + &
							"property tax support desk.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
					
					// pt parcel auto adjust
					delete from pt_parcel_auto_adjust
						where	parcel_id = :a_parcel_id;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "an error occurred while deleting the parcel (automatic adjustments).  if you continue to receive this error please contact your internal powerplant support or the " + &
							"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
					
					// pt parcel geography
					delete from pt_parcel_geography
						where	parcel_id = :a_parcel_id;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "an error occurred while deleting the parcel (geography).  if you continue to receive this error please contact your internal powerplant support or the powerplan " + &
							"property tax support desk.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
					
					// pt parcel history
					delete from pt_parcel_history
						where	parcel_id = :a_parcel_id;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "an error occurred while deleting the parcel (history).  if you continue to receive this error please contact your internal powerplant support or the powerplan property " + &
							"tax support desk.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
					
					// pt parcel responsibility
					delete from pt_parcel_responsibility
						where	parcel_id = :a_parcel_id;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "an error occurred while deleting the parcel (responsibility).  if you continue to receive this error please contact your internal powerplant support or the powerplan " + &
							"property tax support desk.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
				end if
			
			////
			////	user input ledger items
			////
				if del_user_input then
					// make sure we're not locked
					sqls =		"	select		ptl.prop_tax_company_id prop_tax_company_id, " + "~r~n" + &
								"				ptl.state_id state_id, " + "~r~n" + &
								"				ptlty.tax_year tax_year " + "~r~n" + &
								"	from		pt_ledger ptl, " + "~r~n" + &
								"				pt_ledger_tax_year ptlty " + "~r~n" + &
								"	where	ptl.parcel_id = " + string( a_parcel_id ) + " " + "~r~n" + &
								"		and	ptl.property_tax_ledger_id = ptlty.property_tax_ledger_id " + "~r~n" + &
								"	" + "~r~n" + &
								"	union " + "~r~n" + &
								"	" + "~r~n" + &
								"	select		preallo.prop_tax_company_id prop_tax_company_id, " + "~r~n" + &
								"				preallo.state_id state_id, " + "~r~n" + &
								"				preallo.tax_year tax_year " + "~r~n" + &
								"	from		pt_preallo_ledger preallo " + "~r~n" + &
								"	where	preallo.parcel_id = " + string( a_parcel_id ) + " "
					
					ds_lock = create uo_ds_top
					rtn_str = f_create_dynamic_ds( ds_lock, "grid", sqls, sqlca, true )
					
					if rtn_str <> "ok" then
						a_msg = "an error occurred while retrieving the ledger years for this parcel.  if you continue to receive this error please contact your internal powerplant support or the " + &
							"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
						destroy ds_lock
						return false
					end if
					
					if ds_lock.rowcount() > 0 then
						uo_taxyear = create nvo_ptc_logic_taxyear
						maxi = ds_lock.rowcount()
						
						for i = 1 to maxi
							ptco_id = ds_lock.getitemnumber( i, 1 )
							st_id = ds_lock.getitemstring( i, 2 )
							ty = ds_lock.getitemnumber( i, 3 )
							
							if uo_taxyear.of_istaxyearlocked( ty, ptco_id, st_id ) then
								a_msg = "one or more of the tax years for which this parcel (parcel id = " + string( a_parcel_id ) + ") has ledger items is locked; the parcel's ledger items and, therefore " + &
									"the parcel, cannot be deleted."
								destroy ds_lock
								destroy uo_taxyear
								return false
							end if
						next
						
						destroy uo_taxyear
						
						// pt case calc authority
						delete from pt_case_calc_authority cca
							where	cca.property_tax_ledger_id in
											(	select		ptl.property_tax_ledger_id
												from		pt_ledger ptl
												where	ptl.parcel_id = :a_parcel_id
													and	ptl.user_input <> 0
											);
						
						if sqlca.sqlcode <> 0 then
							a_msg = "an error occurred while deleting the parcel (case calc auth).  if you continue to receive this error please contact your internal powerplant support or the " + &
								"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
							destroy ds_lock
							return false
						end if
						
						//pt case calc
						delete from pt_case_calc cc
							where	cc.property_tax_ledger_id in
											(	select		ptl.property_tax_ledger_id
												from		pt_ledger ptl
												where	ptl.parcel_id = :a_parcel_id
													and	ptl.user_input <> 0
											);
						
						if sqlca.sqlcode <> 0 then
							a_msg = "an error occurred while deleting the parcel (case calc).  if you continue to receive this error please contact your internal powerplant support or the powerplan " + &
								"property tax support desk.~n~nerror: " + sqlca.sqlerrtext
							destroy ds_lock
							return false
						end if
						
						//pt ledger transfer
						sqls =		"	select		distinct trans.transfer_id transfer_id, " + "~r~n" + &
									"				trans.tax_year tax_year " + "~r~n" + &
									"	from		pt_ledger_transfer trans, " + "~r~n" + &
									"				pt_ledger ptl " + "~r~n" + &
									"	where	ptl.parcel_id = " + string( a_parcel_id ) + " " + "~r~n" + &
									"		and	ptl.user_input <> 0 " + "~r~n" + &
									"		and	( trans.from_ledger_id = ptl.property_tax_ledger_id or trans.to_ledger_id = ptl.property_tax_ledger_id ) "
						
						ds_trans = create uo_ds_top
						rtn_str = f_create_dynamic_ds( ds_trans, "grid", sqls, sqlca, true )
						
						if rtn_str <> "ok" then
							a_msg = "an error occurred while retrieving the ledger transfers for this parcel.  if you continue to receive this error please contact your internal powerplant support or the " + &
								"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
							destroy ds_lock
							destroy ds_trans
							return false
						end if
						
						if ds_trans.rowcount() > 0 then
							uo_ledger = create nvo_ptc_logic_ledger
							maxi = ds_trans.rowcount()
							
							for i = 1 to maxi
								trans_id = ds_trans.getitemnumber( i, 1 )
								ty = ds_trans.getitemnumber( i, 2 )
								
								if uo_ledger.of_deletetransfer( ty, trans_id, a_msg ) <> 1 then
									a_msg = "an error occurred while deleting the parcel (transfers).~n~n" + a_msg
									destroy ds_lock
									destroy ds_trans
									destroy uo_ledger
									return false
								end if
							next
							
							destroy uo_ledger
						end if
						
						destroy ds_trans
						
						//pt ledger adjustment
						delete from pt_ledger_adjustment adj
							where	adj.property_tax_ledger_id in
											(	select		ptl.property_tax_ledger_id
												from		pt_ledger ptl
												where	ptl.parcel_id = :a_parcel_id
													and	ptl.user_input <> 0
											);
						
						if sqlca.sqlcode <> 0 then
							a_msg = "an error occurred while deleting the parcel (ledger adjustment).  if you continue to receive this error please contact your internal powerplant support or the " + &
								"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
							destroy ds_lock
							return false
						end if
						
						//pt_ledger_tax_year
						delete from pt_ledger_tax_year ptlty
							where	ptlty.property_tax_ledger_id in
											(	select		ptl.property_tax_ledger_id
												from		pt_ledger ptl
												where	ptl.parcel_id = :a_parcel_id
													and	ptl.user_input <> 0
											);
						
						if sqlca.sqlcode <> 0 then
							a_msg = "an error occurred while deleting the parcel (ledger tax year).  if you continue to receive this error please contact your internal powerplant support or the " + &
								"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
							destroy ds_lock
							return false
						end if
						
						//pt_ledger
						delete from pt_ledger ptl
							where	ptl.parcel_id = :a_parcel_id
								and	ptl.user_input <> 0;
						
						if sqlca.sqlcode <> 0 then
							a_msg = "an error occurred while deleting the parcel (ledger).  if you continue to receive this error please contact your internal powerplant support or the powerplan " + &
								"property tax support desk.~n~nerror: " + sqlca.sqlerrtext
							destroy ds_lock
							return false
						end if
						
						// pt preallo transfer
						sqls =		"	select		distinct trans.transfer_id transfer_id " + "~r~n" + &
									"	from		pt_preallo_transfer trans, " + "~r~n" + &
									"				pt_preallo_ledger pre " + "~r~n" + &
									"	where	pre.parcel_id = " + string( a_parcel_id ) + " " + "~r~n" + &
									"		and	pre.user_input <> 0 " + "~r~n" + &
									"		and	( trans.from_preallo_id = pre.preallo_ledger_id or trans.to_preallo_id = pre.preallo_ledger_id ) "
						
						ds_pre_trans = create uo_ds_top
						rtn_str = f_create_dynamic_ds( ds_pre_trans, "grid", sqls, sqlca, true )
						
						if rtn_str <> "ok" then
							a_msg = "an error occurred while retrieving the preallo transfers for this parcel.  if you continue to receive this error please contact your internal powerplant support or the " + &
								"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
							destroy ds_lock
							destroy ds_pre_trans
							return false
						end if
						
						if ds_pre_trans.rowcount() > 0 then
							uo_preallo = create nvo_ptc_logic_preallo_ledger
							maxi = ds_pre_trans.rowcount()
							
							for i = 1 to maxi
								trans_id = ds_pre_trans.getitemnumber( i, 1 )
								
								if uo_preallo.of_deletetransfer( trans_id, a_msg ) <> 1 then
									a_msg = "an error occurred while deleting the parcel (preallo transfers).~n~n" + a_msg
									destroy ds_lock
									destroy ds_pre_trans
									destroy uo_preallo
									return false
								end if
							next
							
							destroy uo_preallo
						end if
						
						destroy ds_pre_trans
						
						// pt preallo adjustment
						delete from pt_preallo_adjustment adj
							where	adj.preallo_ledger_id in
											(	select		pre.preallo_ledger_id
												from		pt_preallo_ledger pre
												where	pre.parcel_id = :a_parcel_id
													and	pre.user_input <> 0
											);
						
						if sqlca.sqlcode <> 0 then
							a_msg = "an error occurred while deleting the parcel (preallo adjustments).  if you continue to receive this error please contact your internal powerplant support or the " + &
								"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
							destroy ds_lock
							return false
						end if
						
						// pt preallo ledger
						delete from pt_preallo_ledger pre
							where	pre.parcel_id = :a_parcel_id
								and	pre.user_input <> 0;
						
						if sqlca.sqlcode <> 0 then
							a_msg = "an error occurred while deleting the parcel (preallo ledger).  if you continue to receive this error please contact your internal powerplant support or the powerplan " + &
								"property tax support desk.~n~nerror: " + sqlca.sqlerrtext
							destroy ds_lock
							return false
						end if
					end if
					
					destroy ds_lock
				end if
			
			////
			////	pt parcel flex fields
			////
				delete from pt_parcel_flex_fields
					where	parcel_id = :a_parcel_id;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "an error occurred while deleting the parcel (flex fields).  if you continue to receive this error please contact your internal powerplant support or the powerplan property " + &
						"tax support desk.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
			
			////
			////	pt parcel
			////
				delete from pt_parcel
					where	parcel_id = :a_parcel_id;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "an error occurred while deleting the parcel.  if you continue to receive this error please contact your internal powerplant support or the powerplan property tax support " + &
						"desk.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
			
			////
			////	one to one tax districts.
			////
				if not isnull( td_id ) then
					// tax auth prop tax dist
					delete from tax_auth_prop_tax_dist
						where	tax_district_id = :td_id;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "an error occurred while deleting the parcel (auth-dist for 1-to-1).  if you continue to receive this error please contact your internal powerplant support or the " + &
							"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
					
					// property tax authority : composites
					delete from pt_levy_rate
						where	tax_authority_id in
										(	select		tax_authority_id
											from		property_tax_authority
											where	tax_authority_type_id = -100
												and	tax_district_id = :td_id
										);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "an error occurred while deleting the parcel (levy rates for 1-to-1, composites).  if you continue to receive this error please contact your internal powerplant support " + &
							"or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
					
					delete from property_tax_authority
						where	tax_authority_type_id = -100
							and	tax_district_id = :td_id;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "an error occurred while deleting the parcel (composites for 1-to-1).  if you continue to receive this error please contact your internal powerplant support or the " + &
							"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
					
					// pt district equalization
					delete from pt_district_equalization
						where	tax_district_id = :td_id;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "an error occurred while deleting the parcel (equalizations for 1-to-1).  if you continue to receive this error please contact your internal powerplant support or the " + &
							"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
					
					// pt reserve factor dist pcts
					delete from pt_reserve_factor_dist_pcts
						where	tax_district_id = :td_id;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "an error occurred while deleting the parcel (% goods for 1-to-1).  if you continue to receive this error please contact your internal powerplant support or the " + &
							"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
					
					// pt assessed value tax district
					delete from pt_assessed_value_tax_district
						where	tax_district_id = :td_id;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "an error occurred while deleting the parcel (dist assessed val for 1-to-1).  if you continue to receive this error please contact your internal powerplant support or the " + &
							"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
					
					// prop tax district
					delete from prop_tax_district
						where	tax_district_id = :td_id;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "an error occurred while deleting the parcel (tax dist for 1-to-1).  if you continue to receive this error please contact your internal powerplant support or the " + &
							"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
						return false
					end if
				end if

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parceldelete ( a_parcel_id[], ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_parceldelete()
 **	
 **	deletes the parcels (subject to system option conditions).
 **	
 **	parameters	:		:	(a_parcel_id[]) the parcels which should be deleted.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcels were deleted successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi

////
////	loop over the parcels and process each delete.
////
			maxi = upperbound( a_parcel_id[] )
			
			for i = 1 to maxi
				if not this.of_parceldelete( a_parcel_id[i], a_msg ) then return false
			next

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_assessmentupdateeligibility ( a_case_id, boolean a_is_delete, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_assessmentupdateeligibility()
 **	
 **	checks the eligibility of assessments being updated.
 **	
 **	important: prior to calling this function, the parcel / assessment group combinations being updated must be populated in pt_temp_parcels_asmt_gp.
 **	
 **	this function will check against the case being locked and will check other system options related to updating assessments.  if any of the assessments cannot be updated for some reason,
 **	the function will return an error message (and false).
 **	
 **	parameters	:		:	(a_case_id) the case for which the assessments will be updated.
 **						boolean		:	(a_is_delete) if this "update" is really a delete (i.e., deleting the assessment.).  different validation logic might apply in the case of deletes.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the assessments are eligible for update.
 **											false	:	the assessments are not eligible for update.  check the a_msg parameter to get the error message.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi, rows
		ty, stmt_ay_id
		ptco_id, ag_id
	string	st_id
	string	cs_desc, ptco_desc, st_desc, ag_desc
	string	sqls, rtn_str, sysval

/*
 *	misc objects.
 */
	uo_ds_top ds_ldg
	uo_ds_top ds_agc
	uo_ds_top ds_paid

////
////	make sure this case is not locked.
////
			if i_uo_case.of_iscaselocked( a_case_id ) then
				a_msg = "this case has been locked and cannot be modified."
				return false
			end if

////
////	get the case description for writing to message boxes.
////
			select		description
			into		:cs_desc
			from		pt_case
			where	case_id = :a_case_id;

////
////	make sure the assessment groups we're importing into are set up for the given case / pt company / state combinations.  don't worry about this if we're deleting.
////
			if not a_is_delete then
				sqls =		"	select		prcl.prop_tax_company_id, " + "~r~n" + &
							"				prcl.state_id, " + "~r~n" + &
							"				temp_pag.assessment_group_id " + "~r~n" + &
							"	from		pt_temp_parcels_asmt_gp temp_pag, " + "~r~n" + &
							"				pt_parcel prcl " + "~r~n" + &
							"	where	temp_pag.parcel_id = prcl.parcel_id " + "~r~n" + &
							"	" + "~r~n" + &
							"	minus " + "~r~n" + &
							"	" + "~r~n" + &
							"	select		agc.prop_tax_company_id, " + "~r~n" + &
							"				agc.state_id, " + "~r~n" + &
							"				agc.assessment_group_id " + "~r~n" + &
							"	from		pt_assessment_group_case agc " + "~r~n" + &
							"	where	agc.case_id = " + string( a_case_id ) + " " + "~r~n" + &
							"		and	agc.assessment_group_id in " + "~r~n" + &
							"					(	select		assessment_group_id " + "~r~n" + &
							"						from		pt_temp_parcels_asmt_gp " + "~r~n" + &
							"					) "
				
				ds_agc = create uo_ds_top
				rtn_str = f_create_dynamic_ds( ds_agc, "grid", sqls, sqlca, true )
				
				if rtn_str <> "ok" then
					a_msg = "an error occurred while checking to ensure the assessment groups are set-up properly for the assessments being updated.  the specific error returned is below.  if you " + &
						"continue to receive this error contact your internal powerplant support or the powerplan property tax support desk.~n~n" + rtn_str
					destroy ds_agc
					return false
				end if
				
				if ds_agc.rowcount() > 0 then
					maxi = ds_agc.rowcount()
					
					if maxi <= 10 then
						a_msg = "the following prop tax company / state / assessment group combinations are not valid for the '" + cs_desc + "' case:~n~n"
						
						for i = 1 to maxi
							ptco_id = ds_agc.getitemnumber( i, 1 )
							st_id = ds_agc.getitemstring( i, 2 )
							ag_id = ds_agc.getitemnumber( i, 3 )
							
							select		description
							into		:ptco_desc
							from		pt_company
							where	prop_tax_company_id = :ptco_id;
							
							select		description
							into		:st_desc
							from		state
							where	state_id = :st_id;
							
							select		description
							into		:ag_desc
							from		pt_assessment_group
							where	assessment_group_id = :ag_id;
							
							a_msg = a_msg + "~t" + ptco_desc + " / " + st_desc + " / " + ag_desc + "~n"
						next
						
						a_msg = a_msg + "~nset-up these combinations in the parcel center (assessment groups workspace) before updating."
					else
						a_msg = string( maxi ) + " of the prop tax company / state / assessment group combinations for which assessments are being updated are not valid for the '" + cs_desc + &
							"' case.  set-up these combinations in the parcel center (assessment groups workspace) before updating."
					end if
					
					destroy ds_agc
					return false
				end if
				
				destroy ds_agc
			end if

////
////	see if we should prevent assessments where there is no corresponding ledger item.  we can't deal with the "warn" option here, leave that to the calling program/function to handle correctly;
////	but we will check for the "yes" option here (i.e., the prevent option).  only worry about this if we're not deleting.
////
			if not a_is_delete then
				sysval = upper( trim( i_uo_systemoptions.of_getsystemoption( "assessments - entry - restrict assessment entry by ledger items" ) ) )
				
				if sysval = "yes" then
					select		tax_year
					into		:ty
					from		pt_case
					where	case_id = :a_case_id;
					
					sqls =		"	select		count('x') " + "~r~n" + &
								"	" + "~r~n" + &
								"	from		pt_temp_parcels_asmt_gp temp_pag " + "~r~n" + &
								"	" + "~r~n" + &
								"	where	not exists " + "~r~n" + &
								"					(	select		'x' " + "~r~n" + &
								"						from		pt_ledger_tax_year ptlty, " + "~r~n" + &
								"									pt_ledger ptl, " + "~r~n" + &
								"									pt_assessment_group_types agt " + "~r~n" + &
								"						where	ptlty.tax_year = " + string( ty ) + " " + "~r~n" + &
								"							and	ptlty.property_tax_ledger_id = ptl.property_tax_ledger_id " + "~r~n" + &
								"							and	temp_pag.parcel_id = ptl.parcel_id " + "~r~n" + &
								"							and	ptl.property_tax_type_id = agt.property_tax_type_id " + "~r~n" + &
								"							and	ptl.prop_tax_company_id = agt.prop_tax_company_id " + "~r~n" + &
								"							and	ptl.state_id = agt.state_id " + "~r~n" + &
								"							and	agt.case_id = " + string( a_case_id ) + " " + "~r~n" + &
								"							and	temp_pag.assessment_group_id = agt.assessment_group_id " + "~r~n" + &
								"					) " + "~r~n" + &
								"		and	rownum = 1 "
					
					ds_ldg = create uo_ds_top
					rtn_str = f_create_dynamic_ds( ds_ldg, "grid", sqls, sqlca, true )
					
					if rtn_str <> "ok" then
						a_msg = "an error occurred while checking to ensure ledger items exist for all assessments being updated.  the specific error returned is below.  if you continue to receive this " + &
							"error contact your internal powerplant support or the powerplan property tax support desk.~n~n" + rtn_str
						destroy ds_ldg
						return false
					end if
					
					if ds_ldg.rowcount() = 1 then
						if ds_ldg.getitemnumber( 1, 1 ) > 0 then
							a_msg = "one or more of the parcel assessments being updated does not have an associated ledger item in the ledger.  the system options are set such that assessments " + &
								"may only be entered if there is a corresponding ledger item related to the assessment's group in the ledger."
							destroy ds_ldg
							return false
						end if
					end if
					
					destroy ds_ldg
				end if
			end if

////
////	see if we should allow assessment changes based on the "assessments - allow changes to assessments for paid bills" system option.
////
			sysval = upper( trim( i_uo_systemoptions.of_getsystemoption( "assessments - allow changes to assessments for paid bills" ) ) )
			
			if sysval = "no" or sysval = "allow for partially paid bills" then
				////
				////	see if this is a final case related to an assessment year.  if not, we don't need to perform the check.  if it is a final case, check the conditions of the parcels against the system
				////	option selection.
				////
					setnull( rows )
					
					select		count('x')
					into		:rows
					from		ptv_final_cases
					where	case_id = :a_case_id;
					
					if rows > 0 then
						if rows = 1 then
							setnull( stmt_ay_id )
							
							select		assessment_year_id
							into		:stmt_ay_id
							from		ptv_final_cases
							where	case_id = :a_case_id;
						else
							a_msg = "unable to update assessments.  the case for which you are updating assessments has been assigned as a 'final case' to more than one assessment year.  a case " + &
								"can, at most, be assigned to one assessment year as it's 'final case'.  please update the assessment year - final case assignments to adhere to this rule before continuing."
							return false
						end if
						
						sqls =					"	select		count('x') " + "~r~n" + &
												"	" + "~r~n" + &
												"	from		pt_statement_statement_year ssy, " + "~r~n" + &
												"				pt_statement_line sl, " + "~r~n" + &
												"				pt_temp_parcels_asmt_gp temp_pag " + "~r~n" + &
												"	" + "~r~n" + &
												"	where	ssy.assessment_year_id = " + string( stmt_ay_id ) + " " + "~r~n" + &
												"		and	ssy.statement_id = sl.statement_id " + "~r~n" + &
												"		and	ssy.statement_year_id = sl.statement_year_id " + "~r~n" + "~r~n" + &
												"		and	sl.parcel_id = temp_pag.parcel_id " + "~r~n"
						
						if sysval = "no" then
							sqls = sqls +	"		and	ssy.statement_status_id in ( 5, 6, 7, 8 ) "
						elseif sysval = "allow for partially paid bills" then
							sqls = sqls +	"		and	ssy.statement_status_id in ( 7, 8 ) "
						end if
						
						sqls = sqls +		"		and	rownum = 1 "
						
						ds_paid = create uo_ds_top
						rtn_str = f_create_dynamic_ds( ds_paid, "grid", sqls, sqlca, true )
						
						if rtn_str <> "ok" then
							a_msg = "an error occurred while checking to see if any assessments being updated are for parcels which have already been paid.  the specific error returned is below.  if " + &
								"you continue to receive this error contact your internal powerplant support or the powerplan property tax support desk.~n~n" + rtn_str
							destroy ds_paid
							return false
						end if
						
						if ds_paid.rowcount() = 1 then
							if ds_paid.getitemnumber( 1, 1 ) > 0 then
								sqls =					"	select		distinct prcl.parcel_number parcel_number " + "~r~n" + &
														"	" + "~r~n" + &
														"	from		pt_statement_statement_year ssy, " + "~r~n" + &
														"				pt_statement_line sl, " + "~r~n" + &
														"				pt_temp_parcels_asmt_gp temp_pag, " + "~r~n" + &
														"				pt_parcel prcl " + "~r~n" + &
														"	" + "~r~n" + &
														"	where	ssy.assessment_year_id = " + string( stmt_ay_id ) + " " + "~r~n" + &
														"		and	ssy.statement_id = sl.statement_id " + "~r~n" + &
														"		and	ssy.statement_year_id = sl.statement_year_id " + "~r~n" + "~r~n" + &
														"		and	sl.parcel_id = temp_pag.parcel_id " + "~r~n" + &
														"		and	sl.parcel_id = prcl.parcel_id " + "~r~n" + &
														"		and	temp_pag.parcel_id = prcl.parcel_id " + "~r~n"
								
								if sysval = "no" then
									sqls = sqls +	"		and	ssy.statement_status_id in ( 5, 6, 7, 8 ) "
								elseif sysval = "allow for partially paid bills" then
									sqls = sqls +	"		and	ssy.statement_status_id in ( 7, 8 ) "
								end if
								
								rtn_str = f_create_dynamic_ds( ds_paid, "grid", sqls, sqlca, true )
								
								if rtn_str <> "ok" then
									a_msg = "an error occurred while retrieving a list of the parcels which have assessments being updated but have already been included on paid bills.  the specific " + &
										"error returned is below.  if you continue to receive this error contact your internal powerplant support or the powerplan property tax support desk.~n~n" + rtn_str
									destroy ds_paid
									return false
								end if
								
								maxi = ds_paid.rowcount()
								
								if maxi <= 10 then
									a_msg = "the following parcels have already been paid "
									
									if sysval = "no" then
										a_msg = a_msg + "(either partially or fully) "
									elseif sysval = "allow for partially paid bills" then
										a_msg = a_msg + "(fully; partially paid parcels are eligible to be updated) "
									end if
									
									a_msg = a_msg + "and, based on the current system options, their assessments cannot be updated for this case:" + "~r~n" + "~r~n"
									
									for i = 1 to maxi
										a_msg = a_msg + ds_paid.getitemstring( i, 1 )
										if i <> maxi then a_msg = a_msg + "~r~n"
									next
								else
									a_msg = string( maxi ) + " of the parcels having their assessments updated have already been paid "
									
									if sysval = "no" then
										a_msg = a_msg + "(either partially or fully) "
									elseif sysval = "allow for partially paid bills" then
										a_msg = a_msg + "(fully; partially paid parcels are eligible to be updated) "
									end if
									
									a_msg = a_msg + "and, based on the current system options, their assessments cannot be updated for this case."
								end if
								
								destroy ds_paid
								return false
							end if
						end if
						
						destroy ds_paid
					end if
			end if

////
////	we're cool to update the assessment.  clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_assessmentupdateeligibility ( a_case_id,  a_prcl_id[],  a_ag_id[], boolean a_is_delete, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_assessmentupdateeligibility()
 **	
 **	checks the eligibility of assessments being updated.  this function will check against the case being locked and will check other system options related to updating assessments.  if any of
 **	the assessments cannot be updated for some reason, the function will return an error message (and false).
 **	
 **	parameters	:		:	(a_case_id) the case for which the assessments will be updated.
 **							:	(a_parcel_id[]) the parcel(s) for which the assessments will be updated.  should be in tandem with a_ag_id[]; that is the same number of nodes should be
 **											in both arrays and index 1 in parcel corresponds to index 1 in assessment group (that is, a_parcel_id[1] / a_ag_id[1] is one of the parcel / assessment
 **											group combinations being updated).
 **							:	(a_ag_id[]) the assessment group(s) corresponding to a_parcel_id[] for which the assessments will be updated.  see the note above under a_parcel_id[]
 **											for discussion of how these two arrays relate.
 **						boolean		:	(a_is_delete) if this "update" is really a delete (i.e., deleting the assessment.).  different validation logic might apply in the case of deletes.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the assessments are eligible for update.
 **											false	:	the assessments are not eligible for update.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi

////
////	make sure the parameters are ok.
////
			maxi = upperbound( a_prcl_id[] )
			if maxi = 0 then return true
			
			if upperbound( a_prcl_id[] ) <> upperbound( a_ag_id[] ) then
				a_msg = "internal error.  mismatch between parcels and assessment groups being updated when checking update eligibility.  if you continue to receive this error contact your internal " + &
					"powerplant support or the powerplan property tax support desk."
				return false
			end if

////
////	build temp parcels ag and call the other version of this function to do the validation.
////
			delete from pt_temp_parcels_asmt_gp;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error occurred during validation while deleting the temp parcels / assessment groups stored off from a prior run.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if
			
			for i = 1 to maxi
				insert into pt_temp_parcels_asmt_gp ( parcel_id, assessment_group_id ) values ( :a_prcl_id[i], :a_ag_id[i] );
				
				if sqlca.sqlcode <> 0 then
					a_msg = "error occurred during validation while inserting the temp parcels / assessment groups.  no changes made.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
			next

////
////	do the validation.
////
			if not this.of_assessmentupdateeligibility( a_case_id, a_is_delete, a_msg ) then return false

////
////	delete our temp parcels so we don't cause any errors in other functions that might use it later on before returning.
////
			delete from pt_temp_parcels_asmt_gp;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "error occurred while deleting the temp parcels / assessment groups stored off during validation.  no changes made.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if

////
////	clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_assessmentupdateeligibility ( a_case_id,  a_parcel_id,  a_ag_id, boolean a_is_delete, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_assessmentupdateeligibility()
 **	
 **	checks the eligibility of assessments being updated.  this function will check against the case being locked and will check other system options related to updating assessments.  if any of
 **	the assessments cannot be updated for some reason, the function will return an error message (and false).
 **	
 **	parameters	:		:	(a_case_id) the case for which the assessment will be updated.
 **							:	(a_parcel_id) the parcel for which the assessment will be updated.
 **							:	(a_ag_id) the assessment group for which the assessment will be updated.
 **						boolean		:	(a_is_delete) if this "update" is really a delete (i.e., deleting the assessment.).  different validation logic might apply in the case of deletes.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the assessment is eligible for update.
 **											false	:	the assessment is not eligible for update.  check the a_msg parameter to get the error message.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		prcl_id[], ag_id[]

////
////	make sure the parameters are ok.
////
			if isnull( a_parcel_id ) or isnull( a_ag_id ) then
				a_msg = "internal error.  invalid parcels and assessment groups passed when checking update eligibility.  if you continue to receive this error contact your internal powerplant " + &
					"or the powerplan property tax support desk."
				return false
			end if

////
////	do the validation.
////
			prcl_id[1] = a_parcel_id
			ag_id[1] = a_ag_id
			
			return this.of_assessmentupdateeligibility( a_case_id, prcl_id[], ag_id[], a_is_delete, a_msg )
end function

public function boolean of_assessmentdeleteforcase ( a_case_id, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_assessmentdeleteforcase()
 **	
 **	deletes all existing assessments for the given case.
 **	
 **	parameters	:		:	(a_case_id) the case for which the assessments should be deleted.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the assessments were deleted successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **
 ************************************************************************************************************************************************************/

////
////	don't error.
////
			if isnull( a_case_id ) then
				a_msg = "missing case.  if you continue to receive this error contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if

////
////	validate update eligibility.  before doing this we need to insert the parcel / assessment group combinations we're deleting into temp parcel ag.
////
			delete from pt_temp_parcels_asmt_gp;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while deleting prior parcel / assessment group combinations stored off prior to validation (during the case delete).  if you continue to receive this " + &
					"error contact your internal powerplant support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if
			
			insert into pt_temp_parcels_asmt_gp ( parcel_id, assessment_group_id )
				select		parcel_id, assessment_group_id
				from		pt_parcel_assessment
				where	case_id = :a_case_id;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while storing off parcel / assessment group combinations for validation (during the case delete).  if you continue to receive this error contact " + &
					"your internal powerplant support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				
				delete from pt_temp_parcels_asmt_gp;
				
				return false
			end if
			
			if not this.of_assessmentupdateeligibility( a_case_id, true, a_msg ) then return false
			
			delete from pt_temp_parcels_asmt_gp;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while deleting parcel / assessment group combinations stored for validation (during the case delete).  if you continue to receive this error " + &
					"contact your internal powerplant support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if

////
////	before we delete from parcel assessment we have to delete from case calc and case calc authority (as the deletes for these guys depend on parcel assessment).
////
			if not i_uo_case.of_deletecasecalc( a_case_id, "", a_msg ) then
				a_msg = "an error occurred while deleting the assessment to asset allocation.  if you continue to receive this error please contact your internal powerplant support or the powerplan " + &
					"property tax support desk.  the specific error returned is below:~n~n" + a_msg
				return false
			end if

////
////	do the delete.
////
			delete from pt_parcel_assessment pasmt
				where	pasmt.case_id = :a_case_id;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "an error occurred while deleting all parcel assessments for this case.  if you continue to receive this error please contact your internal powerplant support or the powerplan " + &
					"property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if

////
////	we were successful.  clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_assessmentcalcvalues ( a_prcl_id,  a_ag_id,  a_case_id,  a_fixed_field, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_assessmentcalcvalues()
 **	
 **	calculates two of the three "value" fields on the assessment table (i.e., assessment, equalized value, taxable_value) for given parcel and assessment group.  one of the "value" fields is
 **	identified as the "fixed" value and the other two values are calculated from it.  at the end, this function also updates the case calc and case calc authority tables for the changes.
 **	
 **	parameters	:		:	(a_prcl_id) the parcel for which we're calculating assessment values.
 **							:	(a_ag_id) the assessment group for which we're calculating assessment values.
 **							:	(a_case_id) the case for which we're calculating assessment values.
 **							:	(a_fixed_field) a constant identifying the field that is know - i.e., the field that all other fields will be calculated from.  the constants can be found as
 **											instance variables for this object starting with i_fixed_field_...
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the assessment values were calculated successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		prcl_id[], ag_id[]

////
////	stick the values in an array and call the version of this function taking in arrays.
////
			if isnull( a_prcl_id ) then return true
			if isnull( a_ag_id ) then return true
			
			prcl_id[1] = a_prcl_id
			ag_id[1] = a_ag_id
			
			return this.of_assessmentcalcvalues( prcl_id[], ag_id[], a_case_id, a_fixed_field, a_msg )
end function

public function boolean of_parcelresponsibilitydelete ( a_parcel_id,  a_resp_entity_id,  a_resp_type_id,  a_tax_year, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelresponsibilitydelete()
 **	
 **	deletes the passed in parcel responsibility.  passing in a null responsibile entity id means all responsibilities for the given parcel should be deleted.
 **	
 **	parameters	:		:	(a_parcel_id) the parcel for which the responsibility should be deleted.
 **							:	(a_resp_entity_id) the responsibile entity for which the responsibility should be deleted from the parcel.  a null value means all responsible entities should be deleted.
 **							:	(a_resp_type_id) the responsibility type for which the responsibility should be deleted from the parcel.  a null value means all responsibility types should be deleted.
 **							:	(a_tax_year) the tax year for which the responsibility should be deleted from the parcel.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel responsibility was deleted successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

////
////	validate.
////
		if isnull( a_parcel_id ) then
			a_msg = "an internal error occurred while attempting to delete parcel responsibilities.  no parcel was specified as the parcel from which to delete responsibilities.  if you continue to " + &
				"receive this error please contact your internal powerplant support or the powerplan property tax support desk."
			return false
		end if
		
		if isnull( a_tax_year ) then
			a_msg = "an internal error occurred while attempting to delete parcel responsibilities.  no tax year was specified as the tax year from which to delete responsibilities.  if you continue to " + &
				"receive this error please contact your internal powerplant support or the powerplan property tax support desk."
			return false
		end if

////
////	do the delete.  if the responsible entity id is null, delete all responsible entities for this parcel.  otherwise, just delete the specified responsible entity.
////	if the responsibility type id is null, delete all responsibility types for this parcel.  otherwise, just delete the specified type.
////
		if isnull( a_resp_entity_id ) and isnull( a_resp_type_id ) then
			delete from pt_parcel_responsibility presp
				where	presp.parcel_id = :a_parcel_id
				and		presp.tax_year = :a_tax_year;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "an error occurred while deleting all responsibile entities and types for the selected parcel and tax year.  if you continue to receive this error please contact your internal powerplant support or " + &
					"the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if
		elseif not isnull( a_resp_entity_id ) and  isnull( a_resp_type_id ) then
			delete from pt_parcel_responsibility presp
				where	presp.parcel_id = :a_parcel_id
				and		presp.tax_year = :a_tax_year
				and		presp.responsible_entity_id = :a_resp_entity_id;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "an error occurred while deleting the specified responsible entity for the selected parcel and tax year.  if you continue to receive this error please contact your internal powerplant " + &
					"support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if
		elseif isnull( a_resp_entity_id ) and not isnull( a_resp_type_id ) then
			delete from pt_parcel_responsibility presp
				where	presp.parcel_id = :a_parcel_id
				and		presp.tax_year = :a_tax_year
				and		presp.responsibility_type_id = :a_resp_type_id;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "an error occurred while deleting the specified responsibility type for the selected parcel and tax year.  if you continue to receive this error please contact your internal powerplant " + &
					"support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if
		else
			delete from pt_parcel_responsibility presp
				where	presp.parcel_id = :a_parcel_id
				and		presp.tax_year = :a_tax_year
				and		presp.responsibility_type_id = :a_resp_type_id
				and		presp.responsible_entity_id = :a_resp_entity_id;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "an error occurred while deleting the specified responsible entity and responsibility type for the selected parcel and tax year.  if you continue to receive this error please contact your internal powerplant " + &
					"support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
				return false
			end if
		end if

////
////	everything's ok so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_parcelresponsibilityedit (s_pt_parcel_responsibility as_resp[], ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelresponsibilityedit()
 **	
 **	edits the parcel responsiblity table for the given rows.  editing means inserts/updates/deletes as necessary.  this function will take care of doing the update or the insert correctly based on
 **	whether the relationship exists in the table or not.  if a percent responsible is null, the row will be deleted.
 **	
 **	parameters	:	ref s_pt_parcel_responsibility	:	(as_resp[]) the parcel responsiblity rows to edit.
 **						ref string								:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel responsibility was edited successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the of_parcelresponsibilityeditfromimport() function as both functions are used to update the
 **							pt_parcel_responsibility table.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi
		rows

////
////	loop over the responsibility rows and insert / update as necessary.
////
		maxi = upperbound( as_resp[] )
		
		for i = 1 to maxi
		////
		////	validate.
		////
				if isnull( as_resp[i].tax_year ) then
					a_msg = "'tax year' is a required field.  one of the parcel responsibilities to update is missing a tax year."
					return false
				end if
				
				if isnull( as_resp[i].parcel_id ) then
					a_msg = "'parcel' is a required field.  one of the parcel responsibilities to update is missing a parcel."
					return false
				end if
			
		////
		////	are we deleting?  if so, delete and skip the rest of the loop (which does the insert).
		////
				if isnull( as_resp[i].percent_responsible ) then
					if not this.of_parcelresponsibilitydelete( as_resp[i].parcel_id, as_resp[i].responsible_entity_id, as_resp[i].responsibility_type_id, as_resp[i].tax_year, a_msg ) then return false
					continue
				end if
				
		////
		////	if we're adding or updating, responsibility type and responsible entity are required.
		////
				if isnull( as_resp[i].responsibility_type_id ) then
					a_msg = "'responsibility type' is a required field.  one of the parcel responsibilities to update is missing a responsibility type."
					return false
				end if
				
				if isnull( as_resp[i].responsible_entity_id ) then
					a_msg = "'responsible entity' is a required field.  one of the parcel responsibilities to update is missing a responsible entity."
					return false
				end if
			
		////
		////	do we insert or update.
		////
				setnull( rows )
				select		count('x')
				into		:rows
				from		pt_parcel_responsibility presp
				where	presp.parcel_id = :as_resp[i].parcel_id
				and		presp.tax_year = :as_resp[i].tax_year
				and		presp.responsible_entity_id = :as_resp[i].responsible_entity_id
				and		presp.responsibility_type_id = :as_resp[i].responsibility_type_id;
				
				if isnull( rows ) or rows = 0 then
					insert into pt_parcel_responsibility 
						( parcel_id, tax_year, responsible_entity_id, responsibility_type_id, percent_responsible, lease_start_date, lease_end_date, entity_is_lessor, entity_is_lessee, reference_number )
					values
						(	:as_resp[i].parcel_id, :as_resp[i].tax_year, :as_resp[i].responsible_entity_id, :as_resp[i].responsibility_type_id, :as_resp[i].percent_responsible, :as_resp[i].lease_start_date, :as_resp[i].lease_end_date,
							:as_resp[i].entity_is_lessor, :as_resp[i].entity_is_lessee, :as_resp[i].reference_number
						);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while inserting a new parcel responsibility.  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				else
					update	pt_parcel_responsibility presp
					set			presp.percent_responsible = :as_resp[i].percent_responsible,
								presp.lease_start_date = :as_resp[i].lease_start_date,
								presp.lease_end_date = :as_resp[i].lease_end_date,
								presp.entity_is_lessor = :as_resp[i].entity_is_lessor,
								presp.entity_is_lessee = :as_resp[i].entity_is_lessee,
								presp.reference_number = :as_resp[i].reference_number
					where	presp.parcel_id = :as_resp[i].parcel_id
					and		presp.tax_year = :as_resp[i].tax_year
					and		presp.responsible_entity_id = :as_resp[i].responsible_entity_id
					and		presp.responsibility_type_id = :as_resp[i].responsibility_type_id;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while updating an existing parcel responsibility.  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				end if
		next

////
////	everything's ok so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_parcelresponsibleentityedit (ref s_pt_parcel_responsible_entity as_resp_ent, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelresponsibleentityedit()
 **	
 **	edits the parcel responsible entity table for the given data.  editing means insert/update as necessary.
 **	
 **	parameters	:	ref s_pt_parcel_responsible_entity	:	(as_resp_ent) the parcel responsible entity to edit.
 **						ref string									:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel responsible entity was edited successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the of_parcelresponsibleentityeditfromimport() function as both functions are used to update the
 **							pt_parcel_responsible_entity table.
 **
 ************************************************************************************************************************************************************/

////
////	validate.
////
		if isnull( as_resp_ent.description ) then
			a_msg = "'description' is a required field.  the parcel responsible entity to update is missing a description."
			return false
		end if
			
////
////	do we insert or update.  if the responsible_entity_id is null, we're inserting; get the next in line id.
////
		if isnull( as_resp_ent.responsible_entity_id ) then
			select		nvl( max( responsible_entity_id ), 0 ) + 1
			into		:as_resp_ent.responsible_entity_id
			from		pt_parcel_responsible_entity;
			
			insert into pt_parcel_responsible_entity ( responsible_entity_id, description, description, address_1, address_2, address_3, city, zip_code, state, phone_1, phone_2, contact_name, entity_number, external_code, tax_id_code, tax_id_number, prop_tax_company_id, notes )
				values
					(	:as_resp_ent.responsible_entity_id, :as_resp_ent.description, :as_resp_ent.description, :as_resp_ent.address_1, :as_resp_ent.address_2, :as_resp_ent.address_3,
						:as_resp_ent.city, :as_resp_ent.zip_code, :as_resp_ent.state, :as_resp_ent.phone_1, :as_resp_ent.phone_2, :as_resp_ent.contact_name, :as_resp_ent.entity_number, :as_resp_ent.external_code,
						:as_resp_ent.tax_id_code, :as_resp_ent.tax_id_number, :as_resp_ent.prop_tax_company_id, :as_resp_ent.notes
					);
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while inserting a new parcel responsible entity.  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
					"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		else
			update	pt_parcel_responsible_entity pre
			set			pre.description = :as_resp_ent.description,
						pre.description = :as_resp_ent.description,
						pre.address_1 = :as_resp_ent.address_1,
						pre.address_2 = :as_resp_ent.address_2,
						pre.address_3 = :as_resp_ent.address_3,
						pre.city = :as_resp_ent.city,
						pre.state = :as_resp_ent.state,
						pre.zip_code = :as_resp_ent.zip_code,
						pre.phone_1 = :as_resp_ent.phone_1,
						pre.phone_2 = :as_resp_ent.phone_2,
						pre.contact_name = :as_resp_ent.contact_name,
						pre.entity_number = :as_resp_ent.entity_number,
						pre.external_code = :as_resp_ent.external_code,
						pre.tax_id_code = :as_resp_ent.tax_id_code,
						pre.tax_id_number = :as_resp_ent.tax_id_number,
						pre.prop_tax_company_id  = :as_resp_ent.prop_tax_company_id,
						pre.notes = :as_resp_ent.notes
			where	pre.responsible_entity_id = :as_resp_ent.responsible_entity_id;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while updating the existing parcel responsible entity.  if you continue to receive this error contact your internal powerplant support or the " + &
					"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if

////
////	everything's ok so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_parcelgeographyfactorcopy ( a_tax_year_from,  a_tax_year_to,  a_geography_type_id, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelgeographyfactorcopy()
 **	
 **	copies percents from one tax year to another based on the passed-in criteria.
 **	
 **	parameters	:		:	(a_tax_year_from) the tax year to copy from
 **							:	(a_tax_year_to) the tax year to copy to
 **							:	(a_geography_type_id) the geography type to copy.  if null, copy for all geography types.
 **						ref string							:	(a_msg) i'll pass a friendly error message back using this parameter.  this error can be displayed straight to a message box.
 **	
 **	returns		:	boolean		:	true	:	the copy was successful.
 **											false	:	the copy failed; check the a_msg parameter for the reason.
 **	
 ************************************************************************************************************************************************************/

	count, rtn
string	check_sql, delete_sql, insert_sql, rtn_str

uo_ds_top	ds_count

////
////	make sure we have a from and to tax year.
////
		if isnull( a_tax_year_from ) then
			a_msg = "missing 'copy from' tax year."
			return false
		end if
		
		if isnull( a_tax_year_to ) then
			a_msg = "missing 'copy to' tax year."
			return false
		end if
		
////
////	check if there are existing percents.
////
		check_sql =						"select	count(*) " + "~r~n" + &
											"from		pt_parcel_geo_type_factors " + "~r~n" + &
											"where	tax_year = " + string( a_tax_year_to ) + " " + "~r~n"
							
		if not isnull( a_geography_type_id ) then
			check_sql = check_sql +	"and		geography_type_id = " + string( a_geography_type_id ) + " " + "~r~n"
		end if
		
////
////	retrieve the datastore to get the count.
////
		ds_count = create uo_ds_top
		rtn_str = f_create_dynamic_ds( ds_count, "grid", check_sql, sqlca, true )
		
		if rtn_str <> "ok" then
			a_msg = "an error occurred while checking whether percents exist in the 'copy to' tax year.  if you continue to receive this error contact your internal " + &
				"powerplant support or the powerplan property tax support desk.  the specific error (1) returned was: " + rtn_str
			destroy ds_count
			return false
		end if
		
		count = ds_count.getitemnumber( 1, 1 )
		
		if not isnull( count ) and count > 0 then
			rtn = messagebox( "delete existing percents?", "there are existing percents in the 'copy to' tax year for the given criteria.  would you like to delete and re-copy?", &
									question!, yesno!, 1 )
			if rtn = 2 then
				a_msg = "user aborted copy process."
				destroy ds_count
				return false
			end if
			
			delete_sql =							"delete from pt_parcel_geo_type_factors " + "~r~n" + &
													"where	tax_year = " + string( a_tax_year_to ) + "~r~n"
											
			if not isnull( a_geography_type_id ) then
				delete_sql = delete_sql +	"and		geography_type_id = " + string( a_geography_type_id ) + " " + "~r~n"
			end if
								
			execute immediate :delete_sql;
					
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while deleting the existing factors from 'pt parcel geography type factors'.  " + &
							"if you continue to receive this message contact your internal powerplant support or the powerplan property tax " + &
							"support desk.  the specific error returned was: " + sqlca.sqlerrtext
				destroy ds_count
				return false
			end if
		end if //existing percents
		
		destroy ds_count
		
////
////	now copy the percents.
////
		insert_sql =						"insert into pt_parcel_geo_type_factors " + "~r~n" + &
											"( tax_year, geography_type_id, age, percent ) " + "~r~n" + &
											"select	" + string( a_tax_year_to ) + ", " + "~r~n" + &
											"			geography_type_id, " + "~r~n" + &
											"			age, " + "~r~n" + &
											"			percent " + "~r~n" + &
											"from 	pt_parcel_geo_type_factors " + "~r~n" + &
											"where	tax_year = " + string( a_tax_year_from ) + "~r~n"
												
		if not isnull( a_geography_type_id ) then
			insert_sql = insert_sql +	"and		geography_type_id = " + string( a_geography_type_id ) + " " + "~r~n"
		end if
							
		execute immediate :insert_sql;
				
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while copying the factors into 'pt parcel geography type factors'.  " + &
						"if you continue to receive this message contact your internal powerplant support or the powerplan property tax " + &
						"support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	we were successful so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_parcelgeographyfactordelete ( a_tax_year,  a_geography_type_id,  a_age, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelgeographyfactordelete()
 **	
 **	deletes the passed in parcel geography type factor.
 **	
 **	parameters	:		:	(a_tax_year) the tax year for which the factor should be deleted.
 **							:	(a_geography_type_id) the geography type for which the factor should be deleted.
 **							:	(a_age) the age for which the factor should be deleted.
 **						ref string		:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the factor was deleted successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

////
////	validate.
////
		if isnull( a_tax_year ) then
			a_msg = "an internal error occurred while attempting to delete parcel geography type factors.  no tax year was specified as the tax year from which to delete factors.  if you continue to " + &
				"receive this error please contact your internal powerplant support or the powerplan property tax support desk."
			return false
		end if
		
		if isnull( a_geography_type_id ) then
			a_msg = "an internal error occurred while attempting to delete parcel geography type factors.  no geography type was specified as the geography type from which to delete factors.  if you continue to " + &
				"receive this error please contact your internal powerplant support or the powerplan property tax support desk."
			return false
		end if
		
		if isnull( a_age ) then
			a_msg = "an internal error occurred while attempting to delete parcel geography type factors.  no age was specified as the age from which to delete factors.  if you continue to " + &
				"receive this error please contact your internal powerplant support or the powerplan property tax support desk."
			return false
		end if
		
////
////	do the delete.
////
		delete from pt_parcel_geo_type_factors gtf
		where	gtf.tax_year = :a_tax_year
		and		gtf.geography_type_id = :a_geography_type_id
		and		gtf.age = :a_age;
	
		if sqlca.sqlcode <> 0 then
			a_msg = "an error occurred while deleting the factor for the selected tax year, geography type, and age.  if you continue to receive this error please contact your internal powerplant " + &
				"support or the powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
			return false
		end if

////
////	everything's ok so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_parcelgeographyfactoredit (s_pt_parcel_geo_type_factors as_factors[], ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelgeographyfactorsedit()
 **	
 **	edits the parcel geography type factors table for the given rows.  editing means inserts/updates/deletes as necessary.  this function will take care of doing the update or the insert correctly based on
 **	whether the relationship exists in the table or not.  if a percent is null, the row will be deleted.
 **	
 **	parameters	:	ref s_pt_parcel_geo_type_factors	:	(as_factors[]) the parcel responsiblity rows to edit.
 **						ref string									:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel geography type factor was edited successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi
		rows

////
////	loop over the factors and insert / update as necessary.
////
		maxi = upperbound( as_factors[] )
		
		for i = 1 to maxi
		////
		////	validate.
		////
				if isnull( as_factors[i].tax_year ) then
					a_msg = "'tax year' is a required field.  one of the parcel geography factors to update is missing a tax year."
					return false
				end if
				
				if isnull( as_factors[i].geography_type_id ) then
					a_msg = "'geography type' is a required field.  one of the parcel geography factors to update is missing a geography type."
					return false
				end if
				
				if isnull( as_factors[i].age ) then
					a_msg = "'age' is a required field.  one of the parcel geography factors to update is missing an age."
					return false
				end if
			
		////
		////	are we deleting?  if so, delete and skip the rest of the loop (which does the insert).
		////
				if isnull( as_factors[i].percent ) then
					if not this.of_parcelgeographyfactordelete( as_factors[i].tax_year, as_factors[i].geography_type_id, as_factors[i].age, a_msg ) then return false
					continue
				end if
				
		////
		////	do we insert or update.
		////
				setnull( rows )
				select		count('x')
				into		:rows
				from		pt_parcel_geo_type_factors gtf
				where	gtf.tax_year = :as_factors[i].tax_year
				and		gtf.geography_type_id = :as_factors[i].geography_type_id
				and		gtf.age = :as_factors[i].age;
				
				if isnull( rows ) or rows = 0 then
					insert into pt_parcel_geo_type_factors 
						( tax_year, geography_type_id, age, percent )
					values
						( :as_factors[i].tax_year, :as_factors[i].geography_type_id, :as_factors[i].age, :as_factors[i].percent );
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while inserting a new parcel geography type factor.  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				else
					update	pt_parcel_geo_type_factors gtf
					set			gtf.percent = :as_factors[i].percent
					where	gtf.tax_year = :as_factors[i].tax_year
					and		gtf.geography_type_id = :as_factors[i].geography_type_id
					and		gtf.age = :as_factors[i].age;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while updating an existing parcel geography type factor.  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				end if
		next

////
////	everything's ok so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_parcelgeographyeditfromimport ( a_run_id,  a_template_id, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_parcelgeographyeditfromimport()
 **	
 **	adds or updates the parcel geography table from the given import run.  this function will take care of doing the update or the insert correctly based on whether this is a new row or not
 **	(and based on the ability to add/update for the template being used).
 **	
 **	parameters	:		:	(a_run_id) the import run to use in updating parcel geography.
 **							:	(a_template_id) the import template to use.
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	parcel geography was edited successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.  in addtion, errors are written back to the import run table.
 **
 ************************************************************************************************************************************************************/


/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the of_parcelgeographyedit() function as both functions are used to update the pt_parcel_geography table.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
	boolean	do_updates

/*
 *	misc objects.
 */
	nvo_ppbase_import_templates uo_templates

////
////	check to see if we're just adding or if we can do adding and updating.
////
		uo_templates = create nvo_ppbase_import_templates
		do_updates = uo_templates.of_importtemplateisaddupdate( a_template_id )
		destroy uo_templates
		
////
////	if we're updatable mark those guys that have changed to speed things up (and ensure we don't update every row in the file if we have to).
////
			if do_updates then
				update	pt_import_prcl_geo ipg
				set			ipg.is_modified = 0
				where	ipg.import_run_id = :a_run_id;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while resetting the modified flag for parcel / geography type values.  if you continue to receive this error contact your internal powerplant " + &
						"support or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
				
				update	pt_import_prcl_geo ipg
				set			ipg.is_modified = 1
				where	ipg.import_run_id = :a_run_id
					and	exists
								(	select		'x'
									from		pt_parcel_geography geo
									where	ipg.parcel_id = geo.parcel_id
										and	ipg.geography_type_id = geo.geography_type_id
										and	nvl( trim( ipg.value ), '-90909' ) <> nvl( trim( geo.value ), '-90909' )
								);
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while flagging the modified parcel / geography type values.  if you continue to receive this error contact your internal powerplant support " + &
						"or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	do the updates (if we need to).  do these before the inserts to cut down on the number of rows touched (otherwise we're updating the rows we just inserted).  also, update anything that's in
////	here, even if it's updating to the same value - that's for speed reasons.  (if this causes a problem with the audit triggers showing the wrong times, so be it; if it's really a problem then you can
////	update it at that point - until then i'm leaving it as is.)
////
			if do_updates then
				update	pt_parcel_geography geo
				set			geo.value =
								(	select		trim( ipg.value )
									from		pt_import_prcl_geo ipg
									where	ipg.import_run_id = :a_run_id
										and	geo.parcel_id = ipg.parcel_id
										and	geo.geography_type_id = ipg.geography_type_id
								)
				where	( geo.parcel_id, geo.geography_type_id ) in
								(	select		parcel_id, geography_type_id
									from		pt_import_prcl_geo
									where	import_run_id = :a_run_id
										and	nvl( is_modified, 0 ) = 1
								);
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while updating existing parcel / geography type values.  if you continue to receive this error contact your internal powerplant support or " + &
						"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	do the inserts.
////
			insert into pt_parcel_geography ( parcel_id, geography_type_id, value )
				select		ipg.parcel_id, ipg.geography_type_id, trim( ipg.value )
				from		pt_import_prcl_geo ipg
				where	ipg.import_run_id = :a_run_id
					and	( ipg.parcel_id, ipg.geography_type_id ) not in
								(	select		geo.parcel_id, geo.geography_type_id
									from		pt_parcel_geography geo
								);
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while inserting new parcel / geography type relationships.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parcelgeographyvalidatefromimport ( a_run_id,  a_template_id, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_parcelgeographyvalidatefromimport()
 **	
 **	validates the data in the parcel geography import table from the given import run.
 **	
 **	parameters	:		:	(a_run_id) the import run to use in validating parcel geography.
 **							:	(a_template_id) the import template to use.
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	validation completed successfully (there could be error messages on the table, but the validation process completed successfully).
 **											false	:	an error occurred while validating.
 **
 ************************************************************************************************************************************************************/


/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the of_parcelgeographyedit() function as both functions are used to update the pt_parcel_geography table.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
	boolean	do_updates

/*
 *	misc objects.
 */
	nvo_ppbase_import_templates uo_templates

////
////	check to see if we're just adding or if we can do adding and updating.
////
		uo_templates = create nvo_ppbase_import_templates
		do_updates = uo_templates.of_importtemplateisaddupdate( a_template_id )
		destroy uo_templates

////
////	validate.  be sure to use a_sqlsa as the transaction object.  if we're maanging commits, commit/rollback after each update.
////
			////
			////	ensure all required fields are populated.
			////
				update	pt_import_prcl_geo ipg
				set			ipg.error_message = trim( nvl( ipg.error_message, '' ) || '  ' || case when ipg.parcel_id is null then 'parcel is a required field and is missing on this row.  ' else '' end ||
								case when ipg.geography_type_id is null then 'geography type is a require field and is missing on this row.  ' else '' end ||
								case when ipg.value is null then 'value is a require field and is missing on this row.  ' else '' end )
				where	ipg.import_run_id = :a_run_id
					and	(	ipg.parcel_id is null or
								ipg.geography_type_id is null or
								trim( ipg.value ) is null
							);
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while validating that all required fields are populated.  if you continue to receive this error contact your internal powerplant support or " + &
						"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
				
			////
			////	if we're not allowed to update, make sure there are no rows in the import which are already in the database.
			////
				if not do_updates then
					update	pt_import_prcl_geo ipg
					set			ipg.error_message = trim( nvl( ipg.error_message, '' ) || '  ' || 'this parcel / geography type combination already exists and cannot be imported using this template.' )
					where	ipg.import_run_id = :a_run_id
						and	( ipg.parcel_id, ipg.geography_type_id ) in
									(	select		geo.parcel_id, geo.geography_type_id
										from		pt_parcel_geography geo
									);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate entries.  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
							"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				end if
			
			////
			////	check to see if the import contains two identical rows and flag them.
			////
				update	pt_import_prcl_geo ipg
				set			ipg.error_message = trim( nvl( ipg.error_message, '' ) || '  ' || 'this parcel / geography type combination is duplicated in the import file.' )
				where	ipg.import_run_id = :a_run_id
					and	( ipg.parcel_id, ipg.geography_type_id ) in
								(	select		parcel_id, geography_type_id
									from		pt_import_prcl_geo
									where	import_run_id = :a_run_id
									having	count('x') > 1
									group by	parcel_id, geography_type_id
								);
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while checking for duplicate import rows.  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
						"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
				
////
////	the validation completed successfully, so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_parcelresponsibilityeditfromimport ( a_run_id,  a_template_id, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_parcelresponsibilityeditfromimport()
 **	
 **	adds or updates the parcel responsibility table from the given import run.  this function will take care of doing the update or the insert correctly based on whether this is a new row or not
 **	(and based on the ability to add/update for the template being used).
 **	
 **	parameters	:		:	(a_run_id) the import run to use in updating parcel responsibility.
 **							:	(a_template_id) the import template to use.
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	parcel responsibility was edited successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.  in addtion, errors are written back to the import run table.
 **
 ************************************************************************************************************************************************************/


/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the of_parcelresponsibilityedit() function as both functions are used to update the pt_parcel_responsibility
 **							table.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi
	string	sqls
	string	flds_type[]
	boolean	do_updates

/*
 *	misc objects.
 */
	nvo_ppbase_import_templates uo_templates
	s_ppbase_import_template_fields	s_flds[]

////
////	check to see if we're just adding or if we can do adding and updating.
////
		uo_templates = create nvo_ppbase_import_templates
		do_updates = uo_templates.of_importtemplateisaddupdate( a_template_id )
		
		if do_updates then 
			//get the fields - do not include autocreate fields, and only include fields on the table.
			if not uo_templates.of_importtemplategetfields( a_template_id, false, true, s_flds[], flds_type[], a_msg ) then
				destroy uo_templates
				return false
			end if
		end if
		
		destroy uo_templates

////
////	if we're updatable mark those guys that have changed to speed things up (and ensure we don't update every row in the file if we have to).
////
			if do_updates then
				update	pt_import_prcl_rsp ipr
				set			ipr.is_modified = 0
				where	ipr.import_run_id = :a_run_id;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while resetting the modified flag for parcel responsibilities.  if you continue to receive this error contact your internal powerplant " + &
						"support or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
				
				sqls =						"	update	pt_import_prcl_rsp ipr " + "~r~n" + &
											"	set		ipr.is_modified = 1 " + "~r~n" + &
											"	where	ipr.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
											"		and	exists " + "~r~n" + &
											"					(	select		'x' " + "~r~n" + &
											"						from		pt_parcel_responsibility pres " + "~r~n" + &
											"						where	ipr.parcel_id = pres.parcel_id " + "~r~n" + &
											"							and	ipr.responsible_entity_id = pres.responsible_entity_id " + "~r~n" + &
											"							and	ipr.responsibility_type_id = pres.responsibility_type_id " + "~r~n" + &
											"							and	ipr.tax_year = pres.tax_year " + "~r~n" + &
											"							and	(			"
				
				maxi = upperbound( s_flds[] )
				
				for i = 1 to maxi
					if i > 1 then
						sqls = sqls +	"										or		"
					end if
					
					if left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
						sqls = sqls + "nvl( trim( ipr." + s_flds[i].column_name + " ), '-90909' ) <> nvl( trim( pres." + s_flds[i].column_name + " ), '-90909' ) " + "~r~n"
					elseif left( flds_type[i], 6 ) = "number" then
						sqls = sqls + "nvl( ipr." + s_flds[i].column_name + ", -90909 ) <> nvl( pres." + s_flds[i].column_name + ", -90909 ) " + "~r~n"
					elseif left( flds_type[i], 4 ) = "date" then
						sqls = sqls + "nvl( to_date( ipr." + s_flds[i].column_name + ", 'mm/dd/yyyy' ), to_date( '1900-01-01', 'yyyy-mm-dd' ) ) <> nvl( pres." + s_flds[i].column_name + ", to_date( '1900-01-01', 'yyyy-mm-dd' ) ) " + "~r~n"
					end if
				next
				
				sqls = sqls +			"									) " + "~r~n" + &
											"					) "
				
				execute immediate :sqls;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while flagging the modified parcel responsibilities.  if you continue to receive this error contact your internal powerplant support " + &
						"or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	do the updates (if we need to).  do these before the inserts to cut down on the number of rows touched (otherwise we're updating the rows we just inserted).  also, update anything that's in
////	here, even if it's updating to the same value - that's for speed reasons.  (if this causes a problem with the audit triggers showing the wrong times, so be it; if it's really a problem then you can
////	update it at that point - until then i'm leaving it as is.)
////
			if do_updates then
				sqls =				"	update	pt_parcel_responsibility pres " + "~r~n" + &
									"	set		( "
				
				maxi = upperbound( s_flds[] )
				
				for i = 1 to maxi
					if i > 1 then sqls = sqls + ", "
					sqls = sqls + "pres." + s_flds[i].column_name
				next
				
				sqls = sqls + " ) = " + "~r~n"
				
				sqls = sqls +	"					(	select		"
				
				for i = 1 to maxi
					if i > 1 then sqls = sqls + ", "
					
					if left( flds_type[i], 4 ) = "date" then
						sqls = sqls + "to_date( ipr." + s_flds[i].column_name + ", 'mm/dd/yyyy' )"
					elseif left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
						sqls = sqls + "trim( ipr." + s_flds[i].column_name + ")"
					else
						sqls = sqls + "ipr." + s_flds[i].column_name
					end if
				next
				
				sqls = sqls +	"						from		pt_import_prcl_rsp ipr " + "~r~n" + &
									"						where	ipr.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
									"							and	pres.parcel_id = ipr.parcel_id " + "~r~n" + &
									"							and	pres.responsible_entity_id = ipr.responsible_entity_id " + "~r~n" + &
									"							and	pres.responsibility_type_id = ipr.responsibility_type_id " + "~r~n" + &
									"							and	pres.tax_year = ipr.tax_year " + "~r~n" + &
									"					) " + "~r~n" + &
									"	where	( pres.parcel_id, pres.responsible_entity_id, pres.responsibility_type_id, pres.tax_year ) in " + "~r~n" + &
									"					(	select		parcel_id, responsible_entity_id, responsibility_type_id, tax_year " + "~r~n" + &
									"						from		pt_import_prcl_rsp " + "~r~n" + &
									"						where	import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
									"							and	nvl( is_modified, 0 ) = 1 " + "~r~n" + &
									"					) "
				
				execute immediate :sqls;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while updating existing parcel responsibilities.  if you continue to receive this error contact your internal powerplant support or " + &
						"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	do the inserts.
////
			insert into pt_parcel_responsibility ( parcel_id, responsible_entity_id, responsibility_type_id, tax_year, percent_responsible, lease_start_date, lease_end_date, entity_is_lessor, entity_is_lessee, reference_number )
				select		ipr.parcel_id, ipr.responsible_entity_id, ipr.responsibility_type_id, ipr.tax_year, ipr.percent_responsible, to_date( ipr.lease_start_date, 'mm/dd/yyyy' ), to_date( ipr.lease_end_date, 'mm/dd/yyyy' ), ipr.entity_is_lessor, ipr.entity_is_lessee, trim( ipr.reference_number )
				from		pt_import_prcl_rsp ipr
				where	ipr.import_run_id = :a_run_id
					and	( ipr.parcel_id, ipr.responsible_entity_id, ipr.responsibility_type_id, ipr.tax_year ) not in
								(	select		pres.parcel_id, pres.responsible_entity_id, pres.responsibility_type_id, pres.tax_year
									from		pt_parcel_responsibility pres
								);
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while inserting new parcel responsibilities.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parcelresponsibilityvalidatefromimp ( a_run_id,  a_template_id, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_parcelresponsibilityvalidatefromimp()
 **	
 **	validates the data in the parcel responsibility import table from the given import run.
 **	
 **	parameters	:		:	(a_run_id) the import run to use in updating parcel responsibility.
 **							:	(a_template_id) the import template to use.
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	validation completed successfully (there could be error messages on the table, but the validation process completed successfully).
 **											false	:	an error occurred while validating.
 **
 ************************************************************************************************************************************************************/


/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the of_parcelresponsibilityedit() function as both functions are used to update the pt_parcel_responsibility
 **							table.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
	string	flds_type[]
	boolean	do_updates

/*
 *	misc objects.
 */
	nvo_ppbase_import_templates uo_templates
	s_ppbase_import_template_fields	s_flds[]

////
////	check to see if we're just adding or if we can do adding and updating.
////
		uo_templates = create nvo_ppbase_import_templates
		do_updates = uo_templates.of_importtemplateisaddupdate( a_template_id )
		
		if do_updates then 
			//get the fields - do not include autocreate fields, and only include fields on the table.
			if not uo_templates.of_importtemplategetfields( a_template_id, false, true, s_flds[], flds_type[], a_msg ) then
				destroy uo_templates
				return false
			end if
		end if
		
		destroy uo_templates

////
////	validate.  be sure to use a_sqlsa as the transaction object.  if we're maanging commits, commit/rollback after each update.
////
			////
			////	ensure all required fields are populated.
			////
				update	pt_import_prcl_rsp ipr
				set			ipr.error_message = trim( nvl( ipr.error_message, '' ) || '  ' || case when ipr.parcel_id is null then 'parcel is a required field and is missing on this row.  ' else '' end ||
								case when ipr.tax_year is null then 'tax year is a require field and is missing on this row.  ' else '' end ||
								case when ipr.responsibility_type_id is null then 'responsibility type is a require field and is missing on this row.  ' else '' end ||
								case when ipr.responsible_entity_id is null then 'responsible entity is a required field and is missing on this row.  ' else '' end ||
								case when ipr.percent_responsible is null then 'percent responsible is a required field and is missing on this row.  ' else '' end )
				where		ipr.import_run_id = :a_run_id
					and	(	ipr.parcel_id is null or
								ipr.tax_year is null or
								ipr.responsibility_type_id is null or
								ipr.responsible_entity_id is null or
								ipr.percent_responsible is null
							);
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while validating that all required fields are populated.  if you continue to receive this error contact your internal powerplant support or " + &
						"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
				
			////
			////	if we're not allowed to update, make sure there are no rows in the import which are already in the database.
			////
				if not do_updates then
					update	pt_import_prcl_rsp ipr
					set			ipr.error_message = trim( nvl( ipr.error_message, '' ) || '  ' || 'this parcel responsibility entry already exists and cannot be imported using this template.' )
					where	ipr.import_run_id = :a_run_id
						and	( ipr.parcel_id, ipr.responsible_entity_id, ipr.responsibility_type_id, ipr.tax_year ) in
									(	select		pres.parcel_id, pres.responsible_entity_id, pres.responsibility_type_id, pres.tax_year
										from		pt_parcel_responsibility pres
									);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate entries.  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
							"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				end if
			
			////
			////	check to see if the import contains two identical rows and flag them.
			////
				update	pt_import_prcl_rsp ipr
				set			ipr.error_message = trim( nvl( ipr.error_message, '' ) || '  ' || 'this parcel / responsible entity type combination is duplicated in the import file.' )
				where	ipr.import_run_id = :a_run_id
					and	( ipr.parcel_id, ipr.responsible_entity_id, ipr.responsibility_type_id, ipr.tax_year ) in
								(	select		parcel_id, responsible_entity_id, responsibility_type_id, tax_year
									from		pt_import_prcl_rsp
									where	import_run_id = :a_run_id
									having	count('x') > 1
									group by	parcel_id, responsible_entity_id, responsibility_type_id, tax_year
								);
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while checking for duplicate import rows.  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
						"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
				
////
////	the validation completed successfully, so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_parcelresponsibleentityeditfromimport ( a_run_id,  a_template_id, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_parcelresponsibleentityeditfromimport()
 **	
 **	adds or updates the responsible entity table from the given import run.  this function will take care of doing the update or the insert correctly based on whether this is a new row or not
 **	(and based on the ability to add/update for the template being used).
 **	
 **	parameters	:		:	(a_run_id) the import run to use in updating parcel responsible entity.
 **							:	(a_template_id) the import template to use.
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	parcel responsible entity was edited successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.  in addtion, errors are written back to the import run table.
 **
 ************************************************************************************************************************************************************/


/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the of_parcelresponsibleentityedit() function as both functions are used to update the
 **							pt_parcel_responsible_entity table.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi
		updt_ilook_id
	string	sqls, rtn_str
	string	updt_key_col, updt_constr_cols
	string	constr_cols_arr[], flds_type[]
	boolean	do_updates, all_null_key_cols

/*
 *	misc objects.
 */
	uo_ds_top ds_check
	nvo_ppbase_import_templates uo_templates
	s_ppbase_import_template_fields	s_flds[]

////
////	check to see if we're just adding or if we can do adding and updating.  if we can do updates, get the unique key column for updating (i.e., for determining if something already exists in the
////	database and needs to be updated or if it's new).  finally, if we're updating get the fields for this run.
////
		uo_templates = create nvo_ppbase_import_templates
		do_updates = uo_templates.of_importtemplateisaddupdate( a_template_id )
		updt_ilook_id = uo_templates.of_importtemplategetupdateslookup( a_template_id, updt_key_col, updt_constr_cols )
		
		if do_updates then 
			//get the fields - do not include autocreate fields, and only include fields on the table.
			if not uo_templates.of_importtemplategetfields( a_template_id, false, true, s_flds[], flds_type[], a_msg ) then
				destroy uo_templates
				return false
			end if
		end if
		
		destroy uo_templates
		
		if do_updates and isnull( updt_ilook_id ) then
			a_msg = "no updates lookup is specified for this template.  please update the template to specify the unique key column and try again."
			return false
		end if

////
////	split the constraint columns into an array (if there's more than one).
////
			if not isnull( updt_constr_cols ) then
				if pos( updt_constr_cols, "," ) > 0 then
					f_parsestringintostringarray( updt_constr_cols, ",", constr_cols_arr[] )
					
					maxi = upperbound( constr_cols_arr[] )
					
					for i = 1 to maxi
						constr_cols_arr[i] = trim( constr_cols_arr[i] )
					next
				elseif trim( updt_constr_cols ) <> "" then
					constr_cols_arr[1] = updt_constr_cols
				end if
			end if

////
////	if we're updating, see if the key column in populated in the database.
////
			all_null_key_cols = false
			
			if do_updates then
				sqls	=	"	select		count('x') " + "~r~n" + &
							"	from		pt_parcel_responsible_entity " + "~r~n" + &
							"	where	trim( " + updt_key_col + " ) is not null "
				
				ds_check = create uo_ds_top
				rtn_str = f_create_dynamic_ds( ds_check, "grid", sqls, sqlca, true )
				
				if rtn_str <> "ok" then
					a_msg = "a database error occurred while checking the state of key columns in the database.  if you continue to receive this error contact your internal powerplant support or " + &
						"the powerplan property tax support desk.  the specific error returned was: " + rtn_str
					return false
				end if
				
				if ds_check.rowcount() = 0 then
					all_null_key_cols = true
				else
					if ds_check.getitemnumber( 1, 1 ) = 0 then
						all_null_key_cols = true
					else
						all_null_key_cols = false
					end if
				end if
			end if

////
////	if we're updatable mark those guys that have changed to speed things up (and ensure we don't update every row in the file if we have to).  (be sure to update the responsible entity ids for
////	those that are changing as well.)
////
			if do_updates then
				update	pt_import_prcl_rsp_ent ipre
				set		ipre.is_modified = 0
				where	ipre.import_run_id = :a_run_id;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while resetting the modified flag for responsible entities.  if you continue to receive this error contact your internal powerplant support or the " + &
						"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
				
				sqls =						"	update	pt_import_prcl_rsp_ent ipre " + "~r~n" + &
											"	set		ipre.is_modified = 1, " + "~r~n" + &
											"				ipre.responsible_entity_id = " + "~r~n" + &
											"					(	select		pre.responsible_entity_id " + "~r~n" + &
											"						from		pt_parcel_responsible_entity pre " + "~r~n" + &
											"						where	trim( ipre." + updt_key_col + " ) = trim( pre." + updt_key_col + " ) " + "~r~n"
				
				maxi = upperbound( constr_cols_arr[] )
				
				for i = 1 to maxi
					sqls = sqls +		"							and	nvl( trim( ipre." + constr_cols_arr[i] + " ), '-90909' ) = nvl( trim( pre." + constr_cols_arr[i] + " ), '-90909' ) " + "~r~n"
				next
				
				sqls = sqls +			"					) " + "~r~n" + &
											"	where	ipre.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
											"		and	exists " + "~r~n" + &
											"					(	select		'x' " + "~r~n" + &
											"						from		pt_parcel_responsible_entity pre " + "~r~n" + &
											"						where	trim( ipre." + updt_key_col + " ) = trim( pre." + updt_key_col + " ) " + "~r~n"
				
				maxi = upperbound( constr_cols_arr[] )
				
				for i = 1 to maxi
					sqls = sqls +		"							and	nvl( trim( ipre." + constr_cols_arr[i] + " ), '-90909' ) = nvl( trim( pre." + constr_cols_arr[i] + " ), '-90909' ) " + "~r~n"
				next
				
				sqls = sqls +			"							and	(			"
				
				maxi = upperbound( s_flds[] )
				
				for i = 1 to maxi
					if i > 1 then
						sqls = sqls +	"										or		"
					end if
					
					if left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
						sqls = sqls + "nvl( trim( ipre." + s_flds[i].column_name + " ), '-90909' ) <> nvl( trim( pre." + s_flds[i].column_name + " ), '-90909' ) " + "~r~n"
					elseif left( flds_type[i], 6 ) = "number" then
						sqls = sqls + "nvl( ipre." + s_flds[i].column_name + ", -90909 ) <> nvl( pre." + s_flds[i].column_name + ", -90909 ) " + "~r~n"
					elseif left( flds_type[i], 4 ) = "date" then
						sqls = sqls + "nvl( to_date( ipre." + s_flds[i].column_name + ", 'mm/dd/yyyy' ), to_date( '1900-01-01', 'yyyy-mm-dd' ) ) <> nvl( pre." + s_flds[i].column_name + ", to_date( '1900-01-01', 'yyyy-mm-dd' ) ) " + "~r~n"
					end if
				next
				
				sqls = sqls +			"									) " + "~r~n" + &
											"					) "
				
				execute immediate :sqls;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while flagging the modified responsible entity.  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
						"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	do the updates (if we need to).  do these before the inserts to cut down on the number of rows touched (otherwise we're updating the rows we just inserted).  also, update anything that's in
////	here, even if it's updating to the same value - that's for speed reasons.  (if this causes a problem with the audit triggers showing the wrong times, so be it; if it's really a problem then you can
////	update it at that point - until then i'm leaving it as is.)
////
			if do_updates then
				sqls =				"	update	pt_parcel_responsible_entity pre " + "~r~n" + &
									"	set		( "
				
				maxi = upperbound( s_flds[] )
				
				for i = 1 to maxi
					if i > 1 then sqls = sqls + ", "
					sqls = sqls + "pre." + s_flds[i].column_name
				next
				
				sqls = sqls + " ) = " + "~r~n"
				
				sqls = sqls +	"					(	select		"
				
				for i = 1 to maxi
					if i > 1 then sqls = sqls + ", "
					
					if left( flds_type[i], 4 ) = "date" then
						sqls = sqls + "to_date( ipre." + s_flds[i].column_name + ", 'mm/dd/yyyy' )"
					elseif left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
						sqls = sqls + "trim( ipre." + s_flds[i].column_name + " )"
					else
						sqls = sqls + "ipre." + s_flds[i].column_name
					end if
				next
				
				sqls = sqls +	"						from		pt_import_prcl_rsp_ent ipre " + "~r~n" + &
									"						where	ipre.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
									"							and	pre.responsible_entity_id = ipre.responsible_entity_id " + "~r~n" + &
									"					) " + "~r~n" + &
									"	where	( pre.responsible_entity_id ) in " + "~r~n" + &
									"					(	select		responsible_entity_id " + "~r~n" + &
									"						from		pt_import_prcl_rsp_ent " + "~r~n" + &
									"						where	import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
									"							and	nvl( is_modified, 0 ) = 1 " + "~r~n" + &
									"					) "
				
				execute immediate :sqls;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while updating existing responsible entities.  if you continue to receive this error contact your internal powerplant support or " + &
						"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	do the inserts.  before doing the inserts, put the new responsible entity id on the import table for archiving.  if we're updating use the key column to check for uniqueness; otherwise all goes.
////
			if do_updates then
				sqls =						"	update	pt_import_prcl_rsp_ent " + "~r~n" + &
											"	set		responsible_entity_id = nvl( ( select max( responsible_entity_id ) from pt_parcel_responsible_entity ), 0 ) + rownum " + "~r~n" + &
											"	where	import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
											"		and	responsible_entity_id is null " + "~r~n"
				
				if not all_null_key_cols then
					if upperbound( constr_cols_arr[] ) = 0 then
						sqls = sqls +	"		and	( trim( " + updt_key_col + " ) ) not in " + "~r~n" + &
											"					(	select		trim( " + updt_key_col + " ) " + "~r~n"
					else
						sqls = sqls +	"		and	( trim( " + updt_key_col + " ), " + updt_constr_cols + " ) not in " + "~r~n" + &
											"					(	select		trim( " + updt_key_col + " ), " + updt_constr_cols + " " + "~r~n"
					end if
					
					sqls = sqls +		"						from		pt_parcel_responsible_entity " + "~r~n" + &
											"						where	trim( " + updt_key_col + " ) is not null " + "~r~n" + &
											"					) "
				end if
				
				execute immediate :sqls;
			else
				update	pt_import_prcl_rsp_ent
				set		responsible_entity_id = nvl( ( select max( responsible_entity_id ) from pt_parcel_responsible_entity ), 0 ) + rownum
				where	import_run_id = :a_run_id;
			end if
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while assigning internal ids to new responsible entities.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
			
			insert into pt_parcel_responsible_entity ( responsible_entity_id, description, description, address_1, address_2, address_3, city, zip_code, state, phone_1, phone_2, contact_name, entity_number, external_code, tax_id_code, tax_id_number, prop_tax_company_id, notes )
				select	ipre.responsible_entity_id, trim( ipre.description ), trim( ipre.description ), trim( ipre.address_1 ), trim( ipre.address_2 ), trim( ipre.address_3 ), trim( ipre.city ), trim( ipre.zip_code ), trim( ipre.state ), trim( ipre.phone_1 ), trim( ipre.phone_2 ), trim( ipre.contact_name ), trim( ipre.entity_number ), trim( ipre.external_code ), trim( ipre.tax_id_code ), trim( ipre.tax_id_number ), ipre.prop_tax_company_id, trim( ipre.notes )
				from		pt_import_prcl_rsp_ent ipre
				where	ipre.import_run_id = :a_run_id
					and	ipre.responsible_entity_id is not null
					and	( ipre.responsible_entity_id ) not in
								(	select		pre.responsible_entity_id
									from		pt_parcel_responsible_entity pre
								);
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while inserting new responsible entities.  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
					"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parcelresponsibleentityvalidfromimp ( a_run_id,  a_template_id, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_parcelresponsibleentityvalidfromimp()
 **	
 **	validates the data in the responsible entity import table from the given import run.
 **	
 **	parameters	:		:	(a_run_id) the import run to use in validating parcel responsible entity.
 **							:	(a_template_id) the import template to use.
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	validation completed successfully (there could be error messages on the table, but the validation process completed successfully).
 **											false	:	an error occurred while validating.
 **
 ************************************************************************************************************************************************************/


/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the of_parcelresponsibleentityedit() function as both functions are used to update the
 **							pt_parcel_responsible_entity table.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi
		updt_ilook_id
	string	sqls, rtn_str
	string	updt_key_col, updt_constr_cols
	string	constr_cols_arr[]
	boolean	do_updates, all_null_key_cols

/*
 *	misc objects.
 */
	uo_ds_top ds_check
	nvo_ppbase_import_templates uo_templates

////
////	check to see if we're just adding or if we can do adding and updating.  if we can do updates, get the unique key column for updating (i.e., for determining if something already exists in the
////	database and needs to be updated or if it's new).  finally, if we're updating get the fields for this run.
////
		uo_templates = create nvo_ppbase_import_templates
		do_updates = uo_templates.of_importtemplateisaddupdate( a_template_id )
		updt_ilook_id = uo_templates.of_importtemplategetupdateslookup( a_template_id, updt_key_col, updt_constr_cols )
		destroy uo_templates
		
		if do_updates and isnull( updt_ilook_id ) then
			a_msg = "no updates lookup is specified for this template.  please update the template to specify the unique key column and try again."
			return false
		end if

////
////	split the constraint columns into an array (if there's more than one).
////
			if not isnull( updt_constr_cols ) then
				if pos( updt_constr_cols, "," ) > 0 then
					f_parsestringintostringarray( updt_constr_cols, ",", constr_cols_arr[] )
					
					maxi = upperbound( constr_cols_arr[] )
					
					for i = 1 to maxi
						constr_cols_arr[i] = trim( constr_cols_arr[i] )
					next
				elseif trim( updt_constr_cols ) <> "" then
					constr_cols_arr[1] = updt_constr_cols
				end if
			end if

////
////	if we're updating, see if the key column in populated in the database.
////
			all_null_key_cols = false
			
			if do_updates then
				sqls	=	"	select		count('x') " + "~r~n" + &
							"	from		pt_parcel_responsible_entity " + "~r~n" + &
							"	where	trim( " + updt_key_col + " ) is not null "
				
				ds_check = create uo_ds_top
				rtn_str = f_create_dynamic_ds( ds_check, "grid", sqls, sqlca, true )
				
				if rtn_str <> "ok" then
					a_msg = "a database error occurred while checking the state of key columns in the database.  if you continue to receive this error contact your internal powerplant support or " + &
						"the powerplan property tax support desk.  the specific error returned was: " + rtn_str
					return false
				end if
				
				if ds_check.rowcount() = 0 then
					all_null_key_cols = true
				else
					if ds_check.getitemnumber( 1, 1 ) = 0 then
						all_null_key_cols = true
					else
						all_null_key_cols = false
					end if
				end if
			end if

////
////	validate. 
////
			////
			////	ensure all required fields are populated.
			////
				update	pt_import_prcl_rsp_ent ipre
				set			ipre.error_message = trim( nvl( ipre.error_message, '' ) || '  ' || case when ipre.description is null then 'description is a required field and is missing on this row.  ' else '' end )
				where	ipre.import_run_id = :a_run_id
					and	trim( ipre.description ) is null;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while validating that all required fields are populated.  if you continue to receive this error contact your internal powerplant support or " + &
						"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
				
			////
			////	make sure we don't have duplicate descriptions (if we're not doing updates).  if we're doing updates and the key column is not the description then also check.  (if we're doing updates and the
			////	description is the key column we'd expect duplicates.)
			////
				if not do_updates then
					update	pt_import_prcl_rsp_ent
					set			error_message = trim( nvl( error_message, '' ) || '  ' || 'this responsible entity already exists and cannot be imported using this template.' )
					where	import_run_id = :a_run_id
						and	lower( trim( description ) ) in
									(	select		lower( trim( description ) )
										from		pt_parcel_responsible_entity
									);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate entries by description.  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				else
					if updt_key_col <> "description" then
						sqls =					"	update	pt_import_prcl_rsp_ent ipre " + "~r~n" + &
												"	set			ipre.error_message = trim( nvl( ipre.error_message, '' ) || '  ' || 'this responsible entity already exists and cannot be imported using this template.' ) " + "~r~n" + &
												"	where	ipre.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
												"		and	exists " + "~r~n" + &
												"					(	select		'x' " + "~r~n" + &
												"						from		pt_parcel_responsible_entity pre " + "~r~n" + &
												"						where	lower( trim( ipre.description ) ) = lower( trim( pre.description ) ) " + "~r~n" + &
												"							and	(	nvl( trim( ipre." + updt_key_col + " ), '-90909' ) <> nvl( trim( pre." + updt_key_col + " ), '-90909' ) " + "~r~n"
						
						maxi = upperbound( constr_cols_arr[] )
						
						for i = 1 to maxi
							sqls = sqls +		"									or nvl( trim( ipre." + constr_cols_arr[i] + " ), '-90909' ) <> nvl( trim( pre." + constr_cols_arr[i] + " ), '-90909' ) " + "~r~n"
						next
						
						sqls = sqls +		"									) " + "~r~n" + &
												"					) "
						
						execute immediate :sqls;
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while checking for duplicate entries by description.  if you continue to receive this error contact your internal powerplant support or the " + &
								"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					end if
				end if
			
			////
			////	if we're updating make sure we don't have duplicate key columns in the database.
			////
				if do_updates and ( not all_null_key_cols ) then
					sqls = 				"	update	pt_import_prcl_rsp_ent " + "~r~n" + &
											"	set			error_message = trim( nvl( error_message, '' ) || '  ' || 'multiple responsible entities exist in the database for this key column (" + string( updt_key_col ) + ").' ) " + "~r~n" + &
											"	where	import_run_id = " + string( a_run_id ) + " " + "~r~n"
					
					if upperbound( constr_cols_arr[] ) = 0 then
						sqls = sqls +	"		and	( trim( " + updt_key_col + " ) ) in " + "~r~n" + &
											"					(	select		trim( " + updt_key_col + ") " + "~r~n"
					else
						sqls = sqls +	"		and	( trim( " + updt_key_col + " ), " + updt_constr_cols + " ) in " + "~r~n" + &
											"					(	select		trim( " + updt_key_col + " ), " + updt_constr_cols + " " + "~r~n"
					end if
					
					sqls = sqls +		"						from		pt_parcel_responsible_entity " + "~r~n" + &
											"						where	trim( " + updt_key_col + " ) is not null " + "~r~n" + &
											"						having	count('x') > 1 " + "~r~n"
					
					if upperbound( constr_cols_arr[] ) = 0 then
						sqls = sqls +	"						group by	trim( " + updt_key_col + " ) " + "~r~n"
					else
						sqls = sqls +	"						group by	trim( " + updt_key_col + " ), " + updt_constr_cols + " " + "~r~n"
					end if
					
					sqls = sqls +		"					) "
					
					execute immediate :sqls;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate entries by key column.  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				end if
			
			////
			////	check to see if the import contains two identical rows and flag them.  first make sure there aren't duplicate descriptions.  second, if we're updating, make sure there aren't duplicate
			////	key columns.
			////
				////
				////	description.
				////
					update	pt_import_prcl_rsp_ent
					set			error_message = trim( nvl( error_message, '' ) || '  ' || 'this responsible entity description is duplicated in the import file.' )
					where	import_run_id = :a_run_id
						and	lower( trim( description ) ) in
									(	select		lower( trim( description ) )
										from		pt_import_prcl_rsp_ent
										where	import_run_id = :a_run_id
										having	count('x') > 1
										group by	description
									);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate import rows (on description).  if you continue to receive this error contact your internal powerplant support " + &
							"or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
					
				////
				////	key column.  (no need to do description here as we've already done it above.)
				////
					if do_updates then
						if updt_key_col <> "description" then
							sqls =					"	update	pt_import_prcl_rsp_ent " + "~r~n" + &
													"	set		error_message = trim( nvl( error_message, '' ) || '  ' || 'this responsible entity key column (" + updt_key_col + ") is duplicated in the import file.' ) " + "~r~n" + &
													"	where	import_run_id = " + string( a_run_id ) + " " + "~r~n"
							
							if upperbound( constr_cols_arr[] ) = 0 then
								sqls = sqls +	"		and	( trim( " + updt_key_col + " ) ) in " + "~r~n" + &
													"					(	select		trim( " + updt_key_col + " ) " + "~r~n"
							else
								sqls = sqls +	"		and	( trim( " + updt_key_col + " ), " + updt_constr_cols + " ) in " + "~r~n" + &
													"					(	select		trim( " + updt_key_col + " ), " + updt_constr_cols + " " + "~r~n"
							end if
							
							sqls = sqls +		"						from		pt_import_prcl_rsp_ent " + "~r~n" + &
													"						where	import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
													"						having	count('x') > 1 " + "~r~n"
							
							if upperbound( constr_cols_arr[] ) = 0 then
								sqls = sqls +	"						group by	trim( " + updt_key_col + " ) " + "~r~n"
							else
								sqls = sqls +	"						group by	trim( " + updt_key_col + " ), " + updt_constr_cols + " " + "~r~n"
							end if
							
							sqls = sqls +		"					) "
							
							execute immediate :sqls;
							
							if sqlca.sqlcode <> 0 then
								a_msg = "a database error occurred while checking for duplicate import rows (on key column).  if you continue to receive this error contact your internal powerplant support " + &
									"or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
								return false
							end if
						end if
					end if
					
////
////	the validation completed successfully, so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_parcelgeographyfactorvalidatefromimp ( a_run_id, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_parcelgeographyfactorvalidatefromimp()
 **
 **	validates the data in the geography factor import table from the given import run.
 **	
 **	parameters	:		:	(a_run_id) the import run to validate.
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	validation completed successfully (there could be error messages on the table, but the validation process completed successfully).
 **											false	:	an error occurred while validating.
 **
 ************************************************************************************************************************************************************/

////
////	check for duplicate rows.
////
		update	pt_import_prcl_geofctr imp
		set			imp.error_message = substr( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
					'this row is duplicated in the import file.', 1, 4000 )
		where	imp.import_run_id = :a_run_id
		and		( imp.tax_year, imp.geography_type_id, imp.age ) in
					(	select		impin.tax_year, impin.geography_type_id, impin.age
						from		pt_import_prcl_geofctr impin
						where	impin.import_run_id = :a_run_id
						having	count(*) > 1
						group by impin.tax_year, impin.geography_type_id, impin.age
					);
		
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while checking for duplicate rows.  if you continue to receive this error contact your internal powerplant support or " + &
				"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	the validation completed successfully, so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_parcelgeographyfactorupdatefromimport ( a_run_id, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_parcelgeographyfactorupdatefromimport()
 **	
 **	adds/edits the geography factors for the given import run id.  checks to see if the factor exists - if so, it edits it, otherwise, it adds it.
 **	
 **	parameters	:		:	(a_run_id) the import run to use when loading.
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	data loaded successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.  in addtion, errors are written back to the import run table.
 **
 ************************************************************************************************************************************************************/


/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the of_geographyfactoredit() function as both functions are used to add/edit geography factors.
 **
 ************************************************************************************************************************************************************/

	i, maxi
string			sqls, rtn_str
uo_ds_top	ds_check

////
////	if the row in the import file has a null percent, delete the record.
////	
		delete from pt_parcel_geo_type_factors geo
		where	exists
					(	select		'x'
						from		pt_import_prcl_geofctr imp
						where	imp.import_run_id = :a_run_id
						and		imp.tax_year = geo.tax_year
						and		imp.geography_type_id = geo.geography_type_id
						and		to_number( imp.age ) = geo.age
						and		to_number( imp.percent ) is null
					);
					
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while deleting existing geography type factors from 'pt parcel geo type factors' for rows with a null percent.  " + &
						"if you continue to receive this message contact your internal powerplant support or the powerplan property tax " + &
						"support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
					
////
////	update existing rows.
////
		update	pt_parcel_geo_type_factors geo
		set			geo.percent =
					(	select		to_number( imp.percent )
						from		pt_import_prcl_geofctr imp
						where	imp.import_run_id = :a_run_id
						and		imp.tax_year = geo.tax_year
						and		imp.geography_type_id = geo.geography_type_id
						and		to_number( imp.age ) = geo.age
					)
		where	exists
					(	select		'x'
						from		pt_import_prcl_geofctr imp
						where	imp.import_run_id = :a_run_id
						and		imp.tax_year = geo.tax_year
						and		imp.geography_type_id = geo.geography_type_id
						and		to_number( imp.age ) = geo.age
					);
		
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while updating existing geography type factors in 'pt parcel geo type factors'.  " + &
						"if you continue to receive this message contact your internal powerplant support or the powerplan property tax " + &
						"support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	insert new rows.
////
		insert into pt_parcel_geo_type_factors ( tax_year, geography_type_id, age, percent )
			select		imp.tax_year, imp.geography_type_id, to_number( imp.age ), to_number( imp.percent )
			from		pt_import_prcl_geofctr imp
			where	imp.import_run_id = :a_run_id
			and		to_number( imp.percent ) is not null
			and		not exists
						(	select		'x'
							from		pt_parcel_geo_type_factors geo
							where	imp.tax_year = geo.tax_year
							and		imp.geography_type_id = geo.geography_type_id
							and		to_number( imp.age ) = geo.age
						);
		
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while inserting the new geography type factors into 'pt parcel geo type factors'.  " + &
						"if you continue to receive this message contact your internal powerplant support or the powerplan property tax " + &
						"support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	check to make sure the ages for each tax year and geography type start with 1 and are sequential, with no numbers skipped.
////	do this after loading, so we can just look at the pt_parcel_geo_type_factors table and don't have to worry about duplicates
////	in the import table.
////	we can just check to make sure the count of ages equals the max age.  if it doesn't, we know something is off.
////
		ds_check = create uo_ds_top
		
		sqls =		"select	distinct imp.tax_year_xlate, imp.geography_type_xlate, imp.tax_year, imp.geography_type_id " + "~r~n" + &
					"from		pt_import_prcl_geofctr imp, " + "~r~n" + &
					"			(	select		tax_year, geography_type_id, count(*) age_count " + "~r~n" + &
					"				from		pt_parcel_geo_type_factors " + "~r~n" + &
					"				group by	tax_year, geography_type_id " + "~r~n" + &
					"			) count_view, " + "~r~n" + &
					"			(	select		tax_year, geography_type_id, max( age ) max_age " + "~r~n" + &
					"				from		pt_parcel_geo_type_factors " + "~r~n" + &
					"				group by	tax_year, geography_type_id " + "~r~n" + &
					"			) max_view " + "~r~n" + &
					"where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
					"and		imp.tax_year = count_view.tax_year " + "~r~n" + &
					"and		imp.geography_type_id = count_view.geography_type_id " + "~r~n" + &
					"and		imp.tax_year = max_view.tax_year " + "~r~n" + &
					"and		imp.geography_type_id = max_view.geography_type_id " + "~r~n" + &
					"and		count_view.age_count <> max_view.max_age "
		
		rtn_str = f_create_dynamic_ds( ds_check, "grid", sqls, sqlca, true )
		
		if rtn_str <> "ok" then
			a_msg = "error occurred while checking to make sure all ages loaded are valid.  no changes made." + "~n~nerror: " + rtn_str
			destroy ds_check
			return false
		end if
		
		maxi = ds_check.rowcount()
		
		if maxi > 0 then
			a_msg = "the following tax year and geography type combinations have invalid ages in the system.  age must start at 1 and increase sequentially.~r~n~r~n"
			
			for i = 1 to maxi
				a_msg = a_msg + trim( ds_check.getitemstring( i, 1 ) ) + " - " + trim( ds_check.getitemstring( i, 2 ) ) + "~r~n"
			next
			
			a_msg = a_msg + "~r~n" + "please review the existing data in the parcel center, correct the import file, and reload."
			return false
		end if
		
////
////	we were successful so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_parceladdfromimport ( a_run_id,  a_template_id, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parceladdfromimport()
 **	
 **	add new parcels to the database from the import table.  this function will insert a new tax district if the parcel is one-to-one with tax districts.
 **	
 **	parameters	:		:	(a_run_id) the import run to use.
 **							:	(a_template_id) the import template to use.
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	table was edited successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.  in addtion, errors are written back to the import run table.
 **	
 ************************************************************************************************************************************************************/

/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the other of_parceladd() functions as all functions are used to add parcels.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi, pr_index, ff_index, td_index
		updt_ilook_id
	string	sqls, rtn_str, sqls_update_ff, sqls_update_td
	string	updt_key_col, updt_constr_cols
	string	constr_cols_arr[], flds_type[]
	boolean	do_updates, all_null_key_cols, update_ff, update_td, updt_constr_has_td, first_field_added

/*
 *	misc objects.
 */
	uo_ds_top ds_check
	nvo_ppbase_import_templates uo_templates
	s_ppbase_import_template_fields	s_flds[]
	
////
////	check to see if we're just adding or if we can do adding and updating.  
////	if we can do updates, get the unique key column for updating (i.e., for determining if something already exists in the database and needs to be updated or if it's new).
////	then get the constraint columns into an array.
////	finally, if we're updating get the fields for this run.
////
		uo_templates = create nvo_ppbase_import_templates
		
		do_updates = uo_templates.of_importtemplateisaddupdate( a_template_id )
		updt_ilook_id = uo_templates.of_importtemplategetupdateslookup( a_template_id, updt_key_col, updt_constr_cols )
		
		if do_updates and isnull( updt_ilook_id ) then
			a_msg = "no updates lookup is specified for this template.  please update the template to specify the unique key column and try again."
			destroy uo_templates
			return false
		end if
		
		if not isnull( updt_constr_cols ) then
			 uo_templates.of_importlookupgetconstrainingcolumns( updt_ilook_id, constr_cols_arr[] )
		end if
		
		if do_updates then 
			//get the fields - do not include autocreate fields, and only include fields on the table.
			if not uo_templates.of_importtemplategetfields( a_template_id, false, true, s_flds[], flds_type[], a_msg ) then
				destroy uo_templates
				return false
			end if
		end if
		
		destroy uo_templates
		
////
////	see if we need to join in tax district for the constraining columns.
////
		updt_constr_has_td = false
		
		maxi = upperbound( constr_cols_arr[] )
		for i = 1 to maxi
			if constr_cols_arr[i] = "assessor_id" or constr_cols_arr[i] = "county_id" then 
				updt_constr_has_td = true
				exit
			end if
		next		
		
////
////	if we're updating, see if the key column in populated in the database.
////
		all_null_key_cols = false
		
		if do_updates then
			sqls	=	"	select		count('x') " + "~r~n" + &
						"	from		pt_parcel " + "~r~n" + &
						"	where	trim( " + updt_key_col + " ) is not null "
			
			ds_check = create uo_ds_top
			rtn_str = f_create_dynamic_ds( ds_check, "grid", sqls, sqlca, true )
			
			if rtn_str <> "ok" then
				a_msg = "a database error occurred while checking the state of key columns in the database.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + rtn_str
				return false
			end if
			
			if ds_check.rowcount() = 0 then
				all_null_key_cols = true
			else
				if ds_check.getitemnumber( 1, 1 ) = 0 then
					all_null_key_cols = true
				else
					all_null_key_cols = false
				end if
			end if
		end if
		
////
////	if we're updatable mark those guys that have changed to speed things up (and ensure we don't update every row in the file if we have to).
////	be sure to update the ids for those that are changing as well.)
////
////	for parcels, fields may be changed on these three tables:
////	- pt_parcel
////	- pt_parcel_flex_fields
////	- prop_tax_district (if a 1-to-1 district)
////
////	note: we already translated to parcel_id in the validation function.
////
		update_ff = false
		update_td = false
		
		if do_updates then
			update	pt_import_parcel
			set			is_modified = 0
			where	import_run_id = :a_run_id;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while resetting the modified flag (parcel).  if you continue to receive this error contact your internal powerplant support or the " + &
					"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
			
			sqls =						"	update	pt_import_parcel imp " + "~r~n" + &
										"	set			imp.is_modified = 1 " + "~r~n" + &
										"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
										"	and		imp.parcel_id is not null " + "~r~n" + &
										"		and	exists " + "~r~n" + &
										"					(	select		'x' " + "~r~n" + &
										"						from		pt_parcel pr, " + "~r~n" + &
										"									pt_parcel_flex_fields ff, " + "~r~n" + &
										"									prop_tax_district td " + "~r~n" + &
										"						where	pr.parcel_id = ff.parcel_id " + "~r~n" + &
										"						and		pr.tax_district_id = td.tax_district_id " + "~r~n" + &
										"						and		pr.parcel_id = imp.parcel_id " + "~r~n" + &
										"						and		(			"
			
			maxi = upperbound( s_flds[] )
			
			first_field_added = false
			
			for i = 1 to maxi
				if s_flds[i].column_name = "is_parcel_one_to_one" then continue //this field can't be updated
				
				if first_field_added then
					sqls = sqls +	"										or		"
				end if
				
				first_field_added = true
				
				//process based on the table where the column is.
				if left( s_flds[i].column_name, 4 ) = "flex" then
					update_ff = true
					
					if left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
						sqls = sqls + "nvl( trim( imp." + s_flds[i].column_name + " ), '-90909' ) <> nvl( trim( ff." + s_flds[i].column_name + " ), '-90909' ) " + "~r~n"
					elseif left( flds_type[i], 6 ) = "number" then
						sqls = sqls + "nvl( imp." + s_flds[i].column_name + ", -90909 ) <> nvl( ff." + s_flds[i].column_name + ", -90909 ) " + "~r~n"
					elseif left( flds_type[i], 4 ) = "date" then
						sqls = sqls + "nvl( to_date( imp." + s_flds[i].column_name + ", 'mm/dd/yyyy' ), to_date( '1900-01-01', 'yyyy-mm-dd' ) ) <> nvl( ff." + s_flds[i].column_name + ", to_date( '1900-01-01', 'yyyy-mm-dd' ) ) " + "~r~n"
					end if
				elseif s_flds[i].column_name = "assessor_id" or s_flds[i].column_name = "assignment_indicator" or s_flds[i].column_name = "county_id" or s_flds[i].column_name = "grid_coordinate" or s_flds[i].column_name = "tax_district_code" or s_flds[i].column_name = "type_code_id" or s_flds[i].column_name = "use_composite_authority_yn" then
					update_td = true
					
					if left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
						sqls = sqls + "nvl( trim( imp." + s_flds[i].column_name + " ), '-90909' ) <> nvl( trim( td." + s_flds[i].column_name + " ), '-90909' ) " + "~r~n"
					elseif left( flds_type[i], 6 ) = "number" then
						sqls = sqls + "nvl( imp." + s_flds[i].column_name + ", -90909 ) <> nvl( td." + s_flds[i].column_name + ", -90909 ) " + "~r~n"
					elseif left( flds_type[i], 4 ) = "date" then
						sqls = sqls + "nvl( to_date( imp." + s_flds[i].column_name + ", 'mm/dd/yyyy' ), to_date( '1900-01-01', 'yyyy-mm-dd' ) ) <> nvl( td." + s_flds[i].column_name + ", to_date( '1900-01-01', 'yyyy-mm-dd' ) ) " + "~r~n"
					end if
				else
					if left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
						sqls = sqls + "nvl( trim( imp." + s_flds[i].column_name + " ), '-90909' ) <> nvl( trim( pr." + s_flds[i].column_name + " ), '-90909' ) " + "~r~n"
					elseif left( flds_type[i], 6 ) = "number" then
						sqls = sqls + "nvl( imp." + s_flds[i].column_name + ", -90909 ) <> nvl( pr." + s_flds[i].column_name + ", -90909 ) " + "~r~n"
					elseif left( flds_type[i], 4 ) = "date" then
						sqls = sqls + "nvl( to_date( imp." + s_flds[i].column_name + ", 'mm/dd/yyyy' ), to_date( '1900-01-01', 'yyyy-mm-dd' ) ) <> nvl( pr." + s_flds[i].column_name + ", to_date( '1900-01-01', 'yyyy-mm-dd' ) ) " + "~r~n"
					end if
				end if
			next
			
			sqls = sqls +			"									) " + "~r~n" + &
										"					) "
			
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while flagging the modified records (parcel).  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
					"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if		
		
////
////	do the updates (if we need to).  do these before the inserts to cut down on the number of rows touched (otherwise we're updating the rows we just inserted).  also, update anything that's in
////	here, even if it's updating to the same value - that's for speed reasons.  (if this causes a problem with the audit triggers showing the wrong times, so be it; if it's really a problem then you can
////	update it at that point - until then i'm leaving it as is.)
////
		if do_updates then
			sqls =					"	update	pt_parcel pr " + "~r~n" + &
									"	set		( "
								
			sqls_update_ff =	"	update	pt_parcel_flex_fields ff " + "~r~n" + &
									"	set		( "
			
			sqls_update_td =	"	update	prop_tax_district td " + "~r~n" + &
									"	set		( "
			
			maxi = upperbound( s_flds[] )
			
			pr_index = 1
			ff_index = 1
			td_index = 1
			
			for i = 1 to maxi
				if s_flds[i].column_name = "is_parcel_one_to_one" then continue //this field can't be updated
				
				if left( s_flds[i].column_name, 4 ) = "flex" then
					if ff_index > 1 then sqls_update_ff = sqls_update_ff + ", "
					sqls_update_ff = sqls_update_ff + "ff." + s_flds[i].column_name
					ff_index++
				elseif s_flds[i].column_name = "assessor_id" or s_flds[i].column_name = "assignment_indicator" or s_flds[i].column_name = "county_id" or s_flds[i].column_name = "grid_coordinate" or s_flds[i].column_name = "tax_district_code" or s_flds[i].column_name = "type_code_id" or s_flds[i].column_name = "use_composite_authority_yn" then
					if td_index > 1 then sqls_update_td = sqls_update_td + ", "
					sqls_update_td = sqls_update_td + "td." + s_flds[i].column_name
					td_index++
				else
					if pr_index > 1 then sqls = sqls + ", "
					sqls = sqls + "pr." + s_flds[i].column_name
					pr_index++
					
					//if they're updating state on the parcel, we want to update it on the district as well.  we only need to do this if it's 1-to-1, and that gets built into the where clause below.
					if s_flds[i].column_name = "state_id" then
						if td_index > 1 then sqls_update_td = sqls_update_td + ", "
						sqls_update_td = sqls_update_td + "td." + s_flds[i].column_name
						td_index++
					end if
				end if
			next
			
			sqls = sqls + &
							" ) = " + "~r~n" + &		
							"					(	select		"
								
			sqls_update_ff = sqls_update_ff + &
							" ) = " + "~r~n" + &		
							"					(	select		"
							
			sqls_update_td = sqls_update_td + &
							" ) = " + "~r~n" + &		
							"					(	select		"
			
			pr_index = 1
			ff_index = 1
			td_index = 1
			
			for i = 1 to maxi
				if s_flds[i].column_name = "is_parcel_one_to_one" then continue //this field can't be updated
				
				if left( s_flds[i].column_name, 4 ) = "flex" then
					if ff_index > 1 then sqls_update_ff = sqls_update_ff + ", "
					if left( flds_type[i], 4 ) = "date" then
						sqls_update_ff = sqls_update_ff + "to_date( imp." + s_flds[i].column_name + ", 'mm/dd/yyyy' )"
					elseif left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
						sqls_update_ff = sqls_update_ff + "trim( imp." + s_flds[i].column_name + " )"
					else
						sqls_update_ff = sqls_update_ff + "imp." + s_flds[i].column_name
					end if
					ff_index++
				elseif s_flds[i].column_name = "assessor_id" or s_flds[i].column_name = "assignment_indicator" or s_flds[i].column_name = "county_id" or s_flds[i].column_name = "grid_coordinate" or s_flds[i].column_name = "tax_district_code" or s_flds[i].column_name = "type_code_id" or s_flds[i].column_name = "use_composite_authority_yn" then
					if td_index > 1 then sqls_update_td = sqls_update_td + ", "
					if left( flds_type[i], 4 ) = "date" then
						sqls_update_td = sqls_update_td + "to_date( imp." + s_flds[i].column_name + ", 'mm/dd/yyyy' )"
					elseif left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
						sqls_update_td = sqls_update_td + "trim( imp." + s_flds[i].column_name + " )"
					else
						sqls_update_td = sqls_update_td + "imp." + s_flds[i].column_name
					end if
					td_index++
				else
					if pr_index > 1 then sqls = sqls + ", "
				
					//if they're updating tax district, the import file may contain some 1-to-1 districts.  to handle this, if the tax district in the import is null, use the existing tax district on the parcel.
					if s_flds[i].column_name = "tax_district_id" then
						sqls = sqls + "nvl( imp." + s_flds[i].column_name + ", pr." + s_flds[i].column_name + " )"
					else
						if left( flds_type[i], 4 ) = "date" then
							sqls = sqls + "to_date( imp." + s_flds[i].column_name + ", 'mm/dd/yyyy' )"
						elseif left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
							sqls = sqls + "trim( imp." + s_flds[i].column_name + " )"
						else
							sqls = sqls + "imp." + s_flds[i].column_name
						end if
					end if
					
					pr_index++
					
					//if they're updating state on the parcel, we want to update it on the district as well.  we only need to do this if it's 1-to-1, and that gets built into the where clause below.
					if s_flds[i].column_name = "state_id" then
						if td_index > 1 then sqls_update_td = sqls_update_td + ", "
						sqls_update_td = sqls_update_td + "trim( imp." + s_flds[i].column_name + " )"
						td_index++
					end if
				end if
			next
			
			sqls = sqls +	"						from		pt_import_parcel imp " + "~r~n" + &
								"						where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
								"						and		imp.parcel_id = pr.parcel_id " + "~r~n" + &
								"					) " + "~r~n" + &
								"	where		pr.parcel_id in " + "~r~n" + &
								"					(	select		parcel_id " + "~r~n" + &
								"						from		pt_import_parcel " + "~r~n" + &
								"						where	import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
								"						and		nvl( is_modified, 0 ) = 1 " + "~r~n" + &
								"					) "
								
			sqls_update_ff = sqls_update_ff +	&
								"						from		pt_import_parcel imp " + "~r~n" + &
								"						where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
								"						and		imp.parcel_id = ff.parcel_id " + "~r~n" + &
								"					) " + "~r~n" + &
								"	where		ff.parcel_id in " + "~r~n" + &
								"					(	select		parcel_id " + "~r~n" + &
								"						from		pt_import_parcel " + "~r~n" + &
								"						where	import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
								"						and		nvl( is_modified, 0 ) = 1 " + "~r~n" + &
								"					) "
								
			sqls_update_td = sqls_update_td + &
								"						from		pt_import_parcel imp, " + "~r~n" + &
								"									pt_parcel pr " + "~r~n" + &
								"						where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
								"						and		imp.parcel_id = pr.parcel_id " + "~r~n" + &
								"						and		pr.tax_district_id = td.tax_district_id " + "~r~n" + &
								"					) " + "~r~n" + &
								"	where		td.is_parcel_one_to_one = 1 " + "~r~n" + &
								"	and			td.tax_district_id in " + "~r~n" + &
								"					(	select		pr.tax_district_id " + "~r~n" + &
								"						from		pt_import_parcel imp, " + "~r~n" + &
								"									pt_parcel pr " + "~r~n" + &
								"						where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
								"						and		nvl( imp.is_modified, 0 ) = 1 " + "~r~n" + &
								"						and		imp.parcel_id = pr.parcel_id " + "~r~n" + &
								"					) "
			
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while updating existing records (parcel).  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
			
			if update_ff then
				execute immediate :sqls_update_ff;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while updating existing records (parcel flex fields).  if you continue to receive this error contact your internal powerplant support or " + &
						"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if
			
			if update_td then
				execute immediate :sqls_update_td;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while updating existing records (parcel one-to-one tax districts).  if you continue to receive this error contact your internal powerplant support or " + &
						"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if
		end if		
		
////
////	do the inserts.  before doing the inserts, put the new id on the import table for archiving.  if we're updating, we've already translated the records we can, so just update where parcel_id is null.
////	first update with a tax district id for the new parcels getting a 1-to-1 tax district.
////
		update	pt_import_parcel
		set			tax_district_id = nvl( ( select max( tax_district_id ) from prop_tax_district ), 0 ) + rownum
		where	import_run_id = :a_run_id
		and		nvl( is_parcel_one_to_one, 0 ) = 1
		and		parcel_id is null
		and		tax_district_id is null;
		
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while assigning internal ids to new records (1-to-1 tax districts).  if you continue to receive this error contact your internal powerplant support or " + &
				"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if	

		update	pt_import_parcel
		set			parcel_id = pt_parcel_seq.nextval
		where	import_run_id = :a_run_id
		and		parcel_id is null;

		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while assigning internal ids to new records (parcel).  if you continue to receive this error contact your internal powerplant support or " + &
				"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if		
		
////
////	create tax districts for new parcels with one-to-one tax districts.
////
		insert into prop_tax_district ( tax_district_id, description, description, state_id, county_id, assignment_indicator, type_code_id, grid_coordinate, tax_district_code, assessor_id, is_parcel_one_to_one, use_composite_authority_yn, notes )
			select		tax_district_id, 'parcel_id = '||to_char( parcel_id ), 'parcel_id = '||to_char( parcel_id ), state_id, county_id, to_number( assignment_indicator ), type_code_id, trim( grid_coordinate ), trim( tax_district_code ), assessor_id, is_parcel_one_to_one, use_composite_authority_yn, notes
			from		pt_import_parcel
			where	import_run_id = :a_run_id
				and	nvl( is_parcel_one_to_one, 0 ) = 1
				and	tax_district_id is not null
				and	tax_district_id not in
							(	select		tax_district_id
								from		prop_tax_district
							);
		
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while inserting new records (1-to-1 tax districts).  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
				"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	make our inserts into the parcel table for new records.
////
		insert into pt_parcel
		( parcel_id, parcel_number, description, description, parcel_type_id, prop_tax_company_id, state_id, tax_district_id, land_acreage, improve_sq_ft, mineral_acreage,
			legal_description, legal_description_b, legal_description_c, legal_description_d, grantor, address_1, address_2, city, zip_code, company_grid_number, state_grid_number,
			x_coordinate, y_coordinate, retired_date, notes )
			select		parcel_id, trim( parcel_number ), trim( description ), trim( description ), parcel_type_id, prop_tax_company_id, state_id, tax_district_id, to_number( land_acreage ), to_number( improve_sq_ft ), to_number( mineral_acreage ),
						trim( legal_description ), trim( legal_description_b ), trim( legal_description_c ), trim( legal_description_d ), trim( grantor ), trim( address_1 ), trim( address_2 ), trim( city ), trim( zip_code ), trim( company_grid_number ), trim( state_grid_number ),
						trim( x_coordinate ), trim( y_coordinate ), to_date( retired_date, 'mm/dd/yyyy' ), trim( notes )
			from		pt_import_parcel
			where	import_run_id = :a_run_id
				and	parcel_id is not null
				and	parcel_id not in
							(	select		parcel_id
								from		pt_parcel
							);
							
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while inserting new records (parcel).  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
				"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	make our inserts into the flex field table for new records.
////
		insert into pt_parcel_flex_fields
		( parcel_id, flex_1, flex_2, flex_3, flex_4, flex_5, flex_6, flex_7, flex_8, flex_9, flex_10, flex_11, flex_12, flex_13, flex_14, flex_15, flex_16, flex_17, flex_18, flex_19, flex_20,
			flex_21, flex_22, flex_23, flex_24, flex_25, flex_26, flex_27, flex_28, flex_29, flex_30, flex_31, flex_32, flex_33, flex_34, flex_35, flex_36, flex_37, flex_38, flex_39, flex_40,
			flex_41, flex_42, flex_43, flex_44, flex_45, flex_46, flex_47, flex_48, flex_49, flex_50 )
			select		parcel_id, to_number( flex_1 ), to_number( flex_2 ), to_number( flex_3 ), to_number( flex_4 ), to_number( flex_5 ), 
						to_date( flex_6, 'mm/dd/yyyy' ), to_date( flex_7, 'mm/dd/yyyy' ), to_date( flex_8, 'mm/dd/yyyy' ), to_date( flex_9, 'mm/dd/yyyy' ), to_date( flex_10, 'mm/dd/yyyy' ), 
						trim( flex_11 ), trim( flex_12 ), trim( flex_13 ), trim( flex_14 ), trim( flex_15 ), trim( flex_16 ), trim( flex_17 ), trim( flex_18 ), trim( flex_19 ), trim( flex_20 ), trim( flex_21 ), trim( flex_22 ), trim( flex_23 ),
						to_number( flex_24 ), to_number( flex_25 ), to_number( flex_26 ), to_number( flex_27 ), to_number( flex_28 ), 
						to_number( flex_29 ), to_number( flex_30 ), to_number( flex_31 ), to_number( flex_32 ), to_number( flex_33 ), 
						to_date( flex_34, 'mm/dd/yyyy' ), to_date( flex_35, 'mm/dd/yyyy' ), to_date( flex_36, 'mm/dd/yyyy' ), to_date( flex_37, 'mm/dd/yyyy' ), to_date( flex_38, 'mm/dd/yyyy' ), 
						trim( flex_39 ), trim( flex_40 ), trim( flex_41 ), trim( flex_42 ), trim( flex_43 ), trim( flex_44 ), trim( flex_45 ), trim( flex_46 ), trim( flex_47 ), trim( flex_48 ), trim( flex_49 ), trim( flex_50 )
			from		pt_import_parcel
			where	import_run_id = :a_run_id
				and	parcel_id is not null
				and	parcel_id not in
							(	select		parcel_id
								from		pt_parcel_flex_fields
							);
							
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while inserting new records (flex fields).  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
				"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	everything's ok so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_parcelvalidateaddfromimport ( a_run_id,  a_template_id, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelvalidateaddfromimport()
 **	
 **	validates all of the parcel fields for parcels being added/updated from the import tool.  this function will also validate tax district details if the parcel is one-to-one with tax districts.
 **	
 **	parameters	:		:	(a_run_id) the import run to validate.
 **							:	(a_template_id) the import template to use.
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	validation completed successfully (there could be error messages on the table, but the validation process completed successfully).
 **											false	:	an error occurred while validating.
 **	
 ************************************************************************************************************************************************************/

/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the of_parcelvalidate() function as both functions are used to validate parcels being added.
 **
 ************************************************************************************************************************************************************/

////
////	maint history for object changes
////
////	30996		qa discovered that if trying to import parcels that link to valid values, then the update statement checking for this fails if that parcel type has a flex field with no label
////				it tries to build out the sql in a string and fails because the label variable is null in this case.  changed to make label = "<no label on flex field>" if null


/*
 *	processing.
 */
		i, maxi, j, maxj
		updt_ilook_id, ptype_id, ff_id, use_id, validate_yn, flex_idx
	string	sqls, sqls2, rtn_str, sysval, ff_label, update_sqls
	string	updt_key_col, updt_constr_cols
	string	constr_cols_arr[], flds_type[]
	boolean	do_updates, all_null_key_cols, updt_constr_has_td
	boolean	temp_has_cty, temp_has_auth_yn, temp_has_1to1, temp_has_td
	boolean	temp_has_flex[]

/*
 *	misc objects.
 */
	uo_ds_top 	ds_check
	uo_ds_top	ds_ptype
	uo_ds_top	ds_ff
	nvo_ppbase_import_templates uo_templates
	s_ppbase_import_template_fields	s_flds[]
	
////
////	check to see if we're just adding or if we can do adding and updating.  
////	if we can do updates, get the unique key column for updating (i.e., for determining if something already exists in the database and needs to be updated or if it's new).
////	then get the constraint columns into an array.
////	finally, if we're updating, get the fields for this run.
////
		uo_templates = create nvo_ppbase_import_templates
		
		do_updates = uo_templates.of_importtemplateisaddupdate( a_template_id )
		updt_ilook_id = uo_templates.of_importtemplategetupdateslookup( a_template_id, updt_key_col, updt_constr_cols )
		
		if do_updates and isnull( updt_ilook_id ) then
			a_msg = "no updates lookup is specified for this template.  please update the template to specify the unique key column and try again."
			destroy uo_templates
			return false
		end if
		
		if not isnull( updt_constr_cols ) then
			 uo_templates.of_importlookupgetconstrainingcolumns( updt_ilook_id, constr_cols_arr[] )
		end if
		
		//get the fields - do not include autocreate fields, and only include fields on the table.  get them even if we're not updating, since we need to know what flex fields are in the template.
		if not uo_templates.of_importtemplategetfields( a_template_id, false, true, s_flds[], flds_type[], a_msg ) then
			destroy uo_templates
			return false
		end if
		
		destroy uo_templates
		
////
////	see if we need to join in tax district for the constraining columns.
////
		updt_constr_has_td = false
		
		maxi = upperbound( constr_cols_arr[] )
		for i = 1 to maxi
			if constr_cols_arr[i] = "assessor_id" or constr_cols_arr[i] = "county_id" then 
				updt_constr_has_td = true
				exit
			end if
		next		
		
////
////	if we're updating, see if any non-nullable fields are included in the template.
////	if so, we'll need to check later to make sure they're not null.
////
		maxj = upperbound( s_flds[] )
		
		if do_updates then
			temp_has_cty = false
			temp_has_auth_yn = false
			temp_has_1to1 = false
			temp_has_td = false
			
			for j = 1 to maxj
				if s_flds[j].column_name = "county_id" then temp_has_cty = true
				if s_flds[j].column_name = "use_composite_authority_yn" then temp_has_auth_yn = true
				if s_flds[j].column_name = "is_parcel_one_to_one" then temp_has_1to1 = true
				if s_flds[j].column_name = "tax_district_id" then temp_has_td = true
			next
		end if
		
////
////	see what flex fields are in the template, as we may need to validate some of those.
////
		for j = 1 to 50
			temp_has_flex[j] = false
		next
		
		for j = 1 to maxj
			if mid( s_flds[j].column_name, 1, 4 ) = "flex" then
				flex_idx = long( trim( mid( s_flds[j].column_name, 6 ) ) )
				temp_has_flex[flex_idx] = true
			end if
		next
		
////
////	if we're updating, see if the key column is populated in the database.
////
		all_null_key_cols = false
		
		if do_updates then
			sqls	=	"	select		count('x') " + "~r~n" + &
						"	from		pt_parcel " + "~r~n" + &
						"	where	trim( " + updt_key_col + " ) is not null "
			
			ds_check = create uo_ds_top
			rtn_str = f_create_dynamic_ds( ds_check, "grid", sqls, sqlca, true )
			
			if rtn_str <> "ok" then
				a_msg = "a database error occurred while checking the state of key columns in the database.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + rtn_str
				return false
			end if
			
			if ds_check.rowcount() = 0 then
				all_null_key_cols = true
			else
				if ds_check.getitemnumber( 1, 1 ) = 0 then
					all_null_key_cols = true
				else
					all_null_key_cols = false
				end if
			end if
		end if
		
////
////	if we're updating, make sure we don't have duplicate key columns in the database.
////
		if do_updates and not all_null_key_cols then
			sqls = 					"	update	pt_import_parcel " + "~r~n" + &
										"	set			error_message = substr( trim( nvl( error_message, '' ) || decode( trim( error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
										"				'multiple parcels exist in the database for this key column (" + string( updt_key_col ) + ").' ), 1, 4000 ) " + "~r~n" + &
										"	where	import_run_id = " + string( a_run_id ) + " " + "~r~n"
			
			if upperbound( constr_cols_arr[] ) = 0 then
				sqls = sqls +		"		and	( trim( " + updt_key_col + " ) ) in " + "~r~n" + &
										"					(	select		trim( " + updt_key_col + ") " + "~r~n"
			else
				if updt_constr_has_td then
					sqls = sqls +	"		and	( trim( " + updt_key_col + " ), " + updt_constr_cols + " ) in " + "~r~n" + &
										"					(	select		trim( pr." + updt_key_col + " ), "
										
					for i = 1 to maxi
						if i <> 1 then sqls = sqls + ", "
						if constr_cols_arr[i] = "assessor_id" or constr_cols_arr[i] = "county_id" then
							sqls = sqls + "td." + constr_cols_arr[i]
						else
							sqls = sqls + "pr." + constr_cols_arr[i]
						end if
					next
					
					sqls = sqls + " " + "~r~n"
				else
					sqls = sqls +	"		and	( trim( " + updt_key_col + " ), " + updt_constr_cols + " ) in " + "~r~n" + &
										"					(	select		trim( " + updt_key_col + " ), " + updt_constr_cols + " " + "~r~n"
				end if
			end if
			
			sqls = sqls +		"						from		pt_parcel pr"
			
			if updt_constr_has_td then
				sqls = sqls +	", " + "~r~n" + &
									"									prop_tax_district td " + "~r~n"
			else
				sqls = sqls + 	" " + "~r~n"
			end if
			
			sqls = sqls +		"						where	trim( " + updt_key_col + " ) is not null " + "~r~n"
			
			if updt_constr_has_td then
				sqls = sqls + 	"						and		pr.tax_district_id = td.tax_district_id " + "~r~n"
			end if
			
			sqls = sqls +		"						having	count('x') > 1 " + "~r~n"
			
			if upperbound( constr_cols_arr[] ) = 0 then
				sqls = sqls +	"						group by	trim( " + updt_key_col + " ) " + "~r~n"
			else
				if updt_constr_has_td then
					sqls = sqls +	"					group by	trim( pr." + updt_key_col + " ), "
										
					for i = 1 to maxi
						if i <> 1 then sqls = sqls + ", "
						if constr_cols_arr[i] = "assessor_id" or constr_cols_arr[i] = "county_id" then
							sqls = sqls + "td." + constr_cols_arr[i]
						else
							sqls = sqls + "pr." + constr_cols_arr[i]
						end if
					next
					
					sqls = sqls + " " + "~r~n"
				else
					sqls = sqls +	"						group by	trim( " + updt_key_col + " ), " + updt_constr_cols + " " + "~r~n"
				end if
			end if
			
			sqls = sqls +		"					) "
			
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while checking for duplicate entries by key column.  if you continue to receive this error contact your internal powerplant support or the " + &
					"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if		
		
////
////	if we're updating, translate to the parcel id for existing parcels.  this will make our validation checks much easier.
////	don't try to update for rows that have duplicate matches (which we flagged above).
////	if they're rerunning after changing something in the import file and now it doesn't translate to a parcel, the id will get nulled out (since we aren't checking to make sure a parcel exists).
////
		if do_updates then
			sqls =						"	update	pt_import_parcel imp " + "~r~n" + &
										"	set			imp.parcel_id = " + "~r~n" + &
										"					(	select		pr.parcel_id " + "~r~n" + &
										"						from		pt_parcel pr"
										
			if updt_constr_has_td then
				sqls = sqls +		"," + "~r~n" + &
										"									prop_tax_district td " + "~r~n"
			else
				sqls = sqls + " " + "~r~n"
			end if
									
			sqls = sqls +			"						where	trim( imp." + updt_key_col + " ) = trim( pr." + updt_key_col + " ) " + "~r~n"
			
			if updt_constr_has_td then
				sqls = sqls +		"						and		pr.tax_district_id = td.tax_district_id " + "~r~n"
			end if
			
			for i = 1 to maxi
				if constr_cols_arr[i] = "assessor_id" or constr_cols_arr[i] = "county_id" then
					sqls = sqls +	"						and		nvl( trim( imp." + constr_cols_arr[i] + " ), '-90909' ) = nvl( trim( td." + constr_cols_arr[i] + " ), '-90909' ) " + "~r~n"
				else
					sqls = sqls +	"						and		nvl( trim( imp." + constr_cols_arr[i] + " ), '-90909' ) = nvl( trim( pr." + constr_cols_arr[i] + " ), '-90909' ) " + "~r~n"
				end if
			next
			
			sqls = sqls +			"					) " + "~r~n" + &
										"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
										"		and	nvl( trim( imp.error_message ), '-90909' ) not like '%multiple parcels exist in the database for this key column%' " + "~r~n"
			
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while translating the existing parcels.  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
					"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
////
////	validate.
////

////
////	ensure all required fields are populated for parcels.
////
		update	pt_import_parcel
		set			error_message = substr( trim( nvl( error_message, '' ) || decode( trim( error_message ), null, '', '', '', '  /  ' ) || 
						case when prop_tax_company_id is null then 'prop tax company is a required field for creating a parcel.  ' else '' end ||
						case when trim( description ) is null or trim( description ) = '' then 'parcel description is a required field for creating a parcel.  ' else '' end ||
						case when trim( parcel_number ) is null or trim( parcel_number ) = '' then 'parcel number is a required field for creating a parcel.  ' else '' end ||
						case when parcel_type_id is null then 'parcel type is a required field for creating a parcel.  ' else '' end ||
						case when state_id is null then 'state is a required field for creating a parcel.  ' else '' end
						), 1, 4000 )
		where	import_run_id = :a_run_id
			and	(	prop_tax_company_id is null or
						trim( description ) is null or trim( description ) = '' or
						trim( parcel_number ) is null or trim( parcel_number ) = '' or
						parcel_type_id is null or
						state_id is null
					);
		
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while validating that all required fields are populated (parcel).  if you continue to receive this error contact your internal powerplant support or " + &
				"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	for new parcels, certain other fields are required.  check those here.
////
		update	pt_import_parcel
		set			error_message = substr( trim( nvl( error_message, '' ) || decode( trim( error_message ), null, '', '', '', '  /  ' ) || 
						case when tax_district_id is null and nvl( is_parcel_one_to_one, 0 ) = 0 then 'tax district is a required field for creating a parcel without a one-to-one tax district.  ' else '' end ||
						case when county_id is null and nvl( is_parcel_one_to_one, 0 ) = 1 then 'county is a required field for creating a parcel with a one-to-one tax district.  ' else '' end ||
						case when use_composite_authority_yn is null and nvl( is_parcel_one_to_one, 0 ) = 1 then 'use composite authorities is a required field for creating a parcel with a one-to-one tax district.  ' else '' end ||
						case when tax_district_id is not null and nvl( is_parcel_one_to_one, 0 ) = 1 then 'tax district should not be populated when creating a parcel with a one-to-one tax district.  ' else '' end
						), 1, 4000 )
		where	import_run_id = :a_run_id
			and	parcel_id is null
			and	(	( tax_district_id is null and nvl( is_parcel_one_to_one, 0 ) = 0 ) or
						( county_id is null and nvl( is_parcel_one_to_one, 0 ) = 1 ) or
						( use_composite_authority_yn is null and nvl( is_parcel_one_to_one, 0 ) = 1 ) or
						( tax_district_id is not null and nvl( is_parcel_one_to_one, 0 ) = 1 )
					);
		
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while validating that all required fields are populated for new records (parcel).  if you continue to receive this error contact your internal powerplant support or " + &
				"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	for existing parcels, certain other fields are required.  check those here to make sure they're not being nulled out.
////	only check if the field is included in the template; otherwise, it won't get updated.
////
		if do_updates then
			if temp_has_cty then
				//county is required for 1-to-1 parcels.
				update	pt_import_parcel imp
				set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
								'county is a required field for parcels with one-to-one tax districts.' ), 1, 4000 )
				where	imp.import_run_id = :a_run_id
					and	imp.parcel_id is not null
					and	imp.county_id is null
					and	exists
							(	select		'x'
								from		pt_parcel pr,
											prop_tax_district td
								where	pr.tax_district_id = td.tax_district_id
								and		pr.parcel_id = imp.parcel_id
								and		td.is_parcel_one_to_one = 1
							);
							
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while validating that 'county' is populated for all existing 1-to-1 parcels.  if you continue to receive this error contact your internal powerplant support or " + &
						"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if
			
			if temp_has_auth_yn then
				//use composite authority yn is required for 1-to-1 parcels.  since the 1-to-1 indicator can't be changed on an existing parcel, look at the indicator on the currently assigned tax district.
				update	pt_import_parcel imp
				set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
								'use composite authorities is a required field for parcels with one-to-one tax districts.' ), 1, 4000 )
				where	imp.import_run_id = :a_run_id
					and	imp.parcel_id is not null
					and	imp.use_composite_authority_yn is null
					and	exists
							(	select		'x'
								from		pt_parcel pr,
											prop_tax_district td
								where	pr.tax_district_id = td.tax_district_id
								and		pr.parcel_id = imp.parcel_id
								and		td.is_parcel_one_to_one = 1
							);
							
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while validating that 'use composite authorities' is populated for all existing 1-to-1 parcels.  if you continue to receive this error contact your internal powerplant support or " + &
						"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if
			
			if temp_has_td then
				//tax district is required for all non-1-to-1 parcels.
				update	pt_import_parcel imp
				set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
								'tax district is a required field for all parcels without a one-to-one district.' ), 1, 4000 )
				where	imp.import_run_id = :a_run_id
					and	imp.parcel_id is not null
					and	imp.tax_district_id is null
					and	exists
							(	select		'x'
								from		pt_parcel p,
											prop_tax_district td
								where	p.parcel_id = imp.parcel_id
								and		p.tax_district_id = td.tax_district_id
								and		td.is_parcel_one_to_one = 0
							);
							
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while validating that 'tax district' is populated for all existing parcels without a one-to-one district.  if you continue to receive this error contact your internal powerplant support or " + &
						"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if
		end if
		
////
////	if we're updating, we can't change the "is_parcel_one_to_one" field on a tax district after it's been created.  check to make sure they aren't doing that (if the field is in the template).
////
		if do_updates then
			if temp_has_1to1 then
				update	pt_import_parcel imp
				set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
								'the is district 1-to-1 field cannot be changed on an existing parcel.' ), 1, 4000 )
				where	imp.import_run_id = :a_run_id
					and	imp.parcel_id is not null
					and	imp.is_parcel_one_to_one is not null
					and	exists
							(	select		'x'
								from		pt_parcel pr,
											prop_tax_district td
								where	pr.tax_district_id = td.tax_district_id
								and		pr.parcel_id = imp.parcel_id
								and		td.is_parcel_one_to_one <> imp.is_parcel_one_to_one
							);
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while validating that the 'is district 1-to-1' field is not being changed for any existing parcels.  if you continue to receive this error contact your internal powerplant support or " + &
						"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if
		end if
		
////
////	if we're updating, we can't change the "tax_district_id" field on a 1-to-1 parcel after it's been created.  check to make sure they aren't doing that.
////
		if do_updates and temp_has_td then
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'the tax district field cannot be changed on an existing 1-to-1 parcel.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	imp.parcel_id is not null
				and	imp.tax_district_id is not null
				and	exists
						(	select		'x'
							from		pt_parcel pr,
										prop_tax_district td
							where	pr.tax_district_id = td.tax_district_id
							and		pr.parcel_id = imp.parcel_id
							and		pr.tax_district_id <> imp.tax_district_id
							and		td.is_parcel_one_to_one = 1
						);
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that the 'tax district' field is not being changed for any existing 1-to-1 parcels.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
////
////	if we're updating, there are certain fields that can't be changed once a parcel has been created.  check for any violations.
////
////	- state - cannot change if the parcel is assigned to any bills or ledger items.
////	- prop tax company - cannot change if the parcel is assigned to any locations, bills, or ledger items.
////	- parcel type - cannot change if the parcel is assigned to any locations.
////
		if do_updates then
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'the state field cannot be changed on an existing parcel if it is assigned to any bills or ledger items.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	imp.parcel_id is not null
				and	imp.state_id is not null
				and	exists
						(	select		'x'
							from		pt_parcel pr
							where	pr.parcel_id = imp.parcel_id
							and		pr.state_id <> imp.state_id
						)
				and	(		exists
								(	select		'x'
									from		pt_statement_line sl
									where	sl.parcel_id = imp.parcel_id
								)
							or	exists
								(	select		'x'
									from		pt_ledger ldg
									where	ldg.parcel_id = imp.parcel_id
								)
						);
								
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that the 'state' field is not being changed for any existing parcels with bills or ledger items.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
			
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'the prop tax company field cannot be changed on an existing parcel if it is assigned to any locations, bills, or ledger items.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	imp.parcel_id is not null
				and	imp.prop_tax_company_id is not null
				and	exists
						(	select		'x'
							from		pt_parcel pr
							where	pr.parcel_id = imp.parcel_id
							and		pr.prop_tax_company_id <> imp.prop_tax_company_id
						)
				and	(		exists
								(	select		'x'
									from		pt_statement_line sl
									where	sl.parcel_id = imp.parcel_id
								)
							or	exists
								(	select		'x'
									from		pt_ledger ldg
									where	ldg.parcel_id = imp.parcel_id
								)
							or	exists
								(	select		'x'
									from		pt_parcel_location ploc
									where	ploc.parcel_id = imp.parcel_id
								)
						);
								
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that the 'prop tax company' field is not being changed for any existing parcels with locations, bills, or ledger items.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
			
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'the parcel type field cannot be changed on an existing parcel if it is assigned to any locations.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	imp.parcel_id is not null
				and	imp.parcel_type_id is not null
				and	exists
						(	select		'x'
							from		pt_parcel pr
							where	pr.parcel_id = imp.parcel_id
							and		pr.parcel_type_id <> imp.parcel_type_id
						)
				and	exists
						(	select		'x'
							from		pt_parcel_location ploc
							where	ploc.parcel_id = imp.parcel_id
						);
								
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that the 'parcel type' field is not being changed for any existing parcels with locations.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if

////
////	make sure we aren't importing a parcel number that already exists in the database (if we're not doing updates), as parcel number should always be unique.  
////	if we're doing updates and the key column is not the parcel number then also check.  (if we're doing updates and the parcel number is the key column we'd expect duplicates.)
////	we want to make sure they're not changing the parcel number and making it a duplicate, or that we're not adding a parcel number that already exists.
////	get the system control to see what determines a unique parcel.
////
		sysval = upper( trim( i_uo_systemoptions.of_getsystemoption( "parcels - parcel uniqueness columns" ) ) )
		
		if not do_updates then
			choose case sysval
				case	"parcel number"
					update	pt_import_parcel imp
					set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
								'this parcel number already exists.' ), 1, 4000 )
					where	imp.import_run_id = :a_run_id
						and	lower( trim( imp.parcel_number ) ) in
									(	select		lower( trim( parcel_number ) )
										from		pt_parcel
									);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate entries by parcel number.  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				case	"parcel number, state"
					update	pt_import_parcel imp
					set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
								'this parcel number already exists for the given state.' ), 1, 4000 )
					where	imp.import_run_id = :a_run_id
						and	lower( trim( imp.parcel_number ) ) in
									(	select		lower( trim( parcel_number ) )
										from		pt_parcel
										where	state_id = imp.state_id
									);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate entries by parcel number and state.  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				case	"parcel number, state, county"
					//for parcels with a non 1-to-1 tax district.
					update	pt_import_parcel imp
					set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
								'this parcel number already exists for the given state and county.' ), 1, 4000 )
					where	imp.import_run_id = :a_run_id
						and	imp.tax_district_id is not null
						and	nvl( imp.is_parcel_one_to_one, 0 ) = 0
						and	lower( trim( imp.parcel_number ) ) in
									(	select		lower( trim( pr.parcel_number ) )
										from		pt_parcel pr,
													prop_tax_district pr_td,
													prop_tax_district imp_td
										where	pr.tax_district_id = pr_td.tax_district_id
										and		imp.tax_district_id = imp_td.tax_district_id
										and		pr_td.state_id = imp_td.state_id
										and		pr_td.county_id = imp_td.county_id
									) ;
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate entries by parcel number, state, and county.  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
					
					//for parcels with a 1-to-1 tax district.
					update	pt_import_parcel imp
					set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
								'this parcel number already exists for the given state and county.' ), 1, 4000 )
					where	imp.import_run_id = :a_run_id
						and	nvl( imp.is_parcel_one_to_one, 0 ) = 1
						and	imp.county_id is not null
						and	lower( trim( imp.parcel_number ) ) in
									(	select		lower( trim( pr.parcel_number ) )
										from		pt_parcel pr,
													prop_tax_district pr_td
										where	pr.tax_district_id = pr_td.tax_district_id
										and		pr_td.state_id = imp.state_id
										and		pr_td.county_id = imp.county_id
									);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate entries by parcel number, state, and county (1-to-1 tax district).  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				case	"parcel number, state, prop tax company"
					update	pt_import_parcel imp
					set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
								'this parcel number already exists for the given state and prop tax company.' ), 1, 4000 )
					where	imp.import_run_id = :a_run_id
						and	lower( trim( imp.parcel_number ) ) in
									(	select		lower( trim( parcel_number ) )
										from		pt_parcel
										where	state_id = imp.state_id
										and		prop_tax_company_id = imp.prop_tax_company_id
									);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate entries by parcel number, state, and prop tax company.  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				case	"parcel number, state, county, prop tax company"
					//for parcels with a non 1-to-1 tax district.
					update	pt_import_parcel imp
					set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
								'this parcel number already exists for the given state, county, and prop tax company.' ), 1, 4000 )
					where	imp.import_run_id = :a_run_id
						and	imp.tax_district_id is not null
						and	nvl( imp.is_parcel_one_to_one, 0 ) = 0
						and	lower( trim( imp.parcel_number ) ) in
									(	select		lower( trim( pr.parcel_number ) )
										from		pt_parcel pr,
													prop_tax_district pr_td,
													prop_tax_district imp_td
										where	pr.tax_district_id = pr_td.tax_district_id
										and		imp.tax_district_id = imp_td.tax_district_id
										and		pr_td.state_id = imp_td.state_id
										and		pr_td.county_id = imp_td.county_id
										and		pr.prop_tax_company_id = imp.prop_tax_company_id
									);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate entries by parcel number, state, county, and prop tax company.  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
					
					//for parcels with a 1-to-1 tax district.
					update	pt_import_parcel imp
					set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
								'this parcel number already exists for the given state, county, and prop tax company.' ), 1, 4000 )
					where	imp.import_run_id = :a_run_id
						and	nvl( imp.is_parcel_one_to_one, 0 ) = 1
						and	imp.county_id is not null
						and	lower( trim( imp.parcel_number ) ) in
									(	select		lower( trim( pr.parcel_number ) )
										from		pt_parcel pr,
													prop_tax_district pr_td
										where	pr.tax_district_id = pr_td.tax_district_id
										and		pr_td.state_id = imp.state_id
										and		pr_td.county_id = imp.county_id
										and		pr.prop_tax_company_id = imp.prop_tax_company_id
									);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate entries by parcel number, state, county, and prop tax company (1-to-1 tax district).  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				case	else
					a_msg = "unable to determine whether this is a unique parcel.  please check the system options for the property tax center (admin center, system options) to ensure that a " + &
						"system control named 'parcels - parcel uniqueness columns' is available and that values are set for that option.  if the system control is not present or there is another " + &
						"problem with the system option contact the powerplan property tax support desk for assistance."
					return false
			end choose
		else
			if updt_key_col <> "parcel_number" then
				choose case sysval
					case	"parcel number"
						update	pt_import_parcel imp
						set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
									'this parcel number already exists.' ), 1, 4000 )
						where	imp.import_run_id = :a_run_id
							and	exists
										(	select		'x'
											from		pt_parcel pr
											where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) )
											and		pr.parcel_id <> nvl( imp.parcel_id, -90909 )
										);
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while checking for duplicate entries by parcel number.  if you continue to receive this error contact your internal powerplant support or the " + &
								"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					case	"parcel number, state"
						//state will be in the template because it's required.
						update	pt_import_parcel imp
						set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
									'this parcel number already exists for the given state.' ), 1, 4000 )
						where	imp.import_run_id = :a_run_id
							and	exists
										(	select		'x'
											from		pt_parcel pr
											where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) )
											and		pr.state_id = imp.state_id
											and		pr.parcel_id <> nvl( imp.parcel_id, -90909 )
										);
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while checking for duplicate entries by parcel number and state.  if you continue to receive this error contact your internal powerplant support or the " + &
								"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					case	"parcel number, state, county"
						//for parcels with a non 1-to-1 tax district.
						//if the template includes the "tax district" field, use the county on the tax district in the import table (it will be required for both new and existing parcels if it's in the template).
						//if the template does not include the "tax district" field, then all the parcels being added must be 1-to-1, so we don't have to worry about checking them here, but the parcels being updated could be non 1-to-1.  
						//use the tax district currently assigned in pt_parcel to get the county.
						if temp_has_td then
							update	pt_import_parcel imp
							set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
										'this parcel number already exists for the given state and county.' ), 1, 4000 )
							where	imp.import_run_id = :a_run_id
								and	imp.tax_district_id is not null
								and	exists
											(	select		'x'
												from		pt_parcel pr,
															prop_tax_district pr_td,
															prop_tax_district imp_td
												where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) )
												and		pr.tax_district_id = pr_td.tax_district_id
												and		imp.tax_district_id = imp_td.tax_district_id
												and		pr_td.state_id = imp_td.state_id
												and		pr_td.county_id = imp_td.county_id
												and		pr.parcel_id <> nvl( imp.parcel_id, -90909 )
												and		imp_td.is_parcel_one_to_one = 0
											);
							
							if sqlca.sqlcode <> 0 then
								a_msg = "a database error occurred while checking for duplicate entries by parcel number, state, and county.  if you continue to receive this error contact your internal powerplant support or the " + &
									"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
								return false
							end if
						else
							update	pt_import_parcel imp
							set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
										'this parcel number already exists for the given state and county.' ), 1, 4000 )
							where	imp.import_run_id = :a_run_id
								and	imp.parcel_id is not null
								and	exists
											(	select		'x'
												from		pt_parcel pr,
															prop_tax_district td,
															pt_parcel imp_pr,
															prop_tax_district imp_td
												where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) )
												and		pr.parcel_id <> imp.parcel_id
												and		pr.tax_district_id = td.tax_district_id
												and		imp_pr.parcel_id = imp.parcel_id
												and		imp_pr.tax_district_id = imp_td.tax_district_id
												and		td.state_id = imp_td.state_id
												and		td.county_id = imp_td.county_id
												and		imp_td.is_parcel_one_to_one = 0
											);
							
							if sqlca.sqlcode <> 0 then
								a_msg = "a database error occurred while checking for duplicate entries by parcel number, state, and county.  if you continue to receive this error contact your internal powerplant support or the " + &
									"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
								return false
							end if
						end if
						
						//for parcels with a 1-to-1 tax district.
						//if the template includes the "county" field, use the county on the import table (it's required for 1-to-1 parcels).
						//if the template does not include the "county" field, then all the parcels being added must be non 1-to-1, so we don't have to worry about checking them here, but the parcels being updated could be 1-to-1.  
						//use the tax district currently assigned in pt_parcel to get the county, as tax districts can't be changed on 1-to-1 parcels.
						if temp_has_cty then
							update	pt_import_parcel imp
							set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
										'this parcel number already exists for the given state and county.' ), 1, 4000 )
							where	imp.import_run_id = :a_run_id
								and	imp.county_id is not null
								and	(	nvl( imp.is_parcel_one_to_one, 0 ) = 1
											or	exists
												(	select		'x'
													from		pt_parcel p,
																prop_tax_district td
													where	p.parcel_id = imp.parcel_id
													and		p.tax_district_id = td.tax_district_id
													and		td.is_parcel_one_to_one = 1
												)
										)
								and	exists
											(	select		'x'
												from		pt_parcel pr,
															prop_tax_district td
												where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) )
												and		pr.parcel_id <> nvl( imp.parcel_id, -90909 )
												and		pr.tax_district_id = td.tax_district_id
												and		td.state_id = imp.state_id
												and		td.county_id = imp.county_id
											);
							
							if sqlca.sqlcode <> 0 then
								a_msg = "a database error occurred while checking for duplicate entries by parcel number, state, and county (1-to-1 tax district).  if you continue to receive this error contact your internal powerplant support or the " + &
									"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
								return false
							end if
						else
							update	pt_import_parcel imp
							set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
										'this parcel number already exists for the given state and county.' ), 1, 4000 )
							where	imp.import_run_id = :a_run_id
								and	imp.parcel_id is not null
								and	exists
											(	select		'x'
												from		pt_parcel pr,
															prop_tax_district td,
															pt_parcel imp_pr,
															prop_tax_district imp_td
												where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) )
												and		pr.parcel_id <> imp.parcel_id
												and		pr.tax_district_id = td.tax_district_id
												and		imp_pr.parcel_id = imp.parcel_id
												and		imp_pr.tax_district_id = imp_td.tax_district_id
												and		td.state_id = imp_td.state_id
												and		td.county_id = imp_td.county_id
												and		imp_td.is_parcel_one_to_one = 1
											);
							
							if sqlca.sqlcode <> 0 then
								a_msg = "a database error occurred while checking for duplicate entries by parcel number, state, and county (1-to-1 tax district).  if you continue to receive this error contact your internal powerplant support or the " + &
									"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
								return false
							end if
						end if
					case	"parcel number, state, prop tax company"
						update	pt_import_parcel imp
						set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
									'this parcel number already exists for the given state and prop tax company.' ), 1, 4000 )
						where	imp.import_run_id = :a_run_id
							and	exists
										(	select		'x'
											from		pt_parcel pr
											where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) )
											and		pr.state_id = imp.state_id
											and		pr.prop_tax_company_id = imp.prop_tax_company_id
											and		pr.parcel_id <> nvl( imp.parcel_id, -90909 )
										);
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while checking for duplicate entries by parcel number, state, and prop tax company.  if you continue to receive this error contact your internal powerplant support or the " + &
								"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					case	"parcel number, state, county, prop tax company"
						//for parcels with a non 1-to-1 tax district.
						//if the template includes the "tax district" field, use the county on the tax district in the import table (it will be required for both new and existing parcels if it's in the template).
						//if the template does not include the "tax district" field, then all the parcels being added must be 1-to-1, so we don't have to worry about checking them here, but the parcels being updated could be non 1-to-1.  
						//use the tax district currently assigned in pt_parcel to get the county.
						if temp_has_td then
							update	pt_import_parcel imp
							set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
										'this parcel number already exists for the given state, county, and prop tax company.' ), 1, 4000 )
							where	imp.import_run_id = :a_run_id
								and	imp.tax_district_id is not null
								and	exists
											(	select		'x'
												from		pt_parcel pr,
															prop_tax_district pr_td,
															prop_tax_district imp_td
												where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) )
												and		pr.tax_district_id = pr_td.tax_district_id
												and		imp.tax_district_id = imp_td.tax_district_id
												and		pr_td.state_id = imp_td.state_id
												and		pr_td.county_id = imp_td.county_id
												and		pr.prop_tax_company_id = imp.prop_tax_company_id
												and		pr.parcel_id <> nvl( imp.parcel_id, -90909 )
												and		imp_td.is_parcel_one_to_one = 0
											);
							
							if sqlca.sqlcode <> 0 then
								a_msg = "a database error occurred while checking for duplicate entries by parcel number, state, county, and prop tax company.  if you continue to receive this error contact your internal powerplant support or the " + &
									"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
								return false
							end if
						else
							update	pt_import_parcel imp
							set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
										'this parcel number already exists for the given state, county, and prop tax company.' ), 1, 4000 )
							where	imp.import_run_id = :a_run_id
								and	imp.parcel_id is not null
								and	exists
											(	select		'x'
												from		pt_parcel pr,
															prop_tax_district td,
															pt_parcel imp_pr,
															prop_tax_district imp_td
												where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) )
												and		pr.parcel_id <> imp.parcel_id
												and		pr.tax_district_id = td.tax_district_id
												and		imp_pr.parcel_id = imp.parcel_id
												and		imp_pr.tax_district_id = imp_td.tax_district_id
												and		td.state_id = imp_td.state_id
												and		td.county_id = imp_td.county_id
												and		pr.prop_tax_company_id = imp.prop_tax_company_id
												and		imp_td.is_parcel_one_to_one = 0
											);
							
							if sqlca.sqlcode <> 0 then
								a_msg = "a database error occurred while checking for duplicate entries by parcel number, state, county, and prop tax company.  if you continue to receive this error contact your internal powerplant support or the " + &
									"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
								return false
							end if
						end if		
							
						//for parcels with a 1-to-1 tax district.
						//if the template includes the "county" field, use the county on the import table (it's required for 1-to-1 parcels).
						//if the template does not include the "county" field, then all the parcels being added must be non 1-to-1, so we don't have to worry about checking them here, but the parcels being updated could be 1-to-1.  
						//use the tax district currently assigned in pt_parcel to get the county, as tax districts can't be changed on 1-to-1 parcels.
						if temp_has_cty then
							update	pt_import_parcel imp
							set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
										'this parcel number already exists for the given state, county, and prop tax company.' ), 1, 4000 )
							where	imp.import_run_id = :a_run_id
								and	imp.county_id is not null
								and	(	nvl( imp.is_parcel_one_to_one, 0 ) = 1
											or	exists
												(	select		'x'
													from		pt_parcel p,
																prop_tax_district td
													where	p.parcel_id = imp.parcel_id
													and		p.tax_district_id = td.tax_district_id
													and		td.is_parcel_one_to_one = 1
												)
										)
								and	exists
											(	select		'x'
												from		pt_parcel pr,
															prop_tax_district td
												where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) )
												and		pr.parcel_id <> nvl( imp.parcel_id, -90909 )
												and		pr.tax_district_id = td.tax_district_id
												and		td.state_id = imp.state_id
												and		td.county_id = imp.county_id
												and		pr.prop_tax_company_id = imp.prop_tax_company_id
											);
							
							if sqlca.sqlcode <> 0 then
								a_msg = "a database error occurred while checking for duplicate entries by parcel number, state, county, and prop tax company (1-to-1 tax district).  if you continue to receive this error contact your internal powerplant support or the " + &
									"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
								return false
							end if
						else
							update	pt_import_parcel imp
							set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
										'this parcel number already exists for the given state, county, and prop tax company.' ), 1, 4000 )
							where	imp.import_run_id = :a_run_id
								and	imp.parcel_id is not null
								and	exists
											(	select		'x'
												from		pt_parcel pr,
															prop_tax_district td,
															pt_parcel imp_pr,
															prop_tax_district imp_td
												where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) )
												and		pr.parcel_id <> imp.parcel_id
												and		pr.tax_district_id = td.tax_district_id
												and		imp_pr.parcel_id = imp.parcel_id
												and		imp_pr.tax_district_id = imp_td.tax_district_id
												and		td.state_id = imp_td.state_id
												and		td.county_id = imp_td.county_id
												and		pr.prop_tax_company_id = imp.prop_tax_company_id
												and		imp_td.is_parcel_one_to_one = 1
											);
							
							if sqlca.sqlcode <> 0 then
								a_msg = "a database error occurred while checking for duplicate entries by parcel number, state, county, and prop tax company (1-to-1 tax district).  if you continue to receive this error contact your internal powerplant support or the " + &
									"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
								return false
							end if
						end if
					case	else
						a_msg = "unable to determine whether this is a unique parcel.  please check the system options for the property tax center (admin center, system options) to ensure that a " + &
							"system control named 'parcels - parcel uniqueness columns' is available and that values are set for that option.  if the system control is not present or there is another " + &
							"problem with the system option contact the powerplan property tax support desk for assistance."
					return false
				end choose
			end if
		end if
		
////
////	check to see if the import contains two identical rows and flag them.  make sure there are no duplicate parcel numbers based on the system option.
////	then, if we're updating, make sure there aren't duplicate key columns.
////
		////
		////	parcel number.
		////
			choose case sysval
				case	"parcel number"
					update	pt_import_parcel
					set			error_message = substr( trim( nvl( error_message, '' ) || decode( trim( error_message ), null, '', '', '', '  /  ' ) || 'this parcel number is duplicated in the import file.' ), 1, 4000 )
					where	import_run_id = :a_run_id
						and	lower( trim( parcel_number ) ) in
									(	select		lower( trim( parcel_number ) )
										from		pt_import_parcel
										where	import_run_id = :a_run_id
										having	count('x') > 1
										group by	lower( trim( parcel_number ) )
									);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate import rows on parcel number.  if you continue to receive this error contact your internal powerplant support " + &
							"or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				case	"parcel number, state"
					update	pt_import_parcel
					set			error_message = substr( trim( nvl( error_message, '' ) || decode( trim( error_message ), null, '', '', '', '  /  ' ) || 'this parcel number is duplicated in the import file for the given state.' ), 1, 4000 )
					where	import_run_id = :a_run_id
						and	( lower( trim( parcel_number ) ), state_id ) in
									(	select		lower( trim( parcel_number ) ), state_id
										from		pt_import_parcel
										where	import_run_id = :a_run_id
										having	count('x') > 1
										group by	lower( trim( parcel_number ) ), state_id
									);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate import rows on parcel number and state.  if you continue to receive this error contact your internal powerplant support " + &
							"or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				case	"parcel number, state, county"
					//county may come from several different places.  we have to check for duplicates all in one update statement; otherwise, we may miss some.  
					//check the following places for county in this order:
					//- for non 1-to-1 parcels, use the county_id on the tax_district_id from the import file if it's populated.  otherwise, use the county_id on the parcel's assigned tax district.
					//- for 1-to-1 parcels, use county_id from the import file if it's populated.  otherwise, use the county_id on the parcel's assigned tax district.
					//when checking the 1-to-1 indicator, first check on the existing td, if it's an existing parcel, as the indicator can't change.  if it's not an existing parcel, get it from the import file.
					update	pt_import_parcel imp
					set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 'this parcel number is duplicated in the import file for the given state and county.' ), 1, 4000 )
					where	imp.import_run_id = :a_run_id
					and		imp.line_id in
								(	select		imp.line_id
									from		pt_import_parcel imp,
												prop_tax_district imp_td,
												(	select		p.parcel_id parcel_id,
																td.county_id county_id,
																td.is_parcel_one_to_one is_parcel_one_to_one
													from		pt_import_parcel imp,
																pt_parcel p,
																prop_tax_district td
													where	imp.import_run_id = :a_run_id
													and		imp.parcel_id = p.parcel_id
													and		p.tax_district_id = td.tax_district_id
												) p_view
									where	imp.import_run_id = :a_run_id
									and		imp.tax_district_id = imp_td.tax_district_id (+)
									and		imp.parcel_id = p_view.parcel_id (+)	
									and		( lower( trim( imp.parcel_number ) ), imp.state_id, decode( nvl( p_view.is_parcel_one_to_one, nvl( imp.is_parcel_one_to_one, 0 ) ), 0, nvl( imp_td.county_id, p_view.county_id ), nvl( imp.county_id, p_view.county_id ) ) ) in
												(	select		lower( trim( imp.parcel_number ) ), 
																imp.state_id, 
																decode( nvl( p_view.is_parcel_one_to_one, nvl( imp.is_parcel_one_to_one, 0 ) ), 0, nvl( imp_td.county_id, p_view.county_id ), nvl( imp.county_id, p_view.county_id ) )
													from		pt_import_parcel imp,
																prop_tax_district imp_td,
																(	select		p.parcel_id parcel_id,
																				td.county_id county_id,
																				td.is_parcel_one_to_one is_parcel_one_to_one
																	from		pt_import_parcel imp,
																				pt_parcel p,
																				prop_tax_district td
																	where	imp.import_run_id = :a_run_id
																	and		imp.parcel_id = p.parcel_id
																	and		p.tax_district_id = td.tax_district_id
																) p_view
													where	imp.import_run_id = :a_run_id
													and		imp.tax_district_id = imp_td.tax_district_id (+)
													and		imp.parcel_id = p_view.parcel_id (+)
													having	count('x') > 1
													group by	lower( trim( imp.parcel_number ) ), 
																imp.state_id, 
																decode( nvl( p_view.is_parcel_one_to_one, nvl( imp.is_parcel_one_to_one, 0 ) ), 0, nvl( imp_td.county_id, p_view.county_id ), nvl( imp.county_id, p_view.county_id ) )
												)
								);
		
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate import rows on parcel number, state, and county.  if you continue to receive this error contact your internal powerplant support " + &
							"or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				case	"parcel number, state, prop tax company"
					update	pt_import_parcel
					set			error_message = substr( trim( nvl( error_message, '' ) || decode( trim( error_message ), null, '', '', '', '  /  ' ) || 'this parcel number is duplicated in the import file for the given state and prop tax company.' ), 1, 4000 )
					where	import_run_id = :a_run_id
						and	( lower( trim( parcel_number ) ), state_id, prop_tax_company_id ) in
									(	select		lower( trim( parcel_number ) ), state_id, prop_tax_company_id
										from		pt_import_parcel
										where	import_run_id = :a_run_id
										having	count('x') > 1
										group by	lower( trim( parcel_number ) ), state_id, prop_tax_company_id
									);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate import rows on parcel number, state, and prop tax company.  if you continue to receive this error contact your internal powerplant support " + &
							"or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				case	"parcel number, state, county, prop tax company"
					//county may come from several different places.  we have to check for duplicates all in one update statement; otherwise, we may miss some.  
					//check the following places for county in this order:
					//- for non 1-to-1 parcels, use the county_id on the tax_district_id from the import file if it's populated.  otherwise, use the county_id on the parcel's assigned tax district.
					//- for 1-to-1 parcels, use county_id from the import file if it's populated.  otherwise, use the county_id on the parcel's assigned tax district.
					//when checking the 1-to-1 indicator, first check on the existing td, if it's an existing parcel, as the indicator can't change.  if it's not an existing parcel, get it from the import file.
					update	pt_import_parcel imp
					set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 'this parcel number is duplicated in the import file for the given state, county, and prop tax company.' ), 1, 4000 )
					where	imp.import_run_id = :a_run_id
					and		imp.line_id in
								(	select		imp.line_id
									from		pt_import_parcel imp,
												prop_tax_district imp_td,
												(	select		p.parcel_id parcel_id,
																td.county_id county_id,
																td.is_parcel_one_to_one is_parcel_one_to_one
													from		pt_import_parcel imp,
																pt_parcel p,
																prop_tax_district td
													where	imp.import_run_id = :a_run_id
													and		imp.parcel_id = p.parcel_id
													and		p.tax_district_id = td.tax_district_id
												) p_view
									where	imp.import_run_id = :a_run_id
									and		imp.tax_district_id = imp_td.tax_district_id (+)
									and		imp.parcel_id = p_view.parcel_id (+)	
									and		( lower( trim( imp.parcel_number ) ), imp.state_id, decode( nvl( p_view.is_parcel_one_to_one, nvl( imp.is_parcel_one_to_one, 0 ) ), 0, nvl( imp_td.county_id, p_view.county_id ), nvl( imp.county_id, p_view.county_id ) ), imp.prop_tax_company_id ) in
												(	select		lower( trim( imp.parcel_number ) ), 
																imp.state_id, 
																decode( nvl( p_view.is_parcel_one_to_one, nvl( imp.is_parcel_one_to_one, 0 ) ), 0, nvl( imp_td.county_id, p_view.county_id ), nvl( imp.county_id, p_view.county_id ) ),
																imp.prop_tax_company_id
													from		pt_import_parcel imp,
																prop_tax_district imp_td,
																(	select		p.parcel_id parcel_id,
																				td.county_id county_id,
																				td.is_parcel_one_to_one is_parcel_one_to_one
																	from		pt_import_parcel imp,
																				pt_parcel p,
																				prop_tax_district td
																	where	imp.import_run_id = :a_run_id
																	and		imp.parcel_id = p.parcel_id
																	and		p.tax_district_id = td.tax_district_id
																) p_view
													where	imp.import_run_id = :a_run_id
													and		imp.tax_district_id = imp_td.tax_district_id (+)
													and		imp.parcel_id = p_view.parcel_id (+)
													having	count('x') > 1
													group by	lower( trim( imp.parcel_number ) ), 
																imp.state_id, 
																decode( nvl( p_view.is_parcel_one_to_one, nvl( imp.is_parcel_one_to_one, 0 ) ), 0, nvl( imp_td.county_id, p_view.county_id ), nvl( imp.county_id, p_view.county_id ) ),
																imp.prop_tax_company_id
												)
								);
	
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate import rows on parcel number, state, county, and prop tax company.  if you continue to receive this error contact your internal powerplant support " + &
							"or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if					
			end choose
			
		////
		////	key column.  (no need to do parcel number here as we've already done it above.)
		////	we can check using parcel_id, since we've already translated using the key/constraining columns.
		////
			if do_updates then
				if updt_key_col <> "parcel_number" then
					update	pt_import_parcel
					set			error_message = substr( trim( nvl( error_message, '' ) || decode( trim( error_message ), null, '', '', '', '  /  ' ) || 'this parcel key column (' || :updt_key_col || ') is duplicated in the import file.' ), 1, 4000 )
					where	import_run_id = :a_run_id
						and	parcel_id is not null
						and	parcel_id in
									(	select		parcel_id
										from		pt_import_parcel
										where	import_run_id = :a_run_id
										and		parcel_id is not null
										having	count('x') > 1
										group by	parcel_id
									);
					
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for duplicate import rows (on key column).  if you continue to receive this error contact your internal powerplant support " + &
							"or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				end if
			end if		

////
////	make sure this parcel isn't a -100 parcel type as those are special and can only be changed by special processing.
////
		if do_updates then
			update	pt_import_parcel
			set			error_message = substr( trim( nvl( error_message, '' ) || decode( trim( error_message ), null, '', '', '', '  /  ' ) || 'this parcel is a special system-only parcel used to track allocated locations and cannot be modified.' ), 1, 4000 )
			where	import_run_id = :a_run_id
				and	parcel_id is not null
				and	parcel_id in
							(	select		parcel_id
								from		pt_parcel
								where	parcel_type_id = :i_reserved_ptype_allocated
							);
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while checking for updates to allocated parcels.  if you continue to receive this error contact your internal powerplant support or the " + &
					"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
		update	pt_import_parcel imp
		set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
					'the allocated property parcel type is reserved for system-only special processing and cannot be used for a new parcel.' ), 1, 4000 )
		where	imp.import_run_id = :a_run_id
		and		imp.parcel_type_id = :i_reserved_ptype_allocated;
				
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while checking for allocated parcels being added.  if you continue to receive this error contact your internal powerplant support or the " + &
				"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	validate the flex fields for this parcel (based on the flex field set up for this parcel type).  get the distinct parcel types in the import table and loop through them.
////
		sqls =		"	select		distinct parcel_type_id " + "~r~n" + &
					"	from		pt_import_parcel " + "~r~n" + &
					"	where	import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
					"	and		parcel_type_id is not null "
		
		ds_ptype = create uo_ds_top
		rtn_str = f_create_dynamic_ds( ds_ptype, "grid", sqls, sqlca, true )
		
		if rtn_str <> "ok" then
			a_msg = "an error occurred while retrieving the distinct parcel types being imported.  if you continue to receive this error contact your internal powerplant support or the " + &
				"powerplan property tax support desk."
			destroy ds_ptype
			return false
		end if
		
		maxi = ds_ptype.rowcount()
		for i = 1 to maxi
			ptype_id = ds_ptype.getitemnumber( i, 1 )
			
			sqls2 =	"	select		ffc.flex_field_id flex_field_id, " + "~r~n" + &
						"				ffc.flex_field_label flex_field_label, " + "~r~n" + &
						"				ffc.flex_usage_id flex_usage_id, " + "~r~n" + &
						"				nvl( ffc.validate_value, 0 ) validate_value " + "~r~n" + &
						"	from		pt_parcel_flex_field_control ffc " + "~r~n" + &
						"	where	ffc.parcel_type_id = " + string( ptype_id ) + " " + "~r~n" + &
						"		and	( ffc.flex_usage_id = 2 or ffc.validate_value = 1 ) "
			
			ds_ff = create uo_ds_top
			rtn_str = f_create_dynamic_ds( ds_ff, "grid", sqls2, sqlca, true )
			
			if rtn_str <> "ok" then
				a_msg = "an error occurred while retrieving the data used to validate the flex fields for parcel type id = " + string( ptype_id ) + ".  if you continue to receive this error contact your internal powerplant support or the " + &
					"powerplan property tax support desk."
					destroy ds_ff
				return false
			end if
			
			maxj = ds_ff.rowcount()
			for j = 1 to maxj
				//get the data
				ff_id = ds_ff.getitemnumber( j, 1 )
				ff_label = ds_ff.getitemstring( j, 2 )
				if isnull( ff_label ) then ff_label = "<no label on flex field>"
				use_id = ds_ff.getitemnumber( j, 3 )
				validate_yn = ds_ff.getitemnumber( j, 4 )
				
				//see if this flex field is in the template.  if not, and it's usage id is not required, continue.
				if not temp_has_flex[ff_id] and use_id <> i_flex_usage_required then continue
				
				//if the field is required:
				//- if the field is in the template, all rows should have it populated.
				//- if the field is not in the template, put an error on all new records; we'll assume the existing parcels have it populated since they were able to be created.
				if use_id = i_flex_usage_required then
					if temp_has_flex[ff_id] then
						update_sqls =	"update	pt_import_parcel imp " + "~r~n" + &
											"set		imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
											"			'" + ff_label + " (flex field " + string( ff_id ) + ") is required for this parcel type.' ), 1, 4000 ) " + "~r~n" + &
											"where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
											"and		imp.flex_" + string( ff_id ) + " is null " + "~r~n" + &
											"and 		imp.parcel_type_id = " + string( ptype_id ) + " "
					else
						update_sqls =	"update	pt_import_parcel imp " + "~r~n" + &
											"set		imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
											"			'" + ff_label + " (flex field " + string( ff_id ) + ") is required for this parcel type but is not included in the template.' ), 1, 4000 ) " + "~r~n" + &
											"where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
											"and		imp.parcel_type_id = " + string( ptype_id ) + " "
					end if
										
					execute immediate :update_sqls;
							
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for required flex field " + string( ff_id ) + ".  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				end if
				
				//if the field is marked to be validated, make sure it has a valid value.
				if validate_yn = 1 then
					update_sqls =	"update	pt_import_parcel imp " + "~r~n" + &
										"set		imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
										"			'" + ff_label + " (flex field " + string( ff_id ) + ") is not a valid value for this parcel type.' ), 1, 4000 ) " + "~r~n" + &
										"where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
										"and		imp.flex_" + string( ff_id ) + " is not null " + "~r~n" + &
										"and 		imp.parcel_type_id = " + string( ptype_id ) + " " + &
										"and		lower( trim( imp.flex_" + string( ff_id ) + " ) ) not in " + "~r~n" + &
										"			(	select		lower( trim( value ) )  " + "~r~n" + &
										"				from		pt_parcel_flex_field_values " + "~r~n" + &
										"				where	parcel_type_id = " + string( ptype_id ) + " " + "~r~n" + &
										"				and		flex_field_id = " + string( ff_id ) + " " + "~r~n" + &
										"			) "
										
					execute immediate :update_sqls;
							
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for valid values in flex field " + string( ff_id ) + ".  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				end if
			next //flex field loop
		next //parcel type loop

		destroy ds_ff
		destroy ds_ptype

////
////	make sure the state on the parcel and state on the tax district match, if we're not creating a 1-to-1 tax district.
////
		update	pt_import_parcel imp
		set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) ||
					'the state on the parcel does not match the state on the tax district.' ), 1, 4000 )
		where	imp.import_run_id = :a_run_id
		and		imp.tax_district_id is not null
		and		nvl( imp.is_parcel_one_to_one, 0 ) = 0
		and		imp.state_id not in
					(	select		state_id
						from		prop_tax_district
						where	tax_district_id = imp.tax_district_id
					);
				
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while checking for state mismatches between parcel and tax district.  if you continue to receive this error contact your internal powerplant support or the " + &
				"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if

////
////	the validation completed successfully, so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_parcelvalidateeditfromimport ( a_run_id,  a_template_id, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelvalidateeditfromimport()
 **	
 **	validates all of the parcel fields for parcels being updated from the import tool.  this function will also validate tax district details if the parcel is one-to-one with tax districts.
 **	
 **	parameters	:		:	(a_run_id) the import run to validate.
 **							:	(a_template_id) the import template for this run.
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	validation completed successfully (there could be error messages on the table, but the validation process completed successfully).
 **											false	:	an error occurred while validating.
 **	
 ************************************************************************************************************************************************************/

/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the of_parcelvalidate() function as both functions are used to validate parcels being edited.
 **
 ************************************************************************************************************************************************************/

////
////	maint history for object changes
////
////	30996		qa discovered that if trying to import parcels that link to valid values, then the update statement checking for this fails if that parcel type has a flex field with no label
////				it tries to build out the sql in a string and fails because the label variable is null in this case.  changed to make label = "<no label on flex field>" if null

/*
 *	processing.
 */
		i, maxi, j, maxj
		ilook_id, ptype_id, ff_id, use_id, validate_yn, flex_idx
	string		sqls, sqls2, rtn_str, sysval, ff_label, update_sqls, ilook_col
	string		flds_type[]
	boolean	temp_has_cty, temp_has_auth_yn, temp_has_td, temp_has_ptco, temp_has_desc, temp_has_pnum, temp_has_ptype, temp_has_st
	boolean	temp_has_flex[]

/*
 *	misc objects.
 */
	uo_ds_top	ds_ptype
	uo_ds_top	ds_ff
	nvo_ppbase_import_templates uo_templates
	s_ppbase_import_template_fields	s_flds[]
	
////
////	for this import type (update : parcels), the "parcel" field is required and thus has already been translated.  so we don't need to handle it
////	like we do in the of_parcelvalidateaddfromimport function (where some records may exist and some may be new).  all records here
////	are existing parcels.
////

////
////	get the fields for this run.
////
		uo_templates = create nvo_ppbase_import_templates
		
		if not uo_templates.of_importtemplategetfields( a_template_id, false, true, s_flds[], flds_type[], a_msg ) then
			destroy uo_templates
			return false
		end if
		
////
////	get the lookup used for parcel_id.
////
		maxj = upperbound( s_flds[] )
		
		for j = 1 to maxj
			if s_flds[j].column_name = "parcel_id" then
				ilook_id = s_flds[j].import_lookup_id
				ilook_col = uo_templates.of_importlookupgetlookupcolumn( ilook_id )
				exit
			end if			
		next
		
		destroy uo_templates
		
////
////	see if any non-nullable fields are included in the template.
////	if so, we'll need to check later to make sure they're not null.
////	also, see what flex fields are in the template, as we may need to validate some of those.
////
		temp_has_cty = false
		temp_has_auth_yn = false
		temp_has_td = false
		temp_has_ptco = false
		temp_has_desc = false
		temp_has_pnum = false
		temp_has_ptype = false
		temp_has_st = false
		
		for j = 1 to 50
			temp_has_flex[j] = false
		next
		
		for j = 1 to maxj
			if s_flds[j].column_name = "county_id" then temp_has_cty = true
			if s_flds[j].column_name = "use_composite_authority_yn" then temp_has_auth_yn = true
			if s_flds[j].column_name = "tax_district_id" then temp_has_td = true
			if s_flds[j].column_name = "prop_tax_company_id" then temp_has_ptco = true
			if s_flds[j].column_name = "description" then temp_has_desc = true
			if s_flds[j].column_name = "parcel_number" then temp_has_pnum = true
			if s_flds[j].column_name = "parcel_type_id" then temp_has_ptype = true
			if s_flds[j].column_name = "state_id" then temp_has_st = true
			
			if mid( s_flds[j].column_name, 1, 4 ) = "flex" then
				flex_idx = long( trim( mid( s_flds[j].column_name, 6 ) ) )
				temp_has_flex[flex_idx] = true
			end if
		next
		
////
////	validate.
////
		
////
////	check to make sure that no required fields are being nulled out.
////	only check if the field is included in the template; otherwise, it won't get updated.
////
		if temp_has_cty then
			//county is required for 1-to-1 parcels.  since the 1-to-1 indicator can't be changed on an existing parcel, look at the indicator on the currently assigned tax district.
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'county is a required field for parcels with one-to-one tax districts.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	imp.county_id is null
				and	exists
						(	select		'x'
							from		pt_parcel pr,
										prop_tax_district td
							where	pr.tax_district_id = td.tax_district_id
							and		pr.parcel_id = imp.parcel_id
							and		td.is_parcel_one_to_one = 1
						);
						
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that 'county' is populated for all existing 1-to-1 parcels.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
		if temp_has_auth_yn then
			//use composite authority yn is required for 1-to-1 parcels.  since the 1-to-1 indicator can't be changed on an existing parcel, look at the indicator on the currently assigned tax district.
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'use composite authorities is a required field for parcels with one-to-one tax districts.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	imp.use_composite_authority_yn is null
				and	exists
						(	select		'x'
							from		pt_parcel pr,
										prop_tax_district td
							where	pr.tax_district_id = td.tax_district_id
							and		pr.parcel_id = imp.parcel_id
							and		td.is_parcel_one_to_one = 1
						);
						
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that 'use composite authorities' is populated for all existing 1-to-1 parcels.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
		if temp_has_td then
			//tax district is required for all non-1-to-1 parcels.
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'tax district is a required field for all parcels without a one-to-one district.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	imp.tax_district_id is null
				and	exists
						(	select		'x'
							from		pt_parcel p,
										prop_tax_district td
							where	p.parcel_id = imp.parcel_id
							and		p.tax_district_id = td.tax_district_id
							and		td.is_parcel_one_to_one = 0
						);
						
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that 'tax district' is populated for all existing parcels without a one-to-one district.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
		if temp_has_ptco then
			//prop tax company is required for all parcels.
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'prop tax company is a required field for parcels.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	imp.prop_tax_company_id is null;
						
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that 'prop tax company' is populated for all existing parcels.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
		if temp_has_desc then
			//description is required for all parcels.
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'description is a required field for parcels.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	imp.description is null;
						
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that 'description' is populated for all existing parcels.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
		if temp_has_pnum then
			//parcel number is required for all parcels.
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'parcel number is a required field for parcels.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	( trim( imp.parcel_number ) is null or trim( imp.parcel_number ) = '' );
						
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that 'parcel number' is populated for all existing parcels.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
		if temp_has_ptype then
			//parcel type is required for all parcels.
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'parcel type is a required field for parcels.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	imp.parcel_type_id is null;
						
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that 'parcel type' is populated for all existing parcels.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
		if temp_has_st then
			//state is required for all parcels.
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'state is a required field for parcels.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	imp.state_id is null;
						
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that 'state' is populated for all existing parcels.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
////
////	we can't change the "tax_district_id" field on a 1-to-1 parcel after it's been created.  check to make sure they aren't doing that.
////
		if temp_has_td then
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'the tax district field cannot be changed on an existing 1-to-1 parcel.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	imp.tax_district_id is not null
				and	exists
						(	select		'x'
							from		pt_parcel pr,
										prop_tax_district td
							where	pr.tax_district_id = td.tax_district_id
							and		pr.parcel_id = imp.parcel_id
							and		pr.tax_district_id <> imp.tax_district_id
							and		td.is_parcel_one_to_one = 1
						);
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that the 'tax district' field is not being changed for any existing 1-to-1 parcels.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
////
////	if we're updating, there are certain fields that can't be changed once a parcel has been created.  check for any violations.
////
////	- state - cannot change if the parcel is assigned to any bills or ledger items.
////	- prop tax company - cannot change if the parcel is assigned to any locations, bills, or ledger items.
////	- parcel type - cannot change if the parcel is assigned to any locations.
////
		if temp_has_st then
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'the state field cannot be changed on an existing parcel if it is assigned to any bills or ledger items.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	imp.state_id is not null
				and	exists
						(	select		'x'
							from		pt_parcel pr
							where	pr.parcel_id = imp.parcel_id
							and		pr.state_id <> imp.state_id
						)
				and	(		exists
								(	select		'x'
									from		pt_statement_line sl
									where	sl.parcel_id = imp.parcel_id
								)
							or	exists
								(	select		'x'
									from		pt_ledger ldg
									where	ldg.parcel_id = imp.parcel_id
								)
						);
								
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that the 'state' field is not being changed for any existing parcels with bills or ledger items.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
		if temp_has_ptco then
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'the prop tax company field cannot be changed on an existing parcel if it is assigned to any locations, bills, or ledger items.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	imp.prop_tax_company_id is not null
				and	exists
						(	select		'x'
							from		pt_parcel pr
							where	pr.parcel_id = imp.parcel_id
							and		pr.prop_tax_company_id <> imp.prop_tax_company_id
						)
				and	(		exists
								(	select		'x'
									from		pt_statement_line sl
									where	sl.parcel_id = imp.parcel_id
								)
							or	exists
								(	select		'x'
									from		pt_ledger ldg
									where	ldg.parcel_id = imp.parcel_id
								)
							or	exists
								(	select		'x'
									from		pt_parcel_location ploc
									where	ploc.parcel_id = imp.parcel_id
								)
						);
							
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that the 'prop tax company' field is not being changed for any existing parcels with locations, bills, or ledger items.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
		if temp_has_ptype then
			update	pt_import_parcel imp
			set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
							'the parcel type field cannot be changed on an existing parcel if it is assigned to any locations.' ), 1, 4000 )
			where	imp.import_run_id = :a_run_id
				and	imp.parcel_type_id is not null
				and	exists
						(	select		'x'
							from		pt_parcel pr
							where	pr.parcel_id = imp.parcel_id
							and		pr.parcel_type_id <> imp.parcel_type_id
						)
				and	exists
						(	select		'x'
							from		pt_parcel_location ploc
							where	ploc.parcel_id = imp.parcel_id
						);
								
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while validating that the 'parcel type' field is not being changed for any existing parcels with locations.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if

////
////	make sure they're not changing the parcel number and making it a duplicate.
////	get the system control to see what determines a unique parcel.
////	we only need to do this if the lookup column is not parcel_number - if it is, then we translated to parcel_id based on parcel number and thus aren't updating it.
////
		sysval = upper( trim( i_uo_systemoptions.of_getsystemoption( "parcels - parcel uniqueness columns" ) ) )
	
		if ilook_col <> "parcel_number" and temp_has_pnum then
			sqls = ""
			sqls2 = ""
			
			choose case sysval
				case	"parcel number"
					sqls =					"	update	pt_import_parcel imp " + "~r~n" + &
											"	set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
											"				'this parcel number already exists.' ), 1, 4000 ) " + "~r~n" + &
											"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
											"		and	trim( imp.parcel_number ) is not null " + "~r~n" + &
											"		and	exists " + "~r~n" + &
											"					(	select		'x' " + "~r~n" + &
											"						from		pt_parcel pr " + "~r~n" + &
											"						where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) ) " + "~r~n" + &
											"						and		pr.parcel_id <> imp.parcel_id " + "~r~n" + &
											"					) "
				case	"parcel number, state"
					//if the template includes the "state" field, use the state on the import table; otherwise, use the state on pt_parcel.
					sqls =					"	update	pt_import_parcel imp " + "~r~n" + &
											"	set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
											"				'this parcel number already exists for the given state.' ), 1, 4000 ) " + "~r~n" + &
											"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
											"		and	trim( imp.parcel_number ) is not null " + "~r~n" + &
											"		and	exists " + "~r~n" + &
											"					(	select		'x' " + "~r~n" + &
											"						from		pt_parcel pr"
											
					if temp_has_st then
						sqls = sqls +	" " + "~r~n"
					else
						sqls = sqls +	", " + "~r~n" + &
											"									pt_parcel imp_pr " + "~r~n"
					end if

					sqls = sqls +		"						where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) ) " + "~r~n" + &
											"						and		pr.parcel_id <> imp.parcel_id " + "~r~n"
								
					if temp_has_st then
						sqls = sqls +	"						and		pr.state_id = imp.state_id " + "~r~n"
					else
						sqls = sqls +	"						and		imp_pr.parcel_id = imp.parcel_id " + "~r~n" + &
											"						and		pr.state_id = imp_pr.state_id " + "~r~n"
					end if
						
					sqls = sqls + 		"					) "
				case	"parcel number, state, county"
					//for parcels with a non 1-to-1 tax district.
					//if the template includes the "state" field, use the state on the import table; otherwise, use the state on pt_parcel.
					//if the template includes the "tax district" field, use the county on the tax district in the import table; otherwise, use the county on the tax district currently in pt_parcel.
					sqls =					"	update	pt_import_parcel imp " + "~r~n" + &
											"	set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
											"				'this parcel number already exists for the given state and county.' ), 1, 4000 ) " + "~r~n" + &
											"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
											"		and	trim( imp.parcel_number ) is not null " + "~r~n" + &
											"		and	exists " + "~r~n" + &
											"					(	select		'x' " + "~r~n" + &
											"						from		pt_parcel pr, " + "~r~n" + &
											"									prop_tax_district td, " + "~r~n" + &
											"									pt_parcel imp_pr, " + "~r~n" + &
											"									prop_tax_district imp_td " + "~r~n" + &
											"						where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) ) " + "~r~n" + &
											"						and		pr.parcel_id <> imp.parcel_id " + "~r~n" + & 
											"						and		pr.tax_district_id = td.tax_district_id " + "~r~n" + &
											"						and		imp_pr.parcel_id = imp.parcel_id " + "~r~n"
											
					if temp_has_td then
						sqls = sqls +	"						and		imp_td.tax_district_id = imp.tax_district_id " + "~r~n"
					else
						sqls = sqls +	"						and		imp_td.tax_district_id = imp_pr.tax_district_id " + "~r~n"
					end if
								
					if temp_has_st then
						sqls = sqls +	"						and		pr.state_id = imp.state_id " + "~r~n"
					else
						sqls = sqls +	"						and		pr.state_id = imp_pr.state_id " + "~r~n"
					end if
						
					sqls = sqls + 		"						and		td.county_id = imp_td.county_id " + "~r~n" + &
											"						and		imp_td.is_parcel_one_to_one = 0 " + "~r~n" + &
											"					) "
					
					//for parcels with a 1-to-1 tax district.
					//if the template includes the "state" field, use the state on the import table; otherwise, use the state on pt_parcel.
					//if the template includes the "county" field, use the county on the import table (it's required for 1-to-1 parcels); otherwise, use the county on the tax district currently in pt_parcel.  tax districts can't be changed on 1-to-1 parcels.
					sqls2 =				"	update	pt_import_parcel imp " + "~r~n" + &
											"	set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
											"				'this parcel number already exists for the given state and county.' ), 1, 4000 ) " + "~r~n" + &
											"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
											"		and	trim( imp.parcel_number ) is not null " + "~r~n" + &
											"		and	exists " + "~r~n" + &
											"					(	select		'x' " + "~r~n" + &
											"						from		pt_parcel pr, " + "~r~n" + &
											"									prop_tax_district td, " + "~r~n" + &
											"									pt_parcel imp_pr, " + "~r~n" + &
											"									prop_tax_district imp_td " + "~r~n" + &
											"						where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) ) " + "~r~n" + &
											"						and		pr.parcel_id <> imp.parcel_id " + "~r~n" + & 
											"						and		pr.tax_district_id = td.tax_district_id " + "~r~n" + &
											"						and		imp_pr.parcel_id = imp.parcel_id " + "~r~n" + &
											"						and		imp_pr.tax_district_id = imp_td.tax_district_id " + "~r~n"
								
					if temp_has_st then
						sqls2 = sqls2 +	"						and		pr.state_id = imp.state_id " + "~r~n"
					else
						sqls2 = sqls2 +	"						and		pr.state_id = imp_pr.state_id " + "~r~n"
					end if
					
					if temp_has_cty then
						sqls2 = sqls2 + "						and		td.county_id = imp.county_id " + "~r~n"
					else
						sqls2 = sqls2 + "						and		td.county_id = imp_td.county_id " + "~r~n"
					end if
						
					sqls2 = sqls2 + 		"						and		imp_td.is_parcel_one_to_one = 1 " + "~r~n" + &
											"					) "
				case	"parcel number, state, prop tax company"
					//if the template includes the "state" field, use the state on the import table; otherwise, use the state on pt_parcel.
					//if the template includes the "prop tax company" field, use the prop tax company on the import table; otherwise, use the prop tax company on pt_parcel.
					sqls =					"	update	pt_import_parcel imp " + "~r~n" + &
											"	set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
											"				'this parcel number already exists for the given state and prop tax company.' ), 1, 4000 ) " + "~r~n" + &
											"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
											"		and	trim( imp.parcel_number ) is not null " + "~r~n" + &
											"		and	exists " + "~r~n" + &
											"					(	select		'x' " + "~r~n" + &
											"						from		pt_parcel pr"
											
					if temp_has_st and temp_has_ptco then
						sqls = sqls +	" " + "~r~n"
					else
						sqls = sqls +	", " + "~r~n" + &
											"									pt_parcel imp_pr " + "~r~n"
					end if

					sqls = sqls +		"						where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) ) " + "~r~n" + &
											"						and		pr.parcel_id <> imp.parcel_id " + "~r~n"
											
					if not temp_has_st or not temp_has_ptco then
						sqls = sqls +	"						and		imp_pr.parcel_id = imp.parcel_id " + "~r~n"
					end if
								
					if temp_has_st then
						sqls = sqls +	"						and		pr.state_id = imp.state_id " + "~r~n"
					else
						sqls = sqls +	"						and		pr.state_id = imp_pr.state_id " + "~r~n"
					end if
					
					if temp_has_ptco then
						sqls = sqls +	"						and		pr.prop_tax_company_id = imp.prop_tax_company_id " + "~r~n"
					else
						sqls = sqls +	"						and		pr.prop_tax_company_id = imp_pr.prop_tax_company_id " + "~r~n"
					end if
						
					sqls = sqls + 		"					) "
				case	"parcel number, state, county, prop tax company"
					//for parcels with a non 1-to-1 tax district.
					//if the template includes the "state" field, use the state on the import table; otherwise, use the state on pt_parcel.
					//if the template includes the "tax district" field, use the county on the tax district in the import table; otherwise, use the county on the tax district currently in pt_parcel.
					//if the template includes the "prop tax company" field, use the prop tax company on the import table; otherwise, use the prop tax company on pt_parcel.
					sqls =					"	update	pt_import_parcel imp " + "~r~n" + &
											"	set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
											"				'this parcel number already exists for the given state, county, and prop tax company.' ), 1, 4000 ) " + "~r~n" + &
											"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
											"		and	trim( imp.parcel_number ) is not null " + "~r~n" + &
											"		and	exists " + "~r~n" + &
											"					(	select		'x' " + "~r~n" + &
											"						from		pt_parcel pr, " + "~r~n" + &
											"									prop_tax_district td, " + "~r~n" + &
											"									pt_parcel imp_pr, " + "~r~n" + &
											"									prop_tax_district imp_td " + "~r~n" + &
											"						where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) ) " + "~r~n" + &
											"						and		pr.parcel_id <> imp.parcel_id " + "~r~n" + & 
											"						and		pr.tax_district_id = td.tax_district_id " + "~r~n" + &
											"						and		imp_pr.parcel_id = imp.parcel_id " + "~r~n"
											
					if temp_has_td then
						sqls = sqls +	"						and		imp_td.tax_district_id = imp.tax_district_id " + "~r~n"
					else
						sqls = sqls +	"						and		imp_td.tax_district_id = imp_pr.tax_district_id " + "~r~n"
					end if
								
					if temp_has_st then
						sqls = sqls +	"						and		pr.state_id = imp.state_id " + "~r~n"
					else
						sqls = sqls +	"						and		pr.state_id = imp_pr.state_id " + "~r~n"
					end if
					
					if temp_has_ptco then
						sqls = sqls +	"						and		pr.prop_tax_company_id = imp.prop_tax_company_id " + "~r~n"
					else
						sqls = sqls +	"						and		pr.prop_tax_company_id = imp_pr.prop_tax_company_id " + "~r~n"
					end if
						
					sqls = sqls + 		"						and		td.county_id = imp_td.county_id " + "~r~n" + &
											"						and		imp_td.is_parcel_one_to_one = 0 " + "~r~n" + &
											"					) "
					
					//for parcels with a 1-to-1 tax district.
					//if the template includes the "state" field, use the state on the import table; otherwise, use the state on pt_parcel.
					//if the template includes the "county" field, use the county on the import table (it's required for 1-to-1 parcels); otherwise, use the county on the tax district currently in pt_parcel.  tax districts can't be changed on 1-to-1 parcels.
					//if the template includes the "prop tax company" field, use the prop tax company on the import table; otherwise, use the prop tax company on pt_parcel.
					sqls2 =				"	update	pt_import_parcel imp " + "~r~n" + &
											"	set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
											"				'this parcel number already exists for the given state, county, and prop tax company.' ), 1, 4000 ) " + "~r~n" + &
											"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
											"		and	trim( imp.parcel_number ) is not null " + "~r~n" + &
											"		and	exists " + "~r~n" + &
											"					(	select		'x' " + "~r~n" + &
											"						from		pt_parcel pr, " + "~r~n" + &
											"									prop_tax_district td, " + "~r~n" + &
											"									pt_parcel imp_pr, " + "~r~n" + &
											"									prop_tax_district imp_td " + "~r~n" + &
											"						where	lower( trim( pr.parcel_number ) ) = lower( trim( imp.parcel_number ) ) " + "~r~n" + &
											"						and		pr.parcel_id <> imp.parcel_id " + "~r~n" + & 
											"						and		pr.tax_district_id = td.tax_district_id " + "~r~n" + &
											"						and		imp_pr.parcel_id = imp.parcel_id " + "~r~n" + &
											"						and		imp_pr.tax_district_id = imp_td.tax_district_id " + "~r~n"
								
					if temp_has_st then
						sqls2 = sqls2 +	"						and		pr.state_id = imp.state_id " + "~r~n"
					else
						sqls2 = sqls2 +	"						and		pr.state_id = imp_pr.state_id " + "~r~n"
					end if
					
					if temp_has_cty then
						sqls2 = sqls2 +	"						and		td.county_id = imp.county_id " + "~r~n"
					else
						sqls2 = sqls2+ 	"						and		td.county_id = imp_td.county_id " + "~r~n"
					end if
					
					if temp_has_ptco then
						sqls2 = sqls2 +	"						and		pr.prop_tax_company_id = imp.prop_tax_company_id " + "~r~n"
					else
						sqls2 = sqls2 +	"						and		pr.prop_tax_company_id = imp_pr.prop_tax_company_id " + "~r~n"
					end if
						
					sqls2 = sqls2 + 	"						and		imp_td.is_parcel_one_to_one = 1 " + "~r~n" + &
											"					) "
				case	else
					a_msg = "unable to determine whether this is a unique parcel.  please check the system options for the property tax center (admin center, system options) to ensure that a " + &
						"system control named 'parcels - parcel uniqueness columns' is available and that values are set for that option.  if the system control is not present or there is another " + &
						"problem with the system option contact the powerplan property tax support desk for assistance."
				return false
			end choose
			
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while checking for duplicate entries by parcel number (uniqueness by '" + sysval + "').  if you continue to receive this error contact your internal powerplant support or the " + &
					"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
			
			if sqls2 <> "" then
				execute immediate :sqls2;
			
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while checking for duplicate entries by parcel number (uniqueness by '" + sysval + "', 1-to-1 district).  if you continue to receive this error contact your internal powerplant support or the " + &
						"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if
		end if
		
////
////	check to see if the import contains two identical rows and flag them.  check by parcel_id, since we already have that translated.
////	then make sure there are no duplicate parcel numbers based on the system option (only if our lookup column wasn't parcel number and parcel number is included in the template).
////
		////
		////	parcel id.
		////
				update	pt_import_parcel
				set			error_message = substr( trim( nvl( error_message, '' ) || decode( trim( error_message ), null, '', '', '', '  /  ' ) || 'this parcel is duplicated in the import file.' ), 1, 4000 )
				where	import_run_id = :a_run_id
					and	parcel_id in
								(	select		parcel_id
									from		pt_import_parcel
									where	import_run_id = :a_run_id
									having	count('x') > 1
									group by	parcel_id
								);
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while checking for duplicate import rows on parcel.  if you continue to receive this error contact your internal powerplant support " + &
						"or the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
				
		////
		////	parcel number.
		////
			if ilook_col <> "parcel_number" and temp_has_pnum then
				sqls = ""
				
				choose case sysval
					case	"parcel number"
						sqls =					"	update	pt_import_parcel imp " + "~r~n" + &
												"	set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
												"				'this parcel number is duplicated in the import file.' ), 1, 4000 ) " + "~r~n" + &
												"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
												"		and	trim( imp.parcel_number ) is not null " + "~r~n" + &
												"		and	lower( trim( parcel_number ) ) in " + "~r~n" + &
												"					(	select		lower( trim( impin.parcel_number ) ) " + "~r~n" + &
												"						from		pt_import_parcel impin " + "~r~n" + &
												"						where	impin.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
												"						having	count('x') > 1 " + "~r~n" + &
												"						group by	lower( trim( impin.parcel_number ) ) " + "~r~n" + &
												"					) "
					case	"parcel number, state"
						//if the template includes the "state" field, use the state on the import table; otherwise, use the state on pt_parcel.
						sqls =					"	update	pt_import_parcel imp " + "~r~n" + &
												"	set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
												"				'this parcel number is duplicated in the import file for the given state.' ), 1, 4000 ) " + "~r~n" + &
												"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
												"		and	trim( imp.parcel_number ) is not null " + "~r~n" + &
												"		and	imp.line_id in " + "~r~n" + &
												"				(	select		impin.line_id " + "~r~n" + &
												"					from		pt_import_parcel impin, " + "~r~n" + &
												"								pt_parcel pr " + "~r~n" + &
												"					where	impin.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
												"					and		impin.parcel_id = pr.parcel_id " + "~r~n" + &
												"					and		( lower( trim( impin.parcel_number ) ), nvl( impin.state_id, pr.state_id ) ) in " + "~r~n" + &
												"								(	select		lower( trim( impin.parcel_number ) ), nvl( impin.state_id, pr.state_id ) " + "~r~n" + &
												"									from		pt_import_parcel impin, " + "~r~n" + &
												"												pt_parcel pr " + "~r~n" + &
												"									where	impin.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
												"									and		impin.parcel_id = pr.parcel_id " + "~r~n" + &
												"									having	count('x') > 1 " + "~r~n" + &
												"									group by	lower( trim( impin.parcel_number ) ), nvl( impin.state_id, pr.state_id ) " + "~r~n" + &
												"								) " + "~r~n" + &
												"				) "
					case	"parcel number, state, county"
						//if the template includes the "state" field, use the state on the import table; otherwise, use the state on pt_parcel.
						//county may come from several different places.  we have to check for duplicates all in one update statement; otherwise, we may miss some.  
						//check the following places for county in this order:
						//- for non 1-to-1 parcels, use the county_id on the tax_district_id from the import file if it's populated.  otherwise, use the county_id on the parcel's assigned tax district.
						//- for 1-to-1 parcels, use county_id from the import file if it's populated.  otherwise, use the county_id on the parcel's assigned tax district.
						// kamal, 20190712. removed the extra bracket on lines 750 and 762 (decode statements). fixed this as part of 2018.2.1.1 upgrade.
						sqls =					"	update	pt_import_parcel imp " + "~r~n" + &
												"	set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 'this parcel number is duplicated in the import file for the given state and county.' ), 1, 4000 ) " + "~r~n" + &
												"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
												"	and		imp.line_id in " + "~r~n" + &
												"				(	select		imp.line_id " + "~r~n" + &
												"					from		pt_import_parcel imp, " + "~r~n" + &
												"								pt_parcel p, " + "~r~n" + &
												"								prop_tax_district td, " + "~r~n" + &
												"								prop_tax_district imp_td " + "~r~n" + &
												"					where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
												"					and		imp.parcel_id = p.parcel_id " + "~r~n" + &
												"					and		p.tax_district_id = td.tax_district_id " + "~r~n" + &
												"					and		imp.tax_district_id = imp_td.tax_district_id (+) " + "~r~n" + &
												"					and		( lower( trim( imp.parcel_number ) ), nvl( imp.state_id, p.state_id ), decode( td.is_parcel_one_to_one, 0, nvl( imp_td.county_id, td.county_id ), nvl( imp.county_id, td.county_id ) ) ) in " + "~r~n" + &
												"								(	select		lower( trim( imp.parcel_number ) ),  " + "~r~n" + &
												"												nvl( imp.state_id, p.state_id ),  " + "~r~n" + &
												"												decode( td.is_parcel_one_to_one, 0, nvl( imp_td.county_id, td.county_id ), nvl( imp.county_id, td.county_id ) ) " + "~r~n" + &
												"									from		pt_import_parcel imp, " + "~r~n" + &
												"												pt_parcel p, " + "~r~n" + &
												"												prop_tax_district td, " + "~r~n" + &
												"												prop_tax_district imp_td " + "~r~n" + &
												"									where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
												"									and		imp.parcel_id = p.parcel_id " + "~r~n" + &
												"									and		p.tax_district_id = td.tax_district_id " + "~r~n" + &
												"									and		imp.tax_district_id = imp_td.tax_district_id (+) " + "~r~n" + &
												"									having	count('x') > 1 " + "~r~n" + &
												"									group by	lower( trim( imp.parcel_number ) ),  " + "~r~n" + &
												"												nvl( imp.state_id, p.state_id ),  " + "~r~n" + &
												"												decode( td.is_parcel_one_to_one, 0, nvl( imp_td.county_id, td.county_id ), nvl( imp.county_id, td.county_id ) ) " + "~r~n" + &
												"								) " + "~r~n" + &
												"				) "
					case	"parcel number, state, prop tax company"
						//if the template includes the "state" field, use the state on the import table; otherwise, use the state on pt_parcel.
						//if the template includes the "prop tax company" field, use the prop tax company on the import table; otherwise, use the prop tax company on pt_parcel.
						sqls =					"	update	pt_import_parcel imp " + "~r~n" + &
												"	set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
												"				'this parcel number is duplicated in the import file for the given state and prop tax company.' ), 1, 4000 ) " + "~r~n" + &
												"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
												"		and	trim( imp.parcel_number ) is not null " + "~r~n" + &
												"		and	imp.line_id in " + "~r~n" + &
												"				(	select		impin.line_id " + "~r~n" + &
												"					from		pt_import_parcel impin, " + "~r~n" + &
												"								pt_parcel pr " + "~r~n" + &
												"					where	impin.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
												"					and		impin.parcel_id = pr.parcel_id " + "~r~n" + &
												"					and		( lower( trim( impin.parcel_number ) ), nvl( impin.state_id, pr.state_id ), nvl( impin.prop_tax_company_id, pr.prop_tax_company_id ) ) in " + "~r~n" + &
												"								(	select		lower( trim( impin.parcel_number ) ), nvl( impin.state_id, pr.state_id ), nvl( impin.prop_tax_company_id, pr.prop_tax_company_id ) " + "~r~n" + &
												"									from		pt_import_parcel impin, " + "~r~n" + &
												"												pt_parcel pr " + "~r~n" + &
												"									where	impin.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
												"									and		impin.parcel_id = pr.parcel_id " + "~r~n" + &
												"									having	count('x') > 1 " + "~r~n" + &
												"									group by	lower( trim( impin.parcel_number ) ), nvl( impin.state_id, pr.state_id ), nvl( impin.prop_tax_company_id, pr.prop_tax_company_id ) " + "~r~n" + &
												"								) " + "~r~n" + &
												"				) "					
					case	"parcel number, state, county, prop tax company"
						//if the template includes the "state" field, use the state on the import table; otherwise, use the state on pt_parcel.
						//if the template includes the "prop tax company" field, use the prop tax company on the import table; otherwise, use the prop tax company on pt_parcel.
						//county may come from several different places.  we have to check for duplicates all in one update statement; otherwise, we may miss some.  
						//check the following places for county in this order:
						//- for non 1-to-1 parcels, use the county_id on the tax_district_id from the import file if it's populated.  otherwise, use the county_id on the parcel's assigned tax district.
						//- for 1-to-1 parcels, use county_id from the import file if it's populated.  otherwise, use the county_id on the parcel's assigned tax district.
						// kamal, 20190712. removed the extra bracket on lines 813 and 826 (decode statements). carried over the fix from previous version 10.4.1.2.
						sqls =					"	update	pt_import_parcel imp " + "~r~n" + &
												"	set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 'this parcel number is duplicated in the import file for the given state and county.' ), 1, 4000 ) " + "~r~n" + &
												"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
												"	and		imp.line_id in " + "~r~n" + &
												"				(	select		imp.line_id " + "~r~n" + &
												"					from		pt_import_parcel imp, " + "~r~n" + &
												"								pt_parcel p, " + "~r~n" + &
												"								prop_tax_district td, " + "~r~n" + &
												"								prop_tax_district imp_td " + "~r~n" + &
												"					where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
												"					and		imp.parcel_id = p.parcel_id " + "~r~n" + &
												"					and		p.tax_district_id = td.tax_district_id " + "~r~n" + &
												"					and		imp.tax_district_id = imp_td.tax_district_id (+) " + "~r~n" + &
												"					and		( lower( trim( imp.parcel_number ) ), nvl( imp.state_id, p.state_id ), decode( td.is_parcel_one_to_one, 0, nvl( imp_td.county_id, td.county_id ), nvl( imp.county_id, td.county_id ) ), nvl( imp.prop_tax_company_id, p.prop_tax_company_id ) ) in " + "~r~n" + &
												"								(	select		lower( trim( imp.parcel_number ) ),  " + "~r~n" + &
												"												nvl( imp.state_id, p.state_id ),  " + "~r~n" + &
												"												decode( td.is_parcel_one_to_one, 0, nvl( imp_td.county_id, td.county_id ), nvl( imp.county_id, td.county_id ) ), " + "~r~n" + &
												"												nvl( imp.prop_tax_company_id, p.prop_tax_company_id ) " + "~r~n" + &
												"									from		pt_import_parcel imp, " + "~r~n" + &
												"												pt_parcel p, " + "~r~n" + &
												"												prop_tax_district td, " + "~r~n" + &
												"												prop_tax_district imp_td " + "~r~n" + &
												"									where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
												"									and		imp.parcel_id = p.parcel_id " + "~r~n" + &
												"									and		p.tax_district_id = td.tax_district_id " + "~r~n" + &
												"									and		imp.tax_district_id = imp_td.tax_district_id (+) " + "~r~n" + &
												"									having	count('x') > 1 " + "~r~n" + &
												"									group by	lower( trim( imp.parcel_number ) ),  " + "~r~n" + &
												"												nvl( imp.state_id, p.state_id ),  " + "~r~n" + &
												"												decode( td.is_parcel_one_to_one, 0, nvl( imp_td.county_id, td.county_id ), nvl( imp.county_id, td.county_id ) ), " + "~r~n" + &
												"												nvl( imp.prop_tax_company_id, p.prop_tax_company_id ) " + "~r~n" + &
												"								) " + "~r~n" + &
												"				) "
					case	else
						a_msg = "unable to determine whether this is a unique parcel.  please check the system options for the property tax center (admin center, system options) to ensure that a " + &
							"system control named 'parcels - parcel uniqueness columns' is available and that values are set for that option.  if the system control is not present or there is another " + &
							"problem with the system option contact the powerplan property tax support desk for assistance."
					return false
				end choose
				
				execute immediate :sqls;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "a database error occurred while checking for duplicate import rows on parcel number (uniqueness by '" + sysval + "').  if you continue to receive this error contact your internal powerplant support or the " + &
						"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	make sure this parcel isn't a -100 parcel type as those are special and can only be changed by special processing.
////
		update	pt_import_parcel
		set			error_message = substr( trim( nvl( error_message, '' ) || decode( trim( error_message ), null, '', '', '', '  /  ' ) || 'this parcel is a special system-only parcel used to track allocated locations and cannot be modified.' ), 1, 4000 )
		where	import_run_id = :a_run_id
			and	parcel_id is not null
			and	parcel_id in
						(	select		parcel_id
							from		pt_parcel
							where	parcel_type_id = :i_reserved_ptype_allocated
						);
		
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while checking for updates to allocated parcels.  if you continue to receive this error contact your internal powerplant support or the " + &
				"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	validate the flex fields for this parcel (based on the flex field set up for this parcel type).  get the distinct parcel types (from the import table if the field is in the template; otherwise from the parcel) and loop through them.
////	only check for flex fields included in the template.
////
		if temp_has_ptype then
			sqls =			"	select		distinct imp.parcel_type_id " + "~r~n" + &
							"	from		pt_import_parcel imp " + "~r~n" + &
							"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
							"	and		imp.parcel_type_id is not null "
		else
			sqls =			"	select		distinct pr.parcel_type_id " + "~r~n" + &
							"	from		pt_import_parcel imp, " + "~r~n" + &
							"				pt_parcel pr " + "~r~n" + &
							"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
							"	and		imp.parcel_id = pr.parcel_id "
		end if
		
		ds_ptype = create uo_ds_top
		rtn_str = f_create_dynamic_ds( ds_ptype, "grid", sqls, sqlca, true )
		
		if rtn_str <> "ok" then
			a_msg = "an error occurred while retrieving the distinct parcel types being imported.  if you continue to receive this error contact your internal powerplant support or the " + &
				"powerplan property tax support desk."
			destroy ds_ptype
			return false
		end if
		
		maxi = ds_ptype.rowcount()
		for i = 1 to maxi
			ptype_id = ds_ptype.getitemnumber( i, 1 )
			
			sqls2 =	"	select		ffc.flex_field_id flex_field_id, " + "~r~n" + &
						"				ffc.flex_field_label flex_field_label, " + "~r~n" + &
						"				ffc.flex_usage_id flex_usage_id, " + "~r~n" + &
						"				nvl( ffc.validate_value, 0 ) validate_value " + "~r~n" + &
						"	from		pt_parcel_flex_field_control ffc " + "~r~n" + &
						"	where	ffc.parcel_type_id = " + string( ptype_id ) + " " + "~r~n" + &
						"		and	( ffc.flex_usage_id = 2 or ffc.validate_value = 1 ) "
			
			ds_ff = create uo_ds_top
			rtn_str = f_create_dynamic_ds( ds_ff, "grid", sqls2, sqlca, true )
			
			if rtn_str <> "ok" then
				a_msg = "an error occurred while retrieving the data used to validate the flex fields for parcel type id = " + string( ptype_id ) + ".  if you continue to receive this error contact your internal powerplant support or the " + &
					"powerplan property tax support desk."
					destroy ds_ff
				return false
			end if
			
			maxj = ds_ff.rowcount()
			for j = 1 to maxj
				//get the data
				ff_id = ds_ff.getitemnumber( j, 1 )
				ff_label = ds_ff.getitemstring( j, 2 )
				if isnull( ff_label ) then ff_label = "<no label on flex field>"
				use_id = ds_ff.getitemnumber( j, 3 )
				validate_yn = ds_ff.getitemnumber( j, 4 )
				
				//see if this flex field is in the template.  if not, continue.
				if not temp_has_flex[ff_id] then continue
				
				//if the field is required, check to make sure all rows have it populated.
				if use_id = i_flex_usage_required then
					update_sqls =							"update	pt_import_parcel imp " + "~r~n" + &
																"set		imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
																"			'" + ff_label + " (flex field " + string( ff_id ) + ") is required for this parcel type.' ), 1, 4000 ) " + "~r~n" + &
																"where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
																"and		imp.flex_" + string( ff_id ) + " is null " + "~r~n"
										
					if temp_has_ptype then
						update_sqls = update_sqls +	"and 		imp.parcel_type_id = " + string( ptype_id ) + " "
					else
						update_sqls = update_sqls +	"and 		exists " + "~r~n" + &
																"			(	select		'x' " + "~r~n" + &
																"				from		pt_parcel p " + "~r~n" + &
																"				where	p.parcel_id = imp.parcel_id " + "~r~n" + &
																"				and		p.parcel_type_id = " + string( ptype_id ) + " " + "~r~n" + &
																"			) "
					end if
										
					execute immediate :update_sqls;
							
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for required flex field " + string( ff_id ) + ".  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				end if
				
				//if the field is marked to be validated, make sure it has a valid value.
				if validate_yn = 1 then
					update_sqls =							"update	pt_import_parcel imp " + "~r~n" + &
																"set		imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
																"			'" + ff_label + " (flex field " + string( ff_id ) + ") is not a valid value for this parcel type.' ), 1, 4000 ) " + "~r~n" + &
																"where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
																"and		imp.flex_" + string( ff_id ) + " is not null " + "~r~n" + &
																"and		lower( trim( imp.flex_" + string( ff_id ) + " ) ) not in " + "~r~n" + &
																"			(	select		lower( trim( value ) )  " + "~r~n" + &
																"				from		pt_parcel_flex_field_values " + "~r~n" + &
																"				where	parcel_type_id = " + string( ptype_id ) + " " + "~r~n" + &
																"				and		flex_field_id = " + string( ff_id ) + " " + "~r~n" + &
																"			) " + "~r~n"
										
					if temp_has_ptype then
						update_sqls = update_sqls +	"and 		imp.parcel_type_id = " + string( ptype_id ) + " "
					else
						update_sqls = update_sqls +	"and 		exists " + "~r~n" + &
																"			(	select		'x' " + "~r~n" + &
																"				from		pt_parcel p " + "~r~n" + &
																"				where	p.parcel_id = imp.parcel_id " + "~r~n" + &
																"				and		p.parcel_type_id = " + string( ptype_id ) + " " + "~r~n" + &
																"			) "
					end if
										
					execute immediate :update_sqls;
							
					if sqlca.sqlcode <> 0 then
						a_msg = "a database error occurred while checking for valid values in flex field " + string( ff_id ) + ".  if you continue to receive this error contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
						return false
					end if
				end if
			next //flex field loop
		next //parcel type loop

		destroy ds_ff
		destroy ds_ptype

////
////	make sure the state on the parcel and state on the tax district match, if we're not updating a 1-to-1 tax district (if we are updating a 1-to-1 district, its state will get updated as well as the parcel's state).
////	if the template has state, compare to that state; otherwise, compare to the state currently on the parcel.
////	if the template has tax district, compare to that tax district; otherwise, compare to the tax district currently on the parcel.
////	we only need to do this check if the template has state or tax district; otherwise, nothing will be changing.
////
		if temp_has_td or temp_has_st then
			sqls =					"	update	pt_import_parcel imp " + "~r~n" + &
									"	set			imp.error_message = substr( trim( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
									"				'the state on the parcel does not match the state on the tax district.' ), 1, 4000 ) " + "~r~n" + &
									"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n"
						
			if temp_has_td then
				sqls = sqls +	"	and		imp.tax_district_id is not null " + "~r~n"
			end if
			
			if temp_has_st then
				sqls = sqls +	"	and		imp.state_id is not null " + "~r~n"
			end if
			
			sqls = sqls +		"	and		exists " + "~r~n" + &
									"	(	select		'x' " + "~r~n" + &
									"		from		pt_parcel p, " + "~r~n" + &
									"					prop_tax_district td " + "~r~n" + &
									"		where	p.parcel_id = imp.parcel_id " + "~r~n"
									
			if temp_has_td then
				sqls = sqls +	"		and		td.tax_district_id = imp.tax_district_id " + "~r~n"
			else
				sqls = sqls +	"		and		td.tax_district_id = p.tax_district_id " + "~r~n"
			end if
									
			if temp_has_st then
				sqls = sqls +	"		and		td.state_id <> imp.state_id " + "~r~n"
			else
				sqls = sqls +	"		and		td.state_id <> p.state_id " + "~r~n"
			end if
									
			sqls = sqls +		"		and		td.is_parcel_one_to_one = 0 " + "~r~n" + &
									"	) "
									
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while checking for state mismatches between parcel and tax district.  if you continue to receive this error contact your internal powerplant support or the " + &
					"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if

////
////	the validation completed successfully, so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_parceleditfromimport ( a_run_id,  a_template_id, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parceleditfromimport()
 **	
 **	edits parcels in the database from the import table.  this function will update a tax district if the parcel is one-to-one with a tax district (based on the current database value since this cannot be changed after a parcel
 **	has been added).  if a parcel is not one-to-one, any data in the district fields will be ignored.
 **	
 **	parameters	:		:	(a_run_id) the import run to use.
 **							:	(a_template_id) the import template to use.
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	table was edited successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.  in addtion, errors are written back to the import run table.
 **	
 ************************************************************************************************************************************************************/

/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the other of_parceledit() functions as all functions are used to edit parcels.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi, pr_index, ff_index, td_index
	string		sqls, sqls_update_ff, sqls_update_td
	string		flds_type[]
	boolean	update_pr, update_ff, update_td

/*
 *	misc objects.
 */
	nvo_ppbase_import_templates uo_templates
	s_ppbase_import_template_fields	s_flds[]
	
////
////	for this import type (update : parcels), the "parcel" field is required and thus has already been translated.  so we don't need to handle it
////	like we do in the of_parceladdfromimport function (where some records may exist and some may be new).  all records here
////	are existing parcels.
////
	
////
////	get the fields for this run.
////
		uo_templates = create nvo_ppbase_import_templates
		
		if not uo_templates.of_importtemplategetfields( a_template_id, false, true, s_flds[], flds_type[], a_msg ) then
			destroy uo_templates
			return false
		end if
		
		destroy uo_templates
		
////
////	if we're updatable mark those guys that have changed to speed things up (and ensure we don't update every row in the file if we don't have to).
////
////	for parcels, fields may be changed on these three tables:
////	- pt_parcel
////	- pt_parcel_flex_fields
////	- prop_tax_district (if a 1-to-1 district)
////
		update_pr = false
		update_ff = false
		update_td = false
		
		update	pt_import_parcel
		set			is_modified = 0
		where	import_run_id = :a_run_id;
		
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while resetting the modified flag (parcel).  if you continue to receive this error contact your internal powerplant support or the " + &
				"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
		sqls =						"	update	pt_import_parcel imp " + "~r~n" + &
									"	set			imp.is_modified = 1 " + "~r~n" + &
									"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
									"	and		imp.parcel_id is not null " + "~r~n" + &
									"	and		exists " + "~r~n" + &
									"					(	select		'x' " + "~r~n" + &
									"						from		pt_parcel pr, " + "~r~n" + &
									"									pt_parcel_flex_fields ff, " + "~r~n" + &
									"									prop_tax_district td " + "~r~n" + &
									"						where	pr.parcel_id = ff.parcel_id " + "~r~n" + &
									"						and		pr.tax_district_id = td.tax_district_id " + "~r~n" + &
									"						and		pr.parcel_id = imp.parcel_id " + "~r~n" + &
									"						and		(			"
		
		maxi = upperbound( s_flds[] )
		
		for i = 1 to maxi
			if i > 1 then
				sqls = sqls +	"										or		"
			end if
			
			//process based on the table where the column is.
			if left( s_flds[i].column_name, 4 ) = "flex" then
				update_ff = true
				
				if left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
					sqls = sqls + "nvl( trim( imp." + s_flds[i].column_name + " ), '-90909' ) <> nvl( trim( ff." + s_flds[i].column_name + " ), '-90909' ) " + "~r~n"
				elseif left( flds_type[i], 6 ) = "number" then
					sqls = sqls + "nvl( imp." + s_flds[i].column_name + ", -90909 ) <> nvl( ff." + s_flds[i].column_name + ", -90909 ) " + "~r~n"
				elseif left( flds_type[i], 4 ) = "date" then
					sqls = sqls + "nvl( to_date( imp." + s_flds[i].column_name + ", 'mm/dd/yyyy' ), to_date( '1900-01-01', 'yyyy-mm-dd' ) ) <> nvl( ff." + s_flds[i].column_name + ", to_date( '1900-01-01', 'yyyy-mm-dd' ) ) " + "~r~n"
				end if
			elseif s_flds[i].column_name = "assessor_id" or s_flds[i].column_name = "assignment_indicator" or s_flds[i].column_name = "county_id" or s_flds[i].column_name = "grid_coordinate" or s_flds[i].column_name = "is_parcel_one_to_one" or s_flds[i].column_name = "tax_district_code" or s_flds[i].column_name = "type_code_id" or s_flds[i].column_name = "use_composite_authority_yn" then
				update_td = true
				
				if left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
					sqls = sqls + "nvl( trim( imp." + s_flds[i].column_name + " ), '-90909' ) <> nvl( trim( td." + s_flds[i].column_name + " ), '-90909' ) " + "~r~n"
				elseif left( flds_type[i], 6 ) = "number" then
					sqls = sqls + "nvl( imp." + s_flds[i].column_name + ", -90909 ) <> nvl( td." + s_flds[i].column_name + ", -90909 ) " + "~r~n"
				elseif left( flds_type[i], 4 ) = "date" then
					sqls = sqls + "nvl( to_date( imp." + s_flds[i].column_name + ", 'mm/dd/yyyy' ), to_date( '1900-01-01', 'yyyy-mm-dd' ) ) <> nvl( td." + s_flds[i].column_name + ", to_date( '1900-01-01', 'yyyy-mm-dd' ) ) " + "~r~n"
				end if
			else
				update_pr = true
				
				if left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
					sqls = sqls + "nvl( trim( imp." + s_flds[i].column_name + " ), '-90909' ) <> nvl( trim( pr." + s_flds[i].column_name + " ), '-90909' ) " + "~r~n"
				elseif left( flds_type[i], 6 ) = "number" then
					sqls = sqls + "nvl( imp." + s_flds[i].column_name + ", -90909 ) <> nvl( pr." + s_flds[i].column_name + ", -90909 ) " + "~r~n"
				elseif left( flds_type[i], 4 ) = "date" then
					sqls = sqls + "nvl( to_date( imp." + s_flds[i].column_name + ", 'mm/dd/yyyy' ), to_date( '1900-01-01', 'yyyy-mm-dd' ) ) <> nvl( pr." + s_flds[i].column_name + ", to_date( '1900-01-01', 'yyyy-mm-dd' ) ) " + "~r~n"
				end if
			end if
		next
		
		sqls = sqls +			"									) " + "~r~n" + &
									"					) "
		
		execute immediate :sqls;
		
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while flagging the modified records (parcel).  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
				"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	do the updates.  update any field that's in the template, even if it's updating to the same value - that's for speed reasons.
////
		sqls =					"	update	pt_parcel pr " + "~r~n" + &
								"	set		( "
							
		sqls_update_ff =	"	update	pt_parcel_flex_fields ff " + "~r~n" + &
								"	set		( "
		
		sqls_update_td =	"	update	prop_tax_district td " + "~r~n" + &
								"	set		( "
		
		maxi = upperbound( s_flds[] )
		
		pr_index = 1
		ff_index = 1
		td_index = 1
		
		for i = 1 to maxi
			if left( s_flds[i].column_name, 4 ) = "flex" then
				if ff_index > 1 then sqls_update_ff = sqls_update_ff + ", "
				sqls_update_ff = sqls_update_ff + "ff." + s_flds[i].column_name
				ff_index++
			elseif s_flds[i].column_name = "assessor_id" or s_flds[i].column_name = "assignment_indicator" or s_flds[i].column_name = "county_id" or s_flds[i].column_name = "grid_coordinate" or s_flds[i].column_name = "is_parcel_one_to_one" or s_flds[i].column_name = "tax_district_code" or s_flds[i].column_name = "type_code_id" or s_flds[i].column_name = "use_composite_authority_yn" then
				if td_index > 1 then sqls_update_td = sqls_update_td + ", "
				sqls_update_td = sqls_update_td + "td." + s_flds[i].column_name
				td_index++
			else
				if pr_index > 1 then sqls = sqls + ", "
				sqls = sqls + "pr." + s_flds[i].column_name
				pr_index++
				
				//if they're updating state on the parcel, we want to update it on the district as well.  we only need to do this if it's 1-to-1, and that gets built into the where clause below.
				if s_flds[i].column_name = "state_id" then
					if td_index > 1 then sqls_update_td = sqls_update_td + ", "
					sqls_update_td = sqls_update_td + "td." + s_flds[i].column_name
					td_index++
				end if
			end if
		next
		
		sqls = sqls + &
						" ) = " + "~r~n" + &		
						"					(	select		"
							
		sqls_update_ff = sqls_update_ff + &
						" ) = " + "~r~n" + &		
						"					(	select		"
						
		sqls_update_td = sqls_update_td + &
						" ) = " + "~r~n" + &		
						"					(	select		"
		
		pr_index = 1
		ff_index = 1
		td_index = 1
		
		for i = 1 to maxi
			if left( s_flds[i].column_name, 4 ) = "flex" then
				if ff_index > 1 then sqls_update_ff = sqls_update_ff + ", "
				if left( flds_type[i], 4 ) = "date" then
					sqls_update_ff = sqls_update_ff + "to_date( imp." + s_flds[i].column_name + ", 'mm/dd/yyyy' )"
				elseif left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
					sqls_update_ff = sqls_update_ff + "trim( imp." + s_flds[i].column_name + " )"
				else
					sqls_update_ff = sqls_update_ff + "imp." + s_flds[i].column_name
				end if
				ff_index++
			elseif s_flds[i].column_name = "assessor_id" or s_flds[i].column_name = "assignment_indicator" or s_flds[i].column_name = "county_id" or s_flds[i].column_name = "grid_coordinate" or s_flds[i].column_name = "is_parcel_one_to_one" or s_flds[i].column_name = "tax_district_code" or s_flds[i].column_name = "type_code_id" or s_flds[i].column_name = "use_composite_authority_yn" then
				if td_index > 1 then sqls_update_td = sqls_update_td + ", "
				if left( flds_type[i], 4 ) = "date" then
					sqls_update_td = sqls_update_td + "to_date( imp." + s_flds[i].column_name + ", 'mm/dd/yyyy' )"
				elseif left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
					sqls_update_td = sqls_update_td + "trim( imp." + s_flds[i].column_name + " )"
				else
					sqls_update_td = sqls_update_td + "imp." + s_flds[i].column_name
				end if
				td_index++
			else
				if pr_index > 1 then sqls = sqls + ", "
				
				//if they're updating tax district, the import file may contain some 1-to-1 districts.  to handle this, if the tax district in the import is null, use the existing tax district on the parcel.
				if s_flds[i].column_name = "tax_district_id" then
					sqls = sqls + "nvl( imp." + s_flds[i].column_name + ", pr." + s_flds[i].column_name + " )"
				else
					if left( flds_type[i], 4 ) = "date" then
						sqls = sqls + "to_date( imp." + s_flds[i].column_name + ", 'mm/dd/yyyy' )"
					elseif left( flds_type[i], 4 ) = "char" or left( flds_type[i], 7 ) = "varchar" then
						sqls = sqls + "trim( imp." + s_flds[i].column_name + " )"
					else
						sqls = sqls + "imp." + s_flds[i].column_name
					end if
				end if
				
				pr_index++
				
				//if they're updating state on the parcel, we want to update it on the district as well.  we only need to do this if it's 1-to-1, and that gets built into the where clause below.
				if s_flds[i].column_name = "state_id" then
					if td_index > 1 then sqls_update_td = sqls_update_td + ", "
					sqls_update_td = sqls_update_td + "trim( imp." + s_flds[i].column_name + " )"
					td_index++
				end if
			end if
		next
		
		sqls = sqls +	"						from		pt_import_parcel imp " + "~r~n" + &
							"						where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
							"						and		imp.parcel_id = pr.parcel_id " + "~r~n" + &
							"					) " + "~r~n" + &
							"	where		pr.parcel_id in " + "~r~n" + &
							"					(	select		parcel_id " + "~r~n" + &
							"						from		pt_import_parcel " + "~r~n" + &
							"						where	import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
							"						and		nvl( is_modified, 0 ) = 1 " + "~r~n" + &
							"					) "
							
		sqls_update_ff = sqls_update_ff +	&
							"						from		pt_import_parcel imp " + "~r~n" + &
							"						where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
							"						and		imp.parcel_id = ff.parcel_id " + "~r~n" + &
							"					) " + "~r~n" + &
							"	where		ff.parcel_id in " + "~r~n" + &
							"					(	select		parcel_id " + "~r~n" + &
							"						from		pt_import_parcel " + "~r~n" + &
							"						where	import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
							"						and		nvl( is_modified, 0 ) = 1 " + "~r~n" + &
							"					) "
							
		sqls_update_td = sqls_update_td + &
							"						from		pt_import_parcel imp, " + "~r~n" + &
							"									pt_parcel pr " + "~r~n" + &
							"						where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
							"						and		imp.parcel_id = pr.parcel_id " + "~r~n" + &
							"						and		pr.tax_district_id = td.tax_district_id " + "~r~n" + &
							"					) " + "~r~n" + &
							"	where		td.is_parcel_one_to_one = 1 " + "~r~n" + &
							"	and			td.tax_district_id in " + "~r~n" + &
							"					(	select		pr.tax_district_id " + "~r~n" + &
							"						from		pt_import_parcel imp, " + "~r~n" + &
							"									pt_parcel pr " + "~r~n" + &
							"						where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
							"						and		nvl( imp.is_modified, 0 ) = 1 " + "~r~n" + &
							"						and		imp.parcel_id = pr.parcel_id " + "~r~n" + &
							"					) "
		
		if update_pr then
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while updating existing records (parcel).  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
		if update_ff then
			execute immediate :sqls_update_ff;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while updating existing records (parcel flex fields).  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
		if update_td then
			execute immediate :sqls_update_td;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while updating existing records (parcel one-to-one tax districts).  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
		
////
////	everything's ok so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_assessmentvalidatefromimport ( a_run_id, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_assessmentvalidatefromimport()
 **	
 **	validates the data in the parcel assessment import table from the given import run.
 **	
 **	parameters	:		:	(a_run_id) the import run to validate.
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	validation completed successfully (there could be error messages on the table, but the validation process completed successfully).
 **											false	:	an error occurred while validating.
 **
 ************************************************************************************************************************************************************/

string		sysval, sqls

////
////	make sure the case isn't locked for any of the records.
////
		update	pt_import_prcl_asmt imp
		set			imp.error_message = substr( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
					'this case has been locked and cannot be modified.', 1, 4000 )
		where	imp.import_run_id = :a_run_id
		and		exists
					(	select		'x'
						from		pt_case cs
						where	cs.case_id = imp.case_id
						and		cs.locked = 1
					);
		
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while checking for locked cases.  if you continue to receive this error contact your internal powerplant support or " + &
				"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	make sure the assessment groups we're importing into are set up for the given case / pt company / state combinations.
////
		update	pt_import_prcl_asmt imp
		set			imp.error_message = substr( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
					'this assessment group is not set up for the given case, prop tax company, and state.', 1, 4000 )
		where	imp.import_run_id = :a_run_id
		and		not exists
					(	select		'x'
						from		pt_assessment_group_case agc,
									pt_parcel p
						where	agc.case_id = imp.case_id
						and		agc.assessment_group_id = imp.assessment_group_id
						and		p.parcel_id = imp.parcel_id
						and		agc.prop_tax_company_id = p.prop_tax_company_id
						and		agc.state_id = p.state_id
					);
		
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while checking the assessment group setup.  if you continue to receive this error contact your internal powerplant support or " + &
				"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if

////
////	see if we should prevent assessments where there is no corresponding ledger item.  we can't deal with the "warn" option here, leave that to the calling program/function to handle correctly;
////	but we will check for the "yes" option here (i.e., the prevent option).
////
		sysval = upper( trim( i_uo_systemoptions.of_getsystemoption( "assessments - entry - restrict assessment entry by ledger items" ) ) )
				
		if sysval = "yes" then
			update	pt_import_prcl_asmt imp
			set			imp.error_message = substr( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
						'this case/parcel/assessment group combination does not have a corresponding ledger item, and the system option is set to restrict assessment entry by ledger items.', 1, 4000 )
			where	imp.import_run_id = :a_run_id
			and		not exists
						(	select		'x'
							from		pt_case cs,
										pt_ledger ldg,
										pt_ledger_tax_year lty,
										pt_assessment_group_types agt
							where	cs.case_id = imp.case_id
							and		cs.tax_year = lty.tax_year
							and		ldg.parcel_id = imp.parcel_id
							and		ldg.property_tax_ledger_id = lty.property_tax_ledger_id
							and		agt.case_id = cs.case_id
							and		agt.property_tax_type_id = ldg.property_tax_type_id
							and		agt.prop_tax_company_id = ldg.prop_tax_company_id
							and		agt.state_id = ldg.state_id
							and		agt.assessment_group_id = imp.assessment_group_id
						);
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while checking for ledger items.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if
								
////
////	see if we should allow assessment changes based on the "assessments - allow changes to assessments for paid bills" system option.
////	we only need to check for final cases.
////
		sysval = upper( trim( i_uo_systemoptions.of_getsystemoption( "assessments - allow changes to assessments for paid bills" ) ) )
		
		if sysval = "no" or sysval = "allow for partially paid bills" then
			sqls =					"	update	pt_import_prcl_asmt imp " + "~r~n" + &
									"	set			imp.error_message = substr( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || " + "~r~n" + &
									"				'this case/parcel combination has a paid bill, and the system option is set to not allow changes to assessments for paid bills.', 1, 4000 ) " + "~r~n" + &
									"	where	imp.import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
									"	and		exists " + "~r~n" + &
									"				(	select		'x' " + "~r~n" + &
									"					from		ptv_final_cases final_cs, " + "~r~n" + &
									"								pt_statement_statement_year ssy, " + "~r~n" + &
									"								pt_statement_line sl " + "~r~n" + &
									"					where	final_cs.case_id = imp.case_id " + "~r~n" + &
									"					and		final_cs.assessment_year_id = ssy.assessment_year_id " + "~r~n" + &
									"					and		ssy.statement_id = sl.statement_id " + "~r~n" + &
									"					and		ssy.statement_year_id = sl.statement_year_id " + "~r~n" + &
									"					and		sl.parcel_id = imp.parcel_id " + "~r~n"
			
			if sysval = "no" then
				sqls = sqls +	"					and		ssy.statement_status_id in ( 5, 6, 7, 8 ) " + "~r~n"
			elseif sysval = "allow for partially paid bills" then
				sqls = sqls +	"					and		ssy.statement_status_id in ( 7, 8 ) " + "~r~n"
			end if
			
			sqls = sqls +		"				) "
			
			execute immediate :sqls;
			
			if sqlca.sqlcode <> 0 then
				a_msg = "a database error occurred while checking to see if any assessments being updated are for parcels which have already been paid.  if you continue to receive this error contact your internal powerplant support or " + &
					"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
				return false
			end if
		end if					
		
////
////	check for duplicate rows in the import file.
////
		update	pt_import_prcl_asmt imp
		set			imp.error_message = substr( nvl( imp.error_message, '' ) || decode( trim( imp.error_message ), null, '', '', '', '  /  ' ) || 
					'this row is duplicated in the import file.', 1, 4000 )
		where	imp.import_run_id = :a_run_id
		and		( imp.case_id, imp.assessment_group_id, imp.parcel_id ) in
					(	select		impin.case_id, impin.assessment_group_id, impin.parcel_id
						from		pt_import_prcl_asmt impin
						where	impin.import_run_id = :a_run_id
						having	count(*) > 1
						group by impin.case_id, impin.assessment_group_id, impin.parcel_id
					);
		
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while checking for duplicate rows.  if you continue to receive this error contact your internal powerplant support or " + &
				"the powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	the validation completed successfully, so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_assessmenteditfromimport ( a_run_id,  a_template_id, ref string a_msg);/************************************************************************************************************************************************************
 **
 **	of_assessmenteditfromimport()
 **	
 **	adds/edits the parcel assessments for the given import run id.  checks to see if the assessment exists - if so, it edits it, otherwise, it adds it.
 **	
 **	parameters	:		:	(a_run_id) the import run to use when loading.
 **						ref string				:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	data loaded successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.  in addtion, errors are written back to the import run table.
 **
 ************************************************************************************************************************************************************/

/************************************************************************************************************************************************************
 **	important		:	if updating this function be sure to make the same changes to the of_assessmentedit() function as both functions are used to edit parcel assessments.
 **
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		j, maxj, i, maxi
		asmt_ilook_id, eqval_ilook_id, tval_ilook_id, cs_id, fixed_field
	string		sqls, rtn_str, where_sqls
	string		flds_type[]
	boolean	has_asmt, has_eqval, has_tval
	boolean	calcfrom_asmt, calcfrom_eqval, calcfrom_tval
	
/*
 *	misc objects.
 */
 	nvo_ppbase_import_templates		uo_templates
	s_ppbase_import_template_fields	s_flds[]
	uo_ds_top								ds_case
	 
////
////	get the fields for this run.
////
		uo_templates = create nvo_ppbase_import_templates
		
		if not uo_templates.of_importtemplategetfields( a_template_id, false, true, s_flds[], flds_type[], a_msg ) then
			destroy uo_templates
			return false
		end if
		
		destroy uo_templates
	
////
////	initialize our values.
////
		has_asmt = false
		has_eqval = false
		has_tval = false
		
		calcfrom_asmt = false
		calcfrom_eqval = false
		calcfrom_tval = false
		
		setnull( asmt_ilook_id )
		setnull( eqval_ilook_id )
		setnull( tval_ilook_id )
		
////
////	see what columns we have in the template.  also, store the lookup ids.
////
		maxj = upperbound( s_flds[]  )
		
		for j = 1 to maxj
			if s_flds[j].column_name = "assessment" then 
				has_asmt = true
				asmt_ilook_id = s_flds[j].import_lookup_id
			elseif s_flds[j].column_name = "equalized_value" then 
				has_eqval = true
				eqval_ilook_id = s_flds[j].import_lookup_id
			elseif s_flds[j].column_name = "taxable_value" then 
				has_tval = true
				tval_ilook_id = s_flds[j].import_lookup_id
			end if
		next
		
////
////	figure out if we're calculating from any one value.
////
		//if we don't have a lookup for assessment, then it's being manually entered.  see if the other values should be derived from it.
		if has_asmt and isnull( asmt_ilook_id ) and ( has_eqval or has_tval ) then
			if has_eqval and not isnull( eqval_ilook_id ) then
				if eqval_ilook_id = ilook_derive_eqval_from_asmt then calcfrom_asmt = true
			elseif has_tval and not isnull( tval_ilook_id ) then
				if tval_ilook_id = ilook_derive_taxval_from_asmt then calcfrom_asmt = true
			end if
		end if
		
		//if we don't have a lookup for equalized value, then it's being manually entered.  see if the other values should be derived from it.
		if not calcfrom_asmt then
			if has_eqval and isnull( eqval_ilook_id ) and ( has_asmt or has_tval ) then
				if has_asmt and not isnull( asmt_ilook_id ) then
					if asmt_ilook_id = ilook_derive_asmt_from_eqval then calcfrom_eqval = true
				elseif has_tval and not isnull( tval_ilook_id ) then
					if tval_ilook_id = ilook_derive_taxval_from_eqval then calcfrom_eqval = true
				end if
			end if
		end if
		
		//if we don't have a lookup for taxable value, then it's being manually entered.  see if the other values should be derived from it.
		if not calcfrom_asmt and not calcfrom_eqval then
			if has_tval and isnull( tval_ilook_id ) and ( has_asmt or has_eqval ) then
				if has_asmt and not isnull( asmt_ilook_id ) then
					if asmt_ilook_id = ilook_derive_asmt_from_taxval then calcfrom_tval = true
				elseif has_eqval and not isnull( eqval_ilook_id ) then
					if eqval_ilook_id = ilook_derive_eqval_from_taxval then calcfrom_tval = true
				end if
			end if
		end if

////
////	update existing rows.  if the assessment date or assessor are null in the import table, don't update them.
////
		update	pt_parcel_assessment asmt
		set			( asmt.assessment, asmt.equalized_value, asmt.taxable_value, asmt.assessment_date, asmt.assessor_id, asmt.use_tiered_rates_yn ) =
					(	select		to_number( imp.assessment ), to_number( imp.equalized_value ), to_number( imp.taxable_value ), nvl( to_date( imp.assessment_date, 'mm/dd/yyyy' ), asmt.assessment_date ), nvl( imp.assessor_id, asmt.assessor_id ), nvl( imp.use_tiered_rates_yn, 0 )
						from		pt_import_prcl_asmt imp
						where	imp.import_run_id = :a_run_id
						and		imp.case_id = asmt.case_id
						and		imp.assessment_group_id = asmt.assessment_group_id
						and		imp.parcel_id = asmt.parcel_id
					)
		where	exists
					(	select		'x'
						from		pt_import_prcl_asmt imp
						where	imp.import_run_id = :a_run_id
						and		imp.case_id = asmt.case_id
						and		imp.assessment_group_id = asmt.assessment_group_id
						and		imp.parcel_id = asmt.parcel_id
					);
		
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while updating existing parcel assessments in 'pt parcel assessment'.  " + &
						"if you continue to receive this message contact your internal powerplant support or the powerplan property tax " + &
						"support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	insert new rows.
////
		insert into pt_parcel_assessment ( parcel_id, assessment_group_id, case_id, assessment, equalized_value, taxable_value, assessment_date, assessor_id, use_tiered_rates_yn )
			select		imp.parcel_id, imp.assessment_group_id, imp.case_id, to_number( imp.assessment ), to_number( imp.equalized_value ), to_number( imp.taxable_value ), to_date( imp.assessment_date, 'mm/dd/yyyy' ), imp.assessor_id, nvl( imp.use_tiered_rates_yn, 0 )
			from		pt_import_prcl_asmt imp
			where	imp.import_run_id = :a_run_id
			and		not exists
						(	select		'x'
							from		pt_parcel_assessment asmt
							where	imp.case_id = asmt.case_id
							and		imp.assessment_group_id = asmt.assessment_group_id
							and		imp.parcel_id = asmt.parcel_id
						);
		
		if sqlca.sqlcode <> 0 then
			a_msg = "a database error occurred while inserting the new parcel assessments in 'pt parcel assessment'.  " + &
						"if you continue to receive this message contact your internal powerplant support or the powerplan property tax " + &
						"support desk.  the specific error returned was: " + sqlca.sqlerrtext
			return false
		end if
		
////
////	retrieve a list of cases in the import, as we'll have to loop through them below.
////
		sqls =		"	select		distinct case_id " + "~r~n" + &
					"	from		pt_import_prcl_asmt " + "~r~n" + &
					"	where	import_run_id = " + string( a_run_id ) + " "
		
		ds_case = create uo_ds_top
		rtn_str = f_create_dynamic_ds( ds_case, "grid", sqls, sqlca, true )
		
		if rtn_str <> "ok" then
			a_msg = "an internal error occurred while retrieving the cases being loaded.  if you continue to receive the message contact your internal powerplant support or the " + &
				"powerplan property tax support desk.  the specific error returned was: " + rtn_str
			destroy ds_case
			return false
		end if
		
		maxi = ds_case.rowcount()
		
////
////	now, if we are deriving one of the values from another, do that here.  we'll have to do this case by case, so loop through the list of cases.
////
		if calcfrom_asmt or calcfrom_eqval or calcfrom_tval then
			for i = 1 to maxi
			////
			////	get the case.
			////
					cs_id = ds_case.getitemnumber( i, 1 )
					
			////
			////	set the where clause.
			////
					where_sqls =	"			( pasmt.parcel_id, pasmt.assessment_group_id ) in " + "~r~n" + &
										"			(	select		parcel_id, assessment_group_id " + "~r~n" + &
										"				from		pt_import_prcl_asmt " + "~r~n" + &
										"				where	import_run_id = " + string( a_run_id ) + " " + "~r~n" + &
										"				and		case_id = " + string( cs_id ) + " " + "~r~n" + &
										"			) "
	
			////
			////	call the function to calculate the values.  tell it not to recalc the case, as we'll do that below.
			////
					if calcfrom_asmt then
						fixed_field = i_fixed_field_assessment
					elseif calcfrom_eqval then
						fixed_field = i_fixed_field_equalized_value
					else
						fixed_field = i_fixed_field_taxable_value
					end if
						
					if not this.of_assessmentcalcvalues( cs_id, where_sqls, fixed_field, false, a_msg ) then
						a_msg = "an error occurred while calculating the derived values for case id " + string( cs_id ) + ".  if you continue to receive this error please contact your internal powerplant support or the " + &
							"powerplan property tax support desk.  the specific error returned is below:~n~n" + a_msg
						destroy ds_case
						return false
					end if
			next
		end if
		
////
////	now we need to update the case calc.  there is already a function that does this from a temp table, so insert into the temp table and call the function.
////	we'll have to do this case by case, so loop through the list of cases.
////
		delete from pt_temp_parcels_asmt_gp;
				
		if sqlca.sqlcode <> 0 then
			a_msg = "error occurred during the case calc process while deleting the temp parcel-assessment groups stored off from a prior run.  no changes made.~n~nerror: " + sqlca.sqlerrtext
			destroy ds_case
			return false
		end if
	
		for i = 1 to maxi
		////
		////	get the case.
		////
				cs_id = ds_case.getitemnumber( i, 1 )
				
		////
		////	insert for the given case.
		////
				insert into pt_temp_parcels_asmt_gp ( parcel_id, assessment_group_id )
					select		distinct parcel_id,
								assessment_group_id
					from		pt_import_prcl_asmt
					where	import_run_id = :a_run_id
					and		case_id = :cs_id;
								
				if sqlca.sqlcode <> 0 then
					a_msg = "error occurred while inserting the temp parcel-assessment groups for which to update values for case id " + string( cs_id ) + ".  no changes made.~n~nerror: " + sqlca.sqlerrtext
					destroy ds_case
					return false
				end if
				
		////
		////	set the where clause.
		////
				where_sqls =	"			temp_pag.parcel_id = ptl.parcel_id " + "~r~n" + &
									"	and	temp_pag.assessment_group_id = agt.assessment_group_id "

		////
		////	call the function to update case calc and case calc authority for the changed assessments.
		////
				if not i_uo_case.of_populatecasecalc( cs_id, where_sqls, a_msg ) then
					a_msg = "an error occurred while generating the assessment to asset allocation for case id " + string( cs_id ) + ".  if you continue to receive this error please contact your internal powerplant support or the " + &
						"powerplan property tax support desk.  the specific error returned is below:~n~n" + a_msg
					destroy ds_case
					return false
				end if

		////
		////	delete our temp parcels - assessment groups so we don't cause any errors in other functions that might use it later on before returning.
		////
				delete from pt_temp_parcels_asmt_gp;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "error occurred while deleting the temp parcels stored off when updating parcel assessments.  no changes made.~n~nerror: " + sqlca.sqlerrtext
					destroy ds_case
					return false
				end if
		next
		
		destroy ds_case

////
////	we were successful so clear out the error message and return true.
////
		a_msg = ""
		return true
end function

public function boolean of_parcelappealeventedit (ref s_pt_parcel_appeal_event as_event[], ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelappealeventedit()
 **	
 **	edits the pt parcel appeal event table for the given rows.  editing means inserts or updates as necessary.  this function will take care of doing the update or the insert correctly based on whether
 **	the array node has an event_id defined or not.  an event_id means the row will be updated.  otherwise it will be an insert.  for inserts, the just inserted event_id will be passed back
 **	via the structure.
 **	
 **	parameters	:	ref s_pt_parcel_appeal_event	:	(as_event[]) the parcel appeal event rows to insert/update.  a bllank event_id indicates an insert; a populated event_id indicates an update.  if it's an
 **																		insert, the new event_id will be populated in the event_id column of the structure and passed back.
 **						ref string								:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel appeal events were edited successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi
		evnt_id[]

////
////	loop over the rows and insert / update as necessary.
////
			maxi = upperbound( as_event[] )
			
			for i = 1 to maxi
				////
				////	validate.
				////
					if isnull( as_event[i].parcel_id ) then
						a_msg = "'parcel' is a required field.  one of the appeal events to update is missing a parcel.  if you continue to receive this error contact your internal powerplant support or " + &
							"the powerplan property tax support desk."
						return false
					end if
					
					if isnull( as_event[i].appeal_id ) then
						a_msg = "'appeal  id' is a required field.  one of the appeal events to update is missing an appeal id.  if you continue to receive this error contact your internal powerplant support or " + &
							"the powerplan property tax support desk."
						return false
					end if
					
					if isnull( as_event[i].event_date ) then
						a_msg = "'event date' is a required field.  one of the appeal events to update is missing an event date.  please check row " + string ( i ) + " to ensure an event date is entered."
						return false
					end if
					
					if isnull( as_event[i].notes ) or trim( as_event[i].notes ) = "" then
						a_msg = "'notes' is a required field.  one of the appeal events to update is missing notes.  please check row " + string ( i ) + " to ensure event notes are entered."
						return false
					end if
					
					if len( as_event[i].notes ) > 2000 then
						a_msg = "the event 'notes' field is limited to 2000 characters.  please edit row " + string( i ) + " as the event notes on that row are greater than 2000 characters."
						return false
					end if
				
				////
				////	do we insert or update.  if we're inserting, get the next in line event id and store it in our event id array.  we'll wait until the very end to update the event id on the structure in
				////	case we encounter an error midway through the process.  if we're updating, just set the event id to the event id current in the structure.
				////
					//if the passed-in event id is null, check to see if the given parcel/appeal id/date/notes already exist in the database.  if so, we don't need to do anything since the record already exists.
					setnull( evnt_id[i] )
					
					if isnull( as_event[i].event_id ) then
						select		event_id
						into		:evnt_id[i]
						from		pt_parcel_appeal_event
						where	parcel_id = :as_event[i].parcel_id
						and		appeal_id = :as_event[i].appeal_id
						and		event_date = :as_event[i].event_date
						and		upper( trim( notes ) ) = upper( trim( :as_event[i].notes ) );
						
						if not isnull( evnt_id[i] ) then continue
					else
						evnt_id[i] = as_event[i].event_id
					end if
						
					//insert or update
					if isnull( evnt_id[i] ) then
						select		nvl( max( event_id ), 0 ) + 1
						into		:evnt_id[i]
						from		pt_parcel_appeal_event
						where	parcel_id = :as_event[i].parcel_id
						and		appeal_id = :as_event[i].appeal_id;
						
						insert into pt_parcel_appeal_event ( parcel_id, appeal_id, event_id, event_date, notes )
							values ( :as_event[i].parcel_id, :as_event[i].appeal_id, :evnt_id[i], :as_event[i].event_date, trim( :as_event[i].notes ) );
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while inserting a new appeal event.  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
								"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					else
						update	pt_parcel_appeal_event pae
						set			pae.event_date = :as_event[i].event_date,
									pae.notes = trim( :as_event[i].notes )
						where	pae.parcel_id = :as_event[i].parcel_id
						and		pae.appeal_id = :as_event[i].appeal_id
						and		pae.event_id = :as_event[i].event_id;
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while updating an existing appeal event.  if you continue to receive this error contact your internal powerplant support or the " + &
								"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					end if
			next

////
////	we're in the clear for errors so populate the event id on the passed in structure.
////
			for i = 1 to maxi
				as_event[i].event_id = evnt_id[i]
			next

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parcelappealeventdelete (s_pt_parcel_appeal_event as_event, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelappealeventdelete()
 **	
 **	deletes the passed-in appeal event from the pt parcel appeal event table.  passing in a null event id means all events for the given parcel and appeal should be deleted.
 **	
 **	parameters	:	s_pt_parcel_appeal_event	:	(as_event) the event that should be deleted.
 **						ref string							:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the data was deleted successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

////
////	validate.
////
			if isnull( as_event.parcel_id ) then
				a_msg = "an internal error occurred while attempting to delete appeal events.  no parcel was specified as the parcel from which to delete events.  if you continue to receive this " + &
					"error please contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if
			
			if isnull( as_event.appeal_id ) then
				a_msg = "an internal error occurred while attempting to delete appeal events.  no appeal was specified as the appeal from which to delete events.  if you continue to receive this " + &
					"error please contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if

////
////	do the delete.  if the event id is null, delete all events for this parcel and appeal.  otherwise, just delete the specified event.
////
			if isnull( as_event.event_id ) then
				delete from pt_parcel_appeal_event pae
					where	pae.parcel_id = :as_event.parcel_id
					and		pae.appeal_id = :as_event.appeal_id;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "an error occurred while deleting all appeal events for the selected parcel.  if you continue to receive this error please contact your internal powerplant support or the " + &
						"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
			else
				delete from pt_parcel_appeal_event pae
					where	pae.parcel_id = :as_event.parcel_id
					and		pae.appeal_id = :as_event.appeal_id
					and		pae.event_id = :as_event.event_id;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "an error occurred while deleting the specified appeal event for the selected parcel.  if you continue to receive this error please contact your internal powerplant support or the " + &
						"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parcelappealdelete (s_pt_parcel_appeal as_appeal, ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelappealdelete()
 **	
 **	deletes the passed-in appeal from the pt parcel appeal table.  passing in a null appeal id means all appeals for the given parcel should be deleted.
 **	this function will delete any pt parcel appeal event records associated with the appeal being deleted.
 **	
 **	parameters	:	s_pt_parcel_appeal	:	(as_appeal) the appeal that should be deleted.
 **						ref string					:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the data was deleted successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

////
////	validate.
////
			if isnull( as_appeal.parcel_id ) then
				a_msg = "an internal error occurred while attempting to delete appeals.  no parcel was specified as the parcel from which to delete appeals.  if you continue to receive this " + &
					"error please contact your internal powerplant support or the powerplan property tax support desk."
				return false
			end if
			
////
////	do the delete.  if the appeal id is null, delete all appeals for this parcel.  otherwise, just delete the specified appeal.
////	also, delete any events associated with the appeal.
////
			if isnull( as_appeal.appeal_id ) then
				delete from pt_parcel_appeal_event pae
					where	pae.parcel_id = :as_appeal.parcel_id;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "an error occurred while deleting appeal events for all of the selected parcel's appeals.  if you continue to receive this error please contact your internal powerplant support or the " + &
						"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
				
				delete from pt_parcel_appeal pa
					where	pa.parcel_id = :as_appeal.parcel_id;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "an error occurred while deleting all appeals for the selected parcel.  if you continue to receive this error please contact your internal powerplant support or the " + &
						"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
			else
				delete from pt_parcel_appeal_event pae
					where	pae.parcel_id = :as_appeal.parcel_id
					and		pae.appeal_id = :as_appeal.appeal_id;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "an error occurred while deleting the appeal events for the specified parcel appeal.  if you continue to receive this error please contact your internal powerplant support or the " + &
						"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
				
				delete from pt_parcel_appeal pa
					where	pa.parcel_id = :as_appeal.parcel_id
					and		pa.appeal_id = :as_appeal.appeal_id;
				
				if sqlca.sqlcode <> 0 then
					a_msg = "an error occurred while deleting the specified appeal for the selected parcel.  if you continue to receive this error please contact your internal powerplant support or the " + &
						"powerplan property tax support desk.~n~nerror: " + sqlca.sqlerrtext
					return false
				end if
			end if

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

public function boolean of_parcelappealedit (ref s_pt_parcel_appeal as_appeal[], ref string a_msg);/************************************************************************************************************************************************************
 **	
 **	of_parcelappealedit()
 **	
 **	edits the pt parcel appeal table for the given rows.  editing means inserts or updates as necessary.  this function will take care of doing the update or the insert correctly based on whether
 **	the array node has an appeal_id and if it exists in the database.  an existing appeal_id means the row will be updated.  otherwise it will be an insert.  for inserts, the just inserted appeal_id will be passed back
 **	via the structure.
 **
 **	the array node may have appeal_id populated even if it's a new record.  this is because the parcel details window has to set the appeal_id there so that corresponding events can be added.
 **	
 **	parameters	:	ref s_pt_parcel_appeal			:	(as_appeal[]) the parcel appeal rows to insert/update.  a blank appeal_id indicates an insert; a populated appeal_id indicates an update.  if it's an
 **																		insert, the new appeal_id will be populated in the appeal_id column of the structure and passed back.
 **						ref string								:	(a_msg) a reference variable used to send back an error message if an error occurs.
 **	
 **	returns		:	boolean		:	true	:	the parcel appeals were edited successfully.
 **											false	:	an error occurred.  check the a_msg parameter to get the error message.
 **	
 ************************************************************************************************************************************************************/

/*
 *	processing.
 */
		i, maxi, count
		appl_id[]
	boolean	appeal_exists

////
////	loop over the rows and insert / update as necessary.
////
			maxi = upperbound( as_appeal[] )
			
			for i = 1 to maxi
				////
				////	validate.
				////
					if isnull( as_appeal[i].parcel_id ) then
						a_msg = "'parcel' is a required field.  one of the appeals to update is missing a parcel.  if you continue to receive this error contact your internal powerplant support or " + &
							"the powerplan property tax support desk."
						return false
					end if
					
					if isnull( as_appeal[i].assessment_year_id ) then
						a_msg = "'assessment year' is a required field.  one of the appeals to update is missing an assessment year.  if you continue to receive this error contact your internal powerplant support or " + &
							"the powerplan property tax support desk."
						return false
					end if
					
					if isnull( as_appeal[i].appeal_type_id ) then
						a_msg = "'appeal type' is a required field.  one of the appeals to update is missing an appeal type.  if you continue to receive this error contact your internal powerplant support or " + &
							"the powerplan property tax support desk."
						return false
					end if
					
					if isnull( as_appeal[i].appeal_status_id ) then
						a_msg = "'appeal status' is a required field.  one of the appeals to update is missing an appeal status.  if you continue to receive this error contact your internal powerplant support or " + &
							"the powerplan property tax support desk."
						return false
					end if
					
					if len( as_appeal[i].notes ) > 2000 then
						a_msg = "the event 'notes' field is limited to 2000 characters.  please edit row " + string( i ) + " as the event notes on that row are greater than 2000 characters."
						return false
					end if
				
				////
				////	do we insert or update.  if we're inserting and appeal id is not populated, get the next in line appeal id and store it in our appeal id array.  we'll wait until the very end to update the appeal id on the structure in
				////	case we encounter an error midway through the process.  if we're updating, just set the appeal id to the appeal id current in the structure.
				////
					setnull( appl_id[i] )
					appeal_exists = false
					
					if not isnull( as_appeal[i].appeal_id ) then
						//see if this appeal id exists
						setnull( count )
						select		count(*)
						into		:count
						from		pt_parcel_appeal
						where	parcel_id = :as_appeal[i].parcel_id
						and		appeal_id = :as_appeal[i].appeal_id;
						
						if not isnull( count ) and count > 0 then
							appeal_exists = true
						end if
						
						appl_id[i] = as_appeal[i].appeal_id
					end if
						
					//insert or update
					if not appeal_exists then
						if isnull( appl_id[i] ) then
							//get the next appeal id if one wasn't passed to us - appeal id is by parcel, not parcel/assessment year, as that is complicated since a user may change the assessment year
							select		nvl( max( appeal_id ), 0 ) + 1
							into		:appl_id[i]
							from		pt_parcel_appeal
							where	parcel_id = :as_appeal[i].parcel_id;
						end if
						
						insert into pt_parcel_appeal ( parcel_id, assessment_year_id, appeal_id, appeal_type_id, appeal_date, target_assessment, assigned_to, resolved_date, resolved_assessment, appeal_status_id, notes )
							values ( :as_appeal[i].parcel_id, :as_appeal[i].assessment_year_id, :appl_id[i], :as_appeal[i].appeal_type_id, :as_appeal[i].appeal_date, :as_appeal[i].target_assessment, 
										:as_appeal[i].assigned_to, :as_appeal[i].resolved_date, :as_appeal[i].resolved_assessment, :as_appeal[i].appeal_status_id, trim( :as_appeal[i].notes ) );
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while inserting a new appeal.  if you continue to receive this error contact your internal powerplant support or the powerplan " + &
								"property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					else
						update	pt_parcel_appeal pa
						set			pa.assessment_year_id = :as_appeal[i].assessment_year_id,
									pa.appeal_type_id = :as_appeal[i].appeal_type_id, 
									pa.appeal_date = :as_appeal[i].appeal_date, 
									pa.target_assessment = :as_appeal[i].target_assessment, 
									pa.assigned_to = :as_appeal[i].assigned_to, 
									pa.resolved_date = :as_appeal[i].resolved_date,
									pa.resolved_assessment = :as_appeal[i].resolved_assessment, 
									pa.appeal_status_id = :as_appeal[i].appeal_status_id,
									pa.notes = trim( :as_appeal[i].notes )
						where	pa.parcel_id = :as_appeal[i].parcel_id
						and		pa.appeal_id = :as_appeal[i].appeal_id;
						
						if sqlca.sqlcode <> 0 then
							a_msg = "a database error occurred while updating an existing appeal.  if you continue to receive this error contact your internal powerplant support or the " + &
								"powerplan property tax support desk.  the specific error returned was: " + sqlca.sqlerrtext
							return false
						end if
					end if
			next

////
////	we're in the clear for errors so populate the appeal id on the passed in structure.
////
			for i = 1 to maxi
				as_appeal[i].appeal_id = appl_id[i]
			next

////
////	everything's ok so clear out the error message and return true.
////
			a_msg = ""
			return true
end function

on nvo_ptc_logic_parcel.create
call super::create
triggerevent( this, "constructor" )
end on

on nvo_ptc_logic_parcel.destroy
triggerevent( this, "destructor" )
call super::destroy
end on

event constructor;/*
 *	processing.
 */
	string	sysval

////
////	create some user objects we'll need.
////
			i_uo_case = create nvo_ptc_logic_case
			i_uo_systemoptions = create nvo_ppbase_sysoption_functions

////
////	get the number of parcels at which we start using the temp table from the system options.
////
			sysval = upper( trim( i_uo_systemoptions.of_getsystemoption( "database - temp parcels - minimum number of parcels in list for temp parcels use" ) ) )
			
			if isnull( sysval ) then sysval = "200"
			
			if not isnumber( sysval ) then
				i_temp_prcl_start = 200
			else
				i_temp_prcl_start = long( sysval )
			end if
			
			if isnull( i_temp_prcl_start ) then i_temp_prcl_start = 200

////
////	get the number of parcel - assessment group combos at which we start using the temp table from the system options.
////
			sysval = upper( trim( i_uo_systemoptions.of_getsystemoption( "database - temp parcels asmt groups - minimum number of parcel-assessment group combos in list for temp parcels asmt groups use" ) ) )
			
			if isnull( sysval ) then sysval = "100"
			
			if not isnumber( sysval ) then
				i_temp_prcl_ag_start = 100
			else
				i_temp_prcl_ag_start = long( sysval )
			end if
			
			if isnull( i_temp_prcl_ag_start ) then i_temp_prcl_ag_start = 100
end event

event destructor;////
////	destroy the user objects.
////
			destroy i_uo_case
			destroy i_uo_systemoptions
end event

