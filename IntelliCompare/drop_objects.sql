set serveroutput on
declare
   v_count  number;
   v_sql    varchar2(1000);
   v_table  varchar2(256);
   v_constr varchar2(256);
begin
   v_table  := 'INTELLI_DATA';
   v_constr := 'FK_INTELLI_DATA_UPGD_ID';
   select count(*) into v_count from user_constraints where constraint_name = v_constr AND table_name = v_table;
   if v_count > 0 then
      v_sql := 'alter table ' || v_table || ' drop constraint ' || v_constr;
      EXECUTE IMMEDIATE v_sql;
      dbms_output.put_line('Constraint dropped ' || v_constr || ' from table ' || v_table);
   else
      dbms_output.put_line('Constraint ' || v_constr || ' does not exist on table ' || v_table);
   end if;
   
   v_table  := 'PP_OBJECTS_DC';
   v_constr := 'FK_PP_OBJECTS_DC_UPGD_ID';
   select count(*) into v_count from user_constraints where constraint_name = v_constr AND table_name = v_table;
   if v_count > 0 then
      v_sql := 'alter table ' || v_table || ' drop constraint ' || v_constr;
      EXECUTE IMMEDIATE v_sql;
      dbms_output.put_line('Constraint dropped ' || v_constr || ' from table ' || v_table);
   else
      dbms_output.put_line('Constraint ' || v_constr || ' does not exist on table ' || v_table);
   end if;

   v_table  := 'PP_OBJECTS_IC';
   v_constr := 'FK_PP_OBJECTS_IC_UPGD_ID';
   select count(*) into v_count from user_constraints where constraint_name = v_constr AND table_name = v_table;
   if v_count > 0 then
      v_sql := 'alter table ' || v_table || ' drop constraint ' || v_constr;
      EXECUTE IMMEDIATE v_sql;
      dbms_output.put_line('Constraint dropped ' || v_constr || ' from table ' || v_table);
   else
      dbms_output.put_line('Constraint ' || v_constr || ' does not exist on table ' || v_table);
   end if;

   v_table  := 'INTELLI_SOURCE';
   v_constr := 'FK_INTELLI_SOURCE_UPGD_ID';
   select count(*) into v_count from user_constraints where constraint_name = v_constr AND table_name = v_table;
   if v_count > 0 then
      v_sql := 'alter table ' || v_table || ' drop constraint ' || v_constr;
      EXECUTE IMMEDIATE v_sql;
      dbms_output.put_line('Constraint dropped ' || v_constr || ' from table ' || v_table);
   else
      dbms_output.put_line('Constraint ' || v_constr || ' does not exist on table ' || v_table);
   end if;
   
   v_table  := 'PP_OBJECTS_IC';
   select count(*) into v_count from tab where tname = v_table;
   if v_count > 0 then
      v_sql := 'drop table ' || v_table;
      EXECUTE IMMEDIATE v_sql;
      dbms_output.put_line('Table ' || v_table || ' dropped');
   else
      dbms_output.put_line('Table ' || v_table || ' does not exist, so cannot be dropped');
   end if;
   
   v_table  := 'INTELLI_SOURCE';
   select count(*) into v_count from tab where tname = v_table;
   if v_count > 0 then
      v_sql := 'drop table ' || v_table;
      EXECUTE IMMEDIATE v_sql;
      dbms_output.put_line('Table ' || v_table || ' dropped');
   else
      dbms_output.put_line('Table ' || v_table || ' does not exist, so cannot be dropped');
   end if;

   v_table  := 'INTELLI_DATA';
   select count(*) into v_count from tab where tname = v_table;
   if v_count > 0 then
      v_sql := 'drop table ' || v_table;
      EXECUTE IMMEDIATE v_sql;
      dbms_output.put_line('Table ' || v_table || ' dropped');
   else
      dbms_output.put_line('Table ' || v_table || ' does not exist, so cannot be dropped');
   end if;

   v_table := 'UPGRADE_MASTER';
   select count(*) into v_count from tab where tname = v_table;
   if v_count > 0 then
      v_sql := 'drop table ' || v_table;
      EXECUTE IMMEDIATE v_sql;
      dbms_output.put_line('Table ' || v_table || ' dropped');
   else
      dbms_output.put_line('Table ' || v_table || ' does not exist, so cannot be dropped');
   end if;

   v_table  := 'INTELLI_DATA_ARC';
   select count(*) into v_count from tab where tname = v_table;
   if v_count > 0 then
      v_sql := 'drop table ' || v_table;
      EXECUTE IMMEDIATE v_sql;
      dbms_output.put_line('Table ' || v_table || ' dropped');
   else
      dbms_output.put_line('Table ' || v_table || ' does not exist, so cannot be dropped');
   end if;
exception
   when others then
      dbms_output.put_line('Error came, details are... ' || dbms_utility.format_error_stack() || dbms_utility.format_error_backtrace());
end;