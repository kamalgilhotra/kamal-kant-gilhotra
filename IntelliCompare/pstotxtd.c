
#define _Windows
#define __WIN32__


#include <windows.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <sys/stat.h>


#include "gsdll.h"
#include "ptotdll.h"
#define DllImport	__declspec( dllimport )
#define DllExport	__declspec( dllexport )


//__declspec(dllexport)
int WINAPI VeryPDF_PSToPDF(LPCTSTR lpCommand);
int (WINAPI * lpfnVeryPDF_PSToPDF) (LPCTSTR lpCommand);


#define FALSE 0
#define TRUE 1
#define LINELEN 2000 /* longest allowable line from gs ocr.ps output */
#define ERR_LOAD_LIB -1
#define ERR_LOAD_FUNC -1
/* 4.1.3 removed the batch option for postscript printers 8/23/07 */
/* 4.1.4 added entry point PBORCA_LibraryEntryExport 9/20/07 */
/* 4.1.6 improve speed of creating PDF 10/23/07 */
/* 5.1.1 Fixed VeryPDF Orientation 3/23/09 */
/* 5.1.3 Another Fixed VeryPDF Orientation 3/31/09 */
/* 5.1.4 Removed  dynapdf.dll 7/09/09 */
/* 5.1.5 added path to error msg on loading library  1/27/10 */
/* 5.1.5  Ghostscript Orientation 2/8/10 */
/* 5.1.6  resource IDs for pstotxt3.dll */
/* 5.1.8  added PDF merge */
/* 5.1.9  try to load libraries in the current path */
/* 5.1.11 Changed mode to 0 4/29/10 */
/* 5.1.12  increase the command line length for merge pdfs 11/30/10  */
/* 5.2.0  add support for PowerBuilder 12 8/23/11  */
/* 5.2.1  Support for 4000 Objects  8/23/11  */
#define OCR_PROLOG 1
#define OCR_PROLOG8 109

#define ROT270_PROLOG 2
#define ROT90_PROLOG 3

//#define PS2ASCII

void error_msg(char *msg);
static int cleanup(void);
static void do_it(char *path);
#define strcasecmp stricmp

#define MAXPATHLEN 256
static int debug = FALSE;
static int cork = FALSE;

static char *gscommand = "d:\\ghost\\gs5.10\\gswin32c.exe -Id://ghost//psfonts//;d://ghost//gs5.10// ";
static char GSDLL[400];
static char VERYDLL[400];
static char DYNAPDFDLL[400];

//        Amyuni
#define AmyuniPDF_TYPE 4
static char AMYUNIDLL[400];
long (WINAPI * lpfnDriverInit) (LPCTSTR lpCommand);
long (WINAPI * lpfnPDFDriverInit) (LPCTSTR Printer );
void (WINAPI * lpfnDriverEnd) (long hPrinter);
long (WINAPI * lpfnSetDefaultDirectory) (long hPrinter, long Directory) ;
long (WINAPI * lpfnSetDefaultFileName) (long hPrinter, LPCTSTR FileName) ;
long (WINAPI * lpfnSetFileNameOptions) (long hPrinter, int Options) ;
long (WINAPI * lpfnSetLicenseKeyA) (LPCTSTR szcompany, LPCTSTR szLicense);
long (WINAPI * lpfnSetResolution) (long hPrinter, long Resolution) ;
long (WINAPI * lpfnGetResolution) (long hPrinter) ;
long (WINAPI * lpfnSetDefaultConfig) (long hPrinter) ;
long (WINAPI * lpfnSetDefaultPrinter) (long hPrinter) ;
long (WINAPI * lpfnSetPaperWidth)( long hPrinter, long nPaperWidth ) ;
long (WINAPI * lpfnGetPaperWidth)( long hPrinter );
long (WINAPI * lpfnSetPaperLength)( long hPrinter, long nPaperLength ) ;
long (WINAPI * lpfnGetPaperLength)(long hPrinter ) ;
long (WINAPI * lpfnGetOrientation)(long hPrinter ) ;
long (WINAPI * lpfnSetOrientation)( long hPrinter, long orientation ) ;
long (WINAPI * lpfnRestoreDefaultPrinter)( long hPrinter  );
//Function long SendMail(LPCTSTR Toadd, LPCTSTR CCadd, LPCTSTR BCCadd, LPCTSTR Subject, LPCTSTRMessage, LPCTSTR FileNames, long Options) ; alias for "SendMail;Ansi"
long (WINAPI * lpfnLock) (long hPrinter, LPCTSTR DocumentTitle) ;
long (WINAPI * lpfnUnlock) (long hPrinter, LPCTSTR DocumentTitle, long TimeOut);
long (WINAPI * lpfnSetDocFileProps) (long hPrinter, LPCTSTR DocumentTitle, long FileOptions,LPCTSTR FileDirectory, LPCTSTR FileName) ;
// error message associated with error code
void (WINAPI * lpfnGetLastErrorMsg) (LPCTSTR Msg, long MaxMsg);
long (WINAPI * lpfnGetDocumentTitle) (LPCTSTR Msg, long MaxMsg);
// Functions needed to using the PDF Printer Developer edition version 2.1 to:
// set the permanent license key for the product
long (WINAPI * lpfnSetLicenseKey) (LPCTSTR szCompany, LPCTSTR szLicKey) ;
// Enable the printer before every print job
long (WINAPI * lpfnEnablePrinter) (long hPrinter, LPCTSTR szCompany, LPCTSTR szCode);


static HMODULE pstotextModule;
void *pstotextInstance;

static int search_flag;
static int search_pos;
static int search_options;
static char search_str[LINELEN];
static int page;

#define MAX_SEARCH 200
#define OPT_MATCH_CASE 2
#define TEXT_TYPE 1
#define SEARCH_TYPE 2
#define VERYPDF_TYPE 3
#define VERYPDF_MERGE_TYPE 11
#define GSPDF_TYPE 10
#define PAGE_WIDTH_LAND 8 * 72
#define PAGE_WIDTH_PORT 10.5 * 72
struct s_search
   {
   int page;
   int x;
   int y;
   int h;
   int w;
   } searchlist[MAX_SEARCH];
typedef struct s_search_info
      {
      int count;
      char *str;
      int option;
      struct s_search searchlist[MAX_SEARCH];
      } SEARCH_INFO ,* PSEARCH_INFO;

static char *cmd;
char save_str[LINELEN];
FILE *unit;
#define MAX_LINES 500
#define MAX_STRLEN 400
typedef struct s_linfo
     {
     int xr;
     int xc;
     int xl;
     int y;
     int h;
     char str[MAX_STRLEN + 1];
     int flag;
     struct s_linfo *next;
     }  LINFO,* PLINFO;

PLINFO linelist[MAX_LINES];
int linecount;
int prev_y,prev_xr;
int getdevice(int *inchx,int *inchy);
add_line(PLINFO *pline,int xl,int xr,int y,char *str);
int findstring(char *line,char *search_str,int options);
long WINAPI  GetPrinterOrientation();
static enum {
  portrait,
  landscape,
  landscapeOther} orientation = portrait;

static int bboxes = FALSE;

static int explicitFiles = 0; /* count of explicit file arguments */

#define __WINDOWS__


int (GSDLLAPI *lpfngsdll_revision)(char **product, char **copyright,
        long *gs_revision, long *gs_revisiondate);
int (GSDLLAPI *lpfngsdll_init)(GSDLL_CALLBACK callback, HWND hwnd,
        int argc, char *argv[]);
int (GSDLLAPI *lpfngsdll_execute_begin)(void);
int (GSDLLAPI *lpfngsdll_execute_cont)(const char *str, int len);
int (GSDLLAPI *lpfngsdll_execute_end)(void);
int (GSDLLAPI *lpfngsdll_exit)(void);
int (GSDLLAPI *lpfngsdll_lock_device)(unsigned char *device, int flag);
static HANDLE hLibrary;

/* *************************** DllMain  ***********************************/
BOOL WINAPI DllMain( HINSTANCE hInstance, DWORD dwReason, LPVOID lpvReserved )
  {
   pstotextModule = hInstance;
   return( 1 );
  }

/***************************** Close OCI Handle ****************************/






/**************************** InitGS ***************************************/

int FAR PASCAL InitGS()
{

  if ((hLibrary = LoadLibrary(GSDLL)) == NULL)
    return(ERR_LOAD_LIB);

  if ((lpfngsdll_revision = GetProcAddress(hLibrary,"gsdll_revision")) == NULL)
    return(ERR_LOAD_FUNC);

  if ((lpfngsdll_init= GetProcAddress(hLibrary,"gsdll_init")) == NULL)
    return(ERR_LOAD_FUNC);

  if ((lpfngsdll_execute_begin= GetProcAddress(hLibrary,"gsdll_execute_begin")) == NULL)
    return(ERR_LOAD_FUNC);

  if ((lpfngsdll_execute_cont= GetProcAddress(hLibrary,"gsdll_execute_cont")) == NULL)
    return(ERR_LOAD_FUNC);

  if ((lpfngsdll_execute_end= GetProcAddress(hLibrary,"gsdll_execute_end")) == NULL)
    return(ERR_LOAD_FUNC);

  if ((lpfngsdll_exit= GetProcAddress(hLibrary,"gsdll_exit")) == NULL)
    return(ERR_LOAD_FUNC);

  if ((lpfngsdll_lock_device = GetProcAddress(hLibrary,"gsdll_lock_device")) == NULL)
    return(ERR_LOAD_FUNC);


  return(0);
}

/**************************** InitVeryPDF ***************************************/
int FAR PASCAL InitVeryPDF()
{

	hLibrary = LoadLibrary(DYNAPDFDLL);
       //return(ERR_LOAD_LIB);
	if ((hLibrary = LoadLibrary(VERYDLL)) == NULL)
       return(ERR_LOAD_LIB);


  if ((lpfnVeryPDF_PSToPDF = GetProcAddress(hLibrary,"VeryPDF_PSToPDF")) == NULL)
    return(ERR_LOAD_FUNC);


  return(0);
}

/**************************** Amyuni PDF ***************************************/
int FAR PASCAL InitAmyuniPDF()
{

   if ((hLibrary = LoadLibrary(AMYUNIDLL)) == NULL)
       return(ERR_LOAD_LIB);


		if ((lpfnDriverInit = GetProcAddress(hLibrary,"DriverInit")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnPDFDriverInit = GetProcAddress(hLibrary,"PDFDriverInit")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnDriverEnd = GetProcAddress(hLibrary,"DriverEnd")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnSetDefaultDirectory = GetProcAddress(hLibrary,"SetDefaultDirectory")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnSetDefaultFileName = GetProcAddress(hLibrary,"SetDefaultFileName")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnSetFileNameOptions = GetProcAddress(hLibrary,"SetFileNameOptions")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnSetLicenseKeyA = GetProcAddress(hLibrary,"SetLicenseKeyA")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnSetResolution = GetProcAddress(hLibrary,"SetResolution")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnGetResolution = GetProcAddress(hLibrary,"DriverInit")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnSetDefaultConfig = GetProcAddress(hLibrary,"GetResolution")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnSetDefaultPrinter = GetProcAddress(hLibrary,"SetDefaultPrinter")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnSetPaperWidth= GetProcAddress(hLibrary,"SetPaperWidth")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnGetPaperWidth= GetProcAddress(hLibrary,"GetPaperWidth")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnSetPaperLength= GetProcAddress(hLibrary,"SetPaperLength")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnGetPaperLength= GetProcAddress(hLibrary,"GetPaperLength")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnGetOrientation= GetProcAddress(hLibrary,"GetOrientation")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnSetOrientation= GetProcAddress(hLibrary,"SetOrientation")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnRestoreDefaultPrinter= GetProcAddress(hLibrary,"RestoreDefaultPrinter")) == NULL)
			return(ERR_LOAD_FUNC);
		//Function long SendMail(String Toadd, String CCadd, String BCCadd, String Subject, StringMessage, String FileNames, Long Options) library "cdintf.dll" alias for "SendMail;Ansi"
		if ((lpfnLock = GetProcAddress(hLibrary,"Lock")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnUnlock = GetProcAddress(hLibrary,"Unlock")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnSetDocFileProps = GetProcAddress(hLibrary,"SetDocFileProps")) == NULL)
			return(ERR_LOAD_FUNC);
		// error message associated with error code
		if ((lpfnGetLastErrorMsg = GetProcAddress(hLibrary,"GetLastErrorMsg")) == NULL)
			return(ERR_LOAD_FUNC);
		if ((lpfnGetDocumentTitle = GetProcAddress(hLibrary,"GetDocumentTitle")) == NULL)
			return(ERR_LOAD_FUNC);
		// Functions needed to using the PDF Printer Developer edition version 2.1 to:
		// set the permanent license key for the product
		if ((lpfnSetLicenseKey = GetProcAddress(hLibrary,"SetLicenseKey")) == NULL)
			return(ERR_LOAD_FUNC);
		// Enable the printer before every print job
		if ((lpfnEnablePrinter = GetProcAddress(hLibrary,"EnablePrinter")) == NULL)
			return(ERR_LOAD_FUNC);
		  return(0);
}
/**************************** pdf_gsdll_callback ***************************************/
int  pdf_gsdll_callback(int message, char *str, unsigned long count)
    {
    char *p;
    char msg[200];

        switch (message) {
            case GSDLL_STDIN:
                p = fgets(str, count, stdin);
                if (p)
                    return strlen(str);
                else
                    return 0;
				break;
            case GSDLL_STDOUT:
			   // puts(str);
                if (str != (char *)NULL)
                    fwrite(str, 1, count, stdout);
                return count;
				break;
            case GSDLL_DEVICE:
                sprintf(msg,"Callback: DEVICE %p %s\n", str,
                    count ? "open" : "close");
                error_msg(msg);
                break;
            case GSDLL_SYNC:
                sprintf(msg,"Callback: SYNC %p\n", str);
                error_msg(msg);
                break;
            case GSDLL_PAGE:
                sprintf(msg,"Callback: PAGE %p\n", str);
                error_msg(msg);
                break;
            case GSDLL_SIZE:
                sprintf(msg,"Callback: SIZE %p width=%d height=%d\n", str,
                    (int)(count & 0xffff), (int)((count>>16) & 0xffff) );
                error_msg(msg);
                break;
            case GSDLL_POLL:
		return 0; /* no error */
            default:
                sprintf(msg,"Callback: Unknown message=%d\n",message);
                error_msg(msg);
                break;
        }
        return 0;
    }

/****************************  gsdll_callback ***************************************/

int  gsdll_callback(int message, char *str, unsigned long count)
    {
    char *p,*p2;
    int status;
    char str2[LINELEN];
    char msg[200];
    char *pre, *word, *post;
    int xl1,xl2,xr1,xr2,tab;
    PLINFO plist,plist_temp;
    int i,j,pos,found;
	int new_line;
    int llx, lly, urx, ury;
        switch (message) {
            case GSDLL_STDIN:
                 return 0;
            case GSDLL_STDOUT:
                if (str != (char *)NULL)
                  {
                 str[count] = 0;
                 strcat(save_str,str);
                 p = save_str;
                 while((p2 = strchr(p,'\n')) != NULL )
                     {
                     strncpy(str2,p,p2 - p);
                     str2[p2 - p + 1] = 0;
                     p = p2 + 1;
					 status = pstotextFilter(
								pstotextInstance, str2, &pre, &word, &post, &llx, &lly, &urx, &ury);
					 if (status!=0)
						  {
						  cleanup();
						  }
					 if (word!=NULL)
						  {
						  if(strlen(word) >= MAX_STRLEN)
					          word[MAX_STRLEN] = 0;
						  if(str2[0] == 'Q' && (str2[1] == 'S' || str2[1] == 'P'))
							 {
							  new_line = 1;
							  /* do we need a new line? */
							  if(linecount != -1)
							     for(i = 0;i <= linecount;++i)
							         {
									 plist = linelist[i];
									 if(abs(lly - plist->y) < 2)
									     {
										 new_line = 0;
										 break;
										 }
                                     }
							  /* is this the first string on the Page? */
                              if(prev_y == -1) /* Yes */
                                 {
                                // linelist[0] = (PLINFO)malloc(sizeof(LINFO));
                                 prev_y = lly;
								if(linecount == -1)
								  linecount = 0;
                                linelist[linecount] = (PLINFO)malloc(sizeof(LINFO));
                                linelist[linecount]->xr = urx;
                                linelist[linecount]->xc = (urx - llx)/2 +  llx;
                                linelist[linecount]->xl = llx;
                                linelist[linecount]->y = lly;
                                strcpy(linelist[linecount]->str,word);
                                linelist[linecount]->flag = 0;
                                linelist[linecount]->next = NULL;
                                 }
                              else if(new_line == 0) /* add New Line */
                                add_line(&linelist[i],llx,urx,lly,word);
                              else
                                {
                                linecount++;
                                prev_y = lly;
                                linelist[linecount] = (PLINFO)malloc(sizeof(LINFO));
                                linelist[linecount]->xr = urx;
                                linelist[linecount]->xc = (urx - llx)/2 +  llx;
                                linelist[linecount]->xl = llx;
                                linelist[linecount]->y = lly;
                                strcpy(linelist[linecount]->str,word);
                                linelist[linecount]->flag = 0;
                                linelist[linecount]->next = NULL;
                                }
							 }

						  }
                     if(str2[0] == 'Q' && str2[1] == 'P')
                              {
                              // find the first column
                              pos = 1;
                              found = 1;
                              page++;
                              while(found)
                                 {
                                  found = 0;
                                  xr1 = 0;//100000;
								  xl1 = 100000;
								  /* Define Column Width */
								  for(i = 0;i <= linecount;++i)
									 {
									 plist = linelist[i];
									 while(plist != NULL  && plist->flag != 0)
										 plist = plist->next;
									 if(plist == NULL)
										continue;
									 if(plist->xl < xl1 )
									    {
										xl1 = plist->xl;
										}
									 if(plist->xl <= (xl1 + 20) && plist->xr > xr1)
										xr1 = plist->xr;
									 found = 1;
									 }
								  if(found == 0)
									 break;
								  // matchup columns
								  prev_xr = 0;
								  /* Get All Text that fall within this column  */
								  for(i = 0;i <= linecount;++i)
									 {
									 plist = linelist[i];
									 while(plist != NULL  && plist->flag != 0)
										 plist = plist->next;
									 if(plist == NULL)
										continue;
									 if((plist->xl >= xl1 && plist->xl <= xr1) ||
									    (plist->xr >= xl1 && plist->xr <= xr1 ) ||
										 prev_xr  == plist->xr)
										 {
										 plist->flag=pos;
										 prev_xr = plist->xr;
										 }

									 }
                                  pos++;
                                 }
                               // output data
                              if(search_flag == 1)
                                  {
                                  if(search_pos  >= MAX_SEARCH)
                                     goto free_memory;
                                  for(i = 0;i <= linecount;++i)
										 {
										 plist = linelist[i];
										 while(plist != NULL )
											 {
											 if(findstring(plist->str,search_str,search_options) == TRUE)
                                                    {
                                                     searchlist[search_pos].page = page;
                                                     searchlist[search_pos].x = plist->xl;
                                                     searchlist[search_pos].y = plist->y;
                                                     searchlist[search_pos].w = plist->xr - plist->xl;
                                                     searchlist[search_pos].h = plist->y;
                                                     search_pos++;
                                                     if(search_pos  >= MAX_SEARCH)
                                                       goto free_memory;
                                                    }
											 plist = plist->next;
											 }

										 }
                                  }
                              else
                                  {
								  for(i = 0;i <= linecount;++i)
										 {
										 tab = 1;
										 plist = linelist[i];
										 while(plist != NULL )
											 {
											 for(j = tab;j < plist->flag;++j)
												 fprintf(unit,"\t");
											 fprintf(unit,"%s",plist->str);
											 //fprintf(unit," %d %d %d",plist->xl,plist->xr,plist->y);
											 tab = plist->flag;
											 plist = plist->next;

											 }
										 fprintf(unit,"\n");
										 }
                                  }
                              // free memory
free_memory:
                              for(i = 0;i <= linecount;++i)
									 {
									 plist = linelist[i];
									 while(plist != NULL )
                                         {
										 plist_temp = plist->next;
                                         free(plist);
                                         plist = NULL;
                                         plist = plist_temp;
                                         }
                                     //free(linelist[i]);
                                     linelist[i] = NULL;
									 }
                              linecount = -1;
							  prev_y = -1;
                              }
                    }
                   if(strlen(p) > 0 )
                      strcpy(save_str,p);
                   else
                     save_str[0] = 0;
                  }
                return count;
            case GSDLL_DEVICE:
                sprintf(msg,"Callback: DEVICE %p %s\n", str,
                    count ? "open" : "close");
                error_msg(msg);
                break;
            case GSDLL_SYNC:

                sprintf(msg,"Callback: SYNC %p\n", str);
                error_msg(msg);
                break;
            case GSDLL_PAGE:
                sprintf(msg,"Callback: PAGE %p\n", str);
                error_msg(msg);
                break;
            case GSDLL_SIZE:
                sprintf(msg,"Callback: SIZE %p width=%d height=%d\n", str,
                    (int)(count & 0xffff), (int)((count>>16) & 0xffff) );
                error_msg(msg);
                break;
            case GSDLL_POLL:
		        return 0; /* no error */
            default:
                sprintf(msg,"Callback: Unknown message=%d\n",message);
                error_msg(msg);
                break;
        }
        return 0;
    }


	/****************************  gsdll_callback ***************************************/

int  gsdll_callback8(int message, char *str, unsigned long count)
    {
    char *p,*p2,*p3,chr;
    int status;
    char str2[LINELEN];
    char msg[200];
	char width_str[10];
	long w;
    char *pre, word[4000], *post;
    long xl1,xl2,xr1,xr2,tab;
    PLINFO plist,plist_temp;
    int i,j,pos,found;
	int new_line;
    int llx, lly, urx, ury;
        switch (message) {
            case GSDLL_STDIN:
                 return 0;
            case GSDLL_STDOUT:
                if (str != (char *)NULL)
                  {
                 str[count] = 0;
				 // for testing fprintf(unit,"%s\n",str);
				 //return 0;
                 strcat(save_str,str);
                 p = save_str;
                 while((p2 = strchr(p,'\n')) != NULL )
                     {
                     strncpy(str2,p,p2 - p);
                     str2[p2 - p ] = 0;
                     p = p2 + 1;
					 if( str2[0] == 'S' )
						  {
						   //sscanf(str2,"%1s %d %d",&chr,&llx,&lly);
						   sscanf(str2,"%1s %d %d",&chr,&lly,&llx);
						   p3 = strchr(str2,'(');
						   p2 = strrchr(str2,')');
						   if( p3 == 0 || p2 == 0 )
						       continue;
						   p3++;
						   strncpy(word,p3,p2 - p3 );
						   word[p2 - p3] = 0;
						   strcpy(width_str,++p2);

						   w = atol(width_str) * .84;
						   if(strlen(word) >= MAX_STRLEN)
					          word[MAX_STRLEN] = 0;
						   new_line = 1;
						   /* do we need a new line? */
						   if(linecount != -1)
							  for(i = 0;i <= linecount;++i)
							     {
								 plist = linelist[i];
								 if(abs(lly - plist->y) < 45)
									 {
									 new_line = 0;
									 break;
									 }
                                 }
							  /* is this the first string on the Page? */
                              if(prev_y == -1) /* Yes */
                                 {
                                // linelist[0] = (PLINFO)malloc(sizeof(LINFO));
                                 prev_y = lly;
								if(linecount == -1)
								  linecount = 0;
                                linelist[linecount] = (PLINFO)malloc(sizeof(LINFO));
                                linelist[linecount]->xr = llx + w ;
                                linelist[linecount]->xc = (w/2 )+  llx;
                                linelist[linecount]->xl = llx;
                                linelist[linecount]->y = lly;
                                strcpy(linelist[linecount]->str,word);
                                linelist[linecount]->flag = 0;
                                linelist[linecount]->next = NULL;
                                 }
                              else if(new_line == 0) /* add New Line */
                                add_line(&linelist[i],llx,llx + w,lly,word);
                              else
                                {
                                linecount++;
                                prev_y = lly;
                                linelist[linecount] = (PLINFO)malloc(sizeof(LINFO));
                                linelist[linecount]->xr = llx + w ;
                                linelist[linecount]->xc = (w/2) +  llx;
                                linelist[linecount]->xl = llx;
                                linelist[linecount]->y = lly;
                                strcpy(linelist[linecount]->str,word);
                                linelist[linecount]->flag = 0;
                                linelist[linecount]->next = NULL;
                                }
							 }

                     if(str2[0] == 'P')
                              {
                              // find the first column
                              pos = 1;
                              found = 1;
                              page++;
                              while(found)
                                 {
                                  found = 0;
                                  xr1 = 0;//100000;
								  xl1 = 100000;
								  /* Define Column Width */
								  for(i = 0;i <= linecount;++i)
									 {
									 plist = linelist[i];
									 while(plist != NULL  && plist->flag != 0)
										 plist = plist->next;
									 if(plist == NULL)
										continue;
									 if(plist->xl < xl1 )
									    {
										xl1 = plist->xl;
										}
									 if(plist->xl <= (xl1 + 5) && plist->xr > xr1)
										xr1 = plist->xr;
									 found = 1;
									 }
								  if(found == 0)
									 break;
								  // matchup columns
								  prev_xr = 0;
								  /* Get All Text that fall within this column  */
								  for(i = 0;i <= linecount;++i)
									 {
									 plist = linelist[i];
									 while(plist != NULL  && plist->flag != 0)
										 plist = plist->next;
									 if(plist == NULL)
										continue;
									 if((plist->xl >= xl1 && plist->xl <= xr1) ||
									    (plist->xr >= xl1 && plist->xr <= xr1 ) ||
										 prev_xr  == plist->xr)
										 {
										 plist->flag=pos;
										 prev_xr = plist->xr;
										 }

									 }
                                  pos++;
                                 }
                               // output data
                              if(search_flag == 1)
                                  {
                                  if(search_pos  >= MAX_SEARCH)
                                     goto free_memory;
                                  for(i = 0;i <= linecount;++i)
										 {
										 plist = linelist[i];
										 while(plist != NULL )
											 {
											 if(findstring(plist->str,search_str,search_options) == TRUE)
                                                    {
                                                     searchlist[search_pos].page = page;
                                                     searchlist[search_pos].x = plist->xl;
                                                     searchlist[search_pos].y = plist->y;
                                                     searchlist[search_pos].w = plist->xr - plist->xl;
                                                     searchlist[search_pos].h = plist->y;
                                                     search_pos++;
                                                     if(search_pos  >= MAX_SEARCH)
                                                       goto free_memory;
                                                    }
											 plist = plist->next;
											 }

										 }
                                  }
                              else
                                  {
								  for(i = 0;i <= linecount;++i)
										 {
										 tab = 1;
										 plist = linelist[i];
										 while(plist != NULL )
											 {
											 for(j = tab;j < plist->flag;++j)
												 fprintf(unit,"\t");
											 fprintf(unit,"%s",plist->str);
											 //fprintf(unit," %d %d %d",plist->xl,plist->xr,plist->y);
											 tab = plist->flag;
											 plist = plist->next;

											 }
										 fprintf(unit,"\n");
										 }
                                  }
                              // free memory
free_memory:
                              for(i = 0;i <= linecount;++i)
									 {
									 plist = linelist[i];
									 while(plist != NULL )
                                         {
										 plist_temp = plist->next;
                                         free(plist);
                                         plist = NULL;
                                         plist = plist_temp;
                                         }
                                     //free(linelist[i]);
                                     linelist[i] = NULL;
									 }
                              linecount = -1;
							  prev_y = -1;
                              }
                    }
                   if(strlen(p) > 0 )
                      strcpy(save_str,p);
                   else
                     save_str[0] = 0;
                  }
                return count;
            case GSDLL_DEVICE:
                sprintf(msg,"Callback: DEVICE %p %s\n", str,
                    count ? "open" : "close");
                error_msg(msg);
                break;
            case GSDLL_SYNC:

                sprintf(msg,"Callback: SYNC %p\n", str);
                error_msg(msg);
                break;
            case GSDLL_PAGE:
                sprintf(msg,"Callback: PAGE %p\n", str);
                error_msg(msg);
                break;
            case GSDLL_SIZE:
                sprintf(msg,"Callback: SIZE %p width=%d height=%d\n", str,
                    (int)(count & 0xffff), (int)((count>>16) & 0xffff) );
                error_msg(msg);
                break;
            case GSDLL_POLL:
		        return 0; /* no error */
            default:
                sprintf(msg,"Callback: Unknown message=%d\n",message);
                error_msg(msg);
                break;
        }
        return 0;
    }

/**************************** add_line  ***************************************/

add_line(PLINFO *pline,int xl,int xr,int y,char *str)
   {
	int xc = (xr - xl)/2 + xl;
    PLINFO temp_pline,pline_old,pline_org;
	PLINFO newline ;
    char temp_str[LINELEN];
	char temp_str2[LINELEN];
    pline_org = (*pline);
    pline_old = NULL;
    while(pline_org != NULL)
        {
        if((xl - pline_org->xr) < 5 && xl > pline_org->xr )
            {
			if((strlen(pline_org->str) + strlen(str) )>= MAX_STRLEN)
			    {
				strcpy(temp_str,pline_org->str);
                strcat(temp_str," ");
			    strcat(temp_str,str);
                temp_str[MAX_STRLEN] = 0;
                strcpy(pline_org->str,temp_str);
				}
			else
				{
                strcat(pline_org->str," ");
			    strcat(pline_org->str,str);
				}
			pline_org->xr = xr;
			return;
            }
        if((pline_org->xl - xr) < 5 && pline_org->xl > xr)
            {
			strcpy(temp_str,pline_org->str);
			if((strlen(temp_str) + strlen(str) )>= MAX_STRLEN)
			    {
				strcpy(temp_str2,str);
				strcat(temp_str2," ");
				strcat(temp_str2,temp_str);
                temp_str2[MAX_STRLEN] = 0;
                strcpy(pline_org->str,temp_str2);
				}
			else
			    {
                strcpy(pline_org->str,str);
				strcat(pline_org->str," ");
				strcat(pline_org->str,temp_str);
				}
			pline_org->xl = xl;
			return;
            }
        if(pline_org->xr > xr)
            {
            newline = (PLINFO)malloc(sizeof(LINFO));
			newline->xr = xr;
			newline->xc = (xr - xl)/2 +  xl;
			newline->xl = xl;
			newline->y = y;
			strcpy(newline->str,str);
			newline->flag = 0;
			newline->next = NULL;
            if(pline_old != NULL)
               {
               pline_old->next = newline;
               newline->next = pline_org;
               return;
               }
            (*pline) = newline;
            newline->next = pline_org;
            return;
            }
         pline_old = pline_org;
         pline_org = pline_org->next;
         }
    if((xl - pline_old->xr) < 5)  // must be same line
        {
		if((strlen(pline_old->str) + strlen(str) )>= MAX_STRLEN)
		    {
			strcpy(temp_str,pline_old->str);
            strcat(temp_str," ");
			strcat(temp_str,str);
            temp_str[MAX_STRLEN] = 0;
            strcpy(pline_old->str,temp_str);
		    }
		else
		   {
           strcat(pline_old->str," ");
           strcat(pline_old->str,str);
		   }
        pline_old->xr = xr;
        return;
        }
	newline = (PLINFO)malloc(sizeof(LINFO));
	newline->xr = xr;
	newline->xc = (xr - xl)/2 +  xl;
    newline->xl = xl;
	newline->y = y;
	strcpy(newline->str,str);
	newline->flag = 0;
	newline->next = NULL;
    pline_old->next = newline;
   }


/**************************** send_prolog ***************************************/

void send_prolog(FILE *f, int resource)
{
    HGLOBAL hglobal;
    LPSTR prolog;
    hglobal = LoadResource(pstotextModule,
	FindResource(pstotextModule, (LPSTR)resource, RT_RCDATA));
    if ( (prolog = (LPSTR)LockResource(hglobal)) != (LPSTR)NULL) {
	fputs(prolog, f);
	FreeResource(hglobal);
    }
}

/**************************** scratch_file ***************************************/


/* create an empty temporary file and return its name */
static char *scratch_file(void) {
    FILE *f;
    char *temp;
    char *path = malloc(256);
    if (path == NULL)
	return NULL;
    if ( (temp = getenv("TEMP")) != NULL )
	strcpy(path, temp);
    else if ( (temp = getenv("TMP")) != NULL )
	strcpy(path, temp);
    else
	strcpy(path, "c:\\");

    /* Prevent X's in path from being converted by mktemp. */
    for ( temp = path; *temp; temp++ ) {
	    *temp = (char)tolower(*temp);
	    if (*temp == '/')
		*temp = '\\';
    }
    if ( strlen(path) && (path[strlen(path)-1] != '\\') )
	    strcat(path, "\\");

    strcat(path, "ptXXXXXX");
    mktemp(path);
    f = fopen(path, "w");
    if (f==NULL) {perror(cmd); return(-1);}
    fclose(f);
    return path;
}

/**************************** make_temp ***************************************/


static char *make_temp(int resource) {
  /* Return pathname of temporary file containing prolog from resources.  Caller
     should unlink file (and, technically, free pathname). */
    FILE *f;
    char *path = scratch_file();
    if (path == NULL) {perror(cmd); cleanup(); return(-1);}

    f = fopen(path, "w");
    if (f==NULL) {perror(cmd); cleanup(); return(-1);}

    send_prolog(f, resource);
    fclose(f);
    return path;
}


static char *ocr_path = NULL, *rotate_path = NULL;
static FILE *gs = NULL;
char *gstemp = NULL;
static void *instance; /* pstotext state */

/**************************** cleanup ***************************************/

static int cleanup(void)
  {
  int gsstatus, status = 0;
  if (rotate_path!=NULL & strcmp(rotate_path, "")!=0)
       status = unlink(rotate_path);
  if (ocr_path!=NULL)
       status = unlink(ocr_path);
  return status;
  }

/**************************** handler ***************************************/

static void handler()
  {
  int status = cleanup();
  if (status!=0)
    return(status);
  return(2);
  }

/**************************** PSInterface ***************************************/

#ifdef DLL
 long WINAPI  PSInterface(char *dir,char *infile,char *outfile,int type, int orientation,
         PSEARCH_INFO psearch_info)
   {

#else
main( int argc,  char *argv[])
  {
  char outfile[200] = {"c:\\temp\\rep.pdf"};
  int type;
  PSEARCH_INFO psearch_info;
  char dir[200];
  char infile[200];

#endif
  extern errno;
  /* If "path" is NULL, then "stdin" should be processed. */
  char gs_cmd[2*MAXPATHLEN];
  char input[MAXPATHLEN];
  int status;
  char *gsargv[18],*p;
  int gsargc,i,j;
  int code;
  long size;
  char product[200];
  char copyright[200];
  long gs_revision;
  long gs_revisiondate;
  char buf[2000];
  char str[200];
  char msg[200];
  int len;
  char path[200];
  char dllfile[400];
  char gslib[400];
  HMODULE hMod;
  int text_opt = 0;
  int inchx,inchy;
  struct stat sbuf;
  char *lpLicenseKey = "64XSD4234455P41WET68";
  char szCommandLine[20480];
  char rotate[200];
    // AmyuniPDF AmyuniPDF Variables
  char *LicensedTo ="Evaluation Version Developer Pro";
  char *ActivationCode ="07EFCDAB010001001A62D63A28234ADFD28E5C773A417CBCF622CE045AA431E6CCCFA12932B31B851AAA7C7E11A178A8AF883B10998560A0650A5F27E42FDCD3BADC4C704277F8DC2876EE961D134CE69A4F19730A30176CF265BA20";
  long pdfPrinter = 0;

#ifndef DLL
strcpy(dir,"d:\\ghost5.5\\gs5.50\\obj");
strcpy(outfile,"c:\\temp\\x.txt");
strcpy(infile,"c:\\temp\\pstemp.ps");
text_opt= 1;
search_options = 0;

#endif

if(type == AmyuniPDF_TYPE)
	{
     sprintf(AMYUNIDLL,"%s\\ps2pdfsdk.dll",dir);
	  if(InitAmyuniPDF() == -1)
		 {
		 GetErrorMsg(msg);
		 sprintf(buf,"Failed to load library AmyuniPDF DLL because %s\r\n",msg);
		 error_msg(buf);
		 return -1;
		 }
    pdfPrinter = ( *lpfnDriverInit)("ppc_ps2");
	status =  ( *lpfnEnablePrinter)(pdfPrinter,LicensedTo,ActivationCode);
	if( status == NULL)
		{
		 sprintf(buf,"Failed to enable the printer");
		 error_msg(buf);
		 return -1;
		}

	if(orientation != 0 )
		  ( *lpfnSetOrientation)(pdfPrinter,orientation);

	( *lpfnSetDefaultPrinter)(pdfPrinter);
	( *lpfnRestoreDefaultPrinter)(pdfPrinter);

	}
if(type == VERYPDF_TYPE )
	{
	  code =  _stat(infile,&sbuf);
	  if(code != 0 )
		{
		 sprintf(buf,"Failed get file stats on %s code = %d\r\n",infile,errno);
		 error_msg(buf);
		 return -1;
		 }
	  if(sbuf.st_size < 100)
		{
		 sprintf(buf,"File size is too small. File Size = %d\r\n",sbuf.st_size);
		 error_msg(buf);
		 return -1;
		 }
	  switch (orientation)
		  {
		  case 0: // undefined
			  strcpy(rotate,"  " );
			  break;
		  case 1: // landscape
			  strcpy(rotate," -rotate 90 " );
			  break;
		  case 2: // portrait
			  strcpy(rotate," ");;
			  break;
		  default:
			  strcpy(rotate," ");;
			  break;
		  }
      sprintf(VERYDLL,"%s\\ps2pdfsdk.dll",dir);
	  // Not Used any more
	  sprintf(DYNAPDFDLL,"%s\\dynapdf.dll",dir);
	  if(InitVeryPDF() == -1)
		 {
		 GetErrorMsg(msg);
		 sprintf(VERYDLL,"ps2pdfsdk.dll");
	      // Not Used any more
	     sprintf(DYNAPDFDLL,"dynapdf.dll");
	     if(InitVeryPDF() == -1)
	        {
			 GetErrorMsg(msg);
			 sprintf(buf,"Failed to load library VeryPDF DLL because %s\\%s %s\r\n",dir,VERYDLL,msg);
			 error_msg(buf);
			 return -1;
			 }
		 }

     sprintf(szCommandLine,"ps2pdf -$ \"%s\" -mode 0 %s \"%s\"  \"%s\" ",lpLicenseKey,rotate,infile,outfile);

     code = ( *lpfnVeryPDF_PSToPDF)(szCommandLine);
	 if(code != 0)
		 {
		 sprintf(msg,"Error running VeryPDF code = %d\n",code);
         error_msg(msg);
		}
     return code;
	}
    if(type == VERYPDF_MERGE_TYPE )
	{
	 /* code =  _stat(infile,&sbuf);
	  if(code != 0 )
		{
		 sprintf(buf,"Failed get file stats on %s code = %d\r\n",infile,errno);
		 error_msg(buf);
		 return -1;
		 }
	  if(sbuf.st_size < 100)
		{
		 sprintf(buf,"File size is too small. File Size = %d\r\n",sbuf.st_size);
		 error_msg(buf);
		 return -1;
		 } */
      sprintf(VERYDLL,"%s\\ps2pdfsdk.dll",dir);
	  sprintf(DYNAPDFDLL,"%s\\dynapdf.dll",dir);
	  if(InitVeryPDF() == -1)
		 {
		 GetErrorMsg(msg);
		 sprintf(buf,"Failed to load library VeryPDF DLL because %s %s\r\n",VERYDLL,msg);
		 error_msg(buf);
		 return -1;
		 }

     sprintf(szCommandLine,"ps2pdf -$ \"%s\" \"%s\" -mergepdf \"%s\" ",lpLicenseKey,outfile,infile);

     code = ( *lpfnVeryPDF_PSToPDF)(szCommandLine);
	 //code = VeryPDF_PSToPDF(szCommandLine);
	 if(code != 0)
		 {
		 sprintf(msg,"Error running VeryPDF code = %d\n",code);
         error_msg(msg);
		}
     return code;
	}

  //strcpy(path,argv[1]);
   if(type == SEARCH_TYPE)
      {
      search_options = psearch_info->option;
      }
   search_options = 0;
   if(type == TEXT_TYPE)
      {
      text_opt = 1;
      search_flag = 0;
      }
   else if(type == SEARCH_TYPE)
      {
      search_flag = 1;
      text_opt = 0;
      strcpy(search_str,psearch_info->str);
	  }
  //search_options= 0;
  search_pos = 0;
  page = 0;
  prev_y = -1;

  signal(SIGINT, handler);
  signal(SIGTERM, handler);

  code =  _stat(infile,&sbuf);
  if(code != 0 )
	{
	 sprintf(buf,"Failed get file stats on %s code = %d\r\n",infile,errno);
     error_msg(buf);
     return -1;
     }
  if(sbuf.st_size < 100)
	{
	 sprintf(buf,"File size is too small. File Size = %d\r\n",sbuf.st_size);
     error_msg(buf);
     return -1;
     }

  pstotextInit(&pstotextInstance);


  sprintf(gslib,"-I%s\\fonts\\;%s\\",dir,dir);
  sprintf(GSDLL,"%s\\gsdll32.dll",dir);
  //sprintf(GSDLL,"gsdll32.dll");
  if(InitGS() == -1)
     {
     GetErrorMsg(msg);
     sprintf(buf,"Failed to load library  GS32DLL because %s\r\n",msg);
     error_msg(buf);
     return -1;
     }
  i = 0;

  //getdevice(&inchx,&inchy);

  code = ( *lpfngsdll_revision)(&product, &copyright,
                           &gs_revision, &gs_revisiondate);
  if(gs_revision < 8)
     ocr_path = make_temp(OCR_PROLOG);
  else
     ocr_path = make_temp(OCR_PROLOG8);
  switch (orientation)
      {
	  case 1:  rotate_path = "";
		  break; //landscape:
	  case 0: rotate_path = "";
		  break; //undefined:
	  case 2: rotate_path = make_temp(ROT270_PROLOG);
		  break; //make_temp(ROT90_PROLOG); break; //port
      }
  gsargv[i++] = "gswin32c.exe";
 // gsargv[i++] = gslib;
  if(text_opt || search_flag)
     {
     if(search_flag == 0)
         {
         unit = fopen(outfile,"w+");
		 if(unit == NULL)
			 {
			 sprintf(msg,"Error opening %s code = %d",outfile,errno);
			 error_msg(msg);
			 return -1;
			 }
          }
	  strcpy(path,infile);
	  gsargv[i++] = "-r72";
	  gsargv[i++] = "-dNODISPLAY";
	 // gsargv[i++] = "-dBATCH";
	  gsargv[i++] = "-dNOPROMPT";
	  if(gs_revision < 8)
		gsargv[i++] = "-dSIMPLE";
	  gsargv[i++] = "-dWRITESYSTEMDICT";
	  gsargv[i++] = "-dNOPAUSE";
	  sprintf(str,"-sOutputFile#%s",outfile);
	  gsargv[i++] = str;
	  gsargv[i++] = "-q";
	  //if(rotate_path[0] != 0)
	  //	 gsargv[i++] = rotate_path;
	  gsargv[i++] = ocr_path;
	  gsargv[i++] = "-f";
	  gsargv[i++] = path;
      gsargv[i] = NULL;
	  gsargc = i;
	  prev_y = -1;
	  linecount = -1;
      if(gs_revision < 8)
		code = (*lpfngsdll_init)(gsdll_callback, NULL, gsargc, gsargv);
	  else
        code = (*lpfngsdll_init)(gsdll_callback8, NULL, gsargc, gsargv);
      }
  else
      {
	  unit = fopen(outfile,"w+");
	  if(unit == NULL)
	     {
		 sprintf(msg,"Error opening %s code = %d",outfile,errno);
		 error_msg(msg);
	     return -1;
         }
	  fclose(unit);
	   if(gs_revision > 7)
	    gsargv[i++] = "-dSAFER";
      gsargv[i++] = "-sDEVICE#pdfwrite";
	  /*gsargv[i++] = "-dBATCH"; */
	  gsargv[i++] = "-dNOPAUSE";

      gsargv[i++] = "-q";
      sprintf(str,"-sOutputFile#%s",outfile);
	  gsargv[i++] = str;
      if(rotate_path[0] != 0)
         {
         gsargv[i++] = "-f";
		 gsargv[i++] = rotate_path;
         }
	  gsargv[i++] = "-f";
	   if(gs_revision > 7)
	   {
		//gsargv[i++] = "-c 4000000 setvmthreshold -f";
		gsargv[i++] = "-c";
		gsargv[i++] = "10000000";
		gsargv[i++] = "setvmthreshold";
		gsargv[i++] = "-f";
		}
	  gsargv[i++] = infile;
	  gsargv[i] = NULL;
	  gsargc = i;
	  prev_y = -1;
	  linecount = -1;
	  code = (*lpfngsdll_init)(pdf_gsdll_callback, NULL, gsargc, gsargv);
      }



  code = (*lpfngsdll_execute_begin)();
  if (code==0)
        {

	    code = (*lpfngsdll_execute_end)();

	    //fprintf(stdout,"gsdll_exit returns %d\n", code);
	    }
  code = (*lpfngsdll_exit)();
  FreeLibrary( hLibrary);


 /* code = stat(outfile,&sbuf);
  while(sbuf.st_size  > 0 && code == 0)
	  {
       size = sbuf.st_size;
       Sleep(500);
	   code = stat(outfile,&sbuf);
	   if( size == sbuf.st_size)
		   goto finished
	  }

finished:
*/


  if(text_opt)
    fclose(unit);
  if(search_flag)
    {
    for(i = 0;i < search_pos;++i)
      {
		psearch_info->searchlist[i].page = searchlist[i].page;
		psearch_info->searchlist[i].x = (searchlist[i].x * 1000) /72;
		if(orientation == landscape)
		  psearch_info->searchlist[i].y = ((PAGE_WIDTH_LAND -  searchlist[i].y) * 1000) /72;
		else
          psearch_info->searchlist[i].y = ((PAGE_WIDTH_PORT -  searchlist[i].y) * 1000) /72;
		psearch_info->searchlist[i].h = (searchlist[i].h * 1000) /72;
		psearch_info->searchlist[i].w = (searchlist[i].w * 1000) /72;
      }
    psearch_info->count = search_pos;
    }
  status = cleanup();


  if (status!=0) return(status);
}

/**************************** error_msg ***************************************/

void error_msg(char *msg)
  {
//   MessageBox(0,msg,"PostScript Converting",MB_OK);
    char path[1024]; // = malloc(256);
    char *temp;
    FILE *unit;
    if (path == NULL)
	return NULL;
    if ( (temp = getenv("TEMP")) != NULL )
		strcpy(path, temp);
    else if ( (temp = getenv("TMP")) != NULL )
		strcpy(path, temp);
    else
		strcpy(path, "c:\\");

    strcat(path, "\\pserror.txt");
    unit = fopen(path,"w+");
	if(unit == NULL)
		 {
		 //sprintf(msg,"Error opening %s code = %d",outfile,errno);
		// error_msg(msg);
		 return -1;
		 }
     fwrite(msg,1,strlen(msg),unit);
     fclose(unit);
      }



/**************************** findstring ***************************************/

int findstring(char *line,char *search_str,int options)
{
  int len,i,line_len,status;
  char *p,*p2;
  char type_str[100];
  char line_str[10];
  char line_lower[LINELEN];
  len = strlen(search_str);
  p = line;
  if(options & OPT_MATCH_CASE)
     {
	  while((p = strchr(p,search_str[0])) != NULL)
		  {
		  p2 = p;
		  for(i = 0;i < len && p2[i] != 0;++i)
			  if(p2[i] != search_str[i])
				break;
		  if(i == len)
			  return TRUE;
		  p++;
		  }
	   }
	else
     {
	  line_len = strlen(line);
	  for(i = 0;i < line_len;++i)
	      line_lower[i] = tolower(p[i]);
	  p = line_lower;
	  line_lower[line_len] = 0;
	  while((p = strchr(p,tolower(search_str[0]))) != NULL)
		  {
		  p2 = p;
		  for(i = 0;i < len && p2[i] != 0;++i)
			  if(p2[i] != tolower(search_str[i]))
				break;
		  if(i == len)
		      {

			  return TRUE; // found the string
			  }
		  p++;
		  }
	   }
  return 0; // did not find the string
}

int getdevice(int *inchx,int *inchy)
{
char device[200];
char port[100];
char printer[400];
char *p,*p2;
HANDLE hdc;
int len;
  hdc = CreateDC( "DISPLAY",NULL,NULL,NULL);
  *inchx = GetDeviceCaps(hdc,LOGPIXELSX);
  *inchy = GetDeviceCaps(hdc,LOGPIXELSY);
  DeleteDC(hdc);
  return 0;
}

long WINAPI  GetPrinterOrientation()
  {
  char str[800],*p,*p2;
  long num,status;
  HANDLE hwnd;
  PRINTER_INFO_2 *printer_info;
  char printer[800],drv[400],port[400];
  num = GetProfileString( "Windows","device","",str,200);
  if(num < 1)
     return -1;
  if(num > 0 )
     {
     p = strchr(str,',');
     if(p != NULL)
        {
        strncpy(printer,str,p - str);
        printer[p - str ] = 0;
        p++;
		p2 = strchr(p,',');
		if(p2 != NULL)
			{
            strncpy(drv,p,p2 - p);
            drv[p2 - p ] = 0;
            p2++;
            strcpy(port,p2);
            }
        }
     status = OpenPrinter( printer, &hwnd,NULL);
     if(status == 0)
        return -1;
     status = GetPrinter(hwnd,2,NULL,0,&num);
     if(status == 0)
        {
        status = GetLastError();
        if(status == ERROR_INSUFFICIENT_BUFFER )
            {
             printer_info = malloc(num);
             if(printer_info == NULL)
                {
                ClosePrinter(hwnd);
                return - 1;
                }
             status = GetPrinter(hwnd,2,printer_info,num,&num);
             ClosePrinter(hwnd);
             if(status > 0)
                 {
                 status = printer_info->pDevMode->dmOrientation;
                 free(printer_info);
                 return status;
                  }
             free(printer_info);
             }
         }
    }
  return -1;
 }


long WINAPI  GetPrintDef(int *orientation,int *postscript )
  {
  char str[800],*p,*p2;
  long num,status;
  HANDLE hwnd;
  PRINTER_INFO_2 *printer_info;
  char printer[1000],drv[1000],port[1000];
  num = GetProfileString( "Windows","device","",str,400);
  if(num < 1)
     return -1;
  if(num > 0 )
     {
     p = strchr(str,',');
     if(p != NULL)
        {
        strncpy(printer,str,p - str);
        printer[p - str ] = 0;
        p++;
		p2 = strchr(p,',');
		if(p2 != NULL)
			{
            strncpy(drv,p,p2 - p);
            drv[p2 - p ] = 0;
            p2++;
            strcpy(port,p2);
            }
		else
		 return -1;
        }
	  else
	    return -1;
     status = OpenPrinter( printer, &hwnd,NULL);
     if(status == 0)
        return -1;
     status = GetPrinter(hwnd,2,NULL,0,&num);
     if(status == 0)
        {
        status = GetLastError();
        if(status == ERROR_INSUFFICIENT_BUFFER )
            {
             printer_info = malloc(num);
             if(printer_info == NULL)
                {
                ClosePrinter(hwnd);
                return - 1;
                }
             status = GetPrinter(hwnd,2,printer_info,num,&num);
             ClosePrinter(hwnd);
             if(status > 0)
                 {
                 status = printer_info->pDevMode->dmOrientation;
				 if(printer_info->pDevMode->dmTTOption == DMTT_SUBDEV)
				      *postscript=1;
				 else
				      *postscript=0;
				 *orientation = status;
                 free(printer_info);
                 return 1;
                  }
             free(printer_info);
             }
         }
    }
  return -1;
 }

long WINAPI  GetPostscriptPrinter(char *name,char *drv,char *port)
{
long status,needed,count,i;
PRINTER_INFO_2 *printer_info2;
// try local printers first

status = EnumPrinters( PRINTER_ENUM_LOCAL , NULL ,2,NULL,0,&needed,&count);
if(status == 0)
        {
        status = GetLastError();
        if(status == ERROR_INSUFFICIENT_BUFFER )
            {
             printer_info2 = malloc(needed);
             if(printer_info2 == NULL)
                return - 1;
			 status = EnumPrinters( PRINTER_ENUM_LOCAL , NULL ,2,printer_info2,needed,&needed,&count);
			 for(i = 0;i < count;++i)
			    {
				if(printer_info2[i].pDevMode != NULL)
					{
				//	if(strcmp(printer_info2[i].pDevMode->dmDeviceName,"pscript.dll") == 0)
 					if(printer_info2[i].pDevMode->dmTTOption == DMTT_SUBDEV )// must be a postscipt printer
						{//printer_info2[i].pPrinterName
						 strcpy(name,printer_info2[i].pPrinterName);
						 strcpy(drv,printer_info2[i].pPrintProcessor);
						 strcpy(port,printer_info2[i].pPortName);
						 free(printer_info2);
						 return 1;
						 }
					}
				 }
			 free(printer_info2);
			 }
		else
		  return -1;
		}

	/*status = EnumPrinters( PRINTER_ENUM_CONNECTIONS , NULL ,2,NULL,0,&needed,&count);
	if(status == 0)
			{
			status = GetLastError();
			if(status == ERROR_INSUFFICIENT_BUFFER )
				{
				 printer_info2 = malloc(needed);
				 if(printer_info2 == NULL)
					return - 1;
				 status = EnumPrinters( PRINTER_ENUM_CONNECTIONS , NULL ,2,printer_info2,needed,&needed,&count);
				 for(i = 0;i < count;++i)
					{
					if(printer_info2[i].pDevMode->dmTTOption == DMTT_SUBDEV )// must be a postscipt printer
						{
						 strcpy(name,printer_info2[i].pDriverName);
						 strcpy(drv,printer_info2[i].pPrintProcessor);
						 strcpy(port,printer_info2[i].pPortName);
						 free(printer_info2);
						 return 1;
						 }
					 }
				 free(printer_info2);
				 }
			else
			  return -1;
			} */
return -1;
}

typedef struct s_printer_list
       {
       char name[500];
       struct s_printer_list *next;
       } PRINTER_LIST,*PPRINTER_LIST;
int get_printers(char *name,PPRINTER_LIST *pprinters);
long WINAPI   AddPostPrinter( char *name,char *ptr,char *drv,char *port);
int free_list(PPRINTER_LIST *pprinters);
int GetErrorMsg(char *msg);

long WINAPI  FindAddPostPrinter(char *ptr,char *drv,char *port)
{
long status,needed,count,i;
PRINTER_INFO_2 *printer_info2;
PRINTER_INFO_1 *printer_info1;
PPRINTER_LIST pprinters,pprinters_temp;
char name[1000];
int type;
DWORD ver;

pprinters = NULL;
ver = GetVersion();
//if(HIWORD(ver) > 0x80000000 && LOWORD(ver) > 3)
  return -1;
//  type = PRINTER_ENUM_NAME; // Window95 does not work here
//else
type = PRINTER_ENUM_REMOTE;

status = EnumPrinters(type   , NULL ,1,NULL,0,&needed,&count);
if(status == 0)
        {
        status = GetLastError();
        if(status == ERROR_INSUFFICIENT_BUFFER )
            {
             printer_info1 = malloc(needed);
             if(printer_info1 == NULL)
                  return - 1;
			 status = EnumPrinters( type , NULL ,1,printer_info1,needed,&needed,&count);
			 for(i = 0;i < count;++i)
					{
                    if(printer_info1[i].Flags & PRINTER_ENUM_ICON8 || printer_info1[i].Flags & PRINTER_ENUM_NAME)
                        {
                        if((status = AddPostPrinter(printer_info1[i].pName,ptr,drv,port)) == 1)
                           return 1;
                        if(status == -2)
						   return -1;
                        }
                     else
                        if(printer_info1[i].Flags & PRINTER_ENUM_ICON3)
                           {
                           sprintf(name,"\\\\%s",printer_info1[i].pDescription);
                           get_printers(name ,&pprinters);
                           }
                         else
                           get_printers(printer_info1[i].pName ,&pprinters);
                     }

              free(printer_info1);
              pprinters_temp = pprinters;
              while(pprinters_temp != NULL)
                   {
                    if(AddPostPrinter(pprinters_temp->name,ptr,drv,port) == 1)
                        {
                        free_list(&pprinters);
                        return 1;
                        }
                   pprinters_temp = pprinters_temp->next;
                   }
			  if(pprinters != NULL)
                 free(&pprinters);
			 }
		else
		  return -1;
		}
return -1;
}

long WINAPI AddPostPrinter( char *name,char *ptr,char *drv,char *port)
{
char str[800],*p,*p2;
char msg[800],msg2[800];
  long num,status,ret;
  HANDLE hwnd;
  PRINTER_INFO_2 *printer_info2;

 status = OpenPrinter( name, &hwnd,NULL);
 if(status == 0)
    return -1;
 status = GetPrinter(hwnd,2,NULL,0,&num);
 if(status == 0)
    {
    status = GetLastError();
    if(status == ERROR_INSUFFICIENT_BUFFER )
        {
         printer_info2 = malloc(num);
         if(printer_info2 == NULL)
            {
            ClosePrinter(hwnd);
            return - 1;
            }
         status = GetPrinter(hwnd,2,printer_info2,num,&num);
         ClosePrinter(hwnd);
         if(status > 0)
             {
//			 if(strcmp(printer_info2->pDevMode->dmDeviceName,"pscript.dll") == 0)
			 if(printer_info2->pDevMode->dmTTOption == DMTT_SUBDEV)
                {
			    status = AddPrinter(NULL, 2,printer_info2 );
				if(status == NULL)
				   {
                   GetErrorMsg(msg);
                   sprintf(msg2,"Fail to add printer %s because %s\r\n    Press \r\n    YES to search for another printer or\r\n     No to stop the search.",printer_info2->pDriverName,msg);
                   ret = MessageBox(0,msg2,"PostScript Converting",MB_YESNO);
				   if(ret == IDNO )
				      return -2;
				   return -1;
				   }
				strcpy(ptr,printer_info2->pPrinterName);
				strcpy(drv,printer_info2->pPrintProcessor);
			    strcpy(port,printer_info2->pPortName);
				free(printer_info2);
				return 1;
                }
             free(printer_info2);
             return -1;
              }
         free(printer_info2);
     }
  }
return -1;
}

long WINAPI GetPrinterInfo( char *name,char *ptr,char *drv,char *port)
{
char str[800],*p,*p2;
char msg[800],msg2[800];
  long num,status,ret;
  HANDLE hwnd;
  PRINTER_INFO_2 *printer_info2;

 status = OpenPrinter( name, &hwnd,NULL);
 if(status == 0)
    return -1;
 status = GetPrinter(hwnd,2,NULL,0,&num);
 if(status == 0)
    {
    status = GetLastError();
    if(status == ERROR_INSUFFICIENT_BUFFER )
        {
         printer_info2 = malloc(num);
         if(printer_info2 == NULL)
            {
            ClosePrinter(hwnd);
            return - 1;
            }
         status = GetPrinter(hwnd,2,printer_info2,num,&num);
         ClosePrinter(hwnd);
         if(status > 0)
                {
				strcpy(ptr, printer_info2->pPrinterName);
				strcpy(drv,printer_info2->pPrintProcessor);
			    strcpy(port,printer_info2->pPortName);
				free(printer_info2);
				return 1;
                }
         free(printer_info2);
     }
  }
return -1;
}
int get_printers(char *name,PPRINTER_LIST *pprinters)
 {
 long status,needed,count,i;
 PRINTER_INFO_1 *printer_info1;
 PPRINTER_LIST *p;
 status = EnumPrinters(PRINTER_ENUM_NAME , name ,1,NULL,0,&needed,&count);
 if(status == 0)
      {
      status = GetLastError();
      if(status == ERROR_INSUFFICIENT_BUFFER )
           {
           printer_info1 = malloc(needed);
           if(printer_info1 == NULL)
                return - 1;
		   status = EnumPrinters(PRINTER_ENUM_NAME , name ,1,printer_info1,needed,&needed,&count);
           p = pprinters;
		   for(i = 0;i < count;++i)
			    {
                if(printer_info1[i].Flags == PRINTER_ENUM_ICON8)
                       {

                        while(*p != NULL)
                           *p = (*p)->next;
                        *p = (PPRINTER_LIST)malloc(sizeof(PRINTER_LIST));
                        (*p)->next = NULL;
                        sprintf((*p)->name,"%s",printer_info1[i].pName);
                        }
                     else
                        {
                        get_printers(printer_info1[i].pName,pprinters);
                        }
				 }
			 free(printer_info1);
			 }
		else
		  return 1;
		}
return 1;
}

long WINAPI ListPrinters(char *name,char *pprinters[] ,long pos,long max_printers)
 {
 long status,needed,count,i;
 PRINTER_INFO_1 *printer_info1;
 PPRINTER_LIST *p;
 status = EnumPrinters(PRINTER_ENUM_CONNECTIONS |PRINTER_ENUM_LOCAL , NULL ,1,NULL,0,&needed,&count);
 if(status == 0)
      {
      status = GetLastError();
      if(status == ERROR_INSUFFICIENT_BUFFER )
           {
           printer_info1 = malloc(needed);
           if(printer_info1 == NULL)
                return - 1;
		   status = EnumPrinters(PRINTER_ENUM_CONNECTIONS |PRINTER_ENUM_LOCAL , name ,1,printer_info1,needed,&needed,&count);
           p = pprinters;
		   for(i = 0;i < count && i < max_printers;++i)
			    {
                if(printer_info1[i].Flags == PRINTER_ENUM_ICON8)
                       {

                       // while(*p != NULL)
                       //    *p = (*p)->next;
                       // *p = (PPRINTER_LIST)malloc(sizeof(PRINTER_LIST));
                       // (*p)->next = NULL;
                      //  sprintf((*p)->name,"%s",printer_info1[i].pName);
					    strcpy(pprinters[i ],printer_info1[i].pName);
						pos ++;
                        }
                     else
                        {
                       // ListPrinters(printer_info1[i].pName,pprinters,pos,max_printers);
                        }

		   }
			 free(printer_info1);
			 }
		else
		  return -1;
		}
return i;
}

int free_list(PPRINTER_LIST *pprinters)
 {
 PPRINTER_LIST p,p2;
 p = *pprinters;
 while(p != NULL)
    {
     p2 = p->next;
     free(p);
     p = p2;
    }
return 1;
}

#ifdef UNICODE
  #define GETDEFAULTPRINTER "GetDefaultPrinterW"
#else
  #define GETDEFAULTPRINTER "GetDefaultPrinterA"
#endif

// Size of internal buffer used to hold "printername,drivername,portname"
// string. You may have to increase this for huge strings.
#define MAXBUFFERSIZE 250

/*----------------------------------------------------------------*/
/* DPGetDefaultPrinter                                            */
/*                                                                */
/* Parameters:                                                    */
/*   pPrinterName: Buffer alloc'd by caller to hold printer name. */
/*   pdwBufferSize: On input, ptr to size of pPrinterName.        */
/*          On output, min required size of pPrinterName.         */
/*                                                                */
/* NOTE: You must include enough space for the NULL terminator!   */
/*                                                                */
/* Returns: TRUE for success, FALSE for failure.                  */
/*----------------------------------------------------------------*/
long WINAPI GetDefPrinter(LPTSTR pPrinterName, LPDWORD pdwBufferSize)
{
  BOOL bFlag;
  OSVERSIONINFO osv;
  TCHAR cBuffer[MAXBUFFERSIZE];
  PRINTER_INFO_2 *ppi2 = NULL;
  DWORD dwNeeded = 0;
  DWORD dwReturned = 0;
  HMODULE hWinSpool = NULL;
  PROC fnGetDefaultPrinter = NULL;

  // What version of Windows are you running?
  osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
  GetVersionEx(&osv);

  // If Windows 95 or 98, use EnumPrinters.
  if (osv.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS)
  {
    // The first EnumPrinters() tells you how big our buffer must
    // be to hold ALL of PRINTER_INFO_2. Note that this will
    // typically return FALSE. This only means that the buffer (the 4th
    // parameter) was not filled in. You do not want it filled in here.
    SetLastError(0);
    bFlag = EnumPrinters(PRINTER_ENUM_DEFAULT, NULL, 2, NULL, 0, &dwNeeded, &dwReturned);
    {
      if ((GetLastError() != ERROR_INSUFFICIENT_BUFFER) || (dwNeeded == 0))
        return FALSE;
    }

    // Allocate enough space for PRINTER_INFO_2.
    ppi2 = (PRINTER_INFO_2 *)GlobalAlloc(GPTR, dwNeeded);
    if (!ppi2)
      return FALSE;

    // The second EnumPrinters() will fill in all the current information.
    bFlag = EnumPrinters(PRINTER_ENUM_DEFAULT, NULL, 2, (LPBYTE)ppi2, dwNeeded, &dwNeeded, &dwReturned);
    if (!bFlag)
    {
      GlobalFree(ppi2);
      return FALSE;
    }

    // If specified buffer is too small, set required size and fail.
    if ((DWORD)lstrlen(ppi2->pPrinterName) >= *pdwBufferSize)
    {
      *pdwBufferSize = (DWORD)lstrlen(ppi2->pPrinterName) + 1;
      GlobalFree(ppi2);
      return FALSE;
    }

    // Copy printer name into passed-in buffer.
    lstrcpy(pPrinterName, ppi2->pPrinterName);

    // Set buffer size parameter to minimum required buffer size.
    *pdwBufferSize = (DWORD)lstrlen(ppi2->pPrinterName) + 1;
  }

  // If Windows NT, use the GetDefaultPrinter API for Windows 2000,
  // or GetProfileString for version 4.0 and earlier.
  else if (osv.dwPlatformId == VER_PLATFORM_WIN32_NT)
  {
    if (osv.dwMajorVersion >= 5) // Windows 2000 or later (use explicit call)
    {
      hWinSpool = LoadLibrary("winspool.drv");
      if (!hWinSpool)
        return FALSE;
      fnGetDefaultPrinter = GetProcAddress(hWinSpool, GETDEFAULTPRINTER);
      if (!fnGetDefaultPrinter)
      {
        FreeLibrary(hWinSpool);
        return FALSE;
      }

      bFlag = fnGetDefaultPrinter(pPrinterName, pdwBufferSize);
        FreeLibrary(hWinSpool);
      if (!bFlag)
        return FALSE;
    }

    else // NT4.0 or earlier
    {
      // Retrieve the default string from Win.ini (the registry).
      // String will be in form "printername,drivername,portname".
      if (GetProfileString("windows", "device", ",,,", cBuffer, MAXBUFFERSIZE) <= 0)
        return FALSE;

      // Printer name precedes first "," character.
      strtok(cBuffer, ",");

      // If specified buffer is too small, set required size and fail.
      if ((DWORD)lstrlen(cBuffer) >= *pdwBufferSize)
      {
        *pdwBufferSize = (DWORD)lstrlen(cBuffer) + 1;
        return FALSE;
      }

      // Copy printer name into passed-in buffer.
      lstrcpy(pPrinterName, cBuffer);

      // Set buffer size parameter to minimum required buffer size.
      *pdwBufferSize = (DWORD)lstrlen(cBuffer) + 1;
    }
  }

  // Clean up.
  if (ppi2)
    GlobalFree(ppi2);

  return TRUE;
}

#ifdef UNICODE
  #define SETDEFAULTPRINTER "SetDefaultPrinterW"
#else
  #define SETDEFAULTPRINTER "SetDefaultPrinterA"
#endif
/*-----------------------------------------------------------------*/
/* DPSetDefaultPrinter                                             */
/*                                                                 */
/* Parameters:                                                     */
/*   pPrinterName: Valid name of existing printer to make default. */
/*                                                                 */
/* Returns: TRUE for success, FALSE for failure.                   */
/*-----------------------------------------------------------------*/

long WINAPI SetDefPrinter(LPTSTR pPrinterName)
{
  BOOL bFlag;
  OSVERSIONINFO osv;
  DWORD dwNeeded = 0;
  HANDLE hPrinter = NULL;
  PRINTER_INFO_2 *ppi2 = NULL;
  LPTSTR pBuffer = NULL;
  LONG lResult;
  HMODULE hWinSpool = NULL;
  PROC fnSetDefaultPrinter = NULL;

  // What version of Windows are you running?
  osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
  GetVersionEx(&osv);

  if (!pPrinterName)
    return FALSE;

  // If Windows 95 or 98, use SetPrinter.
  if (osv.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS)
  {
    // Open this printer so you can get information about it.
    bFlag = OpenPrinter(pPrinterName, &hPrinter, NULL);
    if (!bFlag || !hPrinter)
      return FALSE;

    // The first GetPrinter() tells you how big our buffer must
    // be to hold ALL of PRINTER_INFO_2. Note that this will
    // typically return FALSE. This only means that the buffer (the 3rd
    // parameter) was not filled in. You do not want it filled in here.
    SetLastError(0);
    bFlag = GetPrinter(hPrinter, 2, 0, 0, &dwNeeded);
    if (!bFlag)
    {
      if ((GetLastError() != ERROR_INSUFFICIENT_BUFFER) || (dwNeeded == 0))
      {
        ClosePrinter(hPrinter);
        return FALSE;
      }
    }

    // Allocate enough space for PRINTER_INFO_2.
    ppi2 = (PRINTER_INFO_2 *)GlobalAlloc(GPTR, dwNeeded);
    if (!ppi2)
    {
      ClosePrinter(hPrinter);
      return FALSE;
    }

    // The second GetPrinter() will fill in all the current information
    // so that all you have to do is modify what you are interested in.
    bFlag = GetPrinter(hPrinter, 2, (LPBYTE)ppi2, dwNeeded, &dwNeeded);
    if (!bFlag)
    {
      ClosePrinter(hPrinter);
      GlobalFree(ppi2);
      return FALSE;
    }

    // Set default printer attribute for this printer.
    ppi2->Attributes |= PRINTER_ATTRIBUTE_DEFAULT;
    bFlag = SetPrinter(hPrinter, 2, (LPBYTE)ppi2, 0);
    if (!bFlag)
    {
      ClosePrinter(hPrinter);
      GlobalFree(ppi2);
      return FALSE;
    }

    // Tell all open programs that this change occurred.
    // Allow each program 1 second to handle this message.
    lResult = SendMessageTimeout(HWND_BROADCAST, WM_SETTINGCHANGE, 0L,
      (LPARAM)(LPCTSTR)"windows", SMTO_NORMAL, 1000, NULL);
  }

  // If Windows NT, use the SetDefaultPrinter API for Windows 2000,
  // or WriteProfileString for version 4.0 and earlier.
  else if (osv.dwPlatformId == VER_PLATFORM_WIN32_NT)
  {
    if (osv.dwMajorVersion >= 5) // Windows 2000 or later (use explicit call)
    {
      hWinSpool = LoadLibrary("winspool.drv");
      if (!hWinSpool)
        return FALSE;
      fnSetDefaultPrinter = GetProcAddress(hWinSpool, SETDEFAULTPRINTER);
      if (!fnSetDefaultPrinter)
      {
        FreeLibrary(hWinSpool);
        return FALSE;
      }

      bFlag = fnSetDefaultPrinter(pPrinterName);
      FreeLibrary(hWinSpool);
      if (!bFlag)
        return FALSE;
    }

    else // NT4.0 or earlier
    {
      // Open this printer so you can get information about it.
      bFlag = OpenPrinter(pPrinterName, &hPrinter, NULL);
      if (!bFlag || !hPrinter)
        return FALSE;

      // The first GetPrinter() tells you how big our buffer must
      // be to hold ALL of PRINTER_INFO_2. Note that this will
      // typically return FALSE. This only means that the buffer (the 3rd
      // parameter) was not filled in. You do not want it filled in here.
      SetLastError(0);
      bFlag = GetPrinter(hPrinter, 2, 0, 0, &dwNeeded);
      if (!bFlag)
      {
        if ((GetLastError() != ERROR_INSUFFICIENT_BUFFER) || (dwNeeded == 0))
        {
          ClosePrinter(hPrinter);
          return FALSE;
        }
      }

      // Allocate enough space for PRINTER_INFO_2.
      ppi2 = (PRINTER_INFO_2 *)GlobalAlloc(GPTR, dwNeeded);
      if (!ppi2)
      {
        ClosePrinter(hPrinter);
        return FALSE;
      }

      // The second GetPrinter() fills in all the current<BR/>
      // information.
      bFlag = GetPrinter(hPrinter, 2, (LPBYTE)ppi2, dwNeeded, &dwNeeded);
      if ((!bFlag) || (!ppi2->pDriverName) || (!ppi2->pPortName))
      {
        ClosePrinter(hPrinter);
        GlobalFree(ppi2);
        return FALSE;
      }

      // Allocate buffer big enough for concatenated string.
      // String will be in form "printername,drivername,portname".
      pBuffer = (LPTSTR)GlobalAlloc(GPTR,
        lstrlen(pPrinterName) +
        lstrlen(ppi2->pDriverName) +
        lstrlen(ppi2->pPortName) + 3);
      if (!pBuffer)
      {
        ClosePrinter(hPrinter);
        GlobalFree(ppi2);
        return FALSE;
      }

      // Build string in form "printername,drivername,portname".
      lstrcpy(pBuffer, pPrinterName);  lstrcat(pBuffer, ",");
      lstrcat(pBuffer, ppi2->pDriverName);  lstrcat(pBuffer, ",");
      lstrcat(pBuffer, ppi2->pPortName);

      // Set the default printer in Win.ini and registry.
      bFlag = WriteProfileString("windows", "device", pBuffer);
      if (!bFlag)
      {
        ClosePrinter(hPrinter);
        GlobalFree(ppi2);
        GlobalFree(pBuffer);
        return FALSE;
      }
    }

    // Tell all open programs that this change occurred.
    // Allow each app 1 second to handle this message.
    lResult = SendMessageTimeout(HWND_BROADCAST, WM_SETTINGCHANGE, 0L, 0L,
      SMTO_NORMAL, 1000, NULL);
  }

  // Clean up.
  if (hPrinter)
    ClosePrinter(hPrinter);
  if (ppi2)
    GlobalFree(ppi2);
  if (pBuffer)
    GlobalFree(pBuffer);

  return TRUE;
}








int GetErrorMsg(char *msg)
 {
  LPVOID lpMsgBuf;
  FormatMessage(
          FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		  NULL,GetLastError(),MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
          (LPTSTR) &lpMsgBuf,    0,    NULL );
  strcpy(msg,lpMsgBuf);
  LocalFree( lpMsgBuf );
  return 1;
 }
#include "winver.h"
long WINAPI GetFileVersion(char *modulename,char *version)
{
	VS_FIXEDFILEINFO *ver;
	DWORD len;
	TCHAR filename[_MAX_PATH];
	DWORD dwDummyHandle; // will always be set to zero
	LPVOID lpvi;
	UINT iLen;
	BYTE *bytes;
			// default = ANSI code page
	//memset((VS_FIXEDFILEINFO*)this, 0, sizeof(VS_FIXEDFILEINFO));

	// get module handle

/*	HMODULE hModule = GetModuleHandle(modulename);
	if (hModule==NULL && modulename!=NULL)
		return -1;

	// get module file name
	 len = GetModuleFileName(hModule, filename,
		sizeof(filename)/sizeof(filename[0]));
	if (len <= 0)
		return -1;*/

	// read file version info

	len = GetFileVersionInfoSize(modulename, &dwDummyHandle);
	if (len <= 0)
		return -1;

	bytes = malloc(len); // allocate version info
	if (!GetFileVersionInfo(modulename, 0, len, bytes))
		return FALSE;


	if (!VerQueryValue(bytes, "\\", &lpvi, &iLen))
		return -1;

	// copy fixed info to myself, which am derived from VS_FIXEDFILEINFO
	ver  = (VS_FIXEDFILEINFO*)lpvi;
    sprintf(version,"Version: %d.%d.%d.%d",
			HIWORD(ver->dwFileVersionMS), LOWORD(ver->dwFileVersionMS),
			HIWORD(ver->dwFileVersionLS), LOWORD(ver->dwFileVersionLS));
	free(bytes);
	return 0;
}

#include "pborca.h"
#include <stdio.h>

typedef LONG  (WINAPI * FPBORCA_SessionClose)
                 ( HPBORCA hORCASession );

typedef LONG  (WINAPI * FPBORCA_SessionGetError)
                              ( HPBORCA hORCASession,
                                                  LPTSTR   lpszErrorBuffer,
                                                  INT     iErrorBufferSize );

typedef HPBORCA  (WINAPI * FPBORCA_SessionOpen) (void );


typedef INT  (WINAPI * FPBORCA_SessionSetCurrentAppl)( HPBORCA hORCASession,
                                                  LPTSTR   lpszApplLibName,
                                                  LPTSTR   lpszApplName );

typedef LONG  (WINAPI * FPBORCA_SessionSetLibraryList)( HPBORCA    hORCASession,
                                                  LPTSTR FAR *pLibNames,
                                                  INT        iNumberOfLibs );
typedef LONG  (WINAPI * FPBORCA_LibraryCommentModify) ( HPBORCA hORCASession,
                                             LPTSTR   lpszLibName,
                                             LPTSTR   lpszLibComments );

typedef LONG  (WINAPI * FPBORCA_LibraryCreate )   ( HPBORCA hORCASession,
                                         LPTSTR   lpszLibName,
                                         LPTSTR   lpszLibComments );

typedef LONG  (WINAPI * FPBORCA_LibraryDelete)    ( HPBORCA hORCASession,
                                         LPTSTR   lpszLibName );

typedef LONG  (WINAPI * FPBORCA_LibraryDirectory) ( HPBORCA         hORCASession,
                                         LPTSTR           lpszLibName,
                                         LPTSTR           lpszLibComments,
                                         INT             iCmntsBuffSize,
                                         PBORCA_LISTPROC pListProc,
                                         LPVOID          pUserData );

typedef LONG  (WINAPI * FPBORCA_LibraryEntry) ( HPBORCA     hORCASession,
                                           LPTSTR       lpszLibName,
                                           LPTSTR       lpszEntryName,
										   PBORCA_TYPE otEntryType,
                                           PPBORCA_ENTRYINFO pEntryInfo );

typedef LONG  (WINAPI * FPBORCA_LibraryEntryDelete) ( HPBORCA     hORCASession,
                                           LPTSTR       lpszLibName,
                                           LPTSTR       lpszEntryName,
                                           PBORCA_TYPE otEntryType );

typedef LONG  (WINAPI * FPBORCA_LibraryEntryExport) ( HPBORCA     hORCASession,
                                           LPTSTR       lpszLibName,
                                           LPTSTR       lpszEntryName,
                                           PBORCA_TYPE otEntryType,
                                           LPTSTR       lpszExportBuffer,
                                           LONG        lExportBufferSize );

typedef LONG  (WINAPI * FPBORCA_LibraryEntryInformation) ( HPBORCA           hORCASession,
                                                LPTSTR             lpszLibName,
                                                LPTSTR             lpszEntryName,
                                                PBORCA_TYPE       otEntryType,
                                                PPBORCA_ENTRYINFO pEntryInfo );

typedef LONG  (WINAPI * FPBORCA_LibraryEntryMove) ( HPBORCA     hORCASession,
                                         LPTSTR       lpszSourceLibName,
                                         LPTSTR       lpszDestLibName,
                                         LPTSTR       lpszEntryName,
                                         PBORCA_TYPE otEntryType );

/* *********************************************************************** */
/* Source management function prototypes                                   */
/* *********************************************************************** */

typedef LONG  (WINAPI * FPBORCA_CheckOutEntry) (	HPBORCA     hORCASession,
	  									LPTSTR       lpszEntryName,
	  									LPTSTR       lpszSourceLibName,
	  									LPTSTR       lpszDestLibName,
	  									LPTSTR       lpszUserID,
                                    	PBORCA_TYPE otEntryType,
                                    	INT 		bMakeCopy);

typedef LONG  (WINAPI * FPBORCA_CheckInEntry) (	HPBORCA     hORCASession,
	  									LPTSTR       lpszEntryName,
	  									LPTSTR       lpszSourceLibName,
	  									LPTSTR       lpszDestLibName,
	  									LPTSTR       lpszUserID,
                                    	PBORCA_TYPE otEntryType,
                                    	INT 		bMoveEntry);

//typedef LONG  (WINAPI * FPBORCA_ListCheckOutEntries) (	HPBORCA     		hORCASession,
//	  										LPTSTR       		lpszLibraryName,
//	  										PBORCA_CHECKPROC	lpCallbackFunction,
//	  										LPVOID      		pUserData);
//
/* *********************************************************************** */
/* Compilation function prototypes                                         */
/* *********************************************************************** */

typedef LONG  (WINAPI * FPBORCA_CompileEntryImport) ( HPBORCA        hORCASession,
                                           LPTSTR          lpszLibraryName,
                                           LPTSTR          lpszEntryName,
                                           PBORCA_TYPE    otEntryType,
                                           LPTSTR          lpszComments,
                                           LPTSTR          lpszEntrySyntax,
                                           LONG           lEntrySyntaxBuffSize,
                                           PBORCA_ERRPROC pCompErrProc,
                                           LPVOID         pUserData );

typedef LONG  (WINAPI * FPBORCA_CompileEntryImportList) ( HPBORCA          hORCASession,
                                               LPTSTR far       *pLibraryNames,
                                               LPTSTR far       *pEntryNames,
                                               PBORCA_TYPE far *otEntryType,
                                               LPTSTR far       *pComments,
                                               LPTSTR far       *pEntrySyntaxBuffers,
                                               LONG far        *pEntrySyntaxBuffSizes,
                                               INT              iNumberOfEntries,
                                               PBORCA_ERRPROC   pCompErrProc,
                                               LPVOID           pUserData );

typedef LONG  (WINAPI * FPBORCA_CompileEntryRegenerate) ( HPBORCA        hORCASession,
                                               LPTSTR          lpszLibraryName,
                                               LPTSTR          lpszEntryName,
                                               PBORCA_TYPE    otEntryType,
                                               PBORCA_ERRPROC pCompErrProc,
                                               LPVOID         pUserData );

typedef LONG  (WINAPI * FPBORCA_ApplicationRebuild) ( 	HPBORCA        		hORCASession,
                                           	PBORCA_REBLD_TYPE	eRebldType,
                                           	PBORCA_ERRPROC 		pCompErrProc,
                                            LPVOID         		pUserData );

/* *********************************************************************** */
/* Executable construction function prototypes                             */
/* *********************************************************************** */
typedef LONG  (WINAPI * FPBORCA_ExecutableCreate )    ( HPBORCA        hORCASession,
                                             LPTSTR          lpszExeName,
                                             LPTSTR          lpszIconName,
                                             LPTSTR          lpszPBRName,
	                                         PBORCA_LNKPROC pLinkErrProc,
	                                         LPVOID         pUserData,
                                             INT FAR       *iPBDFlags,
                                             INT            iNumberOfPBDFlags,
                                             LONG           lFlags );

typedef LONG  (WINAPI * FPBORCA_DynamicLibraryCreate) ( HPBORCA        hORCASession,
                                             LPTSTR          lpszLibraryName,
                                             LPTSTR          lpszPBRName,
                                             LONG           lFlags );

/* *********************************************************************** */
/* Object query function prototypes                                        */
/* *********************************************************************** */

typedef LONG  (WINAPI * FPBORCA_ObjectQueryHierarchy) ( HPBORCA         hORCASession,
                                             LPTSTR           lpszLibraryName,
                                             LPTSTR           lpszEntryName,
                                             PBORCA_TYPE     otEntryType,
                                             PBORCA_HIERPROC pHierarchyProc,
                                             LPVOID          pUserData );

typedef LONG  (WINAPI * FPBORCA_ObjectQueryReference) ( HPBORCA         hORCASession,
                                             LPTSTR           lpszLibraryName,
                                             LPTSTR           lpszEntryName,
                                             PBORCA_TYPE     otEntryType,
                                             PBORCA_REFPROC  pReferenceProc,
                                             LPVOID          pUserData );


/************************************************************************************/
typedef LONG  (WINAPI * FPPBORCA_SessionClose)
                 ( HPBORCA hORCASession );

typedef LONG  (WINAPI * FPBORCA_SessionGetError)
                              ( HPBORCA hORCASession,
                                                  LPTSTR   lpszErrorBuffer,
                                                  INT     iErrorBufferSize );




typedef LONG  (WINAPI * FPBORCA_SessionSetLibraryList)( HPBORCA    hORCASession,
                                                  LPTSTR FAR *pLibNames,
                                                  INT        iNumberOfLibs );
typedef LONG  (WINAPI * FPBORCA_LibraryCommentModify) ( HPBORCA hORCASession,
                                             LPTSTR   lpszLibName,
                                             LPTSTR   lpszLibComments );

typedef LONG  (WINAPI * FPBORCA_LibraryCreate )   ( HPBORCA hORCASession,
                                         LPTSTR   lpszLibName,
                                         LPTSTR   lpszLibComments );

typedef LONG  (WINAPI * FPBORCA_LibraryDelete)    ( HPBORCA hORCASession,
                                         LPTSTR   lpszLibName );

typedef LONG  (WINAPI * FPBORCA_LibraryDirectory) ( HPBORCA         hORCASession,
                                         LPTSTR           lpszLibName,
                                         LPTSTR           lpszLibComments,
                                         INT             iCmntsBuffSize,
                                         PBORCA_LISTPROC pListProc,
                                         LPVOID          pUserData );

typedef LONG  (WINAPI * FPBORCA_LibraryEntryCopy) ( HPBORCA     hORCASession,
                                         LPTSTR       lpszSourceLibName,
                                         LPTSTR       lpszDestLibName,
                                         LPTSTR       lpszEntryName,
                                         PBORCA_TYPE otEntryType );

typedef LONG  (WINAPI * FPBORCA_LibraryEntryDelete) ( HPBORCA     hORCASession,
                                           LPTSTR       lpszLibName,
                                           LPTSTR       lpszEntryName,
                                           PBORCA_TYPE otEntryType );

typedef LONG  (WINAPI * FPBORCA_LibraryEntryExport) ( HPBORCA     hORCASession,
                                           LPTSTR       lpszLibName,
                                           LPTSTR       lpszEntryName,
                                           PBORCA_TYPE otEntryType,
                                           LPTSTR       lpszExportBuffer,
                                           LONG        lExportBufferSize );

typedef LONG  (WINAPI * FPBORCA_LibraryEntryInformation) ( HPBORCA           hORCASession,
                                                LPTSTR             lpszLibName,
                                                LPTSTR             lpszEntryName,
                                                PBORCA_TYPE       otEntryType,
                                                PPBORCA_ENTRYINFO pEntryInfo );

typedef LONG  (WINAPI * FPBORCA_LibraryEntryMove) ( HPBORCA     hORCASession,
                                         LPTSTR       lpszSourceLibName,
                                         LPTSTR       lpszDestLibName,
                                         LPTSTR       lpszEntryName,
                                         PBORCA_TYPE otEntryType );

/* *********************************************************************** */
/* Source management function prototypes                                   */
/* *********************************************************************** */

typedef LONG  (WINAPI * FPBORCA_CheckOutEntry) (	HPBORCA     hORCASession,
	  									LPTSTR       lpszEntryName,
	  									LPTSTR       lpszSourceLibName,
	  									LPTSTR       lpszDestLibName,
	  									LPTSTR       lpszUserID,
                                    	PBORCA_TYPE otEntryType,
                                    	INT 		bMakeCopy);

typedef LONG  (WINAPI * FPBORCA_CheckInEntry) (	HPBORCA     hORCASession,
	  									LPTSTR       lpszEntryName,
	  									LPTSTR       lpszSourceLibName,
	  									LPTSTR       lpszDestLibName,
	  									LPTSTR       lpszUserID,
                                    	PBORCA_TYPE otEntryType,
                                    	INT 		bMoveEntry);

//typedef LONG  (WINAPI * FPBORCA_ListCheckOutEntries) (	HPBORCA     		hORCASession,
//	  										LPTSTR       		lpszLibraryName,
//	  										PBORCA_CHECKPROC	lpCallbackFunction,
//	  										LPVOID      		pUserData);

/* *********************************************************************** */
/* Compilation function prototypes                                         */
/* *********************************************************************** */

typedef LONG  (WINAPI * FPBORCA_CompileEntryImport) ( HPBORCA        hORCASession,
                                           LPTSTR          lpszLibraryName,
                                           LPTSTR          lpszEntryName,
                                           PBORCA_TYPE    otEntryType,
                                           LPTSTR          lpszComments,
                                           LPTSTR          lpszEntrySyntax,
                                           LONG           lEntrySyntaxBuffSize,
                                           PBORCA_ERRPROC pCompErrProc,
                                           LPVOID         pUserData );

typedef LONG  (WINAPI * FPBORCA_CompileEntryImportList) ( HPBORCA          hORCASession,
                                               LPTSTR far       *pLibraryNames,
                                               LPTSTR far       *pEntryNames,
                                               PBORCA_TYPE far *otEntryType,
                                               LPTSTR far       *pComments,
                                               LPTSTR far       *pEntrySyntaxBuffers,
                                               LONG far        *pEntrySyntaxBuffSizes,
                                               INT              iNumberOfEntries,
                                               PBORCA_ERRPROC   pCompErrProc,
                                               LPVOID           pUserData );

typedef LONG  (WINAPI * FPBORCA_CompileEntryRegenerate) ( HPBORCA        hORCASession,
                                               LPTSTR          lpszLibraryName,
                                               LPTSTR          lpszEntryName,
                                               PBORCA_TYPE    otEntryType,
                                               PBORCA_ERRPROC pCompErrProc,
                                               LPVOID         pUserData );

typedef LONG  (WINAPI * FPBORCA_ApplicationRebuild) ( 	HPBORCA        		hORCASession,
                                           	PBORCA_REBLD_TYPE	eRebldType,
                                           	PBORCA_ERRPROC 		pCompErrProc,
                                            LPVOID         		pUserData );

/* *********************************************************************** */
/* Executable construction function prototypes                             */
/* *********************************************************************** */
typedef LONG  (WINAPI * FPBORCA_ExecutableCreate )    ( HPBORCA        hORCASession,
                                             LPTSTR          lpszExeName,
                                             LPTSTR          lpszIconName,
                                             LPTSTR          lpszPBRName,
	                                         PBORCA_LNKPROC pLinkErrProc,
	                                         LPVOID         pUserData,
                                             INT FAR       *iPBDFlags,
                                             INT            iNumberOfPBDFlags,
                                             LONG           lFlags );

static FPBORCA_SessionClose pPBORCA_SessionClose = 0;
static FPBORCA_SessionGetError pPBORCA_SessionGetError = 0;
static FPBORCA_SessionOpen pPBORCA_SessionOpen = 0;
static FPBORCA_SessionSetCurrentAppl pPBORCA_SessionSetCurrentAppl = 0;
static FPBORCA_SessionSetLibraryList pPBORCA_SessionSetLibraryList = 0;
static FPBORCA_LibraryCommentModify pPBORCA_LibraryCommentModify;
static FPBORCA_LibraryCreate pPBORCA_LibraryCreate = 0;
static FPBORCA_LibraryDelete pPBORCA_LibraryDelete = 0;
static FPBORCA_LibraryEntryExport pPBORCA_LibraryEntryExport = 0;
static FPBORCA_LibraryDirectory pPBORCA_LibraryDirectory = 0;
static FPBORCA_LibraryEntryDelete pPBORCA_LibraryEntryDelete = 0;
static FPBORCA_LibraryEntry pPBORCA_LibraryEntry = 0;
static FPBORCA_CompileEntryImport pPBORCA_CompileEntryImport = 0;

static FPBORCA_LibraryEntryCopy pPBORCA_LibraryEntryCopy = 0;
static FPBORCA_LibraryEntryInformation pPBORCA_LibraryEntryInformation  = 0;
static FPBORCA_LibraryEntryMove pPBORCA_LibraryEntryMove = 0;
static FPBORCA_CompileEntryImportList pPBORCA_CompileEntryImportList = 0;
static FPBORCA_CompileEntryRegenerate pPBORCA_CompileEntryRegenerate = 0;
static FPBORCA_ApplicationRebuild pPBORCA_ApplicationRebuild = 0;
static FPBORCA_ExecutableCreate pPBORCA_ExecutableCreate = 0;
static FPBORCA_DynamicLibraryCreate pPBORCA_DynamicLibraryCreate = 0;
static FPBORCA_ObjectQueryHierarchy pPBORCA_ObjectQueryHierarchy = 0;
static FPBORCA_ObjectQueryReference pPBORCA_ObjectQueryReference = 0;
void ErrorMsg(char *msg,long code);

static long g_ver;
long WINAPI LoadPB(long ver)
	{
	 char msg[200];
	 static HMODULE hModKERNEL32 = 0;
	 g_ver = ver;
	 if ( !hModKERNEL32 )
	 switch(ver)
			{
			case 9:
                hModKERNEL32 = LoadLibrary( "pborc90.dll" );
				break;
			case 10:
				hModKERNEL32 = LoadLibrary( "pborc100.dll" );
				break;
            case 11:
				hModKERNEL32 = LoadLibrary( "pborc110.dll" );
				break;
		    case 12:
				hModKERNEL32 = LoadLibrary( "pborc120.dll" );
				break;
			case 7:
                hModKERNEL32 = LoadLibrary( "pborc70.dll" );
				break;
			case 17:
                hModKERNEL32 = LoadLibrary( "pborc170.dll" );
				break;
			}
	 sprintf(msg,"Error loading Version %d PBORC ",ver);

	if(hModKERNEL32 == 0)
			 {
			 ErrorMsg(msg,GetLastError());
			 return -1;
			 }

	  pPBORCA_SessionClose = (FPBORCA_SessionClose)
		        GetProcAddress( hModKERNEL32, "PBORCA_SessionClose" );
     // if(ver < 10 )
		pPBORCA_SessionOpen = (FPBORCA_SessionOpen)
		        GetProcAddress( hModKERNEL32, "PBORCA_SessionOpen" );
	 // else
	//	pPBORCA_SessionOpen = (FPBORCA_SessionOpen)
	//	        GetProcAddress( hModKERNEL32, "PBORCA_SessionOpenA");

	  pPBORCA_SessionGetError = (FPBORCA_SessionGetError)
        GetProcAddress( hModKERNEL32, "PBORCA_SessionGetError" );


      pPBORCA_SessionSetCurrentAppl = (FPBORCA_SessionSetCurrentAppl)
        GetProcAddress( hModKERNEL32, "PBORCA_SessionSetCurrentAppl" );

	  pPBORCA_SessionOpen = (FPBORCA_SessionOpen)
        GetProcAddress( hModKERNEL32, "PBORCA_SessionOpen" );

	  pPBORCA_SessionSetLibraryList = (FPBORCA_SessionSetLibraryList)
        GetProcAddress( hModKERNEL32, "PBORCA_SessionSetLibraryList" );

	  pPBORCA_LibraryCommentModify = (FPBORCA_LibraryCommentModify)
        GetProcAddress( hModKERNEL32, "PBORCA_LibraryCommentModify" );

	  pPBORCA_LibraryCreate = (FPBORCA_LibraryCreate)
        GetProcAddress( hModKERNEL32, "PBORCA_LibraryCreate" );


	  pPBORCA_LibraryEntry = (FPBORCA_LibraryEntry)
        GetProcAddress( hModKERNEL32, "PBORCA_LibraryEntry" );

	  pPBORCA_LibraryEntryExport = (FPBORCA_LibraryEntryExport)
        GetProcAddress( hModKERNEL32, "PBORCA_LibraryEntryExport" );

	  pPBORCA_LibraryDirectory = (FPBORCA_LibraryDirectory)
        GetProcAddress( hModKERNEL32, "PBORCA_LibraryDirectory" );

	  pPBORCA_LibraryEntryDelete = (FPBORCA_LibraryEntryDelete)
        GetProcAddress( hModKERNEL32, "PBORCA_LibraryEntryDelete" );

	  pPBORCA_CompileEntryImport = (FPBORCA_CompileEntryImport)
        GetProcAddress( hModKERNEL32, "PBORCA_CompileEntryImport" );

	  pPBORCA_LibraryEntryCopy = (FPBORCA_LibraryEntryCopy)
        GetProcAddress( hModKERNEL32, "PBORCA_LibraryEntryCopy" );

	  pPBORCA_LibraryEntryInformation  = (FPBORCA_LibraryEntryInformation)
        GetProcAddress( hModKERNEL32, "PBORCA_LibraryEntryInformation" );

	  pPBORCA_LibraryEntryMove = (FPBORCA_LibraryEntryMove)
        GetProcAddress( hModKERNEL32, "PBORCA_LibraryEntryMove" );

	  pPBORCA_CompileEntryImportList = (FPBORCA_CompileEntryImportList)
        GetProcAddress( hModKERNEL32, "PBORCA_CompileEntryImportList" );

	  pPBORCA_CompileEntryRegenerate = (FPBORCA_CompileEntryRegenerate)
        GetProcAddress( hModKERNEL32, "PBORCA_CompileEntryRegenerate" );

	  pPBORCA_ApplicationRebuild = (FPBORCA_ApplicationRebuild)
        GetProcAddress( hModKERNEL32, "PBORCA_ApplicationRebuild" );

	  pPBORCA_ExecutableCreate = (FPBORCA_ExecutableCreate)
        GetProcAddress( hModKERNEL32, "PBORCA_ExecutableCreate" );

	  pPBORCA_DynamicLibraryCreate = (FPBORCA_DynamicLibraryCreate)
        GetProcAddress( hModKERNEL32, "PBORCA_DynamicLibraryCreate" );

	  pPBORCA_ObjectQueryHierarchy = (FPBORCA_ObjectQueryHierarchy)
        GetProcAddress( hModKERNEL32, "PBORCA_ObjectQueryHierarchy" );

	  pPBORCA_ObjectQueryReference = (FPBORCA_ObjectQueryReference)
        GetProcAddress( hModKERNEL32, "PBORCA_ObjectQueryReference" );

return 0;
}

/**************************** PBORCA_SessionClose  ****************************/
PBWINAPI_(VOID) PBORCA_SessionClose( HPBORCA hORCASession )
{
pPBORCA_SessionClose(hORCASession );
return ;
}

/**************************** PBORCA_SessionGetError ****************************/
PBWINAPI_(VOID) PBORCA_SessionGetError( HPBORCA hORCASession,
                                        LPTSTR   lpszErrorBuffer,
                                        INT     iErrorBufferSize )
{
pPBORCA_SessionGetError( hORCASession,lpszErrorBuffer,iErrorBufferSize );
return ;
}

/**************************** PBORCA_SessionOpen ****************************/
PBWINAPI_(HPBORCA) PPBORCA_SessionOpen( void )
{
return pPBORCA_SessionOpen();
}


/**************************** PBORCA_SessionSetCurrentAppl ****************************/
PBWINAPI_(INT) PBORCA_SessionSetCurrentAppl( HPBORCA hORCASession,
                                                  LPTSTR   lpszApplLibName,
                                                  LPTSTR   lpszApplName )
{
return pPBORCA_SessionSetCurrentAppl(hORCASession,lpszApplLibName,lpszApplName );
}

/**************************** PBORCA_SessionSetLibraryList ****************************/
PBWINAPI_(INT) PBORCA_SessionSetLibraryList ( HPBORCA    hORCASession,
                                                  LPTSTR FAR *pLibNames,
                                                  INT        iNumberOfLibs )
{
return pPBORCA_SessionSetLibraryList (hORCASession,pLibNames,iNumberOfLibs );
}


/**************************** PBORCA_LibraryCommentModify ****************************/
PBWINAPI_(INT) PBORCA_LibraryCommentModify ( HPBORCA hORCASession,
                                             LPTSTR   lpszLibName,
                                             LPTSTR   lpszLibComments )
{
return pPBORCA_LibraryCommentModify (hORCASession,lpszLibName,lpszLibComments );
}


/**************************** PBORCA_LibraryCreate ****************************/
PBWINAPI_(INT) PBORCA_LibraryCreate    ( HPBORCA hORCASession,
                                         LPTSTR   lpszLibName,
                                         LPTSTR   lpszLibComments )
{
return pPBORCA_LibraryCreate(hORCASession,lpszLibName,lpszLibComments );
}

/**************************** PBORCA_LibraryDelete ****************************/
PBWINAPI_(INT) PBORCA_LibraryDelete    ( HPBORCA hORCASession,
                                         LPTSTR   lpszLibName )
{
return pPBORCA_LibraryDelete(hORCASession,lpszLibName );
}

/**************************** PBORCA_LibraryDirectory ****************************/
PBWINAPI_(INT) PBORCA_LibraryDirectory ( HPBORCA         hORCASession,
                                         LPTSTR           lpszLibName,
                                         LPTSTR           lpszLibComments,
                                         INT             iCmntsBuffSize,
                                         PBORCA_LISTPROC pListProc,
                                         LPVOID          pUserData )
{
return pPBORCA_LibraryDirectory(hORCASession,lpszLibName,lpszLibComments,iCmntsBuffSize,
                                         pListProc,pUserData );
}

/**************************** PBORCA_LibraryEntryCopy ****************************/
PBWINAPI_(INT) PBORCA_LibraryEntryCopy ( HPBORCA     hORCASession,
                                         LPTSTR       lpszSourceLibName,
                                         LPTSTR       lpszDestLibName,
                                         LPTSTR       lpszEntryName,
                                         PBORCA_TYPE otEntryType )
{
return pPBORCA_LibraryEntryCopy (hORCASession,lpszSourceLibName,lpszDestLibName,
                                         lpszEntryName,otEntryType );
}

/****************************  PBORCA_LibraryEntryDelete ****************************/
PBWINAPI_(INT) PBORCA_LibraryEntryDelete ( HPBORCA     hORCASession,
                                           LPTSTR       lpszLibName,
                                           LPTSTR       lpszEntryName,
                                           PBORCA_TYPE otEntryType )
{
return pPBORCA_LibraryEntryDelete (hORCASession,lpszLibName,lpszEntryName,
                                           otEntryType );
}

/****************************  PBORCA_LibraryEntry  ****************************/
PBWINAPI_(INT) PBORCA_LibraryEntry (  HPBORCA           hORCASession,
                                                LPTSTR             lpszLibName,
                                                LPTSTR             lpszEntryName,
                                                PBORCA_TYPE       otEntryType,
                                                PPBORCA_ENTRYINFO pEntryInfo )
{
return pPBORCA_LibraryEntry( hORCASession,lpszLibName,
                                                lpszEntryName,
                                                otEntryType,
                                                pEntryInfo );
}
/**************************** PBORCA_LibraryEntryExport ****************************/
PBWINAPI_(INT) PPBORCA_LibraryEntryExport ( HPBORCA     hORCASession,
                                           LPTSTR       lpszLibName,
                                           LPTSTR       lpszEntryName,
                                           PBORCA_TYPE otEntryType,
                                           LPTSTR       lpszExportBuffer,
                                           LONG        lExportBufferSize )
{
return pPBORCA_LibraryEntryExport (hORCASession,lpszLibName,lpszEntryName,
                                           otEntryType,lpszExportBuffer,lExportBufferSize );
}

/**************************** PBORCA_LibraryEntryInformation ****************************/
PBWINAPI_(INT) PBORCA_LibraryEntryInformation ( HPBORCA           hORCASession,
                                                LPTSTR             lpszLibName,
                                                LPTSTR             lpszEntryName,
                                                PBORCA_TYPE       otEntryType,
                                                PPBORCA_ENTRYINFO pEntryInfo )
{
return pPBORCA_LibraryEntryInformation (hORCASession,lpszLibName,lpszEntryName,
                                                otEntryType,pEntryInfo );
}

/**************************** PBORCA_LibraryEntryMove ****************************/
PBWINAPI_(INT) PBORCA_LibraryEntryMove ( HPBORCA     hORCASession,
                                         LPTSTR       lpszSourceLibName,
                                         LPTSTR       lpszDestLibName,
                                         LPTSTR       lpszEntryName,
                                         PBORCA_TYPE otEntryType )
{
return pPBORCA_LibraryEntryMove (hORCASession,lpszSourceLibName,lpszDestLibName,
                                         lpszEntryName,otEntryType );
}


/**************************** PBORCA_LibraryEntryMove ****************************/
PBWINAPI_(INT) PBORCA_CompileEntryImport ( HPBORCA        hORCASession,
                                           LPTSTR          lpszLibraryName,
                                           LPTSTR          lpszEntryName,
                                           PBORCA_TYPE    otEntryType,
                                           LPTSTR          lpszComments,
                                           LPTSTR          lpszEntrySyntax,
                                           LONG           lEntrySyntaxBuffSize,
                                           PBORCA_ERRPROC pCompErrProc,
                                           LPVOID         pUserData )
{
return PBORCA_CompileEntryImport (hORCASession,lpszLibraryName,lpszEntryName,otEntryType,
                                           lpszComments,lpszEntrySyntax,lEntrySyntaxBuffSize,
                                           pCompErrProc,pUserData );
}

/**************************** PBORCA_CompileEntryImportList ****************************/
PBWINAPI_(INT) PBORCA_CompileEntryImportList ( HPBORCA          hORCASession,
                                               LPTSTR far       *pLibraryNames,
                                               LPTSTR far       *pEntryNames,
                                               PBORCA_TYPE far *otEntryType,
                                               LPTSTR far       *pComments,
                                               LPTSTR far       *pEntrySyntaxBuffers,
                                               LONG far        *pEntrySyntaxBuffSizes,
                                               INT              iNumberOfEntries,
                                               PBORCA_ERRPROC   pCompErrProc,
                                               LPVOID           pUserData )
{
return pPBORCA_CompileEntryImportList (hORCASession,pLibraryNames,pEntryNames, otEntryType,
                                               pComments,pEntrySyntaxBuffers,
                                               pEntrySyntaxBuffSizes,iNumberOfEntries,
                                               pCompErrProc,pUserData );
}
/**************************** PBORCA_CompileEntryRegenerate ****************************/
PBWINAPI_(INT) PBORCA_CompileEntryRegenerate ( HPBORCA        hORCASession,
                                               LPTSTR          lpszLibraryName,
                                               LPTSTR          lpszEntryName,
                                               PBORCA_TYPE    otEntryType,
                                               PBORCA_ERRPROC pCompErrProc,
                                               LPVOID         pUserData )
{
return pPBORCA_CompileEntryRegenerate (hORCASession,lpszLibraryName,lpszEntryName,
                                       otEntryType,pCompErrProc,pUserData );
}

/**************************** PBORCA_ApplicationRebuild ****************************/
PBWINAPI_(INT) PBORCA_ApplicationRebuild ( 	HPBORCA        		hORCASession,
                                           	PBORCA_REBLD_TYPE	eRebldType,
                                           	PBORCA_ERRPROC 		pCompErrProc,
                                            LPVOID         		pUserData )
{
return pPBORCA_ApplicationRebuild (hORCASession,eRebldType,pCompErrProc,pUserData );
}

/**************************** PBORCA_ExecutableCreate ****************************/
PBWINAPI_(INT) PBORCA_ExecutableCreate     ( HPBORCA        hORCASession,
                                             LPTSTR          lpszExeName,
                                             LPTSTR          lpszIconName,
                                             LPTSTR          lpszPBRName,
	                                         PBORCA_LNKPROC pLinkErrProc,
	                                         LPVOID         pUserData,
                                             INT FAR       *iPBDFlags,
                                             INT            iNumberOfPBDFlags,
                                             LONG           lFlags )
{
return pPBORCA_ExecutableCreate(hORCASession,lpszExeName,lpszIconName, lpszPBRName,
	                                 pLinkErrProc,pUserData,iPBDFlags,iNumberOfPBDFlags,
                                     lFlags );
}

/**************************** PBORCA_LibraryEntryMove ****************************/
PBWINAPI_(INT) PBORCA_DynamicLibraryCreate ( HPBORCA        hORCASession,
                                             LPTSTR          lpszLibraryName,
                                             LPTSTR          lpszPBRName,
                                             LONG           lFlags )
{
return pPBORCA_DynamicLibraryCreate(hORCASession,lpszLibraryName,lpszPBRName,lFlags );
}


/**************************** PBORCA_ObjectQueryHierarchy ****************************/
PBWINAPI_(INT) PBORCA_ObjectQueryHierarchy ( HPBORCA         hORCASession,
                                             LPTSTR           lpszLibraryName,
                                             LPTSTR           lpszEntryName,
                                             PBORCA_TYPE     otEntryType,
                                             PBORCA_HIERPROC pHierarchyProc,
                                             LPVOID          pUserData )
{
return pPBORCA_ObjectQueryHierarchy (hORCASession,lpszLibraryName,lpszEntryName,
                                             otEntryType,pHierarchyProc,pUserData );
}

/**************************** PBORCA_ObjectQueryReference ****************************/
PBWINAPI_(INT) PBORCA_ObjectQueryReference ( HPBORCA         hORCASession,
                                             LPTSTR           lpszLibraryName,
                                             LPTSTR           lpszEntryName,
                                             PBORCA_TYPE     otEntryType,
                                             PBORCA_REFPROC  pReferenceProc,
                                             LPVOID          pUserData )
{
return pPBORCA_ObjectQueryReference (hORCASession,lpszLibraryName,lpszEntryName,
                                             otEntryType,pReferenceProc,pUserData );
}


void ErrorMsg(char *msg,long code)
{
LPVOID lpMsgBuf;
char buffer[200];

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		code,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) &lpMsgBuf,
		0,
		NULL);
	if(lpMsgBuf != NULL)
		{
	  strcpy(buffer,(LPTSTR)lpMsgBuf);
	  LocalFree( lpMsgBuf );
		}
	else
	  sprintf(buffer,"Error code = %d",GetLastError());
MessageBox( NULL, (LPCTSTR)buffer, msg, MB_OK | MB_ICONINFORMATION );
}





/******************************* Call BackDirecectory ****************/

struct s_liblist
{
char object_name[60];
char object_mdate[40];
char object_cdate[40];
long object_type;
unsigned long object_time;
};
#define NULL_TERMINATED 0
// This is for Version 10 PowerBuilder
typedef struct ppc_pborca_direntry
{
					                        /* Comments */
    wchar_t            szComments[PBORCA_MAXCOMMENT + 1];

    LONG            lCreateTime;            /* Time of entry create/mod */
    LONG            lEntrySize;             /* Size of entry */
    LPWSTR           lpszEntryName;          /* Pointer to entry name */
    long     otEntryType;            /* Entry type */

} PPC_PBORCA_DIRENTRY, FAR *PPC_PPBORCA_DIRENTRY;

void  CALLBACK CallBackDirList10(PPC_PPBORCA_DIRENTRY dir, LPVOID  p)
 {
   long i;
   struct s_liblist *liblist;
   liblist = (struct s_liblist *)p;
   if(dir->otEntryType == 0)
	   return;
   for(i =0;i < 2000;i++)
	  {

	  if(liblist[i].object_type==-1)
          break;
      }

 lstrcpy(liblist[i].object_cdate,ctime(&dir->lCreateTime));
 liblist[i].object_time = dir->lCreateTime;
 //lstrcpy(liblist[i].object_mdate,ctime(&dir->lModTime[1]));
 //lstrcpy(liblist[i]->object_mdate,mtime(&dir->lCreateTime));
 liblist[i].object_type = dir->otEntryType;
 UnicodeToAnsiString(dir->lpszEntryName,liblist[i].object_name,NULL_TERMINATED);
 }

void  CALLBACK CallBackDirList(PPBORCA_DIRENTRY dir, LPVOID  p)
 {
   long i;
   struct s_liblist *liblist;
   liblist = (struct s_liblist *)p;
   if(dir->otEntryType == 0)
	   return;
   for(i =0;i < 2000;i++)
	  {

	  if(liblist[i].object_type==-1)
          break;
      }

 lstrcpy(liblist[i].object_cdate,ctime(&dir->lCreateTime));
 liblist[i].object_time = dir->lCreateTime;
 liblist[i].object_type = dir->otEntryType;
 strcpy(liblist[i].object_name,dir->lpszEntryName);
 }

PBORCA_ENTRYINFO pbentry;

unsigned long nodlist[100];
long nod_count;
/******************************** lowercase **********************************/
long lowercase(char *str)
{
long len,i;
len = strlen(str);
for(i = 0;i < len;++i)
   {
    str[i] = tolower(str[i]);
	}
}
LPWSTR AllocateUnicodeString(LPSTR  pAnsiString);
 /********************** GetPBDircectoryList **************************/
long WINAPI GetPBDirectoryList(HPBORCA hPboorca,char *pblpath,char *names[],char *dates[],char *mdates[],long types[],long len,long flag)
{
long i,status;
TCHAR str[256];
struct s_liblist liblist[4000];
unsigned long time1;
char pblext[10];
char *p;
long pbllib_flag;

LPWSTR pszPath;
for(i =0;i < len;i++)
	liblist[i].object_type= -1;

if(g_ver > 9 )
	{
	pszPath = AllocateUnicodeString(pblpath);
	status = PBORCA_LibraryDirectory( hPboorca,
					pszPath,
					str,PBORCA_MAXCOMMENT, CallBackDirList10,(LPVOID)&liblist);
	}
else
   status = PBORCA_LibraryDirectory( hPboorca,
					pblpath,
					str,PBORCA_MAXCOMMENT, CallBackDirList,(LPVOID)&liblist);

//liblist[i].object_type
nod_count=0;
p = strrchr(pblpath,'.');
if(p != NULL)
	{
	p++;
    strcpy(pblext,p);
    lowercase(pblext);
    if(strcmp(pblext,"pbl") == 0)
        pbllib_flag = 1;
	else
		pbllib_flag = 0;
	}
if(flag != 1 )
   GetModDate(pblpath,liblist);




for(i=0; liblist[i].object_type != -1 && i < 4000;++i)
	{
	//memset(&pbentry,0x00, sizeof(PBORCA_ENTRYINFO));
    //PBORCA_LibraryEntryInformation ( hPboorca,pblpath, liblist[i].object_name ,
	//	     liblist[i].object_type ,&pbentry);
    //PBORCA_LibraryEntry( hPboorca,pblpath, liblist[i].object_name ,
	//	     liblist[i].object_type ,&pbentry);
	time1 = liblist[i].object_time;
	if(ctime(&time1) != NULL)
         strcpy(mdates[i],ctime(&time1));
	strcpy(names[i],liblist[i].object_name);
	strcpy(dates[i],liblist[i].object_cdate);

	types[i] = liblist[i].object_type;
	}


return status;
}

#include <stdio.h>
char *ext[8] = {".srd",".fun",".men",".que",".str",".udo",".win"};
char *ext2[8] = {"srd","fun","men","que","str","udo","win"};
//PBORCA_APPLICATION,
//    PBORCA_DATAWINDOW,  dwo
//    PBORCA_FUNCTION,     fun
//    PBORCA_MENU,      men
 //   PBORCA_QUERY,      que
 //   PBORCA_STRUCTURE,   str
 //   PBORCA_USEROBJECT,    usd
 //   PBORCA_WINDOW,       win
 //   PBORCA_PIPELINE,
 //   PBORCA_PROJECT,
 //   PBORCA_PROXYOBJECT,
 //   PBORCA_BINARY

 long get_pos(struct s_liblist *a_lib,char *a_name)
   {
   long i;
   char *p;
   char name_ext[10];
   char name[100];
   long found = 0;
   strcpy(name,a_name);
   p = strchr(name,'.');
   *p = 0;
   p++;
   strcpy(name_ext,p);
   for(i=0; i < 7;++i)
      {
      if(strcmp(name_ext,ext2[i]) == 0)
	        {
	        found= 1;
			break;
			}
		}
   if(found == 0)
      return -1;

   for(i=0; a_lib[i].object_type != -1 && i < 2000;++i)
	   {
        if(strcmp(name,a_lib[i].object_name) == 0)
		    return i;
	   }
	return -1;

}
unsigned long get_time(FILE *,long );
long get_name(FILE *fileunit,char *obj_name)
{
char chr;
long pos = 0;
while(fread(&chr,1,1,fileunit))
		{

        if(chr == 0)
		   {
		   obj_name[pos] = 0;
		   return pos;
           }
        obj_name[pos] = chr;
        pos++;
		}
}

unsigned long GetModDate(char *file,struct s_liblist *a_lib)
{
FILE *fileunit;
unsigned char chr;
char *ent4 = "ENT*0400";
char *ent5 = "ENT*0500";
char *ent6 = "ENT*0600";
char *nod = "NOD*";
long nodlen,nodpos;
unsigned long loc,loc1,loc2;
unsigned long time1,time2;
long objpos,objlen;
long entlen;
long found,pos;
long num;
unsigned char chr1,chr2;
char obj_name[100];
long count=0;
long myloc,obj_pos;
long status;

entlen = strlen(ent4);
nodlen = strlen(nod);
fileunit = fopen(file,"rb");
fseek( fileunit, 0x400, SEEK_SET );
if(nod_count == 0)
   {
   pos=0;
   while(fread(&chr,1,1,fileunit))
		{

		if(chr ==nod[pos])
			{
			pos++;
			if(pos == nodlen)
			    {
			    nodlist[nod_count] = ftell(fileunit);
				nod_count++;
				}
			}
		else
		   pos = 0;
	   }
    fseek( fileunit, 0x400, SEEK_SET );
	}
found = 0;
nodpos=1;
top:
pos=0;
objpos = 0;
count=0;
while(fread(&chr,1,1,fileunit))
		{
		count++;
		if(chr == ent4[pos] || chr == ent5[pos] || chr == ent6[pos])
			{
			pos++;
			if(pos == entlen)
			    {
				myloc = ftell(fileunit);
				status = fseek( fileunit,myloc + 16, SEEK_SET );
				get_name(fileunit,obj_name);
				//status = fscanf(fileunit,"%s",obj_name);
                obj_pos = get_pos(a_lib,obj_name);
				if(obj_pos == -1)
				    {
                    pos=0;
					}
				else if(a_lib[obj_pos].object_type == 1)
                      {
                       fseek( fileunit, myloc + 8, SEEK_SET );
					   fread(&time1,1,4,fileunit);
                       a_lib[obj_pos].object_time = time1;

					  }
				else
				    {
					status = fseek( fileunit,myloc , SEEK_SET );
					while(fread(&chr1,1,1,fileunit) )
						 {
						  if(chr1 != 0)
							 break;
						 }
					fread(&chr2,1,1,fileunit);
					loc1 = chr1;
					loc2 = chr2;
					if(chr2 == 0)
						loc = loc1 * 256;
					else
						loc = ((loc1 + (loc2 * 256)) * 256);
					if((time1 = get_time(fileunit, loc)) != 0)
					   {
                        a_lib[obj_pos].object_time = time1;
					   }

					}
			    pos=0;
				count=0;
				fseek( fileunit,myloc + 15, SEEK_SET );
				continue;
				}
			}
		else
		   pos = 0;
        if(count > 100)
            {
			if(nodpos < nod_count)
			   {

			   count=0;
	           fseek( fileunit,nodlist[nodpos], SEEK_SET );
			   nodpos++;
			   }
			else
			   {
			   fclose(fileunit);
			   return 0;
			   }
			}
		}

}

unsigned long get_time(FILE *fileunit,long loc)
{
char chr,chr1;
unsigned long time1,time2;
long zero,num;
zero = 0;
fseek( fileunit, loc, SEEK_SET );
num = 0;
while(fread(&chr,1,1,fileunit) != 0)
  {
   num++;
   if(num > 200)
        {

	    return 0;
	    }
   if(chr == 0)
      {
	  if(zero == 9)
	     {
		 loc =  ftell(fileunit) - 10;
	     fseek( fileunit,loc, SEEK_SET  );
         fread(&time1,1,4,fileunit);
         //fread(&time2,1,4,fileunit);
		 //fread(&chr1,1,1,fileunit);
		 return time1;
		 }
      zero = 0;
	  }
   else
      zero++;

  }
return 0;

}

unsigned long GetObjModDate(char *,char *,long obj_type);
 /********************** GetPBDircectoryList **************************/
long WINAPI GetPBObjectDataInfo(HPBORCA hPboorca,char *pblpath,char *name,char *mdate,long types)
{
long i,status;
char str[256];
PBORCA_ENTRYINFO entry;
unsigned long time1;


//status = PBORCA_LibraryEntryInformation ( hPboorca,pblpath, name,types ,&entry);
//if(status != PBORCA_OK )
//  return -1;

nod_count=0;
time1 = entry.lCreateTime;
strcpy(mdate,ctime(&time1));
time1 = GetObjModDate(pblpath,name,types - 1);
if(time1 ==0)
  mdate[0] = 0;
else
  strcpy(mdate,ctime(&time1));

return status;
}


unsigned long GetObjModDate(char *file,char *obj,long obj_type)
{
FILE *fileunit;
unsigned char chr;
char *ent4 = "ENT*0400";
char *ent5 = "ENT*0500";
char *ent6 = "ENT*0600";
char *nod = "NOD*";
long nodlen,nodpos;
unsigned long loc,pos,loc1,loc2;
unsigned long time1,time2;
long objpos,objlen;
long entlen;
long found;
long num,zero;
unsigned char chr1,chr2;
char obj_name[100];
long count=0;
sprintf(obj_name,"%s%s",obj,ext[obj_type]);
entlen = strlen(ent4);
objlen = strlen(obj_name);
nodlen = strlen(nod);
fileunit = fopen(file,"rb");
fseek( fileunit, 0x400, SEEK_SET );
if(nod_count == 0)
   {
   pos=0;
   while(fread(&chr,1,1,fileunit))
		{

		if(chr ==nod[pos])
			{
			pos++;
			if(pos == nodlen)
			    {
			    nodlist[nod_count] = ftell(fileunit);
				nod_count++;
				}
			}
		else
		   pos = 0;
	   }
    fseek( fileunit, 0x400, SEEK_SET );
	}
found = 0;
nodpos=1;
top:
pos=0;
objpos = 0;
count=0;
while(fread(&chr,1,1,fileunit))
		{
		count++;
		if(chr == ent4[pos] || chr == ent5[pos] || chr == ent6[pos])
			{
			pos++;
			if(pos == entlen)
			    {
			    found=1;
				break;
				}
			}
		else
		   pos = 0;
		if(chr == obj_name[objpos])
            {
             objpos++;
             if(objpos == objlen)
			   {
			   found=1;
			   break;
			   }
			}
		 else
			objpos = 0;
        if(count > 100)
            {
			if(nodpos < nod_count)
			   {

			   count=0;
	           fseek( fileunit,nodlist[nodpos], SEEK_SET );
			   nodpos++;
			   }
			else
			   {
			   fclose(fileunit);
			   return 0;
			   }
			}
		}
if(found == 0)
   {
   fclose(fileunit);
   return 0;
   }
if(objpos == objlen)
    {

	goto findpos;
	}

if(pos != entlen)
	  {
	  goto top;
	  }
//if(found == 0)
//  goto top;
if(obj_type == 0)
    {
    loc = ftell(fileunit);
	goto top;
	}
while(fread(&chr1,1,1,fileunit) )
     {
      if(chr1 != 0)
	     break;
     }
fread(&chr2,1,1,fileunit);
loc1 = chr1;
loc2 = chr2;
if(chr2 == 0)
    loc = loc1 * 256;
else
    loc = ((loc1 + (loc2 * 256)) * 256);
if(fread(&chr1,1,1,fileunit) == 0)
   {
   fclose(fileunit);
   return 0;
   }
goto top;

findpos:

//if(nodlist[nodpos - 1] > loc)
//   loc *= 16;

;
if(obj_type == 0)
   {
   fseek( fileunit, loc + 8, SEEK_SET );
   fread(&time1,1,4,fileunit);
   return time1;
   }


fseek( fileunit, loc, SEEK_SET );
num = 0;
while(fread(&chr,1,1,fileunit) != 0)
  {
   num++;
   if(num > 200)
        {
	    fclose(fileunit);
	    return 0;
	    }
   if(chr == 0)
      {
	  if(zero == 9)
	     {
		 loc =  ftell(fileunit) - 10;
	     fseek( fileunit,loc, SEEK_SET  );
         fread(&time1,1,4,fileunit);
         fread(&time2,1,4,fileunit);
		 fread(&chr1,1,1,fileunit);
		 //fscanf(fileunit,"%ld%ld%c",&time1,&time2,&chr);
		 fclose(fileunit);
		 return time1;
		 }
      zero = 0;
	  }
   else
      zero++;

  }
return 0;

}

/********************* Enumerating the Installed Fonts *****************************/
BOOL  CALLBACK EnumFamCallBack(LPLOGFONT lplf, LPNEWTEXTMETRIC lpntm, DWORD FontType, LPVOID aFontCount);
struct s_font_list
{
long count;
char **font_names;
};
  long WINAPI GetFontList(char *fontlist[] ,long max_fonts)
{

	HDC hdc;
	long i;
	struct s_font_list fonts;
    fonts.count = 0;
	fonts.font_names = NULL;
    hdc = CreateDC( "DISPLAY",NULL,NULL,NULL);
    EnumFontFamilies(hdc, (LPCTSTR) NULL, (FONTENUMPROC) EnumFamCallBack, (LPARAM) &fonts);
    for( i = 0; i < max_fonts && i < fonts.count;++i)
		{
		strcpy(fontlist[i],fonts.font_names[i]);
		}
	for(i = 0;i < fonts.count;++i)
		free(fonts.font_names[i]);
	free(fonts.font_names);
    return fonts.count;
}

BOOL CALLBACK EnumFamCallBack(LPLOGFONT lplf, LPNEWTEXTMETRIC lpntm, DWORD FontType, LPVOID iFont_list)
{
    long len;
    struct s_font_list *fonts;
    fonts = (struct s_font_list *)iFont_list;
	if( fonts->font_names == NULL)
		fonts->font_names = (char **)malloc( (fonts->count + 1) * sizeof(char *));
	else
		fonts->font_names = (char **)realloc(fonts->font_names, (fonts->count + 1) * sizeof(char *));
    len = strlen(lplf->lfFaceName);
    fonts->font_names[fonts->count] = (char *)malloc(sizeof(char) * (len + 1));
    strcpy(fonts->font_names[fonts->count],lplf->lfFaceName);
    fonts->count++;
	return TRUE;
}


//#include <shtypes.h>
#include <shlobj.h>
//#include <Afx.h>
//#include <ShlObj.h>
//#include <Atlbase.h>
//#include <String.h>
//#define CSIDL_COMMON_PROGRAMS 0x0017
#define CSIDL_COMMON_DESKTOPDIRECTORY 0x0019
#define CSIDL_DESKTOPDIRECTORY 0x0010
#define CSIDL_PROGRAMS 0x0002

long WINAPI  CreateProgramGroup(char *prog_exe ,char *title,char *arg, char *location,
							    char *link_name, char *link_path )
{
HRESULT hr;
char path[MAX_PATH];
char path2[MAX_PATH];
char *common = "yes";
 IShellLink* psl ;
 IPersistFile* ppf ;
 HMODULE hmod;
 HKEY hKey;
 long code,valuetype,len,len2;
 long i,j;
 char prog[MAX_PATH];
 WCHAR wpath[MAX_PATH];

 if(strcmp(common,"yes") == 0 )
	{
	 if(strcmp(location,"menu") == 0 )
		{
		 hr = SHGetSpecialFolderPath(0, path,CSIDL_COMMON_PROGRAMS, FALSE);
		 if ( !SUCCEEDED ( hr ) )  return -1;
		 wsprintf(path,"%s\\PowerPlant",path);
		 CreateDirectory(path,NULL);
		}
	 else if(strcmp(location,"desktop") == 0 )
		 hr = SHGetSpecialFolderPath(0, path,CSIDL_COMMON_DESKTOPDIRECTORY, FALSE);
	 else
		 return -1;
	}
 else if(strcmp(common,"no") == 0 )
	{
	 if(strcmp(location,"menu") == 0 )
		{
		 hr = SHGetSpecialFolderPath(0, path2,CSIDL_PROGRAMS, FALSE);
	     if ( !SUCCEEDED ( hr ) )  return -1;
		 wsprintf(path,"%s\\PowerPlant",path2);
		 CreateDirectory(path2,NULL);
		}
	 else if(strcmp(location,"desktop") == 0)
		 hr = SHGetSpecialFolderPath(0, path,CSIDL_DESKTOPDIRECTORY, FALSE);
	 else
		 return -1;
	}
 else
	 return -1;




 CoInitialize(NULL);

  hr = CoCreateInstance ( &CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER,&IID_IShellLink, ( void** ) &psl ) ;
  if ( !SUCCEEDED ( hr ) )  return -1;

  hr = psl ->lpVtbl->QueryInterface (psl, &IID_IPersistFile, ( void** ) &ppf ) ;
  if ( !SUCCEEDED ( hr ) )  return -1;

  psl ->lpVtbl-> SetPath (psl,prog_exe ) ;  // Replace with your AppPath
  psl ->lpVtbl-> SetDescription (psl, title ) ; // Shortcut escription
   psl ->lpVtbl-> SetArguments(psl, arg);

   strcat(path,"\\");
   strcat(path,link_name);

   MultiByteToWideChar(CP_ACP, 0, path, -1, wpath, MAX_PATH);
   ppf ->lpVtbl-> Save(ppf, wpath, TRUE ) ;

   ppf  ->lpVtbl-> Release(ppf ) ;
   psl  ->lpVtbl-> Release(psl ) ;
   CoUninitialize();
   len = strlen(path);
   strcpy(link_path,"file:///");
   len2 = strlen(link_path);
   for(i = 0; i < len ;++i)
	{
	switch(path[i])
	{
	   // case '/':
	//		link_path[len2++] = '\\';
	//		link_path[len2++] = '\\';
	//		break;
		 //case ':':
		//	link_path[len2++] = ':';
		//	link_path[len2++] = '';
	//		break;
	    case ' ':
            link_path[len2++] = '%';
			link_path[len2++] = '2';
			link_path[len2++] = '0';
			break;
	    default:
			link_path[len2] = path[i];
			len2++;
			break;
   }

   }

return 0;
}



int
AnsiToUnicodeString(
    LPSTR pAnsi,
    LPWSTR pUnicode,
    DWORD StringLength
    )
{
    int iReturn;

    if( StringLength == NULL_TERMINATED )
        StringLength = strlen( pAnsi );

    iReturn = MultiByteToWideChar(CP_ACP,
                                  MB_PRECOMPOSED,
                                  pAnsi,
                                  StringLength + 1,
                                  pUnicode,
                                  StringLength + 1 );

    //
    // Ensure NULL termination.
    //
    pUnicode[StringLength] = 0;

    return iReturn;
}


int
UnicodeToAnsiString(
    LPWSTR pUnicode,
    LPSTR pAnsi,
    DWORD StringLength
    )
{
    LPSTR pTempBuf = NULL;
    INT   rc = 0;

    if( StringLength == NULL_TERMINATED ) {

        //
        // StringLength is just the
        // number of characters in the string
        //
        StringLength = wcslen( pUnicode );
    }

    //
    // WideCharToMultiByte doesn't NULL terminate if we're copying
    // just part of the string, so terminate here.
    //

    pUnicode[StringLength] = 0;

    //
    // Include one for the NULL
    //
    StringLength++;

    //
    // Unfortunately, WideCharToMultiByte doesn't do conversion in place,
    // so allocate a temporary buffer, which we can then copy:
    //

    if( pAnsi == (LPSTR)pUnicode )
    {
        pTempBuf = (LPSTR)LocalAlloc( LPTR, StringLength );
        pAnsi = pTempBuf;
    }

    if( pAnsi )
    {
        rc = WideCharToMultiByte( CP_ACP,
                                  0,
                                  pUnicode,
                                  StringLength,
                                  pAnsi,
                                  StringLength,
                                  NULL,
                                  NULL );
    }

    /* If pTempBuf is non-null, we must copy the resulting string
     * so that it looks as if we did it in place:
     */
    if( pTempBuf && ( rc > 0 ) )
    {
        pAnsi = (LPSTR)pUnicode;
        strcpy( pAnsi, pTempBuf );
        LocalFree( pTempBuf );
    }

    return rc;
}


LPWSTR
AllocateUnicodeString(
    LPSTR  pAnsiString
    )
{
    LPWSTR  pUnicodeString = NULL;

    if (!pAnsiString)
        return NULL;

    pUnicodeString = (LPWSTR)LocalAlloc(
                        LPTR,
                        strlen(pAnsiString)*sizeof(WCHAR) +sizeof(WCHAR)
                        );

    if (pUnicodeString) {

        AnsiToUnicodeString(
            pAnsiString,
            pUnicodeString,
            NULL_TERMINATED
            );
    }

    return pUnicodeString;
}

