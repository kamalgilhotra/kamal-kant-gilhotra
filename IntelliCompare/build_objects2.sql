-- 20190519
ALTER TABLE PP_OBJECTS_IC ADD (full_library_path VARCHAR2(1000));

UPDATE pp_objects_ic SET full_library_path = library_name;

UPDATE pp_objects_ic SET library_name = Substr(full_library_path, Length(full_library_path) - (Instr(Reverse(full_library_path), '\', 1, 1) - 2));

CREATE TABLE IC_OBJECT_USAGE (UPGRADE_ID NUMBER, SEARCH_OBJECT VARCHAR2(256), FROM_PBL VARCHAR2(260), FOUND_IN_PBL VARCHAR2(260), FOUND_IN_OBJECT VARCHAR2(260), PRIMARY KEY (UPGRADE_ID, SEARCH_OBJECT, FROM_PBL));

Create table dc_base_libraries_ic (
	library_name VARCHAR2(254),
	time_stamp DATE,
	user_id VARCHAR2(18))
;

ALTER TABLE dc_base_libraries_ic ADD (CONSTRAINT pk_dc_base_libraries_ic PRIMARY KEY (library_name));

INSERT INTO dc_base_libraries_ic  VALUES (
	'adapters.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'dwtemp.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'fusioncharts.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'lessordw.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'lessorgui.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'lessorlogic.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'lsdw.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'lsgui.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'lslogic.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'powerfilter.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'powerpla.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pparchive.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pparo.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppbase_sqlstatements.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppbasegui.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppbaseguidw.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppbaseimport.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppbaselogic.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppbasequery.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppbasereporting.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppbatch.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppbudget.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppcatlog.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppcostrp.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppcpr.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppcprdw.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppdepr.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppdeprfcst.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppextensions.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppmypp.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppprojct.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppprojdw.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppreggui.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppreglogic.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppreimbursable.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppstub.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppstudy.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppsystem.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pptables.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pptax.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pptaxaccrual.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pptaxaccrual_reports.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pptaxaccrual_tbbs.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pptaxalo.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pptaxamtace.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pptaxarc.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pptaxdll.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pptaxint.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pptaxmng.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pptaxrep.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pptaxrepairs.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pptaxrepairsdw.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'pptaxrepairsreports.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ptccenters.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ptccentersdw.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ptcdw.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ptclogic.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ptcreports.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ptcreturns.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'taxcenters.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'taxcentersdw.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'taxdw.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'taxlogic.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'taxreports.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppbaseexception.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_libraries_ic  VALUES (
	'ppbaseolecontrols.pbl',
	NULL,
	NULL);

COMMIT;

CREATE TABLE dc_base_ssp_ic (
	object_name VARCHAR2(254),
	library_name VARCHAR2(254),
	time_stamp DATE,
	user_id VARCHAR2(18));

INSERT INTO dc_base_ssp_ic VALUES (
	'f_pre_approve_depr_co_custom',
	'ssp_depr_approval_custom',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_depr_calc_and_approve_custom',
	'ppdepr_interface_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_release_je',
	'ssp_release_je_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_reconcile_gl',
	'ssp_gl_recon_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_cpr_control_close_month',
	'ssp_close_powerplant_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_rwip_allo_load',
	'ppdepr_interface_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_allo_reserve_custom',
	'ppdepr_interface_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_wo_control_pre_afudc',
	'ppprojct_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_wip_comp_extension',
	'ppprojct_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_accrual_custom_calc_validation',
	'ssp_wo_calculate_accruals_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_accrual_custom_validation',
	'ssp_wo_calculate_accruals_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_non_unitization_audit',
	'ppprojct_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_unitization_audit',
	'ppprojct_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_create_wo_gl_trans',
	'ssp_release_je_wo_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_reconcile_gl_wo',
	'ssp_wo_gl_reconciliation_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_wo_control_close_month',
	'ssp_wo_close_month_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_budget_afudc_custom',
	'ppprojct_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_budget_closings_pct',
	'ppprojct_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_budget_overheads_custom',
	'ppprojct_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_budget_update_with_act_custom',
	'ppprojct_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_cr_to_budget_to_cr_custom',
	'ppprojct_custom.pbl',
	NULL,
	NULL);
INSERT INTO dc_base_ssp_ic VALUES (
	'f_wo_cr_derivations_custom',
	'ppprojct_custom.pbl',
	NULL,
	NULL);


COMMIT;

