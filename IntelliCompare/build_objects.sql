create table upgrade_master (upgrade_id number, client_name varchar2(200) not null, current_version varchar2(50) not null, upgrade_version varchar2(50) not null, matching_base_version_path varchar2(500), current_version_path varchar2(500), upgrade_version_path varchar2(500), primary key (upgrade_id));
alter table upgrade_master add (master_file_path varchar2(500), report_save_path varchar2(500));
alter table upgrade_master add (current_status varchar2(200), final_review varchar2(1) default 'N', mb_loaded varchar2(1) default 'N', current_loaded varchar2(1) default 'N', upgrade_loaded varchar2(1) default 'N');

create table intelli_data (object_name varchar2(255), modified_time varchar2(30), object_comments varchar2(1000), object_type varchar2(20), object_name_type varchar2(260), library_name varchar2(256), full_library_path varchar2(1000), library_sequence number, version_number varchar2(50), version_type varchar2(30), upgrade_id number, equivalent_matching_base varchar2(1000), equivalent_current_customer varchar2(1000), equivalent_upgrade varchar2(1000), entry_dt date, primary key (upgrade_id, object_name, library_name, object_type, version_type));
alter table intelli_data add constraint fk_intelli_data_upgd_id foreign key (upgrade_id) references upgrade_master(upgrade_id);

create table pp_objects_ic (object_name varchar2(255), library_name varchar2(256), modify_date date, compile_date date, "COMMENT" varchar2(1000), note varchar2(1000),
time_stamp date, user_id varchar2(18), status varchar2(1), "ORDER" number(22), upgrade_id number, entry_dt date, primary key (object_name, library_name, upgrade_id));
alter table pp_objects_ic add constraint fk_pp_objects_ic_upgd_id foreign key (upgrade_id) references upgrade_master(upgrade_id);

create table intelli_source (upgrade_id number, library_name varchar2(500), object_name varchar2(500), lower_source_code clob, object_num number, library_type varchar2(15), primary key (upgrade_id, library_name, object_name, object_num));
alter table intelli_source add constraint fk_intelli_source_upgd_id foreign key (upgrade_id) references upgrade_master(upgrade_id);

create table intelli_data_arc as select * from intelli_data where 1 = 0;