SET ECHO ON
SET TIME ON


SET SQLPROMPT '_CONNECT_IDENTIFIER> '

/* Set PP_SCRIPT_PATH = to the path of the update scripts. */
/* If you are running sqlplus from the script directory    */
/* then there is no need to set PP_SCRIPT_PATH.            */
/* Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'       */
DEFINE PP_SCRIPT_PATH=""

/*
***** NOTE: The following statement will exit SQLPlus if any ORA- errors occur.
*****       If SQLPlus exists then review last Log file, resolve issues and
*****       continue running the rest of the scripts after the error location.
*/
WHENEVER SQLERROR exit failure rollback

SPOOL &&PP_SCRIPT_PATH.file.log
@&&PP_SCRIPT_PATH.file.sql
SPOOL OFF

