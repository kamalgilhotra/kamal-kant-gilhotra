select 1 from dual;

/*
|| Log the run of the script
*/
insert into PP_UPDATE_FLEX
                (COL_ID,
                 TYPE_NAME,
                 FLEXVCHAR1)
select NVL(MAX(COL_ID), 0) + 1,
                 '2018_LKE_Upgrade_Scripts', --this should be type name
                'file.sql' --this should be your script name
from PP_UPDATE_FLEX;
commit;
