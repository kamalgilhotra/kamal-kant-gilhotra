//go:generate goversioninfo -icon=icon.ico
package main

import (
    "os"
    "fmt"
    "io/ioutil"
    "strings"
    "bufio"
    "flag"
)

func check(err error){
    if err != nil {
        panic(err)
    }
}

func slugify(path string) string {
    replacements := []string{" ", "\\", "/", "'"}
    for _, str := range replacements {
        path = strings.Replace(path, str, "_", -1)
    }
    return path
}

func visit(path string, f os.FileInfo, err error) error {
    inFile, _ := os.Open(path)
    defer inFile.Close()
    outFile, err := os.Create("formatted/"+slugify(path))
    check(err)
    defer outFile.Close()
    writer := bufio.NewWriter(outFile)
    scanner := bufio.NewScanner(inFile)
    scanner.Split(bufio.ScanLines)
    quoted := false
    ended := false
    commented := false
    beginComment := false
    beginMultiComment := false
    appendLog := true
    
	var scriptType string
	fmt.Print(" \r\n")
	fmt.Print("**************************************************************************************************************************\r\n")
    fmt.Print("Default Script Type value = CustomScript\r\n")
	fmt.Print(" \r\n")
	fmt.Print("If you wish to change Script Type, key in the new Script Type value (without spaces) and press ENTER to continue.\r\n")
	fmt.Print(" \r\n")
	fmt.Print("If you wish to use the default Script Type, just press ENTER to continue.\r\n")
	fmt.Print("**************************************************************************************************************************\r\n")
	fmt.Print(" \r\n")
	fmt.Print("Type or press enter: ")
	var customScriptType string
    writer.Flush()
    fmt.Scanln(&customScriptType)
	
	if strings.TrimSpace(customScriptType) == "" {
	   scriptType = "CustomScript"
	} else {
	   scriptType = string(customScriptType)
	}
	fmt.Print(" \r\n")
	fmt.Print("**************************************************************************************************************************\r\n")
	fmt.Print(" \r\n")
	fmt.Print("Using the Script Type value: " + scriptType + "\r\n")
    fmt.Print(" \r\n")
	fmt.Print("**************************************************************************************************************************\r\n")
	for scanner.Scan() {
        text := scanner.Text()
        if strings.Contains(strings.ToUpper(text), "PP_UPDATE_FLEX"){
            appendLog = false
        }
        if !appendLog && strings.Contains(strings.ToLower(text), "--this should be your script name") && strings.Count(text, "'") == 2{
            text = text[:strings.Index(text, "'")+1] + f.Name() + text[strings.LastIndex(text, "'"):]
        }
        if !appendLog && strings.Contains(strings.ToLower(text), "--this should be type name") && strings.Count(text, "'") == 2{
            text = text[:strings.Index(text, "'")+1] + scriptType + text[strings.LastIndex(text, "'"):]
        }
        if len(strings.TrimSpace(text)) == 0 && !ended {
            continue
        }
        for i := 0 ; i < len(text); i++ {
            if !commented && !quoted{
                if string(text[i]) == "-"{
                    if !beginComment {
                        beginComment = true
                    } else {
                        beginComment = false
                        break
                    }
                } else {
                    beginComment = false
                }
                if beginMultiComment{
                    beginMultiComment = false
                    if string(text[i]) == "*" {
                        commented = true
                    }
                }
                if string(text[i]) == "/"{
                    beginMultiComment = true
                }
            }
            if commented {
                if beginMultiComment{
                    beginMultiComment = false
                    if string(text[i]) == "/" {
                        commented = false
                    }
                }
                if string(text[i]) == "*"{
                    beginMultiComment = true
                }
            }
            if string(text[i]) == "'" && !commented{
                quoted = !quoted
            }
            if !quoted && !commented {
                ended = false
                if string(text[i]) == ";"{
                    ended = true
                }
                if string(text[i]) == "/" && i == 0{
                    ended = true
                }
            }
            if !quoted && !ended && !commented {
                // fmt.Print(string(text[i]))
            }
        }
        beginMultiComment = false
        beginComment = false
        _,err := writer.WriteString(text+"\r\n")
        check(err)
    }
    if appendLog {
        _,err = writer.WriteString("\r\n/*\r\n|| Log the run of the script\r\n*/\r\ninsert into PP_UPDATE_FLEX\r\n                (COL_ID,\r\n                 TYPE_NAME,\r\n                 FLEXVCHAR1)\r\nselect NVL(MAX(COL_ID), 0) + 1,\r\n                 '" + scriptType + "', --this should be type name\r\n                '"+f.Name()+"' --this should be your script name\r\nfrom PP_UPDATE_FLEX;\r\ncommit;\r\n")
        check(err)
    }
    fmt.Printf("Done: %s\n", path)
    writer.Flush()
    return nil
}


func main() {
    logsPtr := flag.String("logs", "", "folder to place log files (defaults to the regular path)")
    flag.Parse()
    logs := *logsPtr
    os.Mkdir("formatted", 0777)
    files, _ := ioutil.ReadDir("./")
    outFile, err := os.Create("formatted/RENAME_ME.txt")
    check(err)
    defer outFile.Close()
    writer := bufio.NewWriter(outFile)
    _, err = writer.WriteString("SET ECHO ON\r\nSET TIME ON\r\n\r\n\r\nSET SQLPROMPT '_CONNECT_IDENTIFIER> '\r\n\r\n/* Set PP_SCRIPT_PATH = to the path of the update scripts. */\r\n/* If you are running sqlplus from the script directory    */\r\n/* then there is no need to set PP_SCRIPT_PATH.            */\r\n/* Example: DEFINE PP_SCRIPT_PATH='C:\\temp\\scripts\\'       */\r\nDEFINE PP_SCRIPT_PATH=\"\"\r\n\r\n/*\r\n***** NOTE: The following statement will exit SQLPlus if any ORA- errors occur.\r\n*****       If SQLPlus exists then review last Log file, resolve issues and\r\n*****       continue running the rest of the scripts after the error location.\r\n*/\r\nWHENEVER SQLERROR exit failure rollback\r\n\r\n") 
    
    if logs != "" && string(logs[len(logs)-1:]) != "\\"{
        logs = logs + "\\"
        writer.WriteString("host mkdir "+logs+"\r\n\r\n")
    }
    for _, f := range files {
        if !f.IsDir() && strings.HasSuffix(strings.ToLower(f.Name()), ".sql"){
            visit(f.Name(), f, nil)
            writer.WriteString("SPOOL &&PP_SCRIPT_PATH."+logs+slugify(f.Name())[:len(f.Name())-4]+".log\r\n")
            writer.WriteString("@&&PP_SCRIPT_PATH."+slugify(f.Name())+"\r\nSPOOL OFF\r\n\r\n")
        }
    }
    fmt.Print("Finished. Press ENTER to quit.")
    var input string
    writer.Flush()
    fmt.Scanln(&input)
}