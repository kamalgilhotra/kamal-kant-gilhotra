For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set mydate=%%c%%a%%b)
For /f "tokens=1-3 delims=/:" %%a in ("%TIME%") do (set mytime=%%a%%b%%c)
md K:\LKE\ForTraining\%mydate%%mytime%

copy "R:\LKE\scripts\20_2018_upgrade_LKE_scripts\part_1\0521_LKE_B50_create_mass_upload_tables_DDL.sql" "K:\LKE\ForTraining\%mydate%%mytime%"
if %errorlevel% neq 0 goto error_came
copy "R:\LKE\scripts\20_2018_upgrade_LKE_scripts\part_1\0522_LKE_B50_add_powerplant_tables_DML.sql" "K:\LKE\ForTraining\%mydate%%mytime%"
if %errorlevel% neq 0 goto error_came
copy "R:\LKE\scripts\20_2018_upgrade_LKE_scripts\part_1\0523_LKE_B50_pp_table_groups_DML.sql" "K:\LKE\ForTraining\%mydate%%mytime%"
if %errorlevel% neq 0 goto error_came
copy "R:\LKE\scripts\20_2018_upgrade_LKE_scripts\part_1\0527_LKE_B51_configure_cr_data_mover_proj_DML.sql" "K:\LKE\ForTraining\%mydate%%mytime%"
if %errorlevel% neq 0 goto error_came
copy "R:\LKE\scripts\20_2018_upgrade_LKE_scripts\part_1\1340_LKE_B53_data_mover_setup_mass_contact_admin_update_dml.sql" "K:\LKE\ForTraining\%mydate%%mytime%"
if %errorlevel% neq 0 goto error_came
copy "R:\LKE\scripts\20_2018_upgrade_LKE_scripts\part_1\1380_LKE_B60_B61_B62_data_mover_setup_peoplesoft_to_pp_hr_attributes_dml.sql" "K:\LKE\ForTraining\%mydate%%mytime%"
if %errorlevel% neq 0 goto error_came
copy "R:\LKE\scripts\20_2018_upgrade_LKE_scripts\part_1\1390_LKE_B68_table_maintenance_setup_peoplesoft_to_pp_hr_attributes_dml.sql" "K:\LKE\ForTraining\%mydate%%mytime%"
if %errorlevel% neq 0 goto error_came
copy "R:\LKE\scripts\20_2018_upgrade_LKE_scripts\part_1\1400_LKE_B59_create_tables_peoplesoft_to_pp_hr_attributes_ddl.sql" "K:\LKE\ForTraining\%mydate%%mytime%"
if %errorlevel% neq 0 goto error_came
copy "R:\LKE\scripts\20_2018_upgrade_LKE_scripts\part_1\1610_LKE_B53_data_mover_setup_mass_task_admin_dml.sql" "K:\LKE\ForTraining\%mydate%%mytime%"
if %errorlevel% neq 0 goto error_came

goto byebye

:error_came
pause


:byebye